﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esx.Web.ViewModels
{
    public class LoanViewModel
    {
        [Required]
        public long LoanID { get; set; }

        public long? DealID { get; set; }

        public IEnumerable<DealViewModel> Deals { get; set; }

        public long? PropertyID { get; set; }

        [Required]
        public string LoanName { get; set; }

        public decimal? LoanBalance { get; set; }

        public DateTime? OriginationDate { get; set; }

        public DateTime? OriginalMaturityDate { get; set; }

        public long? LenderCompanyID { get; set; }
        
        public string Lender { get; set; }

        public IEnumerable<CompanyViewModel> LoanBorrowers { get; set; }
    }
}
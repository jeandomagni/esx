﻿namespace Esx.Web.ViewModels
{
    public class ImpersonateUserViewModel
    {
        public string UserName { get; set; }
        public int UserKey { get; set; }
    }
}
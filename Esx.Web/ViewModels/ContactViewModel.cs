﻿using System.ComponentModel.DataAnnotations;

namespace Esx.Web.ViewModels
{
    public class ContactViewModel
    {
        [Required]
        public long ContactID { get; set; }
        public string MentionId { get; set; }

        public string FullName { get; set; }

        [Required]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string Phone { get; set; }

        public string EmailAddress { get; set; }

        public string Title { get; set; }

        public string Department { get; set; }

        public bool? Active { get; set; }

        public string Status { get; set; }

        [Required]
        public int CategoryID { get; set; }

        public long? CompanyID { get; set; }

        public CompanyViewModel Company { get; set; }
    }
}
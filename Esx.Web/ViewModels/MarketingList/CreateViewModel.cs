﻿using Esx.Contracts.Models.MarketingList;

namespace Esx.Web.ViewModels.MarketingList
{
    public class CreateViewModel
    {
        public string DealMentionId { get; set; }
        public MarketingListLookup[] ExistingLists { get; set; }
    }
}
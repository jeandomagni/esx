﻿using System;

namespace Esx.Web.ViewModels.MarketingList
{
    public class UpdateViewModel
    {
        public string[] ContactMentionIds { get; set; }
        public string Status { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Day { get; set; }
    }
}
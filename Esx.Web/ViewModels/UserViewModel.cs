﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Esx.Web.ViewModels
{
    public class UserViewModel
    {
        public string FullName { get; set; }
        public string UserKey { get; set; }
        public bool IsPrincipal { get; set; }
        public string PrincipalUserFullName { get; set; }


    }
}
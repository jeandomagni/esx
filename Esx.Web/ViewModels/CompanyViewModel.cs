﻿using System.ComponentModel.DataAnnotations;

namespace Esx.Web.ViewModels
{
    public class CompanyViewModel
    {
        [Required]
        public long CompanyID { get; set; }

        [Required]
        public string CompanyName { get; set; }

        public string CompanyEmail { get; set; }

        public string Website { get; set; }

        public string Status { get; set; }

        public int? NumActiveContacts { get; set; }

        public int? NumContacts { get; set; }

        public int? NumDealsAsInv { get; set; }

        public int? NumDealsAsClient { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Esx.Web.ViewModels
{
    public class EmployeeViewModel
    {
        public long EmployeeID { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeeInitials { get; set; }

        public string EmployeeType { get; set; }

        public string UserName { get; set; }

        public string UserType { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Nullable<int> CompanyID { get; set; }

        public string CompanyName { get; set; }

        public string LegacyCompany { get; set; }

        public string Title { get; set; }

        public string EmailAddress { get; set; }

        public string Salutation { get; set; }

        public string StreetAddress { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Phone { get; set; }

        public string MobilePhone { get; set; }

        public string WorkFax { get; set; }

        public string WorkPhone { get; set; }

        public string Extension { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Esx.Web.ViewModels
{
    public class IsDelegatedUserViewModel
    {
        public  int UserKey { get; set; }
        public bool IsDelegated { get; set; }
    }
}
﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Esx.Web.ViewModels
{
    public class AddCompanyViewModel
    {        
        public long CompanyID { get; set; }

        [Required]
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }

        public string AliasName { get; set; }

        public string Website { get; set; }

        public string Phone { get; set; }

        public string CompanyEmail { get; set; }

        // Parent Company
        public int ParentCompany { get; set; }

        // Location Properties
        public string LocationName { get; set; }

        [Required]
        [DisplayName("Location Address 1")]
        public string LocationAddress1 { get; set; }

        public string LocationAddress2 { get; set; }

        [Required]
        [DisplayName("Location City")]
        public string LocationCity { get; set; }

        [Required]
        [DisplayName("Location State")]
        public string LocationState { get; set; }

        [Required]
        [DisplayName("Location Zip/Postal Code")]
        public string LocationZip { get; set; }

        [Required]
        [DisplayName("Location Country")]
        public string LocationCountry { get; set; }

        public string LocationPhone { get; set; }

        // Contact Properties
        public int ContactKey { get; set; }

        [Required]
        [DisplayName("Contact Name")]
        public string ContactName { get; set; }

        public string ContactTitle { get; set; }

        public string ContactEmailAddress { get; set; }

        public string ContactMobilePhone { get; set; }

        public string ContactOfficePhone { get; set; }

        public string ContactHomePhone { get; set; }

        public string ContactAddress1 { get; set; }

        public string ContactAddress2 { get; set; }

        public string ContactCity { get; set; }

        public string ContactState { get; set; }

        public string ContactZip { get; set; }

        public string ContactCountry { get; set; }

        public bool SetDefaultCountry { get; set; }
    }
}
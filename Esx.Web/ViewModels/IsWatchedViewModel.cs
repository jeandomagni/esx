﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Esx.Web.ViewModels
{
    public class IsWatchedViewModel
    {
        public string MentionId { get; set; }
        public bool IsWatched { get; set; }
    }
}
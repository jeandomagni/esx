﻿using System.ComponentModel.DataAnnotations;

namespace Esx.Web.ViewModels
{
    public class PropertyViewModel
    {
        [Required]
        public long PropertyID { get; set; }

        [Required]
        public string PropertyName { get; set; }

        public string Address1 { get; set; }

        public string City { get; set; }

        public string State { get; set; }
    }
}
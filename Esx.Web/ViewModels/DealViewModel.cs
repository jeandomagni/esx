﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Esx.Web.ViewModels
{
    public class DealViewModel
    {
        [Required]
        public long DealID { get; set; }

        [Required]
        public string DealName { get; set; }

        public string DealCode { get; set; }

        public string PreCode { get; set; }

        public string DealOffice { get; set; }

        public string DealCallNote { get; set; }

        public string TransactionType { get; set; }

        public long DealCompanyID { get; set; }

        public string DealCompanyName { get; set; }

    }
}
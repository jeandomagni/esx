﻿using AutoMapper;
using Esx.Business.Companies;
using Esx.Business.Deals;
using Esx.Business.Properties;
using Esx.Contracts.Paging;
using System.Collections.Generic;
using System.Web.Http;
using Esx.Business.Loans;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Models.Loan;
using Esx.Contracts.Models.Property;
using Esx.Contracts.Models.Search;


namespace Esx.Web.Controllers
{
    [RoutePrefix("api/properties")]
    public class PropertiesController : ApiController
    {
        private readonly IPropertiesService _propertiesService;
        private readonly ILoansService _loansService;
        private readonly ICompaniesService _companiesService;
        private readonly IDealsService _dealsService;
        private readonly IMappingEngine _mapper;

        public PropertiesController(
            ILoansService loansService,
            IPropertiesService propertiesService,
            ICompaniesService companiesService,
            IDealsService dealsService,
            IMappingEngine mapper)
        {
            _loansService = loansService;
            _propertiesService = propertiesService;
            _companiesService = companiesService;
            _dealsService = dealsService;
            _mapper = mapper;
        }

        
        [HttpPost]
        [Route("addproperty")]
        public NewProperty AddProperty(NewProperty property)
        {
            //var result = _propertiesService.AddProperty(property);
            //property.MentionId = result; 
            //return new NewProperty { MentionId=result};
            return new NewProperty();
        }

        [HttpGet]
        [Route("list")]
        public PagedResources<PropertyListItem> List(int pageSize = 20, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return _propertiesService.Search(new PropertySearchCriteria
            {
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            });
        }

        [HttpGet]
        [Route("{id}/details")]
        public PropertyDetails GetPropertyDetails(string id)
        {
            return _propertiesService.GetPropertyDetails(id);
        }

        [HttpGet]
        [Route("{id}/valuations")]
        public PagedResources<ValuationDetails> GetPropertyValuations(string id, int pageSize = 30, int offset = 0)
        {
            var pagingData = new PagingData
            {
                PageSize = pageSize,
                Offset = offset
            };
            return _propertiesService.GetPropertyValuations(pagingData, id);
        }

        [HttpGet]
        [Route("{id}/deals")]
        public PagedResources<DealListItem> GetDeals(string id, int pageSize = 10, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return _dealsService.Search(new DealSearchCriteria
            {
                PropertyMentionId = id,
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            });
        }

        [HttpGet]
        [Route("{id}/loans")]
        public PagedResources<LoanListItem> GetLoans(string id, int pageSize = 10, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return _loansService.Search(new LoanSearchCriteria
            {
                PropertyMentionId = id,
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            });
        }

        [HttpGet]
        [Route("intellisense")]
        public IEnumerable<PropertyIntellisense> Intellisense(string query)
        {
            return _propertiesService.PropertyIntellisense(query);
        }
    }
}

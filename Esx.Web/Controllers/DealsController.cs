﻿using Esx.Business.Deals;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Paging;
using System.Web.Http;
using System.Collections.Generic;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Search;
using Esx.Business.Properties;
using Esx.Contracts.Models.Property;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/deals")]
    public class DealsController : ApiController
    {
        private readonly IDealsService _dealsService;
        private readonly IPropertiesService _propertiesService;

        public DealsController(IDealsService dealsService, IPropertiesService propertiesService)
        {
            _dealsService = dealsService;
            _propertiesService = propertiesService;
        }

        [HttpGet]
        [Route("list")]
        public PagedResources<DealListItem> GetDealsList(int pageSize = 20, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null, bool? isPitch = null)
        {
            return _dealsService.Search(new DealSearchCriteria
            {
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            }, isPitch);
        }

        [HttpGet]
        [Route("{id}/summary")]
        public DealSummary GetDealSummary(string id)
        {
            return _dealsService.GetDealSummary(id);
        }

        [HttpGet]
        [Route("{id}/properties")]
        public PagedResources<PropertyListItem> GetDealProperties(string id, int pageSize = 20, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            var searchCriteria = new PropertySearchCriteria
            {
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending,
                DealMentionId = id,
            };
            return _propertiesService.Search(searchCriteria);
        }
        
        [HttpGet]
        [Route("{id}/propertyUsages")]
        public IEnumerable<dESx_AssetUsageType> GetDealPropertyPrimaryUsages(int id, [FromUri]params int[] propertyAssetKeys)
        {
            return _dealsService.GetDealPropertyPrimaryUsages(id, propertyAssetKeys);
        }

        [HttpGet]
        [Route("{id}/latestValuation")]
        public dESx_DealValuation GetLatestValuation(int id, [FromUri]params int[] propertyAssetKeys)
        {
            return _dealsService.GetLatestValuation(id, propertyAssetKeys) ?? new dESx_DealValuation();
        }


        [HttpGet]
        [Route("GetActivePipeline")]
        public IHttpActionResult GetActivePipeline()
        {
            var result = new List<ActivePipelineDeal>();
            result.Add(new ActivePipelineDeal()
            {
                DealId = 0,
                Name = "1200 Park Ave",
                EntityType = "Property",
                Office = "New York",
                Client = "BlackStone Group",
                DealType = "Equity Sale",
                PropertyType = "Office, Retail",
                EstPrice = 700000000,
                EstFees = 500000,
                Status = "Marketing",
                PitchLaunchDate = new System.DateTime(2016, 2, 15),
                MyDeal = false,
                MyOffice = true
            });

            result.Add(new ActivePipelineDeal()
            {
                DealId = 1,
                Name = "500 Bogner Place",
                EntityType = "Property",
                Office = "Chicago",
                Client = "Johnstone Investments",
                DealType = "Equity Sale",
                PropertyType = "Office",
                EstPrice = 312000000,
                EstFees = 225000,
                Status = "Marketing",
                PitchLaunchDate = new System.DateTime(2016, 3, 1),
                MyDeal = false,
                MyOffice = false
            });

            result.Add(new ActivePipelineDeal()
            {
                DealId = 2,
                Name = "Esperanza Hotel",
                EntityType = "Pitch",
                Office = "New York",
                Client = "Paradigm Hotel Group",
                DealType = "Debt Placement",
                PropertyType = "Hotel",
                EstPrice = 296000000,
                EstFees = 182000,
                Status = "Packaging",
                PitchLaunchDate = new System.DateTime(2016, 4, 15),
                MyDeal = false,
                MyOffice = true
            });

            result.Add(new ActivePipelineDeal()
            {
                DealId = 3,
                Name = "Linehan Center",
                EntityType = "Pitch",
                Office = "Los Angeles",
                Client = "Mile High Commercial Reality",
                DealType = "Loan Sale",
                PropertyType = "Office",
                EstPrice = 750000000,
                EstFees = 550000,
                Status = "Pitching",
                PitchLaunchDate = new System.DateTime(2016, 4, 5),
                MyDeal = false,
                MyOffice = false
            });

            result.Add(new ActivePipelineDeal()
            {
                DealId = 4,
                Name = "Marina City",
                EntityType = "Pitch",
                Office = "Chicago",
                Client = "Lasalle Hotel Properties",
                DealType = "Equity Sale",
                PropertyType = "Residential, Retail, Hotel",
                EstPrice = 842000000,
                EstFees = 628000,
                Status = "Pitching",
                PitchLaunchDate = new System.DateTime(2016, 3, 31),
                MyDeal = false,
                MyOffice = false
            });

            result.Add(new ActivePipelineDeal()
            {
                DealId = 5,
                Name = "Nakatomi Plaza",
                EntityType = "Pitch",
                Office = "Los Angeles",
                Client = "Nakatomi Corporation",
                DealType = "Equity Sale",
                PropertyType = "Office",
                EstPrice = 921000000,
                EstFees = 714000,
                Status = "Packaging",
                PitchLaunchDate = new System.DateTime(2016, 5, 1),
                MyDeal = false,
                MyOffice = false
            });

            result.Add(new ActivePipelineDeal()
            {
                DealId = 6,
                Name = "Stuyvesant Towers",
                EntityType = "Property",
                Office = "New York",
                Client = "Village Commercial Reality",
                DealType = "Debt Placement",
                PropertyType = "Office, Retail",
                EstPrice = 485000000,
                EstFees = 330000,
                Status = "Marketing",
                PitchLaunchDate = new System.DateTime(2016, 1, 28),
                MyDeal = false,
                MyOffice = true
            });

            result.Add(new ActivePipelineDeal()
            {
                DealId = 7,
                Name = "Traxis Tower",
                EntityType = "Property",
                Office = "New York",
                Client = "EyeCorp",
                DealType = "Equity Sale",
                PropertyType = "Office",
                EstPrice = 500000000,
                EstFees = 465000,
                Status = "",
                PitchLaunchDate = new System.DateTime(2016, 1, 15),
                MyDeal = false,
                MyOffice = true
            });

            result.Add(new ActivePipelineDeal()
            {
                DealId = 8,
                Name = "Salt River Fields",
                EntityType = "Property",
                Office = "Scottsdale",
                Client = "Salt River Tribe",
                DealType = "Debt Placement",
                PropertyType = "Commercial",
                EstPrice = 200000000,
                EstFees = 150000,
                Status = "",
                PitchLaunchDate = new System.DateTime(2016, 1, 1),
                MyDeal = false,
                MyOffice = false
            });

            result.Add(new ActivePipelineDeal()
            {
                DealId = 9,
                Name = "125 Worth Street",
                EntityType = "Property",
                Office = "New York",
                Client = "City of New York",
                DealType = "Equity Sale",
                PropertyType = "Office",
                EstPrice = 27000000,
                EstFees = 90000,
                Status = "",
                PitchLaunchDate = new System.DateTime(2015, 12, 1),
                MyDeal = true,
                MyOffice = true
            });


            return Ok(result);
        }

        [HttpGet]
        [Route("GetPipelineClosedDeals")]
        public IHttpActionResult GetPipelineClosedDeals()
        {
            var result = new List<ClosedPipelineDeal>();
            result.Add(new ClosedPipelineDeal()
            {
                DealId = 0,
                Name = "1 Battery Park Place",
                EntityType = "Property",
                Office = "New York",
                Client = "Blackstone Group",
                Buyer = "Rudin Management",
                DealType = "Equity Sale",
                PropertyType = "Office, Retail",
                PurchasePrice = 700000000,
                Fees = 5000000,
                BidsRecieved = 28,
                MyDeal = false,
                MyOffice = true
            });
            result.Add(new ClosedPipelineDeal()
            {
                DealId = 1,
                Name = "123 Division Street",
                EntityType = "Property",
                Office = "Chicago",
                Client = "The Carlyle Group",
                Buyer = "Tych Investments",
                DealType = "Equity Sale",
                PropertyType = "Hotel",
                PurchasePrice = 312000000,
                Fees = 225000,
                BidsRecieved = 22,
                MyDeal = false,
                MyOffice = false
            });
            result.Add(new ClosedPipelineDeal()
            {
                DealId = 2,
                Name = "2500 Mariott Road",
                EntityType = "Property",
                Office = "New York",
                Client = "Washington Financial",
                Buyer = "Google",
                DealType = "Debt Placement",
                PropertyType = "Office",
                PurchasePrice = 296000000,
                Fees = 182000,
                BidsRecieved = 36,
                MyDeal = false,
                MyOffice = true
            });
            result.Add(new ClosedPipelineDeal()
            {
                DealId = 3,
                Name = "56 Race Street",
                EntityType = "Property",
                Office = "New York",
                Client = "Colony American Homes",
                Buyer = "56 Race Street LLC",
                DealType = "Equity Sale",
                PropertyType = "Office",
                PurchasePrice = 25000000,
                Fees = 100000,
                BidsRecieved = 1,
                MyDeal = true,
                MyOffice = true
            });
            result.Add(new ClosedPipelineDeal()
            {
                DealId = 4,
                Name = "Chatham Square",
                EntityType = "Property",
                Office = "New York",
                Client = "BlackStone",
                Buyer = "Mulberry St Reality",
                DealType = "Equity Sale",
                PropertyType = "Residential",
                PurchasePrice = 5000000,
                Fees = 50000,
                BidsRecieved = 32,
                MyDeal = true,
                MyOffice = true
            });
            result.Add(new ClosedPipelineDeal()
            {
                DealId = 5,
                Name = "Ghery Towers",
                EntityType = "Property",
                Office = "Chicago",
                Client = "Bank of America",
                Buyer = "Five Guys LLC",
                DealType = "Equity Sale",
                PropertyType = "Office,Retail",
                PurchasePrice = 90000000,
                Fees = 5000000,
                BidsRecieved = 12,
                MyDeal = false,
                MyOffice = false
            });

            return Ok(result);
        }
    }
}

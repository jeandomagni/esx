﻿using System.Linq;
using System.Web.Http;
using Esx.Business.Comments;
using Esx.Business.Mentions;
using Esx.Contracts.Models.Comment;
using Esx.Contracts.Models.Mention;
using Esx.Contracts.Paging;
using Esx.Web.ViewModels;

namespace Esx.Web.Controllers
{

    [RoutePrefix("api/mentions")]
    public class MentionsController : ApiController
    {
        private readonly IMentionsService _mentionsService;
        private readonly ICommentsService _commentsService;

        public MentionsController(IMentionsService mentionsService, ICommentsService commentsService)
        {
            _mentionsService = mentionsService;
            _commentsService = commentsService;
        }

        [HttpGet]
        [Route("blahblah")]
        public PagedResources<Mention> Get(string query, int count = 10)
        {
            var result = new PagedResources<Mention>
            {
                PagingData = new PagingData
                {
                    PageSize = count
                },
                Resources = _mentionsService.GetMentions(query, count)
            };
            result.PagingData.TotalCount = result.Resources.Count();
            return result;
        }

        [HttpGet]
        [Route("List")]
        public PagedResources<Mention> List(int pageSize = 10, int offset = 0, string sort = null,
            bool sortDescending = false, string searchText = null, string coreObjectType = null)
        {
            var pagingData = new PagingData
            {
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            };

            return _mentionsService.GetMentions2(pagingData, searchText, coreObjectType);
        }

        [HttpPut]
        [Route("setIsWatched")]
        public void SetIsWatched(IsWatchedViewModel model)
        {
            _mentionsService.SetIsWatched(model.MentionId, model.IsWatched);
        }

        [HttpPost]
        [Route("addReminder")]
        public Reminder AddReminder(Reminder reminder)
        {
            return _commentsService.AddReminder(reminder);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Esx.Business.Documents;
using Esx.Contracts.Models.Document;
using Esx.Web.Common;
using Esx.Web.ViewModels;

namespace Esx.Web.Controllers
{
    /// <summary>
    ///   This code is from a sample on MSDN  by Jay Chase
    ///   https://code.msdn.microsoft.com/AngularJS-with-Web-API-22f62a6e
    /// </summary>
    [RoutePrefix("api/documents")]
    public class DocumentsController : ApiController
    {
        private readonly IDocumentsService _documentsService;
        private readonly string workingFolder = HttpRuntime.AppDomainAppPath + @"\Uploads";

        public DocumentsController(IDocumentsService documentsService)
        {
            _documentsService = documentsService;
        }

        [HttpGet]
        [Route("download/{*filePath}")]
        public IHttpActionResult GetFile(string filePath)
        {
            var serverPath = Path.Combine(workingFolder, filePath);
            var fileInfo = new FileInfo(serverPath);

            return !fileInfo.Exists
                ? (IHttpActionResult) NotFound()
                : new FileResult(fileInfo.FullName);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                return BadRequest("Unsupported media type");
            }
            try
            {
                var provider = new CustomMultipartFormDataStreamProvider(workingFolder);
                await Request.Content.ReadAsMultipartAsync(provider);
                var fileData = provider.FileData.LastOrDefault();
                
                var fileInfo = new FileInfo(fileData.LocalFileName);
                var document = new Document
                {
                    Name = fileInfo.Name,
                    Size = fileInfo.Length/1024
                };

                _documentsService.AddDocument(document);

                return Ok(new {Message = "Documents uploaded successfully", Document = document});
            }
            catch (Exception ex)
            {
                return BadRequest(ex.GetBaseException().Message);
            }
        }

        public class FileResult : IHttpActionResult
        {
            private readonly string _filePath;
            private readonly string _contentType;

            public FileResult(string filePath, string contentType = null)
            {
                if (filePath == null) throw new ArgumentNullException(nameof(filePath));

                _filePath = filePath;
                _contentType = contentType;
            }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(File.OpenRead(_filePath))
                };

                var contentType = _contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(_filePath));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);

                return Task.FromResult(response);
            }
        }
    }
}
﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Esx.Business.MarketingList;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Company;
using Esx.Contracts.Models.Contact;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Models.MarketingList;
using Esx.Contracts.Paging;
using Esx.Web.ViewModels.MarketingList;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/marketinglists")]

    public class MarketingListController : ApiController
    {
        private readonly IMarketingListService _marketingListService;

        public MarketingListController(IMarketingListService marketingListService)
        {
            _marketingListService = marketingListService;
        }

        [HttpGet]
        [Route("lookupExisting")]
        public PagedResources<MarketingListLookup> LookupExistingMarketingLists(string searchText, int pageSize = 20, int offset = 0)
        {
            var criteria = new MarketingListSearchCriteria
            {
                PageSize = pageSize,
                Offset = offset,
                SearchText = searchText
            };
            return _marketingListService.FindExistingMarketingLists(criteria);
        }

        [HttpPost]
        [Route("")]
        public MarketingListLookup Create(CreateViewModel vm )
        {
            return _marketingListService.Add(vm.DealMentionId, vm.ExistingLists);
        }

        [HttpGet]
        [Route("deals/{dealMentionId}")]
        public MarketingListLookup Get(string dealMentionId)
        {
            return _marketingListService.GetDealMarketingList(dealMentionId);
        }

        [HttpGet]
        [Route("{marketingListId}/lookupContacts")]
        public PagedResources<MarketingListContact> LookupContacts(int marketingListId, string searchText, int pageSize = 20, int offset = 0)
        {
            var criteria = new MarketingListSearchCriteria
            {
                PageSize = pageSize,
                Offset = offset,
                SearchText = searchText,
                MarketingListId = marketingListId
            };
            return _marketingListService.GetContacts(criteria);
        }

        [HttpGet]
        [Route("{marketingListId}")]
        public PagedResources<MarketingListCompany> Search(int marketingListId, int pageSize = 20, int offset = 0, string contactMentionId = null, string companyMentionId= null, string searchText = null)
        {
            var criteria = new MarketingListSearchCriteria
            {
                PageSize = pageSize,
                Offset = offset,
                MarketingListId = marketingListId,
                ContactMentionId = contactMentionId,
                CompanyMentionId = companyMentionId,
                SearchText = searchText
            };
            return _marketingListService.Search(criteria);
        }

        [HttpPost]
        [Route("{marketingListId}")]
        public MarketingListLookup AddContacts(int marketingListId, string[] mentions)
        {
            return _marketingListService.AddContacts(marketingListId, mentions);
        }

        [HttpPost]
        [Route("{marketingListId}/deleteContacts")]
        public MarketingListLookup RemoveContacts(int marketingListId, string[] contactMentionIds)
        {
            return _marketingListService.RemoveContacts(marketingListId, contactMentionIds);
        }

        [HttpPost]
        [Route("{marketingListId}/status")]
        public MarketingListCompany SetStatus(int marketingListId, UpdateViewModel vm)
        {
            DateTime date;
            if (vm.Day == "today")
                date = DateTime.Today;
            else if (vm.Day == "yesterday")
                date = DateTime.Today.AddDays(-1);
            else
                date = vm.UpdateDate;

            return _marketingListService.SetStatus(marketingListId, vm.ContactMentionIds, vm.Status, date);
        }

        [HttpPost]
        [Route("{marketingListId}/contact/{contactMentionId}")]
        public MarketingListContact UpdateContact(int marketingListId, string contactMentionId, MarketingListContact contact)
        {
            // use rout values
            contact.MarketingListId = marketingListId;
            contact.ContactMentionId = contactMentionId;
            return _marketingListService.UpdateContact(contact);
        }

        [HttpPost]
        [Route("{marketingListId}/CA")]
        public void UpdateCompanyCa(int marketingListId, CompanyCA ca)
        {
            _marketingListService.AddCA(marketingListId, ca);
        }

        [HttpGet]
        [Route("{marketingListId}/export")]
        public IHttpActionResult Export(int marketingListId, string status, bool initial = false)
        {
            //TODO return an actual filestream. 
             var doc = _marketingListService.Export(marketingListId, status, initial);
             return new FileResult(doc);
        }

        private class FileResult : IHttpActionResult
        {
            private readonly byte[] _file;
            private readonly string _contentType;

            public FileResult(byte[] file, string contentType = null)
            {
                if (file == null) throw new ArgumentNullException(nameof(file));

                _file = file;
                _contentType = contentType;
            }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {

                Stream stream = new MemoryStream(_file);
                var response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                return Task.FromResult(response);
            }
        }
        // Engagement stuff
        [HttpGet]
        [Route("companies/engage")]
        public PagedResources<CompanyEngagementItem> GetCompaniesForEngagement(string searchText = null, string companyMentionId = null, int pageSize = 20, int offset = 0)
        {
            var criteria = new MarketingListSearchCriteria
            {
                PageSize = pageSize,
                Offset = offset,
                SearchText = searchText,
                CompanyMentionId = companyMentionId
            };

            return _marketingListService.GetCompaniesForEngagement(criteria);
        }

        [HttpGet]
        [Route("engage/{companyMentionId}/contacts")]
        public PagedResources<MarketingListContact> GetPrimaryContactsForCompany(string companyMentionId, string searchText = null)
        {
            return _marketingListService.GetPrimaryContactsForCompany(companyMentionId, searchText);
        }

        [HttpGet]
        [Route("engage/{companyMentionId}/deals")]
        public PagedResources<DealListItem> GetDealsForCompany(string companyMentionId, string contactMentionId = null, bool hasCa = false)
        {
            return _marketingListService.GetDealsForCompany(companyMentionId, contactMentionId, hasCa);
        }

    }
}
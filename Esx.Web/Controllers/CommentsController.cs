﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Esx.Business.Comments;
using Esx.Contracts.ExceptionHandling;
using Esx.Contracts.Models.Comment;
using Esx.Contracts.Paging;
using Esx.Web.Common;
using Esx.Web.ViewModels;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/comments")]
    public class CommentsController : ApiController
    {
        private readonly ICommentsService _commentsService;

        public CommentsController(ICommentsService commentsService)
        {
            _commentsService = commentsService;
        }

        [HttpGet]
        [Route("list")]
        public PagedResources<CommentListItem> List(string mentionId, int count = 10, int offSet = 0)
        {
            var result = new PagedResources<CommentListItem>
            {
                PagingData = new PagingData
                {
                    PageSize = count,
                    Offset = offSet
                },
                Resources = _commentsService.GetComments(mentionId, count, offSet)
            };
            result.PagingData.TotalCount = result.Resources.Count();
            return result;
        }

        [HttpGet]
        [Route("mentioned/list")]
        public PagedResources<CommentListItem> Mentioned(int pageSize = 10, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null, bool isWatched = false, bool reminders = false, bool isSnoozed = false, bool isDone = false, string mentionId = null)
        {
            var pagingData = new PagingData
            {
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            };

            return _commentsService.GetMentionComments2(pagingData, searchText, isWatched, reminders, isSnoozed, isDone, mentionId);
        }


        [HttpPost]
        [Route("save")]
        public HttpResponseMessage Save(Comment comment)
        {
            int commentId = _commentsService.SaveComment(comment);

            var response = Request.CreateResponse(HttpStatusCode.Created, commentId);
            return response;
        }

        [HttpDelete]
        [Route("delete")]
        public HttpResponseMessage Delete(int commentId)
        {
            _commentsService.DeleteComment(commentId);

            var response = Request.CreateResponse(HttpStatusCode.OK, commentId);
            return response;
        }

        [HttpPost]
        [Route("addReminder")]
        public Reminder AddReminder(Reminder reminder)
        {
            return _commentsService.AddReminder(reminder);
        }

        [HttpPost]
        [Route("snooze")]
        public Reminder Snooze(Reminder reminder)
        {
            if (!reminder.ReminderId.HasValue)
                return null;

            return _commentsService.MarkReminderAsSnoozed(reminder);
        }

        [HttpPost]
        [Route("done")]
        public Reminder Done(Reminder reminder)
        {
            if (!reminder.ReminderId.HasValue)
                return null;

            return _commentsService.MarkReminderAsDone(reminder.ReminderId.Value);
        }

        [HttpDelete]
        [Route("deleteReminder")]
        public IHttpActionResult DeleteReminder(int reminderId)
        {
            _commentsService.DeleteReminder(reminderId);
            return Ok();
        }

        [HttpGet]
        [Route("reminderCount")]
        public object GetActiveRemindersCount()
        {
            var count = _commentsService.GetActiveRemindersCount();
            return new
            {
                Count = count
            };
        }
    }
}

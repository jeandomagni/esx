﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/testSwagger")]
    public class TestSwaggerController : ApiController
    {
        [Route("testAction")]
        [HttpGet]
        public string TestAction(string echo)
        {
            return echo;
        }
    }
}

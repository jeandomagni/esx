﻿using System.Collections.Generic;
using System.Web.Http;
using AutoMapper;
using Esx.Business.Companies;
using Esx.Business.Contacts;
using Esx.Business.Deals;
using Esx.Business.Loans;
using Esx.Business.Properties;
using Esx.Contracts.ExceptionHandling;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Company;
using Esx.Contracts.Models.Contact;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Models.Loan;
using Esx.Contracts.Models.Property;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;
using Esx.Web.ViewModels;
using Alias = Esx.Contracts.Models.Company.Alias;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/companies")]
    public class CompaniesController : ApiController
    {
        private readonly ICompaniesService _companiesService;
        private readonly IContactsService _contactsService;
        private readonly IPropertiesService _propertiesService;
        private readonly ILoansService _loansService;
        private readonly IDealsService _dealsService;
        private readonly IMappingEngine _mapper;

        public CompaniesController(
            ICompaniesService companiesService,
            IContactsService contactsService,
            IPropertiesService propertiesService,
            ILoansService loansService,
            IDealsService dealsService,
            IMappingEngine mapper)
        {
            _companiesService = companiesService;
            _contactsService = contactsService;
            _propertiesService = propertiesService;
            _loansService = loansService;
            _dealsService = dealsService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("list")]
        public PagedResources<CompanyListItem> List(int pageSize = 20, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return _companiesService.Search(new CompanySearchCriteria
            {
                SearchText = searchText,
                SearchPrimaryAlias = true,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            });
        }

        [HttpGet]
        [Route("{id}/statistics")]
        public CompanyStatistics Statistics(string id)
        {
            return _companiesService.GetCompanyStatistics(id);
        }

        [HttpGet]
        [Route("{id}/details")]
        public CompanyDetails GetCompanyDetails(string id)
        {
            return _companiesService.GetCompanyDetails(id);
        }

        [HttpGet]
        [Route("{id}/name")]
        public GenericModel<string> GetCompanyName(string id)
        {
            return new GenericModel<string> { Data = _companiesService.GetCompanyName(id) };
        }

        [HttpGet]
        [Route("{id}/summaryStats")]
        public CompanySummaryStats GetSummaryStats(string id, int pitches, int bidActivity, int holdPeriod, int loansMaturing)
        {
            return _companiesService.GetCompanySummaryStats(id, pitches, bidActivity, holdPeriod, loansMaturing);
        }

        [HttpPost]
        [Route("addAlias")]
        public Alias AddAlias(Alias alias)
        {
            return _companiesService.AddAlias(alias);
        }

        [HttpPost]
        [Route("")]
        public AddCompanyViewModel Add(AddCompanyViewModel company)
        {
            if (!ModelState.IsValid)
            {
                throw new BadRequestException("Invalid company data.");
            }

            var newCompany = _companiesService.AddCompany(_mapper.Map<CompanyAdd>(company));
            return _mapper.Map<AddCompanyViewModel>(newCompany);
        }

        [HttpPatch]
        [Route("")]
        public void Update(CompanyDetails details)
        {
            if (!ModelState.IsValid)
            {
                throw new BadRequestException("Invalid company data");
            }

            _companiesService.UpdateCompany(details);
        }

        [HttpGet]
        [Route("{id}/properties")]
        public PagedResources<PropertyListItem> GetProperties(string id, int pageSize = 20, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return _propertiesService.Search(new PropertySearchCriteria
            {
                CompanyMentionId = id,
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            });
        }

        [HttpGet]
        [Route("{id}/loans")]
        public PagedResources<LoanListItem> GetLoans(string id, int pageSize = 20, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return _loansService.Search(new LoanSearchCriteria
            {
                CompanyMentionId = id,
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            });
        }

        [HttpGet]
        [Route("{id}/locations")]
        public PagedResources<CompanyLocation> GetLocations(string id, int pageSize = 20, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            var pagingData = new PagingData
            {
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            };
            return _companiesService.GetCompanyLocations(id, pagingData, searchText);
        }

        [HttpGet]
        [Route("{id}/deals")]
        public PagedResources<DealListItem> GetDeals(string id, int pageSize = 20, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return _dealsService.Search(new DealSearchCriteria
            {
                CompanyMentionId = id,
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            });
        }

        [HttpGet]
        [Route("{id}/contacts")]
        public PagedResources<ContactListItem> GetContacts(string id, int pageSize = 10, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return _contactsService.Search(new ContactSearchCriteria
            {
                CompanyMentionId = id,
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            });
        }

        [HttpGet]
        [Route("intellisense")]
        public IEnumerable<CompanyIntellisense> Intellisense(string query, int count = 10)
        {
            return _companiesService.CompanyIntellisense(query, count);
        }

        [HttpPost]
        [Route("{id}/locations")]
        public int AddLocation(string id, CompanyLocationAdd location)
        {
            return _companiesService.AddCompanyLocation(id, location);
        }

        [HttpPost]
        [Route("{id}/contacts")]
        public int AddContact(string id, CompanyContactAdd contact)
        {
            return _companiesService.AddCompanyContact(id, contact);
        }

        [HttpPost]
        [Route("{id}/loans")]
        public CompanyLoanAdd AddLoan(string id, CompanyLoanAdd loan)
        {
            return _companiesService.AddCompanyLoan(id, loan);
        }
    }
}

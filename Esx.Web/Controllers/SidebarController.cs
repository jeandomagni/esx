﻿using Esx.Business.sidebar;
using Esx.Web.ViewModels;
using System.Web.Http;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/sidebar")]
    public class SidebarController : ApiController
    {
        private readonly ISidebarService _sidebarService;
        public SidebarController(ISidebarService sidebarService)
        {
            _sidebarService = sidebarService;
        }
        [HttpGet]
        [Route("username")]
        public UserViewModel GetUseFullName()
        {
            var userDetails = _sidebarService.GetCurrentUserDetails();
            return new UserViewModel { FullName = userDetails.FullName, IsPrincipal = userDetails.IsPrincipal,
                PrincipalUserFullName = userDetails.PrincipalUserFullName };
        }
    }
}

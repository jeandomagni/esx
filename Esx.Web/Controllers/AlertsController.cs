﻿using System.Web.Http;
using Esx.Business.Alerts;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Alert;
using Esx.Contracts.Paging;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/alerts")]
    public class AlertsController : ApiController
    {
        private readonly IAlertsService _alertsService;

        public AlertsController(IAlertsService alertsService)
        {
            _alertsService = alertsService;
        }

        [HttpGet]
        [Route("{id}/summaryAlerts")]
        public PagedResources<Alert> GetAlerts(string id, int pageSize = 30, int offset = 0)
        {
            var pagingData = new PagingData
            {
                PageSize = pageSize,
                Offset = offset
            };
            return _alertsService.GetAlerts(id, pagingData);
        }

        [HttpPut]
        [Route("acknowledgeAlert")]
        public void AcknowledgeAlert(GenericModel<int> alertId)
        {
            _alertsService.AcknowledgeAlert(alertId.Data);
        }

        [HttpPut]
        [Route("removeAlert")]
        public void RemoveAlert(GenericModel<int> alertId)
        {
            _alertsService.RemoveAlert(alertId.Data);
        }

        [HttpPut]
        [Route("snoozeAlert")]
        public void SnoozeAlert(GenericModel<int> alertId)
        {
            _alertsService.SnoozeAlert(alertId.Data);
        }
    }
}

﻿using Esx.Business.Contacts;
using Esx.Contracts.Paging;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using Esx.Contracts.Models.Contact;
using Esx.Contracts.Models.Search;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/contacts")]
    public class ContactsController : ApiController
    {
        private readonly IContactsService _contactsService;

        public ContactsController(IContactsService contactsService)
        {
            _contactsService = contactsService;
        }

        [HttpGet]
        [Route("list")]
        public PagedResources<ContactListItem> List(int pageSize = 20, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return _contactsService.Search(new ContactSearchCriteria
            {
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            });
        }

        //TODO change this to use mentionID
        [HttpGet]
        [Route("{id:int}")]
        public ContactDetails Get(int id)
        {
            return _contactsService.GetContactById(id);
        }

        [HttpGet]
        [Route("{contactId:int}/summarystats")]
        public ContactDetails GetContactSummary(int contactId, int pitches, int bidActivity, int holdPeriod, int loansMaturing)
        {
            return _contactsService.GetContactSummary(contactId, pitches, bidActivity, holdPeriod, loansMaturing);
        }

        [HttpGet]
        [Route("search")]
        public PagedResources<ContactListItem> Search(string searchText, int pageSize = 10, int offset = 0, string companyMentionId = null)
        {
            return _contactsService.Search(new ContactSearchCriteria
            {
                SearchText = searchText,
                CompanyMentionId = companyMentionId
            });
        }
    }
}

﻿using System.Collections.Generic;
using System.Web.Http;
using Esx.Business.Resources;
using Esx.Business.Users;
using Esx.Contracts.Models;
using Esx.Contracts.Models.User;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/resources")]
    public class ResourcesController : ApiController
    {
        private readonly IResourcesService _resourcesService;
        private readonly IUsersService _usersService;

        public ResourcesController(IResourcesService resourcesService, IUsersService usersService)
        {
            _resourcesService = resourcesService;
            _usersService = usersService;
        }

        [HttpGet]
        [Route("stateProvinces")]
        public IEnumerable<StateProvinceListItem> GetStateProvinceList()
        {
            return _resourcesService.GetStateProvinceList();
        }

        [HttpGet]
        [Route("countries")]
        public IEnumerable<CountryListItem> GetCountryList()
        {
            return _resourcesService.GetCountryList();
        }

        [HttpGet]
        [Route("userPreferences")]
        public IEnumerable<UserPreferenceListItem> GetUserPreferences()
        {
            return _usersService.GetUserPreferences();
        }
    }
}

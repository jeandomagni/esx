﻿using System.Web.Http;
using Esx.Business.Users;
using Esx.Contracts.Models.User;
using Esx.Contracts.Paging;
using Esx.Contracts.Models;
using System.Collections.Generic;
using Esx.Web.ViewModels;
using System.Net.Http;
using Esx.Web.Authentication;
using System.Net;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        private readonly IUsersService _usersService;
        private readonly IFormsAuthenticationService _formsAuthenticationService;
        public UsersController(IUsersService usersService, IFormsAuthenticationService formsAuthenticationService)
        {
            _usersService = usersService;
            _formsAuthenticationService = formsAuthenticationService;
        }

        [HttpGet]
        [Route("list")]
        public PagedResources<UserListItem> List(int pageSize = 5, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            var pagingData = new PagingData
            {
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            };

            return _usersService.GetUsersList(pagingData, searchText);
        }
        [HttpPut]
        [Route("setIsDelegated")]
        public void SetDelegatedUser(IsDelegatedUserViewModel isDelegateUserViewModel)
        {
            _usersService.SetDelegatedUser(isDelegateUserViewModel.UserKey, isDelegateUserViewModel.IsDelegated);
        }
        [HttpGet]
        [Route("delegatedUsers")]
        public IEnumerable<UserListItem> DelegateUserList()
        {
            return _usersService.GetDelegatedUserList();
        }
        [HttpPost]
        [Route("impersonateUser")]
        public HttpResponseMessage ImpersonateUser(ImpersonateUserViewModel user)
        {
            _formsAuthenticationService.SignIn(user.UserName, true);

            var response = Request.CreateResponse(HttpStatusCode.Created, true);
            return response;
        }
        [HttpPost]
        [Route("clearImpersonation")]
        public HttpResponseMessage ClearImpersonation()
        {
            _formsAuthenticationService.ClearImpersonation();

            var response = Request.CreateResponse(HttpStatusCode.Created, true);
            return response;
        }
    }
}

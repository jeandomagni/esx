﻿using System.Linq;
using System.Web.Mvc;
using Esx.Business.Companies;
using Esx.Business.Contacts;
using Esx.Contracts.Paging;
using Esx.Web.Common;

namespace Esx.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICompaniesService _companiesService;
        public HomeController(ICompaniesService companiesService)
        {
            _companiesService = companiesService;
        }

        // GET: Home
        [SecurityContextMvcFilter]
        public ActionResult Index()
        {
            return View();
        }
       
    }
}

﻿using Esx.Business.UserImpersonation;
using Esx.Web.Authentication;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/userImpersonation")]
    public class UserImpersonationController : ApiController
    {
        private readonly IUserImpersonationService _userImpersonationService;
        private readonly IFormsAuthenticationService _formsAuthenticationService;
        public UserImpersonationController(IUserImpersonationService userImpersonationService, 
            IFormsAuthenticationService formsAuthenticationService)
        {
            _userImpersonationService = userImpersonationService;
            _formsAuthenticationService = formsAuthenticationService;
        }
        [HttpPost]
        [Route("impersonateUser")]
        public HttpResponseMessage ImpersonateUser(ImpersonatedUser user)
        {
            _formsAuthenticationService.SignIn(user.UserName, true);

            var response = Request.CreateResponse(HttpStatusCode.Created, true);
            return response;
        }

        [HttpGet]
        [Route("userList")]
        public IEnumerable<string> Get()
        {
            return _userImpersonationService.GetUserList();
        }
        public class ImpersonatedUser
        {
            public string UserName { get; set; }
            public int Id { get; set; }
        }
    }
}

﻿using System.Collections.Generic;
using System.Web.Http;
using Esx.Business.Tags;
using Esx.Contracts.Models.Tag;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/tags")]
    public class TagsController : ApiController
    {
        private readonly ITagsService _tagsService;

        public TagsController(ITagsService tagsService)
        {
            _tagsService = tagsService;
        }

        [HttpGet]
        [Route("list")]
        public List<Tag> List(string query, int pageSize = 10)
        {
            return _tagsService.GetTags(query, pageSize);
        }

        [HttpGet]
        [Route("notes")]
        public List<TagNote> GetTagNotes(string tagName, int pageSize = 10, int offset = 0)
        {
            return _tagsService.GetTagNotes(string.Format("#{0}", tagName), pageSize, offset);
        }
    }
}

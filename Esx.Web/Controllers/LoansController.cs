﻿using Esx.Business.Loans;
using Esx.Contracts.Paging;
using System.Web.Http;
using Esx.Contracts.Models.Search;
using Esx.Web.ViewModels;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api/loans")]
    public class LoansController : ApiController
    {
        private readonly ILoansService _loansService;

        public LoansController(ILoansService loansService)
        {
            _loansService = loansService;
        }


        [HttpGet]
        public IHttpActionResult List(string id, int pageSize = 10, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return Ok(new PagedResources<LoanViewModel>());
        }

        [HttpGet]
        [Route("list")]
        public PagedResources<Contracts.Models.Loan.LoanListItem> List(int pageSize = 20, int offset = 0, string sort = null, bool sortDescending = false, string searchText = null)
        {
            return _loansService.Search(new LoanSearchCriteria
            {
                SearchText = searchText,
                PageSize = pageSize,
                Offset = offset,
                Sort = sort,
                SortDescending = sortDescending
            });
        }
    }
}

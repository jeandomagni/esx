﻿using System.Collections.Generic;
using System.Web.Http;
using Esx.Business.Filter;
using Esx.Business.Search;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;

namespace Esx.Web.Controllers
{
    [RoutePrefix("api")]
    public class SearchController : ApiController
    {
        private readonly ISearchService _searchService;
        private readonly IFilterService _filterService;

        public SearchController(ISearchService searchService, IFilterService filterService)
        {
            _searchService = searchService;
            _filterService = filterService;
        }

        [HttpPost]
        [Route("search")]
        public PagedResources<object> Search(SearchCriteria searchCriteria)
        {
            return _searchService.Search(searchCriteria);
        }

        [HttpGet]
        [Route("filters")]
        public List<FilterOption> GetFilters(string coreObjectType)
        {
            return _filterService.GetFilterOptions(coreObjectType);
        }

        [HttpPost]
        [Route("search/getmapitems")]
        public IEnumerable<MapItem> GetMapItems(SearchCriteria searchCriteria)
        {
           return _searchService.MapItems(searchCriteria);
        }
    }
}
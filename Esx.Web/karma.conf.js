/// <reference path="C:\dev\EsxWeb\Esx.Web\Scripts/lib/angular-material-icons/angular-material-icons.js" />
/// <reference path="C:\dev\EsxWeb\Esx.Web\Scripts/lib/angular-material-icons/angular-material-icons.js" />
/// <reference path="C:\dev\esx\Esx.Web\Scripts/lib/angular-simple-logger/angular-simple-logger.js" />
/// <reference path="C:\dev\esx\Esx.Web\Scripts/lib/angular-simple-logger/angular-simple-logger.js" />
/// <reference path="C:\dev\esx\Esx.Web\Scripts/bower_components/angular-resource/angular-resource.js" />
// Karma configuration
// Generated on Tue Aug 25 2015 16:13:43 GMT-0500 (Central Daylight Time)

module.exports = function(config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: [
            'Scripts/lib/jquery/jquery.js',
            'Scripts/lib/angular/angular.js',
            'Scripts/lib/angular-animate/angular-animate.js',
            'Scripts/lib/angular-messages/angular-messages.js',
            'Scripts/lib/angular-resource/angular-resource.js',
            'Scripts/lib/angular-ui-router/angular-ui-router.js',
            'Scripts/lib/angular-sanitize/angular-sanitize.js',
            'Scripts/lib/angular-aria/angular-aria.js',
            'Scripts/lib/angular-material/angular-material.js',
            'Scripts/lib/angular-material-icons/angular-material-icons.js',
            'Scripts/lib/angular-mocks/angular-mocks.js',
            'Scripts/lib/lodash/lodash.js',
            'Scripts/lib/angular-simple-logger/angular-simple-logger.js',
            'Scripts/lib/angular-google-maps/angular-google-maps.js',
            'Scripts/lib/rangy/rangy-core.js',
            'Scripts/lib/rangy/rangy-textrange.js',
            'Scripts/toastr.js',
            'Scripts/paging.js',
            'Scripts/ng-infinite-scroll.js',
            'Scripts/lib/ment.io/dist/mentio.js',
            'Scripts/lib/ng-file-upload/ng-file-upload.js',
            'app/test/*.js',
            'app/directives/*.html',
            'app/*.js',
            'app/**/*.js'
        ],

        // list of files to exclude
        exclude: [
            'app/integrationTests/*.js'
        ],

        pugins: [
            'karma-ng-html2js-preprocessor'
        ],

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            "app/directives/*.html": ["ng-html2js"]
        },

        ngHtml2JsPreprocessor: {
            // include beforeEach(module('templates')) in unit tests
            moduleName: 'templates'
        },


        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress'],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        //browsers: ['PhantomJS'],
        browsers: ['Chrome'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false
    });
}

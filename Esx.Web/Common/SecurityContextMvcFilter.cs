﻿using System.Web.Mvc;
using Esx.Contracts.Security;
using Microsoft.Practices.ServiceLocation;
using Esx.Web.Authentication;

namespace Esx.Web.Common
{
    public class SecurityContextMvcFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var principal = filterContext.RequestContext.HttpContext.User;

            var authService = ServiceLocator.Current.GetInstance<IFormsAuthenticationService>();

            authService.SignIn(principal.Identity.Name);


            base.OnActionExecuting(filterContext);
        }
    }
}
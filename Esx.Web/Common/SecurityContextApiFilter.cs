﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Esx.Contracts.Security;
using Microsoft.Practices.ServiceLocation;
using System.Web.Security;
using System.Net.Http;
using Esx.Web.Authentication;

namespace Esx.Web.Common
{
    public class SecurityContextApiFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var principal = actionContext.RequestContext.Principal;

            var authService = ServiceLocator.Current.GetInstance<IFormsAuthenticationService>();

            authService.SignIn(principal.Identity.Name);

            base.OnActionExecuting(actionContext);
        }
    }

}
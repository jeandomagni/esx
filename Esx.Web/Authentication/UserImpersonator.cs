﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Esx.Web.Authentication
{
    public interface IUserImpersonator
    {
        void ImpersonateUser(string userName, string[] roles);
    }
    public class UserImpersonator : IUserImpersonator
    {
        public void ImpersonateUser(string userName, string[] roles)
        {

        }
    }
}
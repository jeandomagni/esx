using Esx.Business.Security;
using Esx.Contracts.Security;
using System;
using System.Web;
using System.Web.Security;
using System.Security;
using System.Collections.Generic;
using Esx.Contracts.Models.User;
using Esx.Contracts.Extensions;
using System.Text;
using Esx.Contracts.Enums;

namespace Esx.Web.Authentication
{
    public interface IFormsAuthenticationService
    {
        void SignIn(string userName);
        void SignIn(string userName, bool impersonatingUser);
        void ClearImpersonation();

    }
    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        private ISecurityContext _securityContext;
        private IUserRoleService _userRoleService;
        public FormsAuthenticationService(ISecurityContext securityContext, IUserRoleService userRoleService)
        {
            _securityContext = securityContext;
            _userRoleService = userRoleService;
        }
        public void SignIn(string userName)
        {
            SignIn(userName, false);
        }
        public void SignIn(string userName, bool impersonatingUser)
        {
            if(impersonatingUser == false)
            {
                var authTicket = GetUserAuthTicket();
                if (authTicket != null)
                {
                    _securityContext.Init(authTicket.Name, authTicket.UserData, userName);
                    _userRoleService.Init(authTicket.UserData);
                    return;
                }
            }

            var user = ValidateUser(userName, impersonatingUser);
            var adUserInfo = _userRoleService.GetUser(userName);
            if (user != null)
            {
                var userData = CreateUserData(user, adUserInfo.Roles);
                var usernameData = $"{userName}{UserDataDelimiter.Delimiter}{user.UserKey}";
                SetAuthCookie(usernameData, userData);
                return;
            }
            if (impersonatingUser == true) throw new SecurityException("Impersonated User Not Found!");

            if (_userRoleService.HasAccess(Feature.Application.Code, userName))
            {
                var userSuccessfullyAdded = _securityContext.AddNewUser(userName, adUserInfo.FullName);
                if(userSuccessfullyAdded == false) throw new SecurityException("Unable to add new user!");
                SignIn(userName);
                return;
            }
        }

        private void SetAuthCookie(string userName, string userData)
        {

            var cookieTicket = new FormsAuthenticationTicket(
              1,                                   
              userName,                             
              DateTime.Now,                         
              DateTime.Now.AddDays(1d),          
              false,
              userData,                            
              FormsAuthentication.FormsCookiePath);  

            var encryptedCookieTicket = FormsAuthentication.Encrypt(cookieTicket);

            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedCookieTicket);
            cookie.HttpOnly = true;

            HttpContext.Current.Response.Cookies.Add(cookie);
        }
        private User ValidateUser(string userName, bool impersonatingUser) {

            return _securityContext.PrepareContext(userName, impersonatingUser);
           
        }
        private FormsAuthenticationTicket GetUserAuthTicket()
        {
            var cookie = HttpContext.Current.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
            if (cookie != null && cookie.Value != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

                if (ticket != null && ticket.Expired == false && String.IsNullOrEmpty(ticket.Name) == false)
                {
                    return ticket;
                }
            }
            return null;
        }
        private string CreateUserData(User user, IEnumerable<string> roles){

            var roleList = String.Join(UserDataDelimiter.RoleDelimiter.ToString(), roles);
            var userData = $"{user.AccessToken.ToBString()}{UserDataDelimiter.Delimiter}{roleList}";

            return userData;
        }
        public void ClearImpersonation(){
            ForceExpire();
        }
        private void ForceExpire()
        {
            var cookie = HttpContext.Current.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
            if (cookie != null && cookie.Value != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-2d);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }
    }
}
﻿(function () {
    'use strict';

    var serviceId = 'loansService';

    angular.module('app').factory(serviceId, loansService);

    loansService.$inject = ['uriService', '$resource', 'wizardService', 'dealsService', 'dealSummaryService', '_'];

    function loansService(uriService, $resource, wizardService, dealsService, dealSummaryService, _) {

        var resource = $resource(
            uriService.loan
        );

        var service = {
            getLoans: getLoans,
            getWizardForm: getWizardForm
        };

        return service;

        function getLoans(query) {
            return $resource(uriService.loansList).get(query).$promise;
        }
        function getWizardForm(wizardCompleteCallback) {

            var form = wizardService.createForm('app/loans/addLoan/templates/', 'loanUserSelections.html', wizardCompleteCallback);;

            form.addStep({ label: 'Loan Basics' })
                .addQuestion({
                    templateFile: 'basics.deal.html',
                    continueAllowedFn: function (result) { return result.deal; },
                    isImmediateContinue: true
                })
                // Get the deal summary
                .addProcess(function (process) {
                    process.addDefaults({
                        dealAllProperties: true,
                        propertyUsages: {}
                    });
                    return dealSummaryService.getSummary(form.result.deal.dealMentionId).then(function (response) {
                        form.meta.dealSummary = response;
                    });
                })
                 .addQuestion({
                     templateFile: 'basics.dealmustclose.html',
                     showFn: function (result, meta) { return true; }
                 })
               .addQuestion({
                   templateFile: 'basics.properties.html',
                   showFn: function (result, meta) { return meta.dealSummary && meta.dealSummary.properties.length > 1; }
               })
               .addQuestion({
                   templateFile: 'basics.seniority.html'
               })
                .addQuestion({
                   templateFile: 'basics.borrowers.html'
               })
               .addQuestion({
                   templateFile: 'basics.lenders.html'
               })
               .addQuestion({
                   templateFile: 'basics.loanRetired.html'
               });

            form.addStep({ label: 'Loan Details' })
              .addQuestion({
                  templateFile: 'loanDetails.cluster1.html'
              })
              .addQuestion({
                  templateFile: 'loanDetails.cluster2.html'
              })
                .addQuestion({
                    templateFile: 'loanDetails.cluster3.html'
                })
                .addQuestion({
                    templateFile: 'loanDetails.cluster4.html',
                    isWizardExit: true
              });

            return form;
        }
    }
})();
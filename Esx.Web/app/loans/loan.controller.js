﻿(function () {
    'use strict';
    var controllerId = 'loanController';

    angular.module('app').controller(controllerId, loanController);

    loanController.$inject = ['common', 'loansService'];

    function loanController(common, loansService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;

        activate();

        function activate() {
            common.activateController([], controllerId).then(function () { log('Retrieved loan details.'); });
        }
      
    }
})();
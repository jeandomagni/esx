﻿describe('loans.service', function () {

    beforeEach(module('app'));

    var loansService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_loansService_, _uriService_, _$httpBackend_) {

        loansService = _loansService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        uriService.loan = 'api/loans/:loanId';

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getLoans', function () {
        it('should call server with loans list uri', function () {

            //Arrange
            $httpBackend.expectGET('api/loans/list', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            loansService.getLoans();

            //Assert
            $httpBackend.flush();

        });
       
        it('should make GET request with query', function () {

            //Arrange
            var query = {
                offset: 100,
                pageSize: 10
            }

            var expectedUri = uriService.loansList + '?' + $.param(query);

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            loansService.getLoans(query);

            //Assert
            $httpBackend.flush();

        });
    });
});
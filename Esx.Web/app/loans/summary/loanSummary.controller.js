﻿(function () {
    'use strict';

    var controllerId = 'loanSummaryController';

    angular.module('app').controller(controllerId, loanSummaryController);

    loanSummaryController.$inject = ['common'];

    function loanSummaryController(common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () {  });
        }
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'loansController';

    angular
      .module('app')
        .controller(controllerId, loansController);

    loansController.$inject = ['common', 'loansService', 'companiesService', 'mentionsService', '_', '$state', '$timeout'];

    function loansController(common, loansService, companiesService, mentionsService, _, $state, $timeout) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.searchText = '';

        vm.getLoans = _.debounce(getLoans, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.searchLoans = _.debounce(searchLoans, 200);
        vm.selectRow = selectRow;

        vm.queryObject = {
            offset: 0,
            pageSize: 20
        };

        vm.loansList = {
            resources: [],
             pagingData: {
                pageSize: 30,
                offset: 0
            },
        }

        function activate() {
            common.activateController([getLoans(false)], controllerId)
                .then(function () { log('Activated Loans.'); });
        }

        function getLoans(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            return loansService.getLoans(vm.queryObject).then(function (loansList) {
                vm.loansList.resources = vm.loansList.resources.concat(loansList.resources);
                return vm.loansList;
            });
        }

        function searchLoans() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.loansList.resources.length = 0;
                vm.queryObject.offset = 0;
                getLoans(true);
            }
        }

        function sortColumn(columnName) {
            vm.loansList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            getLoans(false);
        }

        function selectRow(item, event) {
            $timeout(function () {
                if (event.target.id === 'esx-watch') {
                    toggleIsWatched(item);
                } else {
                    editLoan(item);
                }
            });
        }

        function editLoan(loan) {
            $state.go('loan.loanSummary', { 'loanMentionId': loan.loanMentionId });
        }

        function toggleIsWatched(loan) {
            loan.isWatched = !loan.isWatched;
            mentionsService.setIsWatched(loan.loanMentionId, loan.isWatched);
        }

        activate();
    }
})();
﻿(function () {
    describe('loans.controller', function () {

        beforeEach(module('app'));

        var $controller,
            common,
            loansService,
            mentionsService, 
            fakeLoans,
            $scope,
            controller,
            $timeout,
            $state,
            _;

        var activateController = function (context) {
            controller = $controller('loansController', {
                common: common,
                loansService: loansService,
                mentionsService: mentionsService,
                '_': _,
                $timeout: $timeout,
                $state: $state
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _loansService_, _mentionsService_, _$timeout_, _$state_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            loansService = _loansService_;
            mentionsService = _mentionsService_;
            _ = window._; // lodash
            $timeout = _$timeout_;
            $state = _$state_;

            //fakes
            $scope = {
                $apply: angular.noop
            };

            fakeLoans = {
                resources: [
                    { fullName: 'John Landers', title: 'Deal Coordinator', company: 'Colony American Homes' },
                    { fullName: 'Bob Dobbs', title: 'Managing Director', company: 'Church of the SubGenius' }
                ],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn(loansService, 'getLoans').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeLoans);
                    }
                };
            });

            spyOn(mentionsService, 'setIsWatched');

        }));

        describe('activate', function () {
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define the query object. ', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.loansList.pagingData).toEqual(expectedPagingData);
            });
        });

        describe('getLoans', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get loans with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    offset: 0,
                    pageSize: 30
                };

                controller.queryObject = expectedSearchQuery;
                controller.loansList.resources.length = 0; // clear loans that "activate" added.

                //act
                controller.getLoans();

                //assert
                expect(loansService.getLoans).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.loansList).toEqual(fakeLoans);
            });

        });

        describe('sort', function () {
            beforeEach(function () {
                activateController();
            });

            it('should sort loans list', function () {
                var columnName = 'fullName';

                //arrange
                var queryObject = { filters: undefined, searchText: '' };

                controller.queryObject = queryObject;

                //act
                controller.sortColumn(columnName);

                //assert
                expect(loansService.getLoans).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.queryObject.sort).toEqual(columnName);
                expect(controller.queryObject.sortDescending).toBe(false);
                expect(controller.loansList).toEqual(fakeLoans);
            });

            it('should clear current loans and offset', function () {

                //arrange
                var columnName = 'fullName';
                loansService.getLoans.and.callFake(function () {
                    return {
                        then: angular.noop
                    };
                });

                controller.loansList.resources = ['one', 'two'];
                controller.queryObject.offset = 79;

                //act
                controller.sortColumn(columnName);

                //assert
                expect(controller.queryObject.offset).toBe(0);
                expect(controller.loansList.resources.length).toBe(0);

            });
        });

        describe('searchLoans', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get filtered loans with the query object', function () {

                //arrange
                controller.searchText = 'text!';
                var expectedSearchQuery = {
                    searchText: 'text!',
                    offset: 0
                };

                controller.queryObject = {};
                controller.loansList.resources.length = 0;

                //act
                controller.searchLoans();

                //assert
                expect(loansService.getLoans).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.loansList).toEqual(fakeLoans);
            });

            it('should not get loans with the query object when searchText < 3', function () {

                //arrange
                loansService.getLoans.calls.reset(); //reset the spy
                controller.searchText = 'aa';
                controller.loansList.resources.length = 0;

                //act
                controller.searchLoans();

                //assert
                expect(loansService.getLoans).not.toHaveBeenCalled();
                expect(controller.loansList.resources.length).toBe(0);
            });

            it('should clear current loans and offset', function() {

                //arrange
                loansService.getLoans.and.callFake(function () {
                    return {
                        then: angular.noop
                    };
                });

                controller.loansList.resources = ['one', 'two'];
                controller.queryObject.offset = 79;
                controller.searchText = 'text!';

                //act
                controller.searchLoans();

                //assert
                expect(controller.queryObject.offset).toBe(0);
                expect(controller.loansList.resources.length).toBe(0);

            });

        });

        describe('selectRow', function () {

            var event;

            beforeEach(function () {

                //Flatten timeout
                $timeout = function(callback) {
                    return callback();
                };

                event = {
                    target: {
                        id: ''
                    }
                };

                activateController();
            });

            it('should send false to the mentions service isWatched is true and event.target.id is esx-watch', function () {
                // Arrange
                var loan = {
                    isWatched: true
                };
                event.target.id = 'esx-watch';


                // Act
                controller.selectRow(loan, event);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(loan.loanMentionId, false);
            });

            it('should send true to the mentions service when isWatched is false and event.target.id is esx-watch', function () {
                // Arrange
                var loan = {
                    isWatched: false
                };
                event.target.id = 'esx-watch';

                // Act
                controller.selectRow(loan, event);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(loan.loanMentionId, true);
            });

            it('should change the state to loan.loanSummary', function() {
                //arrange
                spyOn($state, 'go');

                var loan = {
                    loanMentionId: "@me"
                };

                // Act
                controller.selectRow(loan, event);

                //assert
                expect($state.go).toHaveBeenCalledWith('loan.loanSummary', { 'loanMentionId': loan.loanMentionId });

            });
        });

    });
})();



﻿(function () {
    'use strict';

    var controllerId = 'loanDetailsClusterTwoController';

    angular.module('app').controller(controllerId, loanDetailsClusterTwoController);

    loanDetailsClusterTwoController.$inject = [];

    function loanDetailsClusterTwoController() {
        var vm = this;
        vm.AllExtendedMaturityDate = '';
        vm.setInitialMaturityDate = function () {
            vm.initialMaturityDate2 = new Date(
                vm.originationDate.getFullYear(),
                vm.originationDate.getMonth() + vm.initialMaturityDate,
                vm.originationDate.getDate()
                );
        }

        vm.setExtendedMaturityDate = function () {
            vm.extendedMaturityDate2 = new Date(
                vm.originationDate.getFullYear(),
                vm.originationDate.getMonth() + vm.extendedMaturityDate,
                vm.originationDate.getDate()
                );
        }
        vm.addMaturityDate = function () {
            if (vm.AllExtendedMaturityDate.length > 0) vm.AllExtendedMaturityDate += " , ";

            vm.AllExtendedMaturityDate += new Date(vm.extendedMaturityDate2).toLocaleDateString();

            vm.extendedMaturityDate = '';
            vm.extendedMaturityDate2 = null;
        }

        vm.init = function (form) {
            vm.form = form;
        };
        
        
    }
})();
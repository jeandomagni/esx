﻿(function () {
    'use strict';

    var controllerId = 'basicsSeniorityController';

    angular.module('app').controller(controllerId, basicsSeniorityController);

    basicsSeniorityController.$inject = [];

    function basicsSeniorityController() {
        var vm = this;

        vm.dragControlListeners = {
            //  accept: function (sourceItemHandleScope, destSortableScope) {return boolean}//override to determine drag is allowed or not. default is true.
            ////  itemMoved: function (event) {//Do what you want},
            //  orderChanged: function(event) {//Do what you want},
            //          containment: '#board'//optional param.
            //          clone: true //optional param for clone feature.
            //          allowDuplicates: false //optional param allows duplicates to be dropped.
        };

        vm.dragControlListeners1 = {
            //containment: '#board'//optional param.
            //allowDuplicates: true //optional param allows duplicates to be dropped.
        };

        vm.init = function (form) {
            vm.form = form;
        };
        
        vm.activeLoans = [
             { loanId: 1, loanName: 'This loan', orderNumber:1},
            { loanId: 1, loanName: 'loan 101 for abc', orderNumber:2, loanAmount: '450000000', maturityDate: '05/12/2018' },
            { loanId: 2, loanName: 'loan 103 for tyz',orderNumber:3, loanAmount: '10000000000', maturityDate: '01/09/2017' }
        ];
        vm.addMissingLoan = addMissingLoan;
        function addMissingLoan() {

            var loanScenario = vm.missingLoanScenario ? vm.missingLoanScenario : vm.missingLoanCustomScenario;
            vm.activeLoans.push({ loanName: loanScenario, loanAmount: vm.missingLoanAmount, orderNumber: vm.activeLoans.length + 1 });
        }
    }
})();
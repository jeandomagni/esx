﻿(function () {
    'use strict';

    var controllerId = 'basicsLendersController';

    angular.module('app').controller(controllerId, basicsLendersController);

    basicsLendersController.$inject = [];

    function basicsLendersController() {
        var vm = this;

        vm.init = function (form) {
            vm.form = form;
        };
        
        vm.lenders = [
            { companyId: 1, companyName: 'ULLIC' },
            { companyId: 2, companyName: 'Stewart DeLuca' },
            { companyId: 3, companyName: 'AMT Investments' }
        ];
    }
})();
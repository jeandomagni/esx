﻿(function () {
    'use strict';

    var controllerId = 'loanDetailsClusterOneController';

    angular.module('app').controller(controllerId, loanDetailsClusterOneController);

    loanDetailsClusterOneController.$inject = [];

    function loanDetailsClusterOneController() {
        var vm = this;

        vm.init = function (form) {
            vm.form = form;
        };
        
        vm.borrowers = [
            { companyId: 1, companyName: 'Pacific Allied Asset Management' },
            { companyId: 2, companyName: 'Standard Chartered Bank' },
            { companyId: 3, companyName: 'Munchener Hypothekenbank eG' },
            { companyId: 4, companyName: 'Millennium Group' },
            { companyId: 5, companyName: 'Zugimpex International GmbH' }
        ];
    }
})();
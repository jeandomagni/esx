﻿(function () {
    'use strict';

    var controllerId = 'basicsPropertiesController';

    angular.module('app').controller(controllerId, basicsPropertiesController);

    basicsPropertiesController.$inject = [];

    function basicsPropertiesController() {
        var vm = this;

        vm.init = function (form) {
            vm.form = form;
            vm.form.meta.basicsPropertiesIncludedTouched = true;

            if (angular.isUndefined(vm.form.result.loanProperties)) {
                onAllPropertiesChange();
            }

        };

        vm.onAllPropertiesChange = onAllPropertiesChange;
        
        function onAllPropertiesChange() {
            vm.form.result.loanProperties = vm.form.result.dealAllProperties
                ? vm.form.meta.dealSummary.properties
                : [];
        }
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'basicsLoanRetiredController';

    angular.module('app').controller(controllerId, basicsLoanRetiredController);

    basicsLoanRetiredController.$inject = [];

    function basicsLoanRetiredController() {
        var vm = this;

        vm.deal = { hasAssociatedLoans: 'true' };

        vm.activeLoans = [
           { loanId: 0, loanName: 'loan 108 for abc', orderNumber: 1, loanAmount: '2400000000', maturityDate: '05/12/2018' },
          { loanId: 1, loanName: 'loan 101 for abc', orderNumber: 2, loanAmount: '2000000000', maturityDate: '05/12/2018' },
          { loanId: 2, loanName: 'loan 103 for tyz', orderNumber: 3, loanAmount: '32000000', maturityDate: '01/09/2017' }
        ];

        vm.init = function (form) {
            vm.form = form;
        };

        function getLoans(isSearching) {
          
        }

        function searchLoans() {
           
        }
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'loanUserSelectionsController';

    angular.module('app').controller(controllerId, loanUserSelectionsController);

    loanUserSelectionsController.$inject = [];

    function loanUserSelectionsController() {
        var vm = this;
        vm.isEmptyObject = isEmptyObject;

        vm.init = function (form) {
            vm.form = form;
        }

        function isEmptyObject(obj) {
            for (var prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    return false;
                }
            }
            return true;
        }
    }
})();
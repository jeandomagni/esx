﻿(function () {
    'use strict';

    var controllerId = 'addLoanController';

    angular.module('app').controller(controllerId, addLoanController);

    addLoanController.$inject = ['common', 'loansService'];

    function addLoanController(common, loansService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.form = loansService.getWizardForm(wizardComplete);

        function wizardComplete(result) {
            console.log("Wizard Finished");
        }
    }
})();
﻿describe('loan.controller', function () {
    beforeEach(module('app'));

    var $controller,
        common,
        loansService,
        fakeLoan,
        $scope,
        controller;

    var activateController = function (loanMentionId) {
        common.$stateParams.loanId = loanMentionId;

        controller = $controller('loanController', {
            common: common,
            loansService: loansService
        });
    };

    beforeEach(inject(function (_$controller_, _common_, _loansService_) {

        //inject
        $controller = _$controller_;
        common = _common_;
        loansService = _loansService_;

        //fakes
        $scope = {};

        common.$window = { location: {} };

        fakeLoan = {
            loanMentionId: '@something|L',
            loanName: 'loanName'
        };

        //spys
       
    }));

    describe('activate existing loan', function () {

        beforeEach(function () {
            var loanMentionId = "@laon";
            activateController(loanMentionId);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

    });

});
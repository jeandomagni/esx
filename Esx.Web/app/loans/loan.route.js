﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider'];

    function routeConfigurator($stateProvider) {

        $stateProvider
            .state('loans', {
                url: '/loans',
                templateUrl: 'app/loans/loans.html',
                controller: 'loansController',
                controllerAs: 'vm',
                data: {
                    title: 'Loans',
                    defaultSearch: 'loan',
                    sidebaricon: 'loans',
                    includeInSideBar: true,
                    subnavClass: 'loans-bg'
                }
            }).state('loan', {
                abstract: true,
                url: '/loans/:loanMentionId',
                templateUrl: 'app/loans/loan.html',
                controller: 'loanController',
                controllerAs: 'vm',
                data: {
                    title: ''
                }
            }).state('loan.loanSummary', {
                url: '/summary',
                data: {
                    title: '',
                    sidebaricon: 'loans'
                },
                views: {
                    '': {
                        templateUrl: 'app/loans/summary/loanSummary.html',
                        controller: 'loanSummaryController',
                        controllerAs: 'vm'
                    }
                }
            }).state('addloan', {
                url: '/addloan',
                templateUrl: 'app/loans/addLoan/addLoan.html',
                controller: 'addLoanController',
                controllerAs: 'vm',
                data: {
                    title: 'Add a loan',
                    sidebaricon: 'loans',
                    includeInSideBar: true,
                    subnavClass: 'loans-bg'
                }
            });
    }
})();
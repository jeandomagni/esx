﻿(function () {
    'use strict';

    var serviceId = 'valuationService';

    angular.module('app').factory(serviceId, valuationService, _);

    valuationService.$inject = ['uriService', '$resource', '$http', 'lookups', 'wizardService', 'dealsService', 'dealSummaryService', '_'];

    function valuationService(uriService, $resource, $http, lookups, wizardService, dealsService, dealSummaryService, _) {

        var service = {
            getWizardForm: getWizardForm
        };

        return service;

        function getWizardForm(wizardCompleteCallback) {

            var form = wizardService.createForm('app/valuations/addValuation/templates/', 'valuationUserSelections.html', wizardCompleteCallback);

            var preBidDealStatuses = [
                lookups.DealStatus.Targeting.Code,
                lookups.DealStatus.Pitching.Code,
                lookups.DealStatus.PitchedAndLost.Code,
                //lookups.DealStatus.OnHold.Code, // A deal can be on hold anytime, not just pre-bid, so omit this.
                lookups.DealStatus.Packaging.Code,
                lookups.DealStatus.Marketing.Code
            ];

            form.addStep({ label: 'Valuation Basics' })
                .addQuestion({
                    templateFile: 'basics.deal.html',
                    continueAllowedFn: function (result) { return result.deal; },
                    isImmediateContinue: true
                })
                // Get the deal summary
                .addProcess(function (process) {
                    process.addDefaults({
                        dealAllProperties: true,
                        propertyUsages: {}
                    });
                    return dealSummaryService.getSummary(form.result.deal.dealMentionId).then(function (response) {
                        form.meta.dealSummary = response;
                        form.meta.isEastdilBroker = _.find(form.meta.dealSummary.brokers, function (b) { return b.isEastdil; }) != null;
                    });
                })
                .addQuestion({
                    templateFile: 'basics.economicSize.html',
                    showFn: function (result) { return result.deal && result.deal.dealType === lookups.DealType.InvestmentBanking.Code; },
                    continueAllowedFn: function (result) { return result.economicSize; }
                })
                .addQuestion({
                    templateFile: 'common.attachDocuments.html',
                    showFn: function (result) { return result.deal && result.deal.dealType === lookups.DealType.InvestmentBanking.Code; },
                    isWizardExit: true
                })
                .addQuestion({
                    templateFile: 'basics.propertiesIncluded.html',
                    showFn: function (result, meta) { return meta.dealSummary && meta.dealSummary.properties.length > 1; },
                    continueAllowedFn: function (result) { return result.valuationProperties && result.valuationProperties.length > 0; }
                })
                // Get the latest existing valuation for the same properties.
                .addProcess(function (processQuestion) {
                    var propertyKeys = _.map(form.result.valuationProperties, function (p) { return p.propertyKey; });
                    return dealsService.getLatestValuation(form.result.deal.dealID, propertyKeys).then(function (response) {
                        form.meta.latestValuation = response;

                        // Default these values to what was in the last valuation.
                        var defaults = {
                            economicSize: response.value,
                            riskProfile: response.dealValuationRiskProfile_Code,
                            goingInCapRate: response.goingInCapRate,
                            goingInPricePerUnit: response.goingInPricePerUnit,
                            irr: response.irr,
                            isLeveraged: response.isLeveraged,
                            interestBeingSold: response.interestBeingSold,
                            loanAmount: response.loanAmount,
                            loanUnpaidBalance: response.loanUnpaidBalance,
                            loanDuration: response.loanDuration,
                            allInInterestRate: response.allInInterestRate
                        };
                        if (response.dealValuationAssetUsageValues) {
                            for (var i = 0; i < response.dealValuationAssetUsageValues.length; i++) {
                                var usage = response.dealValuationAssetUsageValues[i];
                                defaults.propertyUsages[usage.usageType] = usage.value;
                            }
                        }
                        processQuestion.addDefaults(defaults);
                    });
                })
                .addQuestion({
                    templateFile: 'basics.economicSize.html',
                    showFn: function (result) { return result.deal && result.deal.dealType === lookups.DealType.NonTransactionalAdvisory.Code; },
                    continueAllowedFn: function (result) { return result.economicSize; }
                })
                .addQuestion({
                    templateFile: 'common.attachDocuments.html',
                    showFn: function (result) { return result.deal && result.deal.dealType === lookups.DealType.NonTransactionalAdvisory.Code; },
                    isWizardExit: true
                })
                .addQuestion({
                    templateFile: 'basics.loansInvolved.html',
                    showFn: function (result) { return result.deal && result.deal.dealType === lookups.DealType.LoanSale.Code; },
                    continueAllowedFn: function (result) { return result.valuationLoans && result.valuationLoans.length > 0; }
                })
                .addQuestion({
                    templateFile: 'basics.riskProfile.html',
                    continueAllowedFn: function (result) { return result.riskProfile; },
                    isImmediateContinue: true
                })
                .addQuestion({
                    templateFile: 'basics.dealPhysicalSize.html',
                    continueAllowedFn: function (result) { return hasAllValues(result.propertyUsages); }
                });

            form.addStep({ label: 'Valuation Scenario and Deal Status' })
                .addQuestion({
                    templateFile: 'scenario.scenarioType.html',
                    showFn: function (result, meta) { return meta.isEastdilBroker; },
                    continueAllowedFn: function (result) { return result.dealValuationType; },
                    isImmediateContinue: true,
                    data: { lookups: lookups }
                })
                .addQuestion({
                    templateFile: 'scenario.dealStatusForValuation.html',
                    showFn: function (result) { return result.dealValuationType !== lookups.DealValuationType.Bid.Code; },
                    continueAllowedFn: function (result) { return result.valuationDealStatus; },
                    isImmediateContinue: true
                })
                .addQuestion({
                    templateFile: 'scenario.dealStatusForBids.html',
                    showFn: function (result) { return result.dealValuationType === lookups.DealValuationType.Bid.Code && preBidDealStatuses.indexOf(result.deal.dealStatus) > -1; }
                });

            form.addStep({
                label: 'Bid Basics',
                isOptional: false,
                showFn: function (result) { return result.dealValuationType === lookups.DealValuationType.Bid.Code; }
            })
                .addQuestion({
                    templateFile: 'bidBasics.bidRound.html',
                    continueAllowedFn: function (result) { return result.bidRound; },
                    isImmediateContinue: true
                })
                .addQuestion({
                    templateFile: 'bidBasics.companiesSubmitting.html'
                })
                .addQuestion({
                    templateFile: 'bidBasics.attachBid.html'
                });

            form.addStep({ label: 'Valuation Details' })
                .addQuestion({
                    templateFile: 'valuationDetails.economicSize.html',
                    data: { lookups: lookups }
                })
                .addQuestion({
                    templateFile: 'common.attachDocuments.html',
                    isWizardExit: true
                });

            return form;
        }

        function hasAllValues(obj) {
            for (var prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    if (!obj[prop] || !obj[prop].value) return false;
                }
            }
            return true;
        }
    }
})();
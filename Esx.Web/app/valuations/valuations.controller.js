﻿(function () {
    'use strict';

    var controllerId = 'valuationsController';

    angular.module('app').controller(controllerId, valuationsController);

    valuationsController.$inject = ['common'];

    function valuationsController(common) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

    }
})();
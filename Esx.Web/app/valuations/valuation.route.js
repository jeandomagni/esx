﻿
(function () {
    'use strict';

    var app = angular.module('app');

    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider'];

    function routeConfigurator($stateProvider) {

        $stateProvider
             .state('addvaluation', {
                 url: '/addvaluation',
                 templateUrl: 'app/valuations/addValuation/addValuation.html',
                 controller: 'addValuationController',
                 controllerAs: 'vm',
                 data: {
                     title: 'Add a Valuation',
                     sidebaricon: 'valuations',
                     includeInSideBar: true,
                     subnavClass: 'valuations-bg'
                 }
             })
            .state('valuations', {
                url: '/valuations',
                templateUrl: 'app/valuations/valuations.html',
                controller: 'valuationsController',
                controllerAs: 'vm',
                data: {
                    title: 'Valuations',
                    defaultSearch: 'valuation',
                    sidebaricon: 'valuations',
                    includeInSideBar: true,
                    subnavClass: 'valuations-bg'
                }
            })

        ////// I have no idea what pages are going to be needed or if valuations are going to have mentions, 
        ////// so I grabbed this from deal.route.js and am just leaving it here for now.

        //.state('valuation', {
        //    abstract: true,
        //    url: '/valuations/:valuationMentionId',
        //    templateUrl: 'app/valuations/valuation.html',
        //    controller: 'valuationController',
        //    controllerAs: 'vm',
        //    data: {
        //        title: '',
        //        subnavClass: 'valuations-bg',
        //        subtabs: [
        //               { sref: 'valuation.valuationSummary', label: 'SUMMARY' }
        //        ]
        //    }
        //}).state('valuation.valuationSummary', {
        //    url: '/summary',
        //    data: {
        //        title: '',
        //        sidebaricon: 'valuations'
        //    },
        //    views: {
        //        '': {
        //            templateUrl: 'app/valuations/summary/valuationSummary.html',
        //            controller: 'valuationSummaryController',
        //            controllerAs: 'vm'
        //        },
        //        'valuationSummaryHeader@valuation.valuationSummary': {
        //            templateUrl: 'app/valuations/summary/valuationSummaryHeader.html',
        //            controller: 'valuationSummaryHeaderController',
        //            controllerAs: 'vm'
        //        }
        //    }
        //}).state('valuation.valuationProperties', {
        //    url: '/properties',
        //    data: {
        //        title: '',
        //        sidebaricon: 'valuations'
        //    },
        //    views: {
        //        '': {
        //            templateUrl: 'app/valuations/properties/valuationProperties.html',
        //            controller: 'valuationPropertiesController',
        //            controllerAs: 'vm'
        //        }
        //    }
        //})
        ;
    }
})();
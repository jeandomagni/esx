﻿(function () {
    'use strict';

    var controllerId = 'basicsDealPhysicalSizeController';

    angular.module('app').controller(controllerId, basicsDealPhysicalSizeController);

    basicsDealPhysicalSizeController.$inject = ['dealsService', '_'];

    function basicsDealPhysicalSizeController(dealsService, _) {
        var vm = this;
        vm.init = function (form) {
            vm.form = form;

            // If no valuation properties, we probably skipped that step because there was only one property.
            // So copy it over to the valuation properties.
            if (!form.result.valuationProperties) {
                form.result.valuationProperties = form.meta.dealSummary.properties;
            }

            // If we haven't already loaded them, load the distinct primary property usages.
            if (!form.meta.propertyUsages) {
                var propertyKeys = _.map(form.result.valuationProperties, function(p) { return p.propertyKey });
                dealsService.getPropertyUsages(form.result.deal.dealID, propertyKeys).then(function (response) {
                    vm.form.meta.propertyUsages = response;
                });
            }
        }
    }
})();
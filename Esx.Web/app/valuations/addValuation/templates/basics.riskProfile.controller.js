﻿(function () {
    'use strict';

    var controllerId = 'basicsRiskProfileController';

    angular.module('app').controller(controllerId, basicsRiskProfileController);

    basicsRiskProfileController.$inject = ['lookups'];

    function basicsRiskProfileController(lookups) {
        var vm = this;
        vm.options = lookups.DealValuationRiskProfile.GetAll();
    }
})();
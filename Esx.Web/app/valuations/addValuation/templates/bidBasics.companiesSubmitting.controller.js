﻿(function () {
    'use strict';

    var controllerId = 'bidBasicsCompaniesSubmittingController';

    angular.module('app').controller(controllerId, bidBasicsCompaniesSubmittingController);

    bidBasicsCompaniesSubmittingController.$inject = ['companiesService'];

    function bidBasicsCompaniesSubmittingController(companiesService) {
        var vm = this;
        vm.init = init;
        vm.selectedCompanyChange = selectedCompanyChange;
        vm.companyIntellisense = companyIntellisense;
        vm.removeCompany = removeCompany;
        
        function init(form) {
            vm.form = form;
        };

        function selectedCompanyChange(item) {
            if (item) {
                vm.form.result.companiesSubmittingBids = vm.form.result.companiesSubmittingBids || [];
                vm.form.result.companiesSubmittingBids.push(item);
                vm.companySearch.searchText = null;
                vm.selectedCompany = null;
            }
        }

        function companyIntellisense(query) {
            if (query.length < 3) {
                return null;
            }
            return companiesService.companyIntellisense(query).then(function (searchResults) {
                return vm.intellisenseSearchResults = searchResults;
            });
        }

        function removeCompany(index) {
            vm.form.result.companiesSubmittingBids = vm.form.result.companiesSubmittingBids.splice(index, 1);
        }
    }
})();
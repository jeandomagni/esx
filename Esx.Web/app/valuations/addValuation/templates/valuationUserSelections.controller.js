﻿(function () {
    'use strict';

    var controllerId = 'valuationUserSelectionsController';

    angular.module('app').controller(controllerId, valuationUserSelectionsController);

    valuationUserSelectionsController.$inject = [];

    function valuationUserSelectionsController() {
        var vm = this;
        vm.isEmptyObject = isEmptyObject;

        vm.init = function(form) {
            vm.form = form;
        }

        function isEmptyObject(obj) {
            for (var prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    return false;
                }
            }
            return true;
        }
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'basicsLoansInvolvedController';

    angular.module('app').controller(controllerId, basicsLoansInvolvedController);

    basicsLoansInvolvedController.$inject = ['$scope', 'coreType', 'searchService', '_'];

    function basicsLoansInvolvedController($scope, coreType, searchService, _) {
        var vm = this;
        vm.init = function (form) {
            vm.form = form;

            if (!form.meta.dealLoans) {
                var queryObject = {
                    coreObjectType: coreType.loan,
                    dealMentionId: form.result.deal.dealMentionId,
                    pageSize: 100,
                    filters: [
                        {
                            Name: "PropertyKey",
                            Values: _.map(form.result.valuationProperties, function (p) { return { Value: p.propertyKey } })
                        }
                    ]
                };

                searchService.search(queryObject).then(function (response) {
                    vm.form.meta.dealLoans = response.resources;
                });
            }
        }

    }
})();
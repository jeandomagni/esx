﻿(function () {
    'use strict';

    var controllerId = 'scenarioDealStatusForBidsController';

    angular.module('app').controller(controllerId, scenarioDealStatusForBidsController);

    scenarioDealStatusForBidsController.$inject = ['lookups'];

    function scenarioDealStatusForBidsController(lookups) {
        var vm = this;

        var preBidDealStatuses = [
            lookups.DealStatus.Targeting.Code,
            lookups.DealStatus.Pitching.Code,
            lookups.DealStatus.PitchedAndLost.Code,
            //lookups.DealStatus.OnHold.Code, // A deal can be on hold anytime, not just pre-bid, so omit this.
            lookups.DealStatus.Packaging.Code,
            lookups.DealStatus.Marketing.Code
        ];
        
        vm.init = function (form) {
            vm.form = form;
            vm.options = [];

            var statuses = lookups.DealStatus.GetAll();
            for (var i = 0; i < statuses.length; i++) {
                var status = statuses[i];
                if (preBidDealStatuses.indexOf(status.Code) === -1) {
                    vm.options.push(status);
                }
            }
        }
    }
})();
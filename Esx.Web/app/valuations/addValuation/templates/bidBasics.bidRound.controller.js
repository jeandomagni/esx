﻿(function () {
    'use strict';

    var controllerId = 'bidBasicsBidRoundController';

    angular.module('app').controller(controllerId, bidBasicsBidRoundController);

    bidBasicsBidRoundController.$inject = [];

    function bidBasicsBidRoundController() {
        var vm = this;
        vm.init = init;

        function init(form) {
            vm.form = form;

            if (form.meta.latestValuation && form.meta.latestValuation.bidRound > 0) {
                vm.maxRound = form.meta.latestValuation.bidRound + 1;
            } else {
                vm.maxRound = 2;
            }
        };
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'basicsPropertiesIncludedController';

    angular.module('app').controller(controllerId, basicsPropertiesIncludedController);

    basicsPropertiesIncludedController.$inject = [];

    function basicsPropertiesIncludedController() {
        var vm = this;

        vm.init = function (form) {
            vm.form = form;
            vm.form.meta.basicsPropertiesIncludedTouched = true;

            if (angular.isUndefined(vm.form.result.valuationProperties)) {
                onAllPropertiesChange();
            }

        };

        vm.onAllPropertiesChange = onAllPropertiesChange;
        
        function onAllPropertiesChange() {
            vm.form.result.valuationProperties = vm.form.result.dealAllProperties
                ? vm.form.meta.dealSummary.properties
                : [];
        }
    }
})();
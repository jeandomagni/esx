﻿(function () {
    'use strict';

    var controllerId = 'scenarioDealStatusForValuationController';

    angular.module('app').controller(controllerId, scenarioDealStatusForValuationController);

    scenarioDealStatusForValuationController.$inject = ['lookups'];

    function scenarioDealStatusForValuationController(lookups) {
        var vm = this;
        vm.form = null;
        vm.options = [];

        vm.init = function (form, safeApply) {
            vm.form = form;
            var dealStatuses = lookups.DealStatus.GetAll();
            for (var i = 0; i < dealStatuses.length; i++) {
                var dealStatus = dealStatuses[i];
                vm.options.push(dealStatus.Code);

                // Default to the current deal status and don't show any statuses after the current status.
                if (dealStatus.Code === form.result.deal.dealStatus) {
                    (function (dealStatusCode) {
                        safeApply(function () {
                            if (!form.result.valuationDealStatus)
                                form.result.valuationDealStatus = dealStatusCode;
                        });
                    })(dealStatus.Code);
                    
                    break;
                }
            }
        };
    }
})();
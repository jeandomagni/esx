﻿(function () {
    'use strict';

    var controllerId = 'addValuationController';

    angular.module('app').controller(controllerId, addValuationController);

    addValuationController.$inject = ['common', 'valuationService'];

    function addValuationController(common, valuationService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.form = valuationService.getWizardForm(wizardComplete);

        function wizardComplete(result) {
            console.log("Wizard Finished");
        }
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'addValuationBasicsController';

    angular.module('app').controller(controllerId, addValuationBasicsController);

    addValuationBasicsController.$inject = ['$scope', '$q', '$timeout', 'common'];

    function addValuationBasicsController($scope, $q, $timeout, common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;

        // Properties
        vm.questions = [];
        vm.currentQuestion = null;

        // Methods
        vm.goToStep = goToStep;
        vm.addStep = addStep;

        // Initialize
        vm.currentStep = vm.addStep('Valuation Basics', 'app/valuations/addValuation/templates/basics.html');
        vm.addStep('Valuation Scenario and Deal Status', 'app/valuations/addValuation/templates/scenarioAndStatus.html');

        function goToStep(step, index) {
            vm.currentStep = step;
        }

        function addStep(label, stepTemplate) {
            var step = {
                label: label,
                edit: false,
                optional: false,
                progress: 0,
                stepTemplate: stepTemplate
            };
            vm.steps.push(step);
            return step;
        }
    }
})();
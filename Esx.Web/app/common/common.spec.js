﻿describe('common', function () {

    beforeEach(module('app'));

    var common,
        $rootScope,
        $q,
        events;

    beforeEach(inject(function (_common_, _$rootScope_, _$q_, _events_) {
        common = _common_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        events = _events_;

        spyOn($rootScope, '$broadcast');        
    }));

    afterEach(function () {
       
    });

    describe('activateController', function () {
        it('should broadcast begin event', function () {
            // arrange
            var data = { controllerId: 'testController' };

            // act
            common.activateController([], data.controllerId);

            // assert
            expect($rootScope.$broadcast).toHaveBeenCalledWith(events.beginControllerActivate, data);
        });
    });

    describe('pageHeading', function () {
        it('should broadcast page heading', function () {
            // arrange
            var data = { heading: 'testHeading', mentionId: 'testMentionId', phoneNumber: '555-555-1212', emailAddress: 'test@yahoo.com' };

            // act
            common.setPageHeading(data.heading, data.mentionId,data.phoneNumber,data.emailAddress);

            // assert
            expect($rootScope.$broadcast).toHaveBeenCalledWith('handlePageHeading', data);
        });
    });
})
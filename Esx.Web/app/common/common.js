(function () {
    'use strict';

    // Define the common module 
    // Contains services:
    //  - common
    //  - logger
    //  - spinner
    var commonModule = angular.module('common', []);

    commonModule.factory('common', common);

    common.$inject = ['$q', '$rootScope', '$timeout', '$stateParams', '$window', '$log', 'logger', 'events', '$mdMedia', '$document'];

    function common($q, $rootScope, $timeout, $stateParams, $window, $log, logger, events, $mdMedia, $document) {
        var throttles = {};

        var service = {
            // common angular dependencies
            $broadcast: $broadcast,
            $q: $q,
            $timeout: $timeout,
            $stateParams: $stateParams,
            $window: $window,
            // generic
            activateController: activateController,
            createSearchThrottle: createSearchThrottle,
            isNumber: isNumber,
            $log: $log, // for debugging
            logger: logger, // for accessibility
            textContains: textContains,
            setPageHeading: setPageHeading,
            isIE: isIE
        };

        return service;

        function activateController(promises, controllerId) {
            var data = { controllerId: controllerId };
            $broadcast(events.beginControllerActivate, data);
            return $q.all(promises).then(function (eventArgs) {
                $broadcast(events.controllerActivated, data);
            });
        }

        function setPageHeading(heading, mentionId, phoneNumber, emailAddress) {
            $rootScope.$broadcast('handlePageHeading', { heading: heading, mentionId: mentionId, phoneNumber: phoneNumber, emailAddress: emailAddress });
        }

        function $broadcast() {
            return $rootScope.$broadcast.apply($rootScope, arguments);
        }

        function createSearchThrottle(viewmodel, list, filteredList, filter, delay) {
            // After a delay, search a viewmodel's list using 
            // a filter function, and return a filteredList.

            // custom delay or use default
            delay = +delay || 300;
            // if only vm and list parameters were passed, set others by naming convention 
            if (!filteredList) {
                // assuming list is named sessions, filteredList is filteredSessions
                filteredList = 'filtered' + list[0].toUpperCase() + list.substr(1).toLowerCase(); // string
                // filter function is named sessionFilter
                filter = list + 'Filter'; // function in string form
            }

            // create the filtering function we will call from here
            var filterFn = function () {
                // translates to ...
                // vm.filteredSessions 
                //      = vm.sessions.filter(function(item( { returns vm.sessionFilter (item) } );
                viewmodel[filteredList] = viewmodel[list].filter(function (item) {
                    return viewmodel[filter](item);
                });
            };

            return (function () {
                // Wrapped in outer IFFE so we can use closure 
                // over filterInputTimeout which references the timeout
                var filterInputTimeout;

                // return what becomes the 'applyFilter' function in the controller
                return function (searchNow) {
                    if (filterInputTimeout) {
                        $timeout.cancel(filterInputTimeout);
                        filterInputTimeout = null;
                    }
                    if (searchNow || !delay) {
                        filterFn();
                    } else {
                        filterInputTimeout = $timeout(filterFn, delay);
                    }
                };
            })();
        }

        function isNumber(val) {
            // negative or positive
            return /^[-]?\d+$/.test(val);
        }

        function textContains(text, searchText) {
            return text && -1 !== text.toLowerCase().indexOf(searchText.toLowerCase());
        }

        function MoneyFormat(labelValue) {
            // Nine Zeroes for Billions
            return Math.abs(Number(labelValue)) >= 1.0e+9

                 ? Math.abs(Number(labelValue)) / 1.0e+9 + " B"
                 // Six Zeroes for Millions 
                 : Math.abs(Number(labelValue)) >= 1.0e+6

                 ? Math.abs(Number(labelValue)) / 1.0e+6 + " MM"
                 // Three Zeroes for Thousands
                 //: Math.abs(Number(labelValue)) >= 1.0e+3

                 //? Math.abs(Number(labelValue)) / 1.0e+3 + "K"

                 : Math.abs(Number(labelValue));

        }

        function isIE() {
            return !!$document[0].documentMode;
        }
    }
})();
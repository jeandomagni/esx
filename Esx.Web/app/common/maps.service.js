﻿(function () {
    'use strict';

    var serviceId = 'mapsService';
    angular
        .module('app')
        .service(serviceId, mapsService);

    mapsService.$inject = [];

    function mapsService() {

        this.getExtent = function (markers)
        {
            var newExtent = new google.maps.LatLngBounds();
            return  markers.reduce(function (extent, marker) {
                return newExtent.extend(marker.getPosition());
            }, new google.maps.LatLngBounds());
        }
        this.formatAddress = function (place) {
            var addressComponents = {
                street_number: '',
                route: '',
                locality: '',
                administrative_area_level_1: '',
                country: '',
                postal_code: '',
                formatted_address: ''
            };

            if (place && place.address_components) {
                place.address_components.forEach(function (component) {
                    var addressType = component.types[0];
                    addressComponents[addressType] = component.short_name
                    addressComponents.formatted_address = place.formatted_address
                });

                return addressComponents;
            }
        }

        this.formatAddressLongNames = function (place) {
            var addressComponents = {
                street_number: '',
                route: '',
                locality: '',
                administrative_area_level_1: '',
                country: '',
                postal_code: '',
                formatted_address: ''
            };

            if (place && place.address_components) {
                place.address_components.forEach(function (component) {
                    var addressType = component.types[0];
                    addressComponents[addressType] = component.long_name
                    addressComponents.formatted_address = place.formatted_address
                });

                return addressComponents;
            }
        }
 


    }
})();
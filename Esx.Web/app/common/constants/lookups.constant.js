﻿
(function () {
    "use strict";
	angular.module("app").constant("lookups", new function(){

		this.AssetContactType = new function() {
			this.Unknown = { Code: "Unknown", Label: "Unknown" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Unknown];
				return all;
			}
		},
		this.AssetType = new function() {
			this.Loan = { Code: "Loan", Label: "Loan" };
			this.Property = { Code: "Property", Label: "Property" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Loan,this.Property];
				return all;
			}
		},
		this.BidStatus = new function() {
			this.B = { Code: "B", Label: "Bridesmaid" };
			this.Bf = { Code: "BF", Label: "Best and Final" };
			this.D = { Code: "D", Label: "Declined" };
			this.W = { Code: "W", Label: "Bid Awarded" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.B,this.Bf,this.D,this.W];
				return all;
			}
		},
		this.DealContactType = new function() {
			this.Acquisitions = { Code: "Acquisitions", Label: "Acquisitions" };
			this.AssetManagement = { Code: "Asset Management", Label: "Asset Management" };
			this.Development = { Code: "Development", Label: "Development" };
			this.Dispositions = { Code: "Dispositions", Label: "Dispositions" };
			this.ExecutiveCSuite = { Code: "Executive - (C Suite)", Label: "Executive - (C Suite)" };
			this.FinancingDept = { Code: "Financing/Dept", Label: "Financing/Dept" };
			this.Leasing = { Code: "Leasing", Label: "Leasing" };
			this.PortfolioManagement = { Code: "Portfolio Management", Label: "Portfolio Management" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Acquisitions,this.AssetManagement,this.Development,this.Dispositions,this.ExecutiveCSuite,this.FinancingDept,this.Leasing,this.PortfolioManagement];
				return all;
			}
		},
		this.DealParticipationType = new function() {
			this.Borrower = { Code: "Borrower", Label: "Borrower" };
			this.Broker = { Code: "Broker", Label: "Broker" };
			this.Buyer = { Code: "Buyer", Label: "Buyer" };
			this.Lender = { Code: "Lender", Label: "Lender" };
			this.Seller = { Code: "Seller", Label: "Seller" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Borrower,this.Broker,this.Buyer,this.Lender,this.Seller];
				return all;
			}
		},
		this.DealStatus = new function() {
			this.Targeting = { Code: "Targeting", Label: "Targeting" };
			this.Pitching = { Code: "Pitching", Label: "Pitching" };
			this.PitchedAndLost = { Code: "Pitched and Lost", Label: "Pitched and Lost" };
			this.OnHold = { Code: "On Hold", Label: "On Hold" };
			this.Packaging = { Code: "Packaging", Label: "Packaging" };
			this.Marketing = { Code: "Marketing", Label: "Marketing" };
			this.BidReceipt = { Code: "Bid Receipt", Label: "Bid Receipt" };
			this.UnderAgreement = { Code: "Under Agreement", Label: "Under Agreement" };
			this.NonRefundable = { Code: "Non-Refundable", Label: "Non-Refundable" };
			this.WithdrawnByClient = { Code: "Withdrawn by Client", Label: "Withdrawn by Client" };
			this.WithdrawnByCounterparty = { Code: "Withdrawn by Counterparty", Label: "Withdrawn by Counterparty" };
			this.Closed = { Code: "Closed", Label: "Closed" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Targeting,this.Pitching,this.PitchedAndLost,this.OnHold,this.Packaging,this.Marketing,this.BidReceipt,this.UnderAgreement,this.NonRefundable,this.WithdrawnByClient,this.WithdrawnByCounterparty,this.Closed];
				return all;
			}
		},
		this.DealTeamRole = new function() {
			this.Primary = { Code: "PRIMARY", Label: "PRIMARY" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Primary];
				return all;
			}
		},
		this.DealType = new function() {
			this.DebtPlacement = { Code: "Debt Placement", Label: "Debt Placement" };
			this.EquitySaleLessThan100 = { Code: "Equity Sale <100%", Label: "Equity Sale <100%" };
			this.EquitySale100 = { Code: "Equity Sale 100%", Label: "Equity Sale 100%" };
			this.InvestmentBanking = { Code: "Investment Banking", Label: "Investment Banking" };
			this.LoanSale = { Code: "Loan Sale", Label: "Loan Sale" };
			this.NonTransactionalAdvisory = { Code: "Non-transactional Advisory", Label: "Non-transactional Advisory" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.DebtPlacement,this.EquitySaleLessThan100,this.EquitySale100,this.InvestmentBanking,this.LoanSale,this.NonTransactionalAdvisory];
				return all;
			}
		},
		this.DealValuationRiskProfile = new function() {
			this.Core = { Code: "Core", Label: "Core" };
			this.CorePlus = { Code: "Core-Plus", Label: "Core-Plus" };
			this.Trophy = { Code: "Trophy", Label: "Trophy" };
			this.ValueAdd = { Code: "Value-Add", Label: "Value-Add" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Core,this.CorePlus,this.Trophy,this.ValueAdd];
				return all;
			}
		},
		this.DealValuationType = new function() {
			this.Basecase = { Code: "BaseCase", Label: "Base Case" };
			this.Bid = { Code: "Bid", Label: "Bid" };
			this.Customname = { Code: "CustomName", Label: "Custom Name" };
			this.Pushcase = { Code: "PushCase", Label: "Push Case" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Basecase,this.Bid,this.Customname,this.Pushcase];
				return all;
			}
		},
		this.Feature = new function() {
			this.Application = { Code: "application", Label: "application" };
			this.Bids = { Code: "Bids", Label: "Bids" };
			this.Fees = { Code: "Fees", Label: "Fees" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Application,this.Bids,this.Fees];
				return all;
			}
		},
		this.LegalEntity = new function() {
			this.Lgl = { Code: "LGL", Label: "LGL" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Lgl];
				return all;
			}
		},
		this.LoanBenchmarkType = new function() {
			this.E = { Code: "E", Label: "Euribor" };
			this.L = { Code: "L", Label: "Libor" };
			this.Na = { Code: "NA", Label: "Not Available" };
			this.S = { Code: "S", Label: "Swap" };
			this.T = { Code: "T", Label: "Displayed as Treasury" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.E,this.L,this.Na,this.S,this.T];
				return all;
			}
		},
		this.LoanCashMgmtType = new function() {
			this.Dscr = { Code: "DSCR", Label: "Debt service coverage ratio" };
			this.Dy = { Code: "DY", Label: "Debt Yield" };
			this.Ltv = { Code: "LTV", Label: "Loan to Value" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Dscr,this.Dy,this.Ltv];
				return all;
			}
		},
		this.LoanLenderType = new function() {
			this.Db = { Code: "DB", Label: "Domestic Bank" };
			this.Df = { Code: "DF", Label: "Debt Fund" };
			this.Fb = { Code: "FB", Label: "Foreign Bank" };
			this.Lc = { Code: "LC", Label: "Life Co" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Db,this.Df,this.Fb,this.Lc];
				return all;
			}
		},
		this.LoanPaymentType = new function() {
			this.Df = { Code: "DF", Label: "Defeasance" };
			this.L = { Code: "L", Label: "Lockout" };
			this.O = { Code: "O", Label: "Open" };
			this.Pb = { Code: "PB", Label: "% of Balance" };
			this.Sp = { Code: "SP", Label: "Spread Maintenance" };
			this.Ym = { Code: "YM", Label: "Yield Maintenance" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Df,this.L,this.O,this.Pb,this.Sp,this.Ym];
				return all;
			}
		},
		this.LoanRateType = new function() {
			this.E = { Code: "E", Label: "Euribor" };
			this.L = { Code: "L", Label: "Libor" };
			this.S = { Code: "S", Label: "Swap" };
			this.T = { Code: "T", Label: "Treasury" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.E,this.L,this.S,this.T];
				return all;
			}
		},
		this.LoanSourceType = new function() {
			this.A = { Code: "A", Label: "Aquisition" };
			this.R = { Code: "R", Label: "Refinance" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.A,this.R];
				return all;
			}
		},
		this.LoanTermDateType = new function() {
			this.Finalmaturity = { Code: "FinalMaturity", Label: "Final Maturity" };
			this.Lockoutend = { Code: "LockoutEnd", Label: "Lockout End" };
			this.Nextmaturity = { Code: "NextMaturity", Label: "Next Maturity" };
			this.Yieldmaintenanceend = { Code: "YieldMaintenanceEnd", Label: "Yield Maintenance End" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Finalmaturity,this.Lockoutend,this.Nextmaturity,this.Yieldmaintenanceend];
				return all;
			}
		},
		this.MarketingListActionType = new function() {
			this.CalledLeftMsg = { Code: "Called Left Msg", Label: "Called Left Msg" };
			this.Contacted = { Code: "Contacted", Label: "Contacted" };
			this.TourScheduled = { Code: "Tour Scheduled", Label: "Tour Scheduled" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.CalledLeftMsg,this.Contacted,this.TourScheduled];
				return all;
			}
		},
		this.MarketingListStatusType = new function() {
			this.AdditionalMaterials = { Code: "Additional Materials", Label: "Additional Materials" };
			this.CaApproved = { Code: "CA Approved", Label: "CA Approved" };
			this.CaFollowUpSignature = { Code: "CA Follow Up Signature", Label: "CA Follow Up Signature" };
			this.CaFollowUpTerms = { Code: "CA Follow Up Terms", Label: "CA Follow Up Terms" };
			this.CallForOffers = { Code: "Call for Offers", Label: "Call for Offers" };
			this.PrintedOm = { Code: "Printed OM", Label: "Printed OM" };
			this.TeaserSent = { Code: "Teaser Sent", Label: "Teaser Sent" };
			this.WarRoomAccess = { Code: "War Room Access", Label: "War Room Access" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.AdditionalMaterials,this.CaApproved,this.CaFollowUpSignature,this.CaFollowUpTerms,this.CallForOffers,this.PrintedOm,this.TeaserSent,this.WarRoomAccess];
				return all;
			}
		},
		this.MarketType = new function() {
			this.Msa = { Code: "MSA", Label: "Metro Statistical Area" };
			this.Region = { Code: "Region", Label: "U.S. Region" };
			this.State = { Code: "State", Label: "US State Name" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Msa,this.Region,this.State];
				return all;
			}
		},
		this.PhysicalAddressType = new function() {
			this.Blank = { Code: "Blank", Label: "Blank" };
			this.Alt = { Code: "Alt", Label: "Alt" };
			this.Alternate = { Code: "Alternate", Label: "Alternate" };
			this.Business = { Code: "Business", Label: "Business" };
			this.Business2 = { Code: "Business2", Label: "Business2" };
			this.Caleast = { Code: "CalEast", Label: "CalEast" };
			this.Chicago = { Code: "Chicago", Label: "Chicago" };
			this.China = { Code: "China", Label: "China" };
			this.Colorado = { Code: "Colorado", Label: "Colorado" };
			this.Company = { Code: "Company", Label: "Company" };
			this.Eastdil = { Code: "Eastdil", Label: "Eastdil" };
			this.Fedex = { Code: "Fedex", Label: "Fedex" };
			this.Havorvord = { Code: "Havorvord", Label: "Havorvord" };
			this.Home = { Code: "Home", Label: "Home" };
			this.Japan = { Code: "Japan", Label: "Japan" };
			this.London = { Code: "London", Label: "London" };
			this.Mailing = { Code: "Mailing", Label: "Mailing" };
			this.Main = { Code: "Main", Label: "Main" };
			this.Maybe = { Code: "maybe", Label: "maybe" };
			this.New = { Code: "New", Label: "New" };
			this.Ny = { Code: "NY", Label: "NY" };
			this.Office = { Code: "Office", Label: "Office" };
			this.Overnight = { Code: "Overnight", Label: "Overnight" };
			this.Physical = { Code: "Physical", Label: "Physical" };
			this.Po = { Code: "PO", Label: "PO" };
			this.Primary = { Code: "Primary", Label: "Primary" };
			this.Purchase = { Code: "Purchase", Label: "Purchase" };
			this.Secondary = { Code: "Secondary", Label: "Secondary" };
			this.Sf = { Code: "SF", Label: "SF" };
			this.Shipping = { Code: "Shipping", Label: "Shipping" };
			this.South = { Code: "South", Label: "South" };
			this.Street = { Code: "Street", Label: "Street" };
			this.Temp = { Code: "Temp", Label: "Temp" };
			this.Toronto = { Code: "Toronto", Label: "Toronto" };
			this.Usps = { Code: "USPS", Label: "USPS" };
			this.Work = { Code: "Work", Label: "Work" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Blank,this.Alt,this.Alternate,this.Business,this.Business2,this.Caleast,this.Chicago,this.China,this.Colorado,this.Company,this.Eastdil,this.Fedex,this.Havorvord,this.Home,this.Japan,this.London,this.Mailing,this.Main,this.Maybe,this.New,this.Ny,this.Office,this.Overnight,this.Physical,this.Po,this.Primary,this.Purchase,this.Secondary,this.Sf,this.Shipping,this.South,this.Street,this.Temp,this.Toronto,this.Usps,this.Work];
				return all;
			}
		},
		this.PreferenceType = new function() {
			this.Startpage = { Code: "STARTPAGE", Label: "Dashboard" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Startpage];
				return all;
			}
		},
		this.Role = new function() {
			this.DtcaWsdEsxUsers = { Code: "DTCA_WSD_ESX_USERS", Label: "DTCA_WSD_ESX_USERS" };
			this.DtcgWsdEspreportsFees = { Code: "DTCG_WSD_ESPREPORTS_FEES", Label: "DTCG_WSD_ESPREPORTS_FEES" };
			this.DtcgWsdusaEspreportsBids = { Code: "DTCG_WSDUSA_ESPREPORTS_BIDS", Label: "DTCG_WSDUSA_ESPREPORTS_BIDS" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.DtcaWsdEsxUsers,this.DtcgWsdEspreportsFees,this.DtcgWsdusaEspreportsBids];
				return all;
			}
		},
		this.StateProvidence = new function() {
			this.Ab = { Code: "AB", Label: "AB" };
			this.Ak = { Code: "AK", Label: "AK" };
			this.Al = { Code: "AL", Label: "AL" };
			this.Ar = { Code: "AR", Label: "AR" };
			this.Az = { Code: "AZ", Label: "AZ" };
			this.Bc = { Code: "BC", Label: "BC" };
			this.Ca = { Code: "CA", Label: "CA" };
			this.Co = { Code: "CO", Label: "CO" };
			this.Ct = { Code: "CT", Label: "CT" };
			this.Dc = { Code: "DC", Label: "DC" };
			this.De = { Code: "DE", Label: "DE" };
			this.Fl = { Code: "FL", Label: "FL" };
			this.Ga = { Code: "GA", Label: "GA" };
			this.Hi = { Code: "HI", Label: "HI" };
			this.Ia = { Code: "IA", Label: "IA" };
			this.Id = { Code: "ID", Label: "ID" };
			this.Il = { Code: "IL", Label: "IL" };
			this.In = { Code: "IN", Label: "IN" };
			this.Ks = { Code: "KS", Label: "KS" };
			this.Ky = { Code: "KY", Label: "KY" };
			this.La = { Code: "LA", Label: "LA" };
			this.Ma = { Code: "MA", Label: "MA" };
			this.Mb = { Code: "MB", Label: "MB" };
			this.Md = { Code: "MD", Label: "MD" };
			this.Me = { Code: "ME", Label: "ME" };
			this.Mi = { Code: "MI", Label: "MI" };
			this.Mn = { Code: "MN", Label: "MN" };
			this.Mo = { Code: "MO", Label: "MO" };
			this.Ms = { Code: "MS", Label: "MS" };
			this.Mt = { Code: "MT", Label: "MT" };
			this.Nb = { Code: "NB", Label: "NB" };
			this.Nc = { Code: "NC", Label: "NC" };
			this.Nd = { Code: "ND", Label: "ND" };
			this.Ne = { Code: "NE", Label: "NE" };
			this.Nh = { Code: "NH", Label: "NH" };
			this.Nj = { Code: "NJ", Label: "NJ" };
			this.Nl = { Code: "NL", Label: "NL" };
			this.Nm = { Code: "NM", Label: "NM" };
			this.Ns = { Code: "NS", Label: "NS" };
			this.Nv = { Code: "NV", Label: "NV" };
			this.Ny = { Code: "NY", Label: "NY" };
			this.Oh = { Code: "OH", Label: "OH" };
			this.Ok = { Code: "OK", Label: "OK" };
			this.On = { Code: "ON", Label: "ON" };
			this.Or = { Code: "OR", Label: "OR" };
			this.Pa = { Code: "PA", Label: "PA" };
			this.Pe = { Code: "PE", Label: "PE" };
			this.Pr = { Code: "PR", Label: "PR" };
			this.Qc = { Code: "QC", Label: "QC" };
			this.Ri = { Code: "RI", Label: "RI" };
			this.Sc = { Code: "SC", Label: "SC" };
			this.Sd = { Code: "SD", Label: "SD" };
			this.Sk = { Code: "SK", Label: "SK" };
			this.Tn = { Code: "TN", Label: "TN" };
			this.Tx = { Code: "TX", Label: "TX" };
			this.Ut = { Code: "UT", Label: "UT" };
			this.Va = { Code: "VA", Label: "VA" };
			this.Vt = { Code: "VT", Label: "VT" };
			this.Wa = { Code: "WA", Label: "WA" };
			this.Wi = { Code: "WI", Label: "WI" };
			this.Wv = { Code: "WV", Label: "WV" };
			this.Wy = { Code: "WY", Label: "WY" };
			var all = null;
			this.GetAll = function(){
				if(all == null) all = [this.Ab,this.Ak,this.Al,this.Ar,this.Az,this.Bc,this.Ca,this.Co,this.Ct,this.Dc,this.De,this.Fl,this.Ga,this.Hi,this.Ia,this.Id,this.Il,this.In,this.Ks,this.Ky,this.La,this.Ma,this.Mb,this.Md,this.Me,this.Mi,this.Mn,this.Mo,this.Ms,this.Mt,this.Nb,this.Nc,this.Nd,this.Ne,this.Nh,this.Nj,this.Nl,this.Nm,this.Ns,this.Nv,this.Ny,this.Oh,this.Ok,this.On,this.Or,this.Pa,this.Pe,this.Pr,this.Qc,this.Ri,this.Sc,this.Sd,this.Sk,this.Tn,this.Tx,this.Ut,this.Va,this.Vt,this.Wa,this.Wi,this.Wv,this.Wy];
				return all;
			}
		} 
	});
})();

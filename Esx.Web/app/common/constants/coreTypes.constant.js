﻿(function () {
    "use strict";

    angular.module("app").constant('coreType', {
        company: 'A',
        contact: 'C',
        deal: 'D',
        employee: 'E',
        loan: 'L',
        office: 'O',
        property: 'P',
        pitch: 'T'
    });

})();
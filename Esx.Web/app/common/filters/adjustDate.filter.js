﻿(function () {

    "use strict";
    var filterId = "adjustDate";

    angular
        .module('app')
        .filter('adjustDate', adjustDate);

    function adjustDate() {
        return function (input) {
            var myTimeZone = moment.tz.guess().toString();
            var newYork = moment.tz(input.toString(), 'America/New_York');
            var myTime = newYork.clone().tz(myTimeZone);
            return myTime.format();
        };
    }


})();

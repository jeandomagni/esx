﻿(function() {
    angular.module('app').filter('tagsFilter', ['mentionsService',tagsFilter]);

    function tagsFilter(mentionsService) {
        return function (input) {
            var settings = {
                highlight: [
                    { match: new RegExp(mentionsService.mentionRegex), _class: 'mention', uri: '' },
                    { match: /\B#\w+/g, _class: 'hashtag', uri: '#/hashtag/'  }
                ]
            };
            
            var text = input && input.commentText ? input.commentText : (input && input.tagName) ? input.tagName : input;

            if (!text) {
                return '';
            }

            for (var i = 0; i < settings.highlight.length; i++) {
                text = text.replace(settings.highlight[i].match, function (match) {
                    if (settings.highlight[i]._class === 'mention') {
                        var mentionData =  mentionsService.parse(match);
                        return formatTagHtml(settings.highlight[i]._class, mentionData.text, mentionData.uri);
                    }
                    return formatTagHtml(settings.highlight[i]._class, match, settings.highlight[i].uri);
                });
            }            
            return text;
        };
    }

    function formatTagHtml(tagClass, text, href) {
        var span = '<span class="' + tagClass + '">' + text + '</span>';
        if (!angular.isDefined(href) || href === '') {
            return span;
        }
        if (tagClass === 'hashtag') {
            return '<a href="' + href + text.substring(1, text.length) + '">' + span + '</a>';
        } else {
            return '<a href="' + href + '">' + span + '</a>';
        }
    }
})();

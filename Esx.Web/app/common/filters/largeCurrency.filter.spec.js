﻿(function () {
    'use strict';

    beforeEach(module('app'));

    describe('largeCurrency.filter', function () {
        var filterUnderTest;

        beforeEach(inject(function ( largeCurrencyFilter) {
            filterUnderTest = largeCurrencyFilter;
        }));

        describe('init', function () {

            it('should apply angulars currancy filter', function () {

                //arrange
                //act
                var actual = filterUnderTest(123.56);

                //assert
                expect(actual).toEqual("$123.56");

            });

            it('should filter thousands ', function () {
                //arrange
                //act
                var actual = filterUnderTest(1234);

                //assert
                expect(actual).toEqual("$1.23 K");

            });

            it('should filter millions ', function () {
                //arrange
                //act
                var actual = filterUnderTest(1234560);

                //assert
                expect(actual).toEqual("$1.23 MM");

            });

            it('should filter billions', function () {
                //arrange
                //act
                var actual = filterUnderTest(1237560000);

                //assert
                expect(actual).toEqual("$1.24 B");

            });
        });
    });
}());

﻿(function () {
    "use strict";

    var filterId = "largeCurrency";

    angular.module("app").filter(filterId, largeCurrencyFilter);

    largeCurrencyFilter.$inject = ['currencyFilter'];

    function largeCurrencyFilter(currencyFilter) {

        return function (input, includeSign) {

            var sizeIndicator = "";

            if (input / 1000000000 >= 1.0) {
                input = input / 1000000000;
                sizeIndicator = " B";

            } else if (input / 1000000 >= 1.0) {
                input = input / 1000000;
                sizeIndicator = " MM";
            } else if (input / 1000 >= 1.0) {
                input = input / 1000;
                sizeIndicator = " K";
            }

            return (angular.isDefined(includeSign) && includeSign === false)
                ? input == null ? "" : input + sizeIndicator
                : currencyFilter(input) + sizeIndicator;
        };

    }
})();
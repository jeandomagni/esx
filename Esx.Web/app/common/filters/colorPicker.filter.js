﻿(function () {

    "use strict";
    var filterId = "colorPicker";

    angular
        .module('app')
        .filter('colorPicker', colorPicker);

    function colorPicker() {
        return function (input) {
            var colorCode = 'blue';
            switch (input) {
                case 1:
                    colorCode = 'green';
                    break;
                case 2:
                    colorCode = 'orange';
                    break;
                case 3:
                    colorCode = 'silver';
                    break;
                case 4:
                    colorCode = 'yellow';
                    break;
                case 5:
                    colorCode = 'red';
                    break;
                default:
            }
            return colorCode;
        };
    }
})();

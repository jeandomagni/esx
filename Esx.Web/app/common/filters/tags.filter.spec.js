﻿(function () {
    'use strict';

    beforeEach(module('app'));

    beforeEach(module(function ($urlRouterProvider) {
        $urlRouterProvider.otherwise(function () { return false; });
    }));

    describe('tags.filter', function () {
        var filter, tags, compile, scope;

        beforeEach(inject(function ($rootScope, _$filter_, $compile) {
            scope = $rootScope;
            filter = _$filter_;
            compile = $compile;
            tags = '<div>Meeting with #CompanyXYZ later. Having lunch with @JoeJohnson|C at 2. Closed the #123MainStreet deal.</div>';                                
        }));

        describe('init', function () {
            var filterResult, element;

            beforeEach(function () {
                //arrange //act
                filterResult = filter('tagsFilter')(tags);
                element = compile(filterResult)(scope);
                scope.$apply();
            });

            it('should filter tags correctly', function () {
                //assert
                var mentions = element.find('.mention');
                var hashtags = element.find('.hashtag');
                expect(mentions.length).toBe(1);
                expect(hashtags.length).toBe(2);
            });
        });
    });
}());

﻿(function() {
    "use strict";

    var filterId = "encode";

    angular.module("app").filter(filterId, encodeFilter);

    encodeFilter.$inject = ["$window"];

    function encodeFilter($window) {
        return $window.encodeURIComponent;
    }
})();
﻿describe('dialog', function () {

    beforeEach(module('app'));

    var dialog;

    beforeEach(inject(function (_dialog_) {
        dialog = _dialog_;
    }));

    afterEach(function () {

    });

    describe('service', function () {
        it('should be initialized correctly', function () {
            // arrange            
            // act            
            // assert
            expect(dialog.alert).toBeDefined();
            expect(dialog.confirm).toBeDefined();
            expect(dialog.custom).toBeDefined();
        });
    });
})
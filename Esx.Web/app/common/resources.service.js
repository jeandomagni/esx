(function () {
    'use strict';

    var serviceId = 'resourcesService';

    angular.module('app').factory(serviceId, resourcesService);

    resourcesService.$inject = ['uriService', '$resource'];

    function resourcesService(uriService, $resource) {

        var service = {
            getStateProvinces: getStateProvinces,
            getCountries: getCountries,
            getUserPreferences: getUserPreferences
        };

        return service;

        function getStateProvinces() {
            return $resource(uriService.stateProvinces).query().$promise;
        }

        function getCountries() {
            return $resource(uriService.countries).query().$promise;
        }

        function getUserPreferences() {
            return $resource(uriService.userPreferences).query().$promise;
        }
    }
})();
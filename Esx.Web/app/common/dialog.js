﻿(function () {

    angular.module('common').factory('dialog', dialog);

    dialog.$inject = ['$mdDialog'];

    function dialog($mdDialog) {
        var defaultOptions = {
            resolveCallback: function (data) {
                $mdDialog.hide(data);
            },
            cancelCallback: function () {
                $mdDialog.cancel();
            },
            clickOutsideToClose: true,
            parent: angular.element(document.body),
            templateUrl: '',
            controller: function () { },
            okayText: 'Okay',
            cancelText: 'Cancel',
            title: 'Dialog',
            content: 'Content'
        };

        var service = {
            alert: alert,
            confirm: confirm,
            custom: custom
        };

        return service;

        function alert(options) {
            var opts = intializeOptions(options);
            var alertDialog = $mdDialog.alert()
                .parent(opts.parent)
                .clickOutsideToClose(opts.clickOutsideToClose)
                .title(opts.title)
                .content(opts.content)
                .ok(opts.okayText);
            $mdDialog.show(alertDialog);
        }

        function confirm(options) {
            var opts = intializeOptions(options);
            var confirmDialog = $mdDialog.confirm()
                        .title(opts.title)
                        .content(opts.content)
                        .clickOutsideToClose(opts.clickOutsideToClose)
                        .ok(opts.okayText)
                        .cancel(opts.cancelText);
            $mdDialog.show(confirmDialog).then(function () {
                opts.resolveCallback();
            }, function () {
                opts.cancelCallback();
            });
        }

        function custom(options) {
            var opts = intializeOptions(options);
            $mdDialog.show({
                controller: opts.controller,
                controllerAs: 'vm',
                templateUrl: opts.templateUrl,
                parent: opts.parent,
                clickOutsideToClose: opts.clickOutsideToClose
            }).then(function (data) {
                opts.resolveCallback(data);
            }, function () {
                opts.cancelCallback();
            });
        }

        function intializeOptions(options) {
            _.merge(defaultOptions, options);
            return defaultOptions;
        }
    }
})();
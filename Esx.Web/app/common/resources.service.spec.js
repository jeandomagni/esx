describe('companies.service', function () {

    beforeEach(module('app'));

    var resourcesService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_resourcesService_, _uriService_, _$httpBackend_) {

        resourcesService = _resourcesService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        // ignore html templates. Not sure if this is a good approach. 
        $httpBackend.whenGET(/.*.html/).respond(200, '');
        $httpBackend.whenPOST(/.*.html/).respond(201, '');
        $httpBackend.whenPATCH(/.*.html/).respond(204, '');

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getStateProvinces', function () {

        it('should call server with the state provinces uri', function() {

            //Arrange
            $httpBackend.expectGET(uriService.stateProvinces, { 'Accept': 'application/json, text/plain, */*' }).respond([]);

            //Act
            resourcesService.getStateProvinces();

            //Assert
            $httpBackend.flush();

        });
    });

    describe('getCountries', function () {

        it('should call server with the countries uri', function () {

            //Arrange
            $httpBackend.expectGET(uriService.countries, { 'Accept': 'application/json, text/plain, */*' }).respond([]);

            //Act
            resourcesService.getCountries();

            //Assert
            $httpBackend.flush();

        });
    });

    describe('getUserPreferences', function () {

        it('should call server with the user preferences uri', function () {

            //Arrange
            $httpBackend.expectGET(uriService.userPreferences, { 'Accept': 'application/json, text/plain, */*' }).respond([]);

            //Act
            resourcesService.getUserPreferences();

            //Assert
            $httpBackend.flush();

        });
    });
});
﻿(function () {
    'use strict';
    
    var app = angular.module('app', [
        // Angular modules 
        'ngAnimate',        // animations
        'ui.router',          // routing
        'ngSanitize',       // sanitizes html bindings (ex: sidebar.js)
        'ngResource',
        'ngMessages',

        // Custom modules 
        'common',           // common functions, logger, spinner
        'ngMaterial',
        'uiGmapgoogle-maps', // Google Maps integration
        'bw.paging',
        'infinite-scroll',
        'ngMdIcons',
        'mentio',
        'ngFileUpload',
        'as.sortable'
    ]);
    
    // Handle routing errors and success events
    app.run(['$state', '$rootScope', function ($state, $rootScope) {
        // Include $state to kick start the router.
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.subnavClass = toState.data.subnavClass;
        });
    }]);        
})();
﻿(function () {
    describe('shell.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            config,
            state,
            controller,
            events,
            q,
            rootScope;

        var activateController = function () {
            controller = $controller('shell', {
                common: common,
                config: config,
                events: events,
                $state: state,
                $q: q,
                $rootScope: rootScope
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _config_, _events_, _$state_, _$q_, _$rootScope_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            config = _config_;
            events = _events_;
            state = _$state_;
            q = _$q_;
            rootScope = _$rootScope_;

            //fakes
            //$rootScope = {};

            common.$window = { location: {} };
        }));

        describe('controller', function () {
            // arrange
            beforeEach(function () {
                activateController();
            });

            it('shell controller should be defined', function () {
                // act
                // assert
                expect(controller).toBeDefined();
            });

        });
    });
})();

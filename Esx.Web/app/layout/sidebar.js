﻿(function() {
    'use strict';

    var controllerId = 'sidebar';

    angular.module('app').controller(controllerId, sidebar);

    sidebar.$inject = ['common', '$mdSidenav', '$mdMedia', '$mdUtil', '$rootScope', '$scope', '$state', 'events',
        'utilsService', '$location', 'sidebarService', 'commentsService', 'userdeletegatesService'];


    function sidebar(common, $mdSidenav, $mdMedia, $mdUtil, $rootScope, $scope, $state, events,
        utilsService, $location, sidebarService, commentsService, userdeletegatesService) {
        var vm = this;

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var commonService = common;

        vm.toggleLeft = buildToggler('extended');
        vm.closeLeft = buildCloser('left');
        vm.activeIcon = null;
        vm.sidebarOpen = $mdMedia('gt-sm');
        vm.closeSidebar = closeSidebar;

        vm.isLockedOpen = isLockedOpen;
        vm.states = [];
        vm.userList = [];

        $scope.toggleExtendedSidebar = function() {
            $mdSidenav('extended')
                .toggle()
                .then(function() {});
        }

        $rootScope.$on(events.openSidebar, function(data) {
            if (!$mdMedia('gt-sm')) {
                $mdUtil.debounce(function() {
                    $mdSidenav('extended').toggle().then(function() {});
                }, 0)();

            } else {
                vm.sidebarOpen = !vm.sidebarOpen;
            }
        });

        $scope.$on('$viewContentLoaded', function() {
            if ($state && $state.current && $state.current.data && angular.isDefined($state.current.data.sidebaricon)) {
                vm.activeIcon = $state.current.data.sidebaricon;
            }

            var heading = ($state && $state.current && $state.current.data) ? $state.current.data.title : '';
            if (heading != '') {
                common.setPageHeading(heading);
            }
         
        });

        $rootScope.$on(events.refreshRemindersCount, function() {
            getActiveRemindersCount();
        });

        activate();

        function activate() {
            common.activateController([], 'sidebar')
                .then(function() {});
                    
        }

        function closeSidebar(state) {
            if ($state.current.name.indexOf(state) !== -1) {
                $state.go(state, {}, {reload: true});
            }
            else {
                $state.go(state);
            }
            
            $mdSidenav('extended')
                .close()
                .then(function () { });
        }

        function buildToggler(navID) {
            var debounceFn = $mdUtil.debounce(function () {
                $mdSidenav(navID)
                  .toggle()
                  .then(function () {});
            }, 200);
            return debounceFn;
        }

        function buildCloser(navID) {
            if (!$mdMedia('gt-md')) {
                var debounceFn = $mdUtil.debounce(function () {
                    $mdSidenav(navID)
                        .toggle()
                        .then(function () {});
                }, 200);
                return debounceFn;
            }
        }

        function isLockedOpen() {
            return $mdMedia('gt-sm');
        }


        function getBuild() {
            utilsService.getBuildNumber().then(function (result) {
                $scope.buildNumber = result.build.buildNumber; 
            });
        }

        function getActiveRemindersCount() {
            commentsService.getActiveRemindersCount().then(function (result) {
                vm.activeRemindersCount = result.count;
            });
        }
        function getUserFullName() {
            sidebarService.getUserFullName().then(function (user) {
                $scope.fullName = user.isPrincipal ? user.fullName :
                    user.principalUserFullName + " logged in as " + user.fullName;
            });
        }
        getBuild();
        getActiveRemindersCount();
        getUserFullName();
    };
})();

﻿(function () {
    'use strict';

    var serviceId = 'sidebarService';

    angular.module('app').factory(serviceId, sidebarService);

    sidebarService.$inject = ['uriService', '$resource'];

    function sidebarService(uriService, $resource) {

        var service = {
            getUserFullName: getUserFullName
        };

        return service;

        function getUserFullName() {
            return $resource(uriService.userFullName).get().$promise;
        }

    }
})();
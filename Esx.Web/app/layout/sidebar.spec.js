﻿(function () {
    describe('sidebar.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            $mdSidenav,
            $mdMedia,
            $mdUtil,
            $log,
            $rootScope,
            $scope,
            $state,
            controller,
            events,
            $q,

            mock__mdSidenav = function(component) {
                return {
                    isMock: true,
                    close: function() {
                        return $q.when();
                    }
                }
            },

            state,
            current;

        beforeEach(inject(function (_$controller_, _$rootScope_, _common_, _$mdUtil_, _$mdMedia_, _events_, _$state_) {

            //$provide.value('$mdSidenav', mock__mdSidenav);

            //inject
            $controller = _$controller_;
            common = _common_;
            events = _events_;
            $mdSidenav = mock__mdSidenav();
            $mdMedia = _$mdMedia_;
            $mdUtil = _$mdUtil_;

            $rootScope = _$rootScope_;
            $scope = _$rootScope_.$new();

            $state = _$state_;

            state = {
                name: 'dashboard',
                url: '/',
                templateUrl: 'app/dashboard/dashboard.html',
                controller: 'dashboard',
                controllerAs: 'vm',
                data: {
                    title: 'Dashboard',
                    sidebaricon: 'dashboard',
                    includeInSideBar: true
                }
            };

            current = {
                name: 'companies',
                url: '/',
                templateUrl: 'app/companies/companies.html',
                controller: 'companies',
                controllerAs: 'vm',
                data: {
                    title: 'Companies',
                    sidebaricon: 'companies',
                    includeInSideBar: true
                }
            };

            //spys
            $mdSidenav.toggle = jasmine.createSpy();

            spyOn($mdUtil, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn($state, '$current').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(current);
                    }
                };
            });

            common.$window = { location: {} };

            //instantiate controller
            controller = $controller('sidebar', {
                common: common,
                $scope: $scope
            });
        }));
        
    describe('activate', function () {

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });
    });
});
})();


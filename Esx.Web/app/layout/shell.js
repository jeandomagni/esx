﻿(function () {
    'use strict';

    var controllerId = 'shell';

    angular.module('app').controller(controllerId, shell);

    shell.$inject = ['$rootScope', 'common', 'config', 'events', '$state', '$q', '$timeout', '$resource', 'userdeletegatesService', '$mdDialog'];

    function shell($rootScope, common, config, events, $state, $q, $timeout, $resource, userdeletegatesService, $mdDialog) {
        var vm = this;
        var logSuccess = common.logger.getLogFn(controllerId, 'success');
        vm.busyMessage = 'Please wait ...';
        vm.subnavClass = $rootScope.subnavClass;
        vm.isBusy = false;
        vm.spinnerOptions = {
            radius: 40,
            lines: 7,
            length: 0,
            width: 30,
            speed: 1.7,
            corners: 1.0,
            trail: 100,
            color: '#F58A00'
        };

        vm.isFabOpen = false;
        vm.fabHidden = false;
        vm.fabHover = false;

        vm.openSidebar = openSidebar;
        vm.setTab = setTab;
        vm.tabIndex = -1;
        vm.search = search;
        vm.getUsers = getUsers;
        vm.impersonateUser = impersonateUser;
        vm.userList = [];
        vm.resetUserAccount = resetUserAccount;

        // TODO: Clean up when developing Quick Add.
        vm.quickAddItems = [
            { name: "Company", color: '#ff7445', icon: 'companies', href: '#/companies/quickAdd' },
            { name: "Contact", color: '#fbb03b', icon: 'contacts', href: '#/contacts' },
            { name: "Property", color: '#0da3ff', icon: 'properties', href: '#/addproperty' },
            { name: "Loan", color: '#2cbdb9', icon: 'loans', href: '#/loans' },
            { name: "Deal", color: '#2cc23a', icon: 'deals', href: '#/adddeal' },
            { name: "Valuation", color: 'purple', icon: 'valuations', href: '#/addvaluation' }
        ];


        vm.splitMentionId = function (string, nb) {
            var array = string.split('|');
            return array[nb];
        }

        function openSidebar() {
            $rootScope.$broadcast(events.openSidebar);
        };

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            vm.subnavClass = toState.data.subnavClass;
            var subTabs = $state.$current.data.subtabs;
            if (subTabs) {
                vm.subnavItems = subTabs;
                setTab();
            }
            else if ($state.$current.parent.data) {
                vm.subnavItems = $state.$current.parent.data.subtabs;
                setTab();
            } else {
                vm.tabIndex = -1;
                vm.subnavItems = [];
            }
        });

        $rootScope.$on('handlePageHeading', function (event, data) {
            vm.displayHeading = (data && data.heading && data.heading.length > 0);
            vm.displayMentionId = (data && data.mentionId && data.mentionId.length > 0);
            vm.displayPhoneNumber = (data && data.phoneNumber && data.phoneNumber.length > 0);
            vm.displayEmailAddress = (data && data.emailAddress && data.emailAddress.length > 0);

            if (vm.displayHeading) {
                vm.heading = data.heading;
            }

            if (vm.displayMentionId) {
                vm.mentionId = vm.splitMentionId(data.mentionId, 0);
            } else {
                vm.heading = angular.uppercase(vm.heading);
            }

            if (vm.displayPhoneNumber) {
                vm.phoneNumber = data.phoneNumber;
            } 

            if (vm.displayEmailAddress) {
                vm.emailAddress = data.emailAddress;
            }

        });

        function toggleSpinner(on) {
            vm.isBusy = on;
        }

        $rootScope.$on(events.controllerActivated, function (data) {
            toggleSpinner(false);
        });

        function setTab() {
            $timeout(function() {
                return $q(function () {
                switch ($state.current.name) {
                    case "pipeline":
                    case "contact.summary":
                    case "company.companySummary":
                    case "companyEdit.profile":
                    case "property.propertySummary":
                    case "newsFeed":
                    case "reminders":
                    case "deal.dealSummary":
                        vm.tabIndex = 0;
                        break;
                    case "pipelineoffice":
                    case "contact.details":
                    case "company.companyDetails":
                    case "companyEdit.compliance":
                    case "property.propertyDetails":
                    case "newsFeedEveryone":
                    case "remindersSnoozed":
                        vm.tabIndex = 1;
                        break;
                    case "pipelinemy":
                    case "contact.deals":
                    case "company.companyLocations":
                    case "company.companyLocationsAdd":
                    case "property.propertyDeals":
                    case "remindersDone":
                        vm.tabIndex = 2;
                        break;
                        case "contact.marketinglists":
                    case "company.companyContacts":
                    case "company.companyContactsAdd":
                    case "property.propertyLoans":
                        vm.tabIndex = 3;
                        break;
                    case "company.companyDeals":
                    case "deal.marketing.marketingList":
                    case "deal.marketing.selectMarketingContacts":
                        vm.tabIndex = 4;
                        break;
                    case "company.companyProperties":
                        vm.tabIndex = 5;
                        break;
                    case "company.companyLoans":
                    case "company.companyLoansAdd":
                        vm.tabIndex = 6;
                        break;
                    default:
                        vm.tabIndex = -1;
                }
            });
            }, 0);

        }

        function search() {
            $state.go('search', { searchInput: vm.searchInput });
            vm.searchInput = null;
        }
        function getUsers() {
            return userdeletegatesService.getDelegatedUsers().then(function (userList) {
                vm.userList = userList;
                setSelectedUser();
                return vm.userList;
            });
        }
        function impersonateUser(user) {
            showProgressWheel();
            userdeletegatesService.impersonateUser(user).then(function (result) {
                common.$window.location.reload();
            });
        }
        function resetUserAccount(){
            userdeletegatesService.clear().then(function (result) {
                common.$window.location.reload();
            });
        }
        function setSelectedUser () {
            for (var i = 0, l = vm.userList.length; i < l ; i++) {
                if (vm.userList[i].isSelected == true) {
                    vm.selectedUser = vm.userList[i];
                    vm.selectedUserPosition = i;
                    return;
                }
            }
        }
        function showProgressWheel () {
            $mdDialog.show({
                template: ' <div layout="row" layout-sm="column" layout-align="space-around"><md-progress-circular md-mode="indeterminate"></md-progress-circular></div>',
                parent: angular.element(document.body),
                clickOutsideToClose: false
            })
        };
        getUsers();
    };
})();
﻿(function () {
describe('sidebar.service', function () {

    beforeEach(module('app'));

    var sidebarService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_sidebarService_, _uriService_, _$httpBackend_) {

        sidebarService = _sidebarService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getUserFullName', function () {
        it('should call server with sidebar username uri', function () {

            //Arrange
            $httpBackend.expectGET('api/sidebar/username', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            sidebarService.getUserFullName();

            //Assert
            $httpBackend.flush();

        });
    });
});
})();
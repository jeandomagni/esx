﻿(function () {
    'use strict';

    var app = angular.module('app');

    // Configure the strings used for events.
    app.constant('events', getEventStrings());    

    function getEventStrings() {
        return {
            openSidebar: 'open.sidebar',
            beginControllerActivate: 'begin.controller.activate',
            controllerActivated: 'controller.activated',
            refreshMap: 'refresh.map',
            marketingListUpdate: 'marketinglist.updated',
            marketingListCompanyRemoved: 'marketinglist.company.removed',
            marketingListCompanyUpdate: 'marketinglist.company.updated',
            refreshRemindersCount: 'refresh.reminders.count'
        };
    }
})();
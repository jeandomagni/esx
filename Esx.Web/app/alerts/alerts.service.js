﻿(function () {
    'use strict';

    var serviceId = 'alertsService';

    angular.module('app').factory(serviceId, alertsService);

    alertsService.$inject = ['uriService', '$resource', '$http'];

    function alertsService(uriService, $resource, $http) {

        var service = {
            getAlerts: getAlerts,
            acknowledgeAlert: acknowledgeAlert,
            removeAlert: removeAlert,
            snoozeAlert: snoozeAlert
        };

        return service;

        function getAlerts(query) {
            return $resource(uriService.summaryAlerts).get(query).$promise;
        }

        function acknowledgeAlert(alertId) {
            return $http.put(uriService.alertAcknowledge, { data: alertId });
        }

        function removeAlert(alertId) {
            return $http.put(uriService.alertRemove, { data: alertId });
        }

        function snoozeAlert(alertId) {
            return $http.put(uriService.alertSnooze, { data: alertId });
        }
    }
})();
﻿describe('alerts.service', function () {

    beforeEach(module('app'));

    var alertsService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_alertsService_, _uriService_, _$httpBackend_) {

        alertsService = _alertsService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getSummaryAlerts', function () {

        it('should call server with summary alerts uri', function () {

            //Arrange
            $httpBackend.expectGET('api/alerts/summaryAlerts', { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            alertsService.getAlerts({});

            //Assert
            $httpBackend.flush();

        });
    });

    describe('acknowledgeAlert', function () {
        it('should call server with acknowledge alert uri', function () {

            //Arrange
            var data = {
                data: 1
            }

            $httpBackend.expectPUT(uriService.alertAcknowledge, data, { "Accept": "application/json, text/plain, */*", "Content-Type": "application/json;charset=utf-8" }).respond({});

            //Act
            alertsService.acknowledgeAlert(1);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('removeAlert', function () {
        it('should call server with addComment uri', function () {

            //Arrange
            var data = {
                data: 1
            }

            $httpBackend.expectPUT(uriService.alertRemove, data, { "Accept": "application/json, text/plain, */*", 'Content-Type': 'application/json;charset=utf-8' }).respond({});

            //Act
            alertsService.removeAlert(1);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('snoozeAlert', function () {
        it('should call server with addComment uri', function () {

            //Arrange
            var data = {
                data: 1
            }

            $httpBackend.expectPUT(uriService.alertSnooze, data, { "Accept": "application/json, text/plain, */*", 'Content-Type': 'application/json;charset=utf-8' }).respond({});

            //Act
            alertsService.snoozeAlert(1);

            //Assert
            $httpBackend.flush();
        });
    });

});
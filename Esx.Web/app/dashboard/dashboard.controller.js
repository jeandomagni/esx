﻿(function () {
    'use strict';

    var controllerId = 'dashboard';

    angular.module('app').controller(controllerId, dashboard);

    dashboard.$inject = ['$rootScope', 'common', 'datacontext', 'dialog'];

    function dashboard($rootScope, common, datacontext, dialog) {

        var vm = this;
        vm.dashboard = {};
        vm.showConfirm = showConfirm;
        
        activate();

        function activate() {
            common.activateController([], controllerId).then(function () { });
            getMyDeals();
            getWatchlist();
        }

        function getMyDeals() {
            return datacontext.getMyDeals().then(function (data) {
                return vm.myDeals = data;
            });
        }

        function getWatchlist() {
            return datacontext.getWatchlist().then(function (data) {
                return vm.watchList = data;
            });
        }

        function showConfirm() {
            dialog.confirm({
                resolveCallback: function () { console.log('resolved'); },
                cancelCallback: function () { console.log('canceled'); }
            });
        }

    }
})();
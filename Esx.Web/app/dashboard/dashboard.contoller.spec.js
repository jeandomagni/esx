﻿(function() {
    describe('dashboard.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            $scope,
            controller;

        var activateController = function () {
            controller = $controller('dashboard', {
                common: common,
            });
        };

        beforeEach(inject(function (_$controller_, _common_) {

            //inject
            $controller = _$controller_;
            common = _common_;

            //fakes
            $scope = {};

            common.$window = { location: {} };

            //spys

        }));

        describe('controller', function () {
            // arrange
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                // act
                // assert
                expect(controller).toBeDefined();
            });

        });

    });
})();


﻿(function () {
    'use strict';

    var serviceId = 'commentsService';

    angular.module('app').factory(serviceId, commentsService);

    commentsService.$inject = ['uriService', '$resource'];

    function commentsService(uriService, $resource) {

        var service = {
            getComments: getComments,
            addComment: addComment,
            deleteComment: deleteComment,
            getMentionedComments: getMentionedComments,
            addReminder: addReminder,
            markReminderSnoozed: markReminderSnoozed,
            markReminderDone: markReminderDone,
            deleteReminder: deleteReminder,
            getActiveRemindersCount: getActiveRemindersCount
        };

        return service;

        function getComments(query) {
            return $resource(uriService.commentList).get(query).$promise;
        }

        function getMentionedComments(query) {
            return $resource(uriService.commentMentionedList).get(query).$promise;
        }

        function addComment(comment) {
            return $resource(uriService.commentSave).save(comment).$promise;
        }

        function deleteComment(commentId) {
            return $resource(uriService.commentDelete).delete({commentId: commentId}).$promise;
        }

        function addReminder(reminder) {
            return $resource(uriService.commentReminderAdd).save(reminder).$promise;
        }

        function markReminderSnoozed(reminder) {
            return $resource(uriService.commentReminderSnooze).save(reminder).$promise;
        }

        function markReminderDone(reminder) {
            return $resource(uriService.commentReminderDone).save(reminder).$promise;
        }

        function deleteReminder(reminderId) {
            return $resource(uriService.commentReminderDelete).delete({ reminderId: reminderId }).$promise;
        }

        function getActiveRemindersCount() {
            return $resource(uriService.commentReminderCount).get().$promise;
        }
    }
})();
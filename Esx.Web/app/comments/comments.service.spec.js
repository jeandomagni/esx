﻿describe('comments.service', function () {

    beforeEach(module('app'));

    var commentsService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_commentsService_, _uriService_, _$httpBackend_) {

        commentsService = _commentsService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        uriService.commentMentionedList = 'api/comments/mentioned/list';

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getMentionedComments', function () {

        it('should call server with commentMentionedList list uri', function () {

            //Arrange
            $httpBackend.expectGET(uriService.commentMentionedList, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            commentsService.getMentionedComments();

            //Assert
            $httpBackend.flush();

        });

        it('should make GET request with query', function () {

            //Arrange
            var query = {
                mentionId: "@mention|A",
                offset: 100,
                pageSize: 10
            };

            var expectedUri = uriService.commentMentionedList + '?mentionId=@mention%7CA&offset=100&pageSize=10';

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            commentsService.getMentionedComments(query);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('getComments', function () {
        it('should call server with getComments list uri', function () {

            //Arrange
            $httpBackend.expectGET(uriService.commentList, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            commentsService.getComments();

            //Assert
            $httpBackend.flush();

        });

        it('should make GET request with query', function () {

            //Arrange
            var query = {
                mentionId: "@mention|A",
                offset: 100,
                pageSize: 10
            };

            var expectedUri = uriService.commentList + '?mentionId=@mention%7CA&offset=100&pageSize=10';

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            commentsService.getComments(query);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('addComment', function () {
        it('should call server with addComment uri', function () {

            //Arrange
            var comment = {
                accessToken: "1",
                primaryMentionID: "@123",
                commentText: 'test',
                comment_Key: 0,
                tags: [],
                mentions: []
            };

            $httpBackend.expectPOST(uriService.commentSave, comment, { "Accept": "application/json, text/plain, */*", 'Content-Type': 'application/json;charset=utf-8' }).respond({});

            //Act
            commentsService.addComment(comment);

            //Assert
            $httpBackend.flush();

        });

        it('should make POST request with comment', function () {

            //Arrange
            var comment = {
                accessToken: "1",
                primaryMentionID: "@123",
                commentText: 'test',
                comment_Key: 0,
                tags: [],
                mentions: []
            };

            var expectedUri = uriService.commentSave;

            $httpBackend.expectPOST(expectedUri, comment, { "Accept": "application/json, text/plain, */*", 'Content-Type': 'application/json;charset=utf-8' }).respond({});

            //Act
            commentsService.addComment(comment);

            //Assert
            $httpBackend.flush();

        });
    });

});
﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.directive('esReminderPin', esReminderPin);

    esReminderPin.$inject = ['commentsService', '$mdDialog', 'coreType', 'common', 'events', 'employeesService', '$q'];

    function esReminderPin(commentsService, $mdDialog, coreType, common, events, employeesService, $q) {
        return {
            restrict: 'AE',
            scope: true,
            bindToController: {
                mentionId: '@',
                mentionLabel: '@',
                coreObjectType: '@'
            },
            templateUrl: 'app/directives/esReminderPin.html',
            controllerAs: 'vm',
            controller: function () {
                var vm = this;

                vm.setReminder = setReminder;
                vm.showCustomReminderDialog = showCustomReminderDialog;
                vm.saveComment = saveComment;
                vm.getEmployeeText = getEmployeeText;
                vm.searchEmployees = searchEmployees;

                function saveComment(reminder) {
                    if (vm.assignee) {
                        reminder.assignee = vm.assignee;
                        reminder.assigneeName = vm.assigneeName;
                    }
                    var comment =
                    {
                        mentions: [vm.mentionId],
                        documentIds: [],
                        createReminder: true,
                        reminder: reminder
                    };
                    if (reminder.reminderText) {
                        comment.commentText = reminder.reminderText + ' ' + getMentionMarkup();
                    } else {
                        comment.commentText = 'Reminder for ' + getMentionMarkup();
                    }
                    return commentsService.addComment(comment).then(function (result) {
                        vm.activeReminder = true;
                    });
                }

                function setReminder(when) {
                    var reminder = vm.reminder || {};
                    
                    if (vm.reminderText) {
                        reminder.reminderText = vm.reminderText;
                    }
                    if (when === 'tomorrow') {
                        reminder.reminderDate = moment().add(1, 'days')._d;
                        vm.saveComment(reminder).then(function (result) {
                            vm.reminder = result;
                            vm.reminderText = null;
                            common.$broadcast(events.refreshRemindersCount);
                        });
                    }
                    else if (when === 'nextWeek') {
                        reminder.reminderDate = moment().add(7, 'days')._d;
                        vm.saveComment(reminder).then(function (result) {
                            vm.reminder = result;
                            vm.reminderText = null;
                            common.$broadcast(events.refreshRemindersCount);
                        });
                    }
                    else if (when === 'someDay') {
                        reminder.reminderDate = null;
                        vm.saveComment(reminder).then(function (result) {
                            vm.reminder = result;
                            vm.reminderText = null;
                            common.$broadcast(events.refreshRemindersCount);
                        });
                    }
                    else if (when === 'custom') {
                        showCustomReminderDialog().then(function (when) {
                            reminder.reminderDate = when;
                            vm.saveComment(reminder).then(function (result) {
                                vm.reminder = result;
                                vm.reminderText = null;
                                common.$broadcast(events.refreshRemindersCount);
                            });
                        });
                    }
                }

                function showCustomReminderDialog() {
                    return $mdDialog.show({
                        controller: function customReminderController($scope, $mdDialog) {
                            $scope.minDate = moment()._d;
                            $scope.apply = function (reminderForm) {
                                if (reminderForm.$invalid) {
                                    return;
                                }
                                var month = $scope.customDay.getMonth() + 1;
                                var day = $scope.customDay.getDate();
                                var year = $scope.customDay.getFullYear();
                                var customDate = moment(month + '-' + day + '-' + year + ' ' + $scope.customTime, 'MM-DD-YYYY h:mm a');
                                $mdDialog.hide(customDate._d);
                            }
                            $scope.cancel = function () {
                                $mdDialog.cancel();
                            }
                        },
                        templateUrl: 'app/newsFeed/customReminder.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: true,
                        fullscreen: false
                    }).then(function (when) {
                        return when;
                    });
                }

                function getMentionMarkup() {
                    var itemClass = 'tag-link-' + vm.coreObjectType;
                    var page = getDetailPageForType(vm.coreObjectType);
                    var detailPage = '#/' + page + '/' + vm.mentionId + '/summary';
                    return '<a target="_blank" href="' + detailPage + '" class="' + itemClass + '">' + vm.mentionLabel + '</a>';
                }

                function getDetailPageForType(type) {
                    switch (type) {
                        case coreType.company:
                            return 'companies';
                        case coreType.contact:
                            return 'contact';
                        case coreType.property:
                            return 'properties';
                        case coreType.deal:
                            return 'deals';
                        case coreType.loan:
                            return 'loans';
                        case coreType.pitch:
                            return 'pitches';
                    }
                    return null;
                }

                function searchEmployees(term) {
                    employeesService.getEmployees(term).then(function (result) {
                        vm.employees = result.resources;
                    });
                    return $q.when(vm.employees);
                };

                function getEmployeeText(term) {
                    vm.assignee = term.userKey;
                    vm.assigneeName = term.fullName;
                    return term.fullName;
                }

            }
        };
    };
})();

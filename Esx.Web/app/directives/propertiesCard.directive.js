﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.directive('propertiesCard', function () {
        return {
            restrict: 'E',
            scope: {
                getProperties: '&',
                onSelect: '&'
            },
            templateUrl: 'app/directives/propertiesCard.html',
            link: function (scope, element, attrs) {
                scope.getProperties().then(function (result) {
                    scope.loaded = true;
                    scope.properties = result.resources;
                    scope.count = result.pagingData.totalCount;

                    if (attrs.onSelect && scope.properties.length !== 0) {
                        if (scope.properties.length > 1) {
                            scope.allowSelection = true;
                            scope.properties[0].selected = true;
                        }
                        scope.onSelect({ property: scope.properties[0] });
                    }
                });

                scope.getAddress = function (property) {
                    return _.compact([property.address1, property.city, property.state]).join(', ');
                };

                scope.selectProperty = function (property) {
                    // unselect any selected properties
                    _.each(scope.properties, function (p) {
                        if (p.selected) p.selected = false;
                    });
                    property.selected = true;
                    scope.onSelect({ property: property });
                };
            }
        };
    });

})();

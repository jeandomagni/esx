﻿(function () {
    'use strict';

    beforeEach(module('templates'));

    beforeEach(module('app'));

    beforeEach(module(function ($urlRouterProvider) {
        $urlRouterProvider.otherwise(function () { return false; });
    }));

    describe('edTagsField.directive', function () {
        var element,
            scope,
            compile,
            html,
            q,
            commentsService;

        beforeEach(inject(function ($rootScope, $compile, $httpBackend, $templateCache, $q, _commentsService_) {
            scope = $rootScope;
            compile = $compile;
            q = $q;
            commentsService = _commentsService_;


            //spys
            spyOn(commentsService, 'addComment').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback();
                    }
                };
            });

            var template = $templateCache.get('app/directives/edTagsField.html');
            $templateCache.put('app/directives/edTagsField.html', template);
           
        }));

        describe('init', function () {

            beforeEach(function () {
                element = compile('<div ed-tags-field ng-model="vm.messageText" ></div>')(scope);
                scope.$digest();
                html = element.html();
            });

            it('should load the template correctly', function () {
                //assert
                expect(html).toBeDefined();
                var messageBox = element.find('#edMessageBox');
                expect(messageBox.length).toBe(1);
            });
        });

        describe('saveComment', function () {

            beforeEach(function () {

                scope.addedCallback = function() {
                    var x = 23;
                };
                spyOn(scope, 'addedCallback');

            });

            it('should call the commentAddedCallback after comment has been added', function () {

                //arrange
                element = compile('<div ed-tags-field ng-model="vm.messageText" comment-added-callback="addedCallback" ></div>')(scope);
                scope.$digest();
                var button = element.find('button');

                //act
                button.triggerHandler('click');

                //assert
                expect(scope.addedCallback).toHaveBeenCalled();
            });


            it('should not call the commentAddedCallback when it is not set', function () {

                //arrange
                element = compile('<div ed-tags-field ng-model="vm.messageText" ></div>')(scope);
                scope.$digest();
                var button = element.find('button');

                //act
                button.triggerHandler('click');

                //assert
                expect(scope.addedCallback).not.toHaveBeenCalled();
            });
        });

    });
}());

﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.directive('dealsCard', function () {
        return {
            restrict: 'E',
            scope: {
                getDeals: '&'
            },
            templateUrl: 'app/directives/dealsCard.html',
            link: function (scope) {
                scope.getDeals().then(function (result) {
                    scope.loaded = true;
                    scope.deals = result.resources;
                    scope.count = result.pagingData.totalCount;
                });
            }
        };
    });

})();

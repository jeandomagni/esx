﻿describe('directives', function () {

    beforeEach(module('templates'));

    beforeEach(module('app'));

    beforeEach(module(function ($urlRouterProvider) {
        $urlRouterProvider.otherwise(function () { return false; });
    }));

    var $compile,
        $rootScope,
        $templateCache;

    beforeEach(inject(function (_$compile_, _$rootScope_, _$templateCache_) {
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $templateCache = _$templateCache_;

    }));

    describe('propertiesCard', function () {
        var element;
        var fakeProperties;
        var selectedPropertyId;

        beforeEach(function () {

            // Arrange data
            fakeProperties = {
                resources: [
                    { propertyID: 1, propertyName: 'One' },
                    { propertyID: 2, propertyName: 'Two' }
                ],
                pagingData: {
                    totalCount: 2
                }
            };

            $rootScope.dealID = 123;
            $rootScope.getProperties = function () {
                return {
                    then: function (callback) {
                        return callback(fakeProperties);
                    }
                };
            };
            $rootScope.selectProperty = function (property) {
                selectedPropertyId = property.propertyID;
            };

            element = angular.element('<properties-card get-properties="getProperties(dealID)" on-select="selectProperty(property)" />');

            $compile(element)($rootScope);
            $rootScope.$digest();
        });

        it('property links are displayed', function () {
            var propertiesP = element.find('p');
            for (var i = 0; i < propertiesP.length; i++) {
                var links = $(propertiesP[i]).find('a');
                //assert
                expect(links[0].getAttribute('href')).toBe('#/properties/' + fakeProperties.resources[i].propertyID);
                expect(links[0].text).toBe(fakeProperties.resources[i].propertyName);
            }

        });

        it('first property selected', function () {
            //Assert
            expect(selectedPropertyId).toBe(1);
        });
    });
});
(function () {
    'use strict';

    var app = angular.module('app');

    var supportedOperations = [
        {
            'type': 'default',
            'eq': 'equals',
            'ne': 'not equal',
            'sw': 'starts with',
            'ct': 'contains',
            'fw': 'finishes with',
            'in': 'any of',
            'null': 'is empty',
            'nn': 'is not empty'
        },
        {
            'type': 'number',
            'eq': '&#61;',
            'ne': '!&#61;',
            'gt': '&#62;',
            'lt': '&#60;'
        },
        {
            'type': 'date',
            'eq': 'on',
            'ne': 'not on',
            'gt': 'after',
            'lt': 'before',
            'bw': 'between'
        }
    ];

    var coreObjects = [
        {
            type: 'company',
            label: 'Company',
            uri: '#/companies',
            fields: [
                { type: "number", id: "companyid", label: "Company ID" },
                { type: "text", id: "companyname", label: "Company Name" }
            ]
        },
        {
            type: 'contact',
            label: 'Contact',
            uri: '#/contacts',
            fields: [
                        { type: "text", id: "lastname", label: "Last Name" },
                        { type: "text", id: "firstname", label: "First Name" },
                        { type: "text", id: "title", label: "Title" },
                        { type: "number", id: "contactid", label: "Contact ID" },
                        { type: "number", id: "companyid", label: "Company ID" },
                        { type: "boolean", id: "active", label: "Is Active" },
                        { type: "date", id: "dateadded", label: "Created Date" },
                        {
                            type: "list",
                            id: "category",
                            label: "Category",
                            list: [
                                { id: '1', label: '---' },
                                { id: '2', label: 'Investor' },
                                { id: '3', label: 'Lender' },
                                { id: '4', label: 'Broker' },
                                { id: '5', label: 'Offshore' },
                                { id: '6', label: 'Advisor' },
                                { id: '7', label: 'Developer' },
                                { id: '8', label: 'Operator' }
                            ]
                        }
            ]
        },
        {
            type: 'deal',
            label: 'Deal',
            uri: '#/deals',
            fields: [
                { type: "number", id: "dealid", label: "Deal ID" },
                { type: "text", id: "dealname", label: "Deal Name" }
            ]
        },
        {
            type: 'employee',
            label: 'Employee',
            uri: '#/employees',
            fields: [
                { type: "text", id: "firstname", label: "First Name" },
                { type: "text", id: "lastname", label: "Last Name" },
                { type: "text", id: "title", label: "Title" },
                { type: "number", id: "companyid", label: "Company ID" },
                { type: "text", id: "state", label: "State" },
                { type: "text", id: "phone", label: "Phone" }
            ]
        },
        {
            type: 'loan',
            label: 'Loan',
            uri: '#/loans',
            fields: [
                { type: "text", id: "loanname", label: "Loan Name" },
                { type: "number", id: "lendercompanyid", label: "Lender Company ID" },
                { type: "text", id: "lender", label: "Lender" },
                { type: "text", id: "loanname", label: "Loan Name" },
                { type: "number", id: "loanbalance", label: "Loan Balance" },
                { type: "date", id: "originationdate", label: "Origination Date" },
                { type: "date", id: "originalmaturitydate", label: "Original Maturity Date" }
            ]
        },
        {
            type: 'property',
            label: 'Property',
            uri: '#/properties',
            fields: [
                { type: "number", id: "propertyid", label: "Property ID" },
                { type: "text", id: "propertyname", label: "Property Name" },
                { type: "text", id: "state", label: "State" },
                { type: "text", id: "city", label: "City" }
            ]
        }
    ];

    app.directive('advancedSearch', ['common', function (common) {
        return {
            restrict: 'E',
            templateUrl: '/app/directives/advancedSearch.html',
            scope: {
                defaultSearch: '@'
            },
            link: function(scope, element, attrs) {
                scope.coreObjects = coreObjects;
                scope.displayFilters = false;

                var clearFilters = function () {
                    if (scope.$searchWidget.children().length !== 0) {
                        scope.$searchWidget.structFilter("clear");
                    }
                };

                var loadFilters = function (filters) {
                    if (filters && filters.length !== 0) {
                        var coreObject = _.findWhere(scope.coreObjects, { type: scope.selectedObjectType });
                        if (coreObject) {
                            var data = _.map(filters, function (filter) {
                                return formatFilter(filter, coreObject);
                            });
                            scope.$searchWidget.structFilter('val', _.compact(data));
                        }
                    }
                };

                var formatFilter = function (filter, coreObject) {
                    var formattedFilter = null;
                    var field = _.findWhere(coreObject.fields, { id: filter.field });
                    if (field) {
                        var operatorLabel = getOperatorLabel(filter.operator, field.type);
                        if (operatorLabel) {
                            formattedFilter = {
                                field: {
                                    label: field.label,
                                    value: filter.field
                                },
                                operator: {
                                    label: operatorLabel,
                                    value: filter.operator
                                },
                                value: {
                                    label: field.type === 'text' ? '"' + filter.value + '"' : filter.value,
                                    value: field.type === 'boolean' ? (filter.value ? 1 : 0) : filter.value
                                }
                            };
                        }
                    }
                    return formattedFilter;
                };

                var getOperatorLabel = function (opreatorCode, fieldType) {
                    // Merge default and type specific operations
                    var operations = _.merge(supportedOperations[0], _.findWhere(supportedOperations, { type: fieldType }));
                    return operations[opreatorCode];
                };

                var initialize = function () {
                    var coreObjectType = scope.selectedObjectType;
                    scope.$searchWidget = angular.element(element).find('#advancedSearch');
                    if (scope.$searchWidget) {
                        if (coreObjectType) {
                            var coreObject = _.findWhere(scope.coreObjects, { type: coreObjectType }) || { fields: [] };

                            scope.$searchWidget.structFilter({
                                buttonLabels: true,
                                submitButton: true,
                                dateFormat: 'yy-mm-dd',
                                fields: coreObject.fields
                            }).on("submit.search", function (event) {
                                var filters = _.map(scope.$searchWidget.structFilter("val"), function (filter) {
                                    var value = filter.value.value;
                                    var field = _.findWhere(coreObject.fields, { id: filter.field.value });
                                    if (field && field.type === 'boolean') {
                                        value = (!!filter.value.value).toString();
                                    } else if (filter.value.value2) {
                                        value = filter.value.value + ',' + filter.value.value2; // for between operator
                                    }
                                    return {
                                        field: filter.field.value,
                                        operator: filter.operator.value,
                                        value: value
                                    };
                                });
                                common.$window.location.href = coreObject.uri + '?filters=' + encodeURIComponent(JSON.stringify(filters));
                            });
                            scope.displayFilters = true;
                        }
                    }
                };

                var filters;
                if (scope.defaultSearch) {
                    scope.selectedObjectType = scope.defaultSearch;
                    if (common.$stateParams.filters) {
                        filters = JSON.parse(common.$stateParams.filters);
                    }
                }

                initialize();

                if (filters) {
                    loadFilters(filters);
                }

                scope.coreObjectChange = function () {
                    if (scope.$searchWidget && typeof scope.$searchWidget.parent === 'function') {
                        // Replace the widget with a new one
                        var $parent = scope.$searchWidget.parent();
                        scope.$searchWidget.remove();
                        $parent.append(angular.element('<div id="advancedSearch"/>'));
                    }

                    initialize();
                };

                scope.loadSearch = function () {
                    var searchJSON = prompt('Enter Search JSON', '{"type":"contact","filters":[{"field":"active","operator":"eq","value":"true"}]}');
                    var search = JSON.parse(searchJSON);

                    scope.selectedObjectType = search.type;
                    scope.coreObjectChange();
                    loadFilters(search.filters);
                };

                scope.$watch('defaultSearch', function (newValue, oldValue) {
                    if (newValue !== oldValue) {
                        scope.selectedObjectType = scope.defaultSearch;
                        initialize();
                        if (common.$stateParams.filters) {
                            loadFilters(JSON.parse(common.$stateParams.filters));
                        } else {
                            clearFilters();
                        }
                    }
                });
            }
        };
    }]);

})();

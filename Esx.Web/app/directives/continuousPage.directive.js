﻿(function () {
    'use strict';

    angular.module('app').directive('continuousPage', continousPage);

    continousPage.$inject = ['$window', '$q', '$timeout'];

    function continousPage($window, $q, $timeout) {

        return {
            transclude: true,
            templateUrl: 'app/directives/continuousPage.html',
            restrict: 'A',
            scope: {
                queryFunction: '&queryFunction',
                queryObject: '&queryObject',
                container: '@'
            },
            link: function (scope) {

                if (!scope.container) {
                    scope.container = '#contentSection';
                }

                scope.isScrollBusy = function () {
                    return scope.queryObject().isBusy;
                };

                scope.goTo = function () {
                    scope.queryObject().offset = scope.queryObject().offset + scope.queryObject().pageSize;
                    scope.queryFunction()();
                };
            }
        };
    };

}());
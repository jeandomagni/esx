﻿(function () {
    'use strict';

    angular.module('app').directive('edTagsField', edTagsField);

    edTagsField.$inject = ['$compile', 'hashtagService', 'mentionsService', 'commentsService'];

    function edTagsField($compile, hashtagService, mentionsService, commentsService) {
        return {
            restrict: 'EA',
            require: '?ngModel',
            scope: {
                commentAddedCallback: '&commentAddedCallback',
                directionUp: '=',
                placeholder: '@',
                primaryMentionId: '@'
            },
            templateUrl: 'app/directives/edTagsField.html',
            link: function (scope, el, attr, model) {

                if (!model) {
                    return;
                }

                var element = el.find('#edMessageBox');

                if (!!scope.directionUp) {
                    scope.bottomOffset = { bottom: element.parent().height() + 'px' };
                } 

                init();

                function init(settings) {

                    rangy.init();

                    read();

                    scope.settings = {
                        limit: 140,
                        highlight: [
                            { match: /\B@\w+/g, _class: 'mention', url: '#/contacts/' },
                            { match: /\B#\w+/g, _class: 'hashtag', url: '#/hashtag/' }
                        ]
                    };
                    _.merge(scope.settings, settings);

                    scope.previousText = '';
                    scope.mentionExpression = /\B@\w+/g;
                    scope.hashtagExpression = /\B#\w+/g;
                    scope.tagDictionary = {};
                    scope.addedTags = [];
                    scope.addedMentions = [];
                }

                function read() {
                    var html = element.text();
                    model.$setViewValue(html);
                }

                var handleEditorChangeEvent = (function () {
                    var timer;

                    function debouncer() {
                        if (timer) timer = null;
                        processInput();
                    }

                    return function () {
                        if (timer) window.clearTimeout(timer);
                        timer = window.setTimeout(debouncer, 10);
                    };
                })();

                function setCaretToEnd() {
                    var selection = rangy.getSelection();
                    selection.selectAllChildren(element[0]);
                    selection.collapseToEnd();
                }

                function getCaretCharacterOffsetWithin(element) {
                    var caretOffset = 0;
                    var doc = element.ownerDocument || element.document;
                    var win = doc.defaultView || doc.parentWindow;
                    var sel;
                    if (typeof win.getSelection != 'undefined') {
                        sel = win.getSelection();
                        if (sel.rangeCount > 0) {
                            var range = win.getSelection().getRangeAt(0);
                            var preCaretRange = range.cloneRange();
                            preCaretRange.selectNodeContents(element);
                            preCaretRange.setEnd(range.endContainer, range.endOffset);
                            caretOffset = preCaretRange.toString().length;
                        }
                    } else if ((sel = doc.selection) && sel.type != 'Control') {
                        var textRange = sel.createRange();
                        var preCaretTextRange = doc.body.createTextRange();
                        preCaretTextRange.moveToElementText(element);
                        preCaretTextRange.setEndPoint('EndToEnd', textRange);
                        caretOffset = preCaretTextRange.text.length;
                    }
                    scope.cursorPosition = caretOffset;
                    return caretOffset;
                }

                element.on('keyup keydown cut copy paste', function (event) {
                    scope.$evalAsync(read);
                    handleEditorChangeEvent(event);
                });

                element.on('blur', function (event) {
                    if (event.relatedTarget !== null) {
                        return;
                    }
                    scope.hashtags = null;
                    scope.mentions = null;
                });

                scope.autocompleteMention = function (mention) {
                    var text = element.html();
                    var textToReplace = formatTagHtml('mention', scope.currentMention);
                    var replacementText = formatTagHtml('mention', mention.mentionID.replace(' ', ''), '#/contacts/' + mention.id);
                    var n = text.lastIndexOf(textToReplace);
                    text = text.slice(0, n) + text.slice(n).replace(textToReplace, replacementText);
                    element.html(text);
                    element.trigger('focus');
                    setCaretToEnd();
                    $compile(element.contents())(scope);
                    scope.mentions = null;
                    scope.tagDictionary[mention.mentionID.replace(' ', '')] = mention;
                    if (scope.addedMentions.indexOf(mention.mentionID.replace(' ', '')) === -1) {
                        scope.addedMentions.push(mention.mentionID.replace(' ', ''));
                    }
                }

                function getMentionsList() {
                    if (scope.currentMention && model.$viewValue.indexOf(scope.currentMention) === -1) {
                        scope.mentions = null;
                        scope.currentMention = null;
                        return;
                    }

                    var caretPosition = getCaretCharacterOffsetWithin(element[0]);
                    var mentionResults = scope.mentionExpression.exec(model.$viewValue);
                    if (mentionResults) {
                        var substr = mentionResults.input.substr(mentionResults.index, caretPosition);
                        if (/\s/g.test(substr)) {
                            scope.mentions = null;
                            scope.currentMention = null;
                            return;
                        }
                        scope.currentMention = mentionResults[0].trim();
                        mentionsService.getMentions(scope.currentMention.toLowerCase()).then(function (result) {
                            scope.mentions = result.resources;
                        });
                    }
                }

                scope.autocompleteHashtag = function (hashtag) {
                    var text = element.html();
                    var textToReplace = formatTagHtml('hashtag', scope.currentHashtag);
                    var replacementText = formatTagHtml('hashtag', hashtag.tagName.replace(' ', ''), '#/hashtag/' + hashtag.tagName.replace('#', ''));
                    var n = text.lastIndexOf(textToReplace);
                    text = text.slice(0, n) + text.slice(n).replace(textToReplace, replacementText);
                    element.html(text);
                    element.trigger('focus');
                    setCaretToEnd();
                    $compile(element.contents())(scope);
                    scope.hashtags = null;
                    scope.tagDictionary[hashtag.tagName.replace(' ', '')] = hashtag;
                    if (scope.addedTags.indexOf(hashtag.tagName.replace(' ', '')) === -1) {
                        scope.addedTags.push(hashtag.tagName.replace(' ', ''));
                    }
                }

                function getHashtagsList() {
                    if (scope.currentHashtag && model.$viewValue.indexOf(scope.currentHashtag) === -1) {
                        scope.hashtags = null;
                        scope.currentHashtag = null;
                    }

                    var caretPosition = getCaretCharacterOffsetWithin(element[0]);
                    var hashtagResults = scope.hashtagExpression.exec(model.$viewValue);
                    if (hashtagResults) {
                        var substr = hashtagResults.input.substr(hashtagResults.index, caretPosition);
                        if (/\s/g.test(substr)) {
                            scope.hashtags = null;
                            scope.currentHashtag = null;
                            return;
                        }
                        scope.currentHashtag = hashtagResults[0];
                        hashtagService.getTags(scope.currentHashtag.toLowerCase()).then(function (result) {
                            scope.hashtags = result;
                        });
                    }
                }

                function processInput() {
                    var editor = element[0];
                    var rSel = rangy.getSelection().saveCharacterRanges(editor);
                    var formatted;

                    getMentionsList();
                    getHashtagsList();

                    var text = rawText(element.html());

                    formatted = text;
                    for (var i = 0; i < scope.settings.highlight.length; i++) {
                        formatted = formatted.replace(scope.settings.highlight[i].match, function (match) {
                            var tagMatch = scope.tagDictionary[match];
                            if (angular.isDefined(tagMatch)) {
                                return formatTagHtml(scope.settings.highlight[i]._class, match);
                            }
                            return formatTagHtml(scope.settings.highlight[i]._class, match);
                        });
                    }

                    $compile(element.html(formatted))(scope);
                    scope.previousText = text;
                    rangy.getSelection().restoreCharacterRanges(editor, rSel);
                }

                function formatTagHtml(tagClass, text) {
                    var span = '<span class="' + tagClass + '">' + text + '</span>';
                    return span;
                }

                function stripTags(string, allowed) {
                    return string.replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, function ($0, $1) { return '' });
                }

                function ltrim(str) {
                    return str.replace(/^[ \s\u00A0 ]+/g, '');
                }

                function rawText(str, trim) {
                    var text = str
                        .replace(/<br><div>/gi, '\n') //chrome not taking in account white-space:pre
                        .replace(/<div><br><\/div>/gi, '\n') //chrome newline
                        .replace(/<br>&nbsp;/gi, '\n\n') //mozilla newline
                        .replace(/<div>|<br>|<\/p>/gi, '\n'); //html tags
                    text = trim ? $.trim(stripTags(text)) : ltrim(stripTags(text));
                    return text;
                }


                scope.saveComment = function () {

                    var comment =
                    {
                        accessToken: [],
                        primaryMentionID: scope.primaryMentionId,
                        commentText: rawText(element.html()).replace('&nbsp;', ' '),
                        comment_Key: 0,
                        tags: scope.addedTags,
                        mentions: scope.addedMentions
                    };

                    commentsService.addComment(comment).then(function () {
                        if (scope.commentAddedCallback()) {
                            scope.commentAddedCallback()();
                        }
                        element.text('');
                        read();
                    });
                }
            }
        }
    }
})();
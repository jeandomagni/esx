﻿(function () {
    'use strict';

    angular.module('app').directive('bigNumberInput', bigNumberInput);

    bigNumberInput.$inject = [];

    function bigNumberInput() {
        return {
            restrict: 'EA',
            require: 'ngModel', // get a hold of NgModelController
            transclude: true,
            scope: {
                label: '@'
            },
            templateUrl: 'app/directives/bigNumberInput.html',
            link: function (scope, element, attrs, ngModel) {
                //scope.inputValue = null;

                ngModel.$formatters.push(function (modelValue) {
                    var inputValue = null;
                    var sizeValue = "";

                    if (modelValue != null) {
                        if (modelValue / 1000000000 >= 1.0) {
                            inputValue = modelValue / 1000000000;
                            sizeValue = "B";
                        } else if (modelValue / 1000000 >= 1.0) {
                            inputValue = modelValue / 1000000;
                            sizeValue = "MM";
                        } else if (modelValue / 1000 >= 1.0) {
                            inputValue = modelValue / 1000;
                            sizeValue = "K";
                        } else {
                            inputValue = modelValue;
                            sizeValue = "";
                        }
                    }

                    return { inputValue: inputValue, sizeValue: sizeValue };
                });

                ngModel.$render = function () {
                    scope.inputValue = ngModel.$viewValue.inputValue;
                    scope.sizeValue = ngModel.$viewValue.sizeValue;
                };

                scope.$watch('inputValue + sizeValue', function () {
                    ngModel.$setViewValue({
                        inputValue: scope.inputValue,
                        sizeValue: scope.sizeValue
                    });

                    ngModel.$setValidity('required', isFinite(scope.inputValue) && scope.inputValue != null);
                });

                ngModel.$parsers.push(function (viewValue) {
                    var inputValue = viewValue.inputValue;
                    if (inputValue != null) {
                        if (viewValue.sizeValue === 'K') {
                            inputValue = inputValue * 1000;
                        } else if (viewValue.sizeValue === "MM") {
                            inputValue = inputValue * 1000000;
                        } else if (viewValue.sizeValue === "B") {
                            inputValue = inputValue * 1000000000;
                        }
                    }
                    return inputValue;
                });
            }
        };
    }
}());

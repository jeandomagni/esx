﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.directive('contactsCard', function () {
        return {
            restrict: 'E',
            scope: {
                getContacts: '&',
                viewAllUrl: '@'
            },
            templateUrl: 'app/directives/contactsCard.html',
            link: function (scope) {
                scope.getContacts().then(function (result) {
                    scope.loaded = true;
                    scope.contacts = result.resources;
                    scope.count = result.pagingData.totalCount;
                });
            }
        };
    });

})();

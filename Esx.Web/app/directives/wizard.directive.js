﻿(function () {
    'use strict';

    angular.module('app').directive('wizard', wizard);

    wizard.$inject = ['$timeout', '$q'];

    function wizard($timeout, $q) {

        return {
            templateUrl: 'app/directives/wizard.html',
            restrict: 'EA',
            scope: {
                form: '='
            },
            link: function (scope) {
                if (!scope.form) throw 'You must pass a form object created by the wizard service into the "form" attribute of this directive.';

                scope.form.init();
                scope.progress = 0;
                scope.buffer = 0;
                scope.data = null;
                scope.refreshForm = refreshForm;
                scope.next = next;
                scope.previous = previous;
                scope.goToStep = goToStep;
                scope.finish = finish;
                scope.visibleSteps = visibleSteps;
                scope.safeApply = safeApply;

                var endWatch = null;
                var startWatch = function () {
                    // Watch for changes on the result object.
                    return scope.$watch('form.result', function (newValue, oldValue) {
                        if (newValue !== oldValue) {

                            // Enable jumping to this step from the step navigation now that the user has modified it.
                            scope.form.currentStep.isModified = true;

                            // Refresh continueAllowed, progress, and data based on the new values.
                            scope.refreshForm();

                            // Get the current question.
                            var q = scope.form.currentStep.currentQuestion;

                            // Flag it as being modified so any existing downsteam answers will be wiped out.
                            q.isModified = true;

                            // If the current question allows immediate continue,
                            // advance as soon as the condition is satisfied.
                            if (q.isImmediateContinue && q.continueAllowed) {

                                // md-select has a race condition if we advance too quickly after a selection is made.
                                $timeout(function () {
                                    scope.next();
                                }, 100);
                            }
                        }
                    }, true);
                };

                endWatch = startWatch();

                scope.refreshForm();

                // Allows question controllers to modify the result without triggering the watch.
                function safeApply(fn) {
                    if (fn) {
                        if (endWatch) endWatch();
                        fn();
                        endWatch = startWatch();
                        scope.refreshForm();
                    }
                }

                function next() {
                    if (scope.wizardForm && scope.wizardForm.$valid) {
                        if (scope.form.currentStep.currentQuestion.isWizardExit)
                            finish();
                        else if (scope.form.currentStep.currentQuestion.continueAllowed) {
                            safeApply(function () {
                                scope.form.clearDownstream();
                            });

                            $q.when(scope.form.next()).then(function () {
                                scope.refreshForm();
                                scope.wizardForm.$setPristine();
                            });
                        }
                    }
                };

                function previous() {
                    scope.form.previous();
                    scope.refreshForm();
                };

                function goToStep(step) {
                    scope.form.goToStep(step);
                    scope.refreshForm();
                };

                function finish() {
                    scope.form.completeCallback(scope.form.result);
                };

                function visibleSteps() {
                    var steps = [];
                    for (var i = 0; i < scope.form.steps.length; i++) {
                        var step = scope.form.steps[i];
                        if (step.showFn(scope.form.result, scope.form.meta)) steps.push(step);
                    }
                    return steps;
                };

                function refreshForm() {
                    var q = scope.form.currentStep.currentQuestion;
                    q.continueAllowed = q.continueAllowedFn(scope.form.result, scope.form.meta);
                    scope.data = q.data;
                    scope.progress = q.isWizardExit ? 99 : q.overallIndex / scope.form.allQuestions.length * 100;
                }
            }
        };
    }

}());
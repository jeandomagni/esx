﻿(function () {
    'use strict';

    angular.module('app').directive('fileUploader', fileUploader);

    fileUploader.$inject = ['Upload'];

    function fileUploader(upload) {
        return {
            restrict: 'EA',
            scope: {
                files: '='
            },
            templateUrl: 'app/directives/fileUploader.html',
            link: function (scope, element, attrs) {
                scope.files = scope.files || [];

                scope.uploadFile = function (file) {
                    if (file) {
                        upload.upload({
                            url: 'api/documents',
                            data: {
                                files: [file]
                            }
                        }).then(function (response) {
                            scope.files.push(response.data.document);
                            scope.progress = null;
                        }, function (response) {
                            if (response.status > 0) {
                                scope.errorMsg = response.status + ': ' + response.data;
                                scope.progress = null;
                            }
                        }, function (evt) {
                            scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }
                };

                scope.removeFile = function (index) {
                    scope.files.splice(index, 1);
                }
            }
        };
    }
}());




﻿describe('directives', function () {

    beforeEach(module('templates'));

    beforeEach(module('app'));

    beforeEach(module(function ($urlRouterProvider) {
        $urlRouterProvider.otherwise(function () { return false; });
    }));

    var $compile,
        $rootScope,
        $templateCache;

    beforeEach(inject(function (_$compile_, _$rootScope_, _$templateCache_) {
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $templateCache = _$templateCache_;

    }));

    describe('contactsCard', function () {
        var element;
        var fakeContats;

        beforeEach(function () {
            // Setup template

            // Arrange data
            fakeContats = {
                resources: [
                    { contactID: 1, firstName: 'One First', lastName: 'One Last', companyID: 3546345, emailAddress: 'someemail@1' },
                    { contactID: 2, firstName: 'Two', lastName: 'Two Last', companyID: 657845, emailAddress: 'someemail@2' }
                ],
                pagingData: {
                    totalCount: 2
                }
            };

            $rootScope.dealID = 123;
            $rootScope.getContacts = function () {
                return {
                    then: function (callback) {
                        return callback(fakeContats);
                    }
                };
            };

            element = angular.element('<contacts-card get-contacts="getContacts(dealID)" view-all-url="#/deals/{{dealID}}/contacts" />');

            $compile(element)($rootScope);
            $rootScope.$digest();
        });

        it('contact links are displayed', function () {
            //arrange
            var contactsP = element.find('p');
            for (var i = 0; i < contactsP.length; i++) {
                var links = $(contactsP[i]).find('a');
                //assert
                expect(links[0].getAttribute('href')).toBe('#/contacts/' + fakeContats.resources[i].contactID);
                expect(links[0].text).toBe(fakeContats.resources[i].firstName + ' ' + fakeContats.resources[i].lastName);
            }
        });

        it('company links are displayed', function () {
            //arrange
            var contactsP = element.find('p');
            for (var i = 0; i < contactsP.length; i++) {
                var links = $(contactsP[i]).find('a');
                //assert
                expect(links[1].getAttribute('href')).toBe('#/companies/' + fakeContats.resources[i].companyID);
            }
        });

        it('contact emails are displayed', function () {
            //arragne
            var contactsP = element.find('p');
            for (var i = 0; i < contactsP.length; i++) {
                var links = $(contactsP[i]).find('a');
                //assert
                expect(links[2].getAttribute('href')).toBe('mailto://' + fakeContats.resources[i].emailAddress);
            }
            expect(contactsP.length > 0).toBe(true);
        });

        it('view all link is displayed', function () {
            //arragne
            var links = element.find('a');
            //assert
            expect(links[0].getAttribute('href')).toBe('#/deals/123/contacts');
        });

    });

});
﻿describe('directives', function () {

    beforeEach(module('templates'));

    beforeEach(module('app'));

    // Ui-router conflicts with $httpbackend
    // fix found in http://stackoverflow.com/questions/23655307/ui-router-interfers-with-httpbackend-unit-test-angular-js
    beforeEach(module(function ($urlRouterProvider) {
        $urlRouterProvider.otherwise(function () { return false; });
    }));

    var $compile,
        $rootScope,
        $httpBackend,
        $templateCache;

    beforeEach(inject(function (_$compile_, _$rootScope_,  _$templateCache_) {
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $templateCache = _$templateCache_;

    }));

    describe('convertToNumber', function () {
        var element;

        beforeEach(function () {
            $rootScope.vm = { testId: 2 };

            element = angular.element(
                '<form name="testForm">' +
                    '<select name="testSelect" ng-model="vm.testId" convert-to-number>' +
                        '<option value="1">One</option>' +
                        '<option value="2">Two</option>' +
                    '</select>' +
                '</form>'
            );

            $compile(element)($rootScope);
            $rootScope.$digest();
        });

        it('view is in sync with model', function () {
            //Assert
            expect($rootScope.testForm.testSelect.$viewValue).toBe("2");
        });

        it('model is in sync with view', function () {
            //Act
            $rootScope.testForm.testSelect.$setViewValue("1");

            //Assert
            expect($rootScope.vm.testId).toBe(1);
        });
      
    });

    describe('contactsCard', function () {
        var element;
        var fakeContats;

        beforeEach(function () {
            // Setup template

            // Arrange data
            fakeContats = {
                resources: [
                    { contactID: 1, firstName: 'One First', lastName: 'One Last', companyID: 3546345, emailAddress: 'someemail@1' },
                    { contactID: 2, firstName: 'Two', lastName: 'Two Last', companyID: 657845, emailAddress: 'someemail@2' }
                ],
                pagingData: {
                    totalCount: 2
                }
            };

            $rootScope.dealID = 123;
            $rootScope.getContacts = function () {
                return {
                    then: function (callback) {
                        return callback(fakeContats);
                    }
                };
            };

            element = angular.element('<contacts-card get-contacts="getContacts(dealID)" view-all-url="#/deals/{{dealID}}/contacts" />');

            $compile(element)($rootScope);
            $rootScope.$digest();
        });

        it('contact links are displayed', function () {
            //arrange
            var contactsP = element.find('p');
            for (var i = 0; i < contactsP.length; i++) {
                var links = $(contactsP[i]).find('a');
                //assert
                expect(links[0].getAttribute('href')).toBe('#/contacts/' + fakeContats.resources[i].contactID);
                expect(links[0].text).toBe(fakeContats.resources[i].firstName + ' ' + fakeContats.resources[i].lastName);
            }
        });

        it('company links are displayed', function () {
            //arrange
            var contactsP = element.find('p');
            for (var i = 0; i < contactsP.length; i++) {
                var links = $(contactsP[i]).find('a');
                //assert
                expect(links[1].getAttribute('href')).toBe('#/companies/' + fakeContats.resources[i].companyID);
            }
        });

        it('contact emails are displayed', function () {
            //arragne
            var contactsP = element.find('p');
            for (var i = 0; i < contactsP.length; i++) {
                var links = $(contactsP[i]).find('a');
                //assert
                expect(links[2].getAttribute('href')).toBe('mailto://' + fakeContats.resources[i].emailAddress);
            }
            expect(contactsP.length > 0).toBe(true);
        });

        it('view all link is displayed', function () {
            //arragne
            var links = element.find('a');
            //assert
            expect(links[0].getAttribute('href')).toBe('#/deals/123/contacts');
        });

    });

    describe('propertiesCard', function () {
        var element;
        var fakeProperties;
        var selectedPropertyId;

        beforeEach(function () {

            // Arrange data
            fakeProperties = {
                resources: [
                    { propertyID: 1, propertyName: 'One' },
                    { propertyID: 2, propertyName: 'Two' }
                ],
                pagingData: {
                    totalCount: 2
                }
            };

            $rootScope.dealID = 123;
            $rootScope.getProperties = function () {
                return {
                    then: function (callback) {
                        return callback(fakeProperties);
                    }
                };
            };
            $rootScope.selectProperty = function (property) {
                selectedPropertyId = property.propertyID;
            };

            element = angular.element('<properties-card get-properties="getProperties(dealID)" on-select="selectProperty(property)" />');

            $compile(element)($rootScope);
            $rootScope.$digest();
        });

        it('property links are displayed', function () {
            var propertiesP = element.find('p');
            for (var i = 0; i < propertiesP.length; i++) {
                var links = $(propertiesP[i]).find('a');
                //assert
                expect(links[0].getAttribute('href')).toBe('#/properties/' + fakeProperties.resources[i].propertyID);
                expect(links[0].text).toBe(fakeProperties.resources[i].propertyName);
            }

        });

        it('first property selected', function () {
            //Assert
            expect(selectedPropertyId).toBe(1);
        });
    });
});
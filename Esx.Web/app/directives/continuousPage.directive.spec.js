﻿(function () {
    'use strict';

    beforeEach(module('templates'));

    beforeEach(module('app'));

    beforeEach(module(function ($urlRouterProvider) {
        $urlRouterProvider.otherwise(function () { return false; });
    }));

    describe('continuousPage.directive', function () {
        var element, scope, compile, html;
        var fakePagingData = {
            pageSize: 10, offset: 0
        };
        var fakeData = {
            resources: [{ name: "fakeData" }],
            pagingData: fakePagingData
        };

        var fakeQueryFunction = function () {
            return fakeData;
        };

        var fakeQueryObject = {};


        beforeEach(inject(function ($rootScope, $compile, $httpBackend, $templateCache) {
            scope = $rootScope;
            compile = $compile;

            var template = $templateCache.get('app/directives/continuousPage.html');
            $templateCache.put('app/directives/continuousPage.html', template);

            scope.queryFunction = fakeQueryFunction;
            scope.queryObject = fakeQueryObject;
            scope.pagingData = fakePagingData;

        }));

        describe('init', function () {

            beforeEach(function () {
                var markup = '<div id="contentSection" style="overflow: scroll; height: 10px;"><ul continuous-page   query-function="queryFunction" query-object="queryObject" paging-data-watch="pagingData" ><p></p><ul></div>';
                angular.element(document.body).append(markup);
                element = compile(markup)(scope);

                scope.$digest();
                html = element.html();

            });

            it('should call the query function with the expectd paging data', function () {

                // arrange

                scope.queryObject = {
                    offset: 0,
                    pageSize: 30
                };

                spyOn(scope, 'queryFunction');

                var div = angular.element(document.querySelector('#contentSection'));
                div[0].scrollTop = 10;

                // act
                div.triggerHandler('scroll');

                //assert
                expect(scope.queryFunction.calls.count()).toEqual(1);
                expect(scope.queryObject.pageSize).toEqual(30);
                expect(scope.queryObject.offset).toEqual(30);

            });

            afterEach(function() {
                angular.element(document.querySelector('#contentSection')).remove();
            });
        });
    });
}());

﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.directive('propertySelector', ['propertiesService', function (propertiesService) {

        return {
            templateUrl: 'app/directives/propertySelector.html',
            restrict: 'EA',
            scope: {
                mode: '@', // 'single' or 'multiple'
                context: '@',
                contextId: '@',
                raiseComplete: '&onComplete'
            },
            link: function (scope, element, attrs, ngModel) {
                scope.properties = [];

                scope.map = {
                    center: { latitude: 37.7773664, longitude: -99.7175739 },
                    zoom: 3
                };

                scope.options = {
                    
                    
                };

                scope.addProperty = function(property) {
                    scope.properties.push(property);
                    if (scope.mode === 'single') scope.done();
                };

                scope.done = function () {
                    scope.raiseComplete({ properties: properties });
                    console.log('done');
                };

                function init() {
                    //scope.map = new google.maps.Map(element.find("#map")[0], {
                    //    zoom: 3,
                    //    center: new google.maps.LatLng(37.7773664, -99.7175739),
                    //    mapTypeControlOptions: {
                    //        mapTypeIds: [google.maps.MapTypeId.SATELLITE, 'map_style']
                    //    }
                    //});
                    //scope.map.mapTypes.set('map_style', new google.maps.StyledMapType(mapOptions.styles, { name: "Roads" }));
                    //scope.map.setMapTypeId('map_style');

                    //google.maps.event.addListenerOnce(scope.map, 'idle', function () {
                    //    google.maps.event.trigger(scope.map, 'resize');
                    //});
                }

            }
        };
    }]);

}());
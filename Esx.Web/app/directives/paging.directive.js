﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.directive('pagination', function () {

        return {
            templateUrl: 'app/directives/paging.html',
            restrict: 'E',
            scope: {
                queryFunction: '&queryFunction',
                queryObject: '&queryObject',
                pagingDataWatch: '&pagingDataWatch',
                maxSize: '=maxsize'
            },
            link: function (scope) {
                scope.paging = {
                    total: 0,
                    pageCount: 0,
                    currentPage: 0,
                    offset: 0,
                    pageSize: 10
                };

                scope.$watch(scope.pagingDataWatch, function (newValue) {
                    if (!newValue) {
                        return;
                    }
                    
                    scope.paging.total = newValue.totalCount;
                    scope.paging.currentPage = (newValue.offset / newValue.pageSize) + 1;
                    scope.paging.pageSize = newValue.pageSize;
                    scope.paging.sort = newValue.sort;
                });

                scope.goToPage = function (number) {
                    scope.paging.number = number;
                    scope.paging.offset = (scope.paging.number - 1) * scope.paging.pageSize;
                    runQuery();
                };


                function runQuery() {
                    scope.queryObject().pageSize = scope.paging.pageSize;
                    scope.queryObject().offset = scope.paging.offset;
                    scope.queryObject().sort = scope.paging.sort;
                    scope.queryFunction()();
                }
            }
        };
    });

}());
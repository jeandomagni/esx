﻿(function () {
    'use strict';

    beforeEach(module('templates'));

    beforeEach(module('app'));

    beforeEach(module(function ($urlRouterProvider) {
        $urlRouterProvider.otherwise(function () { return false; });
    }));

    describe('paging.directive', function () {
        var element, scope, compile, html;
        var fakePagingData = {
            limit: 10, totalCount: 100, offset: 0
        };
        var fakeData = {
            resources: [{ name: "fakeData" }],
            pagingData: fakePagingData
        };

        var fakeQueryFunction = function () {
            return fakeData;
        };

        var fakeQueryObject = {};


        beforeEach(inject(function ($rootScope, $compile, $httpBackend, $templateCache) {
            scope = $rootScope;
            compile = $compile;
            //$httpBackend.whenGET(/.*.html/).respond(200, '');

            var template = $templateCache.get('app/directives/paging.html');
            $templateCache.put('app/directives/paging.html', template);

            scope.queryFunction = fakeQueryFunction;
            scope.queryObject = fakeQueryObject;
            scope.pagingData = fakePagingData;
        }));

        describe('init', function () {

            beforeEach(function () {

                //arrange
                element = compile('<pagination query-function="queryFunction" query-object="queryObject" paging-data-watch="pagingData"></pagination>')(scope);

                //act
                scope.$digest();
                html = element.html();

            });

            it('should do something', function() {

                expect(html).toBeDefined();
                var div = element.find('div');
                expect(div.length).toBe(2);

            });
        });
    });
}());

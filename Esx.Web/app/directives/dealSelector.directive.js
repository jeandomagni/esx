﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.directive('dealSelector', ['dealsService', 'coreType', function (dealsService, coreType) {

        return {
            templateUrl: 'app/directives/dealSelector.html',
            require: 'ngModel',
            restrict: 'EA',
            scope: {
                context: '@',
                contextId: '@',
                raiseDealSelected: '&onDealSelected'
            },
            link: function (scope, element, attrs, ngModel) {
                scope.coreType = coreType;

                scope.searchDeals = function (searchText) {
                    //TODO: Allow searching of deals within a context (coreType) or via a specific core object id.
                    return dealsService.getDeals({ searchText: searchText, sort: "StatusDate", sortDescending: true }).then(function (result) {
                        return result.resources;
                    });
                };

                scope.getDealFromProperty = function(properties) {
                    var p = properties[0];

                };

                scope.selectedDealChange = function (item) {
                    scope.deal = item;
                    ngModel.$setViewValue(item);
                    scope.raiseDealSelected({ deal: item });
                };

                ngModel.$render = function () {
                    scope.deal = ngModel.$viewValue;
                };
            }
        };
    }]);

}());
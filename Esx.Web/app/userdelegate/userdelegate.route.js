﻿
(function () {
    'use strict';

    var app = angular.module('app');

    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider'];

    function routeConfigurator($stateProvider) {

        $stateProvider
            .state('userdelegates', {
                url: '/userdelegates',
                templateUrl: 'app/userdelegate/userdelegates.html',
                controller: 'userdelegatesController',
                controllerAs: 'vm',
                data: {
                    title: 'user delegates',
                    defaultSearch: 'user delegates',
                    sidebaricon: 'user delegates',
                    includeInSideBar: true,
                    subnavClass: 'deals-bg'
                }
            });
    }
})();
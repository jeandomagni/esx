﻿(function () {
    'use strict';

    var serviceId = 'userdeletegatesService';

    angular.module('app').factory(serviceId, userdeletegatesService);

    userdeletegatesService.$inject = ['uriService', '$resource', '$http'];

    function userdeletegatesService(uriService, $resource, $http) {
        var service = {
            getUsers: getUsers,
            setIsDelegated: setIsDelegated,
            getDelegatedUsers: getDelegatedUsers,
            impersonateUser: impersonateUser,
            clear: clear
        };

        return service;

        function getUsers(query) {
            return $resource(uriService.userList).get(query).$promise;
        }
        function setIsDelegated(id, isDelegated) {
            return $http.put(uriService.userIsDelegated, { userKey: id, isDelegated: isDelegated }).$promise;
        }
        function getDelegatedUsers() {
            return $resource(uriService.delegatedUsers).query().$promise;
        }
        function impersonateUser(user) {
            return $resource(uriService.impersonateUser).save(user).$promise;
        }
        function clear() {
            return $resource(uriService.clearImpersonation).save().$promise;
        }
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'userdelegatesController';

    angular
      .module('app')
        .controller(controllerId, userdelegatesController);

    userdelegatesController.$inject = ['common', 'userdeletegatesService', '_', '$state', '$timeout'];

    function userdelegatesController(common, userdeletegatesService, _, $state, $timeout) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.searchText = '';

        vm.getUsers = _.debounce(getUsers, 200);
        vm.searchUsers = _.debounce(searchUsers, 200);
        vm.toggleDelegatedUser = toggleDelegatedUser;
        vm.backToPreviousPage = backToPreviousPage;

        vm.queryObject = {
            offset: 0,
            pageSize: 20
        };

        vm.userList = {
            resources: [],
            pagingData: {
                pageSize: 30,
                offset: 0
            },
        }

        function activate() {
            common.activateController([getUsers()], controllerId)
                .then(function () { log('Activated Users.'); });
        }

        function getUsers() {
            return userdeletegatesService.getUsers(vm.queryObject).then(function (userList) {
                vm.userList.resources = vm.userList.resources.concat(userList.resources);
                return vm.userList;
            });
        }

        function searchUsers() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.userList.resources.length = 0;
                vm.queryObject.offset = 0;
                getUsers();
            }
        }
        function toggleDelegatedUser(user, event) {
            $timeout(function () {
                user.isDelegatedUser = !user.isDelegatedUser;
                userdeletegatesService.setIsDelegated(user.userKey, user.isDelegatedUser);
            });
        }
        function backToPreviousPage() {
            common.$window.history.back();
        }
        activate();
    }
})();
﻿describe('mentions.service', function () {

    beforeEach(module('app'));

    var mentionsService,
        uriService,
        $httpBackend,
        coreTypes,
        delimeter;

    beforeEach(inject(function (_mentionsService_, _uriService_, _$httpBackend_) {

        mentionsService = _mentionsService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        uriService.mentions = 'api/mentions';
        delimeter = '|';


        // ignore html templates. Not sure if this is a good approach. 
        //$httpBackend.whenGET(/.*.html/).respond(200, '');

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getMentions', function () {

        it('should make GET request with query', function () {

            //arrange
            var mention = '@something';

            var expectedUri = uriService.mentions + '?query=' + mention;

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond({});

            //act
            mentionsService.getMentions(mention);

            //assert
            $httpBackend.flush();

        });
    });

    describe('types', function () {

        it('should have the core types', function () {

            //act
            var actual = mentionsService.types();

            //assert
            expect(actual).toBeDefined();

        });

        it('should make GET request for the core types', function () {

            ////arrange
            //var mention = '@something';

            //var expectedUri = uriService.mentions + '?query=' + mention;

            //$httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond({});

            ////act
            //mentionsService.getMentions(mention);

            ////assert
            //$httpBackend.flush();

        });
    });

    describe('parse', function () {

        beforeEach(function() {

            coreTypes = mentionsService.types();

        });
        
        it('should remove the delimeter and type: from the mention', function () {
            //arrange
            for (var type in coreTypes) {
                if (coreTypes.hasOwnProperty(type)) {
                    var expectedMention = '@mention';
                    var fullMention = expectedMention + delimeter + coreTypes[type].type;

                    //act
                    var actual = mentionsService.parse(fullMention);

                    //assert
                    expect(actual.text).toEqual(expectedMention);
                }
            }

        });

        it('should remove the last delimeter and type from the mention', function () {

            //arrange
            for (var type in coreTypes) {
                if (coreTypes.hasOwnProperty(type)) {

                    var expectedMention = '@mention' + delimeter + delimeter + 'more text';
                    var fullMention = expectedMention + delimeter + coreTypes[type].type;

                    //act
                    var actual = mentionsService.parse(fullMention);

                    //assert
                    expect(actual.text).toEqual(expectedMention);
                }
            }

        });

        it('should set the type char for each mention type', function () {

            //arrange
            for (var type in coreTypes) {
                if (coreTypes.hasOwnProperty(type)) {
                    var expectedType = coreTypes[type].type;
                    var fullMention = '@mention' + delimeter + expectedType;

                    //act
                    var actual = mentionsService.parse(fullMention);

                    //assert
                    expect(actual.type).toEqual(expectedType);
                }
            }
        });

        it('should set the uri for each type', function () {

            //arrange
            for (var type in coreTypes) {
                if (coreTypes.hasOwnProperty(type)) {
                    var mentionText = '@mention';
                    var expectedUri = coreTypes[type].uri + mentionText;
                    var fullMention = mentionText + delimeter + coreTypes[type].type;

                    //act
                    var actual = mentionsService.parse(fullMention);

                    //assert
                    expect(actual.uri).toEqual(expectedUri);
                }
            }
        });

        it('should return the mention with empty uri and empty type when delimenter is not present', function () {

                //arrange
                var fullMention = '@mention';
                var expected = {
                    text: fullMention,
                    type: '',
                    uri: ''
                }

                //act
                var actual = mentionsService.parse(fullMention);

                //assert
                expect(actual).toEqual(expected);
        });
    });

    describe('appendType', function() {

        it('should add the type and delimeter to the end of the mention', function () {

            //arrange
            var mention = '@Something';
            var type = 'A';
            var expected = mention + delimeter + type;

            //act
            var actual = mentionsService.appendType(mention, type);

            //assert
            expect(actual).toEqual(expected);
        });

    });

    describe('setIsWatched', function () {
        it('should call server with setIsWatched uri', function () {

            // Arrange
            var mentionId = "@test";
            var isWatched = true;
            $httpBackend.expectPUT('api/mentions/setIsWatched', { mentionID: mentionId, isWatched: isWatched }).respond({});

            // Act
            mentionsService.setIsWatched(mentionId, isWatched);

            // Assert
            $httpBackend.flush();
        });
    });
});
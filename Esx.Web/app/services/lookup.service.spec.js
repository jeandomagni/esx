﻿describe('lookup.service', function () {
    beforeEach(module('app'));

    var lookupService;

    beforeEach(inject(function (_lookupService_) {
        lookupService = _lookupService_;
    }));

    describe('categories', function () {

        it('should have categories', function () {
            expect(lookupService.contactCategories).toBeDefined();
        });

        it('should have the correct number of categories', function () {
            expect(lookupService.contactCategories.length).toBe(8);
        });
      
    });
});
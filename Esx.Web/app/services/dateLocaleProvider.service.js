﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.config(dateLocaleProvider);

      dateLocaleProvider.$inject = ['$mdDateLocaleProvider'];

      function dateLocaleProvider($mdDateLocaleProvider) {
        $mdDateLocaleProvider.formatDate = function (date) {
            if (date == undefined) return '';
            return moment(date).format('MM/DD/YYYY');
        };
        $mdDateLocaleProvider.parseDate = function (dateString) {
            if (dateString == undefined) return '';
            var m = moment(dateString, 'MM/DD/YYYY', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };
    }
})();
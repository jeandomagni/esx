﻿(function () {
    'use strict';

    var serviceId = 'lookupService';
    angular.module('app').factory(serviceId, lookupService);

    function lookupService() {

        var service = {
            contactCategories: [
                { id: 1, description: '---' },
                { id: 2, description: 'Investor' },
                { id: 3, description: 'Lender' },
                { id: 4, description: 'Broker' },
                { id: 5, description: 'Offshore' },
                { id: 6, description: 'Advisor' },
                { id: 7, description: 'Developer' },
                { id: 8, description: 'Operator' }
            ]
        };

        return service;
    }
})();

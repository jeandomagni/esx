﻿(function () {
    'use strict';

    var serviceId = 'mapsService';
    angular
        .module('app')
        .service(serviceId, mapsService);

    mapsService.$inject = [];

    function mapsService() {

        this.setMapExtent = function(markers) {
            var newExtent = new google.maps.LatLngBounds();
            return markers.reduce(function (extent, marker) {
                return newExtent.extend(marker.getPosition());
            }, new google.maps.LatLngBounds());

        }

        this.getBoundingBox = function (bounds) {

            var northEastLat = bounds.getNorthEast().lat();
            var northEastLng = bounds.getNorthEast().lng();

            var southWestLat = bounds.getSouthWest().lat();
            var southWestLng = bounds.getSouthWest().lng();

            var northWestLat = bounds.getNorthEast().lat();
            var northWestLng = bounds.getSouthWest().lng();

            var southEastLat = bounds.getSouthWest().lat();
            var southEastLng = bounds.getNorthEast().lng();

            var wkt = 'POLYGON ((' +
                northEastLng +
                ' ' +
                northEastLat +
                ',' +
                northWestLng +
                ' ' +
                northWestLat +
                ',' +
                southWestLng +
                ' ' +
                southWestLat +
                ',' +
                southEastLng +
                ' ' +
                southEastLat +
                ',' +
                northEastLng +
                ' ' +
                northEastLat +
                '))';
            return {
                coordinates: {
                    nw: { lat: northWestLat, lng: northWestLng },
                    ne: { lat: northEastLat, lng: northEastLng },
                    se: { lat: southEastLat, lng: southEastLng },
                    sw: { lat: southWestLat, lng: southWestLng }
                },
                wellKnownText: wkt
            }; 

        }


    }
})();

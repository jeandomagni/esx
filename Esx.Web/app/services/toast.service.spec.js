﻿
describe('toast.service', function () {
    beforeEach(module('app'));

    var toastService;

    beforeEach(inject(function (_toastService_) {

        toastService = _toastService_;

        //jquery plugin mock. 
        toastr = {
            info: function() {},
            error: function () { },
            warning: function () { },
            success: function () { }
        };

    }));

    describe('info', function () {

        it('should have a info method', function () {
            expect(toastService.info).toBeDefined();
        });

        it('should add info to toast', function () {

            //arrange
            var expectedMessage = 'some message';

            spyOn(toastr, 'info');

            //act
            toastService.info(expectedMessage);

            //assert
            expect(toastr.info).toHaveBeenCalledWith(expectedMessage);
        });
    });

    describe('error', function () {

        it('should have a error method', function () {
            expect(toastService.info).toBeDefined();
        });

        it('should add error to toast', function () {

            //arrange
            var expectedMessage = 'some message';

            spyOn(toastr, 'error');

            //act
            toastService.error(expectedMessage);

            //assert
            expect(toastr.error).toHaveBeenCalledWith(expectedMessage);
        });
    });

    describe('warning', function () {

        it('should have a warning method', function () {
            expect(toastService.warning).toBeDefined();
        });

        it('should add warning to toast', function () {

            //arrange
            var expectedMessage = 'some message';

            spyOn(toastr, 'warning');

            //act
            toastService.warning(expectedMessage);

            //assert
            expect(toastr.warning).toHaveBeenCalledWith(expectedMessage);
        });
    });

    describe('success', function () {

        it('should have a success method', function () {
            expect(toastService.success).toBeDefined();
        });

        it('should add success to toast', function () {

            //arrange
            var expectedMessage = 'some message';

            spyOn(toastr, 'success');

            //act
            toastService.success(expectedMessage);

            //assert
            expect(toastr.success).toHaveBeenCalledWith(expectedMessage);
        });
    });

});
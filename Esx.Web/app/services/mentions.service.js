﻿(function () {
    'use strict';

    var serviceId = 'mentionsService';
    angular.module('app').factory(serviceId, mentionsService);

    mentionsService.$inject = ['$resource', 'uriService', '$http', 'coreType'];

    function mentionsService($resource, uriService, $http, coreType) {

        var delimeter = '|';
        var mentionRegex = /\B@\w+\|{1}\w{1}/g;
        
        var service = {
            getMentions: getMentions,
            getMentions2: getMentions2,
            types: getCoreTypesWithRoutes,
            parse: parse,
            appendType: appendType,
            mentionRegex: mentionRegex,
            delimeter: delimeter,
            setIsWatched: setIsWatched,
            addMentionReminder: addMentionReminder
        };

        return service;

        function appendType(mention, type) {
            return mention + delimeter + type;
        }

        function parse(fullMention) {

            if (fullMention.lastIndexOf(delimeter) < 0) {
                return {
                    text: fullMention,
                    type: '',
                    uri: ''
                };
            }

            var mentionData = {
                text: fullMention.substr(0, fullMention.lastIndexOf(delimeter)),
                type: fullMention.substr(fullMention.lastIndexOf(delimeter)+1, fullMention.length)
            };
            mentionData.uri = getMentionUri(mentionData.type, mentionData.text);

            return mentionData;
        };

        function getMentionUri(type, mention) {
            var coreTypes = getCoreTypesWithRoutes();
            for (var key in coreTypes) {
                if (coreTypes.hasOwnProperty(key) && 
                    coreTypes[key].type.toLowerCase() === type.toLowerCase()) {
                    return coreTypes[key].uri + mention;
                }
            }
            return '';
        }

        function getCoreTypesWithRoutes() {
            //TODO should we tie these to actual states somehow?
            return {
                company: { type: coreType.company, uri: '#/companies/' },
                contact: { type: coreType.contact, uri: '#/contacts/' },
                deal: { type: coreType.deal, uri: '#/deals/' },
                pitch: { type: coreType.pitch, uri: '#/pitches/' },
                employee: { type: coreType.employee, uri: '#/employees/' },
                loan: { type: coreType.loan, uri: '#/loans/' },
                property: { type: coreType.property, uri: '#/properties/' }
            };
        }

        function getMentions(mention) {
            return $resource(uriService.mentions).get({ query: mention }).$promise;
        }

        function getMentions2(query, coreObjectType) {
            return $resource(uriService.mentions + '/list').get({ searchText: query, coreObjectType: coreObjectType }).$promise;
        }

        function setIsWatched(id, isWatched) {
            return $http.put(uriService.mentionsIsWatched, { mentionID: id, isWatched: isWatched }).$promise;
        }

        function addMentionReminder(reminder) {
            return $http.post(uriService.mentionsAddReminder, reminder).$promise;
        }
    }
})();
﻿(function () {
    "use strict";

    angular.module("app").service("fileService", fileService);
    
    fileService.$inject = ["$location"];
        
    function fileService($location) {

        return {
            getFile: getFile
        };
        
        function getFile(file) {
            var suffix = '#' + $location.url();
            return $location.absUrl().replace(suffix, '') + 'api/documents/download/' + file;
        }           
    };
})();
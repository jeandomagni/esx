﻿describe('uri.service', function () {
    beforeEach(module('app'));

    var uriService;

    beforeEach(inject(function (_uriService_) {
        uriService = _uriService_;
    }));

    describe('contactList', function () {

        it('should have a contactList uri', function () {
            expect(uriService.contactsList).toBeDefined();
        });

        it('should have the correct contactList value', function () {
            expect(uriService.contactsList).toBe('api/contacts/list');
        });

    });

    describe('contactSearch', function () {

        it('should have a contactSearch uri', function () {
            expect(uriService.contactsSearch).toBeDefined();
        });

        it('should have the correct contactSearch value', function () {
            expect(uriService.contactsSearch).toBe('api/contacts/search');
        });
    });

    describe('companyIntellisense', function () {

        it('should have a companyIntellisense uri', function () {
            expect(uriService.companyIntellisense).toBeDefined();
        });

        it('should have the correct companyIntellisense value', function () {
            expect(uriService.companyIntellisense).toBe('api/companies/intellisense');
        });
    });

    describe('CompanyList', function () {

        it('should have a companyList uri', function () {
            expect(uriService.companyList).toBeDefined();
        });

        it('should have the correct companyList value', function () {
            expect(uriService.companyList).toBe('api/companies/list');
        });
    });

    describe('CompanyDetails', function () {

        it('should have a  Details uri', function () {
            expect(uriService.companyDetails).toBeDefined();
        });

        it('should have the correct company details value', function () {
            expect(uriService.companyDetails).toBe('api/companies/:mentionId/details');
        });
    });

    describe('dealsList', function () {

        it('should have a dealsList uri', function () {
            expect(uriService.dealsList).toBeDefined();
        });

        it('should have the correct dealsList value', function () {
            expect(uriService.dealsList).toBe('api/deals/list');
        });
    });

    describe('deal', function () {

        it('should have a deal uri', function () {
            expect(uriService.deal).toBeDefined();
        });

        it('should have the correct deal value', function () {
            expect(uriService.deal).toBe('api/deals/:mentionId');
        });
    });
    describe('dealSummary', function () {
        it('should have the correct deal summary value', function () {
            expect(uriService.dealSummary).toBe('api/deals/:mentionId/summary');
        });
    });

    describe('dealSearch', function () {

        it('should have a dealSearch uri', function () {
            expect(uriService.dealSearch).toBeDefined();
        });

        it('should have the correct dealSearch value', function () {
            expect(uriService.dealSearch).toBe('api/deals/search');
        });
    });

    describe('loan', function () {

        it('should have a loan uri', function () {
            expect(uriService.loan).toBeDefined();
        });

        it('should have the correct loan value', function () {
            expect(uriService.loan).toBe('api/loans/:mentionId');
        });
    });

    describe('propertyList', function () {

        it('should have a property uri', function () {
            expect(uriService.propertyList).toBeDefined();
        });

        it('should have the correct property value', function () {
            expect(uriService.propertyList).toBe('api/properties/list');
        });
    });

    describe('property', function () {

        it('should have a property uri', function () {
            expect(uriService.property).toBeDefined();
        });

        it('should have the correct property value', function () {
            expect(uriService.property).toBe('api/properties/:propertyId');
        });
    });

    describe('propertySearch', function () {

        it('should have a propertySearch uri', function () {
            expect(uriService.propertySearch).toBeDefined();
        });

        it('should have the correct propertySearch value', function () {
            expect(uriService.propertySearch).toBe('api/properties/search');
        });
    });

    describe('dealContacts', function () {

        it('should have a dealContacts uri', function () {
            expect(uriService.dealContacts).toBeDefined();
        });

        it('should have the correct dealContacts value', function () {
            expect(uriService.dealContacts).toBe('api/deals/:dealId/contacts');
        });
    });

    describe('dealProperties', function () {

        it('should have a dealProperties uri', function () {
            expect(uriService.dealProperties).toBeDefined();
        });

        it('should have the correct dealProperties value', function () {
            expect(uriService.dealProperties).toBe('api/deals/:dealId/properties');
        });
    });

    describe('dealBids', function () {

        it('should have a dealBids uri', function () {
            expect(uriService.dealBids).toBeDefined();
        });

        it('should have the correct dealBids value', function () {
            expect(uriService.dealBids).toBe('api/deals/:dealId/bids');
        });
    });

    describe('companyContacts', function () {

        it('should have a companyContacts uri', function () {
            expect(uriService.companyContacts).toBeDefined();
        });

        it('should have the correct companyProperties value', function () {
            expect(uriService.companyContacts).toBe('api/companies/:companyId/contacts');
        });
    });

    describe('companyProperties', function () {

        it('should have a companyProperties uri', function () {
            expect(uriService.companyProperties).toBeDefined();
        });

        it('should have the correct companyProperties value', function () {
            expect(uriService.companyProperties).toBe('api/companies/:companyId/properties');
        });
    });

    describe('companyLoans', function () {

        it('should have a companyLoans uri', function () {
            expect(uriService.companyLoans).toBeDefined();
        });

        it('should have the correct companyLoans value', function () {
            expect(uriService.companyLoans).toBe('api/companies/:companyId/loans');
        });
    });

    describe('companyDeals', function () {

        it('should have a companyDeals uri', function () {
            expect(uriService.companyDeals).toBeDefined();
        });

        it('should have the correct companyDeals value', function () {
            expect(uriService.companyDeals).toBe('api/companies/:companyId/deals');
        });
    });

    describe('propertyCompanies', function () {

        it('should have a propertyCompanies uri', function () {
            expect(uriService.propertyCompanies).toBeDefined();
        });

        it('should have the correct propertyCompanies value', function () {
            expect(uriService.propertyCompanies).toBe('api/properties/:propertyId/companies');
        });
    });

    describe('propertyDeals', function () {

        it('should have a propertyDeals uri', function () {
            expect(uriService.propertyDeals).toBeDefined();
        });

        it('should have the correct propertyDeals value', function () {
            expect(uriService.propertyDeals).toBe('api/properties/:mentionId/deals');
        });
    });

    describe('contactDeals', function () {

        it('should have a contactDeals uri', function () {
            expect(uriService.contactDeals).toBeDefined();
        });

        it('should have the correct contactDeals value', function () {
            expect(uriService.contactDeals).toBe('api/contacts/:contactId/deals');
        });
    });

    describe('loansList', function () {

        it('should have a loansList uri', function () {
            expect(uriService.loansList).toBeDefined();
        });

        it('should have the correct loansList value', function () {
            expect(uriService.loansList).toBe('api/loans/list');
        });
    });

    describe('employeesList', function () {

        it('should have a property uri', function () {
            expect(uriService.employeesList).toBeDefined();
        });

        it('should have the correct property value', function () {
            expect(uriService.employeesList).toBe('api/users');
        });
    });

    describe('employee', function () {

        it('should have a employee uri', function () {
            expect(uriService.employee).toBeDefined();
        });

        it('should have the correct employee value', function () {
            expect(uriService.employee).toBe('api/employees/:employeeId');
        });
    });

    describe('employeeSearch', function () {

        it('should have a employeeSearch uri', function () {
            expect(uriService.employeesSearch).toBeDefined();
        });

        it('should have the correct propertySearch value', function () {
            expect(uriService.employeesSearch).toBe('api/employees/search');
        });
    });

    describe('contact', function () {

        it('should have a contact uri', function () {
            expect(uriService.contact).toBeDefined();
        });

        it('should have the correct contact value', function () {
            expect(uriService.contact).toBe('api/contacts/:contactId');
        });
    });

    describe('company', function () {

        it('should have a company uri', function () {
            expect(uriService.company).toBeDefined();
        });

        it('should have the correct company value', function () {
            expect(uriService.company).toBe('api/companies/:companyId');
        });
    });

    describe('tagsList', function () {

        it('should have a tags list uri', function () {
            expect(uriService.tagsList).toBeDefined();
        });

        it('should have the correct hashtag value', function () {
            expect(uriService.tagsList).toBe('api/tags/list');
        });
    });

    describe('tagNotes', function () {

        it('should have a tag notes uri', function () {
            expect(uriService.tagNotes).toBeDefined();
        });

        it('should have the correct hashtag value', function () {
            expect(uriService.tagNotes).toBe('api/tags/notes');
        });
    });

    describe('mentions', function () {

        it('should have a mentions uri', function () {
            expect(uriService.mentions).toBeDefined();
        });

        it('should have the correct company value', function () {
            expect(uriService.mentions).toBe('api/mentions');
        });
    });

    describe('commentSave', function () {

        it('should have a commentSave uri', function () {
            expect(uriService.commentSave).toBeDefined();
        });

        it('should have the correct comment save value', function () {
            expect(uriService.commentSave).toBe('api/comments/save');
        });
    });

    describe('commentList', function () {

        it('should have a commentList uri', function () {
            expect(uriService.commentList).toBeDefined();
        });

        it('should have the correct comment list value', function () {
            expect(uriService.commentList).toBe('api/comments/list');
        });
    });

    describe('commentMentionedList', function () {

        it('should have a commentMentionedList uri', function () {
            expect(uriService.commentMentionedList).toBeDefined();
        });

        it('should have the correct comment mentioned list value', function () {
            expect(uriService.commentMentionedList).toBe('api/comments/mentioned/list');
        });
    });

    describe('companySummary', function () {
        it('should have the correct company summary value', function () {
            expect(uriService.companySummary).toBe('api/companies/:companyId/summary');
        });
    });
    describe('companySummaryStats', function () {
        it('should have the correct company summary stats value', function () {
            expect(uriService.companySummaryStats).toBe('api/companies/:companyId/summaryStats');
        });
    });

    describe('stateProvinces', function () {
        it('should have the correct state provinces value', function () {
            expect(uriService.stateProvinces).toBe('api/resources/stateProvinces');
        });
    });

    describe('countries', function () {
        it('should have the correct countries value', function () {
            expect(uriService.countries).toBe('api/resources/countries');
        });
    });

    describe('userPreferences', function () {
        it('should have the correct user preferences value', function () {
            expect(uriService.userPreferences).toBe('api/resources/userPreferences');
        });
    });

    describe('addAlias', function () {
        it('should have the correct addAlias value', function () {
            expect(uriService.addAlias).toBe('api/companies/addAlias');
        });
    });

    describe('summaryAlerts', function () {
        it('should have the correct countries value', function () {
            expect(uriService.summaryAlerts).toBe('api/alerts/:mentionId/summaryAlerts');
        });
    });

    describe('alertAcknowledge', function () {
        it('should have the correct countries value', function () {
            expect(uriService.alertAcknowledge).toBe('api/alerts/acknowledgeAlert');
        });
    });

    describe('alertRemove', function () {
        it('should have the correct user preferences value', function () {
            expect(uriService.alertRemove).toBe('api/alerts/removeAlert');
        });
    });

    describe('alertSnooze', function () {
        it('should have the correct addAlias value', function () {
            expect(uriService.alertSnooze).toBe('api/alerts/snoozeAlert');
        });
    });

    describe('propertyIntellisense', function () {

        it('should have a propertyIntellisense uri', function () {
            expect(uriService.propertyIntellisense).toBeDefined();
        });

        it('should have the correct propertyIntellisense value', function () {
            expect(uriService.propertyIntellisense).toBe('api/properties/intellisense');
        });
    });

    describe('search', function () {
        it('should have the correct search value', function () {
            expect(uriService.search).toBe('api/search');
        });
    });
    
    describe('userFullName', function () {
        it('should have the correct search value', function () {
            expect(uriService.userFullName).toBe('api/sidebar/username');
        });
    });

    describe('marketingListsLookupExisting', function () {
        it('should have the correct marketingListsLookupExisting value', function () {
            expect(uriService.marketingListsLookupExisting).toBe('api/marketinglists/lookupexisting');
        });
    });
});

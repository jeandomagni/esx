﻿(function () {
    'use strict';

    // Configure Toastr
    toastr.options.timeOut = 4000;
    toastr.options.positionClass = 'toast-bottom-right';

    var serviceId = 'toastService';
    angular.module('app').factory(serviceId, toastService);

    function toastService() {

        var service = {
            info: info,
            error: error,
            warning: warning,
            success: success
        };

        function info(message) {
            toastr.info(message);
        };

        function error(message) {
            toastr.error(message);
        };

        function warning(message) {
            toastr.warning(message);
        };

        function success(message) {
            toastr.success(message);
        };

        return service;
    }
})();

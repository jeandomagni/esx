(function () {
    'use strict';

    var serviceId = 'datacontext';
    angular.module('app').factory(serviceId, ['common', datacontext]);

    function datacontext(common) {
        var $q = common.$q;

        var service = {
            getMyDeals: getMyDeals,
            getWatchlist: getWatchlist
        };

        return service;

        function getMyDeals() {
            var deals = [
                { dealName: 'The Blackstone Chicago', dealType: 'Equity Sale', dealStatus: 'Marketing', currentBid: 'N/A' },
                { dealName: 'DEI Office Portfolio', dealType: 'Financing', dealStatus: 'Potential New Business', currentBid: 'N/A' },
                { dealName: 'Dover Mall', dealType: 'Equity Sale', dealStatus: 'Brochure Preparation', currentBid: 'N/A' },
                { dealName: 'Western Pacific DTLA', dealType: 'Equity Sale', dealStatus: 'Marketing', currentBid: 'N/A' },
                { dealName: 'ValueRock Hawaii Retail Portfolio', dealType: 'JV/Recap', dealStatus: 'On Hold', currentBid: 'N/A' },
                { dealName: 'Project Kodiak', dealType: 'Investment Banking', dealStatus: 'Brochure Preparation', currentBid: 'N/A' },
                { dealName: 'Dexter Horton Building', dealType: 'Equity Sale', dealStatus: 'Under Contract/LOI', currentBid: '$120MM' },
                //{ dealName: 'PacMutual (Financing)', dealType: 'Financing', dealStatus: 'Reviewing Term Sheets', currentBid: '$105MM' },
            ];
            return $q.when(deals);
        }

        function getWatchlist() {
            var watchList = [
                { name: 'Renaissance Square', status: 'Brochure Preparation', hasUpdate: true, style: { 'border-left': '5px solid green', 'margin-left': '-16px'} },
                { name: 'Brea Place', status: 'Potential New Business', hasUpdate: true, style: { 'border-left': '5px solid green', 'margin-left': '-16px' } },
                { name: '13th Floor Investments, LLC', status: 'Investment Banking', hasUpdate: false, style: { 'border-left': '5px solid lightcoral', 'margin-left': '-16px' } },
                { name: '114 Fifth Ave', status: 'Office-Portfolio', hasUpdate: false, style: { 'border-left': '5px solid rgb(100,181,246)', 'margin-left': '-16px' } },
                { name: 'City Center Plaza', status: '$156MM - 09 July 15', hasUpdate: false, style: { 'border-left': '5px solid teal', 'margin-left': '-16px' } },
            ];
            return $q.when(watchList);
        }
    }
})();
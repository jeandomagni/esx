﻿(function () {

    /////////////////////////////////
    // Most of this code was taken from https://gist.github.com/benmj/6380466
    /////////////////////////////////

    'use strict';

    var serviceId = 'geocodeService';
    angular.module('app').factory(serviceId, geocodeService);

    geocodeService.$inject = ['$q', '$timeout', 'uiGmapGoogleMapApi'];

    function geocodeService($q, $timeout, uiGmapGoogleMapApi) {

        var queue = [];
        // Amount of time (in milliseconds) to pause between each trip to the
        // Geocoding API, which places limits on frequency.
        var queryPause = 250;

        var service = {
            getLatLngForAddress: latLngForAddress
        };

        return service;

        function latLngForAddress(address) {
            var d = $q.defer();

            queue.push({
                address: address,
                d: d
            });

            if (queue.length === 1) {
                executeNext();
            }

            return d.promise;
        }

        /**
         * executeNext() - execute the next function in the queue. 
         *                  If a result is returned, fulfill the promise.
         *                  If we get an error, reject the promise (with message).
         *                  If we receive OVER_QUERY_LIMIT, increase interval and try again.
         */
        function executeNext() {

            uiGmapGoogleMapApi.then(function (maps) {
                var task = queue[0],
                    geocoder = maps.Geocoder.prototype;

                geocoder.geocode({ address: task.address }, function (result, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        var latLng = {
                            latitude: result[0].geometry.location.lat(),
                            longitude: result[0].geometry.location.lng()
                        };

                        queue.shift();

                        task.d.resolve(latLng);

                        if (queue.length) {
                            $timeout(executeNext, queryPause);
                        }
                    } else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                        queue.shift();
                        task.d.reject({
                            type: 'zero',
                            message: 'Zero results for geocoding address ' + task.address
                        });
                    } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                        queryPause += 250;
                        $timeout(executeNext, queryPause);
                    } else if (status === google.maps.GeocoderStatus.REQUEST_DENIED) {
                        queue.shift();
                        task.d.reject({
                            type: 'denied',
                            message: 'Request denied for geocoding address ' + task.address
                        });
                    } else if (status === google.maps.GeocoderStatus.INVALID_REQUEST) {
                        queue.shift();
                        task.d.reject({
                            type: 'invalid',
                            message: 'Invalid request for geocoding address ' + task.address
                        });
                    }
                });
            });
        };
    }
})();
﻿(function () {
    'use strict';

    var serviceId = 'utilsService';
    angular
        .module('app')
        .service(serviceId, utilsService);

    utilsService.$inject = ['$resource'];

    function utilsService($resource) {

        this.getBuildNumber = function () {

            var url = 'app/layout/build.txt';
            var resource = $resource(url, {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });
            return $resource(url).get().$promise;
        }

        this.scrubURL = function (url) {
            var protocol = 'http://';
            if (url.search("https://") !== -1) {
                protocol = 'https://';
                url = url.replace('https://', '');
            } else if (url.search("https:") !== -1) {
                protocol = 'https://';
                url = url.replace('https:', '');
            } else if (url.search("http:") !== -1) {
                url = url.replace('http:', '');
            } else if (url.search("http://") !== -1) {
                url = url.replace('http://', '');
            }
            return protocol + url;
        }


    }
})();

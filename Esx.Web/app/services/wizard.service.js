﻿(function () {
    'use strict';

    var serviceId = 'wizardService';
    angular.module('app').factory(serviceId, ['$http', '$templateCache', 'dialog', wizardService]);

    function wizardService($http, $templateCache, dialog) {

        var service = {
            /// <summary>Creates a new object that will hold all the steps, questions, and logic for navigating between them.</summary>
            /// <param name="templatesFolder">The path to the folder containing the templates (Should start with "app/" and end with a trailing slash).</param>
            /// <param name="detailsFile">The name of the template file that will display the users selections.</param>
            /// <param name="completeCallback">A callback function that will receive the result object as a parameter and will be called when the wizard is complete.</param>
            /// <returns>The object to bind to the wizard directive.</returns>
            createForm: function (templatesFolder, detailsFile, completeCallback) {
                return {
                    templatesFolder: templatesFolder,
                    detailsFile: detailsFile,
                    addStep: addStep,
                    steps: [],
                    allQuestions: [],
                    currentStep: null,
                    next: next,
                    previous: previous,
                    goToStep: goToStep,
                    completeCallback: completeCallback || function () { throw 'A completeCallback has not been specified for this wizard.'; },
                    result: {},
                    meta: {},
                    init: init,
                    clearDownstream: clearDownstream
                };
            }
        };

        return service;

        /// <summary>Adds a new step to the specified array.</summary>
        /// <param name="stepParameters">Object containing the step parameters.</param>
        /// <param name="stepParameters.label">The title text of the step.</param>
        /// <param name="stepParameters.isOptional">Shows "optional" subtitle and "Finish" button when true.</param>
        /// <param name="stepParameters.showFn">Function that receives the result object as a parameter and returns a boolean indicating if the step should be shown to the user.</param>
        /// <param name="stepParameters.data">Anything you'd like passed down to the question template.</param>
        /// <returns>The new step</returns>
        function addStep(stepParameters) {
            var form = this;

            var defaultParams = {
                label: null,
                isOptional: false,
                showFn: function () { return true; },
                data: null
            };

            var step = {
                form: form,
                edit: false,
                questions: [],
                currentQuestion: null,
                addQuestion: addQuestion,
                addProcess: addProcess,
                index: 0
            };

            step = angular.extend({}, defaultParams, stepParameters, step);

            form.steps.push(step);
            step.index = form.steps.length - 1;
            form.currentStep = form.steps[0];
            return step;
        }

        /// <summary>Advances to the next question within all the steps</summary>
        /// <returns>The new step</returns>
        function next() {
            var form = this;
            var question = null;

            // Starting with the current step, find the next question that is allowed to be shown.
            for (var i = form.currentStep.index; i < form.steps.length; i++) {
                var step = form.steps[i];
                if (step.showFn(form.result, form.meta)) {
                    question = findNextQuestion(form.currentStep, step);
                    if (question != null) {
                        form.currentStep = step;
                        break;
                    }
                }
            }

            // If we found a process, do it and then go to the next question.
            if (question != null && question.isProcess) {
                return question.processPromise(question).then(function () {
                    form.next();
                });
            }

            // If no more questions, raise the complete event.
            if (question == null && form.completeCallback) form.completeCallback(form.result);

            return null;
        }

        /// <summary>Returns to the previous question within all the steps</summary>
        /// <returns>The new step</returns>
        function previous() {
            var form = this;

            // Starting with the current step, find the previous question that is allowed to be shown.
            for (var i = form.currentStep.index; i >= 0; i--) {
                var step = form.steps[i];
                if (step.showFn(form.result, form.meta)) {
                    var question = findPreviousQuestion(form.currentStep, step);
                    if (question != null) {
                        form.currentStep = step;
                        break;
                    };
                }
            }
        }

        /// <summary>Goes to the specified step.</summary>
        function goToStep(step) {
            var form = this;
            // If a question has never been answered in this step, do not allow them to jump directly to this step.
            if (!step.isModified) {
                dialog.alert({
                    title: 'Navigation not allowed.',
                    content: 'You can only jump directly to this step once you have answered a question here as well as the questions leading up to this step.'
                });
                return;
            };
            // If going to the same step we're on, do nothing.
            if (step.index === form.currentStep.index) return;
            // If going to a previous step, show the last question in that step.
            if (step.index < form.currentStep.index) step.currentQuestion = step.questions[step.questions.length - 1];
            // If going to a future step, show the first question in that step.
            if (step.index > form.currentStep.index) step.currentQuestion = step.questions[0];
            form.currentStep = step;
        }

        /// <summary>Called by the wizard directive to load the tracking info needed to remove downstream answers when upstream answers are changed.</summary>
        function init() {
            var form = this;

            if (form.steps.length === 0) throw 'You can only edit after the steps/questions have been defined.';

            var ngModelRegex = /="[^" ]*form\.result\.([^"\[ ]*)[^" ]*"/gi;

            // Fetch all the templates.
            for (var i = 0; i < form.steps.length; i++) {
                var step = form.steps[i];
                for (var j = 0; j < step.questions.length; j++) {
                    var question = step.questions[j];
                    (function (q) {
                        $http.get(form.templatesFolder + q.templateFile).then(function (response) {
                            // Might as well cache them for later while we're doing this. :)
                            $templateCache.put(response.config.url, response.data);

                            q.bindings = q.bindings || [];

                            // Find all bindings that bind to form.result and store on the question.
                            var match = ngModelRegex.exec(response.data);
                            while (match != null) {
                                q.bindings.push(match[1]);
                                match = ngModelRegex.exec(response.data);
                            };
                        });
                    })(question);
                }
            }
        }

        /// <summary>
        /// If the current question is modified, this removes any answers that got bound to the 
        /// result object in downstream questions and then reapplies any default values
        /// that were added from any upstream questions or processes.
        /// </summary>
        /// <remarks>This should be called from within the directive's safeApply function so as 
        /// not to trigger any immediateContinue action.</remarks>
        function clearDownstream() {
            var form = this;

            var question = form.currentStep.currentQuestion;

            // If the directive detected a change on the question.
            if (question.isModified) {
                var upstreamDefaults = {};
                var upstreamBindings = [];

                for (var i = 0; i < form.allQuestions.length; i++) {
                    var q = form.allQuestions[i];
                    if (q.bindings != null) {
                        //console.log(q.templateFile, q.bindings);
                        for (var j = 0; j < q.bindings.length; j++) {
                            var resultProperty = q.bindings[j];
                            if (resultProperty != null) {
                                // If upstream or current question.
                                if (i <= question.overallIndex) {
                                    // If the question is visible to the user.
                                    if (question.showFn && question.showFn(form.result, form.meta)) {
                                        angular.extend(upstreamDefaults, q.defaults);
                                        upstreamBindings.push(resultProperty);
                                    }
                                } else {
                                    // Wipe out downstream answers unless they were also bound in upstream or current questions.
                                    if (upstreamBindings.indexOf(resultProperty) === -1) {
                                        delete form.result[resultProperty];
                                    }
                                }
                            }
                        }
                    }
                }

                // Reapply upstream defaults if not bound in ANY upstream (or current) questions.
                for (var l = 0; l <= upstreamBindings.length; l++) {
                    delete upstreamDefaults[upstreamBindings[l]];
                }
                angular.extend(form.result, upstreamDefaults);

                // Set downstream steps as untouched so the user can't jump to them.
                for (var k = question.step.index + 1; k < form.steps.length; k++) {
                    form.steps[k].isModified = false;
                }

                question.isModified = false;
            }
        }

        /// <summary>Adds a new question to the step.</summary>
        /// <param name="questionParameters">Object containing the question parameters.</param>
        /// <param name="questionParameters.templateFile">The template file name of the question.</param>
        /// <param name="questionParameters.showFn">Function that receives the result object as a parameter and returns a boolean indicating if the question should be shown to the user.</param>
        /// <param name="questionParameters.continueAllowedFn">Function that receives the result object as a parameter and returns a boolean indicating if the continue action is allowed.</param>
        /// <param name="questionParameters.isImmediateContinue">Boolean that, when true, will cause the wizard to automatically continue to the next question as soon as the continueAllowedFn delegate returns true.</param>
        /// <param name="questionParameters.isWizardExit">Boolean indicating whether to show the Finish button and hide the Continue button.</param>
        /// <param name="questionParameters.data">Anything you'd like passed down to the question template.</param>
        /// <returns>The existing step.</returns>
        function addQuestion(questionParameters) {
            var step = this;

            var defaultParams = {
                templateFile: null,
                showFn: function () { return true; },
                continueAllowedFn: function () { return true; },
                isImmediateContinue: false,
                isWizardExit: false,
                data: null
            };

            var question = {
                step: step,
                index: 0, // Index within the step
                overallIndex: 0, // Index within all questions
                continueAllowed: true,
                isProcess: false,
                addDefaults: addDefaults,
                isModified: false,
                defaults: {}
            };

            question = angular.extend({}, defaultParams, questionParameters, question);

            step.questions.push(question);
            step.form.allQuestions.push(question);
            question.index = step.questions.length - 1;
            question.overallIndex = step.form.allQuestions.length - 1;
            step.currentQuestion = step.questions[0];
            return step;
        }

        /// <summary>Adds a process to execute between questions.</summary>
        /// <param name="processPromise">Function that returns a promise that must be resolved before continuing.
        /// The function will receive the current process question as a parameter.</param>
        /// <returns>The existing step.</returns>
        function addProcess(processPromiseFn) {
            var step = this;

            var process = {
                step: step,
                index: 0,
                overallIndex: 0,
                continueAllowed: true,
                templateFile: 'pleaseWait.html',
                showFn: function () { return true; },
                continueAllowedFn: function () { return false; },
                isImmediateContinue: false,
                isWizardExit: false,
                data: null,
                isProcess: true,
                processPromise: processPromiseFn,
                addDefaults: addDefaults,
                defaults: {}
            };

            step.questions.push(process);
            step.form.allQuestions.push(process);
            process.index = step.questions.length - 1;
            process.overallIndex = step.form.allQuestions.length - 1;
            step.currentQuestion = step.questions[0];

            return step;
        }

        /// <summary>Adds the defaults to the form.results and also tracks the defaults by the question
        /// to allow for reapplying when changing upstream answers.</summary>
        function addDefaults(resultDefaults) {
            var question = this;
            angular.extend(question.defaults, resultDefaults);
            angular.extend(question.step.form.result, question.defaults);
        }

        /// <summary>Finds the next allowed question.</summary>
        /// <returns>The next question or null if none found.</returns>
        function findNextQuestion(currentStep, step) {
            for (var i = step.currentQuestion.index; i < step.questions.length; i++) {
                var question = step.questions[i];
                if (currentStep.currentQuestion !== question && (question.isProcess || question.showFn(step.form.result, step.form.meta))) {
                    return step.currentQuestion = question;
                }
            }
            return null;
        }

        /// <summary>Finds the previous allowed question.</summary>
        /// <returns>The previous question or null if none found.</returns>
        function findPreviousQuestion(currentStep, step) {
            for (var i = step.currentQuestion.index; i >= 0; i--) {
                var question = step.questions[i];
                if (currentStep.currentQuestion !== question && !question.isProcess && question.showFn(step.form.result, step.form.meta)) {
                    return step.currentQuestion = question;
                }
            }
            return null;
        }
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'newsFeed';

    angular.module('app').controller(controllerId, newsFeed);

    newsFeed.$inject = ['common', '$q', '$mdDialog', '_', 'mentioUtil', '$timeout', 'mentionsService', 'commentsService', '$state', 'coreType', '$scope', 'Upload', 'fileService', 'events', 'employeesService'];

    function newsFeed(common, $q, $mdDialog, _, mentioUtil, $timeout, mentionsService, commentsService, $state, coreType, $scope, upload, fileService, events, employeesService) {
        var vm = this;
        vm.searchMentions = searchMentions;
        vm.getPeopleText = getPeopleText;
        vm.deleteComment = deleteComment;
        vm.getComments = getComments;
        vm.editComment = editComment;
        vm.postComment = postComment;
        vm.autoStartMention = autoStartMention;
        vm.setReminder = setReminder;
        vm.markReminderAsDone = markReminderAsDone;
        vm.uploadFile = uploadFile;
        vm.removeFile = removeFile;
        vm.downloadFile = downloadFile;
        vm.searchEmployees = searchEmployees;
        vm.getEmployeeText = getEmployeeText;

        vm.currentMentions = [];
        vm.files = [];

        vm.queryObject = {
            offset: 0,
            pageSize: 10,
            isWatched: false
        };
        vm.commentsList = {
            pagingData: {
                pageSize: 10,
                offset: 0
            },
            resources: []
        };

        activate();

        function activate() {
            vm.queryObject.isWatched = $state.current.name === 'newsFeed';
            vm.queryObject.reminders = $state.current.name.indexOf('reminders') !== -1;
            vm.queryObject.isSnoozed = $state.current.name === 'remindersSnoozed';
            vm.queryObject.isDone = $state.current.name === 'remindersDone';
            vm.visibility = vm.queryObject.isWatched ? 1 : 2;
            common.activateController([getComments()], controllerId).then(function () {});
        }

        function getComments() {         
            return commentsService.getMentionedComments(vm.queryObject).then(function(result) {
                vm.commentsList.resources = vm.commentsList.resources.concat(result.resources);                
                return vm.commentsList;
            });
        }

        function postComment(bypass) {
            if (!vm.comment) {
                return;
            }
            if (!vm.currentMentions.length && !vm.dontShowAgain && !bypass) {
                showNoMentionsWarning();
                return;
            }
            post();
        }

        function post() {
            var mentions =[];            
            angular.forEach(vm.currentMentions, function(mention) {
                mentions.push(mention.mentionID);
            });
            var documentIds = [];
            angular.forEach(vm.files, function(file) {
                documentIds.push(file.documentKey);
            });
            var comment =
            {
                accessToken: [],
                commentText: vm.comment,
                comment_Key: 0,
                mentions: mentions,
                documentIds: documentIds
            };
            if (vm.queryObject.reminders) {
                comment.reminder = {
                    assignee: vm.assignee,
                    assigneeName: vm.assigneeName
                }
            }
            commentsService.addComment(comment).then(function (result) {
                vm.comment = null;
                vm.currentMentions = [];
                vm.queryObject.offset = 0;
                vm.commentsList.resources = [];
                vm.reminderDate = null;
                vm.files = [];
                vm.assignee = null;
                vm.assigneeText = null;
                vm.getComments();
            });
        }

        function searchMentions(term) {
            mentionsService.getMentions2(term, vm.filterMentionType).then(function(result) {
                vm.mentions = result.resources;                
            });
           return $q.when(vm.mentions);          
        };

        function searchEmployees(term) {
            employeesService.getEmployees(term).then(function (result) {
                vm.employees = result.resources;
            });
            return $q.when(vm.employees);
        };

        function getPeopleText(item) {
            vm.filterMentionType = null;
            item.index = vm.currentMentions.length;
            vm.currentMentions.push(item);
            var itemClass = 'tag-link-' + item.referencedTableCode;
            var page = getDetailPageForType(item.referencedTableCode);
            item.detailPage = '#/' + page + '/' + item.mentionID + '/summary';
            return '<a id="' + item.mentionLabel.replace(/\s/g, '') + item.index + '" target="_blank" href="' +item.detailPage + '" class="' + itemClass + '">' +item.mentionLabel + '</a>';
        };

        function getEmployeeText(term) {
            vm.assignee = term.userKey;
            vm.assigneeName = term.fullName;
            return term.fullName;
        }

        function deleteComment(comment, index) {            
            if (vm.queryObject.reminders) {
                commentsService.deleteReminder(comment.reminder.reminderId).then(function () {
                    vm.commentsList.resources.splice(index, 1);
                    common.$broadcast(events.refreshRemindersCount);
                });
                return;
            }
            commentsService.deleteComment(comment.comment_Key).then(function() {
                vm.commentsList.resources.splice(index, 1);
                common.$broadcast(events.refreshRemindersCount);
            });
        }

        function editComment(item) {
            item.isEditing = true;
        }

        function showNoMentionsWarning() {
            $mdDialog.show({
                controller: function mentionsWarningController($scope, $mdDialog) {                    
                    $scope.continue = function () {
                        $mdDialog.hide($scope.dontShowWarningAgain);
                    }
                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    }
                },
                templateUrl: 'app/newsFeed/noMentionsWarning.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function (dontShowAgain) {
                vm.dontShowAgain = dontShowAgain;
                vm.postComment(true);
            });
        }

        function setReminder(item, when, isSnoozed) {
            var reminder = angular.copy(item.reminder) || {};
            reminder.commentKey = item.comment_Key;
            if (vm.reminderText) {
                reminder.reminderText = vm.reminderText;
            }
            if (vm.assignee) {
                reminder.assignee = vm.assignee;
                reminder.assigneeName = vm.assigneeName;
            }
            var reminderAction = commentsService.addReminder;
            if (isSnoozed) {
                reminderAction = commentsService.markReminderSnoozed;
            }
            if (when === 'tomorrow') {
                reminder.reminderDate = moment().add(1, 'days')._d;
                reminderAction(reminder).then(function (result) {
                    item.reminder = result;
                    vm.reminderText = null;
                    vm.assignee = null;
                    vm.assigneeText = null;
                    common.$broadcast(events.refreshRemindersCount);
                });
            }
            else if (when === 'nextWeek') {
                reminder.reminderDate = moment().add(7, 'days')._d;
                reminderAction(reminder).then(function(result) {
                    item.reminder = result;
                    vm.reminderText = null;
                    vm.assignee = null;
                    vm.assigneeText = null;
                    common.$broadcast(events.refreshRemindersCount);
                });
            }
            else if (when === 'someDay') {
                reminder.reminderDate = null;
                reminderAction(reminder).then(function (result) {
                    item.reminder = result;
                    vm.reminderText = null;
                    vm.assignee = null;
                    vm.assigneeText = null;
                    common.$broadcast(events.refreshRemindersCount);
                });
            }
            else if (when === 'custom') {
                showCustomReminderDialog().then(function(when) {
                    reminder.reminderDate = when;
                    reminderAction(reminder).then(function(result) {
                        item.reminder = result;
                        vm.reminderText = null;
                        vm.assignee = null;
                        vm.assigneeText = null;
                        common.$broadcast(events.refreshRemindersCount);
                    });
                });
            }
        }

        function showCustomReminderDialog() {
            return $mdDialog.show({
                controller: function customReminderController($scope, $mdDialog) {                    
                    $scope.minDate = moment()._d;
                    $scope.apply = function (reminderForm) {
                        if (reminderForm.$invalid) {
                            return;
                        }
                        var month = $scope.customDay.getMonth() + 1;
                        var day = $scope.customDay.getDate();
                        var year = $scope.customDay.getFullYear();
                        var customDate = moment(month + '-' + day + '-' + year + ' ' + $scope.customTime, 'MM-DD-YYYY h:mm a');
                        $mdDialog.hide(customDate._d);
                    }
                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    }
                },
                templateUrl: 'app/newsFeed/customReminder.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function (when) {
                return when;
            });
        }

        function markReminderAsDone(item) {
            commentsService.markReminderDone(item.reminder).then(function (result) {
                item.reminder = result;
                common.$broadcast(events.refreshRemindersCount);
            });
        }

        function autoStartMention(type) {
            $timeout(function () {
                vm.filterMentionType = type;
                var htmlContent = document.querySelector('#htmlContent');
                if (htmlContent) {
                    var ngHtmlContent = angular.element(htmlContent);
                    var html = ngHtmlContent.html() + '@';
                    ngHtmlContent.html(html);

                //mentioUtil.selectElement(null, htmlContent, [vm.currentMentions.length], htmlContent.childNodes[vm.currentMentions.length].length);
                    placeCaretAtEnd(ngHtmlContent[0]);
                    ngHtmlContent.scope().htmlContent = html;
                    ngHtmlContent.scope().$apply();
                    ngHtmlContent.blur();
            }
            }, 0);
        }

        function placeCaretAtEnd(el) {
            el.focus();
            if (typeof window.getSelection != "undefined"
                    && typeof document.createRange != "undefined") {
                var range = document.createRange();
                range.selectNodeContents(el);
                range.collapse(false);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (typeof document.body.createTextRange != "undefined") {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(el);
                textRange.collapse(false);
                textRange.select();
            }
        }

        function getDetailPageForType(type) {
            switch (type) {
                case coreType.company:
                    return 'companies';
                case coreType.contact:
                    return 'contact';
                case coreType.property:
                    return 'properties';
                case coreType.deal:
                    return 'deals';
                case coreType.loan:
                    return 'loans';
                case coreType.pitch:
                    return 'pitches';
            }
            return null;
        }

        function uploadFile(file) {
            if (file) {
                upload.upload({
                    url: 'api/documents',
                        data: {
                            files: [file]
                    }
                }).then(function (response) {                                         
                    vm.files.push(response.data.document);
                    vm.progress = null;
                }, function (response) {
                    if (response.status > 0) {
                        vm.errorMsg = response.status + ': ' +response.data;
                        vm.progress = null;
                    }
                }, function(evt) {
                    vm.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        }

        function removeFile(index) {
            vm.files.splice(index, 1);
        }

        function downloadFile(fileName) {
            var path = fileService.getFile(fileName);
            window.location = path;
        }

        $scope.$watch('vm.comment', function (newVal) {            
            var index = 0;
            angular.forEach(vm.currentMentions, function(mention) {
                var mentionLink = $('#htmlContent').find('[id=' + mention.mentionLabel.replace(/\s/g, '') + mention.index + ']');
                if (mentionLink.text() !== mention.mentionLabel) {
                    mentionLink.remove();
                    vm.currentMentions.splice(index, 1);
                }
                index++;
            });
        });
    }
})();
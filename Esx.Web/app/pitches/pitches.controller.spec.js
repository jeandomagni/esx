﻿(function () {
    describe('pitches.controller', function () {

        beforeEach(module('app'));

        var $controller,
            common,
            pitchesService,
            mentionsService,
            fakePitches,
            $scope,
            controller,
            $timeout,
            $state,
            _;

        var activateController = function (context) {
            controller = $controller('pitchesController', {
                common: common,
                pitchesService: pitchesService,
                mentionsService: mentionsService,
                '_': _,
                $timeout: $timeout,
                $state: $state
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _pitchesService_, _mentionsService_, _$timeout_, _$state_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            pitchesService = _pitchesService_;
            mentionsService = _mentionsService_;
            _ = window._;
            $timeout = _$timeout_;
            $state = _$state_;

            //fakes
            $scope = {
                $apply: angular.noop
            };

            fakePitches = {
                resources: [
                    { fullName: 'John Landers', title: 'pitch Coordinator', company: 'Colony American Homes' },
                    { fullName: 'Bob Dobbs', title: 'Managing Director', company: 'Church of the SubGenius' }
                ],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn(pitchesService, 'getPitches').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakePitches);
                    }
                };
            });

            spyOn(mentionsService, 'setIsWatched');

        }));

        describe('activate', function () {
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define the query object. ', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.pitchesList.pagingData).toEqual(expectedPagingData);
            });
        });

        describe('getPitches', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get pitches with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    offset: 0,
                    pageSize: 30,
                    isPitch:true
                };

                controller.queryObject = expectedSearchQuery;
                controller.pitchesList.resources.length = 0;

                //act
                controller.getPitches();

                //assert
                expect(pitchesService.getPitches).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.pitchesList).toEqual(fakePitches);
            });

        });

        describe('sorting', function () {
            beforeEach(function () {
                activateController();
            });

            it('should sort pitches list', function () {
                var columnName = 'fullName';

                //arrange
                var queryObject = { filters: undefined, searchText: '' };

                controller.queryObject = queryObject;

                //act
                controller.sortColumn(columnName);

                //assert
                expect(pitchesService.getPitches).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.queryObject.sort).toEqual(columnName);
                expect(controller.queryObject.sortDescending).toBe(false);
                expect(controller.pitchesList).toEqual(fakePitches);
            });

            it('should clear current pitches and offset', function () {

                //arrange
                var columnName = 'fullName';
                pitchesService.getPitches.and.callFake(function () {
                    return {
                        then: angular.noop
                    };
                });

                controller.pitchesList.resources = ['one', 'two'];
                controller.queryObject.offset = 79;

                //act
                controller.sortColumn(columnName);

                //assert
                expect(controller.queryObject.offset).toBe(0);
                expect(controller.pitchesList.resources.length).toBe(0);

            });
        });

        describe('searchPitches', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get filtered pitches with the query object', function () {

                //arrange
                controller.searchText = 'text!';
                var expectedSearchQuery = {
                    searchText: 'text!',
                    offset: 0
                };

                controller.queryObject = {};
                controller.pitchesList.resources.length = 0;

                //act
                controller.searchPitches();

                //assert
                expect(pitchesService.getPitches).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.pitchesList).toEqual(fakePitches);
            });

            it('should not get pitches with the query object when searchText < 3', function () {

                //arrange
                pitchesService.getPitches.calls.reset(); //reset the spy
                controller.searchText = 'aa';
                controller.pitchesList.resources.length = 0;

                //act
                controller.searchPitches();

                //assert
                expect(pitchesService.getPitches).not.toHaveBeenCalled();
                expect(controller.pitchesList.resources.length).toBe(0);
            });

            it('should clear current pitches and offset', function () {

                //arrange
                pitchesService.getPitches.and.callFake(function () {
                    return {
                        then: angular.noop
                    };
                });

                controller.pitchesList.resources = ['one', 'two'];
                controller.queryObject.offset = 79;
                controller.searchText = 'text!';

                //act
                controller.searchPitches();

                //assert
                expect(controller.queryObject.offset).toBe(0);
                expect(controller.pitchesList.resources.length).toBe(0);

            });

        });

        describe('selectRow', function () {

            var event;

            beforeEach(function () {

                //Flatten timeout
                $timeout = function (callback) {
                    return callback();
                };

                event = {
                    target: {
                        id: ''
                    }
                };

                activateController();
            });

            it('should send false to the mentions service isWatched is true and event.target.id is esx-watch', function () {
                // Arrange
                var pitch = {
                    isWatched: true
                };
                event.target.id = 'esx-watch';


                // Act
                controller.selectRow(pitch, event);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(pitch.pitchMentionId, false);
            });

            it('should send true to the mentions service when isWatched is false and event.target.id is esx-watch', function () {
                // Arrange
                var pitch = {
                    isWatched: false
                };
                event.target.id = 'esx-watch';

                // Act
                controller.selectRow(pitch, event);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(pitch.pitchMentionId, true);
            });

            it('should change the state to pitch.pitchSummary', function () {
                //arrange
                spyOn($state, 'go');

                var pitch = {
                    dealMentionId: "@me"
                };

                // Act
                controller.selectRow(pitch, event);

                //assert
                expect($state.go).toHaveBeenCalledWith('pitch.pitchSummary', { 'pitchMentionId': pitch.dealMentionId });

            });
        });

    });
})();



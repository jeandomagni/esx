﻿(function () {
    'use strict';

    var controllerId = 'pitchesController';

    angular
      .module('app')
        .controller(controllerId, pitchesController);

    pitchesController.$inject = ['common', 'pitchesService', 'companiesService', 'mentionsService', '_', '$state', '$timeout'];

    function pitchesController(common, pitchesService, companiesService, mentionsService, _, $state, $timeout) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.searchText = '';

        vm.getPitches = _.debounce(getPitches, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.searchPitches = _.debounce(searchPitches, 200);
        vm.selectRow = selectRow;

        vm.queryObject = {
            offset: 0,
            pageSize: 20
        };

        vm.pitchesList = {
            resources: [],
            pagingData: {
                pageSize: 30,
                offset: 0
            },
        }

        function activate() {
            common.activateController([getPitches(false)], controllerId)
                .then(function () { log('Activated pitches.'); });
        }

        function getPitches(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            return pitchesService.getPitches(vm.queryObject).then(function (pitchesList) {
                vm.pitchesList.resources = vm.pitchesList.resources.concat(pitchesList.resources);
                return vm.pitchesList;
            });
        }

        function searchPitches() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.pitchesList.resources.length = 0;
                vm.queryObject.offset = 0;
                getPitches(true);
            }
        }

        function sortColumn(columnName) {
            vm.pitchesList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            getPitches(false);
        }

        function selectRow(item, event) {
            $timeout(function () {
                if (event.target.id === 'esx-watch') {
                    toggleIsWatched(item);
                } else {
                    editPich(item);
                }
            });
        }

        function editPich(pitch) {
            $state.go('pitch.pitchSummary', { 'pitchMentionId': pitch.dealMentionId });
        }

        function toggleIsWatched(pitch) {
            pitch.isWatched = !pitch.isWatched;
            mentionsService.setIsWatched(pitch.dealMentionId, pitch.isWatched);
        }

        activate();
    }
})();
﻿describe('pitches.service', function () {

    beforeEach(module('app'));

    var pitchesService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_pitchesService_, _uriService_, _$httpBackend_) {

        pitchesService = _pitchesService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        uriService.pitch = 'api/pitches/:pitchId';

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getPitches', function () {
        it('should call server with pitches list uri', function () {

            //Arrange
            $httpBackend.expectGET('api/deals/list?isPitch=true', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            pitchesService.getPitches();

            //Assert
            $httpBackend.flush();

        });

        it('should make GET request with query', function () {

            //Arrange
            var query = {
                isPitch:true,
                offset: 100,
                pageSize: 10
            }

            var expectedUri = uriService.pitchesList + '?' + $.param(query);

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            pitchesService.getPitches(query);

            //Assert
            $httpBackend.flush();

        });
    });
});
﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider'];

    function routeConfigurator($stateProvider) {

        $stateProvider
            .state('pitches', {
                url: '/pitches',
                templateUrl: 'app/pitches/pitches.html',
                controller: 'pitchesController',
                controllerAs: 'vm',
                data: {
                    title: 'Pitches',
                    defaultSearch: 'pitch',
                    sidebaricon: 'pitches',
                    includeInSideBar: true,
                    subnavClass: 'pitches-bg'
                }
            }).state('pitch', {
                abstract: true,
                url: '/pitches/:pitchMentionId',
                templateUrl: 'app/pitches/pitch.html',
                controller: 'pitchController',
                controllerAs: 'vm',
                data: {
                    title: ''
                }
            }).state('pitch.pitchSummary', {
                url: '/summary',
                data: {
                    title: '',
                    sidebaricon: 'pitches'
                },
                views: {
                    '': {
                        templateUrl: 'app/pitches/summary/pitchSummary.html',
                        controller: 'pitchSummaryController',
                        controllerAs: 'vm'
                    }
                }
            });
    }
})();
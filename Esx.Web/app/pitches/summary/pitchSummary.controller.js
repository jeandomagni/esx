﻿(function () {
    'use strict';

    var controllerId = 'pitchSummaryController';

    angular.module('app').controller(controllerId, pitchSummaryController);

    pitchSummaryController.$inject = ['common'];

    function pitchSummaryController(common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () {  });
        }
    }
})();
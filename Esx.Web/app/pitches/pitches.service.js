﻿(function () {
    'use strict';

    var serviceId = 'pitchesService';

    angular.module('app').factory(serviceId, pitchesService);

    pitchesService.$inject = ['uriService', '$resource'];

    function pitchesService(uriService, $resource) {

        var resource = $resource(
            uriService.pitch
        );

        var service = {
            getPitches: getPitches
        };

        return service;

        function getPitches(query) {
            if (typeof query == 'undefined') {
                query = {};
            }
            query.isPitch = true;
            return $resource(uriService.pitchesList).get(query).$promise;
        }

    }
})();
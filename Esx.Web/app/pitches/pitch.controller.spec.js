﻿describe('pitch.controller', function () {
    beforeEach(module('app'));

    var $controller,
        common,
        pitchesService,
        fakePitch,
        $scope,
        controller;

    var activateController = function (pitchMentionId) {
        common.$stateParams.pitchId = pitchMentionId;

        controller = $controller('pitchController', {
            common: common,
            pitchesService: pitchesService
        });
    };

    beforeEach(inject(function (_$controller_, _common_, _pitchesService_) {

        //inject
        $controller = _$controller_;
        common = _common_;
        pitchesService = _pitchesService_;

        //fakes
        $scope = {};

        common.$window = { location: {} };

        fakePitch = {
            pitchMentionId: '@something|L',
            pitchName: 'pitchName'
        };

        //spys

    }));

    describe('activate existing pitch', function () {

        beforeEach(function () {
            var pitchMentionId = "@pitch";
            activateController(pitchMentionId);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

    });

});
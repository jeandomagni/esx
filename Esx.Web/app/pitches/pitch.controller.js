﻿(function () {
    'use strict';
    var controllerId = 'pitchController';

    angular.module('app').controller(controllerId, pitchController);

    pitchController.$inject = ['common', 'pitchesService'];

    function pitchController(common, pitchesService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;

        activate();

        function activate() {
            common.activateController([], controllerId).then(function () { log('Retrieved pitch details.'); });
        }

    }
})();
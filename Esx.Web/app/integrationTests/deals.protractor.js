﻿describe('Deal Detail', function () {
    it('should have the correct deal title', function () {
        browser.get('http://localhost:64021/#/deals/2');
        var dealTitle = element(by.binding('vm.deal.dealName')).getText();
        expect(dealTitle).toEqual('Morning Star Littleton Sr. Housing');
    });
});
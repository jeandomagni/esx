﻿describe('edTagsDirective', function () {
    var messageBox;

    beforeEach(function() {
        browser.get('http://localhost:64021/#/companies');
        messageBox = element(by.model('message'));
    });

    it('should show mentions list and auto populate selected mention', function () {
        var mentionPrefix = '@Bob';
        messageBox.sendKeys(mentionPrefix);
        var mentionList = element.all(by.repeater('mention in mentions'));
        expect(mentionList.count()).toBeGreaterThan(0);

        var mentionItem = mentionList.get(0);
        mentionItem.click();

        expect(mentionList.count()).toEqual(0);
        expect(messageBox.getText()).toContain(mentionPrefix);
    });

    it('should show hashtags list and auto populate selected hashtag', function () {
        var hashtagPrefix = '#123';
        messageBox.sendKeys(hashtagPrefix);
        var hashtagList = element.all(by.repeater('hashtag in hashtags'));
        expect(hashtagList.count()).toBeGreaterThan(0);

        var hashtagItem = hashtagList.get(0);
        hashtagItem.click();

        expect(hashtagList.count()).toEqual(0);
        expect(messageBox.getText()).toContain(hashtagPrefix);
    });



});
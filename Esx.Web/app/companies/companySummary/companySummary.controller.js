﻿(function () {
    'use strict';

    var controllerId = 'companySummaryController';

    angular.module('app').controller(controllerId, companySummaryController);

    companySummaryController.$inject = ['common'];

    function companySummaryController(common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () { log('Activated Company Summary View'); });
        }
    }
})();
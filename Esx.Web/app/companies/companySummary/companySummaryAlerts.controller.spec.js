﻿(function () {
    describe('companySummaryAlerts.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            alertsService,
            _,
            controller;

        var fakeAlerts;

        var activateController = function (context) {
            if (context) {
                if (context.companyId) {
                    common.$stateParams.companyId = context.companyId;
                }
            }

            controller = $controller('companySummaryAlertsController', {
                common: common,
                alertsService: alertsService,
                '_': _
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _alertsService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            _ = window._;
            alertsService = _alertsService_;

            //fakes
            fakeAlerts = {
                pagingData: {
                    pageSize: 30,
                    offset: 0
                },
                resources: [{ alertId: 1 }]
            }

            //spys
            spyOn(alertsService, 'getAlerts').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeAlerts);
                    }
                };
            });

            spyOn(alertsService, 'acknowledgeAlert').and.callFake(function() {
                return {
                    then: function(callback) {
                        return callback();
                    }
                }
            });
            spyOn(alertsService, 'removeAlert').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback();
                    }
                }
            });
            spyOn(alertsService, 'snoozeAlert').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback();
                    }
                }
            });

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

        }));

        describe('activate', function () {
            var companyId = '@alertCompany';

            beforeEach(function () {
                activateController({ companyId: companyId });
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should set companyMentionId', function () {
                expect(controller.queryObject.companyId).toBe(companyId);
            });

            it('should get company alerts', function () {
                expect(alertsService.getAlerts).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.alertsList).toEqual(fakeAlerts);
            });
        });

        describe('getAlerts', function () {
            var companyId = "@someCompany";

            beforeEach(function () {
                activateController();
            });

            it('should call the acknowledgeAlert method of the service and remove the alert from the list.', function () {

                //arrange
                controller.alertsList.resources.length = 0; // clear alerts added by 'activate'
                controller.queryObject = {
                    companyId: companyId,
                    offset: 0
                };

                //act
                controller.getAlerts();

                //assert
                expect(alertsService.getAlerts).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.alertsList).toEqual(fakeAlerts);
            });

        });


        describe('acknowledgeAlert', function () {
            beforeEach(function () {
                activateController();
            });

            it('should call the acknowledgeAlert method of the service and remove the alert from the list.', function () {

                //arrange
                var alert = { alertId: 1 };

                //act
                controller.acknowledgeAlert(alert);

                //assert
                expect(alertsService.acknowledgeAlert).toHaveBeenCalledWith(alert.alertId);
                expect(controller.alertsList.resources.filter(function (obj) { return obj.alertId === alert.alertId }).length).toEqual(0);

            });

        });

        describe('removeAlert', function () {
            beforeEach(function () {
                activateController();
            });

            it('should call the removeAlert method of the service and remove the alert from the list.', function () {

                //arrange
                var alert = { alertId: 1 };

                //act
                controller.removeAlert(alert);

                //assert
                expect(alertsService.removeAlert).toHaveBeenCalledWith(alert.alertId);
                expect(controller.alertsList.resources.filter(function (obj) { return obj.alertId === alert.alertId }).length).toEqual(0);

            });

        });

        describe('snoozeAlert', function () {
            beforeEach(function () {
                activateController();
            });

            it('should call the snoozeAlert method of the service and remove the alert from the list.', function () {

                //arrange
                var alert = { alertId: 1 };

                //act
                controller.snoozeAlert(alert);

                //assert
                expect(alertsService.snoozeAlert).toHaveBeenCalledWith(alert.alertId);
                expect(controller.alertsList.resources.filter(function (obj) { return obj.alertId === alert.alertId }).length).toEqual(0);

            });

        });
    });
})();



﻿(function () {
    'use strict';

    var controllerId = 'companySummaryDealActivityController';

    angular.module('app').controller(controllerId, companySummaryDealActivityController);

    companySummaryDealActivityController.$inject = ['common', 'companiesService', '$mdUtil'];

    function companySummaryDealActivityController(common, companiesService, $mdUtil) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () { });
        }

    }
})();
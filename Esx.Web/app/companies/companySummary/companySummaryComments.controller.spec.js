﻿(function () {
    describe('companySummaryComments.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            commentsService,
            $mdUtil,
            controller;

        var fakeComments,
            fakeParams;

        beforeEach(inject(function (_$controller_, _common_, _commentsService_, _$mdUtil_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            $mdUtil = _$mdUtil_;
            commentsService = _commentsService_;

            //fakes
            fakeComments = {
                pagingData: {},
                resources: [{name:'name'}]
            }

            fakeParams = { companyId: "@expectedId" };

            common.$stateParams = fakeParams;

            //spys
            spyOn(commentsService, 'getMentionedComments').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeComments);
                    }
                };
            });

            spyOn($mdUtil, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            //instantiate controller
            controller = $controller('companySummaryCommentsController', {
                common: common,
                commentsService: commentsService
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should set companyMentionId', function() {
                expect(controller.companyMentionId).toBe(fakeParams.companyId);
            });

            it('should set company list', function () {
                expect(commentsService.getMentionedComments).toHaveBeenCalledWith(controller.commentsQueryObject);
                expect(controller.commentsList).toEqual(fakeComments);
            });
        });

        describe('getMentionedComments', function () {

            it('should get comments with the query object and append to comments list resources', function() {

                //arrange
                var expectedQuery = {
                    mentionId: '@someMention'
                };
                controller.commentsQueryObject = expectedQuery;
                var expectedComments = {
                    resources: controller.commentsList.resources.concat(fakeComments.resources),
                    pagingData: fakeComments.pagingData
                };

                //act
                controller.getMentionedComments();

                //assert
                expect(commentsService.getMentionedComments).toHaveBeenCalledWith(expectedQuery);
                expect(controller.commentsList).toEqual(expectedComments);

            });

            it('should get comments with the query object and resset the comments list', function () {

                //arrange
                var expectedQuery = {
                    mentionId: '@someMention'
                };
                controller.commentsQueryObject = expectedQuery;

                controller.commentsList = { resources: [{ name: 'old comment' }] };

                //act
                controller.getMentionedComments(true);

                //assert
                expect(commentsService.getMentionedComments).toHaveBeenCalledWith(expectedQuery);
                expect(controller.commentsList).toEqual(fakeComments);

            });


        });

        describe('searchComments', function () {

            it('should get comments with the query object and reset commentsList resources', function () {

                //arrange
                var expectedQuery = {
                    mentionId: '@someMention',
                    filterText: 'some text',
                    offset: 0
                };
                controller.commentsQueryObject = expectedQuery;

                //act
                controller.searchComments();

                //assert
                expect(commentsService.getMentionedComments).toHaveBeenCalledWith(expectedQuery);
                expect(controller.commentsList).toEqual(fakeComments);

            });

        });

        describe('commentAdded', function () {

            it('should refresh the commentsList resources with the query object', function () {

                //arrange
                var expectedQuery = {
                    mentionId: '@someMention',
                    filterText: 'some text',
                    offset: 0
                };
                controller.commentsQueryObject = expectedQuery;
                controller.commentsList = {};

                //act
                controller.commentAdded();

                //assert
                expect(commentsService.getMentionedComments).toHaveBeenCalledWith(expectedQuery);
                expect(controller.commentsList).toEqual(fakeComments);

            });

        }); 
    });
})();



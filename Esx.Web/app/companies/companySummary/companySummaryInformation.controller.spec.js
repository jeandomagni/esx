﻿describe('companySummaryInformation.controller', function () {
    beforeEach(module('app'));

    var $controller,
        common,
        controller,
        $scope,
        companySummaryService,
        fakeStats;

    var activateController = function (companyId) {
        common.$stateParams.companyId = companyId;

        controller = $controller('companySummaryInformationController', {
            common: common,
            companySummaryService: companySummaryService,
            $scope: $scope
        });
    };

    beforeEach(inject(function (_$controller_, _common_, _companySummaryService_) {
        //inject
        $controller = _$controller_;
        common = _common_;
        companySummaryService = _companySummaryService_;

        //fakes
        $scope = { $watch: function () { } };

        fakeStats = {
            pitches: 1,
            activeDeals: 2,
            bidActivity: 3,
            dealsInPursuit: 4,
            properties: 5,
            holdPeriod: 6,
            loans: 7,
            loansMaturing: 8
        };

        //spys
        spyOn(companySummaryService, 'getStats').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback(fakeStats);
                }
            };
        });
    }));

    describe('activate', function () {
        var companyId = 123;

        beforeEach(function () {
            activateController(companyId);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

        it('should have the correct pitch periods', function () {
            // arrange
            var expectedPeriods = [
                { text: 'Last 30 Days', value: 30 },
                { text: 'Last 45 Days', value: 45 },
                { text: 'Last 90 Days', value: 90 },
                { text: 'Last 180 Days', value: 180 }
            ];

            // assert
            expect(controller.pitchPeriods).toEqual(expectedPeriods);
        });

        it('should have the correct bid activity periods', function () {
            // arrange
            var expectedPeriods = [
                { text: 'Last 1 Month', value: 30 },
                { text: 'Last 3 Months', value: 90 },
                { text: 'Last 6 Months', value: 180 },
                { text: 'Last 12 Months', value: 365 }
            ];

            // assert
            expect(controller.bidActivityPeriods).toEqual(expectedPeriods);
        });

        it('should have the correct hold periods', function () {
            // arrange
            var expectedPeriods = [
                { text: '> 3 Years', value: 36 },
                { text: '> 2 Years', value: 24 }
            ];

            // assert
            expect(controller.holdPeriods).toEqual(expectedPeriods);
        });

        it('should have the correct loans maturing periods', function () {
            // arrange
            var expectedPeriods = [
                { text: 'Next 30 Days', value: 30 },
	            { text: 'Next 60 Days', value: 60 },
	            { text: 'Next 90 Days', value: 90 },
	            { text: 'Next 180 Days', value: 180 },
                { text: 'Next 1 Yr', value: 365 }
            ];

            // assert
            expect(controller.loansMaturingPeriods).toEqual(expectedPeriods);
        });

        it('should have the correct default selected options', function () {
            // Arrange
            var expectedSelectedOptions = {
                pitches: 45,
                bidActivity: 90,
                holdPeriod: 36,
                loansMaturing: 180
            };

            // Act


            // Assert
            expect(controller.selectedOptions).toEqual(expectedSelectedOptions);
        });
    });

    describe('getStats', function () {
        var companyId = 123;
        beforeEach(function () {
            activateController(companyId);
        });

        it('should return the stats when called with the selected options', function () {
            // Arrange
            var expectedSelectedOptions = {
                pitches: 1,
                bidActivity: 2,
                holdPeriod: 3,
                loansMaturing: 4,
                companyId: companyId
            };
            controller.selectedOptions = expectedSelectedOptions;

            // Act
            controller.getStats();

            // Assert
            expect(companySummaryService.getStats).toHaveBeenCalledWith(expectedSelectedOptions);
            expect(controller.stats).toEqual(fakeStats);
        });
    });
});
﻿(function () {
    describe('companySummary.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            controller;

        beforeEach(inject(function (_$controller_, _common_) {

            //inject
            $controller = _$controller_;
            common = _common_;

            //instantiate controller
            controller = $controller('companySummaryController', {
                common: common
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

        });

    });
})();



﻿describe('companySummary.service', function () {

    beforeEach(module('app'));

    var companySummaryService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_companySummaryService_, _uriService_, _$httpBackend_) {

        companySummaryService = _companySummaryService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        //uriService.company = 'api/companies/:companyId';

        // ignore html templates. Not sure if this is a good approach. 
        $httpBackend.whenGET(/.*.html/).respond(200, '');
        $httpBackend.whenPOST(/.*.html/).respond(201, '');
        $httpBackend.whenPATCH(/.*.html/).respond(204, '');

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getStats', function () {

        it('should call server with summary stats uri', function () {

            //Arrange
            var companyId = 123;
            $httpBackend.expectGET('api/companies/' + companyId + '/summaryStats', { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            companySummaryService.getStats({companyId: companyId});

            //Assert
            $httpBackend.flush();
        });
    });
});
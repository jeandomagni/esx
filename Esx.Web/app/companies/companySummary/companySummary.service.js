﻿(function () {
    'use strict';

    var serviceId = 'companySummaryService';

    angular.module('app').factory(serviceId, companySummaryService);

    companySummaryService.$inject = ['uriService', '$resource'];

    function companySummaryService(uriService, $resource) {

        var service = {
            getStats: getStats
        };

        return service;

        function getStats(request) {
            return $resource(uriService.companySummaryStats).get(request).$promise;
        }
    }
})();
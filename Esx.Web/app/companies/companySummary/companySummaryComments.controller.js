﻿(function () {
    'use strict';

    var controllerId = 'companySummaryCommentsController';

    angular.module('app').controller(controllerId, companySummaryCommentsController);

    companySummaryCommentsController.$inject = ['common', 'companiesService', '$mdUtil', 'commentsService'];

    function companySummaryCommentsController(common, companiesService, $mdUtil, commentsService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        vm.companyMentionId = common.$stateParams.companyId;

        vm.commentsList = {
            resources:[],
            pagingData: {
                pageSize: 10,
                offset: 0
            }
        };
        vm.commentsQueryObject = {
            mentionId: vm.companyMentionId,
            filterText: ''
        };
        vm.getMentionedComments = getMentionedComments;
        vm.searchComments = $mdUtil.debounce(searchComments, 200);
        vm.commentAdded = commentAdded;

        activate();

        function activate() {
            common.activateController([getMentionedComments()], controllerId)
                .then(function () { });
        }

        function getMentionedComments(clearList) {
            return commentsService.getMentionedComments(vm.commentsQueryObject).then(function (comments) {
                if (clearList) {
                    vm.commentsList.resources = comments.resources;
                } else {
                    vm.commentsList.resources = vm.commentsList.resources.concat(comments.resources);
                }
                vm.commentsList.pagingData = comments.pagingData;
                return vm.commentsList;
            });
        }

        function searchComments() {
            if (vm.commentsQueryObject.filterText.length === 0 || vm.commentsQueryObject.filterText.length > 2) {
                vm.commentsQueryObject.offset = 0;
                getMentionedComments(true);
            }
        }

        function commentAdded() {
            vm.commentsQueryObject.offset = 0;
            getMentionedComments(true);
        }
    }
})();
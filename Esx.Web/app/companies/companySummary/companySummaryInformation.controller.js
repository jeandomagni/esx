﻿(function () {
    'use strict';

    var controllerId = 'companySummaryInformationController';

    angular.module('app').controller(controllerId, companySummaryInformationController);

    companySummaryInformationController.$inject = ['common', 'companySummaryService', '$stateParams', '$scope'];

    function companySummaryInformationController(common, companySummaryService, $stateParams, $scope) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.getStats = getStats;
        vm.stats = {};
        vm.pitchPeriods = [
            { text: 'Last 30 Days', value: 30 },
            { text: 'Last 45 Days', value: 45 },
            { text: 'Last 90 Days', value: 90 },
            { text: 'Last 180 Days', value: 180 }
        ];
        vm.bidActivityPeriods = [
            { text: 'Last 1 Month', value: 30 },
            { text: 'Last 3 Months', value: 90 },
            { text: 'Last 6 Months', value: 180 },
            { text: 'Last 12 Months', value: 365 }
        ];
        vm.holdPeriods = [
            { text: '> 3 Years', value: 36 },
            { text: '> 2 Years', value: 24 }
        ];
        vm.loansMaturingPeriods = [
            { text: 'Next 30 Days', value: 30 },
	        { text: 'Next 60 Days', value: 60 },
	        { text: 'Next 90 Days', value: 90 },
	        { text: 'Next 180 Days', value: 180 },
            { text: 'Next 1 Yr', value: 365 }
        ];
        vm.selectedOptions = {
            pitches: 45,
            bidActivity: 90,
            holdPeriod: 36,
            loansMaturing: 180
        };

        $scope.$watch('vm.selectedOptions', function (newValue) {
            if (newValue) getStats();
        }, true);

        activate();

        function activate() {
            common.activateController([getStats()], controllerId)
                .then(function () { });
        }

        function getStats() {
            var request = angular.extend({}, vm.selectedOptions, { companyId: $stateParams.companyId });
            return companySummaryService.getStats(request).then(function (stats) {
                vm.stats = stats;
            });
        }

    }
})();
﻿(function () {
    'use strict';
    var controllerId = 'companyEditComplianceController';

    angular.module('app').controller(controllerId, companyEditComplianceController);

    companyEditComplianceController.$inject = ['common', 'companiesService', '$q', '$state', '_'];

    function companyEditComplianceController(common, companiesService, $q, $state, _) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.companyMentionId = common.$stateParams.companyId;
        vm.saveCompanyCompliance = _.debounce(saveCompanyCompliance, 200);
        vm.companyDetails = {};

        activate();

        function activate() {
            common.activateController([getCompanyDetails()], controllerId).then(function () { log('Activated Company Edit Compliance View.'); });
        }

        function getCompanyDetails() {
            return companiesService.getCompanyDetails(vm.companyMentionId).then(function (companyDetails) {
                vm.companyDetails = companyDetails;
                
                vm.companyDetails.hasProfileChanged = false;

                // set doNotSolicit date
                vm.doNotSolicitDate = (vm.companyDetails.isSolicitableDate) ? new Date(vm.companyDetails.isSolicitableDate) : null;
            });
        }

        function saveCompanyCompliance(form, companyDetails) {
            if (form.$dirty) {
                if (form.$invalid) {
                    return;
                }

                companiesService.updateCompany(companyDetails).then(function() {
                    $state.go('company.companyDetails', { companyId: vm.companyMentionId });
                });
            }
        }
    }
})();
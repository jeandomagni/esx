﻿(function () {
    'use strict';
    var controllerId = 'companyController';

    angular.module('app').controller(controllerId, companyController);

    companyController.$inject = ['common', 'companiesService', '$q', '$state', '$stateParams'];

    function companyController(common, companiesService, $q, $state, $stateParams) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;               

        activate();

        function activate() {
            common.activateController([setCompanyName()], controllerId).then(function () { log('Activated Company View.'); });
        }

        function setCompanyName() {
            companiesService.getCompanyName($stateParams.companyId).then(function (response) {
                common.setPageHeading(response.data, $stateParams.companyId);
            });
        }
    }
})();
﻿
(function () {
    describe('companyDeals.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            companiesService,
            mentionsService,
            fakeCompanyDeals,
            $scope,
            controller,
            _,
            $mdMedia;

        beforeEach(inject(function (_$controller_, _common_, _$mdMedia_, _companiesService_, _mentionsService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;
            mentionsService = _mentionsService_;
            _ = window._; // lodash
            $mdMedia = _$mdMedia_;

            //fakes
            $scope = {};
            //$apply: angular.noop
            //};

            fakeCompanyDeals = {
                resources: [{name:'test name'}],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(companiesService, 'getCompanyDeals').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeCompanyDeals);
                    }
                };
            });

            spyOn(mentionsService, 'setIsWatched');

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            //instantiate controller
            var companyId = 123;
            common.$stateParams.companyId = companyId;
            controller = $controller('companyDealsController', {
                common: common,
                companiesService: companiesService,
                $mdMedia: $mdMedia,
                '_': _
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define the query object. ', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.dealsList.pagingData).toEqual(expectedPagingData);
            });
        });

        describe('sort', function () {
            it('should sort deals list', function () {
                var columnName = 'name';

                //arrange
                var queryObject = { filters: undefined, searchText: '' };

                controller.queryObject = queryObject;

                controller.sortColumn(columnName);

                expect(companiesService.getCompanyDeals).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.dealsList).toEqual(fakeCompanyDeals);
            });
        });

        describe('getCompanyDeals', function () {

            it('should get deals with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    offset: 0,
                    pageSize: 30
                };

                controller.queryObject = expectedSearchQuery;
                controller.dealsList.resources.length = 0; 

                //act
                controller.getCompanyDeals();

                //assert
                expect(companiesService.getCompanyDeals).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.dealsList).toEqual(fakeCompanyDeals);
            });

        });

        describe('searchCompanyDeals', function () {

            it('should get deals with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: "test"
                };

                controller.queryObject = expectedSearchQuery;
                controller.dealsList.resources.length = 0;

                //act
                controller.searchCompanyDeals();

                //assert
                expect(companiesService.getCompanyDeals).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.dealsList).toEqual(fakeCompanyDeals);
            });

            it('should not get deals with the query object when searchText < 3', function () {

                companiesService.getCompanyDeals.calls.reset(); //reset the spy
                controller.searchText = 'aa';
                controller.dealsList.resources.length = 0;

                //act
                controller.searchCompanyDeals();

                //assert
                expect(companiesService.getCompanyDeals).not.toHaveBeenCalled();
                expect(controller.dealsList.resources.length).toBe(0);
            });

        });

        describe('toggleIsWatched', function () {
            it('should send false to the company service when the current state is true', function () {
                // Arrange
                var deal = {
                    dealMentionId: '@test',
                    isWatched: true
                };

                // Act
                controller.toggleIsWatched(deal);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(deal.dealMentionId, false);
            });

            it('should send true to the company service when the current state is false', function () {
                // Arrange
                var deal = {
                    dealMentionId: '@test',
                    isWatched: false
                };

                // Act
                controller.toggleIsWatched(deal);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(deal.dealMentionId, true);
            });
        });
    });
})();


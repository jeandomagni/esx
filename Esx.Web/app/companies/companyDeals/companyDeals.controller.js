﻿(function () {
    'use strict';
    var controllerId = 'companyDealsController';

    angular.module('app').controller(controllerId, companyDealsController);

    companyDealsController.$inject = ['common', '$location', '_', 'mentionsService', 'companiesService', '$timeout'];

    function companyDealsController(common, $location, _, mentionsService, companiesService, $timeout) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;

        vm.searchText = '';

        vm.dealsList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        };

        vm.queryObject = {
            companyId: common.$stateParams.companyId,
            offset: 0,
            pageSize: 30
        };

        vm.getCompanyDeals = _.debounce(getCompanyDeals, 200);
        vm.searchCompanyDeals = _.debounce(searchCompanyDeals, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.editDeal = editDeal;
        vm.toggleIsWatched = _.debounce(toggleIsWatched, 200);
        vm.selectRow = selectRow;

        vm.title = 'Company Deals';

        vm.deals = [];

        activate();

        function activate() {
            common.activateController([getCompanyDeals()], controllerId)
                .then(function () { log('Activated Company Deals View'); });
        }

        function searchCompanyDeals() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.queryObject.offset = 0;
                vm.dealsList.resources.length = 0;
                getCompanyDeals(true);
            }
        }

        function getCompanyDeals(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            return companiesService.getCompanyDeals(vm.queryObject).then(function (dealsList) {
                vm.dealsList.resources = vm.dealsList.resources.concat(dealsList.resources);
                vm.queryObject.isBusy = false;
                return vm.dealsList;
            });
        }

        function sortColumn(columnName) {
            vm.dealsList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            getCompanyDeals();
        }

        function selectRow(deal) {
            $timeout(function () {
                if (vm.rowAction === 'toggleWatch')
                    toggleIsWatched(deal);
                else
                    vm.editDeal(deal);
            });
        }

        function editDeal(deal) {
            $location.path('/deals/' + deal.dealMentionId);
        }

        function toggleIsWatched(deal) {
            deal.isWatched = !deal.isWatched;
            mentionsService.setIsWatched(deal.dealMentionId, deal.isWatched);
        }
    }
})();
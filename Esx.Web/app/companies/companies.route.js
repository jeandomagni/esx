﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider'];

    function routeConfigurator($stateProvider) {

        $stateProvider
            .state('quickAddCompany', {
                url: '/companies/quickAdd',
                templateUrl: 'app/companies/companyQuickAdd/quickAddCompany.html',
                controller: 'quickAddCompanyController',
                controllerAs: 'vm',
                data: {
                    title: 'Companies',
                    subnavClass: 'companies-bg'
                }
            }).state('companies', {
                url: '/companies',
                templateUrl: 'app/companies/companies.html',
                controller: 'companiesController',
                controllerAs: 'vm',
                data: {
                    title: 'Companies',
                    defaultSearch: 'company',
                    sidebaricon: 'companies',
                    subnavClass: 'companies-bg'
                }
            }).state('companyInvestments', {
                url: '/companies/:companyId/investments',
                templateUrl: 'app/companies/companyInvestments/companyInvestments.html',
                controller: 'companyInvestmentsController',
                controllerAs: 'vm',
                data: {
                    title: ''
                }
            }).state('company', {
                abstract: true,
                url: '/companies/:companyId',
                templateUrl: 'app/companies/company.html',
                controller: 'companyController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    subnavClass: 'companies-bg',
                    subtabs: [
                        { sref: 'company.companySummary', label: 'SUMMARY' },
                        { sref: 'company.companyDetails', label: 'DETAILS' },
                        { sref: 'company.companyLocations', label: 'LOCATIONS' },
                        { sref: 'company.companyContacts', label: 'CONTACTS' },
                        { sref: 'company.companyDeals', label: 'DEALS' },
                        { sref: 'company.companyProperties', label: 'PROPERTIES' },
                        { sref: 'company.companyLoans', label: 'LOANS' }
                    ]
                }
            }).state('company.companySummary', {
                url: '/summary',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                },
                views: {
                    '': {
                        templateUrl: 'app/companies/companySummary/companySummary.html',
                        controller: 'companySummaryController',
                        controllerAs: 'vm'
                    },
                    'information@company.companySummary': {
                        templateUrl: 'app/companies/companySummary/companySummaryInformation.html',
                        controller: 'companySummaryInformationController',
                        controllerAs: 'vm'
                    },
                    'dealActivity@company.companySummary': {
                        templateUrl: 'app/companies/companySummary/companySummaryDealActivity.html',
                        controller: 'companySummaryDealActivityController',
                        controllerAs: 'vm'
                    },
                    'alerts@company.companySummary': {
                        templateUrl: 'app/companies/companySummary/companySummaryAlerts.html',
                        controller: 'companySummaryAlertsController',
                        controllerAs: 'vm'
                    },
                    'comments@company.companySummary': {
                        templateUrl: 'app/companies/companySummary/companySummaryComments.html',
                        controller: 'companySummaryCommentsController',
                        controllerAs: 'vm'
                    }
                }
            }).state('company.companyDetails', {
                url: '/details',
                templateUrl: 'app/companies/companyDetails/companyDetails.html',
                controller: 'companyDetailsController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('company.companyLocations', {
                url: '/locations',
                templateUrl: 'app/companies/companyLocations/companyLocations.html',
                controller: 'companyLocationsController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('company.companyLocationsAdd', {
                url: '/locations/add',
                templateUrl: 'app/companies/companyLocations/companyLocationsAdd.html',
                controller: 'companyLocationsAddController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('company.companyContacts', {
                url: '/contacts',
                templateUrl: 'app/companies/companyContacts/companyContacts.html',
                controller: 'companyContactsController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('company.companyContactsAdd', {
                url: '/contacts/add',
                templateUrl: 'app/companies/companyContacts/companyContactAdd.html',
                controller: 'companyContactAddController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('company.companyDeals', {
                url: '/deals',
                templateUrl: 'app/companies/companyDeals/companyDeals.html',
                controller: 'companyDealsController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('company.companyProperties', {
                url: '/properties',
                templateUrl: 'app/companies/companyProperties/companyProperties.html',
                controller: 'companyPropertiesController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('company.companyLoans', {
                url: '/loans',
                templateUrl: 'app/companies/companyLoans/companyLoans.html',
                controller: 'companyLoansController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('company.companyLoansAdd', {
                url: '/loans/add',
                templateUrl: 'app/companies/companyLoans/companyLoansAdd.html',
                controller: 'companyLoansAddController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('company.companyHistory', {
                url: '/history',
                templateUrl: 'app/companies/companyHistory/companyHistory.html',
                controller: 'companyHistoryController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('companyEdit', {
                abstract: true,
                url: '/companies/:companyId/edit',
                templateUrl: 'app/companies/companyEdit.html',
                controller: 'companyEditController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    subnavClass: 'companies-bg',
                    sidebaricon: 'companies',
                    subtabs: [
                        { sref: 'companyEdit.profile', label: 'PROFILE' },
                        { sref: 'companyEdit.compliance', label: 'COMPLIANCE' }
                    ]
                }
            }).state('companyEdit.profile', {
                url: '/profile',
                templateUrl: 'app/companies/companyEditProfile.html',
                controller: 'companyEditProfileController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            }).state('companyEdit.compliance', {
                url: '/compliance',
                templateUrl: 'app/companies/companyEditCompliance.html',
                controller: 'companyEditComplianceController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    sidebaricon: 'companies'
                }
            });
    }
})();
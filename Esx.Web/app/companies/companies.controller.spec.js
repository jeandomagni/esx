﻿(function () {
    describe('companies.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            companiesService,
            mentionsService,
            $mdUtil,
            fakeCompanies,
            $scope,
            controller,
            _,
            $mdMedia;

        beforeEach(inject(function (_$controller_, _common_, _companiesService_, _mentionsService_, _$mdMedia_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;
            mentionsService = _mentionsService_;
            _ = window._; // lodash
            $mdMedia = _$mdMedia_;
            
            //fakes
            $scope = {};

            fakeCompanies = {
                resources: [{ name: 'some company' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(companiesService, 'getCompanies').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeCompanies);
                    }
                };
            });

            spyOn(mentionsService, 'setIsWatched');

            spyOn(companiesService, 'getCompanyStatistics').and.callFake(function() {
                return {
                    then: function(callback) {
                        return callback(fakeStatistics);
                    }
                };
            });

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn(_, 'throttle').and.callFake(function (callback) {
                return callback;
            });

            //instantiate controller
            controller = $controller('companiesController', {
                common: common,
                companiesService: companiesService,
                '_': _,
                '$scope': $scope,
                $mdMedia: $mdMedia
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define the query object. ', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should have the correct title', function () {
                expect(controller.title).toBe('Companies');
            });
            
        });

        describe('sort', function () {
            it('should sort companies list', function() {
                var columnName = 'companyName';

                //arrange
                var queryObject = { filters: undefined, searchText: '' };

                controller.queryObject = queryObject;

                controller.sortColumn(columnName);

                expect(companiesService.getCompanies).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.companyList).toEqual(fakeCompanies);
            });
        });

        describe('getCompanies', function () {

            it('should get companies with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: 'text!'
                };

                controller.queryObject = expectedSearchQuery;
                controller.companyList.resources.length = 0; // clear company that "activate" added.

                //act
                controller.getCompanies();

                //assert
                expect(companiesService.getCompanies).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.companyList).toEqual(fakeCompanies);
            });

        });

        describe('toggleIsWatched', function() {
            it('should send false to the company service when the current state is true', function() {
                // Arrange
                var company = {
                    mentionID: '@test',
                    isWatched: true
                };

                // Act
                controller.toggleIsWatched(company);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(company.mentionID, false);
            });

            it('should send true to the company service when the current state is false', function () {
                // Arrange
                var company = {
                    mentionID: '@test',
                    isWatched: false
                };

                // Act
                controller.toggleIsWatched(company);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(company.mentionID, true);
            });
        });
    });
})();



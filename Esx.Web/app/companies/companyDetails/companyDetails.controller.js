﻿(function () {
    'use strict';

    var controllerId = 'companyDetailsController';

    angular.module('app').controller(controllerId, companyDetailsController);

    companyDetailsController.$inject = ['common', 'companiesService', '$state', '$stateParams', '_', '$location', '$timeout'];

    function companyDetailsController(common, companiesService, $state, $stateParams, _, $location, $timeout) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Company Details';
        vm.companyId = common.$stateParams.companyId;
        vm.companyDetails = "";
        vm.structureCompanies = [];
        vm.company = {
            mentionID: vm.companyId
        };
        vm.address = '';
        vm.emailAddress = '';
        vm.webPage = '';
        vm.phoneNumber = '';
        vm.selectRow = selectRow;

        activate();

        function activate() {
            common.activateController([getCompanyDetails()], controllerId)
                .then(function () { log('Activated Company Details View'); });
        }

        function getCompanyDetails() {
            return companiesService.getCompanyDetails(vm.companyId).then(function (companyDetails) {
                vm.companyDetails = companyDetails;

                analyzeElectronicAddress();
                setCompaniesForStructure();
                setParentCompany();

                // set doNotSolicit date
                vm.doNotSolicitDate = (vm.companyDetails.isSolicitableDate) ? new Date(vm.companyDetails.isSolicitableDate) : null;
            });
        }

        function setParentCompany() {
            // set parent company
            if (vm.companyDetails && vm.companyDetails.parentCompany && vm.companyDetails.parentCompany.parentCompanyKey > 0) {
                vm.parentCompany = {
                    company_Key: vm.companyDetails.parentCompany.parentCompanyKey,
                    mentionID: vm.companyDetails.parentCompany.parentMentionId,
                    aliasName: vm.companyDetails.parentCompany.parentCompanyName
                };
                vm.companyHierarchyKey = vm.companyDetails.parentCompany.companyHierarchyKey;
            }
        }

        function setCompaniesForStructure() {
            // reset structure companies
            vm.structureCompanies = [];

            // add company
            vm.structureCompanies.push({
                mentionID: vm.companyId,
                companyName: vm.companyDetails.companyName,
                parentCompany: (vm.companyDetails.parentCompany) ? vm.companyDetails.parentCompany.parentCompanyName : ''
            });

            // add parents
            if (vm.companyDetails && vm.companyDetails.parentCompany && vm.companyDetails.parentCompany.parentCompanyKey > 0) {
                vm.structureCompanies.push({
                    mentionID: vm.companyDetails.parentCompany.parentMentionId,
                    companyName: vm.companyDetails.parentCompany.parentCompanyName,
                    parentCompany: null
                });
            }
        }

        function analyzeElectronicAddress() {
            if (vm.companyDetails && vm.companyDetails.office && vm.companyDetails.office.electronicAddress) {
                angular.forEach(vm.companyDetails.office.electronicAddress, function (value) {
                    switch (value.electronicAddressTypeCode) {
                        case 'EmailAddress':
                            vm.emailAddress = value.address;
                            break;
                        case 'WebPage':
                            vm.webPage = value.address;
                            break;
                        case 'PhoneNumber':
                            vm.phoneNumber = value.address;
                            break;
                        default:
                            break;
                    }
                });
            }
        }

        function selectRow(company) {
            $timeout(function () {
                switch (vm.rowAction) {
                    case "deals":
                        $location.path('/companies/' + company.mentionID + '/deals');
                        break;
                    case "locations":
                        $location.path('/companies/' + company.mentionID + '/locations');
                        break;
                    case "contacts":
                        $location.path('/companies/' + company.mentionID + '/contacts');
                        break;
                    case "summary":
                    default:
                        $location.path('/companies/' + company.mentionID + '/summary');
                        break;
                }
            });
        }
    }
})();
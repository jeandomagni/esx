﻿(function () {
    describe('companyDetails.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            companiesService,
            controller,
            $mdUtil,
            $mdMedia;

        beforeEach(inject(function (_$controller_, _common_, _companiesService_, _$mdUtil_, _$mdMedia_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;
            $mdUtil = _$mdUtil_;
            $mdMedia = _$mdMedia_;

            // fakes
            var fakeDetails =
            {
                mentionId: 'testMentionId',
                companyKey: 123,
                companyName: 'Test company',
                aliases: [
                {
                    aliasKey: 444,
                    aliasName: 'Test Alias'
                }],
                parentCompany:
                {
                    companyHierarchyKey: 124124,
                    parentCompanyKey: 999,
                    childCompanyKey: 123,
                    parentMentionId: 'parent mention',
                    parentCompanyName: 'parent company name'
                },
                office:
                {
                    officeId: 345,
                    officeName: 'Test Office',
                    isHeadQuarters: true,
                    physicalAddress:
                    {
                        physicalAddressKey: 567,
                        address1: 'Test Address',
                        city: 'test City'
                    },
                    electronicAddress:
                    [
                        {
                            electronicAddressTypeCode: 'EmailAddress',
                            address: 'test@email.com'
                        },
                        {
                            electronicAddressTypeCode: 'WebPage',
                            address: 'www.testwebpage.com'
                        },
                        {
                            electronicAddressTypeCode: 'PhoneNumber',
                            address: '999-765-6082'
                        }
                    ]
                },
                isSolicitable: false,
                hasInterestedInDeals: false,
                fcpaName: 'test fcpa name'
            };

            var fakeParentCompany = [{
                mentionID: 'testMentionId',
                aliasName: 'test alias',
                company_key: 123
            }];

            var fakeAlias = {
                aliasKey: 8798,
                aliasName: 'new alias name'
            };

            //spys
            spyOn(companiesService, 'getCompanyDetails').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeDetails);
                    }
                };
            });

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            //instantiate controller
            controller = $controller('companyDetailsController', {
                common: common,
                companiesService: companiesService,
                $mdMedia: $mdMedia,
                '_': _
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should have the correct title', function () {
                expect(controller.title).toBe('Company Details');
            });

            it('should have company details', function () {
                expect(controller.companyDetails).toBeDefined();
            });

            it('should have correct structureCompanies', function () {
                expect(controller.structureCompanies.length).toBeGreaterThan(0);
            });

            it('should have correct email adrress name', function () {
                expect(controller.emailAddress).toBe('test@email.com');
            });
        });

    });
})();
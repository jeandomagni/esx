﻿describe('companies.service', function () {

    beforeEach(module('app'));

    var companiesService,
        uriService,
        $httpBackend,
        _;

    beforeEach(inject(function (_companiesService_, _uriService_, _$httpBackend_) {

        companiesService = _companiesService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;
        _ = window._;

        uriService.company = 'api/companies/:companyId';

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getCompanies', function () {

        it('should call server with Companies list uri', function () {

            //Arrange
            $httpBackend.expectGET(uriService.companyList, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            companiesService.getCompanies();

            //Assert
            $httpBackend.flush();

        });
     
    });


    describe('getCompanyName', function () {

        it('should call server with company name uri', function () {
            var companyId = 123;

            //Arrange
            $httpBackend.expectGET('api/companies/' + companyId + '/name', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            companiesService.getCompanyName(companyId);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('addCompany', function () {
        it('should call server with company uri', function () {
            var company = { companyName: 'Test Name' };

            //Arrange
            $httpBackend.expectPOST('api/companies', company, { 'Accept': 'application/json, text/plain, */*', 'Content-Type': 'application/json;charset=utf-8' }).respond({});

            //Act
            companiesService.addCompany(company);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('updateCompany', function () {
        it('should call server with company uri', function () {
            var company = { companyName: 'Test Name' };

            //Arrange
            $httpBackend.expectPATCH('api/companies', company, { 'Accept': 'application/json, text/plain, */*', 'Content-Type': 'application/json;charset=utf-8' }).respond({});

            //Act
            companiesService.updateCompany(company);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('companyContacts', function () {
        it('should call server with companyContacts uri', function () {
            var query = { companyId: 123 };

            //Arrange
            $httpBackend.expectGET('api/companies/' + query.companyId + '/contacts', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            companiesService.getCompanyContacts(query);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('companyProperties', function () {
        it('should call server with companyProperties uri', function () {
            var query = { companyId: 123 };

            //Arrange
            $httpBackend.expectGET('api/companies/' + query.companyId + '/properties', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            companiesService.getCompanyProperties(query);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('companyLoans', function () {
        it('should call server with companyLoans uri', function () {
            var query = { companyId: 123 };

            //Arrange
            $httpBackend.expectGET('api/companies/' + query.companyId + '/loans', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            companiesService.getCompanyLoans(query);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('companyDeals', function () {
        it('should call server with companyDeals uri', function () {
            var query = { companyId: 123 };

            //Arrange
            $httpBackend.expectGET('api/companies/' + query.companyId + '/deals', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            companiesService.getCompanyDeals(query);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('companyDetails', function () {
        it('should call server with companyDetails uri', function () {
            var mentionId = 'testId';

            //Arrange
            $httpBackend.expectGET('api/companies/' + mentionId + '/details', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            companiesService.getCompanyDetails(mentionId);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('companyIntellisense', function () {
        it('should call server with search uri and query', function () {
            var query = 'TestName';

            //Arrange
            $httpBackend.expectGET('api/companies/intellisense?query=' + query, { 'Accept': 'application/json, text/plain, */*' }).respond([]);

            //Act
            companiesService.companyIntellisense(query);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('companyLocations', function () {
        it('should call server with companyLocations uri', function () {
            var query = { companyId: 123 };

            //Arrange
            $httpBackend.expectGET('api/companies/' + query.companyId + '/locations', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            companiesService.getCompanyLocations(query);

            //Assert
            $httpBackend.flush();

        });
    });
    
    describe('addCompanyLocation', function () {
        it('should post to server with company uri and location', function () {

            //Arrange
            var location = { Name: 'Test Name' };
            var mentionId = "@someMentionId|C";
            var expectedUri = 'api/companies/' + mentionId + '/locations';

            $httpBackend.expectPOST(expectedUri, location, { 'Accept': 'application/json, text/plain, */*', 'Content-Type': 'application/json;charset=utf-8' }).respond({});

            //Act
            companiesService.addCompanyLocation(mentionId, location);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('addAlias', function () {
        it('should call server with addAlias uri', function () {
            //Arrange
            var alias = { companyKey: 123, aliasName: 'Test Name' };
            var expectedUri = 'api/companies/addAlias';

            $httpBackend.expectPOST(expectedUri, alias, { 'Accept': 'application/json, text/plain, */*', 'Content-Type': 'application/json;charset=utf-8' }).respond({});

            //Act
            companiesService.addAlias(alias);

            //Assert
            $httpBackend.flush();
        });
    });

});
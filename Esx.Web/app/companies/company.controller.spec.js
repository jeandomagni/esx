﻿describe('company.controller', function () {
    beforeEach(module('app'));

    var $controller,
        common,
        controller,
        q,
        state;

    var activateController = function (companyId) {
        common.$stateParams.companyId = companyId;
        state.current.name = "company.companySummary";

        controller = $controller('companyController', {
            common: common,
            $q: q,
            $state: state
        });
    };

    beforeEach(inject(function (_$controller_, _common_, $q, $state) {
        //inject
        $controller = _$controller_;
        common = _common_;
        q = $q;
        state = $state;
    }));

    describe('activate existing company', function () {
        var companyId = 123;

        beforeEach(function () {
            activateController(companyId);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

    });
});
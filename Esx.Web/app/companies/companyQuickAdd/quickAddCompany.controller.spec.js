﻿(function () {
    describe('quickAddCompany.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            resourcesService,
            companiesService,
            contactsService,
            $scope,
            controller,
            $state;

        beforeEach(inject(function (_$controller_, _common_, _resourcesService_, _$state_, _companiesService_, _contactsService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            resourcesService = _resourcesService_;
            $state = _$state_;
            companiesService = _companiesService_;
            contactsService = _contactsService_;
            
            //fakes
            $scope = {};

            //spies
            spyOn(resourcesService, 'getStateProvinces').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback([{ stateProvinceCode: 'MN' }]);
                    }
                };
            });
            spyOn(resourcesService, 'getCountries').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback([{ countryName: 'United States'}]);
                    }
                };
            });
            spyOn(resourcesService, 'getUserPreferences').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback([{ preferenceKey: 'Country', preferenceValue: 'United States'}]);
                    }
                };
            });

            //instantiate controller
            controller = $controller('quickAddCompanyController', {
                common: common,
                resourcesService: resourcesService,
                $state: $state,
                companiesService: companiesService
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                //assert
                expect(controller).toBeDefined();
            });

            it('should initialize dependencies correctly', function() {
                //arrange
                //act
                //assert
                expect(resourcesService.getStateProvinces).toHaveBeenCalled();
                expect(resourcesService.getCountries).toHaveBeenCalled();
                expect(resourcesService.getUserPreferences).toHaveBeenCalled();
                expect(controller.stateProvinces.length).toBe(1);
                expect(controller.countries.length).toBe(1);
                expect(controller.hasDefaultCountry).toBe(true);
                expect(controller.company.locationCountry).toBe('United States');
            });
        });

        describe('showCompanyDetails', function () {

            beforeEach(function () {

                spyOn($state, 'go').and.callFake(function () {
                    return {
                        then: function (callback) {
                            return callback({});
                        }
                    };
                });
            });

            it('should redirect to company details page', function () {

                //arrange
                var company = { company_Key: 1 };

                //act
                controller.showCompanyDetails(company);

                //assert
                expect($state.go).toHaveBeenCalled();                
            });
        });

        describe('continueAdding', function () {

            beforeEach(function () {});

            it('should set continueAdd on the view model to true', function () {

                //arrange                
                controller.company = { companyName: 'testing' };
                controller.intellisenseSearchResults = [];

                //act
                controller.continueAdding();

                //assert
                expect(controller.continueAdd).toBe(true);
            });
        });

        describe('isValidCompanyToAdd', function () {

            beforeEach(function () {});

            it('should return valid value of the form', function () {

                //arrange                
                var form = { $valid: true };

                //act
                var actual = controller.isValidCompanyToAdd(form);

                //assert
                expect(actual).toBe(true);
            });
        });

        describe('companyIntellisense', function () {

            beforeEach(function () {

                spyOn(companiesService, 'companyIntellisense').and.callFake(function () {
                    return {
                        then: function (callback) {
                            return callback({});
                        }
                    };
                });
            });

            it('should call companies service to get company intellisense', function () {

                //arrange
                var query = 'testing';

                //act
                controller.companyIntellisense(query);

                //assert
                expect(companiesService.companyIntellisense).toHaveBeenCalled();
            });
        });

        describe('contactsIntellisense', function () {

            beforeEach(function () {

                spyOn(contactsService, 'searchContacts').and.callFake(function () {
                    return {
                        then: function (callback) {
                            return callback({});
                        }
                    };
                });
            });

            it('should call companies service to get contact intellisense', function () {

                //arrange
                var query = 'testing';

                //act
                controller.contactsIntellisense(query);

                //assert
                expect(contactsService.searchContacts).toHaveBeenCalled();
            });
        });

        describe('selectedParentChange', function () {

            beforeEach(function () { });

            it('should set parent of company correctly', function () {

                //arrange                
                controller.company = {};
                var parent = 123;

                //act
                controller.selectedParentChange(parent);

                //assert
                expect(controller.company.parent).toBe(parent);
            });
        });

        describe('addCompany', function () {

            beforeEach(function () {

                spyOn(companiesService, 'addCompany').and.callFake(function () {
                    return {
                        then: function (callback) {
                            return callback({});
                        }
                    };
                });

                spyOn($state, 'go').and.callFake(function () {
                    return {
                        then: function (callback) {
                            return callback({});
                        }
                    };
                });
            });

            it('should add company successfully', function () {

                //arrange
                var form = { $invalid: false };
                controller.company = {};

                //act
                controller.addCompany(form, controller.company);

                //assert
                expect(companiesService.addCompany).toHaveBeenCalled();
                expect($state.go).toHaveBeenCalled();
            });

            it('should not add company if form is invalid', function () {

                //arrange
                var form = { $invalid: true };
                controller.company = {};

                //act
                controller.addCompany(form, controller.company);

                //assert
                expect(companiesService.addCompany).not.toHaveBeenCalled();
                expect($state.go).not.toHaveBeenCalled();
            });
        });
    });
})();



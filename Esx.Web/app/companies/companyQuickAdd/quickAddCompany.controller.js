﻿(function () {
    'use strict';

    var controllerId = 'quickAddCompanyController';

    angular.module('app').controller(controllerId, quickAddCompanyController);

    quickAddCompanyController.$inject = ['common', 'companiesService', 'contactsService', '$state', 'resourcesService'];

    function quickAddCompanyController(common, companiesService, contactsService, $state, resourcesService) {

        var vm = this;
        vm.showCompanyDetails = showCompanyDetails;
        vm.companyIntellisense = companyIntellisense;
        vm.contactsIntellisense = contactsIntellisense;
        vm.selectedParentChange = selectedParentChange;        
        vm.addCompany = addCompany;
        vm.continueAdding = continueAdding;
        vm.isValidCompanyToAdd = isValidCompanyToAdd;        

        vm.company = {};
        vm.continueAdd = false;

        activate();

        function activate() {
            resourcesService.getStateProvinces().then(function(response) {
                vm.stateProvinces = response;
            });
            resourcesService.getCountries().then(function (response) {
                vm.countries = response;
            });
            resourcesService.getUserPreferences().then(function (response) {
                var defaultCountry = _.find(response, { preferenceKey: 'Country' });
                if (typeof defaultCountry !== 'undefined' && defaultCountry !== null) {
                    vm.hasDefaultCountry = true;
                    vm.company.locationCountry = vm.company.contactCountry = defaultCountry.preferenceValue;                     
                }
            });
        }

        function showCompanyDetails(company) {            
            $state.go('company.companyDetails', { companyId: company.company_Key });
        }

        function continueAdding() {
            if (!vm.company.companyName || vm.company.companyName.length < 3) {
                return;
            }
            if (vm.intellisenseSearchResults.length) {
                return;
            }
            vm.continueAdd = true;
        }

        function isValidCompanyToAdd(form) {
            return form.$valid;
        }
        
        function companyIntellisense(query) {
            if (query.length < 3) {
                return null;
            }
            return companiesService.companyIntellisense(query).then(function (searchResults) {
                return vm.intellisenseSearchResults = searchResults;
            });
        }

        function contactsIntellisense(query) {
            if (query.length < 3) {
                return null;
            }
            return contactsService.searchContacts({ searchText: query }).then(function (searchResults) {
                return vm.contactsSearchResults = searchResults.resources;
            });
        }

        function selectedParentChange(item) {
            vm.company.parent = item;
        }

        function addCompany(form, company) {
            if (form.$invalid) {
                vm.formSubmitted = true;
                return;
            }
            if (vm.company.parent) {
                company.parentCompany = vm.company.parent.company_Key;
            }
            if (vm.matchedContact) {
                company.contactKey = vm.matchedContact.contactId;
            }
            companiesService.addCompany(company).then(function (result) {
                $state.go('company.companyDetails', { companyId: result.companyID });            
            }, function(error) {
                vm.validationErrors = error.data;
            });
        }
    }
})();
﻿(function () {
    describe('companyEditCompliance.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            companiesService,
            controller,
            $state,
            $mdMedia;

        beforeEach(inject(function (_$controller_, _common_, _companiesService_, _$state_, _$mdMedia_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;
            $state = _$state_;
            $mdMedia = _$mdMedia_;

            // fakes
            var fakeDetails =
            {
                mentionId: '@companyMentionId',
                companyKey: 123,
                companyName: 'Test company',
                isSolicitable: false,
                hasInterestedInDeals: false,
                fcpaName: 'test fcpa name'
            };

            //spys
            spyOn(companiesService, 'getCompanyDetails').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeDetails);
                    }
                };
            });

            spyOn(companiesService, 'updateCompany').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeDetails);
                    }
                };
            });

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn($state, 'go');

            //instantiate controller
            controller = $controller('companyEditComplianceController', {
                common: common,
                companiesService: companiesService,
                $mdMedia: $mdMedia,
                '_': _
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should call get company details', function () {
                expect(controller.companyDetails).toBeDefined();
            });

            it('should set hasProfileChanged to false', function () {
                expect(controller.companyDetails.hasProfileChanged).toBe(false);
            });
        });

        describe('saveCompanyCompliance', function () {

            var expectedMentionId = '@companyMentionId';
            var details =
                {
                    mentionId: expectedMentionId,
                    companyKey: 123,
                    companyName: 'Test company',
                    isSolicitable: false,
                    hasInterestedInDeals: false,
                    fcpaName: 'test fcpa name'
                };

            it('should not try to save company compliance when form is invalid', function () {

                //arrange
                var fakeForm = { $invalid: true, $dirty: true };

                //act
                controller.saveCompanyCompliance(fakeForm, null);

                //assert
                expect(companiesService.updateCompany).not.toHaveBeenCalled();

            });

            it('should save company compliance when form is valid', function () {

                //arrange
                var fakeForm = { $invalid: false, $dirty: true };
                controller.companyMentionId = expectedMentionId;

                //act
                controller.saveCompanyCompliance(fakeForm, details);

                //assert
                expect(companiesService.updateCompany).toHaveBeenCalledWith(details);

            });

            it('should go to company details after save', function () {

                //arrange
                var fakeForm = { $invalid: false, $dirty: true };
                controller.companyMentionId = expectedMentionId;

                //act
                controller.saveCompanyCompliance(fakeForm, details);

                //assert
                expect($state.go).toHaveBeenCalledWith('company.companyDetails', { companyId: expectedMentionId });

            });

            it('should not save company compliance when form nothing changes', function () {

                //arrange
                var fakeForm = { $invalid: true, $dirty: false };

                //act
                controller.saveCompanyCompliance(fakeForm, null);

                //assert
                expect(companiesService.updateCompany).not.toHaveBeenCalled();

            });
        });

    });
})();
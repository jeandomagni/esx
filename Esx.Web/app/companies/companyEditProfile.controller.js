﻿(function () {
    'use strict';
    var controllerId = 'companyEditProfileController';

    angular.module('app').controller(controllerId, companyEditProfileController);

    companyEditProfileController.$inject = ['common', 'companiesService', '$state', '_'];

    function companyEditProfileController(common, companiesService, $state, _) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.companyMentionId = common.$stateParams.companyId;
        vm.companyIntellisense = companyIntellisense;
        vm.selectedParentChange = selectedParentChange;
        vm.saveCompanyProfile = _.debounce(saveCompanyProfile, 200);
        vm.addAlias = _.debounce(addAlias, 200);
        vm.companyDetails = {};
        activate();

        function activate() {
            common.activateController([getCompanyDetails()], controllerId).then(function () { log('Activated Company Edit Profile View.'); });
        }

        function setParentCompany() {
            // set parent company
            if (vm.companyDetails && vm.companyDetails.parentCompany && vm.companyDetails.parentCompany.parentCompanyKey > 0) {
                vm.parentCompany = {
                    company_Key: vm.companyDetails.parentCompany.parentCompanyKey,
                    mentionID: vm.companyDetails.parentCompany.parentMentionId,
                    aliasName: vm.companyDetails.parentCompany.parentCompanyName
                };
                vm.companyHierarchyKey = vm.companyDetails.parentCompany.companyHierarchyKey;
            }
        }

        function getCompanyDetails() {
            return companiesService.getCompanyDetails(vm.companyMentionId).then(function (companyDetails) {
                vm.companyDetails = companyDetails;

                setParentCompany();

                // reset PROFILE update flag
                vm.companyDetails.hasProfileChanged = false;
            });
        }

        function companyIntellisense(query) {
            if (query.length < 3) {
                return null;
            }
            return companiesService.companyIntellisense(query).then(function (searchResults) {
                return vm.intellisenseSearchResults = searchResults;
            });
        }

        function selectedParentChange(item) {
            vm.parentCompany = item;
        }

        function addAlias() {

            var alias = {
                aliasName: vm.aliasName,
                companyKey: vm.companyDetails.companyKey
            };

            return companiesService.addAlias(alias).then(function (data) {

                // add newly added alias to the collection
                vm.companyDetails.aliases.push(data);

                // reset aliasName
                vm.aliasName = '';
            });
        }

        function saveCompanyProfile(form, companyDetails, isContinue) {
                if (form.$invalid) {
                    return;
                }

                // set PROFILE update flag to TRUE to update only company profile
                vm.companyDetails.hasProfileChanged = true;

                if (form.parentCompanyName.$dirty) {
                    // set parent company
                    vm.companyDetails.parentCompany = {
                        companyHierarchyKey: vm.companyHierarchyKey,
                        parentCompanyKey: (vm.parentCompany) ? vm.parentCompany.company_Key : 0,
                        childCompanyKey: (vm.parentCompany) ? vm.companyDetails.companyKey : 0,
                        parentMentionId: (vm.parentCompany) ? vm.parentCompany.mentionID : '',
                        parentCompanyName: (vm.parentCompany) ? vm.parentCompany.aliasName : ''
                    };
                }

                companiesService.updateCompany(companyDetails).then(function () {

                    // retset PROFILE update flag to FALSE
                    vm.companyDetails.hasProfileChanged = false;

                    // Add primary alias if company name changes
                    if (form.name.$dirty) {
                        var primaryAlias = {
                            aliasName: vm.companyDetails.companyName,
                            companyKey: vm.companyDetails.companyKey,
                            isPrimary: true
                        };

                        companiesService.addAlias(primaryAlias).then(function (data) {

                            // change Company title
                            common.setPageHeading(vm.companyDetails.companyName, data.mentionId);
                        });
                    }

                    if (isContinue) {
                        $state.go('companyEdit.compliance', { companyId: vm.companyMentionId });
                    } else {
                        $state.go('company.companyDetails', { companyId: vm.companyMentionId });
                    }
                });
        }
    }
})();
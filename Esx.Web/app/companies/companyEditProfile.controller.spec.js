﻿(function () {
    describe('companyEditProfile.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            companiesService,
            controller,
            $state,
            $mdMedia;

        beforeEach(inject(function (_$controller_, _common_, _companiesService_, _$state_, _$mdMedia_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;
            $state = _$state_;
            $mdMedia = _$mdMedia_;

            // fakes
            var fakeDetails =
            {
                mentionId: 'testMentionId',
                companyKey: 123,
                companyName: 'Test company',
                aliases: [
                {
                    aliasKey: 444,
                    aliasName: 'Test Alias'
                }],
                parentCompany:
                {
                    companyHierarchyKey: 124124,
                    parentCompanyKey: 999,
                    childCompanyKey: 123,
                    parentMentionId: 'parent mention',
                    parentCompanyName: 'parent company name'
                }
            };

            var fakeParentCompany = [{
                mentionID: 'testMentionId',
                aliasName: 'test alias',
                company_key: 123
            }];

            var fakeAlias = {
                aliasKey: 8798,
                aliasName: 'new alias name'
            };

            //spys
            spyOn(companiesService, 'getCompanyDetails').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeDetails);
                    }
                };
            });

            spyOn(companiesService, 'updateCompany').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeDetails);
                    }
                };
            });

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn(companiesService, 'companyIntellisense').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeParentCompany);
                    }
                };
            });

            spyOn(companiesService, 'addAlias').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeAlias);
                    }
                };
            });

            spyOn($state, 'go');

            //instantiate controller
            controller = $controller('companyEditProfileController', {
                common: common,
                companiesService: companiesService,
                $mdMedia: $mdMedia,
                '_': _
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should call get company details', function () {
                expect(controller.companyDetails).toBeDefined();
            });

            it('should set hasProfileChanged to false', function () {
                expect(controller.companyDetails.hasProfileChanged).toBe(false);
            });
        });

        describe('companyIntellisense', function () {

            it('should call companies service to get company intellisense', function () {

                //arrange
                var query = 'testing';

                //act
                controller.companyIntellisense(query);

                //assert
                expect(companiesService.companyIntellisense).toHaveBeenCalled();
            });
        });

        describe('saveCompanyProfile', function () {

            var details =
                {
                    mentionId: 'testMentionId',
                    companyKey: 123,
                    companyName: 'Test company',
                    aliases: [
                    {
                        aliasKey: 444,
                        aliasName: 'Test Alias'
                    }],
                    parentCompany:
                    {
                        companyHierarchyKey: 124124,
                        parentCompanyKey: 999,
                        childCompanyKey: 123,
                        parentMentionId: 'parent mention',
                        parentCompanyName: 'parent company name'
                    },
                    office:
                    {
                        officeId: 345,
                        officeName: 'Test Office',
                        isHeadQuarters: true,
                        physicalAddress:
                        {
                            physicalAddressKey: 567,
                            address1: 'Test Address',
                            city: 'test City'
                        },
                        electronicAddress:
                        [
                            {
                                electronicAddressTypeCode: 'EmailAddress',
                                address: 'test@email.com'
                            },
                            {
                                electronicAddressTypeCode: 'WebPage',
                                address: 'www.testwebpage.com'
                            },
                            {
                                electronicAddressTypeCode: 'PhoneNumber',
                                address: '999-765-6082'
                            }
                        ]
                    }
                };

            it('should not try to save company profile when form is invalid', function () {

                //arrange
                var fakeForm = { $invalid: true, $dirty: true, parentCompanyName: { $dirty: false }, name: { $dirty: false } };

                //act
                controller.saveCompanyProfile(fakeForm, null, true);

                //assert
                expect(companiesService.updateCompany).not.toHaveBeenCalled();

            });

            it('should save company profile when form is valid', function () {

                //arrange
                var fakeForm = { $invalid: false, $dirty: true, parentCompanyName: { $dirty: false }, name: { $dirty: false } };
                var expectedMentionId = '@companyMentionId';
                controller.companyMentionId = expectedMentionId;

                //act
                controller.saveCompanyProfile(fakeForm, details, true);

                //assert
                expect(companiesService.updateCompany).toHaveBeenCalledWith(details);

            });

            it('should save company profile and add alias as primary when company name changes and form is valid', function () {

                //arrange
                var fakeForm = { $invalid: false, $dirty: true, parentCompanyName: { $dirty: false }, name: { $dirty: true } };
                var expectedMentionId = '@companyMentionId';
                controller.companyMentionId = expectedMentionId;

                //act
                controller.saveCompanyProfile(fakeForm, details, true);

                //assert
                expect(companiesService.updateCompany).toHaveBeenCalledWith(details);
                expect(companiesService.addAlias).toHaveBeenCalled();
                expect(controller.companyDetails.aliases.length).toBe(1);

            });

            it('should go to company details after save', function () {

                //arrange
                var fakeForm = { $invalid: false, $dirty: true, parentCompanyName: { $dirty: false }, name: { $dirty: false } };
                var expectedMentionId = '@companyMentionId';
                controller.companyMentionId = expectedMentionId;

                //act
                controller.saveCompanyProfile(fakeForm, details, false);

                //assert
                expect(companiesService.updateCompany).toHaveBeenCalledWith(details);
                expect($state.go).toHaveBeenCalledWith('company.companyDetails', { companyId: expectedMentionId });

            });

            it('should save company profile and go to compliance edit on continue', function () {

                //arrange
                var fakeForm = { $invalid: false, $dirty: true, parentCompanyName: { $dirty: false }, name: { $dirty: false } };
                var expectedMentionId = '@companyMentionId';
                controller.companyMentionId = expectedMentionId;

                //act
                controller.saveCompanyProfile(fakeForm, details, true);

                //assert
                expect(companiesService.updateCompany).toHaveBeenCalledWith(details);
                expect($state.go).toHaveBeenCalledWith('companyEdit.compliance', { companyId: expectedMentionId });

            });
        });

        describe('addAlias', function () {

            it('should call companies service to add alias', function () {

                // arrange
                controller.aliasName = 'test alias';
                controller.companyDetails = {
                    companyKey: 123,
                    aliases: []
                };

                //act
                var result = controller.addAlias();

                //assert
                expect(companiesService.addAlias).toHaveBeenCalled();
                expect(controller.companyDetails.aliases.length).toBe(1);
            });
        });

    });
})();
﻿(function () {
    describe('companyLocationsAdd.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            resourcesService,
            companiesService,
            $scope,
            $state,
            $q,
            $rootScope,
            controller;

        var fakeStates,
            fakeCountries,
            fakeCompanyAdded,
            fakeMentionId;

        beforeEach(inject(function (_$controller_, _common_, _resourcesService_, _companiesService_, _$state_, _$q_, _$rootScope_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            resourcesService = _resourcesService_;
            companiesService = _companiesService_;
            $state = _$state_;
            $q = _$q_;
            $rootScope = _$rootScope_;

            //fakes
            $scope = {};
            fakeStates = [{ stateProvinceCode: 'MN' }];
            fakeCountries = [{ countryCode: 'United States' }];
            fakeCompanyAdded = {companyLocationMentionId:  '@officeMentionId|O'}
            fakeMentionId = fakeCompanyAdded.companyLocationMentionId;
            common.$stateParams.companyId = fakeCompanyAdded.companyLocationMentionId;

            //spies
            spyOn(companiesService, 'addCompanyLocation').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeCompanyAdded);
                    }
                };
            });
            
            spyOn(resourcesService, 'getStateProvinces').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeStates);
                    }
                };
            });
            spyOn(resourcesService, 'getCountries').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeCountries);
                    }
                };
            });

            spyOn($state, 'go');

            //instantiate controller
            controller = $controller('companyLocationsAddController', {
                common: common,
                resourcesService: resourcesService,
                companiesService: companiesService,
                $state: $state
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                //assert
                expect(controller).toBeDefined();
            });

            it('should initialize dependencies correctly', function () {
                //assert
                expect(resourcesService.getStateProvinces).toHaveBeenCalled();
                expect(resourcesService.getCountries).toHaveBeenCalled();
                expect(controller.stateList).toEqual(fakeStates);
                expect(controller.countries).toBe(fakeCountries);
                expect(controller.companyMentionId).toBe(fakeCompanyAdded.companyLocationMentionId);
                expect(controller.location.primary).toBe(false);

            });
        });

        describe('addCompanyLocation', function () {

            it('should not try to add location when form is invalid', function () {

                //arrange
                var fakeForm = { $invalid: true };

                //act
                controller.addCompanyLocation(fakeForm, null);

                //assert
                expect(companiesService.addCompanyLocation).not.toHaveBeenCalled();

            });

            it('should add location when form is valid', function () {

                //arrange
                var expectedLocationToAdd = { name: "office 2" };
                var fakeForm = { $invalid: false };
                var expectedMentionId = '@companyMentionId';
                controller.companyMentionId = expectedMentionId;

                //act
                controller.addCompanyLocation(fakeForm, expectedLocationToAdd);

                //assert
                expect(companiesService.addCompanyLocation).toHaveBeenCalledWith(expectedMentionId, expectedLocationToAdd);

            });

            it('should go to locations list after add', function () {

                //arrange
                var expectedLocationToAdd = { name: "office 2" };
                var fakeForm = { $invalid: false };
                var expectedMentionId = '@companyMentionId';
                controller.companyMentionId = expectedMentionId;

                //act
                controller.addCompanyLocation(fakeForm, expectedLocationToAdd);

                //assert
                expect($state.go).toHaveBeenCalledWith('company.companyLocations', { companyId: expectedMentionId });

            });

            it('should set validation errors', function () {

                //arrange
                var fakeForm = { $invalid: false };

                var expectedMessage = 'you did something wrong!';
                var expectedErrorData = {
                    data: {
                        errors: [
                            {
                                message: expectedMessage
                            }
                        ]
                    }
                };

                companiesService.addCompanyLocation.and.callFake(function() {
                    var deferred = $q.defer();
                    deferred.reject(expectedErrorData);
                    return deferred.promise;
                });

                //act
                controller.addCompanyLocation(fakeForm, {});
                $rootScope.$apply(); // kick the rejected promise

                //assert
                expect(controller.validationErrors).toEqual(expectedErrorData.data);
            });
        });

        describe('selectedAddressChange', function() {

            it('should set address values except for address2', function () {

                //arrange
                var expected = {
                    address1: 'addr1',
                    address2: 'addr2',
                    city: 'city',
                    stateProvince: 'state',
                    zip: 'zip',
                    countryKey: 'country',
                    name: 'name',
                    phone: '5555'
                }

                //act
                controller.selectedAddressChange(expected);

                //assert
                expect(controller.location.address1).toEqual(expected.address1);
                expect(controller.location.city).toEqual(expected.city);
                expect(controller.location.state).toEqual(expected.stateProvince);
                expect(controller.location.zip).toEqual(expected.zip);
                expect(controller.location.countryKey).toEqual(expected.countryKey);
                expect(controller.location.name).toEqual(expected.name);
                expect(controller.location.phone).toEqual(expected.phone);
                expect(controller.location.address2 == undefined);

            });

            it('should not set change address values if newValue is empty', function () {

                //arrange
                var expected = {
                    address1: 'addr1',
                    address2: 'addr2',
                    city: 'city',
                    stateProvince: 'state',
                    zip: 'zip',
                    countryKey: 'country',
                    name: 'name',
                    phone: '5555'
                }

                controller.location.address1 = expected.address1;
                controller.location.city = expected.city;
                controller.location.state = expected.stateProvince;
                controller.location.zip = expected.zip;
                controller.location.countryKey = expected.countryKey;
                controller.location.name = expected.name;
                controller.location.phone = expected.phone;


                //act
                controller.selectedAddressChange(undefined);

                //assert
                expect(controller.location.address1).toEqual(expected.address1);
                expect(controller.location.city).toEqual(expected.city);
                expect(controller.location.state).toEqual(expected.stateProvince);
                expect(controller.location.zip).toEqual(expected.zip);
                expect(controller.location.countryKey).toEqual(expected.countryKey);
                expect(controller.location.name).toEqual(expected.name);
                expect(controller.location.phone).toEqual(expected.phone);
                expect(controller.location.address2 == undefined);

            });

        });
        
        describe('getCompanyLocations', function () {

            var fakeLoctions = {
                resources: [{ address1: 'addr1' }]
            };

            beforeEach(function () {

                spyOn(companiesService, 'getCompanyLocations').and.callFake(function () {
                    return {
                        then: function (callback) {
                            return callback(fakeLoctions);
                        }
                    };
                });
            });

            it('should call companies service to get company Locations', function () {

                //arrange
                var query = 'testing';

                var expectedQueryObject = {
                    companyId: fakeMentionId,
                    searchText: query
                };

                //act
                var actual = controller.getCompanyLocations(query);

                //assert
                expect(companiesService.getCompanyLocations).toHaveBeenCalledWith(expectedQueryObject);
                expect(actual).toEqual(fakeLoctions.resources);
            });

            it('should not call companies service when query less than 3 characters', function () {

                //arrange
                var query = 'te';

                //act
                controller.getCompanyLocations(query);

                //assert
                expect(companiesService.getCompanyLocations).not.toHaveBeenCalled();
            });


        });

        describe('isUsa', function () {

            it('should return true if the selected country is US.', function () {

                //arrange
                controller.location.countryKey = "102";

                //act
                var result = controller.isUsa();

                //assert
                expect(result).toEqual(true);
            });

            it('should return false if the selected country is not US.', function () {

                //arrange
                controller.location.country = "444";

                //act
                var result = controller.isUsa();

                //assert
                expect(result).toEqual(false);
            });
        });
    });
})();



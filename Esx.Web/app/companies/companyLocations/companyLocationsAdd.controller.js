﻿(function () {
    'use strict';
    var controllerId = 'companyLocationsAddController';

    angular.module('app').controller(controllerId, companyLocationsController);

    companyLocationsController.$inject = ['common', 'companiesService', 'resourcesService', '$state'];

    function companyLocationsController(common, companiesService, resourcesService, $state) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.addCompanyLocation = addCompanyLocation;
        vm.companyMentionId = common.$stateParams.companyId;
        vm.getCompanyLocations = getCompanyLocations;
        vm.selectedAddressChange = selectedAddressChange;
        vm.isUsa = isUsa;

        activate();

        function activate() {

            vm.primaryOptions = [{ value: true, label: 'Yes' }, { value: false, label: 'No' }];
            vm.location = {primary: false};

            common.activateController([setCountries(), setStates()], controllerId)
                .then(function () { log('Activated Company Location Add'); });
        }

        function setCountries() {
            resourcesService.getCountries().then(function (response) {
                vm.countries = response;
            });
        }

        function setStates() {
            resourcesService.getStateProvinces().then(function (response) {
                vm.stateList = response;
            });
        }

        function addCompanyLocation(form, companyLocation) {
            if (form.$invalid) {
                return;
            }
           
            companiesService.addCompanyLocation(vm.companyMentionId, companyLocation).then(function () {
                $state.go('company.companyLocations', { companyId: vm.companyMentionId });
            }, function (error) {
                vm.validationErrors = error.data;
                //something
            });
        }

        function getCompanyLocations(query) {
            if (query.length < 3) {
                return [];
            }
            var queryObject = {
                companyId: vm.companyMentionId,
                searchText: query
            };
            return companiesService.getCompanyLocations(queryObject).then(function (searchResults) {
                return vm.companyLocations = searchResults.resources;
            });
        }

        function selectedAddressChange(newItem) {
            if (newItem) {
                vm.location.address1 = newItem.address1;
                vm.location.city = newItem.city;
                vm.location.state = newItem.stateProvince;
                vm.location.zip = newItem.zip;
                vm.location.countryKey = newItem.countryKey;
                vm.location.name = newItem.name;
                vm.location.phone = newItem.phone;
            }
        }

        function isUsa() {
            return vm.location.countryKey === "102";
        }
    }
})();
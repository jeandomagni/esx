﻿(function () {
    'use strict';
    var controllerId = 'companyLocationsController';

    angular.module('app').controller(controllerId, companyLocationsController);

    companyLocationsController.$inject = ['common', 'companiesService', '_', '$location'];

    function companyLocationsController(common, companiesService, _, $location) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.searchText = '';

        vm.locationsList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        };

        vm.queryObject = {
            companyId: common.$stateParams.companyId,
            offset: 0,
            pageSize: 30
        };
        vm.getLocations = _.debounce(getLocations, 200);
        vm.searchLocations = _.debounce(searchLocations, 200);
        vm.sortColumn = _.debounce(sortColumn, 200, { leading: true, trailing: false });
        vm.editLocation = _.debounce(editLocation, 200, { leading: true, trailing: false });
        vm.selectRow = selectRow;

        activate();

        function activate() {
            common.activateController([getLocations()], controllerId)
                .then(function () { log('Activated Company Locations.'); });
        }

        function searchLocations() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.queryObject.offset = 0;
                vm.locationsList.resources.length = 0;
                getLocations(true);
            }
        }

        function getLocations(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            return companiesService.getCompanyLocations(vm.queryObject).then(function (locationsList) {
                vm.locationsList.resources = vm.locationsList.resources.concat(locationsList.resources);
                vm.queryObject.isBusy = false;
                return vm.locationsList;
            });
        }

        function sortColumn(columnName) {
            vm.locationsList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            getLocations();
        }

        function selectRow(location) {
            editLocation(location);
        }

        function editLocation(location) {
            $location.path('/companies/' + common.$stateParams.companyId + '/locations/' + location.officeKey + '/edit');
        }
    }

})();
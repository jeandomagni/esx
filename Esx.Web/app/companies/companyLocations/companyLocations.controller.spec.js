﻿(function () {
    describe('companyLocations.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            companiesService,
            fakeLocations,
            $scope,
            controller,
            _,
            $mdMedia,
            $stateParams,
            $location;

        var activateController = function(context) {
            if (context) {
                if (context.companyId) {
                    common.$stateParams.companyId = context.companyId;
                }
            }

            controller = $controller('companyLocationsController', {
                common: common,
                companiesService: companiesService,
                '_': _,
                $scope: $scope,
                $mdMedia: $mdMedia,
                $stateParams, $stateParams
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _companiesService_, _$mdMedia_, _$location_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;
            _ = window._; // lodash
            $mdMedia = _$mdMedia_;
            $location = _$location_;
            $stateParams = {companyId: 123};

            //fakes
            $scope = {};

            fakeLocations = {
                resources: [{ name: 'some company' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(companiesService, 'getCompanyLocations').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeLocations);
                    }
                };
            });

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn($location, 'path');
        }));

        describe('activate', function () {
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define the query object. ', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.locationsList.pagingData).toEqual(expectedPagingData);
            });
        });

        describe('sort', function () {
            beforeEach(function () {
                activateController();
            });

            it('should sort locations list', function () {
                var columnName = 'companyName';

                //arrange
                var queryObject = { filters: undefined, searchText: '' };

                controller.queryObject = queryObject;

                controller.sortColumn(columnName);

                expect(companiesService.getCompanyLocations).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.locationsList).toEqual(fakeLocations);
            });
        });

        describe('searchLocations', function () {
            beforeEach(function () {
                activateController();
            });

            it('should search locations with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: 'text!'
                };
                
                controller.searchText = "text!";
                controller.queryObject = expectedSearchQuery;
                controller.locationsList.resources.length = 0; // clear locations that "activate" added.

                //act
                controller.searchLocations();

                //assert
                expect(companiesService.getCompanyLocations).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.locationsList).toEqual(fakeLocations);
            });

            it('should not search locations when the search term is under 3 characters', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: 'xx'
                };

                controller.searchText = 'xx';
                controller.locationsList.resources.length = 0; // clear locations that "activate" added.

                //act
                controller.searchLocations();

                //assert
                expect(companiesService.getCompanyLocations).not.toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.locationsList).not.toEqual(fakeLocations);
            });

        });

        describe('getLocations', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get locations with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: 'text!'
                };

                controller.queryObject = expectedSearchQuery;
                controller.locationsList.resources.length = 0; // clear locations that "activate" added.

                //act
                controller.getLocations();

                //assert
                expect(companiesService.getCompanyLocations).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.locationsList).toEqual(fakeLocations);
            });

        });

        describe('editLocation', function () {
            beforeEach(function () {
                activateController({companyId: 123});
            });

            it('should navigate to edit page', function () {

                //arrange
                var location = { officeKey: 456 };

                //act
                controller.editLocation(location);

                //assert
                expect($location.path).toHaveBeenCalledWith('/companies/123/locations/456/edit');
            });

        });
    });
})();



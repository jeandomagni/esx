﻿(function () {
    describe('companyContactAdd.controller', function () {
        beforeEach(module('app'));

        var $controller,
            fakeCompanyContacts,
            fakeCompanyLocations,
            fakeCountries,
            fakeStates,
            common,
            companiesService,
            contactsService,
            resourcesService,
            $location,
            controller,
            $q,
            $rootScope,
            _;

        var activateController = function (context) {
            if (context) {
                if (context.companyId) {
                    common.$stateParams.companyId = context.companyId;
                }
            }

            controller = $controller('companyContactAddController', {
                common: common,
                companiesService: companiesService,
                contactsService: contactsService,
                resourcesService: resourcesService,
                '_': _
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _companiesService_, _resourcesService_, _contactsService_, _$location_, _$q_, _$rootScope_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;
            contactsService = _contactsService_;
            resourcesService = _resourcesService_;
            $location = _$location_;
            $q = _$q_;
            $rootScope = _$rootScope_;
            _ = window._; // lodash

            //fakes
            fakeCompanyContacts = {
                resources: [{ name: 'some company' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            fakeCompanyLocations = {
                resources: [{ name: 'some location' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            fakeCountries = {
                resources: [{ countryName: 'Merica', countryKey: '1' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            fakeStates = {
                resources: [{ stateProvinceCode: 'WI' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn(contactsService, 'searchContacts').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeCompanyContacts);
                    }
                };
            });

            spyOn(companiesService, 'getCompanyLocations').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeCompanyLocations);
                    }
                };
            });

            spyOn(resourcesService, 'getCountries').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeCountries);
                    }
                };
            });

            spyOn(resourcesService, 'getStateProvinces').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeStates);
                    }
                };
            });

            spyOn(companiesService, 'addCompanyContact').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback();
                    }
                };
            });

            spyOn($location, 'path');
        }));

        describe('activate', function () {
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });
        });

        describe('findContacts', function () {
            beforeEach(function () {
                activateController();
            });

            it('should find contacts with the search text', function () {

                //arrange
                var expectedSearchText = "test";

                //act
                var promise = controller.findContacts(expectedSearchText);

                //assert
                expect(contactsService.searchContacts).toHaveBeenCalledWith(expectedSearchText);
                promise.then(function (contacts) {
                    expect(contacts).toEqual(fakeCompanyContacts);
                });
            });
        });

        describe('addContact', function () {
            var companyId = 123;
            beforeEach(function () {
                activateController({
                    companyId: companyId
                });
            });

            it('should not try to add contact when form is invalid', function () {

                //arrange
                var fakeForm = { $invalid: true };

                //act
                controller.addContact(fakeForm);

                //assert
                expect(companiesService.addCompanyContact).not.toHaveBeenCalled();

            });

            it('should call companiesService.addCompanyContact', function () {

                //arrange
                var fakeForm = { $invalid: false };
                var expectedContact = {
                    name: 'Bob'
                };


                //act
                controller.contact = expectedContact;
                controller.addContact(fakeForm);

                //assert
                expect(companiesService.addCompanyContact).toHaveBeenCalledWith(expectedContact);
                expect($location.path).toHaveBeenCalledWith('/companies/' + companyId + '/contacts');
            });

            it('should set validation errors', function () {

                //arrange
                var fakeForm = { $invalid: false };

                var expectedMessage = 'you did something wrong!';
                var expectedErrorData = {
                    data: {
                        errors: [
                            {
                                message: expectedMessage
                            }
                        ]
                    }
                };

                companiesService.addCompanyContact.and.callFake(function () {
                    var deferred = $q.defer();
                    deferred.reject(expectedErrorData);
                    return deferred.promise;
                });

                //act
                controller.addContact(fakeForm);
                $rootScope.$apply(); // kick the rejected promise

                //assert
                expect(controller.validationErrors).toEqual(expectedErrorData.data);
            });

            it('should set the location if officeKey is not specified', function () {
                //arrange
                var fakeForm = { $invalid: false };
                controller.newLocation = { name: 'test' };

                //act
                controller.addContact(fakeForm);

                //assert
                expect(controller.contact.location).toEqual(controller.newLocation);
            });



        });

        describe('findLocations', function () {
            var companyId = 123;
            beforeEach(function () {
                activateController({
                    companyId: companyId
                });
            });

            it('should find locations with the search text', function () {

                //arrange
                var expectedQuery = {
                    companyId: companyId,
                    searchText: "test",
                    pageSize: 100
                };

                //act
                var promise = controller.findLocations(expectedQuery.searchText);

                //assert
                expect(companiesService.getCompanyLocations).toHaveBeenCalledWith(expectedQuery);
                promise.then(function (locations) {
                    expect(locations).toEqual(fakeCompanyLocations);
                });
            });
        });

        describe('addLocation', function () {
            var companyId = 123;
            beforeEach(function () {
                activateController({
                    companyId: companyId
                });
            });

            it('should populate countries, states, the location name, and set the location form flag to true.', function () {

                //arrange
                controller.addingNewLocation = false;
                controller.locationSearchText = "test";
                controller.newLocation.name = '';

                //act
                controller.addLocation();

                //assert
                expect(resourcesService.getCountries).toHaveBeenCalled();
                expect(resourcesService.getStateProvinces).toHaveBeenCalled();
                expect(controller.newLocation.name).toEqual(controller.locationSearchText);
                expect(controller.addingNewLocation).toEqual(true);
            });
        });

        describe('isUsa', function () {
            beforeEach(function () {
                activateController();
            });

            it('should return true if the selected country is US.', function () {

                //arrange
                controller.newLocation.countryKey = "102";

                //act
                var result = controller.isUsa();

                //assert
                expect(result).toEqual(true);
            });

            it('should return false if the selected country is not US.', function () {

                //arrange
                controller.newLocation.country = "444";

                //act
                var result = controller.isUsa();

                //assert
                expect(result).toEqual(false);
            });
        });

        describe('selectContact', function () {
            beforeEach(function () {
                activateController();
            });

            it('should set the selectedContact with the specified contact.', function () {

                //arrange
                var expectedContact = { contactID: 123 };
                controller.selectedContact = null;

                //act
                controller.selectContact(expectedContact);

                //assert
                expect(controller.selectedContact).toEqual(expectedContact);
            });
        });

        describe('selectLocation', function () {
            beforeEach(function () {
                activateController();
            });

            it('should set the selectedLocation with the specified location.', function () {

                //arrange
                var expectedLocation = { officeKey: '123' };
                controller.selectedLocation = null;

                //act
                controller.selectLocation(expectedLocation);

                //assert
                expect(controller.contact.officeKey).toEqual(expectedLocation.officeKey);
            });
        });
    });
})();



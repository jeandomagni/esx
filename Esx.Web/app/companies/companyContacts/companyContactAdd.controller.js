﻿(function () {
    'use strict';
    var controllerId = 'companyContactAddController';

    angular.module('app').controller(controllerId, companyContactAddController);

    companyContactAddController.$inject = ['common', 'companiesService', '_', '$location', 'resourcesService', 'contactsService', '$q'];

    function companyContactAddController(common, companiesService, _, $location, resourcesService, contactsService, $q) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logSuccess = getLogFn(controllerId, 'success');
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.companyMentionId = common.$stateParams.companyId;
        vm.selectedContact = null;
        vm.states = null;
        vm.countries = null;
        vm.contact = {
            contactName: ''
        };
        vm.newLocation = { isPrimary: false };
        vm.isUsa = isUsa;
        vm.findContacts = findContacts;
        vm.selectContact = selectContact;
        vm.addContact = _.debounce(addContact, 200);
        vm.findLocations = findLocations;
        vm.selectLocation = selectLocation;
        vm.addLocation = addLocation;

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () { log('Activated Company Contact Add.'); });
        }

        function findContacts(searchText) {
            var returnVal = [];
            if (searchText != null && searchText.length > 2) {
                returnVal = contactsService.searchContacts(searchText).then(function (results) {
                    return results;
                });
            }
            return $q.when(returnVal);
        }

        function addContact(form) {
            if (form.$invalid) return null;

            if (!(vm.contact.officeKey > 0)) {
                vm.contact.location = vm.newLocation;
            }

            vm.contact.companyId = vm.companyMentionId;
            return companiesService.addCompanyContact(vm.contact).then(function () {
                logSuccess('The contact was successfully added.');
                $location.path('/companies/' + vm.companyMentionId + '/contacts');
            }, function (error) {
                vm.validationErrors = error.data;
            });
        }

        function getCountries() {
            return resourcesService.getCountries().then(function (result) {
                vm.countries = result;
            });
        }

        function getStates() {
            return resourcesService.getStateProvinces().then(function (result) {
                vm.states = result;
            });
        }

        function findLocations(searchText) {
            var returnVal = [];
            var query = {
                companyId: vm.companyMentionId,
                searchText: searchText,
                pageSize: 100
            };
            returnVal = companiesService.getCompanyLocations(query).then(function (results) {
                return results.resources;
            });
            return $q.when(returnVal);
        }

        function addLocation() {
            getCountries();
            getStates();
            vm.newLocation.name = vm.locationSearchText;
            vm.addingNewLocation = true;
        }

        function isUsa() {
            return vm.newLocation.countryKey === "102";
        }

        function selectContact(contact) {
            vm.selectedContact = contact;
        }

        function selectLocation(location) {
            vm.contact.officeKey = location.officeKey;
        }

    }

})();
﻿(function () {
    'use strict';
    var controllerId = 'companyContactsController';

    angular.module('app').controller(controllerId, companyContactsController);

    companyContactsController.$inject = ['common', 'companiesService', '$stateParams', '_', 'mentionsService', '$location', '$timeout'];

    function companyContactsController(common, companiesService, $stateParams, _, mentionsService, $location, $timeout) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.companyId = $stateParams.companyId;
        vm.searchText = '';

        vm.companyContactList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        };

        vm.queryObject = {
            companyId: $stateParams.companyId,
            offset: 0,
            pageSize: 30
        };
        vm.getCompanyContacts = _.debounce(getCompanyContacts, 200);
        vm.searchCompanyContacts = _.debounce(searchCompanyContacts, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.toggleIsWatched = _.debounce(toggleIsWatched, 200);
        vm.editContact = editContact;
        vm.selectRow = _.debounce(selectRow, 200);

        activate();

        function activate() {
            common.activateController([getCompanyContacts()], controllerId)
                .then(function () { log('Activated Company Contacts.'); });
        }

        function searchCompanyContacts() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.queryObject.offset = 0;
                vm.companyContactList.resources.length = 0;
                getCompanyContacts(true);
            }
        }

        function getCompanyContacts(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            return companiesService.getCompanyContacts(vm.queryObject).then(function (companyContactList) {
                vm.companyContactList.resources = vm.companyContactList.resources.concat(companyContactList.resources);
                vm.queryObject.isBusy = false;
                return vm.companyContactList;
            });
        }

        function sortColumn(columnName) {
            vm.companyContactList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            getCompanyContacts();
        }

        function selectRow(contact) {
            $timeout(function() {
                if (vm.rowAction === 'toggleWatch')
                    toggleIsWatched(contact);
                else
                    vm.editContact(contact);
            });
        }

        function editContact(contact) {
            $location.path('/contacts/' + contact.contactMentionId);
        }

        function toggleIsWatched(contact) {
            contact.isWatched = !contact.isWatched;
            mentionsService.setIsWatched(contact.contactMentionId, contact.isWatched);
        }
    }

})();
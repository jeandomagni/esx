﻿(function () {
    describe('companyContacts.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            companiesService,
            mentionsService,
            fakeCompanyContacts,
            $scope,
            controller,
            _;

        var activateController = function(context) {
            if (context) {
                if (context.companyId) {
                    common.$stateParams.companyId = context.companyId;
                }
            }

            controller = $controller('companyContactsController', {
                common: common,
                companiesService: companiesService,
                mentionsService: mentionsService,
                '_': _
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _companiesService_, _mentionsService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;
            mentionsService = _mentionsService_;
            _ = window._; // lodash

            //fakes
            $scope = {
                $apply: angular.noop
            };

            fakeCompanyContacts = {
                resources: [{ name: 'some company' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn(companiesService, 'getCompanyContacts').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeCompanyContacts);
                    }
                };
            });

            spyOn(mentionsService, 'setIsWatched');
        }));

        describe('activate', function () {
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define the query object. ', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.companyContactList.pagingData).toEqual(expectedPagingData);
            });
        });

        describe('sort', function () {
            beforeEach(function () {
                activateController();
            });

            it('should sort contacts list', function () {
                var columnName = 'Name';

                //arrange
                var queryObject = { filters: undefined, searchText: '' };

                controller.queryObject = queryObject;

                controller.sortColumn(columnName);

                expect(companiesService.getCompanyContacts).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.companyContactList).toEqual(fakeCompanyContacts);
            });
        });

        describe('getCompanyContacts', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get contacts with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    offset: 0,
                    pageSize: 30
                };

                controller.queryObject = expectedSearchQuery;
                controller.companyContactList.resources.length = 0; // clear contacts that "activate" added.

                //act
                controller.getCompanyContacts();

                //assert
                expect(companiesService.getCompanyContacts).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.companyContactList).toEqual(fakeCompanyContacts);
            });

        });

        describe('searchCompanyContacts', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get contacts with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: 'text!'
                };

                controller.queryObject = expectedSearchQuery;
                controller.companyContactList.resources.length = 0; 

                //act
                controller.searchCompanyContacts();

                //assert
                expect(companiesService.getCompanyContacts).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.companyContactList).toEqual(fakeCompanyContacts);
            });

            it('should not get contacts with the query object when searchText < 3', function () {

                //arrange
                companiesService.getCompanyContacts.calls.reset(); //reset the spy
                controller.searchText = 'aa';
                controller.companyContactList.resources.length = 0; 

                //act
                controller.searchCompanyContacts();

                //assert
                expect(companiesService.getCompanyContacts).not.toHaveBeenCalled();
                expect(controller.companyContactList.resources.length).toBe(0);
            });

        });

        describe('toggleIsWatched', function () {
            beforeEach(function () {
                activateController();
            });

            it('should send false to the properties service when the current state is true', function () {
                // Arrange
                var contact = {
                    contactMentionId: "@me",
                    isWatched: true
                };

                // Act
                controller.toggleIsWatched(contact);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(contact.contactMentionId, false);
            });

            it('should send true to the properties service when the current state is false', function () {
                // Arrange
                var contact = {
                    contactMentionId: "@me",
                    isWatched: false
                };

                // Act
                controller.toggleIsWatched(contact);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(contact.contactMentionId, true);
            });
        });

    });
})();



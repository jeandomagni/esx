﻿(function () {
    'use strict';

    var serviceId = 'companiesService';

    angular.module('app').factory(serviceId, companiesService, _);

    companiesService.$inject = ['uriService', '$resource', '$http'];

    function companiesService(uriService, $resource, $http) {

        var resource = $resource(
            uriService.company,
            { companyId: '@id' },
            {
                'update': { method: 'PATCH' }
            }
        );

        var service = {
            getCompanies: getCompanies,
            getCompanyStatistics: getCompanyStatistics,
            getCompanyName: getCompanyName,
            addCompany: addCompany,
            updateCompany: updateCompany,
            getCompanyContacts: getCompanyContacts,
            addCompanyContact: addCompanyContact,
            getCompanyProperties: getCompanyProperties,
            getCompanyLoans: getCompanyLoans,
            getCompanyDeals: getCompanyDeals,
            getCompanyDetails: getCompanyDetails,
            companyIntellisense: companyIntellisense,
            getCompanyLocations: getCompanyLocations,
            addCompanyLocation: addCompanyLocation,
            addAlias: addAlias,
            addCompanyLoan: addCompanyLoan
        };

        return service;

        function getCompanies(query) {
            return $resource(uriService.companyList).get(query).$promise;
        }

        function getCompanyStatistics(company) {
            return $resource(uriService.companyStatistics).get({ mentionId: company.mentionID }).$promise;
        }

        function getCompanyName(id) {
            return $resource(uriService.companyName).get({ companyId: id }).$promise;
        }

        function getCompanyDetails(mentionId) {
            return $resource(uriService.companyDetails).get({ mentionId: mentionId }).$promise;
        }

        function addCompany(company) {
            return resource.save(company).$promise;
        }

        function addAlias(alias) {
            return $resource(uriService.addAlias).save(alias).$promise;
        }

        function updateCompany(company) {
            return resource.update(company).$promise;
        }

        function getCompanyContacts(query) {
            return $resource(uriService.companyContacts).get(query).$promise;
        }

        function addCompanyContact(contact) {
            return $resource(uriService.companyContacts, { companyId: '@companyId' }).save(contact).$promise;
        }

        function getCompanyProperties(query) {
            return $resource(uriService.companyProperties, { companyId: '@companyId' }).get(query).$promise;
        }

        function getCompanyLoans(query) {
            return $resource(uriService.companyLoans, { companyId: '@companyId' }).get(query).$promise;
        }

        function getCompanyDeals(query) {
            return $resource(uriService.companyDeals, { companyId: '@companyId' }).get(query).$promise;
        }

        function companyIntellisense(query) {
            return $resource(uriService.companyIntellisense, { query: '@query' }).query({ query: query }).$promise;
        }

        function getCompanyLocations(query) {
            return $resource(uriService.companyLocations, { companyId: '@companyId' }).get(query).$promise;
        }

        function addCompanyLocation(id, location) {
            var path = uriService.companyLocations.replace(':companyId', id);
            return $resource(path).save(location).$promise;
        }

        function addCompanyLoan(id, loan) {
            var path = uriService.companyLoans.replace(':companyId', id);
            return $resource(path).save(loan).$promise;
        }

    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'companyHistoryController';

    angular.module('app').controller(controllerId, companyHistoryController);

    companyHistoryController.$inject = ['common', 'companiesService', '$mdUtil'];

    function companyHistoryController(common, companiesService, $mdUtil) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Company History';

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () { log('Activated Company History View'); });
        }

    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'companiesController';

    angular.module('app').controller(controllerId, companiesController);

    companiesController.$inject = ['common', 'companiesService', 'mentionsService', '_', '$location', '$timeout'];

    function companiesController(common, companiesService, mentionsService, _, $location, $timeout) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Companies';
        vm.searchText = '';

        vm.companyList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        }

        vm.queryObject = {
            offset: 0,
            pageSize: 30
        };
        vm.getCompanies = _.debounce(getCompanies, 200);
        vm.searchCompanies = _.debounce(searchCompanies, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.toggleIsWatched = _.debounce(toggleIsWatched, 200);
        vm.selectRow = selectRow;

        activate();

        function activate() {
            common.activateController([getCompanies(false)], controllerId)
                .then(function () { });

        }

        function searchCompanies() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.queryObject.offset = 0;
                vm.companyList.resources.length = 0;
                return getCompanies(true);
            }
            return null;
        }

        function getCompanies(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            return companiesService.getCompanies(vm.queryObject).then(function (companyList) {
                vm.companyList.resources = vm.companyList.resources.concat(companyList.resources);
                vm.queryObject.isBusy = false;
                return vm.companyList;
            });
        }

        function sortColumn(columnName) {
            vm.companyList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            getCompanies(false);
        }

        function toggleIsWatched(company) {
            company.isWatched = !company.isWatched;
            mentionsService.setIsWatched(company.mentionID, company.isWatched);
        }

        function editCompany(company) {
            $location.path('/companies/' + company.mentionID + '/summary');
        }

        function selectRow(company) {
            $timeout(function () {
                if (vm.rowAction === 'toggleWatch')
                    toggleIsWatched(company);
                else
                    editCompany(company);
            });
        }
    }
})();
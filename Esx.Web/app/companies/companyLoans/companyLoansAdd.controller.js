﻿(function () {
    'use strict';
    var controllerId = 'companyLoansAddController';

    angular.module('app').controller(controllerId, companyLoansController);

    companyLoansController.$inject = ['common', 'companiesService', 'propertiesService', 'resourcesService', '$state'];

    function companyLoansController(common, companiesService, propertiesService, resourcesService, $state) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.addCompanyLoan = addCompanyLoan;
        vm.companyMentionId = common.$stateParams.companyId;

        vm.changeBorroweLender = changeBorroweLender;
        vm.companyIntellisense = companyIntellisense;
        vm.selectedCompanyChange = selectedCompanyChange;
        vm.selectedPropertyChange = selectedPropertyChange;
        vm.propertyIntellisense = propertyIntellisense;
        vm.originationDate = null;
        vm.maturityDate = null;

        vm.loan = {
            propertyKey: 0,
            loanName: '',
            loanAmount: null,
            valuation: null,
            isBorrower: true,
            isLender: false,
            borrowerLenderCompanyKey: 0,
            originationDate: null,
            maturationDate: null
        };

        vm.propertyTypes = [
            { type: "Property" }
        ];

        activate();

        function activate() {
            common.activateController(controllerId)
                .then(function () { log('Activated Company Loan Add'); });
        }

        function changeBorroweLender(isBorrower) {
            vm.loan.isBorrower = isBorrower;
            vm.loan.isLender = !isBorrower;
        }

        function companyIntellisense(query) {
            if (query.length < 3) {
                return null;
            }
            return companiesService.companyIntellisense(query).then(function (searchResults) {
                return vm.intellisenseSearchResults = searchResults;
            });
        }

        function selectedCompanyChange(item) {
            vm.borrowerLenderCompany = item;
        }

        function propertyIntellisense(query) {
            if (query.length < 3) {
                return null;
            }

            return propertiesService.propertyIntellisense(query).then(function (searchResults) {
                return searchResults;
            });
        }

        function selectedPropertyChange(item) {
            vm.property = item;
        }


        function addCompanyLoan(form, loan) {
            if (form.$invalid) {
                return;
            }

            //TODO - REMOVE
            //This was added to short circut the new required fields in the data model 
            loan.loanBenchmarkType = "E";
            loan.loanCashMgmtType = "DSCR";
            loan.loanPaymentType = "DF";
            loan.loanRateType = "E";
            loan.loanSourceType = "A";
            loan.loanRate = 3;
            //END 

            vm.loan.borrowerLenderCompanyKey = vm.borrowerLenderCompany.company_Key;
            vm.loan.propertyKey = vm.property.propertyKey;

            companiesService.addCompanyLoan(vm.companyMentionId, loan).then(function () {
                $state.go('company.companyLoans', { companyId: vm.companyMentionId });
            });
        }


    }

})();
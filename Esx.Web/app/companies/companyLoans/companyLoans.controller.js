﻿(function () {
    'use strict';

    var controllerId = 'companyLoansController';

    angular.module('app').controller(controllerId, companyLoansController);

    companyLoansController.$inject = ['common', '$timeout', '_', 'mentionsService', 'companiesService'];

    function companyLoansController(common, $timeout, _, mentionsService, companiesService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        vm.searchText = '';

        vm.loansList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        };

        vm.queryObject = {
            companyId: common.$stateParams.companyId,
            offset: 0,
            pageSize: 30
        };
        vm.getCompanyLoans = _.debounce(getCompanyLoans, 200);
        vm.searchCompanyLoans = _.debounce(searchCompanyLoans, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.selectRow = selectRow;
        vm.toggleIsWatched = _.debounce(toggleIsWatched, 200);

        vm.title = 'Company Loans';

        vm.loans = [];

        activate();

        function activate() {
            common.activateController([getCompanyLoans()], controllerId)
                .then(function () { log('Activated Company Loans View'); });
        }

        function searchCompanyLoans() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.loansList.resources.length = 0;
                vm.queryObject.offset = 0;
                getCompanyLoans(true);
            }
            return null;
        }

        function getCompanyLoans(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            return companiesService.getCompanyLoans(vm.queryObject).then(function (loansList) {
                vm.loansList.resources = vm.loansList.resources.concat(loansList.resources);
                vm.queryObject.isBusy = false;
                return vm.loansList;
            });
        }

        function sortColumn(columnName) {
            vm.loansList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            getCompanyLoans();
        }

        function selectRow(loan) {
            $timeout(function () {
                if (vm.rowAction === 'toggleWatch')
                    toggleIsWatched(loan);
                else
                    editLoan(loan);
            });
        }

        function editLoan(loan) {
            //TODO: Implement missing method
        }

        function toggleIsWatched(loan) {
            loan.isWatched = !loan.isWatched;
            mentionsService.setIsWatched(loan.loanMentionId, loan.isWatched);
        }

    }
})();
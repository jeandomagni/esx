﻿
(function () {
    describe('companyLoans.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            companiesService,
            mentionsService,
            fakeCompanyLoans,
            $scope,
            controller,
            _,
            $mdMedia;

        beforeEach(inject(function (_$controller_, _common_, _$mdMedia_, _mentionsService_, _companiesService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;
            mentionsService = _mentionsService_;
            _ = window._; // lodash
            $mdMedia = _$mdMedia_;

            //fakes
            $scope = {
                $apply: angular.noop
            };

            fakeCompanyLoans = {
                resources: [{ name: 'test name' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(companiesService, 'getCompanyLoans').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeCompanyLoans);
                    }
                };
            });

            spyOn(mentionsService, 'setIsWatched');

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            //instantiate controller
            var companyId = 123;
            common.$stateParams.companyId = companyId;
            controller = $controller('companyLoansController', {
                common: common,
                companiesService: companiesService,
                '_': _,
                $scope: $scope,
                $mdMedia: $mdMedia
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define the query object. ', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.loansList.pagingData).toEqual(expectedPagingData);
            });
        });

        describe('sort', function () {
            it('should sort loans list', function () {
                var columnName = 'name';

                //arrange
                var queryObject = { filters: undefined, searchText: '' };

                controller.queryObject = queryObject;

                controller.sortColumn(columnName);

                expect(companiesService.getCompanyLoans).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.loansList).toEqual(fakeCompanyLoans);
            });
        });

        describe('getCompanyLoans', function () {

            it('should get loans with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    offset: 0,
                    pageSize: 30
                };

                controller.queryObject = expectedSearchQuery;
                controller.loansList.resources.length = 0; 

                //act
                controller.getCompanyLoans();

                //assert
                expect(companiesService.getCompanyLoans).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.loansList).toEqual(fakeCompanyLoans);
            });

        });

        describe('searchCompanyLoans', function () {

            it('should get loans with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: "test"
                };

                controller.queryObject = expectedSearchQuery;
                controller.loansList.resources.length = 0;

                //act
                controller.searchCompanyLoans();

                //assert
                expect(companiesService.getCompanyLoans).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.loansList).toEqual(fakeCompanyLoans);
            });

            it('should not get loans with the query object when searchText < 3', function () {

                companiesService.getCompanyLoans.calls.reset(); //reset the spy
                controller.searchText = 'aa';
                controller.loansList.resources.length = 0;

                //act
                controller.searchCompanyLoans();

                //assert
                expect(companiesService.getCompanyLoans).not.toHaveBeenCalled();
                expect(controller.loansList.resources.length).toBe(0);
            });

        });

        describe('toggleIsWatched', function () {
            it('should send false to the company service when the current state is true', function () {
                // Arrange
                var loan = {
                    loanMentionId: '@test',
                    isWatched: true
                };

                // Act
                controller.toggleIsWatched(loan);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(loan.loanMentionId, false);
            });

            it('should send true to the company service when the current state is false', function () {
                // Arrange
                var loan = {
                    loanMentionId: '@test',
                    isWatched: false
                };

                // Act
                controller.toggleIsWatched(loan);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(loan.loanMentionId, true);
            });
        });
    });
})();


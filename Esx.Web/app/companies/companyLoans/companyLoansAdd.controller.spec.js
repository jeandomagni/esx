﻿(function () {
    describe('companyLoansAdd.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            propertiesService,
            companiesService,
            $state,
            controller;

        var fakeStates,
            fakeCountries,
            fakeCompanyAdded,
            fakeMentionId;

        beforeEach(inject(function (_$controller_, _common_, _companiesService_, _propertiesService_, _$state_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            propertiesService = _propertiesService_;
            companiesService = _companiesService_;
            $state = _$state_;

            //fakes
            fakeStates = [{ stateProvinceCode: 'MN' }];
            fakeCountries = [{ countryCode: 'United States' }];
            fakeCompanyAdded = { companyLoanMentionId: '@officeMentionId|O', propertyKey: 123, loanName: 'test loan', loanAmount: 750, borrowerLenderCompanyKey: 3456, isBorrower: true };
            fakeMentionId = fakeCompanyAdded.companyLoanMentionId;
            common.$stateParams.companyId = fakeCompanyAdded.companyLoanMentionId;

            //spies
            spyOn(companiesService, 'addCompanyLoan').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeCompanyAdded);
                    }
                };
            });

            spyOn($state, 'go');

            //instantiate controller
            controller = $controller('companyLoansAddController', {
                common: common,
                resourcesService: propertiesService,
                companiesService: companiesService,
                $state: $state
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                //assert
                expect(controller).toBeDefined();
            });
        });

        describe('addCompanyLoan', function () {

            it('should not try to add loan when form is invalid', function () {

                //arrange
                var fakeForm = { $invalid: true };

                //act
                controller.addCompanyLoan(fakeForm, null);

                //assert
                expect(companiesService.addCompanyLoan).not.toHaveBeenCalled();

            });

            it('should add loan when form is valid', function () {

                //arrange
                var expectedLoanToAdd = { propertyKey: 1234 };
                var fakeForm = { $invalid: false };
                var expectedMentionId = '@companyMentionId';
                controller.borrowerLenderCompany = { company_key: 7988 };
                controller.property = { propertyKey: 837 };
                controller.companyMentionId = expectedMentionId;

                //act
                controller.addCompanyLoan(fakeForm, expectedLoanToAdd);

                //assert
                expect(companiesService.addCompanyLoan).toHaveBeenCalledWith(expectedMentionId, expectedLoanToAdd);

            });

            it('should go to loans list after add', function () {

                //arrange
                var expectedLoanToAdd = { propertyKey: 1234 };
                var fakeForm = { $invalid: false };
                var expectedMentionId = '@companyMentionId';
                controller.companyMentionId = expectedMentionId;
                controller.borrowerLenderCompany = { company_key: 7988 };
                controller.property = { propertyKey: 837 };

                //act
                controller.addCompanyLoan(fakeForm, expectedLoanToAdd);

                //assert
                expect($state.go).toHaveBeenCalledWith('company.companyLoans', { companyId: expectedMentionId });

            });
        });

    });
})();



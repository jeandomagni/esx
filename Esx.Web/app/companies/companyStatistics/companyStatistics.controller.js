﻿(function () {
    'use strict';

    var controllerId = 'companyStatisticsController';

    angular.module('app').controller(controllerId, companyStatisticsController);

    companyStatisticsController.$inject = ['common', 'companiesService'];

    function companyStatisticsController(common, companiesService) {
        var company;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.companyStatistics = {};

        vm.getCompanyStatistics = getCompanyStatistics;
        vm.setCompany = setCompany;

        

        function activate() {
            common.activateController([getCompanyStatistics()], controllerId)
                .then(function () {  });
        }

        function setCompany(companyObj) {
            company = companyObj;
            activate();
        }

        function getCompanyStatistics() {
            return companiesService.getCompanyStatistics(company).then(function (companyStatistics) {
                return vm.companyStatistics = companyStatistics;
            });
        }

    }
})();
﻿(function () {
    describe('companyStatistics.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            companiesService,
            fakeSummary,
            fakeCompany,
            controller;

        beforeEach(inject(function (_$controller_, _common_, _companiesService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;

            //fakes
            fakeSummary = {
                ActiveDeals: 1,
                Investments: 2,
                BidVolume: "1M",
                Locations: 3
            };

            fakeCompany = {};

            //spys
            spyOn(companiesService, 'getCompanyStatistics').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeSummary);
                    }
                };
            });

            //instantiate controller
            controller = $controller('companyStatisticsController', {
                common: common,
                companiesService: companiesService
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should set company summary', function () {

                //act
                controller.setCompany(fakeCompany);

                //assert
                expect(companiesService.getCompanyStatistics).toHaveBeenCalled();
                expect(controller.companyStatistics).toEqual(fakeSummary);
            });
        });

        describe('getCompanyStatistics', function () {

            it('should get the company summary by the mention id', function () {

                // Arrange
                var company = {};

                // Act
                controller.setCompany(company);
                controller.getCompanyStatistics();

                // Assert
                expect(companiesService.getCompanyStatistics).toHaveBeenCalledWith(company);
            });

        });
    });
})();



﻿(function () {
    'use strict';
    var controllerId = 'companyPropertiesController';

    angular.module('app').controller(controllerId, companyPropertiesController);

    companyPropertiesController.$inject = ['common', 'companiesService', 'mentionsService', '$stateParams', '_', '$location', '$timeout'];

    function companyPropertiesController(common, companiesService, mentionsService, $stateParams, _, $location, $timeout) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.searchText = '';

        vm.propertiesList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        };

        vm.queryObject = {
            companyId: $stateParams.companyId,
            pageSize: 30,
            offset: 0
        };
        vm.getProperties = _.debounce(getProperties, 400);
        vm.searchProperties = _.debounce(searchProperties, 200);
        vm.sortColumn = _.debounce(sortColumn, 200, { leading: true, trailing: false });
        vm.editProperty = _.debounce(editProperty, 200, { leading: true, trailing: false });
        vm.toggleIsWatched = _.debounce(toggleIsWatched, 200);
        vm.selectRow = selectRow;

        activate();

        function activate() {
            common.activateController([getProperties(false)], controllerId)
                .then(function () { log('Activated Company Properties.'); });
        }

        function searchProperties() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.propertiesList.resources.length = 0;
                vm.queryObject.offset = 0;
                getProperties(true);
            }
        }

        function getProperties(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            return companiesService.getCompanyProperties(vm.queryObject).then(function (propertiesList) {
                vm.propertiesList.resources = vm.propertiesList.resources.concat(propertiesList.resources);
                vm.queryObject.isBusy = false;
                return vm.propertiesList;
            });
        }

        function sortColumn(columnName) {
            vm.propertiesList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            getProperties(false);
        }

        function selectRow(property) {
            $timeout(function() {
                if (vm.rowAction === 'toggleWatch')
                    toggleIsWatched(property);
                else
                    vm.editProperty(property);
            });
        }

        function editProperty(property) {
            $location.path('/properties/' + property.propertyID);
        }

        function toggleIsWatched(property) {
            property.isWatched = !property.isWatched;
            mentionsService.setIsWatched(property.propertyID, property.isWatched);
        }
    }

})();
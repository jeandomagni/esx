﻿(function () {
    describe('companyProperties.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            companiesService,
            mentionsService,
            fakeProperties,
            $scope,
            controller,
            _,
            $mdMedia,
            $location;

        var activateController = function (context) {
            if (context) {
                if (context.companyId) {
                    common.$stateParams.companyId = context.companyId;
                }
            }

            controller = $controller('companyPropertiesController', {
                common: common,
                companiesService: companiesService,
                mentionsService: mentionsService,
                '_': _,
                '$scope': $scope,
                $mdMedia: $mdMedia
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _companiesService_, _mentionsService_, _$mdMedia_, _$location_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            companiesService = _companiesService_;
            mentionsService = _mentionsService_;
            _ = window._; // lodash
            $mdMedia = _$mdMedia_;
            $location = _$location_;

            //fakes
            $scope = {};

            fakeProperties = {
                resources: [{ name: 'some company' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(companiesService, 'getCompanyProperties').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeProperties);
                    }
                };
            });

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn($location, 'path');
            spyOn(mentionsService, 'setIsWatched');
        }));

        describe('activate', function () {
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define the query object. ', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.propertiesList.pagingData).toEqual(expectedPagingData);
            });
        });

        describe('sort', function () {
            beforeEach(function () {
                activateController();
            });

            it('should sort properties list', function () {
                var columnName = 'companyName';

                //arrange
                var queryObject = { filters: undefined, searchText: '' };

                controller.queryObject = queryObject;

                controller.sortColumn(columnName);

                expect(companiesService.getCompanyProperties).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.propertiesList).toEqual(fakeProperties);
            });
        });

        describe('searchProperties', function () {
            beforeEach(function () {
                activateController();
            });

            it('should search properties with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: 'text!'
                };

                controller.searchText = "text!";
                controller.queryObject = expectedSearchQuery;
                controller.propertiesList.resources.length = 0; // clear properties that "activate" added.

                //act
                controller.searchProperties();

                //assert
                expect(companiesService.getCompanyProperties).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.propertiesList).toEqual(fakeProperties);
            });

            it('should not search properties when the search term is under 3 characters', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: 'xx'
                };

                controller.searchText = 'xx';
                controller.propertiesList.resources.length = 0; // clear properties that "activate" added.

                //act
                controller.searchProperties();

                //assert
                expect(companiesService.getCompanyProperties).not.toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.propertiesList).not.toEqual(fakeProperties);
            });

        });

        describe('getProperties', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get properties with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: 'text!'
                };

                controller.queryObject = expectedSearchQuery;
                controller.propertiesList.resources.length = 0; // clear properties that "activate" added.

                //act
                controller.getProperties(true);

                //assert
                expect(companiesService.getCompanyProperties).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.propertiesList).toEqual(fakeProperties);
            });

        });

        describe('editProperty', function () {
            beforeEach(function () {
                activateController();
            });

            it('should navigate to edit page', function () {

                //arrange
                var property = { propertyID: 456 };

                //act
                controller.editProperty(property);

                //assert
                expect($location.path).toHaveBeenCalledWith('/properties/456');
            });

        });

        describe('toggleIsWatched', function () {
            beforeEach(function () {
                activateController();
            });

            it('should send false to the properties service when the current state is true', function () {
                // Arrange
                var property = {
                    propertyID: 123,
                    isWatched: true
                };

                // Act
                controller.toggleIsWatched(property);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(property.propertyID, false);
            });

            it('should send true to the properties service when the current state is false', function () {
                // Arrange
                var property = {
                    propertyID: 123,
                    isWatched: false
                };

                // Act
                controller.toggleIsWatched(property);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(property.propertyID, true);
            });
        });
    });
})();



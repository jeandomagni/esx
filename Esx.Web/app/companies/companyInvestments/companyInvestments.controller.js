﻿(function () {
    'use strict';
    var controllerId = 'companyInvestmentsController';

    angular.module('app').controller(controllerId, companyInvestmentsController);

    companyInvestmentsController.$inject = ['common', 'companiesService'];

    function companyInvestmentsController(common, companiesService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Company Investments';
    }



})();
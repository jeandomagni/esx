﻿(function () {
    'use strict';

    var controllerId = 'engageDetail';

    angular.module('app').controller(controllerId, engageDetail);

    engageDetail.$inject = ['common', 'engageService', '$stateParams', '$mdDialog', 'commentsService'];

    function engageDetail(common, engageService, $stateParams, $mdDialog, commentsService) {
        var vm = this;
        vm.showStatusOptions = showStatusOptions;
        vm.filterContacts = filterContacts;
        vm.companyMentionId = $stateParams.companyMentionId;
        vm.queryObject = {
            companyMentionId: vm.companyMentionId
        };
        vm.commentsList = {};
        vm.remindersList = {};
        activate();

        function activate() {
            common.activateController([getCompanyEngagement()], controllerId).then(function () { });
        }

        function getCompanyEngagement() {
            engageService.getEngageList(vm.queryObject).then(function (result) {
                vm.company = result.resources[0];
                vm.selectedContact = vm.company.contacts[0];
                getDeals(vm.selectedContact.contactMentionId);
            });
        }

        function filterContacts(filterText) {
            if (!filterText) {
                return;
            }
            vm.queryObject.searchText = filterText;
            getCompanyEngagement();
        }

        function getDeals(contactMentionId) {
            vm.queryObject.contactMentionId = contactMentionId;
            engageService.getDealsForCompany(vm.queryObject).then(function (result) {
                vm.deals = result;
                vm.selectedDeal = vm.deals.resources[0];
                if (vm.selectedDeal) {
                    getPulseComments();
                    getReminderComments();
                }
            });
        }

        function getPulseComments() {
            vm.commentsQueryObject = {};
            vm.commentsQueryObject.isWatched = false;
            vm.commentsQueryObject.reminders = false;
            vm.commentsQueryObject.isSnoozed = false;
            vm.commentsQueryObject.isDone = false;
            vm.commentsQueryObject.mentionId = vm.selectedDeal.dealMentionId;
            return commentsService.getMentionedComments(vm.commentsQueryObject).then(function (result) {
                vm.commentsList.resources = result.resources;
            });
        }

        function getReminderComments() {
            vm.commentsReminderQueryObject = {};
            vm.commentsReminderQueryObject.isWatched = false;
            vm.commentsReminderQueryObject.reminders = true;
            vm.commentsReminderQueryObject.isSnoozed = false;
            vm.commentsReminderQueryObject.isDone = false;
            vm.commentsReminderQueryObject.mentionId = vm.selectedContact.contactMentionId;
            return commentsService.getMentionedComments(vm.commentsReminderQueryObject).then(function (result) {
                vm.remindersList.resources = result.resources;
            });
        }

        function showStatusOptions(status, company) {
            $mdDialog.show({
                controller: 'statusOptionsController',
                controllerAs: 'vm',
                templateUrl: 'app/engage/statusOptions.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false,
                locals: { status: status, company: company }
            }).then(function () {

            }, function () {
            });
        }
        
    }
})();
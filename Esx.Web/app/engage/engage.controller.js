﻿(function () {
    'use strict';

    var controllerId = 'engage';

    angular.module('app').controller(controllerId, engage);

    engage.$inject = ['common', 'engageService', '$state', '$mdDialog'];

    function engage(common, engageService, $state, $mdDialog) {
        var vm = this;
        vm.getEngageList = getEngageList;
        vm.getCompanyEngageDetails = getCompanyEngageDetails;
        vm.search = search;
        vm.showStatusOptions = showStatusOptions;
        vm.queryObject = {
            
        };

        activate();

        function activate() {
            common.activateController([getEngageList()], controllerId).then(function () { });
        }

        function getEngageList() {
            engageService.getEngageList(vm.queryObject).then(function(result) {
                vm.engageList = result;
            });
        }

        function search() {
            if (vm.searchText) {
                vm.queryObject.searchText = vm.searchText;
                getEngageList();
            }
        }

        function getCompanyEngageDetails(companyMentionId) {
            $state.go('engageDetail', { companyMentionId: companyMentionId });
        }

        function showStatusOptions(status, company) {
            $mdDialog.show({
                controller: 'statusOptionsController',
                controllerAs: 'vm',
                templateUrl: 'app/engage/statusOptions.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false,
                locals: { status: status, company: company }
            }).then(function () {

            }, function () {
            });
        }
    }
})();
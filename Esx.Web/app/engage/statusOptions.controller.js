﻿(function () {
    'use strict';

    angular.module('app').controller('statusOptionsController', statusOptionsController);

    statusOptionsController.$inject = ['$scope', '$mdDialog', 'engageService', '$q', 'company', 'status'];

    function statusOptionsController($scope, $mdDialog, engageService, $q, company, status) {
        var vm = this;        
        vm.apply = apply;
        vm.cancel = cancel;
        vm.findContacts = findContacts;
        vm.getDeals = getDeals;
        vm.selectContact = selectContact;
        vm.company = company;
        vm.status = status;
        vm.dealsQuery = {
            companyMentionId: vm.company.mentionID
        };
        activate();

        function activate() {
            getDeals();
        }

        function findContacts(searchText) {
            var returnVal = [];
            if (searchText != null && angular.isDefined(searchText)) {
                returnVal = engageService.getContactsForCompany(vm.company.mentionID, searchText).then(function (results) {
                    return results.resources;
                });
            }
            return $q.when(returnVal);
        }

        function getDeals() {
            vm.dealsQuery.hasCa = vm.status !== 'Decline';
            engageService.getDealsForCompany(vm.dealsQuery).then(function (result) {
                vm.deals = result.resources;
            });
        }

        function selectContact(contact) {
            vm.selectedContact = contact;
        }

        function apply(form) {
            if (form.$invalid) {
                return;
            }
            $mdDialog.hide();
        };

        function cancel() {
            $mdDialog.cancel();
        }


    }
})();

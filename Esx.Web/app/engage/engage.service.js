﻿(function () {
    'use strict';

    var serviceId = 'engageService';

    angular.module('app').factory(serviceId, engageService);

    engageService.$inject = ['uriService', '$resource'];

    function engageService(uriService, $resource) {

        var service = {
            getEngageList: getEngageList,
            getContactsForCompany: getContactsForCompany,
            getDealsForCompany: getDealsForCompany
        };

        return service;

        function getEngageList(query) {
            return $resource(uriService.engageList, { searchText: '@searchText', companyMentionId: '@companyMentionId'}).get(query).$promise;
        }

        function getContactsForCompany(companyMentionId, searchText) {
            return $resource(uriService.engageCompanyContacts).get({ companyMentionId: companyMentionId, searchText: searchText }).$promise;
        }

        function getDealsForCompany(query) {
            return $resource(uriService.engageCompanyDeals, {
                companyMentionId: '@companyMentionId',
                contactMentionId: '@contactMentionId',
                hasCa: '@hasCa'
            }).get(query).$promise;
        }
    }
})();
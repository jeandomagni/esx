﻿(function () {
    'use strict';

    var app = angular.module('app');
    
    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routeConfigurator($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/");

        $stateProvider
             .state('engage', {
                 url: '/engage',
                 templateUrl: 'app/engage/engage.html',
                 controller: 'engage',
                 controllerAs: 'vm',
                 data: {
                     title: 'Engage',
                     sidebaricon: 'engage',
                     includeInSideBar: true,
                     subnavClass: 'springboard-bg',
                     subtabs: [

                     ]
                 }
             }).state('engageDetail', {
                 url: '/engageDetail/:companyMentionId',
                 templateUrl: 'app/engage/engage.detail.html',
                 controller: 'engageDetail',
                 controllerAs: 'vm',
                 data: {
                     title: 'Engage',
                     sidebaricon: 'engage',
                     includeInSideBar: true,
                     subnavClass: 'springboard-bg',
                     subtabs: [

                     ]
                 }
             });
    }
})();
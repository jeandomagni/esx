﻿(function () {
    'use strict';

    var app = angular.module('app');

    var config = {
        appErrorPrefix: '[ESX Error] ', //Configure the exceptionHandler decorator
        docTitle: 'ESX: ',
        version: '2.1.0'
    };

    app.value('config', config);
        
    app.config(['$logProvider', '$mdThemingProvider', function ($logProvider, $mdThemingProvider) {
        // Create custom palette to integrate Eastdil's blue.
        var eastdilTheme = $mdThemingProvider.extendPalette('blue', {
            '500': '003366'
        });
        
        $mdThemingProvider.definePalette('eastdilTheme', eastdilTheme);

        $mdThemingProvider.theme('default')
            .primaryPalette('eastdilTheme')
            .accentPalette('blue-grey');

        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
    }]);   
})();
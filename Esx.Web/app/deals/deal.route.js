
(function () {
    'use strict';

    var app = angular.module('app');

    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider'];

    function routeConfigurator($stateProvider) {

        $stateProvider
             .state('adddeal', {
                 url: '/adddeal',
                 templateUrl: 'app/deals/addDeal/addDeal.view.html',
                 controller: 'addDealController',
                 controllerAs: 'vm',
                 data: {
                     title: 'Add a Deal',
                     sidebaricon: 'deals',
                     includeInSideBar: true,
                     subnavClass: 'deals-bg'
                 }
             })
            .state('deals', {
                url: '/deals',
                templateUrl: 'app/deals/deals.html',
                controller: 'dealsController',
                controllerAs: 'vm',
                data: {
                    title: 'Deals',
                    defaultSearch: 'deal',
                    sidebaricon: 'deals',
                    includeInSideBar: true,
                    subnavClass: 'deals-bg'
                }
            }).state('deal', {
                abstract: true,
                url: '/deals/:dealMentionId',
                templateUrl: 'app/deals/deal.html',
                controller: 'dealController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    subnavClass: 'deals-bg',
                    subtabs: [
                           { sref: 'deal.dealSummary', label: 'SUMMARY' },
                           { sref: 'deal.dealSummary', label: 'DETAILS' },
                           { sref: 'deal.dealProperties', label: 'PROPERTIES' },
                           { sref: 'deal.dealSummary', label: 'ACTION PLAN' },
                           { sref: 'deal.marketing', label: 'MARKETING' },
                           { sref: 'deal.dealSummary', label: 'BIDS' },
                           { sref: 'deal.dealSummary', label: 'EXHIBITS' }
                    ]
                }
            }).state('deal.dealSummary', {
                url: '/summary',
                data: {
                    title: '',
                    sidebaricon: 'deals'
                },
                views: {
                    '': {
                        templateUrl: 'app/deals/summary/dealSummary.html',
                        controller: 'dealSummaryController',
                        controllerAs: 'vm'
                    },
                    'dealSummaryHeader@deal.dealSummary': {
                        templateUrl: 'app/deals/summary/dealSummaryHeader.html',
                        controller: 'dealSummaryHeaderController',
                        controllerAs: 'vm'
                    }
                }
            }).state('deal.dealProperties', {
                url: '/properties',
                data: {
                    title: '',
                    sidebaricon: 'deals'
                },
                views: {
                    '': {
                        templateUrl: 'app/deals/properties/dealProperties.html',
                        controller: 'dealPropertiesController',
                        controllerAs: 'vm'
                    },
                    'dealpropertiesHeader@deal.dealproperties': {
                        templateUrl: 'app/deals/properties/dealPropertiesHeader.html',
                        controller: 'dealPropertiesHeaderController',
                        controllerAs: 'vm'
                    }
                }
            });
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'dealSummaryController';

    angular.module('app').controller(controllerId, dealSummaryController);

    dealSummaryController.$inject = ['common'];

    function dealSummaryController(common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () {  });
        }
    }
})();
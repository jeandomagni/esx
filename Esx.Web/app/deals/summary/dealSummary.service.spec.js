﻿describe('dealSummary.service', function () {

    beforeEach(module('app'));

    var dealSummaryService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_dealSummaryService_, _uriService_, _$httpBackend_) {

        dealSummaryService = _dealSummaryService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        // ignore html templates. Not sure if this is a good approach. 
        $httpBackend.whenGET(/.*.html/).respond(200, '');
        $httpBackend.whenPOST(/.*.html/).respond(201, '');
        $httpBackend.whenPATCH(/.*.html/).respond(204, '');

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getSummary', function () {

        it('should call server with deal summary uri', function () {

            //Arrange  
            var mentionId = '@dealmention';
            $httpBackend.expectGET('api/deals/' + mentionId + '/summary', { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            dealSummaryService.getSummary(mentionId);

            //Assert
            $httpBackend.flush();
        });
    });
});
﻿describe('dealSummaryInformation.controller', function () {
    beforeEach(module('app'));

    var $controller,
        common,
        controller,
        $scope,
        dealSummaryService,
        fakeStats;

    var activateController = function (mentionId) {
        common.$stateParams.dealMentiond = mentionId;

        controller = $controller('dealSummaryInformationController', {
            common: common,
            dealSummaryService: dealSummaryService,
            $scope: $scope
        });
    };

    beforeEach(inject(function (_$controller_, _common_, _dealSummaryService_) {
        //inject
        $controller = _$controller_;
        common = _common_;
        dealSummaryService = _dealSummaryService_;

        //fakes
        $scope = { $watch: function () { } };

        //spys
        spyOn(dealSummaryService, 'getSummary').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback(fakeStats);
                }
            };
        });
    }));

    describe('activate', function () {
        var dealMentiond = '@mentioid';

        beforeEach(function () {
            activateController(dealMentiond);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

        it('should call the dealSummaryService getSummary function', function () {
            // assert
            expect(dealSummaryService.getSummary).toHaveBeenCalled();
        });
    });

});
﻿describe('dealSummaryHeader.controller', function () {
    beforeEach(module('app'));

    var $controller,
        common,
        controller,
        $scope,
        dealSummaryService,
        fakeStats;

    var activateController = function (propertyId) {
        common.$stateParams.propertyId = propertyId;

        controller = $controller('dealSummaryHeaderController', {
            common: common,
            propertySummaryService: dealSummaryService,
            $scope: $scope
        });
    };

    beforeEach(inject(function (_$controller_, _common_, _dealSummaryService_) {
        //inject
        $controller = _$controller_;
        common = _common_;
        dealSummaryService = _dealSummaryService_;

        //fakes
        $scope = { $watch: function () { } };

        var fake = {
            physicalAddress: {
                address1: '',
                city: '',
                state: ''
            }
        }
        //spys
        spyOn(dealSummaryService, 'getSummary').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback(fake);
                }
            };
        });
    }));

    describe('activate', function () {
        var dealMentionId = '@mentionid';

        beforeEach(function () {
            activateController(dealMentionId);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });
    });

});
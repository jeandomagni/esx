(function () {
    'use strict';

    var controllerId = 'dealSummaryHeaderController';

    angular.module('app').controller(controllerId, dealSummaryHeaderController);

    dealSummaryHeaderController.$inject = ['$scope', 'common', '$stateParams', 'uiGmapGoogleMapApi', 'dealSummaryService', '$window', '$timeout', 'geocodeService', 'mapOptions', 'country'];

    function dealSummaryHeaderController($scope, common, $stateParams, uiGmapGoogleMapApi, dealSummaryService, $window, $timeout, geocodeService, mapOptions, country) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.queryObject = {
            dealMentionId: $stateParams.dealMentionId
        }
        vm.map = { center: { latitude: 0, longitude: 0 }, showMarker: false, zoom: 3, control: {} };
        vm.markers = [];
        vm.mapExpanded = false;
        vm.address = {};
        vm.toggleMapSize = toggleMapSize;
        vm.country = country;

        activate();

        function activate() {
            common.activateController([getDealSummary()], controllerId).then(function () { });
            vm.mapOptions = mapOptions;
        }

        function getDealSummary() {
            dealSummaryService.getSummary(vm.queryObject.dealMentionId).then(function(result) {
                vm.dealSummary = result;
                mapLocations(vm.dealSummary.properties);
            });
        }

        function mapLocations(properties) {
            uiGmapGoogleMapApi.then(function (maps) {
                var i = 0;
                angular.forEach(properties, function(property) {
                    if (property.physicalAddress) {
                        var address = property.physicalAddress;
                        //use lat long if both exist
                        if (address.latitude && address.longitude) {
                            vm.markers.push({
                                propertyInfo:property,
                                id: vm.markers.length,
                                latitude: address.latitude,
                                longitude: address.longitude
                            });
                        } else {
                            //fall back to address
                            var addressCityState = '';
                            if (address.address1) {
                                addressCityState = address.address1 + ',';
                            }
                            addressCityState += address.city + ',' + address.state;
                            if (addressCityState.length === 1) {
                                return;
                            }
                            geocodeService.getLatLngForAddress(addressCityState).then(function(value) {
                                value.id = vm.markers.length;
                                value.propertyInfo = property;
                                vm.markers.push(value);
                                if (value.id === 0) { //set the first pin as center 
                                    vm.map.center = angular.copy(vm.markers[0]);
                                }
                            });
                        }
                    }
                });

                if (vm.markers[0]) {
                    vm.map.center = angular.copy(vm.markers[0]);
                }
                vm.map.zoom = 6;
                vm.map.control.refresh();

            });
        }

        vm.propertyMarkersEvents = {
            mouseover: function (marker, eventName, model) {
                ShowPropertyCardWindow(model);
            },
            click: function(marker, eventName, model){
                ShowPropertyCardWindow(model);
            },
            mouseout: function (marker, eventName, model) {
                vm.propertyWindowCard.show = false;
            }
        };
        vm.propertyWindowCard = {
            options: {
                pixelOffset: { width: -1, height: -40 }
            }
        };

        function ShowPropertyCardWindow(model) {
            if (vm.dealSummary.properties.length > 1) {
                vm.propertyWindowCard.model = model;
                vm.propertyWindowCard.show = true;
            }
        }

        function toggleMapSize() {
            vm.mapExpanded = !vm.mapExpanded;
            $timeout(function () {
                var resizeUIEvent = $window.document.createEvent('UIEvent');
                resizeUIEvent.initUIEvent('resize', true, false, $window, 0);
                $window.dispatchEvent(resizeUIEvent);
            }, 100);
        }
    }
})();
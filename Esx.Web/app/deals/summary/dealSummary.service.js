﻿(function () {
    'use strict';

    var serviceId = 'dealSummaryService';

    angular.module('app').factory(serviceId, dealSummaryService);

    dealSummaryService.$inject = ['uriService', '$resource'];

    function dealSummaryService(uriService, $resource) {

        var resource = $resource(
            uriService.dealSummary
        );

        var service = {
            getSummary: getSummary
        };

        return service;

        function getSummary(mentionId) {
            return $resource(uriService.dealSummary).get({ mentionId: mentionId }).$promise;
        }
    }
})();
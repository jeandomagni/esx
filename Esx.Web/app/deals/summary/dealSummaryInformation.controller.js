﻿(function () {
    'use strict';

    var controllerId = 'dealSummaryInformationController';

    angular.module('app').controller(controllerId, dealSummaryInformationController);

    dealSummaryInformationController.$inject = ['common', '$stateParams', 'dealSummaryService', 'events'];

    function dealSummaryInformationController(common, $stateParams, dealSummaryService, events) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        activate();

        function activate() {
            common.activateController([getDealSummary()], controllerId)
                .then(function () { common.$broadcast(events.refreshMap); });
        }

        function getDealSummary() {
            dealSummaryService.getSummary($stateParams.dealMentionId).then(function(response) {
                vm.dealSummary = response;
            });
        }
    }
})();
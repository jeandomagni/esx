﻿describe('deals.service', function () {

    beforeEach(module('app'));

    var dealsService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_dealsService_, _uriService_, _$httpBackend_) {

        dealsService = _dealsService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        uriService.deal = 'api/deals/:dealId';

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getDeals', function () {
        it('should call server with deals list uri', function () {

            //Arrange
            $httpBackend.expectGET('api/deals/list', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            dealsService.getDeals();

            //Assert
            $httpBackend.flush();

        });

        it('should make GET request with query', function () {

            //Arrange
            var query = {
                offset: 100,
                pageSize: 10
            }

            var expectedUri = uriService.dealsList + '?' + $.param(query);

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            dealsService.getDeals(query);

            //Assert
            $httpBackend.flush();

        });
    });
});
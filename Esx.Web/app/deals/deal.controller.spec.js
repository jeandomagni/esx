﻿describe('deal.controller', function () {
    beforeEach(module('app'));

    var $controller,
        common,
        dealsService,
        fakeDeal,
        $scope,
        controller;

    var activateController = function (dealMentionId) {
        common.$stateParams.dealId = dealMentionId;

        controller = $controller('dealController', {
            common: common,
            dealsService: dealsService
        });
    };

    beforeEach(inject(function (_$controller_, _common_, _dealsService_) {

        //inject
        $controller = _$controller_;
        common = _common_;
        dealsService = _dealsService_;

        //fakes
        $scope = {};

        common.$window = { location: {} };

        fakeDeal = {
            dealMentionId: '@something|L',
            dealName: 'dealName'
        };

        //spys

    }));

    describe('activate existing deal', function () {

        beforeEach(function () {
            var dealMentionId = "@deal";
            activateController(dealMentionId);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

    });

});
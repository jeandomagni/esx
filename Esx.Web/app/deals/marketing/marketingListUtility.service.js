﻿(function () {
    'use strict';

    var serviceId = 'marketingListUtilityService';

    angular.module('app').factory(serviceId, marketingListUtilityService);

    marketingListUtilityService.$inject = ['dateFilter', '$stateParams', 'marketingListService'];

    function marketingListUtilityService(dateFilter, $stateParams, marketingListService) {

        var service = {
            formatDates: formatDates,   
        };

        return service;

        function formatDates(dates) {
            if (dates && dates.length) {
                var formatted = "";
                if (dates.length >= 2) {
                    var date1 = dateFilter(dates[dates.length - 1], 'M/d');
                    var date2 = dateFilter(dates[0], 'M/d');
                    if (date1 === date2) {
                        formatted = date1;
                    }
                    else if (dates.length >= 3) {
                        formatted = date1 + '...' + date2;
                    } else {
                        formatted = date1 + ', ' + date2;
                    }
                } else {
                    formatted = dateFilter(dates[0], 'M/d');
                }
                return formatted;
            }
            return '';
        }

    }
})();
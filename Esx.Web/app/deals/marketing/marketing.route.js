﻿
(function () {
    'use strict';

    var app = angular.module('app');

    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider'];

    function routeConfigurator($stateProvider) {

        $stateProvider
            .state('deal.marketing', {
                url: '/marketing',
                data: {
                    title: '',
                    sidebaricon: 'deals'
                },
                views: {
                    '': {
                        templateUrl: 'app/deals/marketing/marketing.html',
                        controller: 'marketingController',
                        controllerAs: 'vm'
                    }
                }
            }).state('deal.marketing.marketingList', {
                url: '/list/:marketingListId',
                data: {
                    title: '',
                    sidebaricon: 'deals'
                },
                views: {
                    '': {
                        templateUrl: 'app/deals/marketing/marketingList.html',
                        controller: 'marketingListController',
                        controllerAs: 'vm'
                    }
                }
            }).state('deal.marketing.selectMarketingContacts', {
                url: '/select',
                data: {
                    title: '',
                    sidebaricon: 'deals'
                },
                views: {
                    '': {
                        templateUrl: 'app/deals/marketing/selectMarketingContacts.html',
                        controller: 'selectMarketingContactsController',
                        controllerAs: 'vm'
                    }
                }
            });
    }
})();
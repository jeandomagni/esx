﻿(function () {
    'use strict';

    var controllerId = 'marketingListContactRowController';

    angular.module('app').controller(controllerId, marketingListContactRowController);

    marketingListContactRowController.$inject = ['$scope', 'common','marketingListUtilityService', '$mdDialog', '$stateParams', 'marketingListService','events'];

    function marketingListContactRowController($scope, common, marketingListUtilityService, $mdDialog, $stateParams, marketingListService,events) {

        var vm = this;

        vm.today = 

        vm.formatDates = formatDates;
        vm.removeContact = removeContact;
        vm.updateStatus = updateStatus;
        vm.togglePermission = togglePermission;
        vm.warRoomIcon = warRoomIcon;
        vm.selectdate = selectdate;
        vm.getAddDateText = getAddDateText;

        function formatDates(dates) {
            return marketingListUtilityService.formatDates(dates);
        }

        function removeContact(companyIndex, contactIndex) {
            
            //TODO Add are you sure dialog
            marketingListService.deleteContactsFromMarketingList($stateParams.marketingListId, [$scope.contact.contactMentionId]).then(function () {
                reloadList( {
                    companyIndex: companyIndex,
                    contactIndex: contactIndex
                });
            });
        }

        function updateStatus(newStatus, date) {

            var query = {
                marketingListId: $stateParams.marketingListId,
                contactMentionIds: [$scope.contact.contactMentionId],
                status: newStatus
            };
            if (date === 'today' || date === 'yesterday') {
                query['day'] = date;
            } else {
                query['updateDate'] = date;
            }

            marketingListService.updateStatus(query).then(function () {
                reloadList();
            });
        }

        function togglePermission(permission, value) {
            var query = {
                marketingListId: $stateParams.marketingListId,
                contactMentionId: $scope.contact.contactMentionId
            };
            query[permission] = value;

            marketingListService.updateContact(query).then(function () {
                reloadList();
            });
        }

        function reloadList(data) {
            var query = angular.copy($scope.queryObject); //copy parent query object
            query['marketingListId'] = $stateParams.marketingListId;
            query['companyMentionId'] = $scope.company.mentionID;
            //fetch updated company
            marketingListService.getMarketingList(query).then(function (results) {

                if (results.resources && results.resources.length > 0) {
                    angular.copy(results.resources[0], $scope.company);
                }
                else if (results.resources.length === 0) {
                    $scope.marketingList.resources.splice(data.companyIndex, 1);
                }
            });
        }

        function warRoomIcon(contact) {
            if (contact.warRoomAccess.level === 'complete') {
                return 'lock_open';
            } else {
                return 'https';
            }
        }

        function selectdate(newStatus) {

            $mdDialog.show({
                controller: 'selectDateMLDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/deals/marketing/dialogs/selectDateMLDialog.html',
                locals: { label: newStatus },
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function (date) {
                updateStatus(newStatus, date);
            }, function () {
                //error?
            });
            
        }

        function getAddDateText(activity) {
            if (activity.dates.length) {
                return formatDates(activity.dates);
            }
            return "Add";
        }

    }
})();
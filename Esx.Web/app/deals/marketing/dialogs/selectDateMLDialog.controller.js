﻿(function () {
    'use strict';

    var controllerId = 'selectDateMLDialogController';

    angular.module('app').controller(controllerId, selectDateMlDialogController);

    selectDateMlDialogController.$inject = ['common', '$mdDialog', 'label'];

    function selectDateMlDialogController(common, $mdDialog, label) {

        var vm = this;

        vm.dateLabel = label;
        vm.cancel = cancel;
        vm.selectDate = selectDate;

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () { });
        }

        function cancel() {
            $mdDialog.cancel();
        }

        function selectDate(form) {
            if (form.$invalid) {
                return;
            }

            $mdDialog.hide(vm.date);
        }
    }
})();

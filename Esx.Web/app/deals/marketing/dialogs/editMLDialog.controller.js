﻿(function () {
    'use strict';

    var controllerId = 'editMLDialogController';

    angular.module('app').controller(controllerId, editMarketingListDialogController);

    editMarketingListDialogController.$inject = ['common', '$mdDialog', '$stateParams', 'marketingListService', 'contactsService', 'company'];

    function editMarketingListDialogController(common, $mdDialog, $stateParams, marketingListService, contactsService, company) {

        var vm = this;

        vm.contactsToAdd = [
            {
                contact: {}
            }
        ];

        vm.apply = apply;
        vm.cancel = cancel;
        vm.addAnotherContact = addAnotherContact;
        vm.removeContact = removeContact;
        vm.getContactName = getContactName;
        vm.lookupContacts = lookupContacts;
        vm.submitContacts = submitContacts;
        vm.selectedContactChanged = selectedContactChanged;

        var lookupContactsQueryObject = {
        
        };

        activate();

        function activate() {
            if (!!company) {
                lookupContactsQueryObject.companyMentionId = company.mentionID;
            }

            common.activateController([], controllerId)
                .then(function () { });
        }

        function apply(form) {
            if (form.$invalid) {
                return;
            }
            $mdDialog.hide();
        }

        function cancel() {
            $mdDialog.cancel();
        }

        function addAnotherContact() {
            vm.contactsToAdd.push({
                marketingList: {},
                recomendedBy: ''
            });
        };

        function removeContact(index) {
            vm.contactsToAdd.splice(index, 1);
        }

        function getContactName(item) {
            return item.fullName + ' of ' + item.companyName;
        }

        function lookupContacts(text) {
            if (text.length < 3) {
                return null;
            }
            lookupContactsQueryObject.searchText = text;
            return contactsService.searchContacts(lookupContactsQueryObject).then(function (searchResults) {
                return vm.results = searchResults.resources;
            });
        }

        function selectedContactChanged(changed, item) {
            changed.contact = item;
        };

        function submitContacts(form) {
            if (form.$invalid) {
                return;
            }

            var contacts = [];
            vm.contactsToAdd.forEach(function (item) {
                if (item.contact) {
                    contacts.push(item.contact.contactMentionId);
                }
            });

            marketingListService.addContactsToMarketingList($stateParams.marketingListId, contacts).then(function (result) {
                $mdDialog.hide();
            });
        }
    }
})();

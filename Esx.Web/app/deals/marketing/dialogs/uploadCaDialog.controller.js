﻿(function () {
    'use strict';

    var controllerId = 'uploadCaDialogController';

    angular.module('app').controller(controllerId, uploadCaDialogController);

    uploadCaDialogController.$inject = ['common','$mdDialog', '$scope', 'Upload', 'fileService', 'marketingListService', 'company','file', '$stateParams'];

    function uploadCaDialogController(common, $mdDialog, $scope, upload, fileService, marketingListService, company, file, $stateParams) {

        var vm = this;
        vm.files = [];

        vm.sumbmitCa = sumbmitCa;
        vm.cancel = cancel;
        vm.uploadFile = uploadFile;
        vm.removeFile = removeFile;
        vm.downloadFile = downloadFile;

        vm.contacts = company.contacts;

        activate();

        function activate() {

            vm.statuses = [
                { value: 'CA Approved', text: 'Complete' },
                { value: 'CA Follow Up Signature', text: 'Follow Up Signature' },
                { value: 'CA Follow Up Terms', text: 'Follow Up Terms' }
            ];
            vm.status = vm.statuses[0].value;

            vm.date = new Date();

            common.activateController([], controllerId)
                .then(function() {
                    if (!!file) {
                        uploadFile(file);
                    }
                });
        }

        function sumbmitCa(form) {
            if (form.$invalid || !vm.files.length) {
                return;
            }

            var fileIds =[];
            angular.forEach(vm.files, function(file) {
                fileIds.push(file.documentKey);
            });

            var ca = {
                marketingListId: $stateParams.marketingListId,
                contactMentionIds: vm.selectedContacts,
                documentIds: fileIds,
                status: vm.status,
                statusDate: vm.date
            };
            marketingListService.uploadCa(ca).then(function() {
                $mdDialog.hide(true);
            });
        }

        function cancel() {
            $mdDialog.cancel();
        }

        function uploadFile(file) {
            if (file) {
                upload.upload({
                    url: 'api/documents',
                    data: {
                        files: [file]
                    }
                }).then(function (response) {
                    vm.files.push(response.data.document);
                    vm.progress = null;
                }, function (response) {
                    if (response.status > 0) {
                        vm.errorMsg = response.status + ': ' + response.data;
                        vm.progress = null;
                    }
                }, function (evt) {
                    vm.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        }

        function removeFile(index) {
            vm.files.splice(index, 1);
        }

        function downloadFile(fileName) {
            var path = fileService.getFile(fileName);
            window.location = path;
        }

    }
})();

﻿(function () {
    'use strict';

    var controllerId = 'exportMLDialogController';

    angular.module('app').controller(controllerId, exportMlDialogController);

    exportMlDialogController.$inject = ['common', '$mdDialog'];

    function exportMlDialogController(common, $mdDialog) {

        var vm = this;

        vm.cancel = cancel;

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () { });
        }

        function cancel() {
            $mdDialog.cancel();
        }

        function selectDate(form) {
            if (form.$invalid) {
                return;
            }

            $mdDialog.hide(vm.date);
        }
    }
})();

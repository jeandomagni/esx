﻿(function () {
    'use strict';

    var controllerId = 'marketingListController';

    angular.module('app').controller(controllerId, marketingListController);

    marketingListController.$inject = ['common', '$scope', '$mdDialog', '$stateParams', '$state', 'marketingListService', 'events', '_', '$window', '$timeout'];

    function marketingListController(common, $scope, $mdDialog, $stateParams, $state, marketingListService, events, _, $window, $timeout) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var showContactRows = {};

        var vm = this;

        $scope.queryObject = {
            marketingListId: $stateParams.marketingListId,
            offset: 0
        }
        $scope.marketingList = {
            resources: [],
            pagingData: {
                pageSize: 10,
                offset: 0
            }
        }

        vm.updateList = updateList;
        vm.exportList = exportList;
        vm.searchList = _.debounce(searchList, 200);

        vm.showContacts = showContacts;
        vm.toggleShowContacts = toggleShowContacts;

        activate();

        function activate() {
            common.activateController([getMarketingList()], controllerId)
                .then(function () { });
        }

        function getMarketingList(isSearching) {
            if ($scope.queryObject.isBusy && !isSearching) return;
            marketingListService.getMarketingList($scope.queryObject).then(function (result) {
                $scope.marketingList.resources = $scope.marketingList.resources.concat(result.resources);
                return $scope.marketingList;
            });
        }

        function updateList() {
            $mdDialog.show({
                controller: 'editMLDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/deals/marketing/dialogs/editMLDialog.html',
                parent: angular.element(document.body),
                locals: { company: null },
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function () {
                $scope.marketingList.resources.length = 0;
                $scope.queryObject.offset = 0;
                common.$broadcast(events.marketingListUpdate, {});
            }, function () {
                //error?
            });
        }

        function openExport() {
            $mdDialog.show({
                controller: 'exportMLDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/deals/marketing/dialogs/exportMLDialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function () {
            }, function () {
                //error?
            });
        }

        function exportList(status, initial) {
            var query = {
                marketingListId: $stateParams.marketingListId,
                status: status,
                initial: initial
            };

            $window.open(marketingListService.getExportListUri(query), "_blank");
            //TODO Figure out better way then just a timeout
            $timeout(function () {
                $scope.marketingList.resources.length = 0;
                $scope.queryObject.offset = 0;
                common.$broadcast(events.marketingListUpdate, {});
            }, 1000);
        }

        $scope.$on(events.marketingListUpdate, function() {
            getMarketingList();
        });

    
        function searchList() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                $scope.queryObject.searchText = vm.searchText;
                $scope.marketingList.resources.length = 0;
                $scope.queryObject.offset = 0;
                getMarketingList(true);
            }
        };

        function showContacts(mentionId) {
            return !!showContactRows[mentionId];
        }

        function toggleShowContacts(mentionId) {
            showContactRows[mentionId] = !showContactRows[mentionId];
        }

    }
})();
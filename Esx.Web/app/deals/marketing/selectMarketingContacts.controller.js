﻿(function () {
    'use strict';

    var controllerId = 'selectMarketingContactsController';

    angular.module('app').controller(controllerId, selectMarketingContactsController);

    selectMarketingContactsController.$inject = ['common','$stateParams','$state', 'marketingListService'];

    function selectMarketingContactsController(common, $stateParams, $state, marketingListService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        vm.existingLists = [
            {
                list: {  },
                recomendedBy: ''
            }
        ];

        vm.addAnotherList = addAnotherList;
        vm.removeList = removeList;
        vm.getListName = getListName;
        vm.lookupExistingMarketingLists = lookupExistingMarketingLists;
        vm.submitMarketingList = submitMarketingList;
        vm.selectedListChanged = selectedListChanged;
        

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () { });
        }

        function addAnotherList() {
            vm.existingLists.push({
                marketingList: {},
                recomendedBy: ''
            });
        };

        function removeList(index) {
            vm.existingLists.splice(index, 1);
        }

        function getListName(item) {
            return item.dealName + ' ' + item.dealCode;
        }

        function lookupExistingMarketingLists(text) {
            if (text.length < 3) {
                return [];
            }

            var criteria = {
                searchText: text
            };

            return marketingListService.lookupExistingLists(criteria).then(function(results) {
                return results.resources;
            });
        }

        function selectedListChanged(changed, item) {
            changed.marketingList = item;
        };

        function submitMarketingList(form) {
            if (form.$invalid) {
                return;
            }

            var lists = [];
            vm.existingLists.forEach(function(item) {
                if (item.marketingList) {
                    lists.push(item.marketingList);
                }
            });

            marketingListService.createMarketingList($stateParams.dealMentionId, lists).then(function (result) {
                $state.go('deal.marketing.marketingList', { marketingListId: result.marketingListId });
            });
        }

        vm.getData = function() {
            return JSON.stringify(vm.existingLists);
        }

    }
})();
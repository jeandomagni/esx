﻿(function () {
    'use strict';

    var controllerId = 'marketingController';

    angular.module('app').controller(controllerId, marketingController);

    marketingController.$inject = ['common', '$stateParams', '$state', 'marketingListService'];

    function marketingController(common, $stateParams, $state, marketingListService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        activate();

        function activate() {
            common.activateController([getListForDeal()], controllerId)
                .then(function () { });
        }

        function getListForDeal() {
            marketingListService.getDealMaretingList($stateParams.dealMentionId).then(function (result) {

                common.setPageHeading(result.dealName, $stateParams.dealMentionId);

                if (result.marketingListId) {
                    // go to thelist
                    vm.progressPercentage = 33;
                    $state.go('deal.marketing.marketingList', { marketingListId: result.marketingListId });

                } else {
                    // go to select a list
                    $state.go('deal.marketing.selectMarketingContacts');

                }
            });
        }
    }
})();
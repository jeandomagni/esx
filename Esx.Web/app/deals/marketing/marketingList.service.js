﻿(function() {
    'use strict';

    var serviceId = 'marketingListService';

    angular.module('app').factory(serviceId, marketingListService);

    marketingListService.$inject = ['uriService', '$resource'];

    function marketingListService(uriService, $resource) {

        var service = {
            lookupExistingLists: lookupExistingLists,
            lookupContactsForList: lookupContactsForList,
            createMarketingList: createMarketingList,
            getDealMaretingList: getDealMaretingList,
            getMarketingList: getMarketingList,
            addContactsToMarketingList: addContactsToMarketingList,
            deleteContactsFromMarketingList: deleteContactsFromMarketingList,
            updateStatus: updateStatus,
            updateContact: updateContact,
            uploadCa: uploadCa,
            exportList: exportList,
            getExportListUri: getExportListUri
        };

        return service;

        function lookupExistingLists(query) {
            return $resource(uriService.marketingListsLookupExisting).get(query).$promise;
        }

        function lookupContactsForList(query) {
            return $resource(uriService.marketingListLookupContacts, { marketingListId: '@marketingListId' }).get(query).$promise;
        }

        function createMarketingList(dealMentionId, existingLists) {
            return $resource(uriService.createMarketingList).save({ dealMentionId: dealMentionId, existingLists: existingLists }).$promise;
        }

        function getDealMaretingList(dealMentionId) {
            return $resource(uriService.marketingLists).get({ dealMentionId: dealMentionId }).$promise;
        }

        function getMarketingList(query) {
            return $resource(uriService.marketingList, { marketingListId: '@marketingListId' }).get(query).$promise;
        }

        function addContactsToMarketingList(marketingListId, contacts) {
            return $resource(uriService.marketingList, { marketingListId: marketingListId }).save(contacts).$promise;
        }

        function deleteContactsFromMarketingList(marketingListId, contacts) {
            return $resource(uriService.marketingListContactsDelete, { marketingListId: marketingListId }).save( contacts ).$promise;
        }

        function updateStatus(status) {
            return $resource(uriService.marketingListStatus, { marketingListId: '@marketingListId' }).save(status).$promise;
        }

        function updateContact(contact) {
            return $resource(uriService.marketingListContact, { marketingListId: '@marketingListId', contactMentionId: '@contactMentionId' }).save(contact).$promise;
        }

        function uploadCa(ca) {
            return $resource(uriService.marketingListCA, { marketingListId: '@marketingListId' }).save(ca).$promise;
        }

        function exportList(marketingListId, query) {
            return $resource(uriService.marketingListExport, { marketingListId: '@marketingListId' }).get(query).$promise;
        }

        function getExportListUri(query) {
            return uriService.marketingListExport.replace(':marketingListId', query.marketingListId) + '?status=' + query.status + '&initial=' + query.initial;
        }
    }
})();
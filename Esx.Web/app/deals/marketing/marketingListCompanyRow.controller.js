﻿(function () {
    'use strict';

    var controllerId = 'marketingListCompanyRowController';

    angular.module('app').controller(controllerId, marketingListCompanyRowController);

    marketingListCompanyRowController.$inject = ['marketingListUtilityService', '$mdDialog', '$scope', 'Upload', 'marketingListService', 'events', '$stateParams', '$q', 'common'];

    function marketingListCompanyRowController(marketingListUtilityService, $mdDialog, $scope, upload, marketingListService, events, $stateParams, $q, common) {

        var vm = this;

        vm.showContacts = false;

        vm.toggleShowContacts = toggleShowContacts;
        vm.formatDates = formatDates;
        vm.warRoomIcon = warRoomIcon;
        vm.openUploadCa = openUploadCa;
        vm.dropFile = dropFile;
        vm.togglePermission = togglePermission;
        vm.addContact = addContact;
        vm.countSolicitableContacts = countSolicitableContacts;

        function toggleShowContacts() {
            vm.showContacts = !vm.showContacts;
        }

        function formatDates(dates) {
            return marketingListUtilityService.formatDates(dates);
        }

        function warRoomIcon(company) {
            if (company.warRoomAccess.level === 'complete') {
                return 'lock_open';
            } else {
                return 'https';
            }
        }

        function openUploadCa(file) {

            $mdDialog.show({
                controller: 'uploadCaDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/deals/marketing/dialogs/uploadCaDialog.html',
                locals: { company: $scope.company, file: file },
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function () {
                reloadCompany();
            }, function () {
                //error?
            });
        }

        function dropFile(file) {
            if (file) {
                //send it to popoup
                openUploadCa(file);

            }
        }

        function togglePermission(permission, value) {

            var promises = [];
            angular.forEach($scope.company.contacts, function(contact) {
                var query = {
                    marketingListId: $stateParams.marketingListId,
                    contactMentionId: contact.contactMentionId
                };
                query[permission] = value;
                promises.push(marketingListService.updateContact(query));
            });
            $q.all(promises).then(function () {
                reloadCompany();
            });
        }

        function addContact() {
            $mdDialog.show({
                controller: 'editMLDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/deals/marketing/dialogs/editMLDialog.html',
                locals: { company: $scope.company },
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function () {
                reloadCompany();
            }, function () {
                //error?
            });
        }

        function reloadCompany() {
            var query = angular.copy($scope.queryObject);
            query['marketingListId'] = $stateParams.marketingListId;
            query['companyMentionId'] = $scope.company.mentionID;
            marketingListService.getMarketingList(query).then(function (results) {
                if (results.resources && results.resources.length > 0) {
                    angular.copy(results.resources[0], $scope.company);
                }
            });
        }

        function countSolicitableContacts() {
            var count = 0;
            angular.forEach($scope.company.contacts, function (contact) {
                if (contact.isSolicitable) {
                    count++;
                }
            });
            return count;
        }
    }
})();
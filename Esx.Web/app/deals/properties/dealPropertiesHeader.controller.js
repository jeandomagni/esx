﻿(function () {
    'use strict';

    var controllerId = 'dealPropertiesHeaderController';

    angular.module('app').controller(controllerId, dealPropertiesHeaderController);

    dealPropertiesHeaderController.$inject = ['$scope', 'common', '$stateParams'];

    function dealPropertiesHeaderController($scope, common, $stateParams) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.queryObject = {
            dealId: $stateParams.dealMentionId
        }

        activate();

        function activate() {
            common.activateController([], controllerId).then(function () { });
        }
    }
})();
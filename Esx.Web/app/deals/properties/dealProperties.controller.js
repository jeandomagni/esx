﻿(function () {
    'use strict';

    var controllerId = 'dealPropertiesController';

    angular.module('app').controller(controllerId, dealPropertiesController);

    dealPropertiesController.$inject = ['common', 'dealsService'];

    function dealPropertiesController(common, dealsService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;

        vm.searchText = '';

        vm.propertiesList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        };

        vm.queryObject = {
            dealId: common.$stateParams.dealMentionId,
            offset: 0,
            pageSize: 30
        };

        activate();

        vm.getDealProperties = _.debounce(getDealProperties, 200);

        vm.title = 'Deal Properties';

        function activate()
        {
            common.activateController([getDealProperties()], controllerId)
                .then(function () { log('Activated Deal Properties View'); });
        }

        function getDealProperties(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            return dealsService.getDealProperties(vm.queryObject).then(function (propertiesList) {
                vm.propertiesList.resources = propertiesList.resources;
                vm.queryObject.isBusy = false;

                console.log('success');
                console.log(vm.propertiesList.resources);

                return vm.propertiesList;
            });
        }
    }
})();
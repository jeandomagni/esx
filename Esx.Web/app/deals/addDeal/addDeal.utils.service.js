﻿(function () {
    'use strict';
    var serviceId = 'addDealUtilsService';

    angular
        .module('app')
        .service(serviceId, addDealUtilsService);

    function addDealUtilsService() {
        this.getHistoricRole = function (dealType) {
            var role = '';
            switch (dealType) {
                case 'Equity Sale <100%':
                    role = 'buyers';
                    break;
                case 'Equity Sale 100%':
                    role = 'buyers';
                    break;
                case 'Loan Sale':
                    role = 'buyers';
                    break;
                case 'Debt Placement':
                    role = 'lenders';
                    break;
                case 'Investment Banking':
                    role = 'investment banking';
                    break;
                case 'Non-transactional Advisory':
                    role = 'non-transactional advisory';
                    break;
            }
            return role;
        }

        this.getRole = function (dealType) {
            var role = '';
            switch (dealType) {
                case 'Equity Sale <100%':
                    role = 'sellers';
                    break;
                case 'Equity Sale 100%':
                    role = 'sellers';
                    break;
                case 'Loan Sale':
                    role = 'sellers';
                    break;
                case 'Debt Placement':
                    role = 'borrowers';
                    break;
                case 'Investment Banking':
                    role = 'investment banking';
                    break;
                case 'Non-transactional Advisory':
                    role = 'non-transactional advisory';
                    break;
            }
            return role; 
        }

        this.getSecondaryTypes = function (types, primaryTypeId) {
            return types.filter(function (type) {
                return type.typeId !== primaryTypeId;
            });
        }

    }
})();

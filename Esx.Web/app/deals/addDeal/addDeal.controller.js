﻿(function () {
    'use strict';
    var controllerId = 'addDealController';

    angular.module('app').controller(controllerId, addDealController);

    addDealController.$inject = ['$scope','$q','$timeout', 'common', 'loansService','companiesService', 'contactsService','addDealUtilsService', 'propertiesService', 'mapsService', 'mapOptions'];

    function addDealController($scope, $q, $timeout, common, loansService,companiesService, contactsService, addDealUtilsService, propertiesService, mapsService, mapOptions) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        var autocomplete;
        var map;
        var propertyMarkers = [];

        var vm = this;
        vm.deal = {
            dealTypes: [],
            properties: [],
            roles: [],
            borrowers: [],
            lenders: [],
            loans: [],
            team: [],
            pitch: { competition: [] }
        }

        //initialize form
        vm.form = {
            stepOne: { text: 'wizard-text-active', background: 'wizard-step-active', progress: 0 },
            stepTwo: { text: 'wizard-text-inactive', background: 'wizard-step-inactive', progress: 0 },
            stepThree: { text: 'wizard-text-inactive', background: 'wizard-step-inactive', progress: 0 },
            currentQuestion: 'app/deals/addDeal/templates/questionOne.html',
            currentStep: 1
        }



        vm.currentQuestion = vm.form.currentQuestion;
        vm.showGoogleAutoComplete = false;
        vm.companyTextDisabled = true;
        vm.showNewCompanyInput = false;
        vm.showNewBorrowerInput = false;
        vm.showNewLenderInput = false;
        vm.showRoleContacts = false;
        vm.showNewCompanyContact = false;
        vm.companyContactTextDisabled = true;
        vm.showNewLoanForm = false;
        vm.newLoanAddDisabled = true;

        //question one 
        vm.selectDealState = function(state)
        {
            vm.form.stepOne.progress = 50; 
            if (state==='Active') {
                vm.form.stepOne.progress = 100; 
            }
 
        }


        //question two
        vm.showBrokerAutoComplete = false;
        vm.companyIntellisense = function(query) {
            if (query.length < 3) {
                return null;
            }
  
            return vm.intellisenseSearchResults = [];
            //return companiesService.companyIntellisense(query).then(function (searchResults) {
            //    return vm.intellisenseSearchResults = searchResults;
            //});
        }

        vm.selectCompany = function(item) {
            vm.deal.broker = item.aliasName;
        }

        vm.moveToStepOne = function() {
            vm.form = {
                stepOne: { text: 'wizard-text-active', background: 'wizard-step-active', progress: 100 },
                stepTwo: { text: 'wizard-text-inactive', background: 'wizard-step-inactive', progress: 0 },
                stepThree: { text: 'wizard-text-inactive', background: 'wizard-step-inactive', progress: 0 },
                currentQuestion: 'app/deals/addDeal/templates/questionOne.html',
                currentStep: 1
            }
        }

        vm.moveToStepTwo = function () {
            vm.form = {
                stepOne: { text: 'wizard-text-inactive', background: 'wizard-step-inactive', progress: 100, edit :true},
                stepTwo: { text: 'wizard-text-active', background: 'wizard-step-active', progress: 0 },
                stepThree: { text: 'wizard-text-inactive', background: 'wizard-step-inactive', progress: 0 },
                currentQuestion: 'app/deals/addDeal/templates/questionTwo.html',
                currentStep: 2
            }
        }
   
     
        vm.selectBroker = function (isEastdil) {
            vm.deal.broker = 'EASTDIL';
            vm.showBrokerAutoComplete = false;
            if (isEastdil === 'Yes') {
                vm.deal.broker = null;
                vm.showBrokerAutoComplete = true;
            } else if (isEastdil ==='Unknown') {
                vm.deal.broker = isEastdil;
            }
            vm.hasBroker = true; 
        }


        vm.setBroker = function() {
            vm.form.currentQuestion = 'app/deals/addDeal/templates/questionThree.html';
            vm.form.stepTwo.progress = 20;
        }
        //question three

        vm.createSecondaryDealTypes = function (type) {
            vm.secondaryDealTypes = addDealUtilsService.getSecondaryTypes(vm.dealTypes, type.typeId);
            vm.deal.dealTypes.push({ typeId: type.typeId, typeName: type.typeName });
            vm.form.stepTwo.progress = 40;
        };

        vm.clearDealTypes = function() {
            vm.form.stepTwo.progress = 20;
            vm.deal.dealTypes = [];
        };

        vm.addSecondaryDealType = function (type) {
            var index = vm.deal.dealTypes.indexOf(type);
            if (index === -1) {
                vm.deal.dealTypes.push(type);
            } else {
                vm.deal.dealTypes.splice(index, 1);
            }
        }

        vm.addDealType = function (type) {
            vm.deal.dealTypes = [];
            vm.deal.dealTypes.push(type);
        }

        vm.checkSecondaryDealTypes = function (type) {
            return vm.deal.dealTypes.indexOf(type) > -1;
        };

        //question four 
        vm.propertyIntellisense = function(query) {
            if (query.length < 3) {
                return null;
            }
            return propertiesService.propertyIntellisense(query).then(function (searchResults) {
                return vm.propertyIntellisenseSearchResults = searchResults;
            });
        }

  
        vm.selectProperty = function (property) {

            if (!map) {
                initMap();
            }

            if (property) {
         
                //todo REMOVE
                property.lat = parseFloat(setRandomLat());
                property.lng = parseFloat(setRandomLng());
                //todo END

                vm.deal.properties = vm.deal.properties.filter(function(p) {
                    return p.propertyKey !== property.propertyKey;
                });
        
                vm.deal.properties.push(property);
                addPropertyLocation({ locationId: property.propertyKey, title:property.propertyName, lat: property.lat, lng: property.lng }, 'P');
            }
            vm.form.stepTwo.progress = 60;
            vm.searchedProperty = '';
        }

        vm.removeProperty = function(property) {
            vm.deal.properties = vm.deal.properties.filter(function (p) {
                return p.propertyKey !== property.propertyKey;
            });

            //esx
            propertyMarkers.forEach(function (marker) {
                if (marker.markerId === property.propertyKey) {
                        marker.setMap(null);
                    }
            });

            propertyMarkers = propertyMarkers.filter(function (marker) {
                return marker.markerId !== property.propertyKey;
            });

            map.fitBounds(mapsService.getExtent(propertyMarkers));
        }
       

        vm.setUpMap = function() {
            vm.showGoogleAutoComplete = true;
            $timeout(function () {
                autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));
                autocomplete.addListener('place_changed', vm.getAddress);
            }, 500);
            if (!map) {
                 initMap(); 
            }
           
        }
        //question five
        
        vm.unknownRoles = function () {
            vm.form.stepTwo.progress = 60;
            if (vm.deal.roleUnknown !== true) {
                vm.form.stepTwo.progress = 80;
            }
        }

        vm.getRoleLabel = function (dealType) {
            return addDealUtilsService.getRole(dealType);
        }

     
//active
        vm.selectRole = function(item,roleType) {
            if (item) {
                vm.deal.roles = vm.deal.roles.filter(function (role) {
                    return role.companyId !== item.companyId;
                });

                vm.deal.roles.push(
                    {
                        company: item.aliasName,
                        companyId: item.company_Key,
                        roleType: roleType,
                        contacts: []
                    });
                vm.selectedCompany = '';

            }
            vm.form.stepTwo.progress = 80;
        }

        vm.checkCompanyText = function () {
            if (vm.newCompany.length > 2) {
                vm.companyTextDisabled = false;
            }
        }

        vm.addNewCompany = function (roleType) {
            //TODO need to get company key 
            vm.deal.roles.push(
                    {
                        company: vm.newCompany,
                        companyId: 'tempId' + getRandomInt().toString(),
                        roleType: roleType,
                        contacts: []
                    });
            vm.newCompany = '';
            //vm.company.companyName = '';
            vm.selectedCompany = '';
            vm.showNewCompanyInput = false;
            vm.form.stepTwo.progress = 80;
        }

        vm.setUpCompanyRole = function () {
            vm.showNewCompanyInput = true;
            vm.newCompany = vm.selectedCompany;
        }

        vm.removeCompany = function (role) {
            vm.deal.roles = vm.deal.roles.filter(function (c) {
                return c.companyId !== role.companyId;
            });
        }

        //historic

        //borrower
        vm.selectBorrower = function (item, roleType) {
            if (item) {
                vm.deal.borrowers = vm.deal.borrowers.filter(function (role) {
                    return role.companyId !== item.company_Key;
                });

                vm.deal.borrowers.push(
                    {
                        company: item.aliasName,
                        companyId: item.company_Key,
                        roleType: roleType,
                        contacts: []
                    });
                vm.selectedBorrower = '';
            }
            vm.form.stepTwo.progress = 75;
        }

        vm.setUpBorrower = function () {
            vm.showNewBorrowerInput = true;
            vm.newBorrower = vm.selectedBorrower;
        }

        vm.addNewBorrower = function (roleType) {
            vm.deal.borrowers.push(
                    {
                        company: vm.newBorrower,
                        companyId: 'tempId' + getRandomInt().toString(),
                        roleType: roleType,
                        contacts: []
                    });
            vm.newBorrower = '';
            vm.selectedBorrower = '';
            vm.showNewBorrowerInput = false;
            vm.form.stepTwo.progress = 75;
        }

        vm.removeBorrower = function (company) {
            vm.deal.borrowers = vm.deal.borrowers.filter(function (c) {
                return c.companyId !== company.companyId;
            });
        }

        //lender
        vm.selectLender = function (item, roleType) {
            if (item) {
                vm.deal.lenders = vm.deal.lenders.filter(function (role) {
                    return role.companyId !== item.companyId;
                });

                vm.deal.lenders.push(
                    {
                        company: item.aliasName,
                        companyId: item.company_Key,
                        roleType: roleType,
                        contacts: []
                    });
                vm.selectedLender = '';
            }
            vm.form.stepTwo.progress = 85;
        }

        vm.removeLender = function (company) {
            vm.deal.lenders = vm.deal.lenders.filter(function (c) {
                return c.companyId !== company.companyId;
            });
        }

        vm.setUpLender = function () {
            vm.showNewLenderInput = true;
            vm.newLender = vm.selectedLender;
        }

        vm.addNewLender = function(roleType) {
            vm.deal.lenders.push(
                {
                    company: vm.newLender,
                    companyId: 'tempId' + getRandomInt().toString(),
                    roleType: roleType,
                    contacts: []
                });
            vm.newLender = '';
            vm.selectedLender = '';
            vm.showNewLenderInput = false;
            vm.form.stepTwo.progress = 85;
        }


        //depricated
        vm.getHistoricRoleLabel = function (dealType) {
            return addDealUtilsService.getHistoricRole(dealType);
        }

        vm.editRoleContacts = function(role) {
            vm.showRoleContacts = true;
            vm.selectedRole = role; 
        }
        
        //todo make this like other intelisense 
        vm.findContacts = function(searchText) {
            var returnVal = [];
            if (searchText != null && searchText.length > 2) {
                returnVal = contactsService.searchContacts(searchText).then(function (results) {
                    return results;
                });
            }
            return $q.when(returnVal);
        }

        vm.selectContact = function(contact) {
            if (contact) {
                vm.selectedRole.contacts = vm.selectedRole.contacts.filter(function(c) {
                    return c.contactID !== contact.contactID;
                });
                vm.selectedRole.contacts.push(contact);
                vm.contact.contactName = '';
            }
        }


      vm.removeContact = function(contact) {
          vm.selectedRole.contacts = vm.selectedRole.contacts.filter(function (c) {
              return c.contactID !== contact.contactID;
          });
      }

      vm.setUpNewCompanyContact = function() {
          vm.showNewCompanyContact = true;
          vm.newCompanyContact = vm.contact.contactName;
      }

        vm.addNewCompanyContact = function() {

            vm.selectedRole.contacts.push({
                contactID: 'tempId' + getRandomInt().toString(),
                fullName: vm.newCompanyContact
            });

            vm.newCompanyContact = '';
            vm.contact.contactName = '';
            vm.showNewCompanyContact = false;

        };

        vm.checkCompanyContactText = function () {
            if (vm.newCompanyContact.length > 2) {
                vm.companyContactTextDisabled = false;
            }
        }


        //five1 loans 
        vm.selectLoan = function (loan) {

            if (loan) {
                vm.deal.loans.filter(function (l) {
                    return l.loanKey !== loan.loanKey;
                });
                vm.deal.loans.push(loan);
            }
         
            vm.loanSearchText = '';
        }

        vm.removeLoan = function (loan) {
            vm.deal.loans =  vm.deal.loans.filter(function (l) {
                return l.loanKey !== loan.loanKey;
            });
        };

        vm.searchLoans = function (searchText) {
            var returnVal = [];

            if (searchText !==null && searchText.length > 2) {
                returnVal = loansService.getLoans({ searchText: searchText, offset: 0 }).then(function (results) {
                    return results.resources;
                });
            }

            return $q.when(returnVal);
        }

        vm.createNewLoan = function() {
            vm.newLoanName = vm.loanSearchText;
            vm.showNewLoanForm = true; 
        }

        vm.checkNewLoanText = function() {
            if (vm.loanSearchText.length>2) {
                vm.newLoanAddDisabled = false; 
            }
        }

        vm.addNewLoan = function() {
    //todo prevent dupes
            vm.deal.loans.push({
                loanKey: 'tempLoanId' + getRandomInt.toString(),
                loanName: vm.newLoanName
            });
            vm.newLoanName = '';
            vm.loanSearchText = '';
            vm.showNewLoanForm = false;
        }


        //question six
        vm.moveToStepThree = function() {
            vm.form = {
                stepOne: { text: 'wizard-text-inactive', background: 'wizard-step-inactive', progress: 100 },
                stepTwo: { text: 'wizard-text-inactive', background: 'wizard-step-inactive', progress: 100 },
                stepThree: { text: 'wizard-text-active', background: 'wizard-step-active', progress: 0 },
                currentQuestion: 'app/deals/addDeal/templates/questionSix.html',
                currentStep: 3
            }


        }

        vm.selectStage = function () {
            vm.form.stepThree.progress = 40;
            if (vm.deal.stage === 'Targeting') {
                vm.form.stepThree.progress = 100;
            } 
        }

        //onboarding.team
        vm.updateLead = function(member) {
            vm.deal.team.forEach(function(m) {
                if (m.contactID !== member.contactID) {
                    m.lead = false;
                } 
            });
        }

        vm.updateManager = function (member) {
            vm.deal.team.forEach(function (m) {
                if (m.contactID !== member.contactID) {
                    m.manager = false;
                }
            });
        }

        vm.selectTeamMember = function (teamMember) {
            if (teamMember) {
                vm.deal.team = vm.deal.team.filter(function(t) {
                return t.contactID !== teamMember.contactID;
            });
            vm.deal.team.push(teamMember);
            }
   
            vm.teamSearch = '';
            vm.form.stepThree.progress = 60;
        }

        vm.removeTeamMember = function (teamMember) {
            vm.deal.team = vm.deal.team.filter(function (t) {
                return t.contactID !== teamMember.contactID;
            });
        }

        vm.selectPitchDateType = function () {
            vm.deal.pitch.pitchDate = null;
            vm.deal.pitch.pitchExactDate = null;
            vm.deal.pitch.approximateDate = null;
            if (vm.deal.pitch.pitchDateSelection === 'unknown') {
                     vm.deal.pitch.pitchDate = 'Unknown';
            }
        }

        vm.derivePitchDates = function () {
                if (vm.deal.pitch.pitchDateSelection === 'date') {
                    vm.deal.pitch.pitchDate = vm.deal.pitch.pitchExactDate;
                } else if (vm.deal.pitch.pitchDateSelection === 'approximateDate') {
                    vm.deal.pitch.pitchDate = vm.deal.pitch.approximateDate;
                }
        }


        vm.checkPitchCompetition = function () {
            vm.form.stepThree.progress = 90;
            if (vm.deal.pitch.competing === 'No') {
                vm.deal.pitch.competition = [];
                vm.form.stepThree.progress = 100;
            }
        }
        
        //onboard pitch competition
        vm.selectCompetition = function(company) {
            if (company) {
                vm.deal.pitch.competition = vm.deal.pitch.competition.filter(function (c) {
                    return c.company_Key !== company.company_Key;
                });
                vm.deal.pitch.competition.push(company);
            }
            vm.competitionSearch = '';
            vm.form.stepThree.progress = 100;
        }

        vm.removeCompetition = function(company) {
            vm.deal.pitch.competition = vm.deal.pitch.competition.filter(function (c) {
                return c.company_Key !== company.company_Key;
            });
        }

        vm.checkPitchStatus = function () {
            vm.form.stepThree.progress = 30;
            if (vm.deal.packaging.pitch==='No') {
              vm.form.stepThree.progress = 40;
            }
        }

        vm.checkPackagePitchCompetition = function() {
                vm.form.stepThree.progress = 50;
                if (vm.deal.pitch.competing === 'No') {
                    vm.deal.pitch.competition = [];
                    vm.form.stepThree.progress = 55;
                }
            }

        vm.addPackagePitchCompetition = function (company) {
            if (company) {
                vm.deal.pitch.competition = vm.deal.pitch.competition.filter(function (c) {
                    return c.company_Key !== company.company_Key;
                });
                vm.deal.pitch.competition.push(company);
            }
            vm.competitionSearch = '';
            vm.form.stepThree.progress = 55;
        }

        vm.deriveTeaserPitchDates = function() {
            if (vm.deal.packaging.teaserDateSelection === 'date') {
                vm.deal.packaging.teaserDate = vm.deal.packaging.teaserExactDate;
            } else if (vm.deal.packaging.teaserDateSelection === 'approximateDate') {
                vm.deal.packaging.teaserDate = vm.deal.packaging.teaserApproximateDate;
            }
        }

        vm.selectTeaserDateType = function() {
            vm.deal.packaging.teaserDate = null;
            vm.deal.packaging.teaserExactDate = null;
            vm.deal.packaging.teaserApproximateDate = null;
            if (vm.deal.packaging.teaserDateSelection === 'unknown') {
                vm.deal.packaging.teaserDate = 'Unknown';
                }
        }


        //test

        //vm.moveToStepThree();
        //vm.form.currentQuestion = 'app/deals/addDeal/templates/onboarding.packaging.teaser.html';

        //vm.deal = {
        //    broker: 'EASTDIL',
        //    state: 'Active',
        //    roles: [],
        //    loans: [],
        //    team: [],
        //    pitch:{competition:[]},
        //    dealTypes: [{
        //        "typeId": 0,
        //        "typeName": "Loan Sale"
        //    }]
        //}

        //end test 
     
        //map functions 
        vm.getAddress = function() {
            var place = autocomplete.getPlace();
            var addressComponents = mapsService.formatAddress(place);
            var addressComponentsLong = mapsService.formatAddressLongNames(place);
            addPropertyLocation({ locationId: place.place_id, title: addressComponents.formatted_address, lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }, 'P');
            vm.deal.properties.push({ propertyKey: place.place_id, propertyName: addressComponents.street_number + ' ' + addressComponents.route });
            $scope.$apply();
            vm.showGoogleAutoComplete = false;
        }

        function initMap() {
            vm.showMap = true;
            vm.mapClass = 'md-whiteframe-2dp';

            var styledMap = new google.maps.StyledMapType(mapOptions.styles, { name: "Roads" });

            //todo use geolocation
            var options = {
                zoom: 3,
                center: new google.maps.LatLng(37.7773664, -99.7175739),
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.SATELLITE, 'map_style']
                }
            };

            map = new google.maps.Map(document.getElementById('map'), options);
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');

            google.maps.event.addListenerOnce(map, 'idle', function () {
                google.maps.event.trigger(map, 'resize');
            });
        }


        function addPropertyLocation(location, type) {

            //create marker
            var marker = new google.maps.Marker({
                position: { lng: location.lng, lat: location.lat },
                map: map,
                label: type,
                draggable: true,
                title: location.title,
                markerId: location.locationId
            });

            // add listener if marker moves
            google.maps.event.addListener(marker, 'dragend', function (evt) {
                marker.lat = evt.latLng.lat();
                marker.lng = evt.latLng.lng();
            });

            //add property list (to get extent later)  
            propertyMarkers.push(marker);
        
            //set zoom and center
            if (propertyMarkers.length === 1) {
                map.setZoom(14);
                map.setCenter(new google.maps.LatLng(location.lat, location.lng));
            } else {
                map.fitBounds(mapsService.getExtent(propertyMarkers));
            };
            vm.form.stepTwo.progress = 60;
            //hate to do this....
            //  $scope.$apply();
        }


        //page functions
        vm.editStepOne = function() {
            vm.form.currentQuestion = 'app/deals/addDeal/templates/questionOne.html';
        }

        //data
        vm.dealTypes = [
        { typeId: 0, typeName: 'Debt Placement' },
        { typeId: 1, typeName: 'Equity Sale <100%' },
        { typeId: 2, typeName: 'Equity Sale 100%' },
        { typeId: 3, typeName: 'Investment Banking' },
        { typeId: 4, typeName: 'Loan Sale' },
        { typeId: 5, typeName: 'Non-transactional Advisory' }
        ];

        vm.dealStatusTypes = [
            { typeId: 0, typeName: 'Packaging' },
            { typeId: 1, typeName: 'Marketing' },
            { typeId: 2, typeName: 'Bid Receipt' },
            { typeId: 3, typeName: 'Under Agreement' },
            { typeId: 4, typeName: 'Non-Refundable' }
        ];

        function setRandomLat() {
            return (Math.random() * (41.957337 - 41.7489) + 41.7489).toFixed(4);
        }

        function setRandomLng() {
            return (Math.random() * (-87.7497 - -87.9977) + -87.9977).toFixed(4);
        }

        function getRandomInt() {
            return (Math.random() * (1000000 - 1) + 1).toFixed(0);
        }

        //end data
    }
})();
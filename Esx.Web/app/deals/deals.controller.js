﻿(function () {
    'use strict';

    var controllerId = 'dealsController';

    angular
      .module('app')
        .controller(controllerId, dealsController);

    dealsController.$inject = ['common', 'dealsService', 'companiesService', 'mentionsService', '_', '$state', '$timeout'];

    function dealsController(common, dealsService, companiesService, mentionsService, _, $state, $timeout) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.searchText = '';

        vm.getDeals = _.debounce(getDeals, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.searchDeals = _.debounce(searchDeals, 200);
        vm.selectRow = selectRow;

        vm.queryObject = {
            offset: 0,
            pageSize: 20
        };

        vm.dealsList = {
            resources: [],
            pagingData: {
                pageSize: 30,
                offset: 0
            },
        }

        function activate() {
            common.activateController([getDeals(false)], controllerId)
                .then(function () { log('Activated Deals.'); });
        }

        function getDeals(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            return dealsService.getDeals(vm.queryObject).then(function (dealsList) {
                vm.dealsList.resources = vm.dealsList.resources.concat(dealsList.resources);
                return vm.dealsList;
            });
        }

        function searchDeals() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.dealsList.resources.length = 0;
                vm.queryObject.offset = 0;
                getDeals(true);
            }
        }

        function sortColumn(columnName) {
            vm.dealsList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            getDeals(false);
        }

        function selectRow(item, event) {
            $timeout(function () {
                if (event.target.id === 'esx-watch') {
                    toggleIsWatched(item);
                } else {
                    editDeal(item);
                }
            });
        }

        function editDeal(deal) {
            $state.go('deal.dealSummary', { 'dealMentionId': deal.dealMentionId });
        }

        function toggleIsWatched(deal) {
            deal.isWatched = !deal.isWatched;
            mentionsService.setIsWatched(deal.dealMentionId, deal.isWatched);
        }

        activate();
    }
})();
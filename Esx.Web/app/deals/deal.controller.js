﻿(function () {
    'use strict';
    var controllerId = 'dealController';

    angular.module('app').controller(controllerId, dealController);

    dealController.$inject = ['common'];

    function dealController(common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;

        activate();

        function activate() {
            common.activateController([], controllerId).then(function () { log('deal activated'); });
        }

    }
})();
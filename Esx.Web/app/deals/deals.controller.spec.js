﻿(function () {
    describe('deals.controller', function () {

        beforeEach(module('app'));

        var $controller,
            common,
            dealsService,
            mentionsService,
            fakeDeals,
            $scope,
            controller,
            $timeout,
            $state,
            _;

        var activateController = function (context) {
            controller = $controller('dealsController', {
                common: common,
                dealsService: dealsService,
                mentionsService: mentionsService,
                '_': _,
                $timeout: $timeout,
                $state: $state
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _dealsService_, _mentionsService_, _$timeout_, _$state_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            dealsService = _dealsService_;
            mentionsService = _mentionsService_;
            _ = window._;
            $timeout = _$timeout_;
            $state = _$state_;

            //fakes
            $scope = {
                $apply: angular.noop
            };

            fakeDeals = {
                resources: [
                    { fullName: 'John Landers', title: 'Deal Coordinator', company: 'Colony American Homes' },
                    { fullName: 'Bob Dobbs', title: 'Managing Director', company: 'Church of the SubGenius' }
                ],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn(dealsService, 'getDeals').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeDeals);
                    }
                };
            });

            spyOn(mentionsService, 'setIsWatched');

        }));

        describe('activate', function () {
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define the query object. ', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.dealsList.pagingData).toEqual(expectedPagingData);
            });
        });

        describe('getDeals', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get deals with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    offset: 0,
                    pageSize: 30
                };

                controller.queryObject = expectedSearchQuery;
                controller.dealsList.resources.length = 0;

                //act
                controller.getDeals();

                //assert
                expect(dealsService.getDeals).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.dealsList).toEqual(fakeDeals);
            });

        });

        describe('sorting', function () {
            beforeEach(function () {
                activateController();
            });

            it('should sort deals list', function () {
                var columnName = 'fullName';

                //arrange
                var queryObject = { filters: undefined, searchText: '' };

                controller.queryObject = queryObject;

                //act
                controller.sortColumn(columnName);

                //assert
                expect(dealsService.getDeals).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.queryObject.sort).toEqual(columnName);
                expect(controller.queryObject.sortDescending).toBe(false);
                expect(controller.dealsList).toEqual(fakeDeals);
            });

            it('should clear current deals and offset', function () {

                //arrange
                var columnName = 'fullName';
                dealsService.getDeals.and.callFake(function () {
                    return {
                        then: angular.noop
                    };
                });

                controller.dealsList.resources = ['one', 'two'];
                controller.queryObject.offset = 79;

                //act
                controller.sortColumn(columnName);

                //assert
                expect(controller.queryObject.offset).toBe(0);
                expect(controller.dealsList.resources.length).toBe(0);

            });
        });

        describe('searchDeals', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get filtered deals with the query object', function () {

                //arrange
                controller.searchText = 'text!';
                var expectedSearchQuery = {
                    searchText: 'text!',
                    offset: 0
                };

                controller.queryObject = {};
                controller.dealsList.resources.length = 0;

                //act
                controller.searchDeals();

                //assert
                expect(dealsService.getDeals).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.dealsList).toEqual(fakeDeals);
            });

            it('should not get deals with the query object when searchText < 3', function () {

                //arrange
                dealsService.getDeals.calls.reset(); //reset the spy
                controller.searchText = 'aa';
                controller.dealsList.resources.length = 0;

                //act
                controller.searchDeals();

                //assert
                expect(dealsService.getDeals).not.toHaveBeenCalled();
                expect(controller.dealsList.resources.length).toBe(0);
            });

            it('should clear current deals and offset', function () {

                //arrange
                dealsService.getDeals.and.callFake(function () {
                    return {
                        then: angular.noop
                    };
                });

                controller.dealsList.resources = ['one', 'two'];
                controller.queryObject.offset = 79;
                controller.searchText = 'text!';

                //act
                controller.searchDeals();

                //assert
                expect(controller.queryObject.offset).toBe(0);
                expect(controller.dealsList.resources.length).toBe(0);

            });

        });

        describe('selectRow', function () {

            var event;

            beforeEach(function () {

                //Flatten timeout
                $timeout = function (callback) {
                    return callback();
                };

                event = {
                    target: {
                        id: ''
                    }
                };

                activateController();
            });

            it('should send false to the mentions service isWatched is true and event.target.id is esx-watch', function () {
                // Arrange
                var deal = {
                    isWatched: true
                };
                event.target.id = 'esx-watch';


                // Act
                controller.selectRow(deal, event);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(deal.dealMentionId, false);
            });

            it('should send true to the mentions service when isWatched is false and event.target.id is esx-watch', function () {
                // Arrange
                var deal = {
                    isWatched: false
                };
                event.target.id = 'esx-watch';

                // Act
                controller.selectRow(deal, event);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(deal.dealMentionId, true);
            });

            it('should change the state to deal.Summary', function () {
                //arrange
                spyOn($state, 'go');

                var deal = {
                    dealMentionId: "@me"
                };

                // Act
                controller.selectRow(deal, event);

                //assert
                expect($state.go).toHaveBeenCalledWith('deal.dealSummary', { 'dealMentionId': deal.dealMentionId });

            });
        });

    });
})();



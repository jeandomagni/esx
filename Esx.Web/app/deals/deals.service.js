﻿(function () {
    'use strict';

    var serviceId = 'dealsService';

    angular.module('app').factory(serviceId, dealsService);

    dealsService.$inject = ['uriService', '$resource'];

    function dealsService(uriService, $resource) {

        var service = {
            getDeals: getDeals,
            getDealProperties: getDealProperties,
            getPropertyUsages: getPropertyUsages,
            getLatestValuation: getLatestValuation
        };

        return service;

        function getDealProperties(query) {
            return $resource(uriService.dealProperties).get(query).$promise;
        }

        function getDeals(query) {
            return $resource(uriService.dealsList).get(query).$promise;
        }

        function getPropertyUsages(dealKey, propertyAssetKeys) {
            var query = {
                dealId: dealKey,
                propertyAssetKeys: propertyAssetKeys
            };
            return $resource(uriService.dealPropertyUsages, { dealId: '@dealId' }).query(query).$promise;
        }

        function getLatestValuation(dealKey, propertyAssetKeys) {
            var query = {
                dealId: dealKey,
                propertyAssetKeys: propertyAssetKeys
            };
            return $resource(uriService.dealLastestValuation, { dealId: '@dealId' }).get(query).$promise;
        }
    }
})();
﻿describe('hashtag.service', function() {

    beforeEach(module('app'));

    var hashtagService,
        uriService,
        $httpBackend;

    beforeEach(inject(function(_hashtagService_, _uriService_, _$httpBackend_) {
        hashtagService = _hashtagService_;
        uriService = _uriService_;
        $httpBackend = _$httpBackend_;

        uriService.hashtag = '/hashtag/:tag';
        uriService.tagsList = 'api/tags/list';

        $httpBackend.whenGET(/.*.html/).respond(200, '');
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('getTags', function () {

        it('should call server with tags uri', function () {
            //Arrange
            var query = 'aT';
            var expectedUri = uriService.tagsList + "?query=" + query;

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond([]);

            //Act
            hashtagService.getTags(query);

            //Assert
            $httpBackend.flush();
        });
    });

    describe('getTagNotes', function () {

        it('should call server with tag notes uri', function () {
            //Arrange
            var query = 'aT';
            var expectedUri = uriService.tagNotes+ '?tagName=' + query;

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond([]);

            //Act
            hashtagService.getTagNotes(query);

            //Assert
            $httpBackend.flush();
        });
    });

});
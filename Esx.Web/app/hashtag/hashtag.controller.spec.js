﻿describe('hashtag.controller', function () {

    beforeEach(module('app'));

    var $controller,
        common,
        hashtagService,
        fakeTagNotes,
        $scope,
        controller;

    var activateController = function (tag) {

        common.$stateParams.tag = tag;

        controller = $controller('hashtagController', {
            common: common,
            hashtagService: hashtagService
        });
    };

    beforeEach(inject(function (_$controller_, _common_, _hashtagService_) {

        // inject
        $controller = _$controller_;
        common = _common_;
        hashtagService = _hashtagService_;

        // fakes
        $scope = {};

        common.$window = { location: {} };

        //fakeTags = {
        //    id: 1,
        //    fullName: 'arialview'
        //};

        fakeTagNotes = [{
            noteId: 1,
            noteText: 'This is a test #abc note.'
        }];

        // spys
        //spyOn(hashtagService, 'getTags').and.callFake(function () {
        //    return {
        //        then: function () {
        //            return callback(fakeTags);
        //        }
        //    }
        //});

        spyOn(hashtagService, 'getTagNotes').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback(fakeTagNotes);
                }
            }
        });
    }));

    describe('get tag notes', function () {

        var tag = 'arialview';

        beforeEach(function () {
            activateController(tag);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

        it('should have correct tite', function () {
            expect(controller.title).toBe('Hashtag');
        });

        it('should set page data', function() {
            expect(hashtagService.getTagNotes).toHaveBeenCalledWith(tag);
            expect(controller.hashtag.tagNotes).toEqual(fakeTagNotes);
        });
    });

});
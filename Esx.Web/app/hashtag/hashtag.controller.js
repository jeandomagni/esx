﻿(function () {
    'use strict';

    var controllerId = 'hashtagController';

    angular.module('app').controller(controllerId, hashtagController);

    hashtagController.$inject = ['common', 'hashtagService'];

    function hashtagController(common, hashtagService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Hashtag';

        vm.hashtag = {tagName: common.$stateParams.tag };

        activate();

        function activate() {
                common.activateController([getTagNotes()], controllerId)
                    .then(function () { log('Retrieved hashtag details.'); });
        }

        function getTagNotes() {
            return hashtagService.getTagNotes(vm.hashtag.tagName).then(function (tagNotes) {
                vm.hashtag.tagNotes = tagNotes;
            });
        }
    }
})();
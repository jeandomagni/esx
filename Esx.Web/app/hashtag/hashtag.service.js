﻿(function () {
    'use strict';

    var serviceId = 'hashtagService';

    angular.module('app').factory(serviceId, hashtagService);

    hashtagService.$inject = ['uriService', '$resource'];

    function hashtagService(uriService, $resource) {

        var service = {
            getTags: getTags,
            getTagNotes: getTagNotes
        };

        return service;

        function getTagNotes(tagName) {
            return $resource(uriService.tagNotes, { tagName: '@tagName' }).query({ tagName: tagName }).$promise;
        }

        function getTags(query) {
            return $resource(uriService.tagsList, { query: '@query' }).query({ query: query }).$promise;
        }
    }
})();
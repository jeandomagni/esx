﻿(function () {
    'use strict';

    var app = angular.module('app');

    // Configure the routes and route resolvers
    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routeConfigurator($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/");

        $stateProvider
             .state('pipeline', {
                 url: '/',
                 templateUrl: 'app/pipeline/pipeline.view.html',
                 controller: 'pipeline',
                 controllerAs: 'vm',
                 data: {
                     title: 'Pipeline',
                     sidebaricon: 'dashboard',
                     includeInSideBar: true,
                     subnavClass: 'springboard-bg',
                     subtabs: [
                         { sref: 'pipeline', label: 'COMPANY' },
                         { sref: 'pipelineoffice', label: 'OFFICE' },
                         { sref: 'pipelinemy', label: 'MY PIPELINE' }
                     ]
                 }
             }).state('pipelineoffice', {
                 url: '/officepipeline',
                 templateUrl: 'app/pipeline/pipeline.view.html',
                 controller: 'pipeline',
                 controllerAs: 'vm',
                 data: {
                     title: 'Pipeline',
                     sidebaricon: 'dashboard',
                     includeInSideBar: true,
                     subnavClass: 'springboard-bg',
                     subtabs: [
                         { sref: 'pipeline', label: 'COMPANY' },
                         { sref: 'pipelineoffice', label: 'OFFICE' },
                         { sref: 'pipelinemy', label: 'MY PIPELINE' }
                     ]
                 }
             }).state('pipelinemy', {
                 url: '/mypipeline',
                 templateUrl: 'app/pipeline/pipeline.view.html',
                 controller: 'pipeline',
                 controllerAs: 'vm',
                 data: {
                     title: 'Pipeline',
                     sidebaricon: 'dashboard',
                     includeInSideBar: true,
                     subnavClass: 'springboard-bg',
                     subtabs: [
                         { sref: 'pipeline', label: 'COMPANY' },
                         { sref: 'pipelineoffice', label: 'OFFICE' },
                         { sref: 'pipelinemy', label: 'MY PIPELINE' }
                     ]
                 }
             })
            .state('newsFeed', {
                url: '/pulse',
                templateUrl: 'app/newsFeed/newsFeed.html',
                controller: 'newsFeed',
                controllerAs: 'vm',
                data: {
                    title: 'Pulse',
                    sidebaricon: 'newsFeed',
                    includeInSideBar: true,
                    subnavClass: 'springboard-bg',
                    subtabs: [
                        { sref: 'newsFeed', label: 'FOLLOWING' },
                        { sref: 'newsFeedEveryone', label: 'EVERYONE' }
                    ]
                }
            })
            .state('newsFeedEveryone', {
                url: '/pulse',
                templateUrl: 'app/newsFeed/newsFeed.html',
                controller: 'newsFeed',
                controllerAs: 'vm',
                data: {
                    title: 'Pulse',
                    sidebaricon: 'newsFeed',
                    includeInSideBar: true,
                    subnavClass: 'springboard-bg',
                    subtabs: [
                        { sref: 'newsFeed', label: 'FOLLOWING' },
                        { sref: 'newsFeedEveryone', label: 'EVERYONE' }
                    ]
                }
            })
            .state('reminders', {
                url: '/reminders',
                templateUrl: 'app/newsFeed/newsFeed.html',
                controller: 'newsFeed',
                controllerAs: 'vm',
                data: {
                    title: 'Reminders',
                    sidebaricon: 'reminders',
                    includeInSideBar: true,
                    subnavClass: 'reminders-bg',
                    subtabs: [
                        { sref: 'reminders', label: 'ACTIVE' },
                        { sref: 'remindersSnoozed', label: 'SNOOZED' },
                        { sref: 'remindersDone', label: 'DONE' }
                    ]
                }
            })
            .state('remindersSnoozed', {
                url: '/reminders',
                templateUrl: 'app/newsFeed/newsFeed.html',
                controller: 'newsFeed',
                controllerAs: 'vm',
                data: {
                    title: 'Reminders',
                    sidebaricon: 'reminders',
                    includeInSideBar: true,
                    subnavClass: 'reminders-bg',
                    subtabs: [
                        { sref: 'reminders', label: 'ACTIVE' },
                        { sref: 'remindersSnoozed', label: 'SNOOZED' },
                        { sref: 'remindersDone', label: 'DONE' }
                    ]
                }
            })
            .state('remindersDone', {
                url: '/reminders',
                templateUrl: 'app/newsFeed/newsFeed.html',
                controller: 'newsFeed',
                controllerAs: 'vm',
                data: {
                    title: 'Reminders',
                    sidebaricon: 'reminders',
                    includeInSideBar: true,
                    subnavClass: 'reminders-bg',
                    subtabs: [
                        { sref: 'reminders', label: 'ACTIVE' },
                        { sref: 'remindersSnoozed', label: 'SNOOZED' },
                        { sref: 'remindersDone', label: 'DONE' }
                    ]
                }
            })
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'app/dashboard/dashboard.html',
                controller: 'dashboard',
                controllerAs: 'vm',
                data: {
                    title: 'Springboard',
                    sidebaricon: 'dashboard',
                    includeInSideBar: true,
                    subnavClass: 'springboard-bg'
                }
            }).state('mydeals', {
                url: '/mydeals',
                templateUrl: 'app/dashboard/dashboard.html',
                controller: 'dashboard',
                controllerAs: 'vm',
                data: {
                    title: 'My Deals',
                    sidebaricon: 'mydeals',
                    includeInSideBar: true
                }
            }).state('watchlist', {
                url: '/watchlist',
                templateUrl: 'app/dashboard/dashboard.html',
                controller: 'dashboard',
                controllerAs: 'vm',
                data: {
                    title: 'Watchlist',
                    sidebaricon: 'watchlist',
                    includeInSideBar: true
                }
            }).state('notifications', {
                url: '/notifications',
                templateUrl: 'app/dashboard/dashboard.html',
                controller: 'dashboard',
                controllerAs: 'vm',
                data: {
                    title: 'Notifications',
                    sidebaricon: 'notifications',
                    includeInSideBar: true,
                    addDivider: true
                }
            }).state('contacts', {
                url: '/contacts',
                templateUrl: 'app/contacts/contacts.html',
                controller: 'contactsController',
                controllerAs: 'vm',
                data: {
                    title: 'Contacts',
                    defaultSearch: 'contact',
                    sidebaricon: 'contacts',
                    includeInSideBar: true,
                    subnavClass: 'contacts-bg'
                }
            }).state('contact', {
                url: '/contact/:contactId',
                templateUrl: 'app/contacts/contact.html',
                controller: 'contactController',
                controllerAs: 'vm',
                data: {
                    title: '',
                    subnavClass: 'contacts-bg',
                    subtabs: [
                        { sref: 'contact.summary', label: 'SUMMARY' },
                        { sref: 'contact.details', label: 'DETAILS' },
                        { sref: 'contact.deals', label: 'DEALS' },
                        { sref: 'contact.marketinglists', label: 'MARKETING LISTS' }
                    ]
                }
            }).state('contact.summary', {
                url: '/summary',
                templateUrl: 'app/contacts/contact.summary.html',
                controller: 'contactSummaryController',
                controllerAs: 'vm',
                data: {
                    title: ''
                }
            }).state('contact.details', {
                url: '/details',
                templateUrl: 'app/contacts/contact.details.html',
                controller: 'contactDetailController',
                controllerAs: 'vm',
                data: {
                    title: ''
                }
            }).state('contact.deals', {
                url: '/deals',
                templateUrl: 'app/contacts/contact.details.html',//TODO create
                controller: 'contactDetailController',//TODO create
                controllerAs: 'vm',
                data: {
                    title: ''
                }
            }).state('contact.marketinglists', {
                url: '/marketinglists',
                templateUrl: 'app/contacts/contact.details.html',//TODO create
                controller: 'contactDetailController',//TODO create
                controllerAs: 'vm',
                data: {
                    title: ''
                }
            })
            .state('dealContacts', {
                url: '/deals/:dealId/contacts',
                templateUrl: 'app/contacts/contacts.html',
                controller: 'contactsController',
                controllerAs: 'vm',
                data: {
                    title: 'Deal Contacts'
                }
            }).state('employees', {
                url: '/employees?filters',
                templateUrl: 'app/employees/employees.html',
                controller: 'employeesController',
                controllerAs: 'vm',
                data: {
                    title: 'Employees',
                    defaultSearch: 'employee'
                }
            }).state('employee', {
                url: '/employees/:employeeId',
                templateUrl: 'app/employees/employee.html',
                controller: 'employeeController',
                controllerAs: 'vm',
                data: {
                    title: 'Employee Details'
                }
            }).state('hashtag', {
                url: '/hashtag/:tag',
                templateUrl: 'app/hashtag/hashtag.html',
                controller: 'hashtagController',
                controllerAs: 'vm',
                data: {
                    title: 'Hashtag'
                }
            }).state('settings', {
                url: '/settings',
                templateUrl: 'app/dashboard/dashboard.html',
                controller: 'dashboard',
                controllerAs: 'vm',
                data: {
                    title: 'Settings',
                    sidebaricon: 'settings',
                    includeInSideBar: true
                }
            });
    }
})();
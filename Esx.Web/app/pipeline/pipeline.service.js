﻿(function () {
    'use strict';

    var serviceId = 'pipelineFactory';

    angular.module('app').factory(serviceId, pipelineFactory);

    pipelineFactory.$inject = ['uriService', '$resource','$http'];

    function pipelineFactory(uriService, $resource, $http) {

        var resource = $resource(uriService.deal, {}, {
            query: {
                method: 'GET',
                isArray: true
                }
        })

        var service = {
            getActivePipeline: getActivePipeline,
            getClosedDeals: getClosedDeals
        };

        return service;

        function getActivePipeline() {
            return $resource(uriService.dealActivePipeline).query().$promise;
        }

        function getClosedDeals() {
            return $resource(uriService.dealClosedDeals).query().$promise;
        }
    }
})();



﻿(function () {
    'use strict';

    var controllerId = 'pipeline';

    angular
      .module('app')
        .controller(controllerId, pipeline);

    pipeline.$inject = ['common','pipelineFactory','pipelineService','$state'];

    function pipeline(common, pipelineFactory, pipelineService, $state) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        vm.periods = [{ period: '30 Days' }, { period: '60 Days' }, { period: '90 Days' }, { period: '120 Days' }];

        function getActivePipeline() {
            pipelineFactory.getActivePipeline().then(function (activeDeals) {
                //filter list 
                var filteredDeals = pipelineService.filterDeals(activeDeals,$state.current.name); 
                //add class for type
                filteredDeals.forEach(function (row) {
                        row.class = pipelineService.getRowClass(row.entityType);
                });
                vm.activePipeline = filteredDeals;
        });
            
        }

        function getClosedDeals() {
            return pipelineFactory.getClosedDeals().then(function (closedDeals) {
                //filter list 
                var filteredDeals = pipelineService.filterDeals(closedDeals, $state.current.name);
                //add class for type
                filteredDeals.forEach(function (row) {
                    row.class = pipelineService.getRowClass(row.entityType);
                });
                vm.closedDeals = filteredDeals;
            });
        }

        //sort for active pipeline
        vm.predicate = 'dealId';
        vm.reverse = false;

        vm.order = function (predicate) {
            vm.reverse = (vm.predicate === predicate) ? !vm.reverse : false;
            vm.predicate = predicate;
        };

        //sort for closed deals
        vm.closedPredicate = 'dealId';
        vm.closedReverse = false;

        vm.orderClosed = function (predicate) {
            vm.closedReverse = (vm.closedPredicate === predicate) ? !vm.closedReverse : false;
            vm.closedPredicate = predicate;
        };


        function activate() {
            common.activateController([getActivePipeline(), getClosedDeals()], controllerId)
                .then(function () { });
        }

        activate();

    }
})();
﻿(function () {
    'use strict';

    var serviceId = 'pipelineService';
    angular
        .module('app')
        .service(serviceId, pipelineService);


    function pipelineService() {
      
        this.getRowClass = function (row) {
            var rowClass;
            if (row === 'Property') {
                rowClass = 'esx-search-properties'
            } else if (row === 'Pitch') {
                rowClass = 'esx-search-pitch'
            }
            return rowClass;
        }

        this.filterDeals =  function(deals,state) {
            var filteredDeals;
            if (state === "pipelinemy") {
                filteredDeals = deals.filter(function (deal) {
                    return deal.myDeal === true; 
                });
            } else if (state === "pipelineoffice") {
                filteredDeals = deals.filter(function (deal) {
                    return deal.myOffice === true;
                });
            } else {
                filteredDeals = deals;
            }
            return filteredDeals;
        }
    }
})();

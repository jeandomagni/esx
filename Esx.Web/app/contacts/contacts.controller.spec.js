﻿(function () {
    describe('contacts.controller', function () {

        beforeEach(module('app'));

        var $controller,
            common,
            contactsService,
            mentionsService, //TODO next story 
            fakeContacts,
            $scope,
            controller,
            _;

        var activateController = function (context) {
            controller = $controller('contactsController', {
                common: common,
                contactsService: contactsService,
                mentionsService: mentionsService,
                '_': _
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _contactsService_, _mentionsService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            contactsService = _contactsService_;
            mentionsService = _mentionsService_;
            _ = window._; // lodash

            //fakes
            $scope = {
                $apply: angular.noop
            };


            fakeContacts = {
                resources: [
                    { fullName: 'John Landers', title: 'Deal Coordinator', company: 'Colony American Homes' },
                    { fullName: 'Bob Dobbs', title: 'Managing Director', company: 'Church of the SubGenius' }
                ],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            //spys
            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn(contactsService, 'getContacts').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeContacts);
                    }
                };
            });

            spyOn(mentionsService, 'setIsWatched');

        }));

        describe('activate', function () {
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define the query object. ', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.contactsList.pagingData).toEqual(expectedPagingData);
            });
        });

        describe('getContacts', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get contacts with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    offset: 0,
                    pageSize: 30
                };

                controller.queryObject = expectedSearchQuery;
                controller.contactsList.resources.length = 0; // clear contacts that "activate" added.

                //act
                controller.getContacts();

                //assert
                expect(contactsService.getContacts).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.contactsList).toEqual(fakeContacts);
            });

        });

        describe('sort', function () {
            beforeEach(function () {
                activateController();
            });

            it('should sort contacts list', function () {
                var columnName = 'fullName';

                //arrange
                var queryObject = { filters: undefined, searchText: '' };

                controller.queryObject = queryObject;

                controller.sortColumn(columnName);

                expect(contactsService.getContacts).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.contactsList).toEqual(fakeContacts);
            });
        });

        describe('searchContacts', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get filtered contacts with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    searchText: 'text!'
                };

                controller.queryObject = expectedSearchQuery;
                controller.contactsList.resources.length = 0;

                //act
                controller.searchContacts();

                //assert
                expect(contactsService.getContacts).toHaveBeenCalledWith(expectedSearchQuery);
                expect(controller.contactsList).toEqual(fakeContacts);
            });

            it('should not get contacts with the query object when searchText < 3', function () {

                //arrange
                contactsService.getContacts.calls.reset(); //reset the spy
                controller.searchText = 'aa';
                controller.contactsList.resources.length = 0;

                //act
                controller.searchContacts();

                //assert
                expect(contactsService.getContacts).not.toHaveBeenCalled();
                expect(controller.contactsList.resources.length).toBe(0);
            });

        });

        describe('toggleIsWatched', function () {
            beforeEach(function () {
                activateController();
            });

            it('should send false to the mentions service when the current state is true', function () {
                // Arrange
                var contact = {
                    contactMentionId: "@me",
                    isWatched: true
                };

                // Act
                controller.toggleIsWatched(contact);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(contact.contactMentionId, false);
            });

            it('should send true to the mentions service when the current state is false', function () {
                // Arrange
                var contact = {
                    contactMentionId: "@me",
                    isWatched: false
                };

                // Act
                controller.toggleIsWatched(contact);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(contact.contactMentionId, true);
            });
        });

    });
})();



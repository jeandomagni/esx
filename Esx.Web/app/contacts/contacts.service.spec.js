﻿describe('contacts.service', function () {

    beforeEach(module('app'));

    var contactsService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_contactsService_,_uriService_, _$httpBackend_) {

        contactsService = _contactsService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        uriService.contactsList = '/contacts';
        uriService.contact = '/contacts/:contactId';

        // ignore html templates. Not sure if this is a good approach. 
        $httpBackend.whenGET(/.*.html/).respond(200, '');

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getContact', function () {
        it('should call server with contact uri', function () {
            var contactId = 123;

            //Arrange
            $httpBackend.expectGET("api/contacts/" + contactId, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            contactsService.getContact(contactId);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('getContacts', function () {

        it('should call server with contacts list uri', function () {

            //Arrange
            $httpBackend.expectGET(uriService.contactsList, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            contactsService.getContacts();

            //Assert
            $httpBackend.flush();

        });

        it('should return a promise that resolves', function () {

            //Arrange
            var fakeContacts = {
                resources: [{ name: 'some name' }]
            };
            
            var testPromise = function (promise) {
                expect(promise.$resolved).toBe(true);
                expect(promise.resources).toEqual(fakeContacts.resources);
            }
            
            $httpBackend.expectGET(uriService.contactsList, { "Accept": "application/json, text/plain, */*" }).respond(fakeContacts);

            //Act & Assert
            contactsService.getContacts().then(testPromise);

            //Flush
            $httpBackend.flush();

        });

        it('should make GET request with query', function () {

            //Arrange
            var query = {
                offset: 100,
                pageSize: 10
            }

            var expectedUri = uriService.contactsList + '?'+ $.param(query);

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            contactsService.getContacts(query);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('searchContacts', function () {

        it('should call server with contacts search uri', function () {

            //Arrange
            var query = { searchText: 'smith' };
            var expectedUri = uriService.contactsSearch + '?searchText=smith';

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            contactsService.searchContacts(query);

            //Assert
            $httpBackend.flush();

        });

       
    });

    describe('contactDeals', function () {
        it('should call server with contactDeals uri', function () {
            var contactId = 123;

            //Arrange
            $httpBackend.expectGET('api/contacts/' + contactId + '/deals', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            contactsService.getContactDeals(contactId);

            //Assert
            $httpBackend.flush();

        });
    });
});
﻿(function () {
    'use strict';

    var controllerId = 'contactDetailController';

    angular
      .module('app')
        .controller(controllerId, contactDetailController);


    contactDetailController.$inject = ['common', 'contactsService','utilsService', '$stateParams'];

    function contactDetailController(common, contactsService,utilsService, $stateParams) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.getContact = getContact;

        vm.contactId = null;
        if ($stateParams.contactId) {
            vm.contactId = parseInt($stateParams.contactId);
        }
      

        function activate() {
            common.activateController([getContact(vm.contactId)], controllerId)
                .then(function () { log('Activated Contact Details.'); })
                .then();
        }

        activate();

        function getContact(contactId) {
            return contactsService.getContact(contactId).then(function (contactData) {

                if (contactData.website) {
                    contactData.website = utilsService.scrubURL(contactData.website);
                }
                vm.contact = contactData;
                vm.contactId = contactData.contactId;
                common.setPageHeading(contactData.fullName, contactData.contactMentionId);
            }); 
        }

    }
})();
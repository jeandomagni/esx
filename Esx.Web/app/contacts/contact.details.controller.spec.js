﻿(function () {
    describe('contact.details.controller', function () {

        beforeEach(module('app'));

        var $controller,
            common,
            contactsService,
            fakeContact,
            $scope,
            controller,
            _;

        var activateController = function (context) {
            controller = $controller('contactDetailController', {
                common: common,
                contactsService: contactsService,
                '_': _
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _contactsService_, _mentionsService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            contactsService = _contactsService_;
            _ = window._; // lodash

            //fakes
            fakeContact = {
                fullName: 'Bob Dobbs',
                title: 'Director of Acquisitions',
                status: 'active',
                linkedIn: 'www.linkedin.com/bobdobbs',
                company: 'Blackstone Group',
                mobilePhone: '212-555-1212',
                officePhone: '212-555-7707',
                email: 'bobdobbs@blackstone.com',
                address: '345 Park Ave. STE 707',
                city: 'New York',
                state: 'NY',
                zipCode: '10019',
                webSite: 'www.blackstone.com'
            };

            //spys
            spyOn(contactsService, 'getContact').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeContact);
                    }
                };
            });


        }));

        describe('activate', function () {
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });
        });

        describe('getContacts', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get contact by contactId', function () {

                //arrange
                var contactId = 8675309;
                controller.contact = {}; // clear the contact.

                //act
                controller.getContact(contactId);

                //assert
                expect(contactsService.getContact).toHaveBeenCalledWith(contactId);
                expect(controller.contact).toEqual(fakeContact);
            });

        });

    });
})();



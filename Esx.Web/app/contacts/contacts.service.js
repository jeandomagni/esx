﻿(function () {
    'use strict';

    var serviceId = 'contactsService';

    angular.module('app').factory(serviceId, contactsService);

    contactsService.$inject = ['uriService', '$resource'];

    function contactsService(uriService, $resource) {

        var contactResource = $resource(
            uriService.contact,
            { contactId: '@id' },
            { 'update': { method: 'PATCH' } }
        );

        var service = {
            getContacts: getContacts,
            searchContacts: searchContacts,
            getContact: getContact,
            getContactSummary:getContactSummary,
            getContactDeals: getContactDeals
        };

        return service;

        function getContacts(query) {
            return $resource(uriService.contactsList).get(query).$promise;
        }
        
        function searchContacts(query) {
            return $resource(uriService.contactsSearch).get(query).$promise;
        }

        function getContact(id) {
            return contactResource.get({ contactId: id }).$promise;
        }
        function getContactSummary(contactId,pitches,bidActivity,holdPeriod, loansMaturing) {
            return $resource(uriService.contactSummary).get({ contactId: contactId,pitches: pitches, bidActivity: bidActivity,holdPeriod: holdPeriod, loansMaturing : loansMaturing }).$promise;
        }
       
        function getContactDeals(id) {
            return $resource(uriService.contactDeals, { contactId: '@id' }).get({ contactId: id }).$promise;
        }
    }
})();

﻿(function () {
    describe('contact.summary.controller', function () {

        beforeEach(module('app'));

        var $controller,
            common,
            contactsService,
            fakeSummary,
            controller,
            _;

        var activateController = function () {
            controller = $controller('contactSummaryController', {
                common: common,
                contactsService: contactsService,
                '_': _
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _contactsService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            contactsService = _contactsService_;
            _ = window._; // lodash

            //fakes
            fakeSummary = {
                ContactId: 123,
                FullName: 'dick blick',
                SummaryRollup: {
                    Pitches: 1,
                    ActiveDeals: 2
                }

            };

            //spys
            spyOn(contactsService, 'getContactSummary').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeSummary);
                    }
                };
            });


        }));

        describe('activate', function () {
            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });
        });

        describe('getContactSummary', function () {
            beforeEach(function () {
                activateController();
            });

            it('should get contact summary by contactId', function () {

                //arrange
                var contactId = 8675309;
                var pitch = 1;
                var bid = 2;
                var hold = 3;
                var loan = 4;

                controller.contactSummary = {}; // clear the contact summary.

                //act
                controller.getContactSummary(contactId, pitch, bid, hold, loan);

                //assert
                expect(contactsService.getContactSummary).toHaveBeenCalledWith(contactId, pitch, bid, hold, loan);
                expect(controller.contactSummary).toEqual(fakeSummary);
            });

        });

    });
})();
﻿(function () {
    'use strict';

    var controllerId = 'contactController';
    angular
      .module('app')
        .controller(controllerId, contactController);

    contactController.$inject = ['common'];

    function contactController(common) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        function activate() {
            common.activateController([], controllerId)
                .then(function () { log('Activated Contact Controller'); });
        }

        activate();

    }
})();
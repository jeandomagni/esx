﻿(function () {
    'use strict';

    var controllerId = 'contactSummaryController';

    angular
      .module('app')
        .controller(controllerId, contactSummaryController);


    contactSummaryController.$inject = ['common', 'contactsService', '$stateParams'];

    function contactSummaryController(common, contactsService, $stateParams) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        //TODO put this in a shared service, or make it come from the database
        vm.pitchPeriods = [
            { text: 'Last 30 Days', value: 30 },
            { text: 'Last 45 Days', value: 45 },
            { text: 'Last 90 Days', value: 90 },
            { text: 'Last 180 Days', value: 180 }
        ];

        vm.bidActivityPeriods = [
            { text: 'Last 1 Month', value: 30 },
            { text: 'Last 3 Months', value: 90 },
            { text: 'Last 6 Months', value: 180 },
            { text: 'Last 12 Months', value: 365 }
        ];
        vm.holdPeriods = [
            { text: '> 3 Years', value: 36 },
            { text: '> 2 Years', value: 24 }
        ];
        vm.loansMaturingPeriods = [
            { text: 'Next 30 Days', value: 30 },
            { text: 'Next 60 Days', value: 60 },
            { text: 'Next 90 Days', value: 90 },
            { text: 'Next 180 Days', value: 180 },
            { text: 'Next 1 Yr', value: 365 }
        ];

        vm.selectedOptions = {
            pitches: 45,
            bidActivity: 90,
            holdPeriod: 36,
            loansMaturing: 180
        };


        vm.getContactSummary = getContactSummary;

        vm.contactId = null;
        if ($stateParams.contactId) {
            vm.contactId = parseInt($stateParams.contactId);
        }
        vm.refreshSummary = function() {
            getContactSummary(vm.contactId, vm.selectedOptions.pitches, vm.selectedOptions.bidActivity, vm.selectedOptions.holdPeriod, vm.selectedOptions.loansMaturing)
        }

        function activate() {
            common.activateController([getContactSummary(vm.contactId, vm.selectedOptions.pitches, vm.selectedOptions.bidActivity, vm.selectedOptions.holdPeriod, vm.selectedOptions.loansMaturing)], controllerId)
                .then(function () { log('Activated Contact Summary.'); });
        }

        function getContactSummary(contactId, pitches, bidActivity, holdPeriod, loansMaturing) {
            return contactsService.getContactSummary(contactId, pitches, bidActivity, holdPeriod, loansMaturing).then(function (contactData) {
                 vm.contactSummary = contactData;
                 vm.contactId = contactData.contactId;
                 common.setPageHeading(contactData.fullName, contactData.contactMentionId, contactData.officePhone, contactData.emailAddress);
            });

        }

        activate();
    }
})();
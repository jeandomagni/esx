﻿(function () {
    'use strict';

    var controllerId = 'contactsController';

    angular
      .module('app')
        .controller(controllerId, contactsController);

    contactsController.$inject = ['common', 'contactsService', 'companiesService', 'mentionsService', '_', '$state', '$timeout'];

    function contactsController(common, contactsService, companiesService, mentionsService, _, $state, $timeout) {

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.searchText = '';

        vm.getContacts = _.debounce(getContacts, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.toggleIsWatched = _.debounce(toggleIsWatched, 200);
        vm.searchContacts = _.debounce(searchContacts, 200);
        vm.editContact = editContact;
        vm.selectRow = selectRow;

        vm.queryObject = {
            offset: 0,
            pageSize: 30
        };

        vm.contactsList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        }

        activate();
        function activate() {
            common.activateController([getContacts(false)], controllerId)
                .then(function () { log('Activated Contacts.'); });
        }


        function getContacts(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            return contactsService.getContacts(vm.queryObject).then(function (contactsList) {
                vm.contactsList.resources = vm.contactsList.resources.concat(contactsList.resources);
                vm.queryObject.isBusy = false;
                return vm.contactsList;
            });
        }

        function searchContacts() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.contactsList.resources.length = 0;
                vm.queryObject.offset = 0;
                getContacts(true);
            }
        }

        function sortColumn(columnName) {
            vm.contactsList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            getContacts(false);
        }
        
        function editContact(contact) {
            $state.go('contact.details', { 'contactId': contact.contactId });
        }

        function selectRow(item, event) {
            $timeout(function () {
                if (event.target.id === 'esx-watch')
                    toggleIsWatched(item);
                else
                    editContact(item);
            });
        }

        function toggleIsWatched(contact) {
            contact.isWatched = !contact.isWatched;
            mentionsService.setIsWatched(contact.contactMentionId, contact.isWatched);
        }
    }
})();
﻿describe('contact.controller', function () {

    beforeEach(module('app'));

    var $controller,
        common,
        controller;

    var activateController = function () {
        controller = $controller('contactController', {
            common: common
        });
    };

    beforeEach(inject(function (_$controller_, _common_) {
        //inject
        $controller = _$controller_;
        common = _common_;
    }));

    describe('activate', function () {
        beforeEach(function () {
            activateController();
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

    });

});
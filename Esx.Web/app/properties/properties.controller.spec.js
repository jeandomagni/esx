﻿(function () {
    describe('properties.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            propertiesService,
            fakeProperties,
            fakeProperty,
            $scope,
            controller,
            dataToVerify,
            mentionsService,
            _;

        var activateController = function () {
            dataToVerify = {};

            controller = $controller('propertiesController', {
                common: common,
                propertiesService: propertiesService,
                '_': _
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _propertiesService_, _mentionsService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            propertiesService = _propertiesService_;
            mentionsService = _mentionsService_;
            _ = window._; // lodash
            //fakes
            $scope = {};

            common.$window = { location: {} };

            fakeProperties = {
                resources: [{ name: 'some property' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            fakeProperty = {
                propertyID: 1234,
                propertyName: 'test property'
            };

            //spys
            spyOn(propertiesService, 'getProperties').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeProperties);
                    }
                };
            });

            spyOn(propertiesService, 'searchProperties').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeProperties);
                    }
                };
            });

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

            spyOn(mentionsService, 'setIsWatched');

        }));

        afterEach(function () {
            common.$stateParams.propertyId = undefined;
            common.$stateParams.companyId = undefined;
        });

        describe('property activate', function () {

            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define queryObject', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.propertiesList.pagingData).toEqual(expectedPagingData);
            });
        });

        describe('searchProperties', function () {
            beforeEach(function () {
                activateController();
            });

            it('should set search results', function () {
                //arrange
                var query = 'testQuery';
                controller.searchText = query;

                //act
                controller.searchProperties();

                //assert
                expect(propertiesService.getProperties).toHaveBeenCalledWith(controller.queryObject);
                expect(controller.queryObject.offset).toBe(0);
                expect(controller.queryObject.searchText).toBe(query);
            });

            it('should not search if search text is invalid', function () {
                //arrange
                var searchQuery = 'x';
                var offset = 10;
                controller.queryObject.offset = offset;
                //act
                controller.searchProperties(searchQuery);

                //assert
                expect(controller.queryObject.offset).not.toBe(offset);
            });
        });

        describe('property search selection', function () {

            beforeEach(function () {
                activateController();
            });

            it('should go to property detail', function () {
                //Act
                controller.selectedPropertyChange(fakeProperty);

                //Assert
                expect(common.$window.location.href).toBe('#/properties/1234');
            });
        });

        describe('getProperties', function () {

            beforeEach(function () {
                activateController();
            });

            it('should get properties with the query object', function () {

                //arrange
                var expectedSearchQuery = {
                    someValue: 5,
                    someText: 'text!'
                };

                controller.queryObject = expectedSearchQuery;

                //act
                controller.getProperties();

                //assert
                expect(propertiesService.getProperties).toHaveBeenCalledWith(expectedSearchQuery);
            });

            it('should correctly concatenate next page of results', function () {

                //arrange                
                //act
                controller.getProperties();

                //assert
                expect(controller.propertiesList.resources.length).toBe(2);
            });
        });

        describe('sortColumn', function () {

            beforeEach(function () {
                activateController();
            });

            it('should call the propertiesService.getProperties with correct sort parameter', function () {

                //arrange
                var columnName = 'TestColumn';

                //act
                controller.sortColumn(columnName);

                //assert
                expect(controller.queryObject.sort).toBe(columnName);
                expect(controller.queryObject.sortDescending).toBe(false);
            });
        });

        describe('IsWatched', function () {
            beforeEach(function () {
                activateController();
            });

            it('should toggle watched property and call mention service.', function () {

                // Arrange
                var property = {
                    mentionId: '@test',
                    isWatched: true
                };

                // Act
                controller.toggleIsWatched(property);

                // Assert
                expect(mentionsService.setIsWatched).toHaveBeenCalledWith(property.mentionId, false);
            });
        });

    });
})();

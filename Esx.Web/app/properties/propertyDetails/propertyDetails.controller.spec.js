﻿(function () {
    describe('propertyDetails.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            propertySummaryService,
            controller,
            $mdUtil,
            $mdMedia;

        beforeEach(inject(function (_$controller_, _common_, _propertySummaryService_, _$mdUtil_, _$mdMedia_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            propertySummaryService = _propertySummaryService_;
            $mdUtil = _$mdUtil_;
            $mdMedia = _$mdMedia_;

            //spys
            spyOn(propertySummaryService, 'getDetails').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback({});
                    }
                };
            });
            spyOn(propertySummaryService, 'getValuationHistory').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback({});
                    }
                };
            });

            //instantiate controller
            controller = $controller('propertyDetailsController', {
                common: common,
                propertySummaryService: propertySummaryService,
                $mdMedia: $mdMedia,
                '_': _
            });

        }));

        describe('activate', function () {

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should get details from properties service', function () {
                expect(propertySummaryService.getDetails).toHaveBeenCalled();
            });

            it('should get valuations from properties service', function () {
                expect(propertySummaryService.getValuationHistory).toHaveBeenCalled();
            });
        });

    });
})();
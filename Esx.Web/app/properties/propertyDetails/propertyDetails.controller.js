﻿(function () {
    'use strict';

    var controllerId = 'propertyDetailsController';

    angular.module('app').controller(controllerId, propertyDetailsController);

    propertyDetailsController.$inject = ['common', 'propertySummaryService', '$state', '$stateParams', '_'];

    function propertyDetailsController(common, propertySummaryService, $state, $stateParams, _) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.title = 'Property Details';
        vm.propertyId = common.$stateParams.propertyId;
        vm.valuationsList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        };
        
        vm.queryObject = {
            mentionId: vm.propertyId
        };
        vm.getValuationHistory = getValuationHistory;

        activate();

        function activate() {
            common.activateController([getPropertyDetails(), getValuationHistory()], controllerId)
                .then(function() {
                    
                });
        }

        function getPropertyDetails() {
            return propertySummaryService.getDetails(vm.propertyId).then(function (propertyDetails) {
                vm.propertyDetails = propertyDetails;
            });
        }

        function getValuationHistory() {
            return propertySummaryService.getValuationHistory(vm.queryObject).then(function (valuations) {
                vm.valuationsList.resources = valuations.resources;
                return vm.valuationsList;
            });
        }
    }
})();
﻿describe('property.controller', function () {
    beforeEach(module('app'));

    var $controller,
        common,
        propertySummaryService,
        fakeProperty,
        $scope, 
        controller;

    var activateController = function (propertyId) {
        common.$stateParams.propertyId = propertyId;

        controller = $controller('propertyController', {
            common: common,
            propertySummaryService: propertySummaryService
        });
    };

    beforeEach(inject(function (_$controller_, _common_, _propertySummaryService_) {

        //inject
        $controller = _$controller_;
        common = _common_;
        propertySummaryService = _propertySummaryService_;

        //fakes
        $scope = {};

        common.$window = { location: {} };

        fakeProperty = {
            propertyID: 123,
            propertyName: 'propertyName'
        };
    }));

    describe('activate', function() {

        var propertyId = 0;

        beforeEach(function() {
            activateController(propertyId);
        });

        it('should be defined', function() {
            expect(controller).toBeDefined();
        });
    });

});
﻿describe('properties.service', function () {

    beforeEach(module('app'));

    var propertiesService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_propertiesService_, _uriService_, _$httpBackend_) {

        propertiesService = _propertiesService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        uriService.property = 'api/properties/:propertyId';

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getProperties', function () {
        it('should call server with propertyList uri', function () {

            //Arrange
            $httpBackend.expectGET('api/properties/list', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            propertiesService.getProperties();

            //Assert
            $httpBackend.flush();

        });
    });

    describe('searchProperties', function () {
        it('should call server with search uri', function () {
            var query = 'TestName';

            //Arrange
            $httpBackend.expectGET('api/properties/search?query=' + query, { 'Accept': 'application/json, text/plain, */*' }).respond([]);
            
            //Act
            propertiesService.searchProperties(query);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('getDealsForProperty', function () {
        it('should call server with propertyDeal uri', function () {
            var queryObject = {
                mentionId: '@mention'
            };

            //Arrange
            $httpBackend.expectGET('api/properties/' + queryObject.mentionId + '/deals', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            propertiesService.getDealsForProperty(queryObject);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('getLoansForProperty', function () {
        it('should call server with propertyLoan uri', function () {
            var queryObject = {
                mentionId: '@mention'
            };

            //Arrange
            $httpBackend.expectGET('api/properties/' + queryObject.mentionId + '/loans', { 'Accept': 'application/json, text/plain, */*' }).respond({});

            //Act
            propertiesService.getPropertyLoans(queryObject);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('propertyIntellisense', function () {
        it('should call server with search uri and query', function () {
            var query = 'TestName';

            //Arrange
            $httpBackend.expectGET('api/properties/intellisense?query=' + query, { 'Accept': 'application/json, text/plain, */*' }).respond([]);

            //Act
            propertiesService.propertyIntellisense(query);

            //Assert
            $httpBackend.flush();

        });
    });
});
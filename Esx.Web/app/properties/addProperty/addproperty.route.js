﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider', 'coreType'];

    function routeConfigurator($stateProvider, coreType) {

        $stateProvider
            .state('addproperty', {
                url: '/addproperty',
                templateUrl: 'app/properties/addProperty/addProperty.view.html',
                controller: 'addPropertyController',
                controllerAs: 'vm',
                data: {
                    title: 'Add a Property',
                    sidebaricon: 'properties',
                    includeInSideBar: true,
                    subnavClass: 'properties-bg'
                },
                params: {
                    searchInput: undefined
                }
            });
    }
})();
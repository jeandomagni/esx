﻿(function () {
    'use strict';

    var serviceId = 'addPropertyUtilsService';
    angular
        .module('app')
        .service(serviceId, addPropertyUtilsService);


    function addPropertyUtilsService() {

        this.getSecondaryTyes = function (types, primaryTypeId) {
            return types.filter(function (type) {
                return type.typeId !== primaryTypeId;
            });
        }

        this.formatAddress = function (place) {
            var addressComponents = {
                street_number: '',
                route: '',
                locality: '',
                administrative_area_level_1: '',
                country: '',
                postal_code: '',
                formatted_address: ''
            };

            if (place && place.address_components) {
                place.address_components.forEach(function (component) {
                    var addressType = component.types[0];
                    addressComponents[addressType] = component.short_name
                    addressComponents.formatted_address = place.formatted_address
                });

                return addressComponents;
            }
        }

        this.formatAddressLongNames = function (place) {
            var addressComponents = {
                street_number: '',
                route: '',
                locality: '',
                administrative_area_level_1: '',
                country: '',
                postal_code: '',
                formatted_address: ''
            };

            if (place && place.address_components) {
                place.address_components.forEach(function (component) {
                    var addressType = component.types[0];
                    addressComponents[addressType] = component.long_name
                    addressComponents.formatted_address = place.formatted_address
                });

                return addressComponents;
            }
        }


        //build property object for server
        this.transformModel = function (property) {
            return {
                    propertyName:property.propertyAddress,
                    formattedAddress: property.location.formattedAddress.formatted_address,
                    address: property.location.formattedAddress.street_number + ' ' + property.location.formattedAddress.route,
                    city: property.location.formattedAddress.locality,
                    state: property.location.formattedAddress.administrative_area_level_1,
                    county: property.location.formattedAddressLong.country,
                    zipcode: property.location.formattedAddress.postal_code,
                    latiude: property.location.lat,
                    longitude: property.location.lng
                    }
        }


    }
})();

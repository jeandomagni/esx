﻿(function () {
    'use strict';
    var controllerId = 'addPropertyController';

    angular.module('app').controller(controllerId, addPropertyController);

    addPropertyController.$inject = ['common', 'propertiesService', 'addPropertyUtilsService', 'mapOptions', '$timeout', '$scope', '$location', '$window', 'searchService', 'coreType'];

    function addPropertyController(common, propertiesService, addPropertyUtilsService, mapOptions, $timeout, $scope, $location, $window, searchService, coreType) {

        var vm = this;
        vm.property = {
            uses:[],
            owners:[],
            landOwners:[],
            improvementsOwners:[],
            additionalLocations:[]
        };
   

        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var autocomplete;
        var mergeAutocomplete;
        var subdivideAutoComplete;
        vm.showPropertyToggle = false;
        var map;
        var propertiesList = [];
        vm.campusProperties = []; 

        //initalize form 

        //TODO check for form 
        vm.form = {
            stepOne: { text: 'property-add-text-active', background: 'property-add-step-active',progress:0},
            stepTwo: { text: 'property-add-text-inactive', background: 'property-add-step-inactive', progress: 0 },
            stepThree: { text: 'property-add-text-inactive', background: 'property-add-step-inactive', progress: 0 },
            currentQuestion: 'questionOne.html',
            currentStep: 1
        }
     
        vm.currentQuestion = 'app/properties/addProperty/templates/' + vm.form.currentQuestion;

        //question one 

        function mapCampusProperties(campus) {

            if (campus.campusProperties) {
             
                //create marker
                campus.campusProperties.forEach(function (property) {
                    var lable = campus.campusProperties.indexOf(property) + 1; 
                    var marker = new google.maps.Marker({
                        position: { lng: property.lng, lat: property.lat },
                        map: map,
                        label: lable.toString(),
                        draggable: false,
                        title: property.address,
                        markerId: lable
                    });
                    vm.campusProperties.push(marker); 
                });
            }; 
        };

        vm.showCampus = function (showCampus) {
            vm.property.isPartOfCampus = showCampus;
            if (showCampus === false) {
                vm.selectedCampus = null;
                vm.campusProperties.forEach(function (marker) {
                    marker.setMap(null);
                });
                vm.property.campus = ''; 
                vm.form.stepOne.progress = 33;
            } else {
                vm.form.stepOne.progress = 15;
                initMap();
            };
            return showCampus; 
        }



        vm.updateCampus = function (campus) {
            vm.selectedCampus = campus;
            vm.campusProperties.forEach(function (marker) {
                marker.setMap(null);
            });
            if (vm.campusSearch.length > 2) {
                vm.property.campus = vm.campusSearch;
                vm.form.stepOne.progress = 33;
            }
            if (campus) {
                vm.property.campus = campus.campusName
                vm.form.stepOne.progress = 33;
            }
            if (campus && campus.campusCoordinates) {
                map.setZoom(15);
                map.setCenter(campus.campusCoordinates);
                mapCampusProperties(campus); 
            }
        }

        //end question one 

        //question two 
        vm.moveToQuestionThree = function(){
            vm.currentQuestion = 'app/properties/addProperty/templates/questionThree.html';
            $timeout(function () {
                initAutoComplete();
                if (!map) {
                    initMap();
                }
            }, 500);
         
        }
        //end question two 


        //question three
        //end question three


        //question four
        vm.showPropertyName = function () {
            vm.showName = true;
            vm.form.stepOne.progress = 100;
        }; 
        //question four

        //question five
        vm.moveToStepTwo = function () {
            vm.currentQuestion = 'app/properties/addProperty/templates/questionFive.html';
            vm.form.stepOne = { text: 'property-add-text-inactive', background: 'property-add-step-inactive', progress: 100 }; 
            vm.form.stepTwo = { text: 'property-add-text-active', background: 'property-add-step-active', progress: 0 };
            vm.form.stepThree = { text: 'property-add-text-inactive', background: 'property-add-step-inactive', progress: 0 };
            vm.form.stepOne.edit = true;
            vm.form.currentStep = 2;
        }
        vm.setOwnerShip = function () {
            vm.form.stepTwo.progress = 50;
            if (vm.property.ownershipStructure==='I Don\'t Know') {
                vm.property.owners = []; 
            }
        }; 
        //end question five

        //question six
        vm.createSecondaryUses = function (type) {
            vm.secondaryUseTypes = addPropertyUtilsService.getSecondaryTyes(vm.propertyTypes, type.typeId);
            vm.property.uses.push({ typeId: type.typeId, typeName: type.typeName });
            vm.form.stepTwo.progress = 75;
        };

        vm.addSecondaryUses = function (type) {
            var index = vm.property.uses.indexOf(type);
            if (index === -1) {
                vm.property.uses.push(type);
            } else {
                vm.property.uses.splice(index, 1);
            }
        }

        vm.checkSecondaryUses = function (type) {
            return vm.property.uses.indexOf(type) > -1
        };

        vm.clearPropertyTypes = function () {
            vm.form.stepTwo.progress = 50;
            vm.property.primaryTypeName = null;
            vm.property.primaryType = null;
            vm.property.uses=[]
        }
        //question six

        //question seven

        vm.addOwner = function (owner) {
            if (owner) {
                vm.property.owners.push(owner);
            }

            vm.ownerSearch = '';

            //remove company to avoid dupes
            if (owner) {
                vm.companies = vm.companies.filter(function (currentOwner) {
                    return currentOwner && (currentOwner.companyId !== owner.companyId);
                });
            }
            vm.form.stepTwo.progress = 100;
        }

        vm.removeOwner = function (owner) {
            vm.property.owners = vm.property.owners.filter(function (currentOwner) {
                return currentOwner.companyId !== owner.companyId;
            });
            //add company back to the list 
            vm.companies.push(owner);
            vm.form.stepTwo.progress = 85;
        };
 

        vm.addLandOwner = function (owner) {

            if (owner) {
                owner.type = 'land';
                owner.companyName = owner.companyName +  ' (Land)'
                vm.property.owners.push(owner);
            }

            vm.landOwnerSearch = '';

            //remove company to avoid dupes
            if (owner) {
                vm.companies = vm.companies.filter(function (currentOwner) {
                    return currentOwner && (currentOwner.companyId !== owner.companyId);
                });
            }
            vm.form.stepTwo.progress = 80; 

        }

        vm.addImprovementOwner = function (owner) {
            //improvementsOwners
            if (owner) {
                owner.type = 'improvement';
                vm.property.owners.push(owner);
            }

            vm.improvementOwnerSearch = '';

            //remove company to avoid dupes
            if (owner) {
                vm.companies = vm.companies.filter(function (currentOwner) {
                    return currentOwner && (currentOwner.companyId !== owner.companyId);
                });
            }
            vm.form.stepTwo.progress = 90;
        }


        vm.moveToStepThree = function () {
            vm.currentQuestion = 'app/properties/addProperty/templates/questionEleven.html';
            vm.form.stepOne = { text: 'property-add-text-inactive', background: 'property-add-step-inactive', progress: 100 };
            vm.form.stepTwo = { text: 'property-add-text-inactive', background: 'property-add-step-inactive', progress: 100 };
            vm.form.stepThree = { text: 'property-add-text-active', background: 'property-add-step-active', progress: 0 };
            vm.form.stepOne.edit = true;
            vm.form.stepTwo.edit = true;
            vm.form.stepThree.edit = true;
            vm.form.currentStep = 3;
        }; 

        //question seven
        


        //question eight
        vm.answerCondoQuestion = function() {
            vm.form.stepTwo.progress = 90;
            if (vm.property.hasCondos === 'Yes') {
                vm.property.condos = [
                    { condoId: 0, condoName: 'Condo 1', condoUseTypes: angular.copy(vm.propertyTypes), uses:[],owners:[]},
                    { condoId: 1, condoName: 'Condo 2', condoUseTypes: angular.copy(vm.propertyTypes), uses: [], owners: [] }
                ]
                vm.selectedCondo =  vm.property.condos[0]; 

            } else {
                vm.property.condos = [];
            }

        }
      
        vm.addCondoUses = function (type) {
            if (type.isChecked !== true) {
                type.isChecked = true; 
            } else {
                type.isChecked = false;
            }
        }

     
        //question eight
       
        //question nine
        vm.addCondoOwner = function (company, selectedCondo) {
            selectedCondo.owners.push(company);
            vm.ownerSearch = '';
            selectedCondo.owners = selectedCondo.owners.filter(function (owner) {
                return owner
            })
        }; 


        vm.removeCondoOwner = function (owner) {
            vm.selectedCondo.owners = vm.selectedCondo.owners.filter(function (currentOwner) {
                return currentOwner.companyId !== owner.companyId;
            });
            //add company back to the list 
            vm.companies.push(owner);
        };

        vm.createSecondaryCondoUses = function (type, selectedCondo) {
            console.log(selectedCondo)
            vm.secondaryUseTypes = addPropertyUtilsService.getSecondaryTyes(vm.propertyTypes, type.typeId);
            vm.selectedCondo.uses.push(type);
            vm.showCondoPrimaryUses = false
            vm.showCondoSecondaryUses = true;
            vm.form.stepTwo.progress = 90;
        };

        vm.clearCondoUseTypes = function () {
            vm.form.stepTwo.progress = 70;
            vm.showCondoPrimaryUses = true;
            vm.showCondoSecondaryUses = false;
            vm.selectedCondo.uses = [];
        }

        vm.addSecondaryCondoUse = function (type, selectedCondo) {
            var index = vm.selectedCondo.uses.indexOf(type);
            if (index === -1) {
                vm.selectedCondo.uses.push(type);
            } else {
                vm.selectedCondo.uses.splice(index, 1);
            }
        }; 

        vm.checkSecondaryCondoUses = function (type) {
            return vm.selectedCondo.uses.indexOf(type) > -1;
        };

        vm.nextCondo = function () {
            var condoIndex = vm.property.condos.indexOf(vm.selectedCondo);
            vm.selectedCondo = vm.property.condos[condoIndex + 1];
            vm.ownerSearch = '';
            vm.editCondoName = false; 
            // vm.clearCondoUseTypes();
        };

        vm.previousCondo = function () {
            var condoIndex = vm.property.condos.indexOf(vm.selectedCondo);
            vm.selectedCondo = vm.property.condos[condoIndex - 1];
            vm.editCondoName = false;
            vm.ownerSearch = '';
        }

        vm.isLastCondo = function () {
            var isLast = false; 
            if (vm.property.condos.length - (vm.property.condos.indexOf(vm.selectedCondo) + 1) ===0) {
                isLast = true; 
            }
            return isLast; 
        };


        vm.isFirstCondo = function () {
            var isFirst = false;
            if (vm.property.condos.length - vm.property.condos.indexOf(vm.selectedCondo) === vm.property.condos.length) {
                isFirst = true;
            }
            return isFirst;
        }

        vm.addCondo = function () {
            var condoNumber = parseInt(vm.property.condos.length) + 1;
            vm.property.condos.push({ condoId: vm.property.condos.length, condoName: 'Condo ' + condoNumber, condoUseTypes: angular.copy(vm.propertyTypes), uses: [], owners: [] });
            vm.selectedCondo = vm.property.condos[(vm.property.condos.indexOf(vm.selectedCondo) + 1)];
        }

        //question nine 

        //question eleven
   

        vm.setUpMerge = function () {
            vm.form.stepThree.progress = 60;
            $timeout(function () {
                vm.type = 'M';
                initMergeAutoComplete();
                if (!map) {
                    initMap();
                }
            }, 500);
        }

     
        vm.getMergeLocations = function () {
            return vm.property.additionalLocations.filter(function(l) {
                return l.type === 'M';
            });

        }

        //question twelve
        vm.setUpSubdivide = function () {
            vm.form.stepThree.progress = 80;
            vm.currentQuestion = 'app/properties/addProperty/templates/questionTwelve.html';
            $timeout(function () {
                vm.type = 'S';
                initSubdivideAutoComplete();
                if (!map) {
                    initMap();
                }
            }, 500);
        }

        vm.getSubdivivisionLocations = function () {
            return vm.property.additionalLocations.filter(function (l) {
                return l.type === 'S';
            });

        }

        //form functions
        vm.saveStepOne = function () {
            var property = addPropertyUtilsService.transformModel(vm.property);
            console.log(vm.property);
            propertiesService.addProperty(property)
               .then(function (property) {
                   log('The property was successfully added.');
                   //  $location.path('/properties/' + property.mentionId + '/summary');
                   $location.path('/properties');
               });
            vm.form.saved = true; 
        }
        
        //end form functions

        vm.editStepOne = function () {
            vm.currentQuestion = 'app/properties/addProperty/templates/questionOne.html';
            vm.form.stepOne = { text: 'property-add-text-active', background: 'property-add-step-active',progress:0};
            vm.form.stepTwo ={ text: 'property-add-text-inactive', background: 'property-add-step-inactive', progress: 0 };
            vm.form.stepThree = { text: 'property-add-text-inactive', background: 'property-add-step-inactive', progress: 0 };
            vm.form.stepTwo.edit = true;
            vm.form.stepOne.progress = 100;
            vm.form.currentStep = 1;
        }

        vm.cancel = function() {
            $window.location.reload();
        }


        //map functions
        vm.showProperties = false;
        vm.mapMarkers = [];
        vm.showESXProperties = function() {
            if (!vm.showProperties === true) {
                vm.queryObject = {
                    filters: [],
                    coreObjectType: coreType.property
                };
                searchService.searchMapItems(vm.queryObject)
                    .then(function(results) {

                        //TODO add this to a service
                        results.forEach(function(item) {
                            var latLng = new google.maps.LatLng(item.latitude, item.longitude);
                            var marker = new google.maps.Marker({
                                'position': latLng,
                                map: map,
                                icon: {
                                    path: google.maps.SymbolPath.CIRCLE,
                                    scale: 4,
                                    strokeWeight: 2,
                                    strokeColor: '#B40404',
                                    fillColor: '#B40404',
                                    fillOpacity: 1
                                }
                            });
                            var infowindow = new google.maps.InfoWindow({
                                content: '<div>' +
                                    item.name +
                                    '</div>' +
                                    '<div><a href="#/properties/' +
                                    item.mentionId +
                                    '/summary">More Info</a></div>'
                            });
                            marker.addListener('click',
                                function() {
                                    infowindow.open(marker.get('map'), marker);
                                });
                            vm.mapMarkers.push(marker);
                        });
                    });
            } else {
                vm.mapMarkers.forEach(function(marker) {
                    marker.setMap(null);
                });
                vm.mapMarkers = [];
            };
        }

        vm.removeMergeLocation = function(location) {
            vm.property.additionalLocations = vm.property.additionalLocations.filter(function (l) {
                return l.locationId !== location.locationId;
            });

            propertiesList.forEach(function (marker) {
                if (marker.markerId === location.locationId) {
                    marker.setMap(null);
                }
            });
    
            propertiesList = propertiesList.filter(function (marker) {
                return marker.markerId !== location.locationId
            });

            newExtent = new google.maps.LatLngBounds();
            var newExtent = propertiesList.reduce(function (extent, marker) {
                return newExtent.extend(marker.getPosition());
            }, new google.maps.LatLngBounds());


            map.fitBounds(newExtent);
        }

        function addMergeLocation(location, type) {
            vm.property.mergeAddress = '';
            vm.property.subdivideAddress = '';
            vm.form.stepThree.progress = 100;

            var lableNumber = propertiesList.filter(function (marker) {
                return marker.label !== 'P';
            }).length + 1; 

            //create marker
            var lable = vm.type + lableNumber.toString();
            var marker = new google.maps.Marker({
                position: { lng: location.lng, lat: location.lat },
                map: map,
                label: lable,
                draggable: true,
                title: location.formattedAddress.formatted_address,
                markerId: location.locationId
            });

            propertiesList.push(marker);
            vm.property.additionalLocations.push({ locationId: location.locationId, locationName: location.formattedAddress.formatted_address,type:vm.type });

            newExtent = new google.maps.LatLngBounds();
            var newExtent = propertiesList.reduce(function (extent, marker) {
                return newExtent.extend(marker.getPosition());
            }, new google.maps.LatLngBounds());

            map.fitBounds(newExtent);
            $scope.$apply();
        }

        function addPropertyLocation(location, type) {

            //remove old location
    
            propertiesList.forEach(function (marker) {
                if (marker.markerId !== location.locationId) {
                    marker.setMap(null);
                }
            });

            propertiesList = propertiesList.filter(function (marker) {
                return marker.markerId === location.locationId
            });
          

            //create marker
            var marker = new google.maps.Marker({
                position: { lng: location.lng, lat: location.lat },
                map: map,
                label: type,
                draggable: true,
                title: location.formattedAddress.formatted_address,
                markerId: location.locationId
            });

            //set zoom and center
            map.setZoom(14);
            map.setCenter(new google.maps.LatLng(location.lat, location.lng));


            //format property object , add listener if marker moves
            vm.property.propertyAddress = location.formattedAddress.formatted_address;
            vm.property.location = location;

            google.maps.event.addListener(marker, 'dragend', function (evt) {
                vm.property.location.lat = evt.latLng.lat();
                vm.property.location.lng = evt.latLng.lng();
            });

            //add property list (to get extent later)  
            propertiesList.push(marker);
            vm.mapMessage = '(Move Map Pin if Locaton is not Correct)';
            vm.form.stepOne.progress = 90;
            //build property name 
            vm.property.propertyName = vm.property.location.formattedAddress.street_number + ' ' + vm.property.location.formattedAddress.route;
            //hate to do this....
            $scope.$apply(); 
        }

        vm.getAddress = function () {
            vm.addressFetched = true;
            var place = autocomplete.getPlace();
            var addressComponents = addPropertyUtilsService.formatAddress(place);
            var addressComponentsLong = addPropertyUtilsService.formatAddressLongNames(place);
            addPropertyLocation({ locationId: place.place_id, locationName: addressComponents.formatted_address, formattedAddress: addressComponents, formattedAddressLong: addressComponentsLong,   lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }, 'P');
        }

        vm.getMergeAddress = function () {
            var place = mergeAutocomplete.getPlace();
            var addressComponents = addPropertyUtilsService.formatAddress(place);
            var addressComponentsLong = addPropertyUtilsService.formatAddressLongNames(place);
            var type = 'M'; 
            addMergeLocation({ locationId: place.place_id, locationName: addressComponents.formatted_address, formattedAddress: addressComponents, formattedAddressLong: addressComponentsLong, lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }, type);
        }

        vm.getSubdivideAddress = function () {
            var place = subdivideAutoComplete.getPlace();
            var addressComponents = addPropertyUtilsService.formatAddress(place);
            var addressComponentsLong = addPropertyUtilsService.formatAddressLongNames(place);
            var type = 'S';

            addMergeLocation({ locationId: place.place_id, locationName: addressComponents.formatted_address, formattedAddress: addressComponents, formattedAddressLong: addressComponentsLong, lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }, type);
        }

        function initAutoComplete(){
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));
            autocomplete.addListener('place_changed', vm.getAddress);
            document.getElementById("autocomplete").placeholder = "Hint: If Google can’t find the address, try just the street and city. Or, try searching by business name. You’ll be able to fine­ tune the location once a pin is on the map";
        };

        function initMergeAutoComplete() {
            mergeAutocomplete = new google.maps.places.Autocomplete(document.getElementById('mergeautocomplete'));
            mergeAutocomplete.addListener('place_changed', vm.getMergeAddress);
        };

        function initSubdivideAutoComplete() {
            subdivideAutoComplete = new google.maps.places.Autocomplete(document.getElementById('subdivisionautocomplete'));
            subdivideAutoComplete.addListener('place_changed', vm.getSubdivideAddress);
        };



        function initMap() {
        
            vm.showMap = true; 
            vm.mapClass = 'md-whiteframe-2dp';

            var styledMap = new google.maps.StyledMapType(mapOptions.styles, { name: "Roads" });
            //todo use geolocation
            var options = {
                zoom: 4,
                center: new google.maps.LatLng(37.7773664, -99.7175739),
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.HYBRID, 'map_style']
                }
            };

            map = new google.maps.Map(document.getElementById('map'), options);
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId(google.maps.MapTypeId.HYBRID);

            google.maps.event.addListenerOnce(map, 'idle', function () {
                google.maps.event.trigger(map, 'resize');
            });

            vm.showPropertyToggle = true;
            var centerControl = document.getElementById('propertyToggle');
            centerControl.index = 1;
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControl);

        }

        //end map  functions

        //data
        vm.existingCampuses = [
            {
                campusId: 0,
                campusName: 'South Street Seaport',
                campusCoordinates: { lat: 40.707262, lng: -74.0063817 },
                campusProperties: [
                    { address: '200 Water Street', lat: 40.7072392, lng: -74.0051617 },
                    { address: '202 Water Street', lat: 40.7073929, lng: -74.0046835 }
                ]
            },
            {
                campusId: 1,
                campusName: 'Minnesota Bonanza',
                campusCoordinates: { lat: 46.9837541572969, lng: -96.4186740249285 },
                campusProperties: []
            },
            {
                campusId: 2,
                campusName: 'Brooklyn Bridge Park',
                campusCoordinates: { lat: 40.699444, lng: -73.9995927 },
                campusProperties: [
                    { address: '1 Pineapple Street', lat: 40.6993948, lng: -73.9963999 }
                ]
            },
            {
                campusId: 3,
                campusName: 'Unicorn Park',
                campusCoordinates: { lat: 42.4848102, lng: -71.1170872 },
                campusProperties: [
                    { address: '100 Unicorn Park Drive', lat: 42.4810285, lng: -71.1144067 },
                    { address: '200 Unicorn Park Drive', lat: 42.4818137, lng: -71.1151452 },
                    { address: '300 Unicorn Park Drive', lat: 42.4836532, lng: -71.1147115 },
                    { address: '400 Unicorn Park Drive', lat: 42.484869, lng: -71.1155243 },
                    { address: '500 Unicorn Park Drive', lat: 42.4879335, lng: -71.1161574 }
                ]
            }
        ];

  

        vm.propertyTypes = [
            { typeId: 0, typeName: 'Office' },
            { typeId: 1, typeName: 'Retail' },
            { typeId: 2, typeName: 'Multi-Family' },
            { typeId: 3, typeName: 'Industrial' },
            { typeId: 4, typeName: 'Development' },
            { typeId: 5, typeName: 'Hotel' },
            { typeId: 6, typeName: 'Parking' }
        ];

        vm.companies = [
            { companyId: 0, companyName: 'AvalonBay Communities' },
            { companyId: 1, companyName: 'Blackstone' },
            { companyId: 2, companyName: 'Colony American Homes' },
            { companyId: 3, companyName: 'Equity Residential' },
            { companyId: 4, companyName: 'Ventas' }
        ];

        //end data

    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'propertyLoansController';

    angular.module('app').controller(controllerId, propertyLoansController);

    propertyLoansController.$inject = ['common', 'propertiesService', 'mentionsService', '$state', '$stateParams', '_', '$timeout'];

    function propertyLoansController(common, propertiesService, mentionsService, $state, $stateParams, _, $timeout) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;

        vm.searchText = '';

        vm.loansList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        };

        vm.propertyId = common.$stateParams.propertyId;
        vm.queryObject = {
            mentionId: vm.propertyId
        };

        vm.getPropertyLoans = _.debounce(getPropertyLoans, 200);
        vm.searchPropertyLoans = _.debounce(searchPropertyLoans, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.toggleIsWatched = _.debounce(toggleIsWatched, 200);
        vm.selectRow = selectRow;

        vm.title = 'Property Loans';

        vm.loans = [];

        activate();

        function activate() {
            common.activateController([getPropertyLoans()], controllerId)
                .then(function () { log('Activated Property Loans View'); });
        }

        function searchPropertyLoans() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.loansList.resources.length = 0;
                vm.queryObject.offset = 0;
                getPropertyLoans(true);
            }
        }

        function getPropertyLoans(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
                vm.queryObject.searchText = vm.searchText;
                return propertiesService.getPropertyLoans(vm.queryObject).then(function (loansList) {
                    vm.loansList.resources = vm.loansList.resources.concat(loansList.resources);
                    vm.queryObject.isBusy = false;
                    return vm.loansList;
                });
            return null;
        }

        function sortColumn(columnName) {
            vm.loansList.resources.length = 0;
            vm.queryObject.offset = 0;

            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;

            return propertiesService.getPropertyLoans(vm.queryObject).then(function (loansList) {
                vm.loansList = loansList;
            });
        }

        function toggleIsWatched(loan) {
            loan.isWatched = !loan.isWatched;
            mentionsService.setIsWatched(loan.loanMentionId, loan.isWatched);
        }

        function selectRow(loan, event) {
            $timeout(function () {
                if (event.target.id === 'esx-watch') {
                    toggleIsWatched(loan);
                } else {
                    editLoan(loan);
                }                    
            });
        }

        function editLoan(loan) {
            // TODO: Go to property loans
        }

     
    }
})();
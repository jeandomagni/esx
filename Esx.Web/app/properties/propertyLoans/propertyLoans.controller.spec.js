﻿(function () {
    describe('propertyLoans.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            propertiesService,
            fakeProperties,
            fakeProperty,
            $scope,
            controller;

        var activateController = function () {

            controller = $controller('propertyLoansController', {
                common: common,
                propertiesService: propertiesService
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _propertiesService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            propertiesService = _propertiesService_;

            //fakes
            $scope = {};

            common.$window = { location: {} };

            fakeProperties = {
                resources: [{ name: 'some property' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            fakeProperty = {
                propertyID: 1234,
                propertyName: 'test property'
            };

            //spys
            spyOn(propertiesService, 'getPropertyLoans').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeProperties);
                    }
                };
            });

            spyOn(_, 'debounce').and.callFake(function (callback) {
                return callback;
            });

        }));

        afterEach(function () {
            common.$stateParams.propertyId = undefined;
            common.$stateParams.companyId = undefined;
        });

        describe('activate', function () {

            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define queryObject', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.loansList.pagingData).toEqual(expectedPagingData);
            });

           
        });

        describe('searchPropertyLoans', function() {
            beforeEach(function () {
                activateController();
            });

            it('should call the search method with the specified search text', function () {
                //arrange
                var expectedSearchQuery = {
                    searchText: 'text!'
                };

                controller.queryObject = expectedSearchQuery;
                controller.loansList.resources.length = 0; 

                //act
                controller.searchPropertyLoans();

                //assert
                expect(propertiesService.getPropertyLoans).toHaveBeenCalledWith(controller.queryObject);
            });
        });
    });
})();

﻿(function () {
    'use strict';

    var serviceId = 'propertiesService';

    angular.module('app').factory(serviceId, propertiesService);

    propertiesService.$inject = ['uriService', '$resource', '$http'];

    function propertiesService(uriService, $resource, $http) {

        var resource = $resource(
            uriService.property,
            { propertyId: '@id' },
            { 'update': { method: 'PATCH' } }
        );

        var service = {
            addProperty:addProperty,
            getDealsForProperty: getDealsForProperty,
            getProperties: getProperties,
            searchProperties: searchProperties,
            getPropertyCompanies: getPropertyCompanies,            
            getPropertyLoans: getPropertyLoans,
            propertyIntellisense: propertyIntellisense
        };

        return service;

        function addProperty(property) {
            return $resource(uriService.propertyAdd).save(property).$promise;
        }

        function getPropertyLoans(query) {
            return $resource(uriService.propertyLoans).get(query).$promise;
        }

        function getDealsForProperty(query) {
            return $resource(uriService.propertyDeals).get(query).$promise;
        }

        function getProperties(query) {
            return $resource(uriService.propertyList).get(query).$promise;
        }

        function searchProperties(query) {
            return $resource(uriService.propertySearch, { query: '@query' }).query({ query: query }).$promise;
        }

        function getPropertyCompanies(id) {
            return $resource(uriService.propertyCompanies, { propertyId: '@id' }).get({ propertyId: id }).$promise;
        }

        function propertyIntellisense(query) {
            return $resource(uriService.propertyIntellisense, { query: '@query' }).query({ query: query }).$promise;
        }
    }
})();
﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.config(routeConfigurator);

    routeConfigurator.$inject = ['$stateProvider'];

    function routeConfigurator($stateProvider) {

        $stateProvider
            .state('properties', {
                url: '/properties',
                templateUrl: 'app/properties/properties.html',
                controller: 'propertiesController',
                controllerAs: 'vm',
                data: {
                    title: 'Properties',
                    defaultSearch: 'property',
                    sidebaricon: 'properties',
                    includeInSideBar: true,
                    subnavClass: 'properties-bg'
                }
            }).state('property', {
                    abstract: true,
                    url: '/properties/:propertyId',
                    templateUrl: 'app/properties/property.html',
                    controller: 'propertyController',
                    controllerAs: 'vm',
                    data: {
                        title: '',
                        subnavClass: 'properties-bg',
                        subtabs: [
                            { sref: 'property.propertySummary', label: 'SUMMARY' },
                            { sref: 'property.propertyDetails', label: 'DETAILS' },
                            { sref: 'property.propertyDeals', label: 'DEALS' },
                            { sref: 'property.propertyLoans', label: 'LOANS' }
                        ]
                    }
                }).state('property.propertySummary', {
                    url: '/summary',
                    data: {
                        title: '',
                        sidebaricon: 'properties'
                    },
                    views: {
                        '': {
                            templateUrl: 'app/properties/propertySummary/propertySummary.html',
                            controller: 'propertySummaryController',
                            controllerAs: 'vm'
                        },
                        'propertyInformation@property.propertySummary': {
                            templateUrl: 'app/properties/propertySummary/propertySummaryInformation.html',
                            controller: 'propertySummaryInformationController',
                            controllerAs: 'vm'
                        },
                        'propertyAlerts@property.propertySummary': {
                            templateUrl: 'app/properties/propertySummary/propertySummaryAlerts.html',
                            controller: 'propertySummaryAlertsController',
                            controllerAs: 'vm'
                        },
                        'propertyComments@property.propertySummary': {
                            templateUrl: 'app/properties/propertySummary/propertySummaryComments.html',
                            controller: 'propertySummaryCommentsController',
                            controllerAs: 'vm'
                        },
                        'propertyHeader@property.propertySummary': {
                            templateUrl: 'app/properties/propertySummary/propertySummaryHeader.html',
                            controller: 'propertySummaryHeaderController',
                            controllerAs: 'vm'
                        }
                    }
                }).state('property.propertyDeals', {
                    url: '/deals',
                    data: {
                        title: '',
                        sidebaricon: 'properties'
                    },
                    views: {
                        '': {
                            templateUrl: 'app/properties/propertyDeals/propertyDeals.html',
                            controller: 'propertyDealsController',
                            controllerAs: 'vm'
                        }
                    }
                }).state('property.propertyLoans', {
                    url: '/loans',
                    data: {
                        title: '',
                        sidebaricon: 'properties'
                    },
                    views: {
                        '': {
                            templateUrl: 'app/properties/propertyLoans/propertyLoans.html',
                            controller: 'propertyLoansController',
                            controllerAs: 'vm'
                        }
                    }
                }).state('property.propertyDetails', {
                    url: '/details',
                    data: {
                        title: '',
                        sidebaricon: 'properties'
                    },
                    views: {
                        '': {
                            templateUrl: 'app/properties/propertyDetails/propertyDetails.html',
                            controller: 'propertyDetailsController',
                            controllerAs: 'vm'
                        },
                        'propertyHeader@property.propertyDetails': {
                            templateUrl: 'app/properties/propertySummary/propertySummaryHeader.html',
                            controller: 'propertySummaryHeaderController',
                            controllerAs: 'vm'
                        }
                    }
                });
        }
})();
﻿describe('propertySummary.service', function () {

    beforeEach(module('app'));

    var propertySummaryService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_propertySummaryService_, _uriService_, _$httpBackend_) {

        propertySummaryService = _propertySummaryService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        //uriService.company = 'api/companies/:companyId';

        // ignore html templates. Not sure if this is a good approach. 
        $httpBackend.whenGET(/.*.html/).respond(200, '');
        $httpBackend.whenPOST(/.*.html/).respond(201, '');
        $httpBackend.whenPATCH(/.*.html/).respond(204, '');

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getDetails', function () {

        it('should call server with property details uri', function () {

            //Arrange  
            var mentionId = '@mention';
            $httpBackend.expectGET('api/properties/' + mentionId + '/details', { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            propertySummaryService.getDetails(mentionId);

            //Assert
            $httpBackend.flush();
        });
    });

    describe('getValuationHistory', function () {

        it('should call server with property valuations uri', function () {

            //Arrange
            var query = { propertyId: 123 };
            $httpBackend.expectGET('api/properties/valuations?propertyId=' + query.propertyId, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            propertySummaryService.getValuationHistory(query);

            //Assert
            $httpBackend.flush();
        });
    });
});
﻿(function () {
    'use strict';

    var controllerId = 'propertySummaryInformationController';

    angular.module('app').controller(controllerId, propertySummaryInformationController);

    propertySummaryInformationController.$inject = ['common', '$stateParams', 'propertySummaryService', 'events'];

    function propertySummaryInformationController(common, $stateParams, propertySummaryService, events) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        activate();

        function activate() {
            common.activateController([getPropertyDetails()], controllerId)
                .then(function () { common.$broadcast(events.refreshMap); });
        }

        function getPropertyDetails() {
            propertySummaryService.getDetails($stateParams.propertyId).then(function(response) {
                vm.propertyDetails = response;
            });
        }
    }
})();
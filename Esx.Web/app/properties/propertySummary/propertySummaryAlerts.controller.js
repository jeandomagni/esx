﻿(function () {
    'use strict';

    var controllerId = 'propertySummaryAlertsController';

    angular.module('app').controller(controllerId, propertySummaryAlertsController);

    propertySummaryAlertsController.$inject = ['common', '$stateParams', '_', '$location', 'alertsService'];

    function propertySummaryAlertsController(common, $stateParams, _, $location, alertsService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.queryObject = {
            mentionId: $stateParams.propertyId
        };
        vm.getAlerts = _.debounce(getAlerts, 200);
        vm.acknowledgeAlert = _.debounce(acknowledgeAlert, 200, { leading: true, trailing: false });
        vm.removeAlert = _.debounce(removeAlert, 200, { leading: true, trailing: false });
        vm.snoozeAlert = _.debounce(snoozeAlert, 200, { leading: true, trailing: false });
        vm.goToEntity = _.debounce(goToEntity, 200, { leading: true, trailing: false });
        vm.alertsList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        };
        vm.styleMap = {
            // Maps TableCode to style class.
            'A': 'alias-notification', // orange
            'E': 'employee-notification', // yellow
            'C': 'contact-notification', // yellow
            'D': 'deal-notification', // green
            'O': 'office-notification', // orange
            'P': 'property-notification', // blue
            'L': 'loan-notification', // teal
            'M': 'market-notification'
        };

        activate();

        function activate() {
            common.activateController([getAlerts()], controllerId)
                .then(function () { });
        }

        function getAlerts() {
            return alertsService.getAlerts(vm.queryObject).then(function (response) {
                vm.alertsList = response;
            });
        }

        function acknowledgeAlert(alert) {
            return alertsService.acknowledgeAlert(alert.alertId).then(function () {
                vm.alertsList.resources.splice(vm.alertsList.resources.indexOf(alert), 1);
            });
        }

        function removeAlert(alert) {
            return alertsService.removeAlert(alert.alertId).then(function () {
                vm.alertsList.resources.splice(vm.alertsList.resources.indexOf(alert), 1);
            });
        }

        function snoozeAlert(alert) {
            return alertsService.snoozeAlert(alert.alertId).then(function () {
                vm.alertsList.resources.splice(vm.alertsList.resources.indexOf(alert), 1);
            });
        }

        function goToEntity(alert) {
            var pathTemplate =
                      alert.entityCode === 'A' ? '/companies/{id}/summary'
                    : alert.entityCode === 'E' ? '/contacts/{id}'
                    : alert.entityCode === 'C' ? '/contacts/{id}'
                    : alert.entityCode === 'D' ? '/deals/{id}'
                    : alert.entityCode === 'P' ? '/properties/{id}/summary'
                    : alert.entityCode === 'L' ? '/loans/{id}'
                    : alert.entityCode === 'M' ? '/markets/{id}'
                    : alert.entityCode === '??' ? '/pitches/{id}'
                    : alert.entityCode === 'T' ? '/targets/{id}'
                    : "";

            if (pathTemplate === "") throw 'Alert entity code not supported';

            var path = pathTemplate.replace('{id}', alert.entityMentionId);
            $location.path(path);
        }
    }
})();
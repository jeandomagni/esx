﻿(function () {
    'use strict';

    var controllerId = 'propertySummaryHeaderController';

    angular.module('app').controller(controllerId, propertySummaryHeaderController);

    propertySummaryHeaderController.$inject = ['$scope', 'common', '$stateParams', 'uiGmapGoogleMapApi', 'propertySummaryService', '$window', '$timeout', 'mapOptions'];

    function propertySummaryHeaderController($scope, common, $stateParams, uiGmapGoogleMapApi, propertySummaryService, $window, $timeout, mapOptions) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.queryObject = {
            propertyId: $stateParams.propertyId
        }
        vm.map = { center: { latitude: 0, longitude: 0 }, showMarker: false, zoom: 3, control: {} };
        vm.marker = {};
        vm.mapLocation = mapLocation;
        vm.mapExpanded = false;
        vm.address = {};
        vm.toggleMapSize = toggleMapSize;

        activate();

        function activate() {
            common.activateController([getPropertyDetails()], controllerId).then(function () { });          
            vm.mapOptions = mapOptions;
        }

        function getPropertyDetails() {
            propertySummaryService.getDetails(vm.queryObject.propertyId).then(function(result) {
                vm.property = result;
                if (vm.property.physicalAddress) {
                    mapLocation(vm.property.physicalAddress);
                }
            });
        }

        function mapLocation(location) {
            vm.address.address1 = location.address1;
            vm.address.city = location.city;
            vm.address.state = location.state;

            uiGmapGoogleMapApi.then(function (maps) {
                var address = '';
                if (location.address1) {
                    address = location.address1 + ',';
                }
                address += location.city + ',' + location.state;
                if (address.length === 1)
                    return;
                maps.Geocoder.prototype.geocode({ 'address': address }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        vm.map.center = {
                            latitude: results[0].geometry.location.lat(),
                            longitude: results[0].geometry.location.lng()
                        };
                        vm.marker.center = angular.copy(vm.map.center);
                        vm.map.zoom = 14;
                        vm.map.showMarker = true;
                        $scope.$apply();
                    } else {
                        console.log('Geocode was not successful for the following reason: ' + status);
                    }
                });
                vm.map.control.refresh();
            });
        }

        function toggleMapSize() {
            vm.mapExpanded = !vm.mapExpanded;
            $timeout(function () {
                var resizeUIEvent = $window.document.createEvent('UIEvent');
                resizeUIEvent.initUIEvent('resize', true, false, $window, 0);
                $window.dispatchEvent(resizeUIEvent);
                $scope.$apply();
            }, 100);
        }
    }
})();
﻿describe('propertySummaryHeader.controller', function () {
    beforeEach(module('app'));

    var $controller,
        common,
        controller,
        $scope,
        propertySummaryService,
        fakeStats;

    var activateController = function (propertyId) {
        common.$stateParams.propertyId = propertyId;

        controller = $controller('propertySummaryHeaderController', {
            common: common,
            propertySummaryService: propertySummaryService,
            $scope: $scope
        });
    };

    beforeEach(inject(function (_$controller_, _common_, _propertySummaryService_) {
        //inject
        $controller = _$controller_;
        common = _common_;
        propertySummaryService = _propertySummaryService_;

        //fakes
        $scope = { $watch: function () { } };

        var fake = {
            physicalAddress: {
                address1: '',
                city: '',
                state: ''
            }
        }
        //spys
        spyOn(propertySummaryService, 'getDetails').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback(fake);
                }
            };
        });
    }));

    describe('activate', function () {
        var propertyId = 123;

        beforeEach(function () {
            activateController(propertyId);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });
    });

});
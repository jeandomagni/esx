﻿(function () {
    'use strict';

    var serviceId = 'propertySummaryService';

    angular.module('app').factory(serviceId, propertySummaryService);

    propertySummaryService.$inject = ['uriService', '$resource'];

    function propertySummaryService(uriService, $resource) {

        var service = {
            getDetails: getDetails,
            getValuationHistory: getValuationHistory
        };

        return service;

        function getDetails(mentionId) {
            return $resource(uriService.propertyDetails).get({ mentionId: mentionId }).$promise;
        }

        function getValuationHistory(query) {
            return $resource(uriService.propertyValuations).get(query).$promise;
        }
    }
})();
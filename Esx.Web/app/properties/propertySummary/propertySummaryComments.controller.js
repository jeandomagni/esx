﻿(function () {
    'use strict';

    var controllerId = 'propertySummaryCommentsController';

    angular.module('app').controller(controllerId, propertySummaryCommentsController);

    propertySummaryCommentsController.$inject = ['common', 'propertiesService', '$mdUtil', 'commentsService'];

    function propertySummaryCommentsController(common, propertiesService, $mdUtil, commentsService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        vm.propertyMentionId = common.$stateParams.propertyId;

        vm.commentsList = {
            resources:[],
            pagingData: {
                pageSize: 10,
                offset: 0
            }
        };
        vm.commentsQueryObject = {
            mentionId: vm.propertyMentionId,
            filterText: ''
        };
        vm.getMentionedComments = getMentionedComments;
        vm.searchComments = $mdUtil.debounce(searchComments, 200);
        vm.commentAdded = commentAdded;

        activate();

        function activate() {
            common.activateController([getMentionedComments()], controllerId)
                .then(function () { });
        }

        function getMentionedComments(clearList) {
            return commentsService.getMentionedComments(vm.commentsQueryObject).then(function (comments) {
                if (clearList) {
                    vm.commentsList.resources = comments.resources;
                } else {
                    vm.commentsList.resources = vm.commentsList.resources.concat(comments.resources);
                }
                vm.commentsList.pagingData = comments.pagingData;
                return vm.commentsList;
            });
        }

        function searchComments() {
            if (vm.commentsQueryObject.filterText.length === 0 || vm.commentsQueryObject.filterText.length > 2) {
                vm.commentsQueryObject.offset = 0;
                getMentionedComments(true);
            }
        }

        function commentAdded() {
            vm.commentsQueryObject.offset = 0;
            getMentionedComments(true);
        }
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'propertySummaryController';

    angular.module('app').controller(controllerId, propertySummaryController);

    propertySummaryController.$inject = ['common'];

    function propertySummaryController(common) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        activate();

        function activate() {
            common.activateController([], controllerId)
                .then(function () {  });
        }
    }
})();
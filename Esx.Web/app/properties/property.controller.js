﻿(function () {
    'use strict';
    var controllerId = 'propertyController';

    angular.module('app').controller(controllerId, propertyController);

    propertyController.$inject = ['common', '$stateParams', 'propertySummaryService'];

    function propertyController(common, $stateParams, propertySummaryService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        activate();

        function activate() {
            common.activateController([setPropertyName()], controllerId).then(function () { log('Activated Property View.'); });
        }

        function setPropertyName() {
            propertySummaryService.getDetails($stateParams.propertyId).then(function (response) {
                common.setPageHeading(response.propertyName, $stateParams.propertyId);
            });
        }
    }
})();
﻿(function () {
    'use strict';

    var controllerId = 'propertiesController';

    angular.module('app').controller(controllerId, propertiesController);

    propertiesController.$inject = ['common', 'propertiesService', 'mentionsService', '$timeout', '$location'];

    function propertiesController(common, propertiesService, mentionsService, $timeout, $location) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        vm.searchResults = {};
        vm.searchText = '';

        vm.queryObject = {
            offset: 0,
            pageSize: 30
        };
        vm.propertiesList = {
            pagingData: {
                pageSize: 30,
                offset: 0
            },
            resources: []
        };
       
        vm.searchProperties = _.debounce(searchProperties, 400);
        vm.getProperties = _.debounce(getProperties, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.selectRow = _.debounce(selectRow, 200);
        vm.toggleIsWatched = _.debounce(toggleIsWatched, 200);
        vm.selectedPropertyChange =  _.debounce(selectedPropertyChange, 200);

        activate();

        function activate() {
            common.activateController([getProperties()], controllerId)
                .then(function () { log('Activated Properties View'); });
        }

        function getProperties(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            return propertiesService.getProperties(vm.queryObject).then(function (propertiesList) {
                vm.propertiesList.resources = vm.propertiesList.resources.concat(propertiesList.resources);
                vm.queryObject.isBusy = false;
                return vm.propertiesList;
            });
        }

        function searchProperties() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.queryObject.offset = 0;
                vm.propertiesList.resources.length = 0;
                getProperties(true);
            }
            return null;
        }

        function sortColumn(columnName) {
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;
            vm.propertiesList.resources.length = 0;
            getProperties();
        }

        function selectedPropertyChange(property) {
            if (property && property.propertyID) {
                common.$window.location.href = '#/properties/' + property.propertyID;
            }
        }

        function selectRow(property) {
            $timeout(function () {
                if (vm.rowAction === 'toggleWatch')
                    toggleIsWatched(property);
                else
                    editProperty(property);
            });
        }

        function editProperty(property) {
            $location.path('/properties/' + property.mentionId + '/summary');
        }

        function toggleIsWatched(property) {
            property.isWatched = !property.isWatched;
            mentionsService.setIsWatched(property.mentionId, property.isWatched);
        }
    }
})();
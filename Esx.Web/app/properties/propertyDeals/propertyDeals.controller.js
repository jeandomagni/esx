﻿(function () {
    'use strict';

    var controllerId = 'propertyDealsController';

    angular.module('app').controller(controllerId, propertyDealsController);

    propertyDealsController.$inject = ['common', 'propertiesService', 'mentionsService', '$state', '$stateParams', '_', '$timeout'];

    function propertyDealsController(common, propertiesService, mentionsService, $state, $stateParams, _, $timeout) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;

        vm.searchText = '';

        var pagingData = {
            pageSize: 30,
            offset: 0
        }

        vm.dealsList = {
            pagingData: pagingData,
            resources: []
        };

        vm.propertyId = common.$stateParams.propertyId;

        vm.queryObject = {
            mentionId: vm.propertyId,
            offset: 0
        };

        vm.getPropertyDeals = _.debounce(getPropertyDeals, 200);
        vm.searchPropertyDeals = _.debounce(searchPropertyDeals, 200);
        vm.sortColumn = _.debounce(sortColumn, 200);
        vm.toggleIsWatched = _.debounce(toggleIsWatched, 200);
        vm.selectRow = selectRow;

        vm.title = 'Property Deals';

        vm.deals = [];

        activate();

        function activate() {
            common.activateController([getPropertyDeals()], controllerId)
                .then(function () { log('Activated Property Deals View'); });
        }

        function searchPropertyDeals() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.dealsList.resources.length = 0;
                vm.queryObject.offset = 0;
                getPropertyDeals(true);
            }
        }

        function getPropertyDeals(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                return propertiesService.getDealsForProperty(vm.queryObject).then(function (dealsList) {
                    vm.dealsList.resources = vm.dealsList.resources.concat(dealsList.resources);
                    vm.queryObject.isBusy = false;
                    return vm.dealsList;
                });
            }
            return null;
        }

        function sortColumn(columnName) {
            vm.dealsList.resources.length = 0;
            vm.queryObject.offset = 0;
            vm.queryObject.sortDescending = vm.queryObject.sort === columnName ? !vm.queryObject.sortDescending : false;
            vm.queryObject.sort = columnName;

            return propertiesService.getDealsForProperty(vm.queryObject).then(function (dealsList) {
                vm.dealsList = dealsList;
            });
        }

        function selectRow(deal, event) {

            $timeout(function () {
                if (event.target.id==='esx-watch')
                    toggleIsWatched(deal);
                else
                    editDeal(deal);
            });
        }

        function editDeal(deal) {
            // TODO: Go to deal details.
        }

        function toggleIsWatched(deal) {
            deal.isWatched = !deal.isWatched;
            mentionsService.setIsWatched(deal.dealMentionId, deal.isWatched);
        }
    }
})();
﻿//TODO: Cover once built
(function () {
    describe('propertyDeals.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            propertiesService,
            fakeProperties,
            fakeProperty,
            $scope,
            controller;

        var activateController = function () {

            controller = $controller('propertyDealsController', {
                common: common,
                propertiesService: propertiesService
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _propertiesService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            propertiesService = _propertiesService_;

            //fakes
            $scope = {};

            fakeProperties = {
                resources: [{ name: 'some property' }],
                pagingData: {
                    pageSize: 30,
                    offset: 0
                }
            };

            fakeProperty = {
                propertyID: 1234,
                propertyName: 'test property'
            };

            //spys
            spyOn(propertiesService, 'getDealsForProperty').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeProperties);
                    }
                };
            });

        }));

        afterEach(function () {
            common.$stateParams.propertyId = undefined;
            common.$stateParams.companyId = undefined;
        });

        describe('activate', function () {

            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define queryObject', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 30,
                    offset: 0
                };

                //assert
                expect(controller.dealsList.pagingData).toEqual(expectedPagingData);
            });
        });
    });
})();

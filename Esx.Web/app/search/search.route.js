﻿(function () {
	'use strict';

	var app = angular.module('app');

	app.config(routeConfigurator);

	routeConfigurator.$inject = ['$stateProvider','coreType'];

	function routeConfigurator($stateProvider, coreType) {

		$stateProvider
            .state('search', {
            	url: '/search',
            	templateUrl: 'app/search/search.html',
            	controller: 'searchController',
            	controllerAs: 'vm',
            	data: {
            		title: 'Search',
                    //TODO update these
            		defaultSearch: 'search',
            		sidebaricon: 'search',
            		includeInSideBar: true,
            		subnavClass: 'springboard-bg'
            	},
                params: {
                    searchInput: undefined
                }
            }).state('search.company', {
                url: '/company',
                templateUrl: 'app/search/search.html',
                controller: 'searchController',
                controllerAs: 'vm',
                data: {
                    coreObjectType: coreType.company,
                    coreObjectName: 'Companies'
                }
            }).state('search.contact', {
                url: '/contact',
                templateUrl: 'app/search/search.html',
                controller: 'searchController',
                controllerAs: 'vm',
                data: {
                    coreObjectType: coreType.contact,
                    coreObjectName: 'Contacts'
                }
            }).state('search.property', {
                url: '/property',
                templateUrl: 'app/search/search.html',
                controller: 'searchController',
                controllerAs: 'vm',
                data: {
                    coreObjectType: coreType.property,
                    coreObjectName: 'Properties'
                }
            }).state('search.deal', {
                url: '/deal',
                templateUrl: 'app/search/search.html',
                controller: 'searchController',
                controllerAs: 'vm',
                data: {
                    coreObjectType: coreType.deal,
                    coreObjectName: 'Deals'
                }
            }).state('search.loan', {
                url: '/loan',
                templateUrl: 'app/search/search.html',
                controller: 'searchController',
                controllerAs: 'vm',
                data: {
                    coreObjectType: coreType.loan,
                    coreObjectName: 'Loans'
                }
            });
	}
})();
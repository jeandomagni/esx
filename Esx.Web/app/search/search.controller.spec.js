﻿(function () {
    describe('search.controller', function () {
        beforeEach(module('app'));

        var $controller,
          common,
          searchService,
          coreType,
          controller,
          searchQueryBuilderService,
          $scope;

        var fakeSearchResults,
            fakeState,
            fakeQueryObject,
            fakeLocation;

        var activateController = function () {

            controller = $controller('searchController', {
                common: common,
                $state: fakeState,
                searchQueryBuilderService: searchQueryBuilderService,
                $location: fakeLocation,
                $scope: $scope
            });
        };


        beforeEach(inject(function (_$controller_, _common_, _searchService_, _coreType_, _searchQueryBuilderService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            searchService = _searchService_;
            coreType = _coreType_;
            searchQueryBuilderService = _searchQueryBuilderService_;

            //fakes
            common.logger = { getLogFn: angular.noop };
            fakeSearchResults = {};
            fakeQueryObject = {};
            fakeState = {};
            fakeLocation = { search: function () { return null; } };

            $scope = {
                $apply: angular.noop
            };

            //spys
            spyOn(searchService, 'search').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeSearchResults);
                    }
                };
            });

            spyOn(searchQueryBuilderService, 'buildParamQueryObject').and.callFake(function () {
                return fakeQueryObject;
            });

        }));

        afterEach(function () {

        });


        describe('activate', function () {

            beforeEach(function () {
            });

            it('should be defined', function () {
                //act
                activateController();
                //assert
                expect(controller).toBeDefined();
            });

            it('should define the query object.', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 20,
                    offset: 0,
                    filters: []
                };

                //assert
                expect(controller.queryObject).toEqual(expectedPagingData);
            });

            it('should build queryObjct from currentState', function () {
                //arrange
                fakeState = {
                    current: {
                        data: { coreObjectType: 'expected' }
                    }
                }

                //act
                activateController();
                //assert
                expect(searchQueryBuilderService.buildParamQueryObject).toHaveBeenCalledWith(fakeState.current);
            });

            it('should not call search service when no state params are present', function () {
                //act
                activateController();
                //assert
                expect(searchService.search).not.toHaveBeenCalled();
            });

            it('should call search service when coreObjectType current state data present', function () {
                //arrange
                fakeState = {
                    current: {
                        data: {
                            coreObjectType: coreType.company
                        }
                    }
                };
                fakeQueryObject = {
                    coreObjectType: coreType.company
                }

                //act
                activateController();

                //assert
                expect(searchService.search).toHaveBeenCalledWith(fakeQueryObject);
            });

        });

        describe('searchForCompanies', function () {

            beforeEach(function () {
                activateController();
            });

            it('should set the core object type to companies', function () {

                //act
                controller.searchForCompanies();

                //assert
                expect(controller.queryObject.coreObjectType).toEqual(coreType.company);

            });

            it('should execute search', function () {

                spyOn(controller, 'search');

                //act
                controller.searchForCompanies();

                //assert
                expect(controller.search).toHaveBeenCalled();

            });

            it('should clear results and set offset to 0', function () {

                controller.queryObject.offset = 40;
                controller.searchResults.resources = ['1', '2'];
                spyOn(controller, 'search');

                //act
                controller.searchForCompanies();

                //assert
                expect(controller.queryObject.offset).toEqual(0);
                expect(controller.searchResults.resources.length).toEqual(0);

            });
        });

        describe('search', function () {

            beforeEach(function () {
                activateController();
            });

            it('should call search service with queryObject', function () {

                //arrange
                var expectedQuery = {
                    coreObjectType: coreType.company
                };
                controller.queryObject = expectedQuery;

                //act
                controller.search();

                //assert
                expect(searchService.search).toHaveBeenCalledWith(expectedQuery);

            });

            it('should set search results', function () {

                //arrange
                fakeSearchResults = {
                    resources: ['some results!']
                }

                //act
                controller.search();

                //assert
                expect(controller.searchResults.resources).toEqual(fakeSearchResults.resources);

            });

            it('should set searchResultsTemplateUri for company', function () {

                //arrange
                controller.queryObject.coreObjectType = coreType.company;

                //act
                controller.search();

                //assert
                expect(controller.searchResultsTemplateUri).toEqual('app/search/companySearchResultsRow.html');
                expect(controller.searchCategoryClass).toEqual('esx-search-companies');

            });

            it('should set searchResultsTemplateUri for contact', function () {

                //arrange
                controller.queryObject.coreObjectType = coreType.contact;

                //act
                controller.search();

                //assert
                expect(controller.searchResultsTemplateUri).toEqual('app/search/contactSearchResultsRow.html');
                expect(controller.searchCategoryClass).toEqual('esx-search-contacts');

            });

            it('should set searchResultsTemplateUri for property', function () {

                //arrange
                controller.queryObject.coreObjectType = coreType.property;

                //act
                controller.search();

                //assert
                expect(controller.searchResultsTemplateUri).toEqual('app/search/propertiesSearchResultsRow.html');
                expect(controller.searchCategoryClass).toEqual('esx-search-properties');

            });

            it('should concat results to existing', function () {

                //arrange
                fakeSearchResults = {
                    resources: ['concat results!']
                }
                controller.searchResults.resources = ['starting Results'];

                var expected = controller.searchResults.resources.concat(fakeSearchResults.resources);

                //act
                controller.search();

                //assert
                expect(controller.searchResults.resources).toEqual(expected);

            });

            it('should block search call when isBusy and not seraching', function () {

                //arrange
                controller.queryObject.isBusy = true;

                //act
                controller.search(false);

                //assert
                expect(searchService.search).not.toHaveBeenCalled();
            });

        });

        describe('searchForContacts', function () {

            beforeEach(function () {
                activateController();
            });

            it('should set the core object type to contact', function () {

                //act
                controller.searchForContacts();

                //assert
                expect(controller.queryObject.coreObjectType).toEqual(coreType.contact);

            });

            it('should execute search', function () {

                spyOn(controller, 'search');

                //act
                controller.searchForContacts();

                //assert
                expect(controller.search).toHaveBeenCalled();

            });

            it('should clear results and set offset to 0', function () {

                controller.queryObject.offset = 40;
                controller.searchResults.resources = ['1', '2'];
                spyOn(controller, 'search');

                //act
                controller.searchForContacts();

                //assert
                expect(controller.queryObject.offset).toEqual(0);
                expect(controller.searchResults.resources.length).toEqual(0);

            });
        });

        describe('searchForProperies', function () {

            beforeEach(function () {
                activateController();
            });

            it('should set the core object type to property', function () {

                //act
                controller.searchForProperties();

                //assert
                expect(controller.queryObject.coreObjectType).toEqual(coreType.property);

            });

            it('should execute search', function () {

                spyOn(controller, 'search');

                //act
                controller.searchForProperties();

                //assert
                expect(controller.search).toHaveBeenCalled();

            });

            it('should clear results and set offset to 0', function () {

                controller.queryObject.offset = 40;
                controller.searchResults.resources = ['1', '2'];
                spyOn(controller, 'search');

                //act
                controller.searchForProperties();

                //assert
                expect(controller.queryObject.offset).toEqual(0);
                expect(controller.searchResults.resources.length).toEqual(0);

            });
        });

    });
})();

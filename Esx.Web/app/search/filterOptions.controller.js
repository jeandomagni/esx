﻿(function () {
    'use strict';

    angular.module('app').controller('filterOptionsController', filterOptionsController);

    filterOptionsController.$inject = ['$scope', '$mdDialog', 'filterType'];

    function filterOptionsController($scope, $mdDialog, filterType) {
        $scope.filterType = filterType;
        if (!$scope.filterType.values || !$scope.filterType.isEditing) {
            $scope.filterType.values = [];
        }

        $scope.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) list.splice(idx, 1);
            else list.push(item);
        };

        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };

        $scope.apply = function (form) {
            if (form.$invalid) {
                return;
            }
            $mdDialog.hide($scope.filterType);
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        }
    }
})();

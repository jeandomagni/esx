﻿(function () {
    'use strict';

    var serviceId = 'searchQueryBuilderService';

    angular.module('app').factory(serviceId, searchService);

    searchService.$inject = ['coreType'];

    function searchService(coreType) {

        var service = {
            buildParamQueryObject: buildParamQueryObject,
            createSearchUri: createSearchUri

        };

        return service;

        function buildParamQueryObject(currentState) {

            var returnObject = {
                pageSize: 20,
                offset: 0
            };
            if (currentState.data && currentState.data.coreObjectType) {
                returnObject.coreObjectType = currentState.data.coreObjectType;
            }
            if (currentState.data && currentState.data.coreObjectName) {
                returnObject.coreObjectName = currentState.data.coreObjectName;
            }
            return returnObject;
        }

        function createSearchUri(queryObject) {

            var returnUri = 'search/';

            switch (queryObject.coreObjectType) {
                case coreType.company:
                    returnUri += 'company';
                    break;
                case coreType.contact:
                    returnUri += 'contact';
                    break;
                case coreType.property:
                    returnUri += 'property';
                    break;
                default:
            }

            return returnUri;
        }

    }
})();
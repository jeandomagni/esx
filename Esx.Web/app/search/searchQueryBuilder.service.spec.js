﻿describe('searchQueryBuilder.service', function () {

    beforeEach(module('app'));

    var service,
        coreType,
        fakeState;

    beforeEach(inject(function (_searchQueryBuilderService_, _coreType_) {

        service = _searchQueryBuilderService_;
        coreType = _coreType_;

    }));

    describe('buildParamQueryObject', function () {

        it('should return base object when currentState data not present', function () {
        
            //arrange
            var currentState = {};
            var expected = {
                pageSize: 20,
                offset: 0
            };

            //act
            var actual = service.buildParamQueryObject(currentState);

            //assert
            expect(actual).toEqual(expected);
        });

        it('should set coreObjectType when present', function () {

            //arrange
            var currentState = {
                data: {
                    coreObjectType: coreType.company
                }
            };

            //act
            var actual = service.buildParamQueryObject(currentState);

            //assert
            expect(actual.coreObjectType).toBe(coreType.company);
        });
    });

    describe('createSearchUri', function () {

        it('should create base uri for search', function() {

            //act
            var actual = service.createSearchUri({});

            //assert
            expect(actual).toBe('search/');

        });

        it('should create a uri for company', function () {

            //arrange
            var queryObject = {
                    coreObjectType: coreType.company
            };

            //act
            var actual = service.createSearchUri(queryObject);

            //assert
            expect(actual).toBe('search/company');
        });

        it('should create a uri for contact', function () {

            //arrange
            var queryObject = {
                coreObjectType: coreType.contact
            };

            //act
            var actual = service.createSearchUri(queryObject);

            //assert
            expect(actual).toBe('search/contact');
        });

        it('should create a uri for property', function () {

            //arrange
            var queryObject = {
                coreObjectType: coreType.property
            };

            //act
            var actual = service.createSearchUri(queryObject);

            //assert
            expect(actual).toBe('search/property');
        });

        it('should create base search uri when coretype does not exist', function () {

            //arrange
            var queryObject = {
                coreObjectType: coreType.faultyCoreType
            };

            //act
            var actual = service.createSearchUri(queryObject);

            //assert
            expect(actual).toBe('search/');
        });
    });
});
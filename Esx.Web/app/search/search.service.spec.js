﻿describe('search.service', function () {

    beforeEach(module('app'));

    var searchService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_searchService_, _uriService_, _$httpBackend_) {

        searchService = _searchService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        uriService.search = 'api/search';

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('search', function () {
        it('should call server with search uri and query', function () {
            var query = {
                coreObjectType: "company",
                pagingData: {
                    pageSize: 10,
                    offset: 20
                }
            };

            //Arrange
            $httpBackend.expectPOST('api/search', query, { 'Accept': 'application/json, text/plain, */*', 'Content-Type': 'application/json;charset=utf-8' }).respond({});

            //Act
            searchService.search(query);

            //Assert
            $httpBackend.flush();

        });
    });
});
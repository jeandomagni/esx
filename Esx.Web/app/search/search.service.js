﻿(function () {
    'use strict';

    var serviceId = 'searchService';

    angular.module('app').factory(serviceId, searchService);

    searchService.$inject = ['uriService', '$resource'];

    function searchService(uriService, $resource) {

        var resource = $resource(
            uriService.search
        );

        var service = {
            search: search,
            getFilters: getFilters,
            searchMapItems: searchMapItems,
        };

        return service;

        function search(query) {
            return resource.save(query).$promise;
        }

        function getFilters(coreObjectType) {
            return $resource(uriService.getFilters).query({ coreObjectType: coreObjectType }).$promise;
        }

        function searchMapItems(query) {
            var mapSearch = $resource(uriService.searchMapItems,
            query,
            {
                getData: { method: 'POST', isArray: true }
            });
            return mapSearch.getData(query).$promise;
        }
    }
})();
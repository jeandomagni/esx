﻿(function () {
    'use strict';

    var controllerId = 'searchController';

    angular.module('app').controller(controllerId, searchController);

    searchController.$inject = ['$scope','common', 'searchService', 'coreType', '$state', 'searchQueryBuilderService', '$mdDialog', '$timeout', '$stateParams', '_', '$location', '$httpParamSerializer', 'searchMapUtils', 'mapsService'];

    function searchController($scope,common, searchService, coreType, $state, searchQueryBuilderService, $mdDialog, $timeout, $stateParams, _, $location, $httpParamSerializer, searchMapUtils, mapsService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var vm = this;

        vm.queryObject = {
            pageSize: 20,
            offset: 0
        };

        vm.search = search;
        vm.searchForCompanies = searchForCompanies;
        vm.searchForContacts = searchForContacts;
        vm.searchForProperties = searchForProperties;
        vm.searchResults = {
            resources:[]
        };
        vm.queryObject.filters = [];
        vm.filters = [];
        vm.filterTypes = [];
        vm.searchResultsTemplateUri = '';
        vm.showFilterOptions = showFilterOptions;
        vm.showNewSearchOptions = showNewSearchOptions;
        vm.filter = filter;
        vm.editFilter = editFilter;
        vm.searchInput = $stateParams.searchInput;
        vm.getShareableLink = getShareableLink;
        vm.showMap = false; 
        activate();

        function activate() {
            vm.initializing = true;
            var startSearchFromRouteParams = angular.noop;
            var routeQueryObject = searchQueryBuilderService.buildParamQueryObject($state.current);
 
            if (!!routeQueryObject.coreObjectType) {
                vm.queryObject = routeQueryObject;
                startSearchFromRouteParams = search;
                getFilters();
            } else {
                vm.showNewSearchOptions();
                return;
            }

            //TODO: Clean up this shareable link code.
            var searchQueryString = $location.search();
            if (searchQueryString) {
                for (var key in searchQueryString) {
                    if (searchQueryString.hasOwnProperty(key)) {
                        var item = searchQueryString[key];
                        var filterJson = JSON.parse(item);
                        vm.filters.push(filterJson);
                    }
                }
                $timeout(function () {
                    vm.filter();
                });
                return;
            }
            common.activateController([startSearchFromRouteParams()], controllerId)
                .then(function () {
                    log('Activated Search View');                    
                });            
        }

        function resetAndSearch() {
            vm.queryObject.offset = 0;
            vm.searchResults.resources.length = 0;
            vm.search();
        }

        function searchForCompanies() {
            vm.queryObject.coreObjectType = coreType.company;
            resetAndSearch();
        }

        function searchForContacts() {
            vm.queryObject.coreObjectType = coreType.contact;
            resetAndSearch();
        }

        function searchForProperties() {
            vm.queryObject.coreObjectType = coreType.property;
            resetAndSearch();
        }

        function search(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            searchService.search(vm.queryObject).then(processSearchResults);
        }

        function processSearchResults(results) {
            switch (vm.queryObject.coreObjectType) {
                case coreType.company:
                    vm.searchCategoryClass = 'esx-search-companies';
                    vm.searchResultsTemplateUri = 'app/search/companySearchResultsRow.html';
                    break;
                case coreType.contact:
                    vm.searchCategoryClass = 'esx-search-contacts';
                    vm.searchResultsTemplateUri = 'app/search/contactSearchResultsRow.html';
                    break;
                case coreType.property:
                    vm.searchCategoryClass = 'esx-search-properties';
                    vm.searchResultsTemplateUri = 'app/search/propertiesSearchResultsRow.html';
                    break;
                case coreType.deal:
                    vm.searchCategoryClass = 'esx-search-deals';
                    vm.searchResultsTemplateUri = 'app/search/dealSearchResultsRow.html';
                    break;
                case coreType.loan:
                    vm.searchCategoryClass = 'esx-search-loans';
                    vm.searchResultsTemplateUri = 'app/search/loanSearchResultsRow.html';
                    break;
                default:
            }
            vm.searchResults.resources = vm.searchResults.resources.concat(results.resources);
            vm.queryObject.isBusy = false;
            if (vm.initializing) {
                vm.initializing = false;
            }
        }

        function showFilterOptions(filterType) {
            $mdDialog.show({
                controller: 'filterOptionsController',
                templateUrl: 'app/search/filterOptions.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false,
                locals: { filterType: filterType.isEditing ? filterType : angular.copy(filterType) }
            }).then(function (newFilter) {
                if (!newFilter || !newFilter.values || !newFilter.values.length) {
                    return;
                }
                if (!newFilter.isEditing) {
                    vm.filters.push(newFilter);
                }                
                vm.filter();                
            }, function () {                
            });
        }

        function showNewSearchOptions() {
            $mdDialog.show({
                controller: ['$scope', '$mdDialog', function ($scope, $mdDialog) {
                    $scope.searchInput = vm.searchInput;
                    $scope.apply = function (state) {
                        $mdDialog.hide(state);
                    }
                }],
                templateUrl: 'app/search/searchOptions.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                escapeToClose: false,
                fullscreen: false
            }).then(function (state) {
                $state.go(state);
                $timeout(function () {
                    vm.searchResults.resources = [];                    
                    activate();
                });                
            });
        }

        function filter(chip) {
            vm.checkMapFilters(chip);
            getMapItems();
            vm.queryObject.filters = vm.filters;
            resetAndSearch();
        }

        function editFilter(filterToEdit) {
            filterToEdit.isEditing = true;
            showFilterOptions(filterToEdit);
        }
        
        function getFilters() {
            vm.queryObject.filters = [];
            vm.filters = [];
            vm.filterTypes = [];
            searchService.getFilters(vm.queryObject.coreObjectType).then(function(result) {
                vm.filterTypes = result;
                populateFilterFromGlobalSearch();
            });
        }

        function populateFilterFromGlobalSearch() {
            if (!vm.searchInput)
                return;

            var category = _.find(vm.filterTypes, { category: vm.queryObject.coreObjectName });
            switch (vm.queryObject.coreObjectType) {
                case 'C':                    
                    var contactName = _.find(category.filters, { name: 'ContactName' });
                    contactName.values = [{ value: vm.searchInput }];
                    vm.filters.push(contactName);
                    break;
            }
            $timeout(function() {
                vm.filter();
                vm.searchInput = null;
            });
        }

        //TODO: Clean up this shareable link code.
        function getShareableLink() {
            var filterQuery = $httpParamSerializer(vm.filters);
            $location.search(filterQuery);
            var shareableLink = $location.absUrl();
            $mdDialog.show({
                controller: ['$scope', '$mdDialog', function ($scope, $mdDialog) {
                    $scope.shareableLink = shareableLink;
                    $scope.apply = function () {
                        $mdDialog.hide();
                    }
                }],
                templateUrl: 'app/search/shareableLink.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function () {
            });
        }

        var map;
        var markers = [];
        var markerCluster;
        vm.mapFilters = []; 


        vm.toggleMap = function() {
            vm.showMap = true;
            if (!map) {
                map = searchMapUtils.initMap(map);
                vm.queryObject.coreObjectType = coreType.property;
                vm.queryObject.filters = vm.filters;
                searchService.searchMapItems(vm.queryObject).then(function (result) {
                    vm.mapObjects = searchMapUtils.addClusteredMarkers(map, result, markers, markerCluster);
                    map.fitBounds(mapsService.setMapExtent(vm.mapObjects.markers));
                    vm.mapCount = result.length;
                });

               
                google.maps.event.addListener(map.getDrawingManager, 'overlaycomplete', function (e) {
                    if (e.type != google.maps.drawing.OverlayType.MARKER) {
                        map.getDrawingManager.setDrawingMode(null);
                        var newShape = e.overlay;
                        newShape.type = e.type;
                        google.maps.event.addListener(newShape, 'click', function () {
                            searchMapUtils.setSelection(newShape);
                        });
                        searchMapUtils.setSelection(newShape);

                        vm.filters.push(searchMapUtils.addFilter(vm.filters, newShape));
                        vm.queryObject.coreObjectType = coreType.property;
                        $scope.$apply();

                        getMapItems();
                    }
                });

            } 
        };


       
        vm.mapFilterValues = [];

        //TODO add to service
        function consolidateMapFilters(filters) {
            var mapFilters = filters.filter(function (f) {
                    return f.type === 'mapChip';
            });
            mapFilters.forEach(function (f) {
                f.values.forEach(function (v) {
                    if (vm.mapFilterValues.indexOf(v)===-1) {
                        vm.mapFilterValues.push(v);
                    }
                    
                });
            });
            filters = filters.filter(function (f) {
                return f.type !== 'mapChip';
            }).filter(function (f) {
                return f.type !== 'map';
            });
            filters.push(  {
                label: 'Map Filter',
                name: 'PropertyMapView',
                type: 'map',
                values: vm.mapFilterValues
            });

            //remove nulls
            filters = filters.filter(function(f) {
                return f; 
            });
            return filters; 
        }


        function getMapItems(chip) {
            if (map) {

                //get count of map items 
                var consolidatedFilters = consolidateMapFilters(vm.filters);
                vm.queryObject.filters = consolidatedFilters;

                searchService.searchMapItems(vm.queryObject)
                             .then(function(result) {
                                 vm.mapCount = result.length;
                                 var nonMapFilters = vm.filters.filter(function(f) {
                                         return f.type !== 'mapChip';
                                     })
                                     .filter(function(f) {
                                         return f.type !== 'map';
                                     });
                                 vm.queryObject.filters = nonMapFilters;

                        searchService.searchMapItems(vm.queryObject)
                            .then(function(result) {
                                vm.mapObjects.clusters.clearMarkers();
                                vm.mapObjects = searchMapUtils.addClusteredMarkers(map, result, markers, markerCluster);
                            });

                    });

          
            }
           
        }

        vm.checkMapFilters = function (chip) {
            if (chip && chip.type === 'mapChip') {
                searchMapUtils.deleteSelectedShape(chip);
                vm.mapFilterValues = vm.mapFilterValues.filter(function (item) {
                    return item.shapeId !== chip.shapeId;
            });
            }
            getMapItems();
    }
        vm.toggleList = function () {
            vm.showMap = false;
            var consolidatedFilters = consolidateMapFilters(vm.filters);
            vm.queryObject.filters = consolidatedFilters;
            resetAndSearch();
        };



    }
})();
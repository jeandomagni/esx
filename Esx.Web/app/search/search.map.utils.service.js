﻿(function () {
    'use strict';

    var serviceId = 'searchMapUtils';
    angular
        .module('app')
        .service(serviceId, searchMapUtils);

    searchMapUtils.$inject = ['mapOptions'];
    var selectedShape;
    var shapes = [];
    var mapItems; 

    function searchMapUtils(mapOptions) {

       

        this.addFilter = function (filters,shape) {
            var mapFilters = filters.filter(function (filter) {
                return filter.type === 'mapChip';
            });

            shapes.push({shapeId:shape.shapeId, shape:shape});

            return {
                label: 'Map Filter' + (mapFilters.length + 1).toString(),
                name: 'PropertyMapView',
                type: 'mapChip',
                shapeId:shape.shapeId,
                values: [
                {
                    label: 'Map View',
                    shapeId: shape.shapeId,
                    value: this.getWKTFilter(shape)
                }]
            }

        }
        this.initMap = function (map) {
           var styledMap = new google.maps.StyledMapType(mapOptions.styles, { name: "Roads" });
           var options = {
               zoom: 4,
               center: new google.maps.LatLng(37.7773664, -99.7175739),
               mapTypeControlOptions: {
                   mapTypeIds: [google.maps.MapTypeId.SATELLITE, 'map_style']
               }
           };
           map = new google.maps.Map(document.getElementById('map'), options);
           map.mapTypes.set('map_style', styledMap);
           map.setMapTypeId('map_style');

           google.maps.event.addListenerOnce(map, 'idle', function () {
               google.maps.event.trigger(map, 'resize');
           });

            //drawing 
           var drawingManager = new google.maps.drawing.DrawingManager({
               drawingMode: google.maps.drawing.OverlayType.POLYGON,
               drawingControl: true,
               drawingControlOptions: {
                   position: google.maps.ControlPosition.TOP_RIGHT,
                   drawingModes: [
                     google.maps.drawing.OverlayType.POLYGON
                   ]
               },
               circleOptions: {
                   fillColor: '#ffff00',
                   fillOpacity: .5,
                   strokeWeight: 2,
                   clickable: true,
                   editable: true,
                   zIndex: 1
               }, polygonOptions: {
                   fillColor: '#ffff00',
                   fillOpacity: .5,
                   strokeWeight: 2,
                   clickable: true,
                   editable: true,
                   zIndex: 1
               }, rectangleOptions: {
                   fillColor: '#ffff00',
                   fillOpacity: .5,
                   strokeWeight: 2,
                   clickable: true,
                   editable: true,
                   zIndex: 1
               }
           });
           drawingManager.setMap(map);
            map.getDrawingManager = drawingManager;
           return map; 
        }


        this.addClusteredMarkers = function (map,mapData, markers, markerCluster) {

            //remove markers
            markers.forEach(function (marker) {
                marker.setMap(null);
            });

            markers = [];

            if (markerCluster) {
                markerCluster.clearMarkers();
            }


            //add new markers
            mapData.forEach(function (item) {
                var latLng = new google.maps.LatLng(item.latitude, item.longitude);
                var marker = new google.maps.Marker({ 'position': latLng });
                var infowindow = new google.maps.InfoWindow({
                    content: '<div>' + item.name + '</div>' + '<div><a href="#/properties/' + item.mentionId + '/summary">More Info</a></div>'
                });
                marker.addListener('click', function () {
                    infowindow.open(marker.get('map'), marker);
                });
                markers.push(marker);
            });

            

            mapItems =  {
                clusters:new MarkerClusterer(map, markers),
                markers: markers
            }
            return mapItems; 
        };


        this.clearSelection = function() {
            if (selectedShape) {
                selectedShape.setEditable(false);
                selectedShape = null;
            }
        }

        this.setSelection = function(shape) {
            this.clearSelection();
            selectedShape = shape;
            shape.setEditable(true);
            shape.shapeId = shapes.length;
            if (shapes.indexOf(shape) === -1) {
                shapes.push(shape);
            }
        }

        this.deleteSelectedShape = function (selectedShape) {
            if (selectedShape) {
                var shapeToDelete = shapes.filter(function (s) {
                    return s.shapeId === selectedShape.shapeId;
                })[0];

                shapes = shapes.filter(function (s) {
                    return s.shapeId !== selectedShape.shapeId;
                });
                shapeToDelete.setMap(null);
            }
        };

     this.getWKTFilter = function(shape) {
                var wkt = 'POLYGON ((';
                shape.getPath()
                    .j.reverse().forEach(function (coordinate) {
                        wkt = wkt + coordinate.lng() + ' ' + coordinate.lat() + ',';
                    });
                //get first coordinate to close polygon
                wkt = wkt + shape.getPath().j[0].lng() + ' ' + shape.getPath().j[0].lat() + '))';
            return wkt;
        }



    }
})();
﻿describe('employee.controller', function () {
    beforeEach(module('app'));

    var $controller,
        common,
        employeesService,
        companiesService,
        fakeEmployee,
        fakeCompany,
        fakeCompanyList,
        $scope,
        controller;

    var activateController = function (employeeId) {
        common.$stateParams.employeeId = employeeId;

        controller = $controller('employeeController', {
            common: common,
            employeesService: employeesService,
            companiesService: companiesService
        });
    };

    beforeEach(inject(function (_$controller_, _common_, _employeesService_, _companiesService_) {

        //inject
        $controller = _$controller_;
        common = _common_;
        employeesService = _employeesService_;
        companiesService = _companiesService_;

        //fakes
        $scope = {};

        common.$window = { location: {} };

        fakeEmployee = {
            employeeID: 123,
            firstName: 'firstName',
            lastName: 'lastName'
        };

        fakeCompany = {
            companyID: 456,
            companyName: 'companyName'
        };

        fakeCompanyList = [
            {
                companyID: 1,
                companyName: 'One'
            },
            {
                companyID: 2,
                companyName: 'Two'
            }
        ];

        //spys
        spyOn(employeesService, 'getEmployee').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback(fakeEmployee);
                }
            };
        });
        spyOn(employeesService, 'addEmployee').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback(fakeEmployee);
                }
            };
        });
        spyOn(employeesService, 'updateEmployee').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback(fakeEmployee);
                }
            };
        });

        spyOn(common, 'activateController').and.callFake(function () {
            return {
                then: function (callback) {
                    return callback(fakeEmployee);
                }
            };
        });

    }));

    describe('activate existing employee', function () {
        var employeeId = 123;

        beforeEach(function () {
            activateController(employeeId);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

        it('should have the correct title', function () {
            expect(controller.title).toBe('Employee Details');
        });

        it('should be in view mode', function () {
            expect(controller.editMode).toBe(false);
        });

        it('should set page data', function () {
            expect(employeesService.getEmployee).toHaveBeenCalledWith(employeeId);
            expect(controller.employee).toEqual(fakeEmployee);
        });
    });

    describe('activate new employee', function () {

        var employeeId = 0;

        beforeEach(function () {
            activateController(employeeId);
        });

        it('should be defined', function () {
            expect(controller).toBeDefined();
        });

        it('should have the correct title', function () {
            expect(controller.title).toBe('Employee Details');
        });

        it('should be in edit mode', function () {
            expect(controller.editMode).toBe(true);
        });

        it('should not set employee data', function () {
            expect(employeesService.getEmployee).not.toHaveBeenCalled();
        });
    });

    describe('employee edit', function () {
        var employeeId = 123;

        beforeEach(function () {
            activateController(employeeId);
        });

        it('should be in edit mode', function () {
            //Act
            controller.edit();

            //Assert
            expect(controller.editMode).toBe(true);
        });
    });

    describe('employee save new', function () {
        var employeeId = 0;

        beforeEach(function () {
            activateController(employeeId);
        });

        it('should add employee', function () {
            //Act
            controller.save({ $valid: true });

            //Assert
            expect(employeesService.addEmployee).toHaveBeenCalled();
            expect(common.$window.location.href).toBe('#/employees/123');
        });
    });

    describe('employee save new invalid data', function () {
        var employeeId = 0;

        beforeEach(function () {
            activateController(employeeId);
        });

        it('should not add employee', function () {
            //Act
            controller.save({ $valid: false });

            //Assert
            expect(employeesService.addEmployee).not.toHaveBeenCalled();
        });
    });

    describe('employee save existing', function () {
        var employeeId = 123;

        beforeEach(function () {
            activateController(employeeId);
        });

        it('should update employee', function () {
            //Act
            controller.edit();
            controller.save({ $valid: true });

            //Assert
            expect(employeesService.updateEmployee).toHaveBeenCalled();
            expect(controller.employee).toEqual(fakeEmployee);
        });
    });

    describe('employee save existing invalid data', function () {
        var employeeId = 123;

        beforeEach(function () {
            activateController(employeeId);
        });

        it('should not update employee', function () {
            //Act
            controller.edit();
            controller.save({ $valid: false });

            //Assert
            expect(employeesService.updateEmployee).not.toHaveBeenCalled();
        });
    });

    describe('employee cancel edit', function () {
        var employeeId = 123;

        beforeEach(function () {
            activateController(employeeId);
        });

        it('should be in view mode', function () {
            //Act
            controller.edit();
            controller.cancel();

            //Assert
            expect(controller.editMode).toBe(false);
        });
    });

    describe('employee cancel add', function () {
        var employeeId = 0;

        beforeEach(function () {
            activateController(employeeId);
        });

        it('should redirect', function () {
            //Act
            controller.cancel();

            //Assert
            expect(common.$window.location.href).toBe('#/employees');
        });
    });

    describe('employee select company', function () {

        beforeEach(function () {
            activateController(0);
        });
        
        it('company selected', function () {
            //Arrange
            var companyId = 123;

            //Act
            controller.selectedCompanyChange({ companyID: companyId });

            //Assert
            expect(controller.employee.companyID).toBe(companyId);
        });

    });
});
﻿(function () {
    'use strict';
    var controllerId = 'employeeController';

    angular.module('app').controller(controllerId, employeeController);

    employeeController.$inject = ['common', 'employeesService', 'companiesService'];

    function employeeController(common, employeesService, companiesService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.title = 'Employee Details';
        vm.employee = {
            employeeID: parseInt(common.$stateParams.employeeId, 10) || 0
        };
        vm.categories = [];
        vm.editMode = vm.employee.employeeID ? false : true;
        vm.edit = edit;
        vm.save = save;
        vm.cancel = cancel;
        vm.showAll = showAll;
        vm.selectedCompanyChange = selectedCompanyChange;
        
        vm.search = {};

        activate();

        function activate() {
            var promises = [];
            if (vm.employee.employeeID) {
                promises.push(getEmployee());
            }

            common.activateController(promises, controllerId).then(function () {
                if (vm.employee.employeeID) {
                    log('Retrieved employee details.');
                }
            });
        }

        function getEmployee() {
            return employeesService.getEmployee(vm.employee.employeeID).then(function (employeeData) {
                if (employeeData.companyID) {
                    vm.company = { companyID: employeeData.companyID, companyName: employeeData.companyName };
                    vm.search.selectedItem = vm.company;
                }
                return vm.employee = employeeData;
            });
        }

        function addEmployee(employee) {
            return employeesService.addEmployee(employee).then(function (employeeData) {
                log('Added employee.');
                common.$window.location.href = '#/employees/' + employeeData.employeeID;
            });
        }

        function updateEmployee(employee) {
            return employeesService.updateEmployee(employee).then(function (employeeData) {
                log('Updated employee.');

                // Refresh employee model 
                getEmployee();
            });
        }

        function edit() {
            if (vm.employee.employeeID) {
                vm.originalEmployee = angular.copy(vm.employee);
            }
            vm.editMode = true;
        }

        function cancel() {
            if (vm.employee.employeeID) {
                vm.employee = vm.originalEmployee;
                vm.search.selectedItem = vm.company;
            } else {
                common.$window.location.href = '#/employees';
            }
            vm.editMode = false;
        }

        function save(employeeForm) {
            if (!employeeForm.$valid) {
                logError('Invalid input.');
                return;
            }

            if (vm.employee.employeeID) {
                updateEmployee(vm.employee);
            } else {
                addEmployee(vm.employee);
            }
            vm.editMode = false;
        }

        function showAll() {
            common.$window.location.href = '#/employees';
        }

        function selectedCompanyChange(company) {
            if (company && company.companyID) {
                vm.employee.companyID = company.companyID;
                vm.employee.companyName = company.companyName;
            }
        }
    }
})();

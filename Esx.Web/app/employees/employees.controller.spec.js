﻿(function () {
    describe('employees.controller', function () {
        beforeEach(module('app'));

        var $controller,
            common,
            employeesService,
            fakeEmployees,
            $scope,
            controller,
            dataToVerify;

        var activateController = function () {
            dataToVerify = {};

            controller = $controller('employeesController', {
                common: common,
                employeesService: employeesService
            });
        };

        beforeEach(inject(function (_$controller_, _common_, _employeesService_) {

            //inject
            $controller = _$controller_;
            common = _common_;
            employeesService = _employeesService_;

            //fakes
            $scope = {};

            fakeEmployees = {
                resources: [{ name: 'some employee' }],
                pagingData: {
                    pageSize: 10,
                    offset: 0
                }
            };

            //spys
            spyOn(employeesService, 'getEmployees').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeEmployees);
                    }
                };
            });

            spyOn(employeesService, 'searchEmployees').and.callFake(function () {
                return {
                    then: function (callback) {
                        return callback(fakeEmployees);
                    }
                };
            });

        }));

        afterEach(function () {
            common.$stateParams.dealId = undefined;
            common.$stateParams.companyId = undefined;
        });

        //cover once build
        /*describe('employee activate', function () {

            beforeEach(function () {
                activateController();
            });

            it('should be defined', function () {
                expect(controller).toBeDefined();
            });

            it('should define queryObject', function () {
                expect(controller.queryObject).toBeDefined();
            });

            it('should set default paging data', function () {

                //arrange
                var expectedPagingData = {
                    pageSize: 10,
                    offset: 0
                };
               
                expect(controller.employeesList.pagingData).toEqual(expectedPagingData);
            });
        });*/
        //cover once build
        describe('employee search', function () {
            beforeEach(function () {
                activateController();
            });

            /*it('should set search results', function () {

                var searchQuery = 'testQuery';
                controller.searchEmployees(searchQuery);

                expect(employeesService.searchEmployees).toHaveBeenCalledWith(searchQuery);
                expect(controller.searchResults).toEqual(fakeEmployees);
            });*/
        });
        
        describe('getEmployees', function () {

            it('should get employees with the query object', function () {

                //arrange
                //activateController();

                var expectedSearchQuery = {
                    someValue: 5,
                    someText: 'text!'
                };

                //controller.queryObject = expectedSearchQuery;

                //cover once build

                //act
                //controller.getEmployees();

               
                //expect(employeesService.getEmployees).toHaveBeenCalledWith(expectedSearchQuery);
            });
        });
    });
})();

﻿(function () {
    'use strict';

    var controllerId = 'employeesController';

    angular.module('app').controller(controllerId, employeesController);

    employeesController.$inject = ['common', 'employeesService'];

    function employeesController(common, employeesService) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;

        vm.searchEmployees = searchEmployees;
        vm.searchResults = {};
        vm.queryObject = {
            sort: 'employeeName', filters: common.$stateParams.filters,
            offset: 0,
            pageSize: 30
        };

        activate();

        function activate() {
            common.activateController([searchEmployees()], controllerId)
                .then(function () { log('Activated Employees View'); });
        }

        function getEmployees(isSearching) {
            if (vm.queryObject.isBusy && !isSearching) return;
            vm.queryObject.isBusy = true;
            return employeesService.getEmployees(vm.queryObject).then(function (employeesList) {
                vm.queryObject.isBusy = false;
                // TODO: concatenate instead of overwrite once infinite scroll is implemented.
                return vm.employeesList = employeesList;
            });
        }

        function searchEmployees() {
            if (vm.searchText.length === 0 || vm.searchText.length > 2) {
                vm.queryObject.searchText = vm.searchText;
                vm.queryObject.offset = 0;
                vm.employeesList.resources.length = 0;
                getEmployees(true);
            }
        }
    }
})();
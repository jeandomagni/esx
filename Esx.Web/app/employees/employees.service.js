﻿(function () {
    'use strict';

    var serviceId = 'employeesService';

    angular.module('app').factory(serviceId, employeesService);

    employeesService.$inject = ['uriService', '$resource'];

    function employeesService(uriService, $resource) {

        var employeeResource = $resource(
            uriService.employee,
            { employeeId: '@id' },
            { 'update': { method: 'PATCH' } }
        );

        var service = {
            getEmployees: getEmployees,
            searchEmployees: searchEmployees,
            getEmployee: getEmployee,
            addEmployee: addEmployee,
            updateEmployee: updateEmployee
        };

        return service;

        function getEmployees(query) {
            return $resource(uriService.employeesList).get({ searchText: query }).$promise;
        }

        function searchEmployees(query) {
            return $resource(uriService.employeesSearch, { query: '@query' }).query({ query: query }).$promise;
        }

        function getEmployee(id) {
            return employeeResource.get({ employeeId: id }).$promise;
        }

        function addEmployee(employee) {
            return employeeResource.save(employee).$promise;
        }

        function updateEmployee(employee) {
            return employeeResource.update(employee).$promise;
        }
    }
})();

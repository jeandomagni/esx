﻿describe('employees.service', function () {

    beforeEach(module('app'));

    var employeesService,
        uriService,
        $httpBackend;

    beforeEach(inject(function (_employeesService_,_uriService_, _$httpBackend_) {

        employeesService = _employeesService_;
        $httpBackend = _$httpBackend_;
        uriService = _uriService_;

        uriService.employeesList = '/employees';
        uriService.employee = '/employees/:employeeId';

        // ignore html templates. Not sure if this is a good approach. 
        $httpBackend.whenGET(/.*.html/).respond(200, '');

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('getEmployee', function () {
        it('should call server with employee uri', function () {
            var employeeId = 123;

            //Arrange
            $httpBackend.expectGET("api/employees/" + employeeId, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            employeesService.getEmployee(employeeId);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('addEmployee', function () {
        it('should call server with employee uri', function () {
            var employee = { firstName: "Test Name" };

            //Arrange
            $httpBackend.expectPOST("api/employees", employee, { "Accept": "application/json, text/plain, */*", "Content-Type": "application/json;charset=utf-8" }).respond({});

            //Act
            employeesService.addEmployee(employee);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('updateEmployee', function () {
        it('should call server with employee uri', function () {
            var employee = { firstName: "Test Name" };

            //Arrange
            $httpBackend.expectPATCH("api/employees", employee, { "Accept": "application/json, text/plain, */*", "Content-Type": "application/json;charset=utf-8" }).respond({});

            //Act
            employeesService.updateEmployee(employee);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('getEmployees', function () {

        it('should call server with employees list uri', function () {

            //Arrange
            $httpBackend.expectGET(uriService.employeesList, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            employeesService.getEmployees();

            //Assert
            $httpBackend.flush();

        });

        it('should return a promise that resolves', function () {

            //Arrange
            var fakeEmployees = {
                resources: [{ name: 'fake name' }]
            };
            
            var testPromise = function (promise) {
                expect(promise.$resolved).toBe(true);
                expect(promise.resources).toEqual(fakeEmployees.resources);
            }
            
            $httpBackend.expectGET(uriService.employeesList, { "Accept": "application/json, text/plain, */*" }).respond(fakeEmployees);

            //Act & Assert
            employeesService.getEmployees().then(testPromise);

            //Flush
            $httpBackend.flush();

        });

        it('should make GET request with query', function () {

            //Arrange
            var query = 'test';

            var expectedUri = uriService.employeesList + '?searchText=' + query;

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond({});

            //Act
            employeesService.getEmployees(query);

            //Assert
            $httpBackend.flush();

        });
    });

    describe('searchEmployees', function () {

        it('should call server with employees search uri', function () {

            //Arrange
            var query = 'john';
            var expectedUri = uriService.employeesSearch + '?query=' + query;

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond([]);

            //Act
            employeesService.searchEmployees(query);

            //Assert
            $httpBackend.flush();

        });

        it('should return a promise that resolves', function () {

            //Arrange
            var fakeResults = [];

            var testPromise = function (promise) {
                expect(promise.$resolved).toBe(true);
            }

            var query = 'john';
            var expectedUri = uriService.employeesSearch + '?query=' + query;

            $httpBackend.expectGET(expectedUri, { "Accept": "application/json, text/plain, */*" }).respond(fakeResults);

            //Act & Assert
            employeesService.searchEmployees(query).then(testPromise);

            //Flush
            $httpBackend.flush();

        });
    });
});
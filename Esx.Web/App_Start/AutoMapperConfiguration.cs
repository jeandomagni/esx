﻿using System.Linq;
using AutoMapper;
using Esx.Contracts.Models.Company;
using Esx.Web.ViewModels;

namespace Esx.Web
{
    public class AutoMapperConfiguration :Profile
    {
        protected override void Configure()
        {
            this.CreateMap<CompanyAdd, AddCompanyViewModel>().ReverseMap();
        }
    }

}
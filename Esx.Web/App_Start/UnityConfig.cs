using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Esx.Business;
using Esx.Business.Alerts;
using Esx.Business.Comments;
using Esx.Business.Companies;
using Esx.Business.Contacts;
using Esx.Business.Deals;
using Esx.Business.Documents;
using Esx.Business.Filter;
using Esx.Business.Loans;
using Esx.Business.MarketingList;
using Esx.Business.Mentions;
using Esx.Business.Properties;
using Esx.Business.Resources;
using Esx.Business.Search;
using Esx.Business.Security;
using Esx.Business.Tags;
using Esx.Business.Users;
using Esx.Contracts.Extensions;
using Esx.Contracts.Paging;
using Esx.Contracts.Security;
using Esx.Data;
using Esx.Data.Dapper;
using Esx.Data.Repositories;
using Esx.Web.Common;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using UnityLog4NetExtension.Log4Net;
using Esx.Business.sidebar;
using Esx.Business.UserImpersonation;
using Esx.Web.Authentication;

namespace Esx.Web
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private readonly static Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            //Register all the automapper maps. 
            container.RegisterAutoMapperTypes();

            // Interception/logging configuration.
            container.AddNewExtension<Log4NetExtension>();
            container.AddNewExtension<Interception>();

            // TODO: Register your types here
            container.RegisterType<ISecurityContext, SecurityContext>(new PerRequestLifetimeManager());

            RegisterServiceTypes(container);

            RegisterDataLayerTypes(container);

            ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(container));

        }

        private static void RegisterServiceTypes(IUnityContainer container)
        {
            container.RegisterType<IContactsService, ContactsService>();
            // Log methods on the ICompaniesService.
            container.RegisterType<ICompaniesService, CompaniesService>(
                new Interceptor<InterfaceInterceptor>(),
                new InterceptionBehavior<LoggingInterceptorBehavior>());

            container.RegisterType<IDealsService, DealsService>();
            container.RegisterType<ILoansService, LoansService>();
            container.RegisterType<IPropertiesService, PropertiesService>();
            container.RegisterType<IMentionsService, MentionsService>();
            container.RegisterType<ITagsService, TagsService>();
            container.RegisterType<ICommentsService, CommentsService>();
            container.RegisterType<IResourcesService, ResourcesService>();
            container.RegisterType<IUsersService, UsersService>();
            container.RegisterType<IAlertsService, AlertsService>();
            container.RegisterType<IMarketingListService, MarketingListService>();
            container.RegisterType<ISearchService, SearchService>();
            container.RegisterType<IFilterService, FilterService>();
            container.RegisterType<ISearchCriteriaFactory, SearchCriteriaFactory>();
            container.RegisterType<ICompanyCriteriaSetService, CompanyCriteriaSetService>();
            container.RegisterType<IContactCriteriaSetService, ContactCriteriaSetService>();
            container.RegisterType<IDealCriteriaSetService, DealCriteriaSetService>();
            container.RegisterType<ILoanCriteriaSetService, LoanCriteriaSetService>();
            container.RegisterType<IPropertyCriteriaSetService, PropertyCriteriaSetService>();
            container.RegisterType<IDocumentsService, DocumentsService>();
            container.RegisterType<ISidebarService, SidebarService>();
            container.RegisterType<IFormsAuthenticationService, FormsAuthenticationService>();
            container.RegisterType<IUserRoleService, UserRoleService>();
            container.RegisterType<IActiveDirectoryService, ActiveDirectoryService>();
            container.RegisterType<IUserImpersonationService, UserImpersonationService>();
            container.RegisterType(typeof(ICacheProvider<>), typeof(CacheProvider<>));
            container.RegisterType<IRoleFeatureService, RoleFeatureService>();
        }

        private static void RegisterDataLayerTypes(IUnityContainer container)
        {

            // Dapper setup. 
            var cn = ConfigurationManager.ConnectionStrings["dbEsx"].ConnectionString;
            container.RegisterType<IDbConnection, SqlConnection>(new PerRequestLifetimeManager(), new InjectionConstructor(cn));
            container.RegisterType<IConnectionWrapper, ConnectionWrapper>();
            container.RegisterType<IDapperParameterProvider, DapperParameterProvider>();

            container.RegisterType<IMentionsRepository, MentionsRepository>();
            container.RegisterType<ITagsRepository, TagsRepository>();
            container.RegisterType<ICompaniesRepository, CompaniesRepository>();
            container.RegisterType<ICommentsRepository, CommentsRepository>();
            container.RegisterType<ISecurityRepository, SecurityRepository>();

            // EF setup.
            container.RegisterType<IEsxDbContext, EsxDbContext>(new PerRequestLifetimeManager(), new InjectionConstructor(new ResolvedParameter<IMentionsRepository>(), new ResolvedParameter<ISecurityContext>(), cn));

        }
    }
}

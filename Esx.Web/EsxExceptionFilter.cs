﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using Esx.Contracts.ExceptionHandling;

namespace Esx.Web
{
    public class EsxExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is BaseException)
            {
                var baseException = (BaseException) actionExecutedContext.Exception;

                actionExecutedContext.Response = new HttpResponseMessage
                {
                    Content = new StringContent(baseException.Message),
                    StatusCode = baseException.StatusCode                    
                };
            }
        }

    }
}
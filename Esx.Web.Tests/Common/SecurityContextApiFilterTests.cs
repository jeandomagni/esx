﻿using System.Net.Http;
using System.Security.Principal;
using System.Web.Http.Controllers;
using Esx.Contracts.Security;
using Esx.Tests.Common.Helpers;
using Esx.Web.Common;
using Microsoft.Practices.ServiceLocation;
using Moq;
using NUnit.Framework;
using Esx.Web.Authentication;

namespace Esx.Web.Tests.Common
{
    public abstract class SecurityContextApiFiltertests
    {

        private SecurityContextApiFilter _filter;
        private Mock<IFormsAuthenticationService> _formsAuthenticationServiceMock;


        [SetUp]
        public void Setup()
        {
            _filter = new SecurityContextApiFilter();

            _formsAuthenticationServiceMock = new Mock<IFormsAuthenticationService>();
            var mockServiceLocator = new Mock<IServiceLocator>();
            mockServiceLocator.Setup(x => x.GetInstance<IFormsAuthenticationService>()).Returns(_formsAuthenticationServiceMock.Object);
            ServiceLocator.SetLocatorProvider(new ServiceLocatorProvider(() => mockServiceLocator.Object));
        }

        public sealed class OnActionExecuting : SecurityContextApiFiltertests
        {

            [Test]
            public void Should_prepare_securityContext_And_SignIn()
            {
                //arrange
                var username = RandomData.GetString(10);
                var actionContext = CreateExecutingContext(username);

                _formsAuthenticationServiceMock.Setup(m => m.SignIn(username));

                //act
                _filter.OnActionExecuting(actionContext);

                //assert    
                _formsAuthenticationServiceMock.VerifyAll();

            }            

        }

        private HttpActionContext CreateExecutingContext(string username)
        {
            return new HttpActionContext
            {
                ControllerContext = new HttpControllerContext
                {
                    Request = new HttpRequestMessage(),
                    RequestContext = new HttpRequestContext
                    {
                        Principal = new GenericPrincipal(new GenericIdentity(username, ""), new string[0])
                    }
                }
            };
        }
        
    }
}

﻿using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Esx.Contracts.Security;
using Esx.Tests.Common.Helpers;
using Esx.Web.Common;
using Microsoft.Practices.ServiceLocation;
using Moq;
using NUnit.Framework;

namespace Esx.Web.Tests.Common
{
    public abstract class SecurityContextMvcFilterTests
    {

        private SecurityContextMvcFilter _filter;
        private Mock<ISecurityContext> _securityContextMock;

        private Mock<HttpContextBase> _contextBase;
        private Mock<ActionExecutingContext> _actionExecutingContext;
        private string _username ;

        [SetUp]
        public void Setup()
        {
            _filter = new SecurityContextMvcFilter();

            _securityContextMock = new Mock<ISecurityContext>();
            var mockServiceLocator = new Mock<IServiceLocator>();
            mockServiceLocator.Setup(x => x.GetInstance<ISecurityContext>()).Returns(_securityContextMock.Object);
            ServiceLocator.SetLocatorProvider(new ServiceLocatorProvider(() => mockServiceLocator.Object));

            _username = RandomData.GetString(10);

            //Setup fake action executiing context
            _contextBase = new Mock<HttpContextBase>();
            _contextBase.Setup(s => s.User).Returns(new GenericPrincipal(new GenericIdentity(_username, ""), new string[0]));

            _actionExecutingContext = new Mock<ActionExecutingContext>();
            _actionExecutingContext.SetupGet(c => c.HttpContext).Returns(_contextBase.Object);

        }

        public sealed class OnActionExecuting : SecurityContextMvcFilterTests
        {

            [Test]
            [Ignore]// TODO: Find out why this is failing. Cannot find assembly System.Web.WebPages?
            public void Should_prepare_securityContext()
            {
                //arrange
                _securityContextMock.Setup(m => m.PrepareContext(_username, false));

                //act
                _filter.OnActionExecuting(_actionExecutingContext.Object);

                //assert    
                _securityContextMock.VerifyAll();

            }

        }
    }
}

﻿using AutoMapper;
using NUnit.Framework;

namespace Esx.Web.Tests.Common
{
    public abstract class AutoMapperTests
    {
        [SetUp]
        public void TestInit()
        {
            // Configure mappings
            Mapper.AddProfile(new AutoMapperConfiguration());
        }

        [TearDown]
        public void TestDispose()
        {
            // Clear mappings
            Mapper.Reset();
        }

        public sealed class Configure : AutoMapperTests
        {

            [Test]
            public void Configuration_Should_Be_Valid()
            {
                //assert
                Mapper.AssertConfigurationIsValid();
            }
        }
    }
}

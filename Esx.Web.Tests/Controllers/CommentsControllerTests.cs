﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Esx.Business.Comments;
using Esx.Contracts.Models.Comment;
using Esx.Contracts.Paging;
using Esx.Tests.Common.Helpers;
using Esx.Web.Controllers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Web.Tests.Controllers
{
    public class CommentsControllerTests
    {
        private Mock<ICommentsService> _commentsServiceMock;
        private CommentsController _controller;

        [SetUp]
        public void TestInit()
        {
            _commentsServiceMock = new Mock<ICommentsService>();
            _controller = new CommentsController(_commentsServiceMock.Object);
        }

        [TearDown]
        public void TestDispose()
        {
        }

        public sealed class ListMethod : CommentsControllerTests
        {
            [Test]
            public void Should_Get_List_Of_Comments()
            {
                var mentionId = "@SomeCompany";

                var comments = new List<CommentListItem>
                {
                    new CommentListItem
                    {
                        Comment_Key = 1234,
                        PrimaryMentionID = "@AnotherCompany",
                        CommentText = "This is a #testTag1 in @SomeCompany"
                    }
                };

                var expected = new PagedResources<CommentListItem>
                {
                    PagingData = new PagingData
                    {
                        PageSize = 10,
                        Offset = 0
                    },
                    Resources = comments
                };
                

                _commentsServiceMock.Setup(s => s.GetComments(It.IsAny<string>(), 10, 0)).Returns(comments);

                // act
                var actual = _controller.List(mentionId);

                // assert
                actual.Resources.Should().Have.SameSequenceAs(comments);
                _commentsServiceMock.VerifyAll();
            }
        }

        public sealed class MentionedMethod : CommentsControllerTests
        {
            [Test]
            public void Should_get_list_of_comments_with_a_mentionId()
            {
                var mentionId = RandomData.GetString(10);
                var expectedPageSize = RandomData.GetInt(1, 10);
                var expectedOffset = RandomData.GetInt(1, 10);
                var filterText = RandomData.GetString(10);

                var comments = new List<CommentListItem>
                {
                    new CommentListItem
                    {
                        Comment_Key = RandomData.GetInt(0,100),
                        PrimaryMentionID = RandomData.GetString(10),
                        CommentText = RandomData.GetString(100) + mentionId + filterText
                    }
                };

                var resources = new PagedResources<CommentListItem>
                {
                    PagingData = new PagingData(),
                    Resources = comments
                };
                _commentsServiceMock.Setup(
                    s =>
                        s.GetMentionComments2(It.IsAny<PagingData>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<string>())).Returns(resources);

                // act
                //var actual = _controller.Mentioned(mentionId, expectedPageSize, expectedOffset, filterText);
                var actual = _controller.Mentioned(10, 0, null, false, null);

                // assert
                actual.Resources.Should().Have.SameSequenceAs(comments);
                _commentsServiceMock.VerifyAll();
            }

            [Test]
            public void Should_get_list_of_comments_using_default_paging()
            {
                var mentionId = RandomData.GetString(10);

                var comments = new List<CommentListItem>
                {
                    new CommentListItem()
                };

                var resources = new PagedResources<CommentListItem>
                {
                    PagingData = new PagingData(),
                    Resources = comments
                };
                _commentsServiceMock.Setup(
                    s =>
                        s.GetMentionComments2(It.IsAny<PagingData>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<string>())).Returns(resources);

                // act
                var actual = _controller.Mentioned(10, 0, null, false, null);

                // assert
                actual.Resources.Should().Have.SameSequenceAs(comments);
                _commentsServiceMock.VerifyAll();
            }
        }

        public sealed class AddMethod : CommentsControllerTests
        {
            [Test]
            public void Should_Add_Comment()
            {
                var tags = new List<string> { "#aTag01", "#aTag02" };
                var mentions = new List<string> { "@SomeEmployee" };

                var comment = new Comment()
                {
                    PrimaryMentionID = "@AnotherCompany",
                    CommentText = "This #aTag01 is a test #aTag02.",
                    Tags = tags,
                    Mentions = mentions
                };

                _commentsServiceMock.Setup(s => s.SaveComment(comment));

                _controller.Request = new HttpRequestMessage();
                _controller.Request.SetConfiguration(new HttpConfiguration());

                // act
                var actual = _controller.Save(comment);

                // assert
                actual.Satisfies(c => c.StatusCode == System.Net.HttpStatusCode.Created);
            }
        }
}
}

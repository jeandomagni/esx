﻿using System.Collections.Generic;
using System.Linq;
using Esx.Business.Deals;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Models.Property;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;
using Esx.Tests.Common.Helpers;
using Esx.Web.Controllers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;
using Esx.Business.Properties;

namespace Esx.Web.Tests.Controllers
{
    public abstract class DealsControllerTests
    {
        private Mock<IDealsService> _dealsServiceMock;
        private Mock<IPropertiesService> _propertiesServiceMock;
        private DealsController _controller;

        [SetUp]
        public void TestInit()
        {
            _dealsServiceMock = new Mock<IDealsService>();
            _propertiesServiceMock = new Mock<IPropertiesService>();
            _controller = new DealsController(_dealsServiceMock.Object, _propertiesServiceMock.Object);
        }

        public sealed class GetSummaryMethod : DealsControllerTests
        {
            [Test]
            public void Should_Get_Deal_Summary()
            {
                //arrange
                _dealsServiceMock.Setup(x => x.GetDealSummary(It.IsAny<string>())).Returns(new DealSummary
                {
                    DealName = "huge deal"
                });

                //act
                var actual = _controller.GetDealSummary("@mention");

                //assert
                actual.DealName.Satisfies(p => p == "huge deal");
            }
        }
    }
}

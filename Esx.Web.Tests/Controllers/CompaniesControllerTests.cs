﻿using System.Collections.Generic;
using AutoMapper;
using Esx.Business.Companies;
using Esx.Business.Contacts;
using Esx.Business.Deals;
using Esx.Business.Loans;
using Esx.Business.Properties;
using Esx.Contracts.Models.Company;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Models.Location;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;
using Esx.Tests.Common.Helpers;
using Esx.Web.Controllers;
using Esx.Web.ViewModels;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Web.Tests.Controllers
{
    public abstract class CompaniesControllerTests
    {
        private Mock<ICompaniesService> _companiesServiceMock;
        private Mock<IContactsService> _contactsServiceMock;
        private Mock<IPropertiesService> _propertiesServiceMock;
        private Mock<IDealsService> _dealsServiceMock;
        private Mock<ILoansService> _loansServiceMock;

        private CompaniesController _controller;

        [SetUp]
        public void TestInit()
        {
            // Configure mappings
            Mapper.AddProfile(new AutoMapperConfiguration());

            _companiesServiceMock = new Mock<ICompaniesService>();
            _contactsServiceMock = new Mock<IContactsService>();
            _propertiesServiceMock = new Mock<IPropertiesService>();
            _dealsServiceMock = new Mock<IDealsService>();
            _loansServiceMock = new Mock<ILoansService>();

            _controller = new CompaniesController(_companiesServiceMock.Object, _contactsServiceMock.Object,
                _propertiesServiceMock.Object, _loansServiceMock.Object, _dealsServiceMock.Object, Mapper.Engine);
        }

        [TearDown]
        public void TestDispose()
        {
            // Clear mappings
            Mapper.Reset();
        }

        public sealed class GetCompanyNameMethod : CompaniesControllerTests
        {
            [Test]
            public void Should_Get_Company_Name()
            {
                //arrange
                var companyId = "@test";
                var expectedName = "My Company";
                _companiesServiceMock.Setup(x => x.GetCompanyName(It.Is<string>(id => id == companyId))).Returns(expectedName);

                //act
                var actual = _controller.GetCompanyName(companyId).Data;

                //assert
                actual.Satisfies(s => s == expectedName);
            }
        }

        public sealed class GetCompanyDeals : CompaniesControllerTests
        {
            [Test]
            public void Should_Get_Company_Deals()
            {
                var companyMentionId = RandomData.GetString(10);

                //arrange
                var companyDeals = new List<DealListItem>
                {
                    new DealListItem
                    {
                        DealName = RandomData.GetString(10)
                    }
                };

                var expected = new PagedResources<DealListItem>
                {
                    Resources = companyDeals,
                    PagingData = new PagingData()
                };

                _dealsServiceMock.Setup(s => s.Search(It.IsAny<DealSearchCriteria>(), null)).Returns(expected);

                //act
                var actual = _controller.GetDeals(companyMentionId);

                //assert
                actual.Resources.Should().Have.SameSequenceAs(companyDeals);
                _companiesServiceMock.VerifyAll();
            }

            [Test]
            public void Should_Call_deals_service_and_return_with_expected_paging_data()
            {

                //arrange
                var companyMentionId = RandomData.GetString(10);
                var deals = new List<DealListItem>
                {
                    new DealListItem
                    {
                        DealName = RandomData.GetString(10)
                    }
                };

                var expectedPagingData = new DealSearchCriteria
                {
                    PageSize = 20,
                    Offset = 100
                };

                var expected = new PagedResources<DealListItem>
                {
                    Resources = deals,
                    PagingData = expectedPagingData
                };

                _dealsServiceMock.Setup(
                    s => s.Search(It.Is<DealSearchCriteria>(p => p.PageSize == expectedPagingData.PageSize &&
                                                                  p.Offset == expectedPagingData.Offset), null))
                    .Returns(expected);


                //act
                var actual = _controller.GetDeals(companyMentionId, expectedPagingData.PageSize, expectedPagingData.Offset);

                //assert    
                actual.Satisfies(a => a.PagingData.Offset == expectedPagingData.Offset &&
                                      a.PagingData.PageSize == expectedPagingData.PageSize);
                _companiesServiceMock.VerifyAll();

            }
        }

        public sealed class ListMethod : CompaniesControllerTests
        {

            [Test]
            public void Should_Get_List_Of_Companies()
            {

                //arrange
                ICollection<CompanyListItem> companies = new List<CompanyListItem>
                {
                    new CompanyListItem
                    {
                        CanonicalCompany = RandomData.GetString(10)
                    }
                };
                var pagingData = new PagingData();

                var expected = new PagedResources<CompanyListItem>
                {
                    Resources = companies,
                    PagingData = pagingData
                };

                _companiesServiceMock.Setup(s => s.Search(It.IsAny<CompanySearchCriteria>())).Returns(expected);

                //act
                var actual = _controller.List();

                //assert
                actual.Resources.Should().Have.SameSequenceAs(companies);
                _companiesServiceMock.VerifyAll();
            }

            [Test]
            public void Should_call_companies_service_and_return_with_expected_paging_data()
            {

                //arrange
                var companies = new List<CompanyListItem>
                {
                    new CompanyListItem
                    {
                        CanonicalCompany = RandomData.GetString(10)
                    }
                };

                var expectedPagingData = new PagingData
                {
                    PageSize = 20,
                    Offset = 100
                };

                var expected = new PagedResources<CompanyListItem>
                {
                    Resources = companies,
                    PagingData = expectedPagingData
                };

                _companiesServiceMock.Setup(
                    s => s.Search(It.Is<CompanySearchCriteria>(p => p.PageSize == expectedPagingData.PageSize &&
                                                                  p.Offset == expectedPagingData.Offset)))
                    .Returns(expected);


                //act
                var actual = _controller.List(expectedPagingData.PageSize, expectedPagingData.Offset);

                //assert    
                actual.Satisfies(a => a.PagingData.Offset == expectedPagingData.Offset &&
                                      a.PagingData.PageSize == expectedPagingData.PageSize);
                _companiesServiceMock.VerifyAll();


            }
        }

        public sealed class SummaryMethod : CompaniesControllerTests
        {
            [Test]
            public void Should_Get_Company_Summary_By_MentionID()
            {
                // Arrange
                var expected = new CompanyStatistics { ActiveDeals = 1, BidVolume = "1M", Investments = 2, Locations = 3 };
                _companiesServiceMock.Setup(s => s.GetCompanyStatistics(It.IsAny<string>())).Returns(expected);

                // Act
                var actual = _controller.Statistics("some mention id");


                // Assert
                actual.Satisfies(a =>
                    a.ActiveDeals == expected.ActiveDeals
                    && a.BidVolume == expected.BidVolume
                    && a.Investments == expected.Investments
                    && a.Locations == expected.Locations);
            }
        }

        public sealed class Intellisense : CompaniesControllerTests
        {
            [Test]
            public void Should_call_intellisense_service_method()
            {
                // Arrange
                var query = RandomData.GetString(10);
                var count = RandomData.GetInt(1, 10);
                var expectedList = new List<CompanyIntellisense>
                {
                     new CompanyIntellisense
                    {
                        MentionID = RandomData.GetString(10),
                        AliasName = RandomData.GetString(10),
                        Relevance = RandomData.GetInt(0,100)
                    },
                    new CompanyIntellisense(),
                    new CompanyIntellisense(),
                };
                _companiesServiceMock.Setup(
                   x => x.CompanyIntellisense(It.Is<string>(m => m == query), It.Is<int>(i => i == count))).Returns(
                   expectedList
                   );

                // Act
                var acutal = _controller.Intellisense(query, count);

                //assert
                acutal.Should().Have.SameValuesAs(expectedList);
                _companiesServiceMock.VerifyAll();
            }

            [Test]
            public void Should_call_intellisense_service_method_with_default_count()
            {
                // Arrange
                const int defaultCount = 10;
                _companiesServiceMock.Setup(
                   x => x.CompanyIntellisense(It.IsAny<string>(), It.Is<int>(i => i == defaultCount)))
                   .Returns(new List<CompanyIntellisense>());

                // Act
                _controller.Intellisense(RandomData.GetString(10));

                //assert
                _companiesServiceMock.VerifyAll();
            }
        }

        public sealed class Add : CompaniesControllerTests
        {
            [Test]
            public void Should_Call_AddCompany_Service_Method()
            {
                // Arrange
                var model = new AddCompanyViewModel
                {
                    CompanyName = "Test"
                };
                _companiesServiceMock.Setup(x => x.AddCompany(It.IsAny<CompanyAdd>())).Returns(new CompanyAdd());

                // Act
                _controller.Add(model);

                //
                _companiesServiceMock.VerifyAll();
            }
        }

        public sealed class GetCompanyDetailsMethod : CompaniesControllerTests
        {
            [Test]
            public void Should_Get_Contacts_For_Company()
            {
                //arrange
                var details = new CompanyDetails
                {
                    MentionId = RandomData.GetString(15),
                    CompanyKey = RandomData.GetInt(),
                    CompanyName = RandomData.GetString(20),
                    Office = new Office
                    {
                        OfficeKey = RandomData.GetInt(),
                        OfficeName = RandomData.GetString(20),
                        IsHeadQuarters = true,
                        PhysicalAddress = new PhysicalAddress
                        {
                            PhysicalAddressKey = RandomData.GetInt(),
                            Address1 = RandomData.GetString(20),
                            City = RandomData.GetString(10)
                        }
                    },
                    IsSolicitable = false,
                    IsDefunct = false,
                    FcpaName = RandomData.GetString(5)
                };

                _companiesServiceMock.Setup(x => x.GetCompanyDetails(It.IsAny<string>())).Returns(details);

                //act
                var actual = _controller.GetCompanyDetails(RandomData.GetString(15));

                //assert
                actual.Should().Not.Be.Null();
                actual.CompanyName.Should().Be.EqualTo(details.CompanyName);
                actual.Office.Should().Be.EqualTo(details.Office);
                actual.FcpaName.Should().Be.EqualTo(details.FcpaName);
            }
        }
    }
}

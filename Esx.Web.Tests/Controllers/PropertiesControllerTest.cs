﻿using AutoMapper;
using Esx.Contracts.ExceptionHandling;
using Esx.Tests.Common.Helpers;
using Esx.Web.Controllers;
using Esx.Web.ViewModels;
using Moq;
using NUnit.Framework;
using SharpTestsEx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using Esx.Business.Companies;
using Esx.Business.Deals;
using Esx.Business.Loans;
using Esx.Business.Properties;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Models.Loan;
using Esx.Contracts.Models.Property;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;
using Esx.Web.Common;

namespace Esx.Web.Tests.Controllers
{
    public abstract class PropertiesControllerTests
    {
        private Mock<IPropertiesService> _propertiesServiceMock;
        private Mock<ICompaniesService> _companiesServiceMock;
        private Mock<IDealsService> _dealsServiceMock;
        private Mock<ILoansService> _loansServiceMock;

        private PropertiesController _controller;

        [SetUp]
        public void TestInit()
        {
            // Configure mappings
            Mapper.AddProfile(new AutoMapperConfiguration());

            _propertiesServiceMock = new Mock<IPropertiesService>();
            _companiesServiceMock = new Mock<ICompaniesService>();
            _dealsServiceMock = new Mock<IDealsService>();
            _loansServiceMock = new Mock<ILoansService>();

            _controller = new PropertiesController(_loansServiceMock.Object, _propertiesServiceMock.Object,
                _companiesServiceMock.Object, _dealsServiceMock.Object, Mapper.Engine);
        }

        [TearDown]
        public void TestDispose()
        {
            // Clear mappings
            Mapper.Reset();
        }

        public sealed class ListMethod : PropertiesControllerTests
        {

            [Test]
            public void Should_Get_List_Of_Properties()
            {

                //arrange
                var properties = new List<PropertyListItem>
                {
                    new PropertyListItem
                    {
                        PropertyName = RandomData.GetString(10)
                    }
                };
                var pagingData = new PagingData();

                var expected = new PagedResources<PropertyListItem>
                {
                    Resources = properties,
                    PagingData = pagingData
                };

                _propertiesServiceMock.Setup(s => s.Search(It.IsAny<PropertySearchCriteria>())).Returns(expected);

                //act
                var actual = _controller.List();

                //assert
                actual.Resources.Should().Have.SameSequenceAs(properties);
                _propertiesServiceMock.VerifyAll();
            }

            [Test]
            public void Should_call_properties_service_with_expected_paging_data()
            {

                //arrange
                var properties = new List<PropertyListItem>
                {
                    new PropertyListItem
                    {
                        PropertyName = RandomData.GetString(10)
                    }
                };

                var expectedPagingData = new PagingData
                {
                    PageSize = 20,
                    Offset = 100
                };

                var expected = new PagedResources<PropertyListItem>
                {
                    Resources = properties,
                    PagingData = expectedPagingData
                };

                _propertiesServiceMock.Setup(
                    s => s.Search(It.Is<PropertySearchCriteria>(p => p.PageSize == expectedPagingData.PageSize &&
                                                                  p.Offset == expectedPagingData.Offset)))
                    .Returns(expected);


                //act
                var actual = _controller.List(expectedPagingData.PageSize, expectedPagingData.Offset);

                //assert    
                actual.Satisfies(a => a.PagingData.Offset == expectedPagingData.Offset &&
                                      a.PagingData.PageSize == expectedPagingData.PageSize);

            }
        }

        public sealed class GetLoansMethod: PropertiesControllerTests
        {
            [Test]
            public void Should_Get_Loans_For_Property()
            {
                //arrange
                var loans = new List<LoanListItem>()
                {
                    new LoanListItem { LoanKey = RandomData.GetInt(1, 1000) },
                    new LoanListItem { LoanKey = RandomData.GetInt(1, 1000) }
                };

                _loansServiceMock.Setup(x => x.Search(It.IsAny<LoanSearchCriteria>()))
                        .Returns(new PagedResources<LoanListItem>()
                        {
                            Resources = loans.ToList(),
                            PagingData = new PagingData()
                        });

                //act
                var actual = _controller.GetLoans("@mention", 10, 0);

                //assert
                actual.Resources.Should().Have.SameSequenceAs(loans);
            }
        }

        public sealed class GetDeals2Method : PropertiesControllerTests
        {
            [Test]
            public void Should_Get_Deals_For_Property()
            {
                //arrange
                var deals = new List<DealListItem>()
                {
                    new DealListItem { DealID = RandomData.GetInt(1, 1000) },
                    new DealListItem { DealID = RandomData.GetInt(1, 1000) }
                };

                _dealsServiceMock.Setup(x => x.Search(It.IsAny<DealSearchCriteria>(), null))
                        .Returns(new PagedResources<DealListItem>()
                        {
                            Resources = deals.ToList(),
                            PagingData = new PagingData()
                        });

                //act
                var actual = _controller.GetDeals("@mention", 10, 0);

                //assert
                actual.Resources.Should().Have.SameSequenceAs(deals);
            }
        }

        public sealed class GetDetailsMethod : PropertiesControllerTests
        {
            [Test]
            public void Should_Get_Details_For_Property()
            {
                //arrange
                _propertiesServiceMock.Setup(x => x.GetPropertyDetails(It.IsAny<string>())).Returns(new PropertyDetails
                {
                    PropertyKey = 1,
                    PropertyName = "Test"
                });

                //act
                var actual = _controller.GetPropertyDetails("@mention");

                //assert
                actual.PropertyName.Satisfies(p => p == "Test");
            }
        }

        public sealed class GetValuationsMethod : PropertiesControllerTests
        {
            [Test]
            public void Should_Get_Valuations_For_Property()
            {
                //arrange
                var valuations = new List<ValuationDetails>
                {
                    new ValuationDetails
                    {
                        AskingPrice = 1000
                    }
                };
                _propertiesServiceMock.Setup(x => x.GetPropertyValuations(It.IsAny<PagingData>(), It.IsAny<string>()))
                    .Returns(new PagedResources<ValuationDetails> { Resources = valuations });

                //act
                var actual = _controller.GetPropertyValuations("@mention");

                //assert
                actual.Resources.Should().Have.SameSequenceAs(valuations);
            }
        }

        public sealed class Intellisense : PropertiesControllerTests
        {
            [Test]
            public void Should_call_intellisense_service_method()
            {
                // Arrange
                var query = RandomData.GetString(10);
                var expectedList = new List<PropertyIntellisense>
                {
                     new PropertyIntellisense
                    {
                        PropertyKey = RandomData.GetInt(),
                        PropertyName = RandomData.GetString(10)
                    },
                    new PropertyIntellisense(),
                    new PropertyIntellisense(),
                };

                _propertiesServiceMock.Setup(x => x.PropertyIntellisense(It.Is<string>(m => m == query))).Returns(expectedList);

                // Act
                var acutal = _controller.Intellisense(query);

                //assert
                acutal.Should().Have.SameValuesAs(expectedList);
                _companiesServiceMock.VerifyAll();
            }

            [Test]
            public void Should_call_intellisense_service_method_with_default_count()
            {
                // Arrange
                _propertiesServiceMock.Setup(x => x.PropertyIntellisense(It.IsAny<string>())).Returns(new List<PropertyIntellisense>());

                // Act
                _controller.Intellisense(RandomData.GetString(10));

                //assert
                _companiesServiceMock.VerifyAll();
            }
        }
    }
}

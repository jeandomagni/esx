﻿using System.Collections.Generic;
using Esx.Business.Comments;
using Esx.Business.Mentions;
using Esx.Contracts.Models.Mention;
using Esx.Tests.Common.Helpers;
using Esx.Web.Controllers;
using Esx.Web.ViewModels;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Web.Tests.Controllers
{
    public abstract class MentionsControllerTests
    {
        private Mock<IMentionsService> _mentionsServiceMock;
        private Mock<ICommentsService> _commentsServiceMock;
        private MentionsController _controller;

        [SetUp]
        public void Init()
        {
            _mentionsServiceMock = new Mock<IMentionsService>();
            _commentsServiceMock = new Mock<ICommentsService>();
            _controller = new MentionsController(_mentionsServiceMock.Object, _commentsServiceMock.Object);
        }

        public sealed class SearchMethod : MentionsControllerTests
        {
            [Test]
            public void Should_Get_Mentions_With_Intellisense_Query()
            {
                //arrange
                string query = RandomData.GetString(10);
                int count = RandomData.GetInt(0,100);

                var expectedMentions = new List<Mention>
                {
                    new Mention {MentionID = RandomData.GetString(10)},
                    new Mention {MentionID = RandomData.GetString(10)},
                    new Mention {MentionID = RandomData.GetString(10)}
                };
                _mentionsServiceMock.Setup(m => m.GetMentions(query, count)).Returns(expectedMentions);

                //act
                var results = _controller.Get(query,count);

                //assert
                results.Resources.Should().Have.SameSequenceAs(expectedMentions);
                results.PagingData.Satisfies(pd => pd.PageSize == count &&
                                                   pd.TotalCount == expectedMentions.Count);
                _mentionsServiceMock.VerifyAll();
            }
        }

        public sealed class SetIsWatchedMethod : MentionsControllerTests
        {
            [Test]
            public void Should_Call_SetIsWatched_Service_Method()
            {
                // Arrange
                var model = new IsWatchedViewModel
                {
                    MentionId = "@test",
                    IsWatched = true
                };
                _mentionsServiceMock.Setup(x => x.SetIsWatched(It.Is<string>(m => m == model.MentionId), It.Is<bool>(b => b == model.IsWatched))).Verifiable();

                // Act
                _controller.SetIsWatched(model);

                //
                _mentionsServiceMock.VerifyAll();
            }
        }
    }
}

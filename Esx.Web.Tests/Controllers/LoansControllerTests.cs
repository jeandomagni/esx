﻿using Esx.Business.Loans;
using Esx.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace Esx.Web.Tests.Controllers
{
    public abstract class LoansControllerTests
    {
        private Mock<ILoansService> _loansServiceMock;

        private LoansController _controller;

        [SetUp]
        public void TestInit()
        {
            _loansServiceMock = new Mock<ILoansService>();

            _controller = new LoansController(_loansServiceMock.Object);
        }
    }
}

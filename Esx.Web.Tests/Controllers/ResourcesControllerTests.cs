﻿using System;
using System.Collections.Generic;
using System.Linq;
using Esx.Business.Resources;
using Esx.Business.Tags;
using Esx.Business.Users;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Tag;
using Esx.Contracts.Models.User;
using Esx.Tests.Common.Helpers;
using Esx.Web.Controllers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Web.Tests.Controllers
{
    public class ResourcesControllerTests
    {
        private Mock<IResourcesService> _resourcesServiceMock;
        private Mock<IUsersService> _usersServiceMock;
        private ResourcesController _controller;

        [SetUp]
        public void TestInt()
        {
            _resourcesServiceMock = new Mock<IResourcesService>();
            _usersServiceMock = new Mock<IUsersService>();

            _controller = new ResourcesController(_resourcesServiceMock.Object, _usersServiceMock.Object);
        }

        [TearDown]
        public void TestDispose()
        {
        }

        public sealed class ListMethods : ResourcesControllerTests
        {
            [Test]
            public void Should_Get_List_Of_StateProvinces()
            {
               //arrange
               var state = "MN";
               List<StateProvinceListItem> items = new List<StateProvinceListItem>
               {
                   new StateProvinceListItem
                   {
                       StateProvinceCode = state
                   }
               };

                _resourcesServiceMock.Setup(s => s.GetStateProvinceList()).Returns(items);

                //act
                var actual = _controller.GetStateProvinceList();

                //assert
                actual.Should().Have.Count.EqualTo(1);
                actual.First().StateProvinceCode.Should().Be(state);
                _resourcesServiceMock.VerifyAll();
            }

            [Test]
            public void Should_Get_List_Of_Countries()
            {
                //arrange
                var country = "United States";
                List<CountryListItem> items = new List<CountryListItem>
               {
                   new CountryListItem
                   {
                       CountryName = country
                   }
               };

                _resourcesServiceMock.Setup(s => s.GetCountryList()).Returns(items);

                //act
                var actual = _controller.GetCountryList();

                //assert
                actual.Should().Have.Count.EqualTo(1);
                actual.First().CountryName.Should().Be(country);
                _resourcesServiceMock.VerifyAll();
            }

            [Test]
            public void Should_Get_List_Of_UserPreferences()
            {
                //arrange                
                List<UserPreferenceListItem> items = new List<UserPreferenceListItem>
               {
                   new UserPreferenceListItem
                   {
                       PreferenceKey = "Country",
                       PreferenceValue = "United States"
                   }
               };

                _usersServiceMock.Setup(s => s.GetUserPreferences()).Returns(items);

                //act
                var actual = _controller.GetUserPreferences();

                //assert
                actual.Should().Have.Count.EqualTo(1);
                actual.First().PreferenceValue.Should().Be("United States");
                _resourcesServiceMock.VerifyAll();
            }
        }
    }
}

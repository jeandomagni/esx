﻿using System;
using System.Collections.Generic;
using System.Linq;
using Esx.Business.Tags;
using Esx.Contracts.Models.Tag;
using Esx.Tests.Common.Helpers;
using Esx.Web.Controllers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Web.Tests.Controllers
{
    public class TagsControllerTests
    {
        private Mock<ITagsService> _tagsServiceMock;
        private TagsController _controller;

        [SetUp]
        public void TestInt()
        {
            _tagsServiceMock = new Mock<ITagsService>();

            _controller = new TagsController(_tagsServiceMock.Object);
        }

        [TearDown]
        public void TestDispose()
        {
        }

        public sealed class ListMethod : TagsControllerTests
        {
            [Test]
            public void Should_Get_List_Of_Tags()
            {
                var tagName = "#abc";
                var tags = new List<Tag>
                {
                    new Tag
                    {
                        Relevance = RandomData.GetInt(1, 1),
                        TagName = "#abcdefg"
                    }
                };

                _tagsServiceMock.Setup(s => s.GetTags(It.IsAny<string>(), 10)).Returns(tags);

                // act
                var actual = _controller.List(tagName);


                //assert
                actual.Should().Have.Count.EqualTo(1);
                actual.First().TagName.Should().Contain(tagName);
                _tagsServiceMock.VerifyAll();
            }

            [Test]
            public void Should_Get_List_Of_Tag_Notes()
            {
                var tagName = "abc";
                var tagNotes = new List<TagNote>
                {
                    new TagNote
                    {
                        CommentID = 1,
                        CreateTime = DateTime.Now.AddDays(-2),
                        UpdateTime = DateTime.Now.AddDays(-1),
                        CommentText = "This is a test #abc note.",
                    }
                };

                _tagsServiceMock.Setup(s => s.GetTagNotes(It.IsAny<string>(), 10, 0)).Returns(tagNotes);

                // act
                var actual = _controller.GetTagNotes(tagName);


                //assert
                actual.Should().Have.Count.EqualTo(1);
                actual.First().CommentText.Should().Contain(tagName);
                _tagsServiceMock.VerifyAll();
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using Esx.Business.Contacts;
using Esx.Contracts.Models.Contact;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;
using Esx.Tests.Common.Helpers;
using Esx.Web.Controllers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Web.Tests.Controllers
{
    public abstract class ContactsControllerTests
    {
        private Mock<IContactsService> _contactsServiceMock;
        private ContactsController _controller;

        [SetUp]
        public void TestInit()
        {
            _contactsServiceMock = new Mock<IContactsService>();
            _controller = new ContactsController(_contactsServiceMock.Object);
        }

        [TearDown]
        public void TestDispose()
        {
        }

        public sealed class ListMethod : ContactsControllerTests
        {

            [Test]
            public void Should_Get_List_Of_Contacts()
            {

                //arrange
                var contacts = new List<ContactListItem>
                {
                    new ContactListItem
                    {
                        FirstName = RandomData.GetString(10)
                    }
                };
                var criteria = new ContactSearchCriteria();

                var expected = new PagedResources<ContactListItem>
                {
                    Resources = contacts,
                    PagingData = criteria
                };

                _contactsServiceMock.Setup(s => s.Search(It.IsAny<ContactSearchCriteria>())).Returns(expected);

                //act
                var actual = _controller.List();

                //assert
                actual.Resources.Should().Have.SameSequenceAs(contacts);
                _contactsServiceMock.VerifyAll();
            }

            [Test]
            public void Should_call_contacts_service_with_expected_paging_data_and_search_text()
            {

                //arrange
                const string columnName = "FullName";
                const string searchText = "Some Name to Search";
                var expectedPagingData = new ContactSearchCriteria
                {
                    SearchText = searchText,
                    PageSize = 20,
                    Offset = 100,
                    Sort = columnName,
                    SortDescending = true,
                };

                var expected = new PagedResources<ContactListItem>
                {
                    PagingData = expectedPagingData
                };

                _contactsServiceMock.Setup(
                    s => s.Search(It.Is<ContactSearchCriteria>(p => p.SearchText == searchText &&
                                                                  p.PageSize == expectedPagingData.PageSize &&
                                                                  p.Offset == expectedPagingData.Offset &&
                                                                  p.Sort == columnName &&
                                                                  p.SortDescending)))
                    .Returns(expected);

                //act
                _controller.List(expectedPagingData.PageSize, expectedPagingData.Offset, columnName, true, searchText);

                //assert    
                _contactsServiceMock.VerifyAll();

            }
        }

        public sealed class GetMethod : ContactsControllerTests
        {
            [Test]
            public void Should_Get_Contact()
            {
                //arrange
                int contactId = RandomData.GetInt(1, 1000);
                var expectedContactDetails = new ContactDetails()
                {
                    ContactId = contactId,
                    FirstName = RandomData.GetString(10),
                    Address = RandomData.GetString(10),
                    CompanyName = RandomData.GetString(10)

                };

                _contactsServiceMock.Setup(
                    x => x.GetContactById(It.Is<int>(id => id == contactId))).Returns(expectedContactDetails);

                //act
                var actual = _controller.Get(contactId);

                //assert
                actual.Satisfies(a => a.FirstName == expectedContactDetails.FirstName &&
                a.ContactId == expectedContactDetails.ContactId &&
                a.Address == expectedContactDetails.Address &&
                a.CompanyName == expectedContactDetails.CompanyName
                );
                _contactsServiceMock.VerifyAll();
            }
        }

        public sealed class SearchMethod : ContactsControllerTests
        {
            [Test]
            public void Should_Get_Search_Results()
            {
                //arrange
                string query = RandomData.GetString(10);
                var contacts = new PagedResources< ContactListItem>
                {
                    Resources = new List<ContactListItem>
                    {
                        new ContactListItem
                        {
                            FullName = RandomData.GetString(10) + query,
                            EmailAddress = ""
                        },
                        new ContactListItem
                        {
                            FullName = RandomData.GetString(10) + query,
                            EmailAddress = ""
                        }
                    }
                };
                var pagingData = new PagingData();

                _contactsServiceMock.Setup(s => s.Search(It.IsAny<ContactSearchCriteria>())).Returns(contacts);

                //act
                var actual = _controller.Search(query, pagingData.PageSize, pagingData.Offset);

                //assert
                actual.Resources.Should().Have.Count.EqualTo(2);
                actual.Resources.First().FullName.Should().Be(contacts.Resources.First().FullName);
                _contactsServiceMock.VerifyAll();
            }
        }
        
    }
}
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/02/22
-- Description: Deletes all data in all tables in the passed schema and reseeds any identity columns.
-- =============================================
CREATE PROCEDURE [Global].[DeleteAllSchemaData]
    @pSchemaName sysname
   ,@pTest bit = 0
AS
/*
    DECLARE @pSchemaName sysname = 'dExcelETL';
    DECLARE @pTest bit          = 1;
--*/
    SET NOCOUNT ON;
    DECLARE @CrLf char(2) = CHAR(13)+CHAR(10);
	DECLARE @SchemaID int = SCHEMA_ID(@pSchemaName);
    DECLARE @Cmds nvarchar(MAX) = '--'+@CrLf;

    -- Trying to add a row to each table before reseeding is a workaround for a bug that makes reseed
    -- behave incorrectly on tables that have never had a row inserted.
	-- Since the operation can actually result in a row being added it's easiest to do it before the deletes which will clean them up.
    SELECT @Cmds = @Cmds
            +'BEGIN TRY SET IDENTITY_INSERT '+@pSchemaName+'.'+QUOTENAME(st.name)+' ON; INSERT '-- use try/catch to suppress expected errors
            +@pSchemaName+'.'+QUOTENAME(st.name)+'('+QUOTENAME(sc.name)+') VALUES (1); END TRY BEGIN CATCH END CATCH; SET IDENTITY_INSERT '
            +@pSchemaName+'.'+QUOTENAME(st.name)+' OFF; '+@CrLf
      FROM sys.tables st
      JOIN sys.columns sc
        ON sc.object_id = st.object_id
       AND sc.is_identity = 1
     WHERE st.schema_id = @SchemaID;
	 
    -- Build structure to determine delete table content order to avoid constraint issues
    SELECT DISTINCT
           RefFrom.object_id                    AS FromID
          ,RefFrom.name                         AS FromTable
          ,RefTo.object_id                      AS ToID
          ,RefTo.name                           AS ToName
          ,CASE WHEN RefTo.object_id IS NULL 
                THEN 10 
                ELSE NULL 
           END                                  AS GroupNumber
      INTO #TableToOrder
      FROM sys.tables RefFrom
      LEFT JOIN sys.foreign_keys FK
           JOIN sys.objects RefTo
             ON RefTo.object_id = FK.referenced_object_id
        ON FK.parent_object_id = RefFrom.object_id
       AND RefFrom.object_id <> RefTo.object_id
     WHERE RefFrom.schema_id = @SchemaID;
    
    -- Calculate delete order
    SET ANSI_WARNINGS OFF;
    WHILE EXISTS(SELECT NULL FROM #TableToOrder WHERE GroupNumber IS NULL)
    UPDATE t SET GroupNumber =  
                CASE WHEN EXISTS(SELECT X.FromID ,*
                                   FROM #TableToOrder AS X
                                   JOIN #TableToOrder AS Y
                                     ON Y.FromID = X.ToID 
                                  WHERE Y.GroupNumber IS NULL
                                    AND x.FromID =  t.FromID) 
                     THEN NULL
                     ELSE (SELECT MIN(GroupNumber)-1 FROM #TableToOrder) 
                END
      FROM #TableToOrder t
    WHERE t.GroupNumber IS NULL;
    SET ANSI_WARNINGS ON;
      
    -- Build the delete commands in the correct order;
    WITH x AS (
        SELECT DISTINCT 
               MIN(GroupNumber) GroupNumber
              ,FromTable 
          FROM #TableToOrder 
         GROUP BY FromTable 
    )
    SELECT @Cmds = @Cmds+'DELETE '+@pSchemaName+'.'+QUOTENAME(x.FromTable)+';'+@CrLf
            +'    PRINT CAST(@@ROWCOUNT AS nvarchar)+'' Rows deleted from '+QUOTENAME(x.FromTable)+''';'+@CrLf
      FROM x
     ORDER BY x.GroupNumber,x.FromTable;
    DROP TABLE #TableToOrder;
    
    --  Now add RESEED commands to reset identity columns.
    DECLARE @T int;
    SELECT @Cmds = @Cmds
            +'PRINT ''Reseed '+QUOTENAME(st.name)+''';'+@CrLf
            +'    DBCC CHECKIDENT ('''+@pSchemaName+'.'+QUOTENAME(st.name)+''''+REPLICATE(' ',30-LEN(st.name))+', reseed, 0);'+@CrLf
           -- This gets around the problem of not being able to use an ORDER BY clause
           -- I want the names ordered so it's easier to tell if a particular one is there/missing
          ,@T = ROW_NUMBER() OVER(ORDER BY st.name)
      FROM sys.tables st
      JOIN sys.columns sc
        ON sc.object_id = st.object_id
       AND sc.is_identity = 1
     WHERE st.schema_id = @SchemaID;

    -- Execute or display as requested
    IF @pTest = 1
        SELECT CAST('<?q '+(@cmds)+' ?>' AS XML);
    ELSE
        EXEC (@cmds);
    
/*Test
GO
exec Global.DeleteAllSchemaData 'dEspStage',1;
--*/
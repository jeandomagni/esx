﻿--=========================================================================================
--Written by: Rick Bielawski
--
--Description: Generates the commands necessary to insert existing data into another table.
--             WARNING: This procedure cannot be used on:
--                           text columns
--                           binary columns
--                           Rows where the script for a single row exceeds 64K.
--                      Also be careful of scripting calculated columns since inserting 
--                           their values typically fails because the column is also
--                           calculated in the target table. (Hint:Use the @pSelect clause.)
--  
--Parameters:
--  @pTable (required)
--     Source table name.  Can be a 1,2,3 or 4-part name.
--     
--  @pDestTable (optional, defaults to @pTable)
--    The table into which the generated script will insert the data.  Used as is
--  
--  @pSelect (optional... defaults to '*')
--     Used to limit the number of rows or columns to be queried and therefore inserted.  
--     Enter a comma-delimited list of columns names.
--     Preceed the list with top(N) to limit the number of rows.
--     Note that if a uniqueidentifier field is included, the resulting code will just
--     define it as a NVARCHAR field.
--  
--  @pJoin (optional... defaults to '')
--     If you want to JOIN any tables to the table specified in the @pTable parameter,
--     you can specify a JOIN clause (or clauses) here.
--  
--  @pWhere (optional... defaults to '1=1')
--     This allows you to limit the rows of the sample. Assume the keyword WHERE
--     already preceeds your clause.
--  
--  @pMaxRows (optional... defaults to 1000)
--     This insures the output isn't huge by mistake.  If the number of rows selected
--     exceeds @pMaxRows, an error will occur before insert statement generation starts.
--  
--  @pOrderBy (optional... defaults to (SELECT 1)
--     Orders the rows being generated.
--  
--  @pIndex (optional... defaults to NULL)
--     An integer value causes 2 extra columns in the output.  This allows the user to 
--     script multiple tables into a single common table and later guarantee the order 
--     of output rows.  The @pIndex value is one of the columns.  The other is a Row_Number
--     to insure a subsequent select orders the rows correctly.
--=========================================================================================
CREATE PROCEDURE [Global].[InsertGenerator]
       @pTable sysname                     --Name of the Source table for the data
      ,@pDestTable sysname = @pTable       --Name of table into which data is inserted
      ,@pSelect nvarchar(MAX) = '*'        --Columns to SELECT from the table(s)
      ,@pJoin nvarchar(MAX) = ''           --Optional JOIN(s)
      ,@pWhere nvarchar(MAX) = '1=1'       --Optional WHERE clause
      ,@pMaxRows bigint = 1000             --Maximum records allowed
      ,@pOrderBy nvarchar(MAX)='(SELECT 1)'--Optional ORDER BY clause of ROW_NUMBER function
      ,@pIndex int = NULL                  --A user defined field that returns with the data
	  ,@pAddPrint bit = 0                  --When 1, adds a print statement after the insert to print @@rowcount
      ,@pTest bit = 0                      --When 1, prints scripts before execution
AS
BEGIN
  SET nocount ON;
/*
  DECLARE @pTest bit = 1;
  DECLARE @pTable nvarchar(500) ='dExternalETL.PhysicalAddressGeoData';
  DECLARE @pDestTable nvarchar(100) = @pTable ;
  DECLARE @pSelect nvarchar(MAX) = 'AddressID,ValidationData,ValidationDatetime';        --   '*'       ;
  DECLARE @pJoin nvarchar(MAX) = '';
  DECLARE @pWhere nvarchar(MAX) = '1=1';
  DECLARE @pMaxRows bigint = 35000;
  DECLARE @pIndex int = null;
  DECLARE @pOrderBy nvarchar(MAX)='AddressID';
  DECLARE @pAddPrint bit = 0;       
--*/
  
  DECLARE @SqlStmt   nvarchar(MAX)
         ,@NumRows   bigint
         ,@ErrMsg    nvarchar(4000)
         ,@ColList   nvarchar(MAX)
         ,@ColValues nvarchar(MAX)
         ,@CrLf      nchar(2)       = CHAR(13)+CHAR(10);

  SET @pDestTable = ISNULL(NULLIF(@pDestTable,''),@pTable);
  
  ---------------------------------------------------------------------------------------
  -- Parameter Validation
  ---------------------------------------------------------------------------------------

  IF ISNULL(@pTable, '') = '' 
    BEGIN
      SET @ErrMsg = 'You must pass a @pTable parameter.';
      RAISERROR(@ErrMsg,16,1);
      RETURN -1;
    END
  
  IF @pSelect = '*'
    AND @pJoin <> '' 
    BEGIN
      SET @ErrMsg = 'You must specify columns in the @pSelect parameter if you pass the @pJoin parameter.';
      RAISERROR(@ErrMsg,16,2);
      RETURN -1;
    END
  
  SET @SqlStmt = 'select @NumRows=count(*) from '+@pTable+' '+@pJoin+' where '+@pWhere+';';

  EXEC sp_executesql @SqlStmt, N'@NumRows bigint output', @NumRows OUTPUT;

  IF @@error <> 0 
    BEGIN
      SET @ErrMsg = @CrLf+'The SQL Statement attempted was:'+@CrLf+@CrLf+@SqlStmt;
      RAISERROR(@ErrMsg,16,3);
      RETURN -1;
    END

  IF @NumRows > @pMaxRows OR @NumRows = 0
    BEGIN
      SET @ErrMsg = @CrLf+'There are '+CAST(@NumRows AS varchar)+' rows in your result set. A maximum of '
       +CAST(@pMaxRows as varchar)+' are allowed.  The minimum is 1.' 
       +@CrLf+@CrLf+'If you want to extract more, use the @pMaxRows parameter.';
      RAISERROR(@ErrMsg,16,4);
      RETURN -1;
    END
  
  ---------------------------------------------------------------------------------------
  -- Create a temp table with data to be scripted to facilitate column name/type scripting.
  ---------------------------------------------------------------------------------------
  IF 1=0
	CREATE TABLE ##_InsertGenerator_Data(dummyTable int);  -- This is required to suppress project errors.  The table is created in dynamic code.

  IF OBJECT_ID('tempdb..##_InsertGenerator_Data', 'U') IS NOT NULL 
    DROP TABLE ##_InsertGenerator_Data;     -- tempdb.sys.columns can't be dynamically queried for session temp tables.  i.e. #_Insert...
  
  SET @SqlStmt = 'select '+@pSelect 
     +@CrLf+'  into ##_InsertGenerator_Data'
     +@CrLf+'  from '+@pTable 
     +CASE @pJoin WHEN '' THEN '' ELSE+@CrLf+'  '+@pJoin END
     +@CrLf+' where '+@pWhere+';';

  IF @pTest = 1 Print @SqlStmt;
  EXEC sp_executesql @SqlStmt;
  
  IF @@error <> 0 
    BEGIN
      SET @ErrMsg = @CrLf+'The SQL Statement attempted was:'+@CrLf+@CrLf+@SqlStmt;
      RAISERROR(@ErrMsg,16,5);
      RETURN -1;
    END
  
  ---------------------------------------------------------------------------------------
  -- Create temp table with column definition data
  ---------------------------------------------------------------------------------------
  IF OBJECT_ID('tempdb..#_InsertGenerator_Struct', 'U') IS NOT NULL 
    DROP TABLE #_InsertGenerator_Struct;

  SELECT  C.name
         ,DataType = TYPE_NAME(C.user_type_id)
         ,C.is_identity
    INTO #_InsertGenerator_Struct  
    FROM tempdb.sys.columns C
   WHERE object_id = OBJECT_ID('tempdb..##_InsertGenerator_Data', 'U')
          AND TYPE_NAME(user_type_id) IN ('bigint', 'bit', 'decimal', 'int', 'money', 'numeric', 'smallint',
                                            'smallmoney', 'tinyint', 'float', 'real', 'date', 'datetime2', 'datetime',
                                            'datetimeoffset', 'smalldatetime', 'time', 'char', 'varchar', 'nchar',
                                            'nvarchar', 'uniqueidentifier', 'xml','geometry','geography')

  ---------------------------------------------------------------------------------------
  -- Create commands to transform actual data into a format useable in a VALUES clause.
  ---------------------------------------------------------------------------------------
  SET @ColValues = '';
  SELECT @ColValues = @ColValues 
--        +'+'', '''+@CrLf+SPACE(11)+'+'
        +CASE WHEN @ColValues = '' 
              THEN @CrLf+SPACE(11)+'+'
              ELSE '+'', '''+@CrLf+SPACE(11)+'+'
         END 
        +'coalesce('+CASE WHEN DataType IN ('char', 'varchar')
                          THEN '''''''''+replace('+QUOTENAME(name)+','''''''','''''''''''')+'''''''''
                          WHEN DataType IN ('nchar', 'nvarchar')
                          THEN '''N''''''+replace('+QUOTENAME(name)+','''''''','''''''''''')+'''''''''
                          WHEN DataType IN ('xml')
                          THEN '''N''''''+replace(cast('+QUOTENAME(name)+' as nvarchar(max)),'''''''','''''''''''')+'''''''''
                          WHEN DataType IN ('geometry','geography')
                          THEN '''''''''+cast('+QUOTENAME(name)+' as varchar(max))+'''''''''
                          WHEN DataType IN ('date')
                          THEN '''''''''+convert(nvarchar(100),'+QUOTENAME(name)+',112)+'''''''''
                          WHEN DataType IN ('time')
                          THEN '''''''''+convert(nvarchar(100),'+QUOTENAME(name)+',108)+'''''''''
                          WHEN DataType IN ('datetime', 'datetime2', 'smalldatetime','datetimeoffset')
                          THEN '''''''''+convert(nvarchar(100),'+QUOTENAME(name)+',126)+'''''''''
                          WHEN DataType IN ('money', 'smallmoney', 'float', 'real')
                          THEN 'convert(nvarchar(100),'+QUOTENAME(name)+',2)'
                          WHEN DataType IN ('uniqueidentifier')
                          THEN '''''''''+convert(nvarchar(100),'+QUOTENAME(name)+')+'''''''''
                          ELSE 'convert(nvarchar(100),'+QUOTENAME(name)+')'
                     END+',''NULL'')'
    FROM #_InsertGenerator_Struct;
  
  IF @pTest = 1 Print @ColValues;
  ---------------------------------------------------------------------------------------
  -- Create the list of columns to specify in the insert.
  ---------------------------------------------------------------------------------------
  SELECT @ColList = '('
        +REPLACE(REPLACE((SELECT REPLACE(QUOTENAME(name),' ',CHAR(9)) [data()]
                            FROM #_InsertGenerator_Struct
                             FOR XML PATH('')),' ',','),CHAR(9),' ')
        +')';
  IF @pTest = 1 Print @ColList;
  
  ---------------------------------------------------------------------------------------
  -- Create the query that yields the desired output and execute it
  ---------------------------------------------------------------------------------------
  DECLARE @HasIdentity int        = (SELECT COUNT(*) FROM #_InsertGenerator_Struct WHERE is_identity = 1);
  DECLARE @pIndexVal     nvarchar(100) = CAST(@pIndex as nvarchar);
  DECLARE @BeginGroup1  nvarchar(100) = '';
  DECLARE @EndGroup1    nvarchar(100) = '';
  DECLARE @BeginGroup2  nvarchar(100) = '';
  DECLARE @EndGroup2    nvarchar(100) = '';
  DECLARE @DataRow      nvarchar(4000)= '';
  IF @pIndex IS NOT NULL
  BEGIN
    SET @BeginGroup1 = @pIndexVal+' AS I,-2 AS R,'
    SET @EndGroup1   = @pIndexVal+' AS I,'+CAST(@pMaxRows+2 as varchar)+' AS R,'
    SET @BeginGroup2 = @pIndexVal+' AS I,-1 AS R,'
    SET @EndGroup2   = @pIndexVal+' AS I,'+CAST(@pMaxRows+1 as varchar)+' AS R,'
    SET @DataRow     = @pIndexVal+' AS I,row_number() over(order by '+@pOrderBy+') AS R,'
  END

  IF @HasIdentity > 0
    SET @SqlStmt = @CrLf+'select '+@BeginGroup1+'''SET IDENTITY_INSERT '+@pDestTable+' ON;'' AS V'
                 + @CrLf+'union all';
  ELSE
    SET @SqlStmt = '';

  SET @SqlStmt = @SqlStmt+@CrLf+'select '+@BeginGroup2
               +'''INSERT '+@pDestTable+@ColList
               + @CrLf+'SELECT * FROM (VALUES'' AS V'
               + @CrLf+'union all';
  
  SET @SqlStmt = @SqlStmt+@CrLf+'select '+@DataRow
               + @CrLf+'       case when row_number() over(order by '+@pOrderBy+') = 1 then '' ('' else '',('' end'
               + @ColValues+'+'')'' AS V'
               + @CrLf+'  from ##_InsertGenerator_Data'
               + @CrLf+'union all';

  SET @SqlStmt = @SqlStmt+@CrLf+'select '+@EndGroup2+''')x'+@ColList+';'
                +CASE WHEN @pAddPrint=1 
                      THEN @CrLf+'PRINT CAST(@@RowCount as nvarchar)+'''' rows inserted into '+@pDestTable+''''';'
                      ELSE ''
                 END +''' AS V';

  IF @HasIdentity > 0
    SET @SqlStmt = @SqlStmt+@CrLf+'union all'
                 + @CrLf+'select '+@EndGroup1+'''SET IDENTITY_INSERT '+@pDestTable+' OFF;'' AS V'
                 + @CrLf;
  ELSE
    SET @SqlStmt = @SqlStmt+';';

  IF @pTest = 1 Print @SqlStmt;
  EXEC sp_executesql @SqlStmt;
  ---------------------------------------------------------------------------------------
  -- Cleanup
  ---------------------------------------------------------------------------------------
  IF @@error <> 0 
    BEGIN
      RAISERROR('Unknown error in executing final SQL Statement.',16,1);
      RETURN -1;
    END
  
  DROP TABLE ##_InsertGenerator_Data;
  DROP TABLE #_InsertGenerator_Struct;

END
/*
GO
EXEC Global.InsertGenerator @pTable='dExcelETL.ExcelSourceFile',@pAddPrint=0,@pTest=1;
EXEC Global.InsertGenerator @pTable='dExcelETL.ExcelSourceFile',@pAddPrint=1,@pTest=1;
--*/
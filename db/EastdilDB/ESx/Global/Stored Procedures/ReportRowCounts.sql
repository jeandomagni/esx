﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/08
-- Description: Reports row counts for all tables in a schema
-- =============================================
CREATE PROCEDURE [Global].[ReportRowCounts]
    @pSchemaName sysname
   ,@pTest bit = 0
AS
/*
    DECLARE @pSchemaName sysname = 'dExcelETL';
    DECLARE @pTest bit          = 1;
--*/
    SET NOCOUNT ON;
    DECLARE @CrLf char(2) = CHAR(13)+CHAR(10);
	DECLARE @SchemaID int = SCHEMA_ID(@pSchemaName);
    DECLARE @Cmds nvarchar(MAX) = '--'+@CrLf;

    SELECT @Cmds = @Cmds
            +'SELECT '''+@pSchemaName+'.'+QUOTENAME(st.name)+''' AS TableName,COUNT(*) CNT FROM '
            +@pSchemaName+'.'+QUOTENAME(st.name)+' UNION ALL'+@CrLf
      FROM sys.tables st
     WHERE st.schema_id = @SchemaID;

    SET @Cmds = SUBSTRING(@Cmds,1,LEN(@Cmds)-12)+';';
	 
    -- Execute or display as requested
    IF @pTest = 1
        SELECT CAST('<?q '+(@cmds)+' ?>' AS XML);
    ELSE
        EXEC (@cmds);
    
/*Test
GO
exec Global.ReportRowCounts 'dExcelETL';
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2010/01/22
-- Description: Scripts out objects created by me.
-- =============================================
CREATE PROCEDURE [Global].CreateScript
AS
WITH X AS (
SELECT 'GO
'     +REPLACE(M.[definition],'==
CREATE'+CHAR(32)             ,'==
ALTER ')                                                    AS [Object Definition]
      ,SCHEMA_NAME(O.schema_id)                             AS [Schema Name]
      ,CASE WHEN SCHEMA_NAME(O.schema_id) LIKE 't%'
            THEN O.name
            ELSE '     '+O.name
       END                                                  AS [Object Name]
      ,O.type_desc                                          AS [Object Type]
      ,CASE WHEN SCHEMA_NAME(O.schema_id) LIKE 't%'
            THEN SUBSTRING(SCHEMA_NAME(O.schema_id),2,100)
            ELSE SCHEMA_NAME(O.schema_id)
       END                                                  AS [Sort Schema Name]
      ,CASE WHEN O.name LIKE 'TEST%'
            THEN SUBSTRING(O.name,6,200)+'!'
            ELSE O.name
       END                                                  AS [Sort Object Name]
  FROM sys.objects O
  JOIN sys.all_sql_modules M
    ON O.object_id = M.object_id
 WHERE M.[definition] LIKE '%Bielawski%'
   AND DATALENGTH(M.definition) > 65535
)
SELECT CAST('<?q '+ X.[Object Definition] + ' ?>' AS xml) [Object Definition]
      ,CAST(DATALENGTH(X.[Object Definition]) AS varchar)+REPLICATE(' ',7-LEN(CAST(DATALENGTH(X.[Object Definition]) AS varchar)))
                                                            AS [ByteLen]
      ,'XML      '                                          AS [Length]
      ,CASE WHEN X.[Object Definition] like '%
/*Test
GO
%'
-- don't remove this. It syntactically balances the one in quotes above --> */
            THEN ' Test Comment OK '
            ELSE '-Missing Comment-'
       END                                                  AS [Test Status]
      ,[Schema Name]+REPLICATE(' ',10 - LEN([Schema Name])) AS [Schema Name]
      ,[Object Name]+REPLICATE(' ',50 - LEN([Object Name])) AS [Object Name]
      ,X.[Object Type]
  FROM X
 ORDER BY [Sort Schema Name],[Sort Object Name];

WITH X AS (
SELECT 'GO
'     +REPLACE(M.[definition],'==
CREATE'+CHAR(32)             ,'==
ALTER ')                                                    AS [Object Definition]
      ,SCHEMA_NAME(O.schema_id)                             AS [Schema Name]
      ,CASE WHEN SCHEMA_NAME(O.schema_id) LIKE 't%'
            THEN O.name
            ELSE '     '+O.name
       END                                                  AS [Object Name]
      ,O.type_desc                                          AS [Object Type]
      ,CASE WHEN SCHEMA_NAME(O.schema_id) LIKE 't%'
            THEN SUBSTRING(SCHEMA_NAME(O.schema_id),2,100)
            ELSE SCHEMA_NAME(O.schema_id)
       END                                                  AS [Sort Schema Name]
      ,CASE WHEN O.name LIKE 'TEST%'
            THEN SUBSTRING(O.name,6,200)+'!'
            ELSE O.name
       END                                                  AS [Sort Object Name]
  FROM sys.objects O
  JOIN sys.all_sql_modules M
    ON O.object_id = M.object_id
 WHERE M.[definition] LIKE '%Bielawski%'
   AND DATALENGTH(M.definition) <= 65535
)
SELECT X.[Object Definition]
      ,CAST(DATALENGTH(X.[Object Definition]) AS varchar)+REPLICATE(' ',7-LEN(CAST(DATALENGTH(X.[Object Definition]) AS varchar)))
                                                            AS [ByteLen]
      ,CASE WHEN DATALENGTH(X.[Object Definition])>65536
            THEN '***XML***'
            WHEN DATALENGTH(X.[Object Definition])>50000
            THEN 'WATCH IT!'
            ELSE 'Length OK'
       END                                                  AS [Length]
      ,CASE WHEN X.[Object Definition] like '%
/*Test
GO
%'
-- don't remove this. It syntactically balances the one in quotes above --> */
            THEN ' Test Comment OK '
            ELSE '-Missing Comment-'
       END                                                  AS [Test Status]
      ,[Schema Name]+REPLICATE(' ',10 - LEN([Schema Name])) AS [Schema Name]
      ,[Object Name]+REPLICATE(' ',50 - LEN([Object Name])) AS [Object Name]
      ,X.[Object Type]
  FROM X
 ORDER BY [Sort Schema Name],[Sort Object Name];
/*Test
go
exec [Global].CreateScript;
--*/
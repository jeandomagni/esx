﻿--------------------------------------------------------------------
-- CREATED BY : Rick Bielawski
-- CREATE DATE: 10/23/2010
-- DESCRIPTION: Converts a string of delimited values to a table 
--              with each value on a seperate row.
--------------------------------------------------------------------
CREATE FUNCTION [Global].[SplitStringVertical] (
     @Data varchar(MAX)     -- list of delimited values like '1|2|5|7'
    ,@Delimiter varchar(10) -- character(s) seperating elements like '|'
)
RETURNS table
    WITH SCHEMABINDING
AS RETURN
    WITH x AS (
        SELECT CAST('<root><item>'+REPLACE(REPLACE(REPLACE(REPLACE(@Data, '&', '&amp;'), '<', '&lt;'), '>','&gt;')
                   ,REPLACE(REPLACE(REPLACE(@Delimiter, '&', '&amp;'), '<', '&lt;'),'>', '&gt;'), '</item><item>')+'</item></root>' AS xml) AS [xml]
)
    SELECT n.c1.value('.[1]', 'varchar(max)') AS [item]
      FROM x
     CROSS APPLY x.XML.nodes('/root/item') AS n (c1);
/*
GO
select ''''+item+'''' from [Global].udt_split('this list,should split,properly, \
even with blanks ,string''s should “behave”,"unbalanced \
or somewhat long strings should not truncate unexpectedly, and imbedded xml \
chars like ><, <or><this><malformed;>''junk''>,should be OK!',',')
--*/
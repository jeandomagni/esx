﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/03/29
-- Description: Populates ValidatedAddress and Coordinates columns from ValidationData in tables where it exists.
--      There are 4 types of rows that won't be touched.
--      1) Those that already have ValidatedAddress.
--      2) Those where ValidationData is null.
--      3) Those where ValidationData indicates the address is invalid
--      4) Those where the ValidationData contains multiple possibilities (except where manual disambiguation has already occurred).
-- =============================================
CREATE PROCEDURE [xESxETL].[ExtractValidatedAddressData]
AS
BEGIN
    SET NOCOUNT ON;

   -- Populate the address/coordinates of validated entries without them that returned only one valid value
    UPDATE pagd
       SET ValidatedAddress =             pagd.ValidationData.value('/GeocodeResponse[1]/result[1]/formatted_address[1]','nvarchar(200)')
          ,Coordinates = geography::Point(pagd.ValidationData.value('/GeocodeResponse[1]/result[1]/geometry[1]/location[1]/lat[1]','float')
                                         ,pagd.ValidationData.value('/GeocodeResponse[1]/result[1]/geometry[1]/location[1]/lng[1]','float'), 4326) 
                                                    -- select *
      FROM dExternalETL.PhysicalAddressGeoData pagd
     WHERE pagd.ValidationData.value('count(/GeocodeResponse/result/formatted_address)','int') = 1
       AND pagd.ValidatedAddress IS NULL;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' PhysicalAddress rows with one validation result updated';
        
END
/*Test
GO
--UPDATE dExternalETL.PhysicalAddressGeoData SET ValidatedAddress = NULL, Coordinates = NULL;
SELECT COUNT(*) FROM dExternalETL.PhysicalAddressGeoData WHERE ValidationData IS NOT NULL AND ValidatedAddress IS NULL ;
EXEC xESxETL.ExtractValidatedAddressData;
SELECT COUNT(*) FROM dExternalETL.PhysicalAddressGeoData WHERE ValidationData IS NOT NULL AND ValidatedAddress IS NULL ;
--*/

﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/01
-- Description: Pulls into the external schema, references to staged address rows not already loaded.
--              That is, a reference to every staged row containing an address that should be validated is made to exist.
--              Since validation may have a cost existing external rows are not deleted if they no longer exist in stage.
-- =============================================
CREATE PROCEDURE [xESxETL].[LoadAddressesToValidate] AS 
BEGIN
    SET NOCOUNT ON;
    
    INSERT INTO dExternalETL.PhysicalAddressGeoData (LookupValue)
    SELECT slPr.LookupValue     -- select *
      FROM dExcelETL.SLProperties slPr
     WHERE slPr.LookupValue NOT IN (
            SELECT ext.LookupValue
              FROM dExternalETL.PhysicalAddressGeoData ext)
     GROUP BY slPr.LookupValue;

    PRINT CAST(@@ROWCOUNT AS nvarchar)+' PhysicalAddressGeoData rows inserted from SLProperties';

    INSERT INTO dExternalETL.PhysicalAddressGeoData (LookupValue)
    SELECT Pr.LookupValue   -- select *
      FROM dEspStage.Properties Pr
     WHERE ISNULL(Pr.Address1,Pr.PropertyName)<>'' 
       AND Pr.City NOT IN ('','0','various','National')
       AND LEN(Pr.State)=2
       AND Pr.LookupValue NOT IN (
            SELECT ext.LookupValue
              FROM dExternalETL.PhysicalAddressGeoData ext)
     GROUP BY Pr.LookupValue;
    
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' PhysicalAddressGeoData rows inserted from Properties';

    INSERT INTO dExternalETL.PhysicalAddressGeoData (LookupValue)
    SELECT Ad.LookupValue   -- select *
      FROM dEspStage.Addresses Ad
     WHERE Ad.LookupValue IS NOT null
       AND Ad.LookupValue NOT LIKE '%,,,'
       AND Ad.LookupValue NOT LIKE '*%'
       AND Ad.LookupValue NOT LIKE '-%'
       AND Ad.LookupValue NOT LIKE 'wrong %'
       AND Ad.LookupValue NOT IN (
            SELECT ext.LookupValue
              FROM dExternalETL.PhysicalAddressGeoData ext)
     GROUP BY Ad.LookupValue;

    PRINT CAST(@@ROWCOUNT AS nvarchar)+' PhysicalAddressGeoData rows inserted from Addresses';

    INSERT INTO dExternalETL.PhysicalAddressGeoData (LookupValue)
    SELECT Ea.LookupValue   -- select *
      FROM dExcelETL.EastdilOffices Ea
     WHERE Ea.LookupValue NOT IN (
            SELECT ext.LookupValue
              FROM dExternalETL.PhysicalAddressGeoData ext)
     GROUP BY Ea.LookupValue;

    PRINT CAST(@@ROWCOUNT AS nvarchar)+' PhysicalAddressGeoData rows inserted from EastdilOffices';
END
/*Test
GO
EXEC xESxETL.LoadAddressesToValidate;
--*/
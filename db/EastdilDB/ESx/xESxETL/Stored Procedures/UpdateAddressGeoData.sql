﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/04
-- Description: Updates the appropriate address row with GeoData.
--              This updates to rows returned by VIEW xESxETL.PropertiesNeedingGeoData
-- =============================================
CREATE PROCEDURE [xESxETL].[UpdateAddressGeoData] (
     @LookupValue   nvarchar(303)
    ,@GeoData       nvarchar(MAX)
)
AS 
BEGIN
    SET NOCOUNT ON;
    UPDATE dExternalETL.PhysicalAddressGeoData 
       SET ValidationData = @GeoData
          ,ValidationDatetime = GETUTCDATE()
          ,ValidatedAddress = NULL
          ,Coordinates      = NULL
     WHERE LookupValue = @LookupValue;
END
/*Test
GO
EXEC xESxETL.UpdateAddressGeoData @LookupValue='One Baylor Plaza,Houston,TX,United States',@GeoData='<GeocodeResponse><status>ZERO_RESULTS</status></GeocodeResponse>';
EXEC xESxETL.UpdateAddressGeoData @LookupValue='625 North Michigan Avenue ,Chicago,IL,',@GeoData='<GeocodeResponse><status>ZERO_RESULTS</status></GeocodeResponse>';
select * from dExternalETL.PhysicalAddressGeoData PAGD WHERE ValidationDatetime > (GETUTCDATE()-0.1)
--*/
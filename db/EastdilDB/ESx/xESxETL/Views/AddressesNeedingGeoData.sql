﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/01
-- Description: Used by the Geo Lookup program to pull rows that require looking up
-- =============================================
CREATE VIEW [xESxETL].[AddressesNeedingGeoData]  AS 
    SELECT pagd.LookupValue
      FROM dExternalETL.PhysicalAddressGeoData pagd
     WHERE pagd.ValidationDatetime IS NULL
       AND pagd.LookupValue NOT LIKE 'Post Office Box%'         -- No point looking up post office boxes in any form
       AND pagd.LookupValue NOT LIKE 'PO Box%'
       AND pagd.LookupValue NOT LIKE 'P.O. Box%'
       AND LookupValue = CAST(LookupValue AS varchar(200));    -- This is an issue with Powershell handling of the data.
/*Test
GO
select * from xESxETL.AddressesNeedingGeoData;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/11/16
-- Description: This returns the CacheSessionQuery_Key under which CacheCompanyBrief data is stored for paged access.
--              An encoded version of all the filter text applied to the cached data is also returned.
-- =============================================
CREATE PROCEDURE iESx.CacheCompanyBrief
     @pAccessToken      iESx.TYP_AccessToken    -- This token represents the user on whose behalf the Put is being made.
    ,@pFilterText       nvarchar(1000)          -- The text to compare company alias names to for filtering purposes.
    ,@pForceRebuild     bit                     -- Caller wants the cache refreshed regardless of its apparent state.
    ,@pNoRebuild        bit                     -- Caller is paging thru the existing cache and just needs the SessionQueryKey
    ,@pSessionQueryKey  int             OUTPUT  -- Cache key holding current result set.
    ,@pTotalFilter      nvarchar(1000)  OUTPUT  -- Text describing all filters applied to the cache
AS
    SET NOCOUNT ON;
    DECLARE @ProcName       sysname = 'CacheCompanyBrief';--OBJECT_NAME(@@PROCID);
/*
DECLARE @ProcName          sysname              = 'CacheCompanyBrief';
DECLARE @pAccessToken      iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
DECLARE @pFilterText       nvarchar(1000)       = '';
DECLARE @pForceRebuild     bit                  = 0;
DECLARE @pNoRebuild        bit                  = 0;
DECLARE @pSessionQueryKey  int                  = null;
DECLARE @pTotalFilter      nvarchar(1000)       = null;
--*/
    DECLARE @CurrentFilter      nvarchar(1000);
    DECLARE @UserKey            int;
    DECLARE @CacheTime          datetime2(0);
    DECLARE @CacheKey           int;

    SELECT @UserKey     = GAcTo.User_Key
          ,@CacheKey    = GAcTo.Cache_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcTo;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

    -- Initialize output params
    SET @pSessionQueryKey   = NULL;
    SET @pTotalFilter       = NULL;

    -- Retrieve session info
    --/*
    SELECT @pSessionQueryKey    = CaSeQ.CacheSessionQuery_Key
          ,@CacheTime           = CaSeQ.CreateTime
          ,@CurrentFilter       = CaSeQ.FilterText
          ,@pTotalFilter        = CaSeQ.TotalFilter
          ,@pFilterText         = REPLACE(REPLACE(REPLACE(ISNULL(@pFilterText,N''),N'_',NCHAR(1)+N'_'),N'%',NCHAR(1)+N'%'),N'[',NCHAR(1)+N'[')
          ,@pNoRebuild          = ISNULL(@pNoRebuild,0) -- */ SELECT @UserKey,@CacheKey; SELECT *
      FROM dESx.CacheSessionQuery CaSeQ
     WHERE CaSeQ.CacheAccessToken_Key = @CacheKey
       AND CaSeQ.QueryType = @ProcName;

    -- When paging we don't insist the client remember the proper filter so set the proper value regardless of what was supplied
    IF @pNoRebuild = 1
        SET @pFilterText = @CurrentFilter;

    -- Often the caller just wants the session query key (retrieved above).  In that case skip all this processing.
    --/*
    IF      @pSessionQueryKey  IS NULL              -- There must be a key to return
        OR  @pForceRebuild      = 1                 -- They can't have specifically asked for the cache to be rebuilt
        OR  @CurrentFilter     != @pFilterText      -- The filter cannot have changed (ignored filter value if no-rebuild explicitly requested)
    BEGIN
        -- Reset the cache when: no cache exists, the filter text is blank or the new filter is not a subset of the current filter
        IF @pSessionQueryKey IS NULL
            OR @pFilterText = ''
            OR @pFilterText NOT LIKE REPLACE(@pTotalFilter,CHAR(1),CHAR(1)+CHAR(1)+CHAR(1)) ESCAPE CHAR(1)
        BEGIN     --*/

            -- Delete existing cache.  Cascading deletes takes care of downstream tables.  If none, no harm.
            DELETE dESx.CacheSessionQuery
             WHERE CacheSessionQuery_Key = @pSessionQueryKey;
            
              --All rows start out being present so filters start empty.
            SELECT @pTotalFilter    = '%%'
                  ,@CurrentFilter   = '';
    
            -- Create a new row for the cache and get its key.  This guarantees the row exists and gets the key.
            DECLARE @SaveCacheKey table(TableKey int);
    
            INSERT dESx.CacheSessionQuery(CacheAccessToken_Key,QueryType,FilterText,TotalFilter,CreateTime) 
            OUTPUT Inserted.CacheSessionQuery_Key INTO @SaveCacheKey
            VALUES  (@CacheKey,@ProcName,@CurrentFilter,@pTotalFilter,CAST(GETUTCDATE() AS datetime2(0)));
    
            SELECT @pSessionQueryKey = TableKey FROM @SaveCacheKey;
    
            -- Fill the cache with all the companies the user is allowed to see (currently all non-defunct companies)
            WITH X AS (
                SELECT Al.CompanyAlias_Key AS Alias_Key
                      ,Al.Company_Key AS Canonical@Company_Key
                  FROM dESx.Company Co
                  JOIN dESx.CompanyAlias Al
                    ON Al.Company_Key = Co.Company_Key
                 WHERE Co.IsDefunct = 0
                   AND ( Co.IsCanonical = 1
                        -- looks to see if a parent exists
                      OR NOT EXISTS (SELECT NULL FROM dESx.CompanyHierarchy iCoH WHERE iCoH.[Child__Company_Key] = Co.Company_Key))
            )
            INSERT dESx.CacheCompanyBrief(CacheSessionQuery_Key,[Canonical__Alias_Key],[Matching__Alias_Key])
            SELECT @pSessionQueryKey
                  ,Al.CompanyAlias_Key     AS [Canonical__Alias_Key]
                  ,X.Alias_Key      AS [Matching__Alias_Key]  --*/ select *
              FROM X
              JOIN dESx.CompanyAlias Al
                ON Al.Company_Key = X.Canonical@Company_Key
               AND Al.IsPrimary = 1;
        END;

        -- When the filter isn't blank, apply the filter to cached rows (this allows addative filtering)
        IF @pFilterText != ''
        BEGIN
            -- Build the actual filter 
            SET @pTotalFilter = N'%'+@pFilterText+N'%';
            BEGIN TRANSACTION;
    
            -- Record the state of the cache
            UPDATE dESx.CacheSessionQuery 
               SET FilterText   = @pFilterText
                  ,TotalFilter  = @pTotalFilter
                  -- don't update CreateTime because that represents the underlying company table snapshot time
             WHERE CacheSessionQuery_Key = @pSessionQueryKey;
    
            -- Make the cache meet the state by deleting rows that don't match the filter text
            DELETE CaCoB
              FROM dESx.CacheCompanyBrief CaCoB
              JOIN dESx.CompanyAlias Al
                ON Al.CompanyAlias_Key = CaCoB.[Matching__Alias_Key]
             WHERE CaCoB.CacheSessionQuery_Key = @pSessionQueryKey
               AND N' '+Al.AliasName   +N' ' NOT LIKE @pTotalFilter ESCAPE CHAR(1);
    
             COMMIT TRANSACTION;
        END;
    END;

    RETURN;

/*Test
GO
DECLARE @pAccessToken     iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
DECLARE @pFilterText      nvarchar(1000)       = '';
DECLARE @pTotalFilter     nvarchar(1000);
DECLARE @pSessionQueryKey int;
SELECT * FROM dESx.CacheSessionQuery LEFT JOIN dESx.CacheCompanyBrief ON CacheSessionQuery.CacheSessionQuery_Key = CacheCompanyBrief.CacheSessionQuery_Key;
EXEC iESx.CacheCompanyBrief @pAccessToken, @pFilterText, 0, 0, @pSessionQueryKey OUTPUT, @pTotalFilter OUTPUT;
SELECT @pFilterText,@pTotalFilter;
SELECT * FROM dESx.CacheSessionQuery LEFT JOIN dESx.CacheCompanyBrief ON CacheSessionQuery.CacheSessionQuery_Key = CacheCompanyBrief.CacheSessionQuery_Key;
SET @pFilterText = 'ack'
EXEC iESx.CacheCompanyBrief @pAccessToken, @pFilterText, 0, 0, @pSessionQueryKey OUTPUT, @pTotalFilter OUTPUT;
SELECT @pFilterText,@pTotalFilter;
SELECT * FROM dESx.CacheSessionQuery LEFT JOIN dESx.CacheCompanyBrief ON CacheSessionQuery.CacheSessionQuery_Key = CacheCompanyBrief.CacheSessionQuery_Key;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/15
-- Description: Generates a name for new Mention
-- =============================================
CREATE FUNCTION iESx.CalculateRawMentionName (
     @pObjectType   char(1)                   -- One of A,C,D,E,L,M,O,P indicating the type of object @pObjectKey belongs to.
    ,@pObjectKey    int   
	,@pDefaultMentionId  nvarchar(40)                   
)
RETURNS iESx.TYP_Reference AS 
BEGIN    
/*
DECLARE @pObjectType   char(1)              = 'A'
DECLARE @pObjectKey    int                  = 1
--*/
    DECLARE @MentionID iESx.TYP_Reference;
    DECLARE @Default nvarchar(40);
   SET @Default = @pDefaultMentionId; 
    
    IF @pObjectType = 'A'
    BEGIN
        SELECT @MentionID = iESx.ScrubRawMentionName(ISNULL(Al.AliasName,@Default))
          FROM dESx.CompanyAlias Al
         WHERE CompanyAlias_Key = @pObjectKey;
    END
    ELSE IF @pObjectType = 'C'
    BEGIN
        SELECT @MentionID = iESx.ScrubRawMentionName(COALESCE(Cn.FullName, Cn.FirstName + ' ' + Cn.LastName,@Default))
          FROM dESx.Contact Cn
         WHERE Contact_Key = @pObjectKey;
    END
    ELSE IF @pObjectType = 'D'
    BEGIN
        SELECT @MentionID = iESx.ScrubRawMentionName(COALESCE(D.DealCode,D.DealName,@Default))
          FROM dESx.Deal D
         WHERE Deal_Key = @pObjectKey;
    END
	ELSE IF @pObjectType = 'M'
    BEGIN
        SELECT @MentionID = iESx.ScrubRawMentionName(COALESCE(Ma.MarketName,@Default))
          FROM dESx.Market Ma 
         WHERE Market_Key = @pObjectKey;
    END
    ELSE IF @pObjectType = 'O'
    BEGIN
        SELECT @MentionID = '^'+iESx.ScrubRawMentionName(COALESCE(O.OfficeName,@Default))
        FROM dESx.Office O
       WHERE Office_Key = @pObjectKey;
    END
    ELSE IF @pObjectType = 'P'
    BEGIN
        SELECT @MentionID = iESx.ScrubRawMentionName(COALESCE(an.AssetName,@Default))
        FROM dESx.AssetName an
        WHERE an.AssetName_Key = @pObjectKey
		AND an.IsCurrent = 1;
    END
	 ELSE IF @pObjectType = 'L'
    BEGIN
        SELECT @MentionID = iESx.ScrubRawMentionName(COALESCE(an.AssetName,@Default))
        FROM dESx.AssetName an
        WHERE an.AssetName_Key = @pObjectKey
		AND an.IsCurrent = 1;
    END
    RETURN @MentionID;
END
/*Test
GO

SELECT iESx.CalculateRawMentionName('L', 21,'somedefaultId')

--DEPRICATED 
SELECT iESx.CalculateRawMentionName('A', 1);
SELECT iESx.CalculateRawMentionName('C', 1);
SELECT iESx.CalculateRawMentionName('D', 1);
SELECT iESx.CalculateRawMentionName('E', 1);
SELECT iESx.CalculateRawMentionName('L', 1);
SELECT iESx.CalculateRawMentionName('M', 1);
SELECT iESx.CalculateRawMentionName('O', 1);
SELECT iESx.CalculateRawMentionName('P', 1);
SELECT iESx.CalculateRawMentionName('C', 2);
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/11/04
-- Description: Gets all the mention keys (and their primary) that belong to a specific object (there could be aliases).
-- =============================================
CREATE FUNCTION iESx.GetMentionIDAliasKeys (
     @pMentionID        iESx.TYP_Reference  -- Identifies the object whose aliases to retrieve
)
RETURNS table AS
RETURN
/*
Declare @pMentionID iESx.TYP_Reference = '@SomeEmployee|E';
--*/
    SELECT M_1.MentionID
          ,M_1.Mention_Key
          ,M_2.Mention_Key              AS Primary__Mention_Key
          ,M_2.MentionID                AS Primary__MentionID
          ,M.[ReferencedTable__Code]    AS TableCode
          ,M.[ReferencedTable__Key]     AS TableKey     --select *
      FROM dESx.Mention M
      JOIN dESx.Mention M_1
        ON M_1.ReferencedTable__Code = M.ReferencedTable__Code
       AND M_1.ReferencedTable__Key  = M.ReferencedTable__Key
      JOIN dESx.Mention M_2
        ON M_2.ReferencedTable__Code = M.ReferencedTable__Code
       AND M_2.ReferencedTable__Key  = M.ReferencedTable__Key
       AND M_2.IsPrimary = 1
     WHERE M.MentionID = @pMentionID;
/*Test
GO
select * from iESx.GetMentionIDAliasKeys('@SomeEmployee|E'     );
select * from iESx.GetMentionIDAliasKeys('@SomeEmployeeAlias|E');
select * from iESx.GetMentionIDAliasKeys('@BigCompany|A'  );
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/10/21
-- Description: Returns one row of access context as put together during login.  But only if the token isn't expired.
-- =============================================
CREATE FUNCTION iESx.GetAccessContext(
     @pAccessToken      iESx.TYP_AccessToken      -- The token to be validated
) RETURNS TABLE
AS RETURN
/*
    DECLARE @pAccessToken      iESx.TYP_AccessToken = 0x34D504DD99A04B49A22F2AAFAEB58879;
--*/

    SELECT CaAcTo.User_Key                  AS User_Key
          ,CaAcTo.CreateTime                AS LoginTime
          ,CaAcTo.User__MentionID           AS MentionID
          ,M.Mention_Key                    AS Mention_Key
          ,M.[ReferencedTable__Code]        AS UserType
          ,M.[ReferencedTable__Key]         AS TypeTableKey
          ,CaAcTo.CacheAccessToken_Key      AS Cache_Key
		  ,U.LoginName						AS LoginName
      FROM dESx.CacheAccessToken CaAcTo             -- Access by PK prevents multiple rows
      JOIN dESx.Mention M                           -- Access by PK prevents multiple rows
        ON M.MentionID = CaAcTo.User__MentionID
      JOIN dESx.[User] U                            -- Access by PK prevents multiple rows
        ON U.User_Key = CaAcTo.User_Key
     WHERE CaAcTo.AccessToken = @pAccessToken
       AND CaAcTo.CreateTime <= DATEADD(SECOND,1,GETUTCDATE())
     -- currently the token lasts 7 days
       AND CaAcTo.CreateTime >  DATEADD(DAY,-30,SYSDATETIME());

/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
SELECT @AccessToken,@MentionID;
SELECT * FROM dESx.CacheAccessToken;
SELECT * FROM iESx.GetAccessContext(@AccessToken);
--*/
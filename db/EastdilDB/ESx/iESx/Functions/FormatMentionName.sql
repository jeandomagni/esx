﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/15
-- Description: Returns the mention name without the type or leading @ sign
-- =============================================
CREATE FUNCTION iESx.FormatMentionName (
     @pObjectType char(1)
    ,@RawMentionID iESx.TYP_Reference
    ,@Uniquifier nvarchar(10)
) RETURNS iESx.TYP_Reference AS 
BEGIN
    RETURN '@'+@RawMentionID+ISNULL('-'+NULLIF(@Uniquifier,''),'')+'|'+@pObjectType;
END
/*Test
GO
SELECT iESx.FormatMentionName('A','SomeName','');
SELECT iESx.FormatMentionName('A','SomeName','1');
--*/
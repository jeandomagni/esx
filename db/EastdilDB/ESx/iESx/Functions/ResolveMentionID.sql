﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/11/05
-- Description: Returns the primary MentionID of a given mention and its Code and Key
-- =============================================
CREATE FUNCTION iESx.ResolveMentionID (
     @pMentionID        iESx.TYP_Reference
) RETURNS table AS
RETURN
/* 
    DECLARE @pMentionID iESx.TYP_Reference = '@BigAlias|A';
--*/
    SELECT M.[ReferencedTable__Code]    AS TableCode
          ,M.ReferencedTable__Key       AS TableKey
          ,M_1.MentionID                AS PrimaryMentionID
      FROM dESx.Mention M
      LEFT JOIN dESx.Mention M_1
        ON M_1.ReferencedTable__Code = M.ReferencedTable__Code
       AND M_1.ReferencedTable__Key  = M.ReferencedTable__Key
       AND M_1.IsPrimary = 1
     WHERE M.MentionID = @pMentionID;
/*Test
GO
SELECT * FROM iESx.ResolveMentionID('@SomeEmployeeAlias|E');
SELECT * FROM iESx.ResolveMentionID('@BigAlias|A');
SELECT * FROM iESx.ResolveMentionID('@NoPrimary|D');
SELECT * FROM iESx.ResolveMentionID('@Bad|D');
--*/
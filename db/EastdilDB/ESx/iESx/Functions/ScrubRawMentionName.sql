﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/13
-- Description: Scrubs illegal characters from what is passed to create the text part of a valid MentionID
-- =============================================
CREATE FUNCTION iESx.ScrubRawMentionName (@pText nvarchar(200))
RETURNS nvarchar(200) AS 
BEGIN
    RETURN (SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(NULLIF(@pText,'')
                  ,';',''),'\',''),'?',''),'/',''),'&',''),'''',''),',',''),'*',''),'&',''),'.',''),'|',''),' ',''),'@',''));
END
/*Test
GO
SELECT iESx.ScrubRawMentionName('TEST; to insure * &.|');
--*/
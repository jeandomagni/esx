﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/15
-- Description: Calculates a uniquifier if needed for the given proposed MentionID
-- =============================================
CREATE FUNCTION iESx.GetMentionNameUniquifier (     
     @pObjectType   char(1)
    ,@RawMentionID  iESx.TYP_Reference
) 
RETURNS nvarchar(3) AS 
BEGIN
    DECLARE @Uniquifier nvarchar(3) = '';
    WHILE EXISTS (SELECT MentionID FROM dESx.Mention WHERE MentionID = iESx.FormatMentionName(@pObjectType,@RawMentionID,@Uniquifier))
        SET @Uniquifier = CAST(@Uniquifier AS int)+1;
    RETURN @Uniquifier;
END
/*Test
GO
SELECT '"'+ iESx.GetMentionNameUniquifier('E','SomeEmployee')+'"';
SELECT '"'+ iESx.GetMentionNameUniquifier('A','Otherwise')+'"';
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/11/03
-- Description: Returns comments created upon a specific object identified by its MentionID
-- =============================================
CREATE FUNCTION iESx.GetMentionObjectComments (
     @pAccessToken      iESx.TYP_AccessToken    -- The token to use for security checks
    ,@pMentionID        iESx.TYP_Reference      -- A legal MentionID identifying the object upon which the comments were created.
    ,@pFilterText       nvarchar(1000)          -- Text that the comment must contain to be returned
)
RETURNS @Result table (
    Comment_Key       int
   ,CreateTime        datetime
   ,UpdateTime        datetime
   ,CommentText       nvarchar(MAX)
   ,PrimaryMentionID  iESx.TYP_Reference
   ,TableCode         char
   ,CreatorName       nvarchar(100)
   ,CreatorMentionID  iESx.TYP_Reference
) AS
BEGIN
/*
    DECLARE @pMentionID     iESx.TYP_Reference   = '@SomeEmployee|E';
    DECLARE @pAccessToken   iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
    DECLARE @pFilterText    nvarchar(1000)       = 'tag';
    DECLARE @Result table (Comment_Key       int
                          ,CreateTime        datetime
                          ,UpdateTime        datetime
                          ,CommentText       nvarchar(MAX)
                          ,PrimaryMentionID  iESx.TYP_Reference
                          ,TableCode         char);
--*/

    -- Employee comments are private so @pMentionIDs of employees return nothing unless @pAccessToken belongs to that employee.
    DECLARE @ObjectMentionID    iESx.TYP_Reference;
    DECLARE @ObjectType         char;
    SELECT @ObjectMentionID = RMId.PrimaryMentionID
          ,@ObjectType      = RMId.TableCode
          ,@pFilterText     = '%'+ISNULL(@pFilterText,'')+'%'
      FROM iESx.ResolveMentionID(@pMentionID) RMId;
    IF @ObjectType = 'E'
    BEGIN
        DECLARE @UserMentionID      iESx.TYP_Reference;
        SELECT @UserMentionID   = GAcCx.MentionID
          FROM iESx.GetAccessContext(@pAccessToken) GAcCx;
        IF @UserMentionID <> ISNULL(@ObjectMentionID,'')
            SET @pMentionID = NULL;
    END;

    INSERT INTO @Result(Comment_Key,CreateTime,UpdateTime,CommentText,PrimaryMentionID,TableCode,CreatorName,CreatorMentionID)
    SELECT Cm.Comment_Key
          ,Cm.CreateTime
          ,Cm.UpdateTime
          ,Cm.CommentText
          ,x.Primary__MentionID
          ,x.TableCode
          ,COALESCE(C.FullName,'')  AS CreatorName
          ,M.MentionID                  AS CreatorMentionID
      FROM iESx.GetMentionIDAliasKeys(@pMentionID) x
      JOIN dESx.Comment Cm
        ON Cm.[Implicit__Mention_Key] = x.Mention_Key
      JOIN dESx.[User] U
        ON U.User_Key = Cm.Create__User_Key
      LEFT JOIN dESx.Contact C
			JOIN dESx.Mention M ON C.Contact_Key = M.Contact_Key
        ON C.Contact_Key = U.Contact_Key
    WHERE Cm.CommentText LIKE @pFilterText;

    RETURN;
END;
/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
SELECT @AccessToken,@MentionID;
select * from iESx.GetMentionObjectComments(@AccessToken,'@SomeEmployee|E'      ,null);
select * from iESx.GetMentionObjectComments(@AccessToken,'@SomeEmployeeAlias|E' ,null);
select * from iESx.GetMentionObjectComments(@AccessToken,'@BigCompany|A'        ,null);
EXEC xESxApp.Login 'User2',@AccessToken OUTPUT,@MentionID OUTPUT;
select * from iESx.GetMentionObjectComments(@AccessToken,'@SomeEmployee|E'      ,null);
select * from iESx.GetMentionObjectComments(@AccessToken,'@SomeEmployeeAlias|E' ,null);
select * from iESx.GetMentionObjectComments(@AccessToken,'@BigCompany|A'        ,null);
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/15
-- Description: Returns the mention name without the type or leading @ sign
-- =============================================
CREATE FUNCTION iESx.ParseMentionName (@pMentionID iESx.TYP_Reference)
RETURNS @Result table(RawName iESx.TYP_Reference, ObjectType char(1), ErrorText nvarchar(200)) AS 
BEGIN
    DECLARE @Err    nvarchar(200)       = '';

    IF LEN(@pMentionID)<4
        SET @Err = 'Expecting "@<rawName>|<typeChar>" found '+'"'+@pMentionID+'"';

    DECLARE @At     nchar(1)            = SUBSTRING(@pMentionID,1,1);
    DECLARE @Raw    iESx.TYP_Reference  = SUBSTRING(@pMentionID,2,LEN(@pMentionID)-3);
    DECLARE @Bar    nchar(1)            = SUBSTRING(@pMentionID,LEN(@pMentionID)-1,1);
    DECLARE @Type   nchar(1)            = SUBSTRING(@pMentionID,LEN(@pMentionID),1);
    DECLARE @Clean  iESx.TYP_Reference  = iESx.ScrubRawMentionName(@Raw);

    IF @Err = '' AND @At <> '@' OR @Bar <> '|'
        SET @Err = 'Expecting format: "@<rawName>|<typeChar>" found "'+@At+@Raw+@Bar+@Type+'"';
    IF @Err = '' AND @Clean <> @Raw
        SET @Err = 'Illegal characters in name: "'+@Raw+'" legal characters are "'+@Clean+'"';

    INSERT INTO @Result
            (RawName,ObjectType,ErrorText)
    VALUES  (@Raw,@Type,@Err);
    RETURN;
END
/*Test
GO
SELECT * from iESx.ParseMentionName('@Any|A');
SELECT * from iESx.ParseMentionName('@|A');
SELECT * from iESx.ParseMentionName('@Any&All|A');
SELECT * from iESx.ParseMentionName('Any|A');
SELECT * from iESx.ParseMentionName('@Any!A');
--*/
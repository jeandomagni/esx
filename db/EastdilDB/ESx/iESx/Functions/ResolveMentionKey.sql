﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/18
-- Description: Returns the primary MentionID of a given mention and its Code and Key
-- =============================================
CREATE FUNCTION iESx.ResolveMentionKey (
     @pMentionKey        int
) RETURNS table AS
RETURN
/* 
    DECLARE @pMentionID iESx.TYP_Reference = '@BigAlias|A';
--*/
    SELECT M.[ReferencedTable__Code]    AS TableCode
          ,M.ReferencedTable__Key       AS TableKey
          ,M_1.MentionID                AS PrimaryMentionID      -- select *
      FROM dESx.Mention M
      LEFT JOIN dESx.Mention M_1
        ON M_1.ReferencedTable__Code = M.ReferencedTable__Code
       AND M_1.ReferencedTable__Key  = M.ReferencedTable__Key
       AND M_1.IsPrimary = 1
     WHERE M.Mention_Key = @pMentionKey
/*Test
GO
SELECT * FROM iESx.ResolveMentionKey(1);
SELECT * FROM iESx.ResolveMentionKey(2);
SELECT * FROM iESx.ResolveMentionKey(3);
SELECT * FROM iESx.ResolveMentionKey(4);
--*/
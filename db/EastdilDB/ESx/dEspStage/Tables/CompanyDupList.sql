﻿CREATE TABLE [dEspStage].[CompanyDupList] (
    [CompanyDupID]  INT            IDENTITY (1, 1) NOT NULL,
    [CompanyID]     INT            NOT NULL,
    [CompanyName]   NVARCHAR (200) NULL,
    [DBAName]       NVARCHAR (200) NULL,
    [SansName]      NVARCHAR (200) NULL,
    [SquashedName]  NVARCHAR (200) NULL,
    [DuplicateOfID] BIGINT         NULL,
    [Canonical]     BIGINT         NULL
);




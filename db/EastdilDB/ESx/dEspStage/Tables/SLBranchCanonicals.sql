﻿CREATE TABLE [dEspStage].[SLBranchCanonicals] (
    [CompanyID]            INT NOT NULL,
    [CanonicalID]          INT NOT NULL,
    [SLBranchCanonicalsID] INT IDENTITY (1, 1) NOT NULL
);






GO
CREATE NONCLUSTERED INDEX [IX_SLBranchCanonicals_CanonicalID]
    ON [dEspStage].[SLBranchCanonicals]([CanonicalID] ASC);


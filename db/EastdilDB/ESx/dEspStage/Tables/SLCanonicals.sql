﻿CREATE TABLE [dEspStage].[SLCanonicals] (
    [CanonicalID]   INT            IDENTITY (1, 1) NOT NULL,
    [CanonicalName] NVARCHAR (200) NULL,
    CONSTRAINT [PK_SLCanonicals_1] PRIMARY KEY CLUSTERED ([CanonicalID] ASC)
);




﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/09
-- Description: Cleans and organizes ESP data for presentation in rESxStage
-- =============================================
CREATE PROCEDURE [dEspStage].[RebuildSchemaData]
AS 
BEGIN
	EXEC [Global].DeleteAllSchemaData 'dEspStage';
	EXEC dEspStage.DedupAndLoadCompanyData;
END;
/*Test
go
EXEC dEspStage.RebuildSchemaData;
*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/11
-- Description: Rebuild company and alias data from ESP data
-- =============================================
CREATE PROCEDURE [dEspStage].[DedupAndLoadCompanyData]
AS
    SET NOCOUNT Off;
    -- Branches need to change to correct hierarchy errors so make a copy.
    TRUNCATE TABLE dEspStage.SLBranchCanonicals;
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
    SELECT DISTINCT
           sbc.CompanyID
          ,sbc.CanonicalID
      FROM dExcelETL.SLBranchCanonicals sbc;
    -- select * from dEspStage.SLBranchCanonicals;
    
    -- Canonicals need to change to correct hierarchy errors so make a copy.
    -- CanonicalID is an identity column so future inserts have distinct values but the existing rows must maintain existing keys.
    TRUNCATE TABLE dEspStage.SLCanonicals;
    SET IDENTITY_INSERT dEspStage.SLCanonicals ON;
    INSERT INTO dEspStage.SLCanonicals(CanonicalID,CanonicalName)
    SELECT sbc.CanonicalID
          ,LTRIM(sbc.CanonicalName) CanonicalName
      FROM dExcelETL.SLCanonicals sbc
     WHERE sbc.CanonicalID IN (SELECT CanonicalID FROM dEspStage.SLBranchCanonicals);
    SET IDENTITY_INSERT dEspStage.SLCanonicals OFF;
    -- select * from dEspStage.SLCanonicals;
    
    -- This allows me to change company names and their dba names to correct misspellings and mis-mappings
    TRUNCATE TABLE dEspStage.CompanyDupList;
    INSERT INTO dEspStage.CompanyDupList(CompanyID,CompanyName,DBAName,SansName,SquashedName,DuplicateOfID,Canonical)
    SELECT Co.CompanyID
          ,LTRIM(Co.CompanyName)CompanyName
          ,LTRIM(Co.DBAName)DBAName
          ,' '+LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CompanyName+' '
                        ,',',' '),'.',' '),';',' '),':',' '),'`',''),'''',''),'-',' ')
                        ,' inc ',' '),' pllc ',' '),' l l c ',' '),' llc ',' '),' co ',' '),' corp ',' '),' ltd ',' '),' lp ',''),' gmbh ',' '),' The ',' ')
                        ,'  ',' '),'  ',' ')))+' '	AS SansName
          ,Co.CompanyName							AS SquashedName
          ,CAST(NULL AS bigint)						AS DuplicateOfID
          ,CAST(slbc.CanonicalID AS bigint)			AS Canonical        -- select *
      FROM rESP.Companies Co
      LEFT JOIN dEspStage.SLBranchCanonicals slbc
        ON slbc.CompanyID = Co.CompanyID
      WHERE LEN(ISNULL(Co.CompanyName,'')) > 2
         OR slbc.CompanyID IS NOT null;
    -- select * from dEspStage.CompanyDupList;
    
    EXEC dEspStage.FixCompanySourceSpelling;
    
    -- Fix the Sans and Squashed names now that spelling is changed
    UPDATE dEspStage.CompanyDupList
       SET SansName = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CompanyName+' '
                        ,',',' '),'.',' '),';',' '),':',' '),'`',' '),'''',' '),'-',' ')
                        ,' inc ',' '),' pllc ',' '),' l l c ',' '),' llc ',' '),' co ',' '),' corp ',' '),' ltd ',' '),' lp ',''),' gmbh ',' '),' The ',' ')
                        ,'  ',' '),'  ',' ')))
          ,SquashedName = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CompanyName+' '
                          ,',',' '),'.',' '),';',' '),':',' '),'`',' '),'''',' '),'-',' ')
                          ,' inc ',''),' pllc ',''),' l l c ',''),' llc ',''),' co ',''),' corp ',''),' ltd ',''),' lp ',''),' gmbh ',''),' The ',' ')
                          ,'  ',''),' ','')));
    
    EXEC dEspStage.FixCompanyAnomaliesRound1;
    EXEC dEspStage.FixCompanyAnomaliesRound2;
    
    -- Remove duplicate CompanyDupList entries created by fixing misspellings and name changes in anomaly fixes
    WITH x AS (
        SELECT dl.CompanyID,dl.Canonical
          FROM dEspStage.CompanyDupList dl
         GROUP BY dl.CompanyID,Canonical
        HAVING COUNT(*) > 1
    ),y AS (
        SELECT MIN(CompanyDupID) CompanyDupID
          FROM dEspStage.CompanyDupList dl
          JOIN x
            ON x.CompanyID = dl.CompanyID
           AND x.Canonical = dl.Canonical
         GROUP BY dl.CompanyID,dl.Canonical
    ) --/*
    DELETE dl           --*/ select *
      FROM dEspStage.CompanyDupList dl
      JOIN x
        ON x.CompanyID = dl.CompanyID
       AND x.Canonical = dl.Canonical
     WHERE CompanyDupID NOT IN (SELECT y.CompanyDupID FROM y);
    
    -- Remove duplicate branch links already in the data but now able to be cleaned up because above logic identified which to keep.
    WITH x AS (
        SELECT CompanyID
              ,CanonicalID
              ,MIN(SLBranchCanonicalsID) saveMe 
          FROM dEspStage.SLBranchCanonicals 
         GROUP BY CompanyID,CanonicalID 
        HAVING COUNT(*)>1
    ) --/* 
    DELETE slbc
    --*/SELECT slbc.CompanyID,slbc.CanonicalID,x.saveMe,slbc.SLBranchCanonicalsID,slc.CanonicalName,dl.CompanyName,dl.DBAName,CASE WHEN slbc.SLBranchCanonicalsID = x.saveMe THEN 'save' ELSE ''END stat
      FROM dEspStage.SLBranchCanonicals slbc
      JOIN x
        ON x.CanonicalID = slbc.CanonicalID
       AND x.CompanyID = slbc.CompanyID
      JOIN dEspStage.SLCanonicals slc
        ON slc.CanonicalID = x.CanonicalID
      JOIN dEspStage.CompanyDupList dl
        ON dl.CompanyID = x.CompanyID
     WHERE slbc.SLBranchCanonicalsID <> x.saveMe
    -- ORDER BY slbc.CanonicalID,slbc.CompanyID
    ;
    
    -- Mitsubishi defers to Rockefeller because the concept of one belonging to the other is in a higher level in the hierarchy
    WITH x AS (SELECT CompanyID,COUNT(*)cnt FROM dEspStage.SLBranchCanonicals GROUP BY CompanyID HAVING COUNT(*)>1)
    --/* 
    DELETE sbc
    --*/ SELECT sbc.CompanyID,sbc.CanonicalID,slc.CanonicalName,x.cnt
      FROM dEspStage.SLBranchCanonicals sbc
      JOIN x
        ON sbc.CompanyID = x.CompanyID
      JOIN dEspStage.SLCanonicals slc
        ON slc.CanonicalID = sbc.CanonicalID
     WHERE slc.CanonicalName IN ('Mitsubishi');
    
    -- All the canonical updates need to be reflected in CompanyDupList
    UPDATE dEspStage.CompanyDupList SET Canonical=NULL;
    UPDATE dl
       SET Canonical= slbc.CanonicalID --*/ select *
      FROM dEspStage.CompanyDupList dl
      JOIN dEspStage.SLBranchCanonicals slbc
            ON slbc.CompanyID = dl.CompanyID;
            
    -- Collapse similar names into DuplicateOfID
    WITH x AS (
        SELECT MIN(dl.CompanyID)    CompanyID
              ,MIN(dl.CompanyName)  CompanyName
              ,MIN(dl.SansName)     SansName
              ,MIN(dl.DBAName)      DBAName
              ,dl.SquashedName      SquashedName
          FROM dEspStage.CompanyDupList dl
         WHERE dl.DuplicateOfID IS NULL
         GROUP BY dl.SquashedName
         HAVING COUNT(*) > 1
    ), y AS (
        SELECT x.CompanyID AS CanonicalID
              ,dl.CompanyID UpdateID
              ,x.SquashedName
              ,dl.CompanyName
          FROM x
          JOIN dEspStage.CompanyDupList dl
            ON dl.SquashedName = x.SquashedName
    )-- /*
    UPDATE dl
       SET DuplicateOfID = y.CanonicalID --*/ select *
      FROM dEspStage.CompanyDupList dl
      JOIN y
        ON y.UpdateID = dl.CompanyID;
    
    -- These are not actually duplicates See Anomalies P4 in AppDev_FixCompanyAnomaliesRound1
    UPDATE dEspStage.CompanyDupList 
       SET DuplicateOfID = NULL 
     WHERE Canonical IN (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName in ('Metropolitan Realty','Metropolitan Realty Corp.'));
    
    -- These are unobvious duplicates (they have the same legal name)
    UPDATE dEspStage.CompanyDupList 
       SET DuplicateOfID = (SELECT CompanyID FROM dEspStage.CompanyDupList dl WHERE dl.CompanyName = 'Taiping Asset Management Co., Ltd') -- select * from dEspStage.CompanyDupList
     WHERE CompanyName IN ('China Taiping','Taiping Asset Management Co., Ltd');
    
    -- Set companies with names identical to Canonical names but not tied to them.
    UPDATE dl
       SET Canonical = CanonicalID
      FROM dEspStage.CompanyDupList dl
      JOIN dEspStage.SLCanonicals slc
        ON slc.CanonicalName = dl.CompanyName
     WHERE dl.DBAName IS NULL 
       AND dl.Canonical IS NULL;
    
    -- Set DBA of companies with names identical to DBA names but not tied to them.
    UPDATE dl
       SET dl.DBAName = dl2.DBAName     -- select *
      FROM dEspStage.CompanyDupList dl
      JOIN dEspStage.CompanyDupList dl2
        ON dl2.DBAName = dl.CompanyName
     WHERE dl.DBAName IS NULL 
       AND dl.Canonical IS NULL;
    
    -- Add CanonicalID to duplicates without them
    WITH x AS (
        SELECT dl.DuplicateOfID,MAX(dl.Canonical) Canonical
          FROM dEspStage.CompanyDupList dl
         WHERE dl.DuplicateOfID IS NOT NULL
         GROUP BY dl.DuplicateOfID
        HAVING COUNT(*)> 1 AND COUNT(dl.Canonical) > 0
           AND COUNT(*)>COUNT(dl.Canonical)
    )
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
    SELECT dl.CompanyID,x.Canonical
      FROM x
      JOIN dEspStage.CompanyDupList dl
        ON dl.DuplicateOfID = x.DuplicateOfID  
     WHERE dl.Canonical IS NULL
     ORDER BY x.DuplicateOfID;
    
    -- Insure CompanyDupList has them too
    UPDATE dl
       SET Canonical= slbc.CanonicalID --*/ select *
      FROM dEspStage.CompanyDupList dl
      JOIN dEspStage.SLBranchCanonicals slbc
            ON slbc.CompanyID = dl.CompanyID
     WHERE dl.Canonical IS NULL;
    
    -- Add DBA names to Canonical groups where some have names and some don't
    WITH x AS (
        SELECT DISTINCT dl.Canonical,dl.DBAName
          FROM dEspStage.SLBranchCanonicals slbc
          JOIN dEspStage.CompanyDupList dl
            ON dl.CompanyID = slbc.CompanyID
         WHERE dl.DBAName IS NOT NULL
    ) --/*
    UPDATE dl
       SET dl.DBAName = x.DBAName
        -- */ SELECT dl.CompanyID,dl.CompanyName,dl.DuplicateOfID,dl.Canonical,x.DBAName
          FROM dEspStage.CompanyDupList dl 
          JOIN x
            ON x.Canonical = dl.Canonical
         WHERE dl.DBAName IS NULL 
           AND dl.Canonical IS NOT NULL;
    
    -- Remove entirely duplicate rows.  This can happen after adjustments to rows previously not duplicates.
    DELETE dEspStage.CompanyDupList
     WHERE CompanyID IN (SELECT dl.CompanyID
                           FROM dEspStage.CompanyDupList dl
                          GROUP BY dl.CompanyID
                         HAVING COUNT(*)>1)
      AND CompanyDupID NOT IN (SELECT MIN(dl.CompanyDupID)
                          FROM dEspStage.CompanyDupList dl
                         GROUP BY dl.CompanyID
                        HAVING COUNT(*)>1);

/*Test
GO
exec dEspStage.DedupAndLoadCompanyData;
--*/
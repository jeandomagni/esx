﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/19
-- Description: Fixes CompanyDupList, SlBranchCanonical and SlCanonical anomalies found in round-2 of the company hierarchy analysis
-- =============================================
CREATE PROCEDURE [dEspStage].[FixCompanyAnomaliesRound2]
AS
    -- fixes to Anomalies2 P1   
    UPDATE dEspStage.CompanyDupList set DBAName = 'EAGLEGROUP' WHERE DBAName = 'EAGLEGROUPLLC';
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE CompanyName = 'Colony, Inc.';
    UPDATE dEspStage.CompanyDupList set DBAName = 'SUMITOMOGROUP' WHERE DBAName = 'SUMITOMO';
    UPDATE dEspStage.SLCanonicals set CanonicalName = 'Mitsui Sumitomo Insurance' WHERE CanonicalName = 'Mitsuo Sumitomo Insurance';
    
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('Sumitomo Group');
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'Sumitomo Group') 
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'SUMITOMOGROUP' 
                                 AND dl.Canonical IS NULL;
    
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'TPG Capital') 
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'TPG' 
                                 AND dl.Canonical IS NULL;
    
    
    -- fixes to Anomalies2 P2
    
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'Hilton Hotels') 
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'BLACKSTONE' 
                                 AND dl.Canonical IS NULL
                                 AND (dl.CompanyName LIKE '%Hilton%'
                                   OR dl.CompanyName LIKE '%Doubletree%');
    
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'GE') 
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'BLACKSTONE' 
                                 AND dl.Canonical IS NULL
                                 AND (dl.CompanyName LIKE '%General Electric%'
                                   OR dl.CompanyName LIKE '%GE %');
    
    UPDATE dEspStage.CompanyDupList SET DBAName = NULL WHERE CompanyName = 'Brixmor Property Group';
    UPDATE dEspStage.CompanyDupList SET DBAName = NULL WHERE CompanyName = 'Carr Properties';
    
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'Blackstone Group') 
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'BLACKSTONE' 
                                 AND dl.Canonical IS NULL
                                 AND (dl.CompanyName IN ('CarrAmerica','CarrAmerica Realty Corporation','Equity Office','Wilson Equity Office LLC'));

    -- Fixes to rows added after Aug 2015 (the date of the backup upon which the older rounds were based) - by guesswork rather than Eastdil input.

    DELETE dEspStage.SLBranchCanonicals 
      WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Rockefeller Group')      
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE CompanyName = 'TA Associates Realty');

    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Equity Commonwealth') -- select * from dEspStage.SLBranchCanonicals
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE CompanyName = 'Equity Commonwealth');

    UPDATE dEspStage.CompanyDupList SET DBAName = 'ESSEX' WHERE CompanyName = 'Essex Property Trust';
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Essex Property Trust') -- select * from dEspStage.SLBranchCanonicals
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE CompanyName = 'Essex Property Trust');

    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Criterion Real Estate Capital') -- select * from dEspStage.SLBranchCanonicals
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE CompanyName = 'Criterion Real Estate Capital');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Criterion Capital';

    UPDATE dEspStage.CompanyDupList set DBAName = 'BAML' WHERE DBAName = 'U.S.TRUST' AND CompanyName = 'Bank of America, N.A.';

    UPDATE dEspStage.CompanyDupList set DBAName = 'CAPITALONE' WHERE DBAName = 'CAPITALONEBANK';

    UPDATE dEspStage.CompanyDupList set DBAName = 'JPMORGANCHASE' WHERE DBAName IS null AND CompanyName = 'J.P. Morgan';
    
    UPDATE dEspStage.CompanyDupList set DBAName = 'FOWLERPROPERTY' WHERE DBAName = 'FPA' AND CompanyName = 'Fowler Property Acquisitions';

    -- These put upsidedown Canonical/DBA relationships back rightside up by elimination or role flipping

    UPDATE dEspStage.CompanyDupList set DBAName = 'ATLANTICREALTY' WHERE DBAName = 'ATLANTICREALTYP';
    
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('JPMorgan Asset Management');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'JPMorgan Asset Management')
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'JPMAM');
    UPDATE dEspStage.CompanyDupList set DBAName = 'JPMORGANCHASE' WHERE DBAName = 'JPMAM';
    
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('Emmes Realty');
    UPDATE dEspStage.SLCanonicals SET CanonicalName = 'Vanbarton Group' WHERE CanonicalName = 'Emmes Asset';
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Emmes Realty')
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'EMMES');
    UPDATE dEspStage.CompanyDupList set DBAName = 'Emmes Asset' WHERE DBAName = 'EMMES';
    UPDATE dEspStage.CompanyDupList set DBAName = 'Emmes Asset' WHERE DBAName = 'VANBARTON';
    
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('Related Beal');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Related Beal')
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'RELATEDBEAL');
    UPDATE dEspStage.CompanyDupList set DBAName = 'RELATEDCOMPANIES' WHERE DBAName = 'RELATEDBEAL';

    -- These I noticed when trying to load only canonicals.  The DBA matched a name that was not a child of itself
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'KRE Group') -- select * from dEspStage.SLBranchCanonicals
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE CompanyName = 'KRE Group');

    UPDATE dEspStage.CompanyDupList set DBAName = 'METROPOLITANREALTYAssociates'WHERE DBAName = 'METROPOLITANREALTY';   -- Matches non-associated METROPOLITAN REALTY
    UPDATE dEspStage.CompanyDupList set DBAName = 'FlynnProperties'             WHERE DBAName = 'Flynn' ;   -- Matches non-associated The Flynn Co.
    UPDATE dEspStage.CompanyDupList set DBAName = 'AresManagement'              WHERE DBAName = 'ARES'  ;   -- Matches non-associated Ares Inc.
/*Test
GO
exec dEspStage.FixCompanyAnomaliesRound2;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/19
-- Description: Fixes CompanyDupList, SlBranchCanonical and SlCanonical anomalies found in round-1 of the company hierarchy analysis
-- =============================================
CREATE PROCEDURE [dEspStage].[FixCompanyAnomaliesRound1]
AS
    -- Fixes I found necessary after round2 was complete
    UPDATE dEspStage.CompanyDupList set DBAName = 'BARCLAYS' where CompanyName = 'Barclays Global Investors, Inc.';
    UPDATE dEspStage.CompanyDupList set DBAName = 'ALLIANCERESIDENTIAL' where CompanyName in ('Alliance Residential','Alliance Residential Company');
    DELETE dEspStage.SLBranchCanonicals WHERE CompanyID IN (SELECT dl.CompanyID FROM dEspStage.CompanyDupList dl WHERE dl.CompanyName in ('VEREIT, Inc.','VEREIT'));
    UPDATE dEspStage.CompanyDupList set DBAName = 'CITGROUP' where DBAName = 'CIT';
    DELETE dEspStage.SLBranchCanonicals WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'OneWest Bank')
                                      AND CompanyID IN (SELECT dl.CompanyID FROM dEspStage.CompanyDupList dl WHERE dl.CompanyName = 'CIT Group Inc.');

    -- fixes to anomalies P1
    UPDATE dEspStage.CompanyDupList set DBAName = null where CompanyName = 'Community Bank';
    UPDATE dEspStage.CompanyDupList set DBAName = 'FIFTHTHIRDBANK' where DBAName = 'FIFTHTHIRD';
    UPDATE dEspStage.CompanyDupList set DBAName = 'JONESDAY' where DBAName = 'JDRP';
    UPDATE dEspStage.CompanyDupList set DBAName = 'OVERSEASUNION' where DBAName = 'THEAMERICAS';
    
    UPDATE dEspStage.SLBranchCanonicals SET CompanyID = (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE CompanyName = 'U.S. Realty Advisors, LLC' AND DBAName = 'USREALTYADVISORS')
            WHERE CompanyID = (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE CompanyName = 'US Realty Advisors' AND DBAName = 'USREALTY');
    DELETE dEspStage.CompanyDupList WHERE CompanyName = 'US Realty Advisors' AND DBAName = 'USREALTY';
    
    -- fixes to anomalies P2
    UPDATE dEspStage.CompanyDupList set DBAName = 'ALBERTAINVESTMENT' where CompanyName = 'Alberta Investment Management Corp.';
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Apartment Investment and Management Company')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Alberta Investment Management Corporation')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'AIMCO');
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Ares Management')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Apollo Global Management')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'ARES');
    UPDATE dEspStage.CompanyDupList set DBAName = 'BLACKSTONE' WHERE DBAName = 'GE' OR DBAName = 'BIOMED';
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE DBAName = 'CARLYLE';
    DELETE dEspStage.SLBranchCanonicals WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Brookfield')
                                      AND CompanyID IN (SELECT dl.CompanyID FROM dEspStage.CompanyDupList dl WHERE dl.CompanyName = 'Thayer Lodging Group, Inc.');
    --UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE DBAName = 'COLONY';
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'CW Capital')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'CWCapital');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'CWCapital';
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Hypo Real Estate')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Deutsche Pfandbriefbank');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Deutsche Pfandbriefbank';
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'DLJ Real Estate Capital Partners')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Credit Suisse')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'DLJ');
    -- fho and rockwood will automatically roll up to DTZ without change
    
    UPDATE dEspStage.CompanyDupList set DBAName = 'REITMANAGEMENTRESEARCH' WHERE DBAName = 'EQUITYCOMMONWEALTH'
            AND Canonical IN (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Commonwealth REIT');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'REIT Management & Research')
            WHERE CanonicalID IN (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Commonwealth REIT');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Commonwealth REIT';
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'REIT Management & Research') AS CanonicalID  -- select *
          FROM dEspStage.CompanyDupList dl WHERE dl.CompanyName = 'RMR Properties Corporation' AND dl.Canonical IS NULL;
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('The RMR Group');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'The RMR Group')
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'RMR');
    UPDATE dEspStage.CompanyDupList set DBAName = 'The RMR Group' WHERE DBAName = 'REITMANAGEMENTRESEARCH';
    UPDATE dEspStage.CompanyDupList set DBAName = 'The RMR Group' WHERE DBAName = 'RMR';
    
    UPDATE dEspStage.CompanyDupList set DBAName = 'COMMERZBANK' WHERE DBAName = 'EUROHYPO';
    DELETE dEspStage.SLBranchCanonicals WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Eurohypo');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Eurohypo';
    DELETE dEspStage.SLBranchCanonicals WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Federal National Mortgage Association');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Federal National Mortgage Association';
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE DBAName = 'FULCRUM';
    -- is 'Fulcrum LLC' part of 'Fulcrum Asset Advisors, LLC','Fulcrum Hospitality LLC' or is there a 3rd FULCRUM canonical.
    
    UPDATE dEspStage.CompanyDupList set DBAName = 'BAML' WHERE Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Merrill Lynch');
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Goldman Sachs')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Archon Group');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Archon Group';
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE DBAName = 'GRAMERCY';
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Global Hyatt');
    UPDATE dEspStage.CompanyDupList set DBAName = 'ONEWESTBANK' WHERE DBAName = 'INDYMAC';
    DELETE dEspStage.SLBranchCanonicals WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Indymac Bank');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Indymac Bank';
    
    DELETE dEspStage.SLBranchCanonicals WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'SPAULDINGSLYE');
    UPDATE dEspStage.CompanyDupList SET Canonical = (SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'Spaulding & Slye') WHERE DBAName = 'SPAULDINGSLYE';
    
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT DISTINCT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'Spaulding & Slye') AS CanonicalID  -- select *
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'SPAULDINGSLYE';
    UPDATE dEspStage.CompanyDupList set DBAName = 'LASALLE' WHERE DBAName in ('SPAULDINGSLYE','JLL');
    
    UPDATE dEspStage.CompanyDupList set DBAName = 'LONESTAR' WHERE DBAName = 'HUDSONADVISORS';
    
    UPDATE dEspStage.SLCanonicals SET CanonicalName = 'Destination Hotels' WHERE CanonicalName = 'Lowe Hospitality';
    
    --Roseland Property Company will roll up to Mack-Cali Realty automatically
    
    DELETE dEspStage.SLBranchCanonicals WHERE CompanyID in (SELECT dl.CompanyID FROM dEspStage.CompanyDupList dl WHERE dl.CompanyName = 'MAGNA/EL CON, L.C.')
            AND CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'El Con');
    
    --John Hancock will automatically roll up to Manulife Financial
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE DBAName = 'PARALLELCAPITAL'
            AND Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Parallel Asset Management');
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Parallel Capital');
    UPDATE dEspStage.SLCanonicals SET CanonicalName = 'Parallel Capital Partners' WHERE CanonicalName = 'Parallel Capital'
    
    DELETE dEspStage.SLBranchCanonicals WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Perseus Realty Partners');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Perseus Realty Partners';
    UPDATE dEspStage.SLCanonicals SET CanonicalName = 'Perseus Realty Partners' WHERE CanonicalName = 'Perseus Realty'
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Bixby Land Company');
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Boas Family');
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Dexus Property Group');
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Digital Media');
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Morningstar Properties');
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Principal Global Investors')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Principal Enterprise Capital');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Principal Enterprise Capital';
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Prudential')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Prudential Capital Group');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Prudential Capital Group';
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Pyramid Hotel Group')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Pyramid Advisors');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Pyramid Advisors';
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'RAIT Financial Trust')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'RAIT Investment Trust');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'RAIT Investment Trust';
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Royal Bank of Canada')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Capital Advisors');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Capital Advisors';
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Select Income REIT');
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Senior Housing Properties Trust');
    
    -- Spaulding & Slye dealt with above
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'iStar')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Starwood Capital Group')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'STARWOODFINANCIAL');
    UPDATE dEspStage.CompanyDupList set DBAName = 'STARWOODCAPITAL' WHERE DBAName in ('STARWOODFINANCIAL','STARWOODHOTELS','ISTAR');
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Sterling American Properties')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'State University of NY');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'State University of NY';
    
    UPDATE dEspStage.SLCanonicals SET CanonicalName = 'Stonehenge NYC' WHERE CanonicalName = 'Stonehenge Partners';
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Stonehenge NYC')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Stonehenge Real Estate');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Stonehenge Real Estate';
    
    --UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical in (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName IN ('Mitsuo Sumitomo Insurance','Sumitomo Mitsui Banking Corporation','Sumitomo Mitsui Trust Bank'));
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical in (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName IN ('Swig Company','Swig Equities'));
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical in (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName IN ('Vulcan','Vulcan Real Estate Partners'));
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Winthrop Realty Trust')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'First Winthrop Realty');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'First Winthrop Realty';
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical in (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName IN ('Winthrop Management'));
    
    -- fixes to anomalies P3
    UPDATE dEspStage.SLCanonicals SET CanonicalName = 'Sun Communities' WHERE CanonicalName = 'American Land Lease';
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE Canonical in (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName IN ('Sun Communities'));
    
    DELETE dEspStage.SLBranchCanonicals WHERE CompanyID in (SELECT dl.CompanyID FROM dEspStage.CompanyDupList dl WHERE DBAName = 'ACCESSO');
    UPDATE dEspStage.CompanyDupList SET DBAName = 'BEACONCAPITAL' WHERE CompanyName = 'Beacon Capital Partners, LLC' AND DBAName IS NULL;
    
    DELETE dEspStage.SLBranchCanonicals WHERE CompanyID in (SELECT dl.CompanyID FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'CANTORFUNDS');
    
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('The Capbridge Group');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'The Capbridge Group')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'CapRidge Partners')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'CAPBRIDGE');
    
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'Deutsche Bank') 
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'DEUTSCHEASSETWEALTH' 
                                 AND dl.Canonical IS NULL;
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE DBAName IN ('DEUTSCHEASSETWEALTH','DEUTSCHEBANK');
    
    UPDATE dEspStage.CompanyDupList set DBAName = 'EQUITYRESIDENTIAL' WHERE DBAName = 'ARCHSTONE';
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'Equity Residential') 
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'EQUITYRESIDENTIAL' 
                                 AND dl.Canonical IS NULL;
    
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('Hersha Hospitality Management');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Hersha Hospitality Management')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Hersha Hospitality Trust')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'HHM');
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'Hersha Hospitality Trust') 
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'HERSHAHOSPITALITY' 
                                 AND dl.Canonical IS NULL;
    
    DELETE dEspStage.SLBranchCanonicals WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'LNR')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'LENNAR');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Starwood Capital Group')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'LNR')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'LNR');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'LNR';
    UPDATE dEspStage.CompanyDupList set DBAName = 'STARWOODCAPITAL' WHERE DBAName = 'LNR';
    
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('Midland Loan Services');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Midland Loan Services')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'PNC Bank')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'MIDLANDLOAN');
    UPDATE dEspStage.CompanyDupList set DBAName = 'PNC' WHERE DBAName = 'MIDLANDLOAN';
    
    UPDATE dEspStage.CompanyDupList set DBAName = 'PROCACCIANTI' WHERE DBAName = 'TPGCOMPANIES';
    
    UPDATE dEspStage.CompanyDupList set DBAName = NULL WHERE DBAName in ('SPIEKERPARTNERS','SPIEKERPROP');
    
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('CalPERS');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'CalPERS')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'State of California')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'CALPERS');
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('CalSTRS');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'CalSTRS')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'State of California')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'CALSTRS');
    UPDATE dEspStage.CompanyDupList set DBAName = 'STATEOFCALIFORNIA' WHERE DBAName IN ('CALSTRS','CALPERS');
    UPDATE dEspStage.CompanyDupList set DBAName = 'STATEOFCALIFORNIA' WHERE Canonical IN (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'State of California');
    
    
    --****************************** MITSUBISHI,MITSUBISHIUFJ,MORGANSTANLEY,MUFGUNIONBANK,ROCKEFELLER,TAASSOCIATES,UBS ultimately roll up to MITSUBISHI
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Mitsubishi')
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'MITSUBISHI' AND CompanyName = 'Mitsubishi - UBS');
    UPDATE dEspStage.CompanyDupList SET DBAName = 'UBS', Canonical = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Mitsubishi')
            WHERE DBAName = 'MITSUBISHI' AND CompanyName = 'Mitsubishi - UBS';
    
    UPDATE dEspStage.CompanyDupList set CompanyName = 'Mitsubishi UFJ Union Bank' WHERE DBAName = 'MUFGUNIONBANK';
    UPDATE dEspStage.CompanyDupList set DBAName = 'UNIONBANK' WHERE DBAName = 'MUFGUNIONBANK';
    
    UPDATE dEspStage.SLCanonicals SET CanonicalName = 'Union Bank' WHERE CanonicalName = 'UnionBank';
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('Mitsubishi UFJ');
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('TA Associates');
    INSERT INTO dEspStage.SLCanonicals(CanonicalName) VALUES  ('Union Bank of California');
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Mitsubishi UFJ') -- select * from dEspStage.SLBranchCanonicals
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'MITSUBISHIUFJ');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'TA Associates') -- select * from dEspStage.SLBranchCanonicals
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'TAASSOCIATES');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'TA Associates Realty') -- select * from dEspStage.SLBranchCanonicals
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'TAASSOCIATESREALTY');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Union Bank') -- select * from dEspStage.SLBranchCanonicals
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'UNIONBANK');
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Union Bank of California') -- select * from dEspStage.SLBranchCanonicals
            WHERE CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE DBAName = 'UNIONBANKOFCA');
    
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'Mitsubishi UFJ')
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'MITSUBISHIUFJ'
                                 AND dl.Canonical IS NULL;
    INSERT INTO dEspStage.SLBranchCanonicals(CompanyID,CanonicalID)
        SELECT dl.CompanyID
              ,(SELECT CanonicalID FROM dEspStage.SLCanonicals WHERE CanonicalName = 'Union Bank') 
          FROM dEspStage.CompanyDupList dl WHERE dl.DBAName = 'UNIONBANK' 
                                 AND dl.Canonical IS NULL;
    
    UPDATE dEspStage.CompanyDupList set DBAName = 'ROCKEFELLER' WHERE CompanyID in (SELECT sbc.CompanyID FROM dEspStage.SLCanonicals sl 
                                                                          JOIN dEspStage.SLBranchCanonicals sbc ON sbc.CanonicalID = sl.CanonicalID
                                                                         WHERE sl.CanonicalName IN ('TA Associates','TA Associates Realty',''));
    UPDATE dEspStage.CompanyDupList set DBAName = 'MITSUBISHI'  WHERE CompanyID in (SELECT sbc.CompanyID FROM dEspStage.SLCanonicals sl 
                                                                          JOIN dEspStage.SLBranchCanonicals sbc ON sbc.CanonicalID = sl.CanonicalID
                                                    WHERE sl.CanonicalName IN ('Union Bank of California','Union Bank','UNIONBANKOFCA','Mitsubishi UFJ'));
    
    -- fixes to anomalies P4
    DELETE dEspStage.SLBranchCanonicals WHERE CanonicalID IN (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'CPC Group')
                                     AND CompanyID IN (SELECT dl.CompanyID FROM dEspStage.CompanyDupList dl WHERE dl.CompanyName = 'Omni Capital');
    UPDATE dEspStage.CompanyDupList SET DBAName = 'CPCGROUP' WHERE Canonical IN (SELECT slc.CanonicalID FROM dEspStage.SLCanonicals slc WHERE slc.CanonicalName IN ('CPC Group','Omni Capital'));
    
    UPDATE dEspStage.CompanyDupList set DBAName = 'VEREIT' where DBAName = 'AMERICANREALTYCAPITAL';
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'American Realty Capital')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Cole Real Estate')
              AND CompanyID IN (SELECT CompanyID FROM dEspStage.CompanyDupList WHERE CompanyName = 'American Realty Capital Properties, Inc.');
    
    
    UPDATE dEspStage.SLBranchCanonicals SET CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Royal Bank of Scotland')
            WHERE CanonicalID = (SELECT sl.CanonicalID FROM dEspStage.SLCanonicals sl WHERE sl.CanonicalName = 'Citizens Bank');
    DELETE dEspStage.SLCanonicals WHERE CanonicalName = 'Citizens Bank';
    UPDATE dEspStage.CompanyDupList set DBAName = 'ROYALBANKOFSCOTLAND' where DBAName IN ('CITIZENSBANK','RBS');
    
    -- fixing of 'Metropolitan Realty' must happen after dups are detected.  They look the same but one is in Honolulu, HI and the other in Detroit, MI
/*Test
GO
EXEC dEspStage.FixCompanyAnomaliesRound1;
--*/
﻿CREATE VIEW [dEspStage].[Addresses] AS 
SELECT AddressID
      ,ADDRESSID_SLX
      ,ENTITYID
      ,ISNULL(REPLACE(SUBSTRING(Description,1,ISNULL(NULLIF(CHARINDEX(' ',Description),0),10)),',',''),'') TypeCode
      ,Description
      ,Address1
      ,Address2
      ,City
      ,State
      ,PostalCode
      ,County
      ,Country
      ,CrossStreet
      ,IsPrimary
      ,IsMailing
      ,Salutation
      ,Routing
      ,Address3
      ,Address4
      ,Timezone
      ,Source
      ,Confidence
      ,Latitude
      ,Longitude
      ,BoundaryBaseHeight
      ,BoundaryMaxHeight
      ,Boundary1
      ,Boundary2
      ,Boundary3
      ,Boundary4
      ,DateAdded
      ,LastUpdate
      ,msrepl_tran_version
      ,RowVersion
      ,AddedBy
      ,UpdatedBy
      ,NULLIF(NULLIF(NULLIF(NULLIF(Address1,'Address'),'x'),''),'test')
      +','+COALESCE(NULLIF(NULLIF(City,''),'x'),Address2,'')
      +','+ISNULL(State,'')
      +','+COALESCE(NULLIF(Country,'x'),PostalCode,'') LookupValue
  FROM rESP.Addresses;
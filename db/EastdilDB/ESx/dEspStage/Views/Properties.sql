﻿CREATE VIEW [dEspStage].[Properties] AS 
SELECT *
      ,ISNULL(Address1,PropertyName)
      +','+ISNULL(City,'')
      +','+ISNULL(State,'')
      +','+ISNULL(Country,'') LookupValue
 FROM [$(EastdilDB)].[dbo].[Properties];
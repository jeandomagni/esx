﻿CREATE VIEW [dEspStage].[SLProperties] AS 
SELECT slp.PropID           PropertyID
      ,slp.PropName         PropertyName
      ,slp.Address          Address1
      ,slp.City             
      ,st.State
      ,ISNULL(st.Country,slp.StateCountry) AS Country
      ,slp.DateAdded
      ,slp.LastUpdate
      ,slp.AddedBy
      ,slp.UpdatedBy
      ,ISNULL(slp.Address,slp.PropName)+','+ISNULL(slp.City,'')+','+ISNULL(st.State,'')+','+COALESCE(st.Country,slp.StateCountry,'') LookupValue
      -- select *
  FROM rESP.[SLProperties] slp
  LEFT JOIN rESP.States st
            ON st.State = slp.StateCountry;
/*Test
GO
select * from dEspStage.SLProperties;
*/
<#
  .SYNOPSIS
    !!!!!!!!!!!!! INTERACTIVE USE ONLY !!!!!!!!!!!!!!!
  .Description
    This defines a function used interactively to validate questionable company names.
    Basically, if looking up the name doesn't result in a "Did you mean ..." warning from Google intellisense the name is considered valid.
    It starts an instance of IE to perform the lookup and IE's DOM to find the elements.
    Mostly I used this to determine if a given company name was misspelled when another name was within 1 character of being the same.
  .Notes
    Author: Rick Bielawski

#>

$IE= new-object -com "InternetExplorer.Application"
$IE.navigate2(“http://www.google.com”)
while ($IE.busy) {
    sleep -milliseconds 100
}
$IE.visible=$true

function LookupCompany {
param([System.String]$LookupString)
    $IE.Document.getElementById(“q”).value=$LookupString
    $IE.document.forms | 
        Select -First 1 | 
            % { $_.submit() }
    while ($IE.busy) {
        sleep -milliseconds 100
    }
    sleep -milliseconds 100
    new-object psobject -Property @{Name    = $LookupString
                                    Warning = $IE.document.getElementById("taw").getElementsByTagName("p")|%{$_.innerText}
                                    #Entries = $IE.document.getElementById("search").getElementsByTagName("h3")|%{$_.innerText}
                                    }
}

LookupCompany -LookupString ”Thacher Proffitt & Wood”
LookupCompany -LookupString "Muller Company; The"
LookupCompany -LookupString "Mullen Company; The"
LookupCompany -LookupString "Yarco Realty"
LookupCompany -LookupString "Arco Realty"

# see also $IE.Document.getElementsByName()

'Advest',
'ALDA Properties',
'Aldar Properties',
'Amvest',
'Arco Realty',
'Ascendant Lodging Partners, LLC',
'Ascendent Lodging Partners, LLC',
'Atalon Group; The',
'Avalon Group; The',
'Banco Popular',
'Banco Pupular',
'Banyan Tree Hotels & Resorts',
'Banyon Tree Hotels & Resorts',
'Bear Capital Partners',
'Beary Capital Partners',
'Briad Group; The',
'Caldwell Companies',
'Cardwell Companies',
'Celcor Realty Services, Inc.',
'Cencor Realty Services, Inc.',
'Hanting Hotel Group',
'Helaba Landesbank Hessen-Thueringen',
'Helaba Landesbank Hessen-Thuringen',
'Helaba Landesbank Hessen-Thüringen',
'Hoden Weill & Associates',
'Hodes Weill & Associates',
'Kensingron Vanguard National Land Services',
'Kensington Vanguard National Land Services',
'Lanting Hotel Group',
'McKnight Property Management',
'McNight Property Management',
'Mullen Company; The',
'Muller Company; The',
'Norhern Trust',
'Northern Trust',
'Royal Capital',
'Royall Capital',
'Sagic Capital',
'SAIC Capital',
'SHEFA',
'Sheva',
'Shimizu Corporation',
'Shimuzu Corporation',
'Sothebys International Realty',
'Sotheby''s International Realty',
'Terrance J. Rose, Inc.',
'Terrence J. Rose, Inc.',
'Thacher Proffit & Wood',
'Thacher Proffitt & Wood',
'Triad Group; The',
'Valor Hospitality',
'Valor Hospitalty',
'Yarco Realty'|%{LookupCompany $_}

'ALDA Properties',
'Bear Capital Partners',
'Cardwell Companies',
'Celcor Realty Services, Inc.',
'Hoden Weill & Associates',
'Royal Capital',
'Royall Capital',
'SHEFA',
'Sheva'|%{LookupCompany $_}|ft -a
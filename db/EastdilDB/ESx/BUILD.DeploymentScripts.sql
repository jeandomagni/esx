﻿/*
    This BUILDS the PostDeployment script.
    The commands executed during deployment are determined here by build configuration i.e.: NoWipe, WipeSeed, WipeETL, WipeEtlSeed

    This tact was forced because the build needs to know what type of deploy will occur.  
    The deploy itself can't decide based on parameters because there is no way to parameterize which files are part of the deploy.

    Notice how what you want needs to be PRINTed to the output file.
    Since print statements are quoted you need to double your quotes to send a quote onto the Post Deploy script.
*/

-- Increment this value when ETL data needs to be re-deployed.
:SETVAR DataVersion 3

PRINT 'Deploy script generation starting at '+CAST(GETDATE() AS nvarchar)+' with Configuration = $(Configuration), Server = $(SQLCMDSERVER).$(SQLCMDDBNAME)';

GO
:OUT ..\..\Script.DynamicPostDeployment.sql

    -- Ensure all :r'd scripts have their Build Action is set to "None".
    -- The GO statements are not technically necessary but swapping could occur if memory can't hold all the statements
    -- in all the scripts and GO tells the executor to stop reading and send the statements to SQL Server.
    PRINT ' /*';
    PRINT '     DO NOT MANUALLY MODIFY THIS SCRIPT.                                               ';
    PRINT '                                                                                       ';
    PRINT '     It is overwritten during build.                                                   ';
    PRINT '     Content IS based on the Build configuration (NoWipe, WipeSeed, WipeETL, WipeEtlSeed...) ';
    PRINT '                                                                                       ';
    PRINT '     Modify BUILD.DeploymentScripts.sql to effect changes in executable content.         ';
    PRINT ' */';
    PRINT 'PRINT ''PostDeployment script starting at''+CAST(GETDATE() AS nvarchar)+'' with Configuration = $(Configuration)'';';
    PRINT 'GO';


    IF  '$(Configuration)' IN ('NoWipe', 'WipeSeed', 'WipeETL', 'WipeEtlSeed')
    BEGIN
        -- Lookup data is merged in ALL environments.
        PRINT ':r .\xESxApp\LookupData\AssetType.sql                  ';
        PRINT ':r .\xESxApp\LookupData\DealValuationRiskProfile.sql   ';
        PRINT ':r .\xESxApp\LookupData\AssetUsageType.sql             ';
        PRINT ':r .\xESxApp\LookupData\BidStatus.sql                  ';
        PRINT ':r .\xESxApp\LookupData\AssetContactType.sql           ';
        PRINT ':r .\xESxApp\LookupData\DealContactType.sql            ';
        PRINT ':r .\xESxApp\LookupData\Country.sql                    ';
        PRINT ':r .\xESxApp\LookupData\DealValuationType.sql          ';
        PRINT ':r .\xESxApp\LookupData\DealStatus.sql                 ';
        PRINT ':r .\xESxApp\LookupData\DealTeamRole.sql               ';
        PRINT 'GO';
        PRINT ':r .\xESxApp\LookupData\DealType.sql                   ';
        PRINT ':r .\xESxApp\LookupData\ElectronicAddressUsageType.sql ';
        PRINT ':r .\xESxApp\LookupData\LegalEntity.sql                ';
        PRINT ':r .\xESxApp\LookupData\LoanBenchmarkType.sql          ';
        PRINT ':r .\xESxApp\LookupData\LoanCashMgmtType.sql           ';
        PRINT ':r .\xESxApp\LookupData\LoanLenderType.sql             ';
        PRINT ':r .\xESxApp\LookupData\LoanPaymentType.sql            ';
        PRINT ':r .\xESxApp\LookupData\LoanRateType.sql               ';
        PRINT ':r .\xESxApp\LookupData\LoanSourceType.sql             ';
        PRINT ':r .\xESxApp\LookupData\LoanTermDateType.sql           ';
        PRINT 'GO';
        PRINT ':r .\xESxApp\LookupData\LogSeverityType.sql            ';
        PRINT ':r .\xESxApp\LookupData\MarketType.sql                 ';
        PRINT ':r .\xESxApp\LookupData\NoticeSeverityType.sql         ';
        PRINT ':r .\xESxApp\LookupData\PhysicalAddressType.sql        ';
        PRINT ':r .\xESxApp\LookupData\PreferenceType.sql             ';
        PRINT ':r .\xESxApp\LookupData\StateProvince.sql              ';
        PRINT ':r .\xESxApp\LookupData\DealParticipationType.sql      ';
        PRINT ':r .\xESxApp\LookupData\MarketingListStatusType.sql    ';
		PRINT ':r .\xESxApp\LookupData\MarketingListActionType.sql    ';
        PRINT ':r .\xESxApp\LookupData\Feature.sql                    ';
        PRINT 'GO';
        PRINT ':r .\xESxApp\LookupData\Role.sql                       ';
        PRINT 'GO';
        PRINT 'PRINT ''Lookup data finished at''+CAST(GETDATE() AS nvarchar);';
        PRINT 'GO';
    END
    
    IF '$(Configuration)' IN ('WipeSeed', 'WipeEtlSeed')
    BEGIN
        -- Please ensure new seed data files are added in the correct order needed to resolve dependencies.
        -- Adam recommends using this script to determine the order: http://sqlblog.com/blogs/jamie_thomson/archive/2009/09/08/deriving-a-list-of-tables-in-dependency-order.aspx
        -- Rick says just use this "EXEC xESxApp.AppDev_DeleteAllTestData @pTest=1".  pTest=1 shows cmds. Insert oposite of delete order.
        PRINT ':r .\xESxApp\SeedData\Company.sql                        ';
        PRINT ':r .\xESxApp\SeedData\Contact.sql                        ';
        PRINT ':r .\xESxApp\SeedData\Park.sql                           ';
        PRINT ':r .\xESxApp\SeedData\CompanyAlias.sql                   ';
        PRINT ':r .\xESxApp\SeedData\CompanyDealTypeInterest.sql        ';
        PRINT ':r .\xESxApp\SeedData\CompanyHierarchy.sql               ';
        PRINT ':r .\xESxApp\SeedData\PhysicalAddress.sql                ';
        PRINT ':r .\xESxApp\SeedData\Office.sql                         ';
        PRINT 'GO';
        PRINT ':r .\xESxApp\SeedData\Deal.sql                           ';
        PRINT ':r .\xESxApp\SeedData\DealStatusLog.sql                  ';
        PRINT ':r .\xESxApp\SeedData\ElectronicAddress.sql              ';
        PRINT ':r .\xESxApp\SeedData\Market.sql                         ';
        PRINT ':r .\xESxApp\SeedData\MarketStates.sql                   ';
        PRINT ':r .\xESxApp\SeedData\MarketMSAs.sql                     ';
        PRINT ':r .\xESxApp\SeedData\User.sql                           ';
        PRINT ':r .\xESxApp\SeedData\Asset.sql                          ';
        PRINT ':r .\xESxApp\SeedData\DealValuation.sql                  ';
        PRINT ':r .\xESxApp\SeedData\Company_ElectronicAddress.sql      ';
        PRINT 'GO';
        PRINT ':r .\xESxApp\SeedData\CompanyMarket.sql                  ';
        PRINT ':r .\xESxApp\SeedData\Contact_ElectronicAddress.sql      ';
        PRINT ':r .\xESxApp\SeedData\ContactMarket.sql                  ';
        PRINT ':r .\xESxApp\SeedData\DealCompany.sql                    ';
        PRINT ':r .\xESxApp\SeedData\DealContact.sql                    ';
        PRINT ':r .\xESxApp\SeedData\DealValuation.sql                  ';
        PRINT ':r .\xESxApp\SeedData\DealTeam.sql                       ';
        PRINT ':r .\xESxApp\SeedData\EastdilOffice.sql                  ';
        PRINT ':r .\xESxApp\SeedData\MarketHierarchy.sql                ';
        PRINT ':r .\xESxApp\SeedData\TagUsage.sql                       ';
        PRINT 'GO';
        PRINT ':r .\xESxApp\SeedData\UserPreference.sql                 ';
        PRINT ':r .\xESxApp\SeedData\AssetContact.sql                   ';
        PRINT ':r .\xESxApp\SeedData\AssetLoanBorrower.sql              ';
        PRINT ':r .\xESxApp\SeedData\AssetLoanTerms.sql                 ';
        PRINT ':r .\xESxApp\SeedData\AssetName.sql                      ';
        PRINT ':r .\xESxApp\SeedData\AssetUsage.sql                     ';
        PRINT ':r .\xESxApp\SeedData\AssetValue.sql                     ';
        PRINT ':r .\xESxApp\SeedData\CompanyAsset.sql                   ';
        PRINT ':r .\xESxApp\SeedData\ContactOffice.sql                  ';
        PRINT ':r .\xESxApp\SeedData\DealAsset.sql                      ';
        PRINT 'GO';
        PRINT ':r .\xESxApp\SeedData\DealValuationAsset.sql             ';
        PRINT ':r .\xESxApp\SeedData\DealValuationCompany.sql           ';
        PRINT ':r .\xESxApp\SeedData\DealValuationDocument.sql          ';
        PRINT ':r .\xESxApp\SeedData\DealContactStatus.sql              ';
        PRINT ':r .\xESxApp\SeedData\Office_ElectronicAddress.sql       ';
        PRINT ':r .\xESxApp\SeedData\OfficeMarket.sql                   ';
        PRINT ':r .\xESxApp\SeedData\AssetLoanProperty.sql              ';
        PRINT ':r .\xESxApp\SeedData\AssetLoanTermDates.sql             ';
        PRINT ':r .\xESxApp\SeedData\DealValuationAsset.sql             ';
        PRINT 'GO';
        PRINT ':r .\xESxApp\SeedData\Mention.sql                        ';
        PRINT '--:r .\xESxApp\SeedData\CacheAccessToken.sql             ';
        PRINT ':r .\xESxApp\SeedData\Comment.sql                        ';
        PRINT ':r .\xESxApp\SeedData\EastdilOfficeMentionUsage.sql      ';
        PRINT ':r .\xESxApp\SeedData\Notice.sql                         ';
        PRINT '--:r .\xESxApp\SeedData\CacheSessionQuery.sql            ';
        PRINT ':r .\xESxApp\SeedData\Comment_Mention.sql                ';
        PRINT ':r .\xESxApp\SeedData\Comment_Tag.sql                    ';
        PRINT ':r .\xESxApp\SeedData\Notice_Mention.sql                 ';
        PRINT ':r .\xESxApp\SeedData\NoticeRecipient.sql                ';
        PRINT 'GO';
        PRINT ':r .\xESxApp\SeedData\MarketingList.sql                  ';
        PRINT ':r .\xESxApp\SeedData\MarketingListContact.sql           ';
        PRINT ':r .\xESxApp\SeedData\MarketingListContactStatusLog.sql  ';
        PRINT ':r .\xESxApp\SeedData\FeatureRole.sql                    ';
        PRINT 'GO';
        PRINT 'PRINT ''Seed data finished at''+CAST(GETDATE() AS nvarchar);';
        PRINT 'GO';
    END

    IF '$(Configuration)' IN ('WipeETL', 'WipeEtlSeed')
    BEGIN
        DECLARE @DataVersion int;
		IF (SELECT OBJECT_ID('rESxStage.DataVersion')) IS NOT NULL
            SET @DataVersion = (SELECT DataVersion FROM rESxStage.DataVersion);
        IF (CASE @DataVersion WHEN $(DataVersion) THEN 1 ELSE 0 END) = 0
        BEGIN
            -- These get a GO statement after every file because most are really HUGE
            PRINT 'PRINT ''ETL data was needed and started at''+CAST(GETDATE() AS nvarchar);';
            PRINT '                                                  ';
            PRINT 'EXEC Global.DeleteAllSchemaData ''dExternalETL'';';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\PhysicalAddressGeoData1.sql  ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\PhysicalAddressGeoData2.sql  ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\PhysicalAddressGeoData3.sql  ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\PhysicalAddressGeoData4.sql  ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\PhysicalAddressGeoData5.sql  ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\PhysicalAddressGeoData6.sql  ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\PhysicalAddressGeoData7.sql  ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\PhysicalAddressGeoData8.sql  ';
            PRINT 'GO';
            PRINT '                                                  ';
            PRINT 'EXEC Global.DeleteAllSchemaData ''dExcelETL'';     ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\ExcelSourceFile.sql          ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\ExcelSourceSheet.sql         ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData1.sql         ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData2.sql         ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData3.sql         ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData4.sql         ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData5.sql         ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData6.sql         ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData7.sql         ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData8.sql         ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData9.sql         ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData10.sql        ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData11.sql        ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData12.sql        ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData13.sql        ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData14.sql        ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData15.sql        ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData16.sql        ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData17.sql        ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData18.sql        ';
            PRINT 'GO';
            PRINT ':r .\xESxApp\EtlData\StagedExcelData19.sql        ';
            PRINT 'GO';
            PRINT '                                                  ';
            PRINT 'EXEC rESxStage.RebuildStageData;                  ';
            PRINT 'EXEC rESxStage.UpdateStageDataVersion @Version = $(DataVersion);';
               PRINT 'GO';
            PRINT 'PRINT ''ETL source data finished at''+CAST(GETDATE() AS nvarchar);';
        END
           PRINT 'GO';

        -- This actually loads imported data into 
        PRINT 'EXEC xESxApp.LoadEspData;';
           PRINT 'GO';
        PRINT 'PRINT ''ESP/ETL data finished at''+CAST(GETDATE() AS nvarchar);';
        PRINT 'GO';
    END

    -- All configurations need Mentions built
    IF '$(Configuration)' IN ('WipeSeed', 'WipeETL', 'WipeEtlSeed')
    BEGIN
        PRINT 'EXEC xESxApp.AppDev_RebuildAllMentions;';
        PRINT 'GO';
    END
    PRINT 'PRINT ''PostDeployment script finished at''+CAST(GETDATE() AS nvarchar);';
    PRINT 'GO';
GO
:OUT ..\..\Script.DynamicPreDeployment.sql
    PRINT ' /*';
    PRINT '     DO NOT MANUALLY MODIFY THIS SCRIPT.                                                     ';
    PRINT '                                                                                             ';
    PRINT '     It is overwritten during build.                                                         ';
    PRINT '     Content IS based on the Build configuration (NoWipe, WipeSeed, WipeETL, WipeEtlSeed...) ';
    PRINT '                                                                                             ';
    PRINT '     Modify BUILD.DeploymentScripts.sql to effect changes in executable content.             ';
    PRINT ' */';
    PRINT 'PRINT ''PreDeployment script starting at''+CAST(GETDATE() AS nvarchar)+'' with Configuration = $(Configuration)'';';
    PRINT 'GO';

	-- If the data version changes we need to insure there is no conflicting data that exists even if it won't be loaded
    DECLARE @DataVersion int;
    IF (SELECT OBJECT_ID('rESxStage.DataVersion')) IS NOT NULL
        SET @DataVersion = (SELECT DataVersion FROM rESxStage.DataVersion);
    IF (CASE @DataVersion WHEN $(DataVersion) THEN 1 ELSE 0 END) = 0
    BEGIN
        PRINT 'PRINT ''PreDeployment data table wipe executing''';
		PRINT 'IF (SELECT OBJECT_ID(''Global.DeleteAllSchemaData'')) IS NOT NULL'
        PRINT 'BEGIN'
        PRINT '    EXEC Global.DeleteAllSchemaData ''dEspStage'';';
        PRINT '    EXEC Global.DeleteAllSchemaData ''pEsxETL'';';
        PRINT '    EXEC Global.DeleteAllSchemaData ''dExternalETL'';';
        PRINT '    EXEC Global.DeleteAllSchemaData ''dExcelETL'';';
        PRINT 'END'
        PRINT 'ELSE'
        PRINT 'BEGIN'
        PRINT '    EXEC iESxETL.DeleteAllSchemaData ''rESxStage'';';
        PRINT '    EXEC iESxETL.DeleteAllSchemaData ''dEspStage'';';
        PRINT '    EXEC iESxETL.DeleteAllSchemaData ''pExternalETL'';';
        PRINT '    EXEC iESxETL.DeleteAllSchemaData ''pExcelETL'';';
        PRINT '    EXEC iESxETL.DeleteAllSchemaData ''pEsxETL'';';
        PRINT 'END'
        PRINT 'GO';
    END

    IF '$(Configuration)' IN ('WipeSeed', 'WipeETL', 'WipeEtlSeed')
    BEGIN
        PRINT 'IF OBJECT_ID(''xESxApp.AppDev_DeleteAllTestData'') IS NOT NULL';
        PRINT '    EXEC xESxApp.AppDev_DeleteAllTestData;';
        PRINT 'GO';
    END
    ELSE
    BEGIN
        PRINT 'PRINT ''No PreDeployment ESX actions required'';';
        PRINT 'GO';
    END
    PRINT 'PRINT ''PreDeployment script finished at''+CAST(GETDATE() AS nvarchar);';
    PRINT 'GO';
GO
:OUT STDOUT

PRINT 'Deploy script generation done';
﻿PRINT N'Seeding DealParticipationType.'

MERGE INTO [dESx].[DealParticipationType] AS trgt
USING	(VALUES
		(N'Buyer',N''),
		(N'Seller',N''),
		(N'Lender',N''),
		(N'Borrower',N''),
		(N'Broker',N'')
		) AS src([DealParticipationType_Code],[Description])
ON
	trgt.[DealParticipationType_Code] = src.[DealParticipationType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[DealParticipationType_Code] = src.[DealParticipationType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealParticipationType_Code],[Description])
	VALUES ([DealParticipationType_Code],[Description])
;

PRINT N'Seeding of DealParticipationType complete.'

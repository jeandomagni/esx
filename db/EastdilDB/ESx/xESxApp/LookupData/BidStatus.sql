
PRINT N'Seeding BidStatus.'

MERGE INTO [dESx].[BidStatus] AS trgt
USING	(VALUES
		('BF', 'Best and Final'),
		('W', 'Bid Awarded'),
		('B', 'Bridesmaid'),
		('D', 'Declined')
		) AS src([BidStatus_Code],[Description])
ON
	trgt.[BidStatus_Code] = src.[BidStatus_Code]
WHEN MATCHED THEN
	UPDATE SET
		[BidStatus_Code] = src.[BidStatus_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([BidStatus_Code],[Description])
	VALUES ([BidStatus_Code],[Description])
;

PRINT N'Completed BidStatus.'

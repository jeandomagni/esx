﻿PRINT N'Seeding DealType.'

MERGE INTO [dESx].[DealType] AS trgt
USING	(VALUES
		(N'Equity Sale 100%',N''),
		(N'Equity Sale <100%',N''),
		(N'Debt Placement',N''),
		(N'Loan Sale',N''),
		(N'Investment Banking',N''),
		(N'Non-transactional Advisory',N'')
		) AS src([DealType_Code],[Description])
ON
	trgt.[DealType_Code] = src.[DealType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[DealType_Code] = src.[DealType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealType_Code],[Description])
	VALUES ([DealType_Code],[Description])
;

PRINT N'Seeding of DealType complete.'

﻿
PRINT N'Seeding AssetContactType.'

MERGE INTO [dESx].[AssetContactType] AS trgt
USING	(VALUES
		(N'Unknown'			, N'Unknown'		   )
		) AS src([AssetContactType_Code],[Description])
ON
	trgt.[AssetContactType_Code] = src.[AssetContactType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AssetContactType_Code],[Description])
	VALUES ([AssetContactType_Code],[Description])
;
PRINT N'Seeding of AssetContactType complete.'


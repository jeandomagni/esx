﻿PRINT N'Seeding Role.'

MERGE INTO [dESx].[Role] AS trgt
USING	(VALUES
		(N'DTCG_WSD_ESPREPORTS_FEES', N''),
		(N'DTCG_WSDUSA_ESPREPORTS_BIDS', N''),
		(N'DTCA_WSD_ESX_USERS', N'')
		) AS src([Role_Code],[Description])
ON
	trgt.[Role_Code] = src.[Role_Code]
WHEN MATCHED THEN
	UPDATE SET
		[Role_Code] = src.[Role_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Role_Code],[Description])
	VALUES ([Role_Code],[Description])
;

PRINT N'Seeding of Role complete.'

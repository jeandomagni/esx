﻿PRINT N'Seeding MarketType.'

MERGE INTO [dESx].[MarketType] AS trgt
USING	(VALUES
		(N'MSA',N'Metro Statistical Area'),
		(N'State',N'US State Name'),
		(N'Region',N'U.S. Region')
		) AS src([MarketType_Code],[Description])
ON
	trgt.[MarketType_Code] = src.[MarketType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[MarketType_Code] = src.[MarketType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([MarketType_Code],[Description])
	VALUES ([MarketType_Code],[Description])
;

PRINT N'Seeding of MarketType complete.'

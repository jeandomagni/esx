﻿PRINT N'Seeding DealValuationType.'

MERGE INTO [dESx].[DealValuationType] AS trgt
USING	(VALUES
		(N'BaseCase',N'Base Case'),
		(N'PushCase',N'Push Case'),
		(N'Bid',N'Bid'),
		(N'CustomName',N'Custom Name')
		) AS src([DealValuationType_Code],[Description])
ON
	trgt.[DealValuationType_Code] = src.[DealValuationType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[DealValuationType_Code] = src.[DealValuationType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealValuationType_Code],[Description])
	VALUES ([DealValuationType_Code],[Description])
;

PRINT N'Seeding of DealValuationType complete.'

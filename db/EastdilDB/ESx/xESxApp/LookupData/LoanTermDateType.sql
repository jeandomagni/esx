PRINT N'Seeding LoanTermDateType.'

MERGE INTO [dESx].[LoanTermDateType] AS trgt
USING	(VALUES
		('YieldMaintenanceEnd', 'Yield Maintenance End'),
		('NextMaturity', 'Next Maturity'),
		('FinalMaturity', 'Final Maturity'),
		('LockoutEnd', 'Lockout End')
		) AS src([LoanTermDateType_Code],[Description])
ON
	trgt.[LoanTermDateType_Code] = src.[LoanTermDateType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LoanTermDateType_Code],[Description])
	VALUES ([LoanTermDateType_Code],[Description])
;

PRINT N'Completed LoanTermDateType.'

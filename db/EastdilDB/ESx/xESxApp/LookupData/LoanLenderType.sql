
PRINT N'Seeding LoanLenderType.'

MERGE INTO [dESx].[LoanLenderType] AS trgt
USING	(VALUES
		('DB', 'Domestic Bank'),
		('FB', 'Foreign Bank'),
		('LC', 'Life Co'),
		('DF', 'Debt Fund')
		) AS src([LoanLenderType_Code],[Description])
ON
	trgt.[LoanLenderType_Code] = src.[LoanLenderType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[LoanLenderType_Code] = src.[LoanLenderType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LoanLenderType_Code],[Description])
	VALUES ([LoanLenderType_Code],[Description])
;

PRINT N'Completed LoanLenderType.'


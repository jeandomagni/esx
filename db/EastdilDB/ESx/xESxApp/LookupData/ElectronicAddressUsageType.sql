﻿PRINT N'Seeding ElectronicAddressUsageType.'

MERGE INTO [dESx].[ElectronicAddressUsageType] AS trgt
USING	(VALUES
		(N'EmailAddress',N'Alternate'),
		(N'EmailAddress',N'Assistant'),
		(N'EmailAddress',N'Main'),
		(N'PhoneNumber',N'Alternate'),
		(N'PhoneNumber',N'Assistant'),
		(N'PhoneNumber',N'Blackberry'),
		(N'PhoneNumber',N'Ext'),
		(N'PhoneNumber',N'Fax'),
		(N'PhoneNumber',N'Home'),
		(N'PhoneNumber',N'Main'),
		(N'PhoneNumber',N'Mobile'),
		(N'Website',N'Main')
		) AS src([ElectronicAddressType_Code],[AddressUsageType_Code])
ON
	trgt.[ElectronicAddressType_Code] = src.[ElectronicAddressType_Code]
AND trgt.[AddressUsageType_Code] = src.[AddressUsageType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[ElectronicAddressType_Code] = src.[ElectronicAddressType_Code]
		, [AddressUsageType_Code] = src.[AddressUsageType_Code]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([ElectronicAddressType_Code],[AddressUsageType_Code])
	VALUES ([ElectronicAddressType_Code],[AddressUsageType_Code])
;

PRINT N'Seeding of ElectronicAddressUsageType complete.'

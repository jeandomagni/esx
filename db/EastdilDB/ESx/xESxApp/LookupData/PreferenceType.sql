PRINT N'Seeding Preference Type.'

MERGE INTO [dESx].[PreferenceType] AS trgt
USING	(VALUES
		('STARTPAGE', 'Dashboard')
		) AS src([PreferenceType_Code],[Description])
ON
	trgt.[PreferenceType_Code] = src.[PreferenceType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[PreferenceType_Code] = src.[PreferenceType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([PreferenceType_Code],[Description])
	VALUES ([PreferenceType_Code],[Description])
;

PRINT N'Completed Preference Type.'

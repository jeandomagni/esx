
PRINT N'Seeding LoanBenchmarkType.'

MERGE INTO [dESx].[LoanBenchmarkType] AS trgt
USING	(VALUES
		('T', 'Displayed as Treasury'),
		('S', 'Swap'),
		('L', 'Libor'),
		('E', 'Euribor'),
		('NA', 'Not Available')
		) AS src([LoanBenchmarkType_Code],[Description])
ON
	trgt.[LoanBenchmarkType_Code] = src.[LoanBenchmarkType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[LoanBenchmarkType_Code] = src.[LoanBenchmarkType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LoanBenchmarkType_Code],[Description])
	VALUES ([LoanBenchmarkType_Code],[Description])
;

PRINT N'Completed LoanBenchmarkType.'

﻿PRINT N'Seeding MarketingListActionType.'

MERGE INTO [dESx].[MarketingListActionType] AS trgt
USING	(VALUES
		(N'Contacted',N''),
		(N'Called Left Msg',N''),
		(N'Tour Scheduled',N'')
		) AS src([MarketingListAction_Code],[Description])
ON
	trgt.[MarketingListAction_Code] = src.[MarketingListAction_Code]
WHEN MATCHED THEN
	UPDATE SET
		[MarketingListAction_Code] = src.[MarketingListAction_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([MarketingListAction_Code],[Description])
	VALUES ([MarketingListAction_Code],[Description])
;

PRINT N'Seeding of MarketingListActionType complete.'

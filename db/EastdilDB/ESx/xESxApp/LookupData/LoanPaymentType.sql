
PRINT N'Seeding LoanPaymentType.'

MERGE INTO [dESx].[LoanPaymentType] AS trgt
USING	(VALUES
		('YM', 'Yield Maintenance'),
		('DF', 'Defeasance'),
		('SP', 'Spread Maintenance'),
		('L', 'Lockout'),
		('PB', '% of Balance'),
		('O', 'Open')
		) AS src([LoanPaymentType_Code],[Description])
ON
	trgt.[LoanPaymentType_Code] = src.[LoanPaymentType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[LoanPaymentType_Code] = src.[LoanPaymentType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LoanPaymentType_Code],[Description])
	VALUES ([LoanPaymentType_Code],[Description])
;

PRINT N'Completed LoanPaymentType.'

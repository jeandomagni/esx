﻿PRINT N'Seeding NoticeSeverityType.'

SET IDENTITY_INSERT [dESx].[NoticeSeverityType] ON;
MERGE INTO [dESx].[NoticeSeverityType] AS trgt
USING	(VALUES
		(1,N'Info'),
		(2,N'Warning'),
		(3,N'Error'),
		(4,N'Critical')
		) AS src([NoticeSeverityType_Key],[SeverityName])
ON
	trgt.[NoticeSeverityType_Key] = src.[NoticeSeverityType_Key]
WHEN MATCHED THEN
	UPDATE SET
		[SeverityName] = src.[SeverityName]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([NoticeSeverityType_Key],[SeverityName])
	VALUES ([NoticeSeverityType_Key],[SeverityName])
;
SET IDENTITY_INSERT [dESx].[NoticeSeverityType] OFF;

PRINT N'Seeding of NoticeSeverityType complete.'

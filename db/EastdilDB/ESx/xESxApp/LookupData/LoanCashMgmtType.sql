
PRINT N'Seeding LoanCashMgmtType.'

MERGE INTO [dESx].[LoanCashMgmtType] AS trgt
USING	(VALUES
		('DY', 'Debt Yield'),
		('DSCR', 'Debt service coverage ratio'),
		('LTV', 'Loan to Value')
		) AS src([LoanCashMgmtType_Code],[Description])
ON
	trgt.[LoanCashMgmtType_Code] = src.[LoanCashMgmtType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[LoanCashMgmtType_Code] = src.[LoanCashMgmtType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LoanCashMgmtType_Code],[Description])
	VALUES ([LoanCashMgmtType_Code],[Description])
;

PRINT N'Completed LoanCashMgmtType.'

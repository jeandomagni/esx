
PRINT N'Seeding LoanSourceType.'

MERGE INTO [dESx].[LoanSourceType] AS trgt
USING	(VALUES
		('A', 'Aquisition'),
		('R', 'Refinance')
		) AS src([LoanSourceType_Code],[Description])
ON
	trgt.[LoanSourceType_Code] = src.[LoanSourceType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[LoanSourceType_Code] = src.[LoanSourceType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LoanSourceType_Code],[Description])
	VALUES ([LoanSourceType_Code],[Description])
;

PRINT N'Completed LoanSourceType.'

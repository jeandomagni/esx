﻿PRINT N'Seeding Physical Address Type.'

MERGE INTO [dESx].[PhysicalAddressType] AS trgt
USING	(VALUES
		(N'Alt',N''),
		(N'Alternate',N''),
		(N'Business',N''),
		(N'CalEast',N''),
		(N'Chicago',N''),
		(N'China',N''),
		(N'Colorado',N''),
		(N'Company',N''),
		(N'Eastdil',N''),
		(N'Fedex',N''),
		(N'Havorvord',N''),
		(N'Home',N''),
		(N'Japan',N''),
		(N'London',N''),
		(N'Mailing',N''),
		(N'Main',N''),
		(N'maybe',N''),
		(N'New',N''),
		(N'NY',N''),
		(N'Office',N''),
		(N'Overnight',N''),
		(N'Physical',N''),
		(N'PO',N''),
		(N'Primary',N''),
		(N'Purchase',N''),
		(N'Secondary',N''),
		(N'SF',N''),
		(N'Shipping',N''),
		(N'South',N''),
		(N'Street',N''),
		(N'Temp',N''),
		(N'Toronto',N''),
		(N'USPS',N''),
		(N'Work',N'')
		) AS src([PhysicalAddressType_Code],[Description])
ON
	trgt.[PhysicalAddressType_Code] = src.[PhysicalAddressType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[PhysicalAddressType_Code] = src.[PhysicalAddressType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([PhysicalAddressType_Code],[Description])
	VALUES ([PhysicalAddressType_Code],[Description])
;

PRINT N'Seeding of Physical Address Type complete.'
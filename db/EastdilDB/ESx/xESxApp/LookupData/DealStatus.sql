﻿PRINT N'Seeding DealStatus.'

MERGE INTO [dESx].[DealStatus] AS trgt
USING	(VALUES
		(N'Targeting',N'Targeting', 1),
		(N'Pitching',N'Pitching', 2),
		(N'Pitched and Lost',N'Pitched and Lost', 3),
		(N'On Hold',N'On Hold', 4),
		(N'Packaging',N'Packaging', 5),
		(N'Marketing',N'Marketing', 6),
		(N'Bid Receipt',N'Bid Receipt', 7),
		(N'Under Agreement',N'Under Agreement', 8),
		(N'Non-Refundable',N'Non-Refundable', 9),
		(N'Withdrawn by Client',N'Withdrawn by Client', 10),
		(N'Withdrawn by Counterparty',N'Withdrawn by Counterparty', 11),
		(N'Closed',N'Closed', 12)
		) AS src([DealStatus_Code],[Description],[Sort])
ON
	trgt.[DealStatus_Code] = src.[DealStatus_Code]
WHEN MATCHED THEN
	UPDATE SET
		[DealStatus_Code] = src.[DealStatus_Code]
		, [Description] = src.[Description]
		, [Sort] = src.[Sort]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealStatus_Code],[Description],[Sort])
	VALUES ([DealStatus_Code],[Description],[Sort])
;

PRINT N'Seeding of DealStatus complete.'

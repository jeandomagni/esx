﻿PRINT N'Seeding MarketingListStatusType.'

MERGE INTO [dESx].[MarketingListStatusType] AS trgt
USING	(VALUES
		(N'Teaser Sent',N''),
		(N'CA Approved',N''),
		(N'CA Follow Up Terms',N''),
		(N'CA Follow Up Signature',N''),
		(N'War Room Access',N''),
		(N'Call for Offers',N''),
		(N'Additional Materials',N''),
		(N'Printed OM',N'')
		) AS src([MarketingListStatus_Code],[Description])
ON
	trgt.[MarketingListStatus_Code] = src.[MarketingListStatus_Code]
WHEN MATCHED THEN
	UPDATE SET
		[MarketingListStatus_Code] = src.[MarketingListStatus_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([MarketingListStatus_Code],[Description])
	VALUES ([MarketingListStatus_Code],[Description])
;

PRINT N'Seeding of MarketingListStatusType complete.'


PRINT N'Seeding LoanRateType.'

MERGE INTO [dESx].[LoanRateType] AS trgt
USING	(VALUES
		('T', 'Treasury'),
		('S', 'Swap'),
		('L', 'Libor'),
		('E', 'Euribor')
		) AS src([LoanRateType_Code],[Description])
ON
	trgt.[LoanRateType_Code] = src.[LoanRateType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[LoanRateType_Code] = src.[LoanRateType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LoanRateType_Code],[Description])
	VALUES ([LoanRateType_Code],[Description])
;

PRINT N'Completed LoanRateType.'

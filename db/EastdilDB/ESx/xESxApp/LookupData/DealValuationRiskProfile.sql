PRINT N'Seeding DealValuationRiskProfile.'

MERGE INTO [dESx].[DealValuationRiskProfile] AS trgt
USING	(VALUES
		('Trophy', 'Trophy'),
		('Core', 'Core'),
		('Core-Plus', 'Core-Plus'),
		('Value-Add', 'Value-Add')
		) AS src([DealValuationRiskProfile_Code],[Description])
ON
	trgt.[DealValuationRiskProfile_Code] = src.[DealValuationRiskProfile_Code]
WHEN MATCHED THEN
	UPDATE SET
		[DealValuationRiskProfile_Code] = src.[DealValuationRiskProfile_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealValuationRiskProfile_Code],[Description])
	VALUES ([DealValuationRiskProfile_Code],[Description])
;

PRINT N'Completed DealValuationRiskProfile.'
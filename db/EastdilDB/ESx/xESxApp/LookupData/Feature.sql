﻿PRINT N'Seeding Feature.'

MERGE INTO [DESX].[Feature] AS trgt
USING	(VALUES
		(N'Fees',N''),
		(N'Bids',N''),
		(N'application',N'')
		) AS src([Feature_Code],[Description])
ON
	trgt.[Feature_Code] = src.[Feature_Code]
WHEN MATCHED THEN
	UPDATE SET
		[Feature_Code] = src.[Feature_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Feature_Code],[Description])
	VALUES ([Feature_Code],[Description])
;

PRINT N'Seeding of Feature complete.'

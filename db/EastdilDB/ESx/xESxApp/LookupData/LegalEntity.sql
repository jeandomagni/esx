﻿PRINT N'Seeding LegalEntity.'

MERGE INTO [dESx].[LegalEntity] AS trgt
USING	(VALUES
		(N'LGL')
		) AS src([LegalEntity_Code])
ON
	trgt.[LegalEntity_Code] = src.[LegalEntity_Code]
WHEN MATCHED THEN
	UPDATE SET
		[LegalEntity_Code] = src.[LegalEntity_Code]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LegalEntity_Code])
	VALUES ([LegalEntity_Code])
;

PRINT N'Seeding of LegalEntity complete.'

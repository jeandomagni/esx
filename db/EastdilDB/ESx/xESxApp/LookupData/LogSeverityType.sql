﻿PRINT N'Seeding LogSeverityType.'

MERGE INTO [dESx].[LogSeverityType] AS trgt
USING	(VALUES
		(-1,N'Unknown'),
		(0,N'Usage'),
		(1,N'Trace'),
		(2,N'Info'),
		(3,N'Warning'),
		(4,N'Error'),
		(5,N'Critical')
		) AS src([LogSeverityType_Key],[SeverityName])
ON
	trgt.[LogSeverityType_Key] = src.[LogSeverityType_Key]
WHEN MATCHED THEN
	UPDATE SET
		[LogSeverityType_Key] = src.[LogSeverityType_Key]
		, [SeverityName] = src.[SeverityName]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LogSeverityType_Key],[SeverityName])
	VALUES ([LogSeverityType_Key],[SeverityName])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;

PRINT N'Seeding of LogSeverityType complete.'

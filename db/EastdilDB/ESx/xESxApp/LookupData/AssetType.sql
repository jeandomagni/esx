﻿PRINT N'Seeding AssetType.'

MERGE INTO [dESx].[AssetType] AS trgt
USING	(VALUES
		--(N'Condo',N'Condo'),			-- We are not supporting Condo now, and may never
		--(N'Park',N'Park'),			-- Parks are now stored in the "Park" table
		--(N'Partition',N'Partition'),	-- We're not doing anything with Partitions yet, and may never
		(N'Property',N'Property'),
		(N'Loan',N'Loan')
		) AS src([AssetType_Code],[Description])
ON
	trgt.[AssetType_Code] = src.[AssetType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[AssetType_Code] = src.[AssetType_Code]
		, [Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AssetType_Code],[Description])
	VALUES ([AssetType_Code],[Description])
;

PRINT N'Seeding of AssetType complete.'

﻿
PRINT N'Seeding DealContactType.'

MERGE INTO [dESx].[DealContactType] AS trgt
USING	(VALUES
		(N'Acquisitions'			, N'Acquisitions'		   ),
		(N'Dispositions'			, N'Dispositions'		   ),
		(N'Asset Management'		, N'Asset Management'	   ),
		(N'Leasing'					, N'Leasing'			   ),
		(N'Development'				, N'Development'		   ),
		(N'Portfolio Management'	, N'Portfolio Management'  ),
		(N'Executive - (C Suite)'	, N'Executive - (C Suite)' ),
		(N'Financing/Dept'			, N'Financing/Dept'		   )
		) AS src([DealContactType_Code],[Description])
ON
	trgt.[DealContactType_Code] = src.[DealContactType_Code]
WHEN MATCHED THEN
	UPDATE SET
		[Description] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealContactType_Code],[Description])
	VALUES ([DealContactType_Code],[Description])
;
PRINT N'Seeding of DealContactType complete.'


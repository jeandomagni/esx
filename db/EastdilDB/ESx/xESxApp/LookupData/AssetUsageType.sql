﻿
PRINT N'Seeding AssetUsageType.'

SET IDENTITY_INSERT [dESx].[AssetUsageType] ON;

MERGE INTO [dESx].[AssetUsageType] AS trgt
USING	(VALUES
		(2,N'Office',N'Office',N'SF','SEEDDATA'),
		(3,N'Retail',N'Retail',N'SF','SEEDDATA'),
		(4,N'Hotel',N'Hotel',N'Keys','SEEDDATA'),
		(5,N'MultiFamily',N'MultiFamily',N'Units','SEEDDATA'),
		(6,N'Industrial',N'Industrial',N'SF','SEEDDATA'),
		(7,N'Development',N'Development',N'Acres','SEEDDATA'),
		(8,N'Parking',N'Parking',N'Spaces','SEEDDATA')
		) AS src([AssetUsageType_Key],[UsageType],[Description],[SpaceType],[CreatedBy])
ON
	trgt.[AssetUsageType_Key] = src.[AssetUsageType_Key]
WHEN MATCHED THEN
	UPDATE SET
		[UsageType] = src.[UsageType]
		, [Description] = src.[Description]
		, [SpaceType] = src.[SpaceType]
		, [CreatedBy] = src.[CreatedBy]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AssetUsageType_Key],[UsageType],[Description],[SpaceType],[CreatedBy])
	VALUES ([AssetUsageType_Key],[UsageType],[Description],[SpaceType],[CreatedBy])
;
SET IDENTITY_INSERT [dESx].[AssetUsageType] OFF;

PRINT N'Seeding of AssetUsageType complete.'


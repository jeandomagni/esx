﻿PRINT N'Seeding DealTeamRole.'

MERGE INTO [dESx].[DealTeamRole] AS trgt
USING	(VALUES
		(N'PRIMARY')
		) AS src([DealTeamRole_Code])
ON
	trgt.[DealTeamRole_Code] = src.[DealTeamRole_Code]
WHEN MATCHED THEN
	UPDATE SET
		[DealTeamRole_Code] = src.[DealTeamRole_Code]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealTeamRole_Code])
	VALUES ([DealTeamRole_Code])
;

PRINT N'Seeding of DealTeamRole complete.'

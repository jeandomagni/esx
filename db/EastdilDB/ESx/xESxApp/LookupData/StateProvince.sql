﻿PRINT N'Seeding StateProvince.'

MERGE INTO [dESx].[StateProvidence] AS trgt
USING	(VALUES
		(N'AB',N''),
		(N'AK',N''),
		(N'AL',N''),
		(N'AR',N''),
		(N'AZ',N''),
		(N'BC',N''),
		(N'CA',N''),
		(N'CO',N''),
		(N'CT',N''),
		(N'DC',N''),
		(N'DE',N''),
		(N'FL',N''),
		(N'GA',N''),
		(N'HI',N''),
		(N'IA',N''),
		(N'ID',N''),
		(N'IL',N''),
		(N'IN',N''),
		(N'KS',N''),
		(N'KY',N''),
		(N'LA',N''),
		(N'MA',N''),
		(N'MB',N''),
		(N'MD',N''),
		(N'ME',N''),
		(N'MI',N''),
		(N'MN',N''),
		(N'MO',N''),
		(N'MS',N''),
		(N'MT',N''),
		(N'NB',N''),
		(N'NC',N''),
		(N'ND',N''),
		(N'NE',N''),
		(N'NH',N''),
		(N'NJ',N''),
		(N'NL',N''),
		(N'NM',N''),
		(N'NS',N''),
		(N'NV',N''),
		(N'NY',N''),
		(N'OH',N''),
		(N'OK',N''),
		(N'ON',N''),
		(N'OR',N''),
		(N'PA',N''),
		(N'PE',N''),
		(N'PR',N''),
		(N'QC',N''),
		(N'RI',N''),
		(N'SC',N''),
		(N'SD',N''),
		(N'SK',N''),
		(N'TN',N''),
		(N'TX',N''),
		(N'UT',N''),
		(N'VA',N''),
		(N'VT',N''),
		(N'WA',N''),
		(N'WI',N''),
		(N'WV',N''),
		(N'WY',N'')
		) AS src([StateProvidence_Code],[StateProvidenceName])
ON
	trgt.[StateProvidence_Code] = src.[StateProvidence_Code]
WHEN MATCHED THEN
	UPDATE SET
		[StateProvidence_Code] = src.[StateProvidence_Code]
		, [StateProvidenceName] = src.[StateProvidenceName]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([StateProvidence_Code],[StateProvidenceName])
	VALUES ([StateProvidence_Code],[StateProvidenceName])
;

PRINT N'Seeding of StateProvince complete.'

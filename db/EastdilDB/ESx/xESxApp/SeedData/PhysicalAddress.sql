﻿PRINT N'Seeding PhysicalAddress.'

SET IDENTITY_INSERT [dESx].[PhysicalAddress] ON;
;WITH Data AS (SELECT TOP 1000 ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber FROM sys.columns )
MERGE INTO [dESx].[PhysicalAddress] AS trgt
USING	(
		SELECT
			-1 * RowNumber AS PhysicalAddress_Key, 'Primary' AS PhysicalAddressType_Code, NULL AS Description, 
			CAST(RowNumber AS VARCHAR(1000)) + ' Main St' AS Address1, 'Unit #' + CAST(RowNumber AS VARCHAR(1000)) AS Address2, 
			NULL AS Address3, NULL AS Address4,
			'Hometown' AS City, 'MN' AS StateProvidence_Code, 50000 + RowNumber  AS Postal_Code, 
			NULL AS Country, 102 AS Country_Key, NULL AS CrossStreet, 
			geography::Point(RAND(CAST( NEWID() AS varbinary ))*(49-25)+25, -- North and South
								CASE WHEN RowNumber % 2 = 0
								THEN (RAND(CAST( NEWID() AS varbinary ))*(124-91)+91) *-1 --West
								ELSE (RAND(CAST( NEWID() AS varbinary ))*(89-66)+66) * -1 --East
								END
								, 4326) AS Coordinates,
			NULL AS AddressValidationDate, NULL AS LegacyTable, NULL AS LegacyRowId,
			(RAND(CHECKSUM(NEWID())) * (49.31 - 45.99)) + 45.99 AS Latitude,
			(RAND(CHECKSUM(NEWID())) * (-94.84 - -97.22)) + -97.22 AS Longitude
		FROM Data
		) AS src([PhysicalAddress_Key],[PhysicalAddressType_Code],[Description],[Address1],[Address2],[Address3],[Address4],[City],
		[StateProvidence_Code],[Postal_Code],[County],[Country_Key],[CrossStreet],[Coordinates],
		[AddressValidationDate],[LegacyTable],[LegacyRowID], [Latitude], [Longitude])
ON
	trgt.[PhysicalAddress_Key] = src.[PhysicalAddress_Key]
WHEN MATCHED THEN
	UPDATE SET
		[PhysicalAddressType_Code] = src.[PhysicalAddressType_Code]
		, [Description] = src.[Description]
		, [Address1] = src.[Address1]
		, [Address2] = src.[Address2]
		, [Address3] = src.[Address3]
		, [Address4] = src.[Address4]
		, [City] = src.[City]
		, [StateProvidence_Code] = src.[StateProvidence_Code]
		, [Postal_Code] = src.[Postal_Code]
		, [County] = src.[County]
		, [Country_Key] = src.[Country_Key]
		, [CrossStreet] = src.[CrossStreet]
		, [Coordinates] = src.[Coordinates]
		, [AddressValidationDate] = src.[AddressValidationDate]
		, [CreatedBy] = 'SEEDDATA'
		, [Latitude] = src.[Latitude]
		, [Longitude] = src.[Longitude]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([PhysicalAddress_Key],[PhysicalAddressType_Code],[Description],[Address1],[Address2],[Address3],[Address4],[City],[StateProvidence_Code],[Postal_Code],[County],[Country_Key],[CrossStreet],[Coordinates],[AddressValidationDate],[CreatedBy],[Latitude],[Longitude], [UpdatedDate])
	VALUES ([PhysicalAddress_Key],[PhysicalAddressType_Code],[Description],[Address1],[Address2],[Address3],[Address4],[City],[StateProvidence_Code],[Postal_Code],[County],[Country_Key],[CrossStreet],[Coordinates],[AddressValidationDate],'SEEDDATA',[Latitude],[Longitude], '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[PhysicalAddress] OFF;

PRINT N'Seeding of PhysicalAddress complete.'

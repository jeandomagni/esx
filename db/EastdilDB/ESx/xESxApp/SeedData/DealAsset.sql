﻿PRINT N'Seeding DealAsset.'

SET IDENTITY_INSERT [dESx].[DealAsset] ON;
;WITH Data AS (
	SELECT TOP 400
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		ROW_NUMBER()  OVER(ORDER BY OBJECT_ID)%10 AS RowNumberModifier, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[DealAsset] AS trgt
USING	(
			SELECT
				-1 * RowNumber AS DealAsset_Key, 
				-1 * RowNumber %100 -1 AS Deal_Key, 
				-1 * (RowNumber + RowNumberModifier)  % 400 -1 AS Asset_Key,
				'SEEDDATA' AS CreatedBy
			FROM Data
		) AS src([DealAsset_Key],[Deal_Key],[Asset_Key],[CreatedBy])
ON
	trgt.[DealAsset_Key] = src.[DealAsset_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Deal_Key] = src.[Deal_Key]
		, [Asset_Key] = src.[Asset_Key]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealAsset_Key],[Deal_Key],[Asset_Key],[CreatedBy], [UpdatedDate])
	VALUES ([DealAsset_Key],[Deal_Key],[Asset_Key],[CreatedBy], '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[DealAsset] OFF;

--- move properties and loans from deal 1 to deal 2 except for one of them
UPDATE [dESx].[DealAsset]
SET Deal_Key = -2
WHERE DealAsset_Key in (-400, -300, -200)

PRINT N'Seeding of DealAsset complete.'

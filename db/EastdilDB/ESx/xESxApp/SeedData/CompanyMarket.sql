PRINT N'Seeding [CompanyMarket].'

SET IDENTITY_INSERT [dESx].[CompanyMarket] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[CompanyMarket] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			-1 * RowNumber,
			NULL,
			'SEEDDATA'
		FROM Data
		) AS src([CompanyMarket_Key],[Company_Key],[Market_Key],[EndDate],[CreatedBy])
ON
	trgt.[CompanyMarket_Key] = src.[CompanyMarket_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Company_Key] = src.[Company_Key]
		, [Market_Key] = src.[Market_Key]
		, [EndDate] = src.[EndDate]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([CompanyMarket_Key],[Company_Key],[Market_Key],[EndDate],[CreatedBy], [UpdatedDate])
	VALUES ([CompanyMarket_Key],[Company_Key],[Market_Key],[EndDate],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[CompanyMarket] OFF;

PRINT N'Seeding [CompanyMarket] complete.'



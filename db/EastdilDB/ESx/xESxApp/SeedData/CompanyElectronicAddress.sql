﻿PRINT N'Seeding CompanyElectronicAddress.'

SET IDENTITY_INSERT [dESx].[Company_ElectronicAddress] ON;
MERGE INTO [dESx].[Company_ElectronicAddress] AS trgt
USING	(
			SELECT Company_Key AS Company_ElectronicAddress_Key, Company_Key, Company_Key - 200 AS ElectronicAddress_Key 
			FROM dESx.Company WHERE Company_Key < 0
		) AS src([Company_ElectronicAddress_Key],[Company_Key],[ElectronicAddress_Key])
ON
	trgt.[Company_ElectronicAddress_Key] = src.[Company_ElectronicAddress_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Company_Key] = src.[Company_Key]
		, [ElectronicAddress_Key] = src.[ElectronicAddress_Key]
		, [CreatedBy] = 'SEEDDATA'
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Company_ElectronicAddress_Key],[Company_Key],[ElectronicAddress_Key],[CreatedBy], [UpdatedDate])
	VALUES ([Company_ElectronicAddress_Key],[Company_Key],[ElectronicAddress_Key],'SEEDDATA', '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[Company_ElectronicAddress] OFF;

PRINT N'Seeding of CompanyElectronicAddress complete.'


--PRINT N'Seeding Tag.'

--SET IDENTITY_INSERT [dESx].[Tag] ON;

--MERGE INTO [dESx].[Tag] AS trgt
--USING	(VALUES
--		(-1, GETUTCDATE(), GETUTCDATE(), 0)
--		) AS src([Tag_Key],[CreateDate],[LastUseDate],[TotalUseCount])
--ON
--	trgt.[Tag_Key] = src.[Tag_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[Tag_Key] = src.[Tag_Key]
--		, [CreateDate] = src.[CreateDate]
--		, [LastUseDate] = src.[LastUseDate]
--		, [TotalUseCount] = src.[TotalUseCount]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([Tag_Key],[CreateDate],[LastUseDate],[TotalUseCount])
--	VALUES ([Tag_Key],[CreateDate],[LastUseDate],[TotalUseCount])

--;

--SET IDENTITY_INSERT [dESx].[Tag] OFF;

--PRINT N'Seeding Tag complete.'
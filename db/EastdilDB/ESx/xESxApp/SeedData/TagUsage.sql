--PRINT N'Seeding [TagUsage].'

--SET IDENTITY_INSERT [dESx].[TagUsage] ON;

--MERGE INTO [dESx].[TagUsage] AS trgt
--USING	(VALUES
--		(add values here)
--		) AS src([TagUsage_Key],[User_Key],[Tag_Key],[UseCount],[LastUseDate])
--ON
--	trgt.[TagUsage_Key] = src.[TagUsage_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[User_Key] = src.[User_Key]
--		, [Tag_Key] = src.[Tag_Key]
--		, [UseCount] = src.[UseCount]
--		, [LastUseDate] = src.[LastUseDate]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([TagUsage_Key],[User_Key],[Tag_Key],[UseCount],[LastUseDate])
--	VALUES ([TagUsage_Key],[User_Key],[Tag_Key],[UseCount],[LastUseDate])

--;
--SET IDENTITY_INSERT [dESx].[TagUsage] OFF;

--PRINT N'Seeding [TagUsage] complete.'



PRINT N'Seeding [OfficeMarket].'

SET IDENTITY_INSERT [dESx].[OfficeMarket] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[OfficeMarket] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			-1 * RowNumber,
			NULL,
			'SEEDDATA'
		FROM Data
		) AS src([OfficeMarket_Key],[Office_Key],[Market_Key],[EndDate],[CreatedBy])
ON
	trgt.[OfficeMarket_Key] = src.[OfficeMarket_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Office_Key] = src.[Office_Key]
		, [Market_Key] = src.[Market_Key]
		, [EndDate] = src.[EndDate]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([OfficeMarket_Key],[Office_Key],[Market_Key],[EndDate],[CreatedBy], [UpdatedDate])
	VALUES ([OfficeMarket_Key],[Office_Key],[Market_Key],[EndDate],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[OfficeMarket] OFF;

PRINT N'Seeding [OfficeMarket] complete.'


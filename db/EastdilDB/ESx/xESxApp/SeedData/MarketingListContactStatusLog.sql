﻿PRINT N'Seeding MarketingListContactStatusLog.'

SET IDENTITY_INSERT [dESx].[MarketingListContactStatusLog] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel,
		ABS(CHECKSUM(name)) AS Random
	FROM sys.columns 
)
MERGE INTO [dESx].[MarketingListContactStatusLog] AS trgt
USING	(
			SELECT 
				[MarketingListContactStatusLog_Key] = -1 * RowNumber,
				[MarketingListContact_Key] = -1 * RowNumber,
				[MarketingListStatus_Code] = CASE Random % 8
									WHEN 1 THEN 'Teaser Sent'
									WHEN 2 THEN 'CA Approved'
									WHEN 3 THEN 'CA Follow Up Terms'
									WHEN 4 THEN 'CA Follow Up Signature'
									WHEN 5 THEN 'Call for Offers'
									WHEN 6 THEN 'War Room Access'
									WHEN 7 THEN 'Printed OM'
									ELSE 'Additional Materials' END,
				[StatusDate] = DATEADD(DAY, ABS(Random % 3650), '2006-01-02'),
				[CreatedDate] = DATEADD(DAY, ABS(Random % 3650), '2006-01-01'),
				[CreatedBy] = 'SEEDDATA',
				[UpdatedDate] = getutcdate(),
				[UpdatedBy] = 'SEEDDATA'
			FROM Data
		) AS src([MarketingListContactStatusLog_Key],[MarketingListContact_Key],[MarketingListStatus_Code],[StatusDate],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
ON
	trgt.[MarketingListContactStatusLog_Key] = src.[MarketingListContactStatusLog_Key]
WHEN MATCHED THEN
	UPDATE SET
		 [MarketingListContact_Key]		= src.[MarketingListContact_Key]
		,[MarketingListStatus_Code]	 = src.[MarketingListStatus_Code]
		,[StatusDate]		        = src.[StatusDate]
		,[CreatedDate]		        = src.[CreatedDate]
		,[CreatedBy]		        = src.[CreatedBy]
		,[UpdatedDate]		        = src.[UpdatedDate]
		,[UpdatedBy]		        = src.[UpdatedBy]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([MarketingListContactStatusLog_Key],[MarketingListContact_Key],[MarketingListStatus_Code],[StatusDate],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
	VALUES ([MarketingListContactStatusLog_Key],[MarketingListContact_Key],[MarketingListStatus_Code],[StatusDate],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])

;
SET IDENTITY_INSERT [dESx].[MarketingListContactStatusLog] OFF;

PRINT N'Seeding of MarketingListContactStatusLog.'
PRINT N'Seeding [DealValuationCompany].'

SET IDENTITY_INSERT [dESx].[DealValuationCompany] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[DealValuationCompany] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			-1 * RowNumber,
			100,
			'SEEDDATA'
		FROM Data
		) AS src([DealValuationCompany_Key],[DealValuation_Key],[Company_Key],[OwnershipPercent],[CreatedBy])
ON
	trgt.[DealValuationCompany_Key] = src.[DealValuationCompany_Key]
WHEN MATCHED THEN
	UPDATE SET
		[DealValuation_Key] = src.[DealValuation_Key]
		, [Company_Key] = src.[Company_Key]
		, [OwnershipPercent] = src.[OwnershipPercent]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealValuationCompany_Key],[DealValuation_Key],[Company_Key],[OwnershipPercent],[CreatedBy], [UpdatedDate])
	VALUES ([DealValuationCompany_Key],[DealValuation_Key],[Company_Key],[OwnershipPercent],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[DealValuationCompany] OFF;

PRINT N'Seeding [DealValuationCompany] complete.'


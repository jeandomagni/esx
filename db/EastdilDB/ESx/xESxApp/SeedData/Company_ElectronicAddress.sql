
PRINT N'Seeding [Company_ElectronicAddress].'

SET IDENTITY_INSERT [dESx].[Company_ElectronicAddress] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[Company_ElectronicAddress] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			(-1 * RowNumber) * 5,
			'SEEDDATA'
		FROM Data
		) AS src([Company_ElectronicAddress_Key],[Company_Key],[ElectronicAddress_Key],[CreatedBy])
ON
	trgt.[Company_ElectronicAddress_Key] = src.[Company_ElectronicAddress_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Company_Key] = src.[Company_Key]
		, [ElectronicAddress_Key] = src.[ElectronicAddress_Key]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Company_ElectronicAddress_Key],[Company_Key],[ElectronicAddress_Key],[CreatedBy], [UpdatedDate])
	VALUES ([Company_ElectronicAddress_Key],[Company_Key],[ElectronicAddress_Key],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[Company_ElectronicAddress] OFF;

PRINT N'Seeding [Company_ElectronicAddress] complete.'


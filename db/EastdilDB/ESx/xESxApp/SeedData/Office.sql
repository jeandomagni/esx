﻿PRINT N'Seeding Office.'

SET IDENTITY_INSERT [dESx].[Office] ON;
MERGE INTO [dESx].[Office] AS trgt
USING	(

		SELECT Company_Key AS Office_Key, 'Test office headquarters ' + CAST(Company_Key AS VARCHAR(100)) AS OfficeName,
			Company_Key, 1 AS IsHeadquarters, Company_Key AS PhysicalAddress_Key
		FROM dESx.Company WHERE Company_Key < 0

		UNION ALL

		SELECT (Company_Key / 3) - 100 AS Office_Key, 'Test office side ' + CAST(Company_Key AS VARCHAR(100)) AS OfficeName,
			Company_Key, 0 AS IsHeadquarters, (Company_Key / 3) - 100 AS PhysicalAddress_Key
		FROM dESx.Company WHERE Company_Key < 0 AND (Company_Key % 3 = 0)

		) AS src([Office_Key],[OfficeName],[Company_Key],[IsHeadquarters],[PhysicalAddress_Key])
ON
	trgt.[Office_Key] = src.[Office_Key]
WHEN MATCHED THEN
	UPDATE SET
		[OfficeName] = src.[OfficeName]
		, [Company_Key] = src.[Company_Key]
		, [IsHeadquarters] = src.[IsHeadquarters]
		, [PhysicalAddress_Key] = src.[PhysicalAddress_Key]
		, [CreatedBy] = 'SEEDDATA'
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Office_Key],[OfficeName],[Company_Key],[IsHeadquarters],[PhysicalAddress_Key],[CreatedBy], [UpdatedDate])
	VALUES ([Office_Key],[OfficeName],[Company_Key],[IsHeadquarters],[PhysicalAddress_Key],'SEEDDATA', '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[Office] OFF;

PRINT N'Seeding of Office complete.'

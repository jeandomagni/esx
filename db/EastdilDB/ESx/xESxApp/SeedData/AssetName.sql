﻿PRINT N'Seeding AssetName.'

-- assets from -1 - -200 are properies and -200 - -300 are loans

SET IDENTITY_INSERT [dESx].[AssetName] ON;
;WITH Data AS (
	SELECT TOP 400
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[AssetName] AS trgt
USING	(
		SELECT
			-1 * RowNumber AS AssetName_Key, 
			-1 * RowNumber AS Asset_Key, 
			CASE  WHEN RowNumber < 200  THEN RowLabel + ' Property Ln'  ELSE 'Loan #'+ RowLabel  END AS AssetName,
			'1/1/2000' AS ValueStartDate,
			NULL AS ValueEndDate
		FROM Data
		) AS src([AssetName_Key],[Asset_Key],[AssetName],[ValueStartDate],[ValueEndDate])
ON
	trgt.[AssetName_Key] = src.[AssetName_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Asset_Key] = src.[Asset_Key]
		, [AssetName] = src.[AssetName]
		, [CreatedBy] = 'SEEDDATA'
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AssetName_Key],[Asset_Key],[AssetName],[CreatedBy], [UpdatedDate])
	VALUES ([AssetName_Key],[Asset_Key],[AssetName],'SEEDDATA', '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[AssetName] OFF;

PRINT N'Seeding of AssetName complete.'

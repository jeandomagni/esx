--PRINT N'Seeding [Comment].'

--SET IDENTITY_INSERT [dESx].[Comment] ON;

--MERGE INTO [dESx].[Comment] AS trgt
--USING	(VALUES
--		(add values here)
--		) AS src([Comment_Key],[Implicit__Mention_Key],[CommentText],[Create__User_Key],[CreateTime],[Update__User_Key],[UpdateTime])
--ON
--	trgt.[Comment_Key] = src.[Comment_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[Implicit__Mention_Key] = src.[Implicit__Mention_Key]
--		, [CommentText] = src.[CommentText]
--		, [Create__User_Key] = src.[Create__User_Key]
--		, [CreateTime] = src.[CreateTime]
--		, [Update__User_Key] = src.[Update__User_Key]
--		, [UpdateTime] = src.[UpdateTime]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([Comment_Key],[Implicit__Mention_Key],[CommentText],[Create__User_Key],[CreateTime],[Update__User_Key],[UpdateTime])
--	VALUES ([Comment_Key],[Implicit__Mention_Key],[CommentText],[Create__User_Key],[CreateTime],[Update__User_Key],[UpdateTime])

--;
--SET IDENTITY_INSERT [dESx].[Comment] OFF;

--PRINT N'Seeding [Comment] complete.'


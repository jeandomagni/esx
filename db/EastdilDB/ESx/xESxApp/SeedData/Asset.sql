﻿PRINT N'Seeding Asset.'

-- assets from -1 - -200 are properies and -200 - -300 are loans

SET IDENTITY_INSERT [dESx].[Asset] ON;

;WITH Data AS (
	SELECT TOP 400
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[Asset] AS trgt
USING	(
		SELECT
			-1 * RowNumber AS Asset_Key, 
			CASE  WHEN RowNumber < 200 THEN 'Property' ELSE 'Loan' END AS AssetType_Code,
			'SEEDDATA' AS CreatedBy,
			-1 * RowNumber AS PhysicalAddress_Key
		FROM Data
		) AS src([Asset_Key],[AssetType_Code],[CreatedBy],[PhysicalAddress_Key])
ON
	trgt.[Asset_Key] = src.[Asset_Key]
WHEN MATCHED THEN
	UPDATE SET
		[AssetType_Code] = src.[AssetType_Code]
		, [CreatedBy] = src.[CreatedBy]
		, [PhysicalAddress_Key] = src.[PhysicalAddress_Key]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Asset_Key],[AssetType_Code],[CreatedBy],[PhysicalAddress_Key])
	VALUES ([Asset_Key],[AssetType_Code],[CreatedBy],[PhysicalAddress_Key])
;
SET IDENTITY_INSERT [dESx].[Asset] OFF;

PRINT N'Seeding of Asset complete.'

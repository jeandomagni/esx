PRINT N'Seeding [User].'

SET IDENTITY_INSERT [dESx].[User] ON;

MERGE INTO [dESx].[User] AS trgt
USING	( VALUES
		(-1, 'AD-ENT\U485076', GETUTCDATE(), 1, -101 ),
		(-5, 'AD-ENT\U460377', GETUTCDATE(), 1, -102 ),
		(-6, 'AD-ENT\U479022', GETUTCDATE(), 1, -103 ),
		(-7, 'AD-ENT\A170254', GETUTCDATE(), 1, -104 ),
		(-8, 'AD-ENT\u042821', GETUTCDATE(), 1, -105 ),
		(-9, 'AD-ENT\u460379', GETUTCDATE(), 1, -106 ),
		(-10, 'AD-ENT\u485073', GETUTCDATE(), 1, -107),
		(-11, 'AD-ENT\u460383', GETUTCDATE(), 1, -108 ),
		(-12, 'AD-ENT\u469335', GETUTCDATE(), 1, -109 ),
		(-13, 'AD-ENT\u460385', GETUTCDATE(), 1, -110 ),
		(-14, 'AD-ENT\A147903', GETUTCDATE(), 1, -111 ),
		(-15, 'AD-ENT\u470172', GETUTCDATE(), 1, -113 ),
		(-16, 'AD-ENT\u460374', GETUTCDATE(), 1, -112 )
		) AS src([User_Key],[LoginName],[LastLoginDate],[HasAccess],[Contact_Key])
ON
	trgt.[User_Key] = src.[User_Key]
WHEN MATCHED THEN
	UPDATE SET
		[LoginName] = src.[LoginName]
		, [LastLoginDate] = src.[LastLoginDate]
		, [HasAccess] = src.[HasAccess]
		, [Contact_Key] = src.[Contact_Key]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([User_Key],[LoginName],[LastLoginDate],[HasAccess],[Contact_Key])
	VALUES ([User_Key],[LoginName],[LastLoginDate],[HasAccess],[Contact_Key])
;

SET IDENTITY_INSERT [dESx].[User] OFF;

PRINT N'Seeding [User] complete.'


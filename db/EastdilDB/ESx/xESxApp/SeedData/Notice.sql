--PRINT N'Seeding [Notice].'

--SET IDENTITY_INSERT [dESx].[Notice] ON;

--MERGE INTO [dESx].[Notice] AS trgt
--USING	(VALUES
--		(add values here)
--		) AS src([Notice_Key],[NoticeSeverityType_Key],[Generating__User_Key],[Subject__Mention_Key],[NoticeText],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
--ON
--	trgt.[Notice_Key] = src.[Notice_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[NoticeSeverityType_Key] = src.[NoticeSeverityType_Key]
--		, [Generating__User_Key] = src.[Generating__User_Key]
--		, [Subject__Mention_Key] = src.[Subject__Mention_Key]
--		, [NoticeText] = src.[NoticeText]
--		, [CreatedDate] = src.[CreatedDate]
--		, [CreatedBy] = src.[CreatedBy]
--		, [UpdatedDate] = src.[UpdatedDate]
--		, [UpdatedBy] = src.[UpdatedBy]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([Notice_Key],[NoticeSeverityType_Key],[Generating__User_Key],[Subject__Mention_Key],[NoticeText],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
--	VALUES ([Notice_Key],[NoticeSeverityType_Key],[Generating__User_Key],[Subject__Mention_Key],[NoticeText],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])

--;
--SET IDENTITY_INSERT [dESx].[Notice] OFF;

--PRINT N'Seeding [Notice] complete.'


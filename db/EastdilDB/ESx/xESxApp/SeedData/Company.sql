﻿PRINT N'Seeding Company.'

SET IDENTITY_INSERT [dESx].[Company] ON;
MERGE INTO [dESx].[Company] AS trgt
USING	(VALUES
		--select top 100 CompanyName from ESNetDBTransition.[dbo].Companies c order by c.CompanyID desc
		(-1, N'Furnished Quarters',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-2, N'Seventh Avenue Capital',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-3, N'Albany Road Real Estate Partners',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-4, N'Circle F Capital',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-5, N'Magsari Holdings LLC',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-6, N'Posel Management Company',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-7, N'EDENS',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-8, N'AMT Investments',N'',N'',NULL,N'Investor',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-9, N'Sequoia Equity Partners, LLC',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-10,N'PKF Consulting',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-11,N'Banyan Street Capital, LLC',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-12,N'Southern States Management',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-13,N'Emmes Realty Services of California, LLC',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-14,N'Stewart DeLuca',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-15,N'ULLIC',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-16,N'Capri Investment Group',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-17,N'Rambleside Holdings LLC',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-18,N'Host Hotels',N'',N'',NULL,N'Investor',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-19,N'Academy of Art University',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-20,N'Samsung Securities Co. Ltd.',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-21,N'Lodha Developers UK Limited',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-22,N'EB Development',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-23,N'FFO Real Estate Advisors',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-24,N'Sotheby''s International Realty',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-25,N'Hobart',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-26,N'Build Capital',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-27,N'Union Bank, N.A.',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-28,N'Lennox Capital Partners, LP',N'',N'',NULL,N'Investor',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-29,N'Double A',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-30,N'BLD Brasil',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-31,N'Steigenberger Hotel Group',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-32,N'Valor Hospitality',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-33,N'Valor Hospitalty',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-34,N'BNP Paribas Real Estate',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-35,N'Origin Capital Partners',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-36,N'Equity Resource Investments',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-37,N'The Howard Hughes Corporation',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-38,N'Stepstone Hospitality',N'',N'',NULL,N'Investor',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-39,N'Wells Fargo',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-40,N'GEMS Americas',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-41,N'Jason Oberman',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-42,N'Seer Capital Management LP',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-43,N'Paris Inn Group',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-44,N'Wilshire Skyline, Inc.',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-45,N'Western National Group',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-46,N'Watermark Capital Group',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-47,N'Virtu Investments',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-48,N'Starlight Investments',N'',N'',NULL,N'Investor',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-49,N'Skinner Development Group, Inc.',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-50,N'Redwood Capital Group',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-51,N'Realm Development',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-52,N'Real Estate Global Partners',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-53,N'RBP Investment Group',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-54,N'Prudential Commercial Real Estate',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-55,N'New Crescent Investments',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-56,N'CBRE Global Investors',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-57,N'Rooney Properties',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-58,N'TA  Patty Development',N'',N'',NULL,N'Investor',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-59,N'Andrew Gordon',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-60,N'Interstate Hotels & Resorts',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-61,N'Stanford University',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-62,N'Queally Group Incorporated',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-63,N'Hengeler Mueller',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-64,N'Colony Captial',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-65,N'Runyon Group',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-66,N'Cheerland Investment Corporation',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-67,N'Capital Foresight',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-68,N'The Dezerland',N'',N'',NULL,N'Investor',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-69,N'Zugimpex International GmbH',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-70,N'949 Group; The',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-71,N'Embrace Hospitality',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-72,N'Bond Street Hospitality',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-73,N'Millennium Group',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-74,N'Munchener Hypothekenbank eG',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-75,N'Roya International',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-76,N'Grant Group',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-77,N'Eurobank Ergasias S.A',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-78,N'Alpha Bank',N'',N'',NULL,N'Investor',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-79,N'Questex Hospitality Group',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-80,N'Kadenwood Partners Limited (UK)',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-81,N'Meir Teper/Ellison Investments',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-82,N'Manhattan Building Company',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-83,N'Alfred Realty',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-84,N'Mega International Commercial Bank',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-85,N'Banco Popular N.A., Inc',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-86,N'Olshan Properties',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-87,N'Llewellyn Development, LLC',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-88,N'Katara Hospitality',N'',N'',NULL,N'Investor',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-89,N'Greene & Co',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-90,N'Standard Chartered Bank',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-91,N'Standard Chartered Bank',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-92,N'JAG LLC',N'',N'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-93,N'Prudential Commercial Real Estate',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-94,N'Prudential Commercial Real Estate',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-95,N'Premier Pacific Properties',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-96,N'Pacifica Equities, LLC',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-97,N'Pacific Development Partners',N'',N'',NULL,N'',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-98,N'Pacific Allied Asset Management',N'',N'',NULL,N'Investor',N'',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-99,N'OSM Investment Company',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL),
		(-100,N'Imian Partners',N'',N'',NULL,N'',N'- None -',N'',NULL,NULL,1,0,0,0,0,0,0,0,NULL,'Test',NULL)

		) AS src([Company_Key],[Division],[TickerSymbol],[CibosCode],[FcpaName],[CompanyType],[EsClassification],[InstitutionType],[OfacCheckDate],[IsSolicitableDate],[IsCanonical],[IsDefunct],[IsSolicitable],[IsForeignGovernment],[IsTrackedByAsiaTeam],[HasInterestedInDeals],[HasWfsCorrelation],[HasRestrictedVisibility],[RealEstateAllocation],[LegacyType],[LegacyRowID])
ON
	trgt.[Company_Key] = src.[Company_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Division] = src.[Division]
		, [TickerSymbol] = src.[TickerSymbol]
		, [CibosCode] = src.[CibosCode]
		, [FcpaName] = src.[FcpaName]
		, [CompanyType] = src.[CompanyType]
		, [EsClassification] = src.[EsClassification]
		, [InstitutionType] = src.[InstitutionType]
		, [OfacCheckDate] = src.[OfacCheckDate]
		, [IsSolicitableDate] = src.[IsSolicitableDate]
		, [IsCanonical] = src.[IsCanonical]
		, [IsDefunct] = src.[IsDefunct]
		, [IsSolicitable] = src.[IsSolicitable]
		, [IsForeignGovernment] = src.[IsForeignGovernment]
		, [IsTrackedByAsiaTeam] = src.[IsTrackedByAsiaTeam]
		, [HasInterestedInDeals] = src.[HasInterestedInDeals]
		, [HasWfsCorrelation] = src.[HasWfsCorrelation]
		, [HasRestrictedVisibility] = src.[HasRestrictedVisibility]
		, [RealEstateAllocation] = src.[RealEstateAllocation]
		, [CreatedBy] = 'seeddata'
		, [CreatedDate] = '2006-01-01'
		, [UpdatedBy] = 'seeddata'
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Company_Key],[Division],[TickerSymbol],[CibosCode],[FcpaName],[CompanyType],[EsClassification],[InstitutionType],[OfacCheckDate],[IsSolicitableDate],[IsCanonical],[IsDefunct],[IsSolicitable],[IsForeignGovernment],[IsTrackedByAsiaTeam],[HasInterestedInDeals],[HasWfsCorrelation],[HasRestrictedVisibility],[RealEstateAllocation],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
	VALUES ([Company_Key],[Division],[TickerSymbol],[CibosCode],[FcpaName],[CompanyType],[EsClassification],[InstitutionType],[OfacCheckDate],[IsSolicitableDate],[IsCanonical],[IsDefunct],[IsSolicitable],[IsForeignGovernment],[IsTrackedByAsiaTeam],[HasInterestedInDeals],[HasWfsCorrelation],[HasRestrictedVisibility],[RealEstateAllocation],'2006-01-01','seeddata','2006-01-01','seeddata')
;
SET IDENTITY_INSERT [dESx].[Company] OFF;

PRINT N'Seeding of Company complete.'

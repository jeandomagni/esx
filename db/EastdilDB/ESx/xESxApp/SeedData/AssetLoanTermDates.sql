PRINT N'Seeding [AssetLoanTermDates].'

SET IDENTITY_INSERT [dESx].[AssetLoanTermDates] ON;

;WITH Data AS (
	SELECT TOP 200
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel,
		ABS(CHECKSUM(name)) AS Random
	FROM sys.columns 
)
MERGE INTO [dESx].[AssetLoanTermDates] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber -200,
			CASE Random % 4
					WHEN 0 THEN 'YieldMaintenanceEnd'
					WHEN 1 THEN 'NextMaturity'
					WHEN 2 THEN 'FinalMaturity'
					WHEN 3 THEN 'LockoutEnd'
					ELSE NULL
					END,
			'1/1/2010'
		FROM Data
		) AS src([AssetLoanTermDates_Key],[Asset_Key],[LoanTermDateType_Code],[LoanTermDate])
ON
	trgt.[AssetLoanTermDates_Key] = src.[AssetLoanTermDates_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Asset_Key] = src.[Asset_Key]
		, [LoanTermDateType_Code] = src.[LoanTermDateType_Code]
		, [LoanTermDate] = src.[LoanTermDate]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AssetLoanTermDates_Key],[Asset_Key],[LoanTermDateType_Code],[LoanTermDate])
	VALUES ([AssetLoanTermDates_Key],[Asset_Key],[LoanTermDateType_Code],[LoanTermDate])
;

SET IDENTITY_INSERT [dESx].[AssetLoanTermDates] OFF;

PRINT N'Seeding [AssetLoanTermDates] complete.'

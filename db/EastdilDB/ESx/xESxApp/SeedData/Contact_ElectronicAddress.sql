
PRINT N'Seeding [Contact_ElectronicAddress].'

SET IDENTITY_INSERT [dESx].[Contact_ElectronicAddress] ON;

;WITH Data AS (
	SELECT TOP 1000
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[Contact_ElectronicAddress] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber % 100 - 1,
			-1 * RowNumber,
			'SEEDDATA'
		FROM Data
		) AS src([Contact_ElectronicAddress_Key],[Contact_Key],[ElectronicAddress_Key],[CreatedBy])
ON
	trgt.[Contact_ElectronicAddress_Key] = src.[Contact_ElectronicAddress_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Contact_Key] = src.[Contact_Key]
		, [ElectronicAddress_Key] = src.[ElectronicAddress_Key]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Contact_ElectronicAddress_Key],[Contact_Key],[ElectronicAddress_Key],[CreatedBy], [UpdatedDate])
	VALUES ([Contact_ElectronicAddress_Key],[Contact_Key],[ElectronicAddress_Key],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[Contact_ElectronicAddress] OFF;

PRINT N'Seeding [Contact_ElectronicAddress] complete.'


﻿PRINT N'Seeding DealValuation.'

SET IDENTITY_INSERT [dESx].[DealValuation] ON;
;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel,
		ABS(CHECKSUM(name)) AS Random
	FROM sys.columns 
)
MERGE INTO [dESx].[DealValuation] AS trgt
USING	(
		SELECT
			 [DealValuation_Key] = -1 * RowNumber 
			,[Deal_Key] = -1 * RowNumber
			,[BidStatusCode]         = CASE WHEN RowNumber % 2 = 0 THEN 'W' ELSE 'D' END
			,[PercentOverPitchValue] = 10 + (RowNumber * 0.01)
			,[PercentOverLastBid]    = 2 + (RowNumber * 0.01)
			,[ValuationName] = 'Test Valuation #' + RowLabel
			,[DealValuationType_Code] = CASE Random % 4
										WHEN 0 THEN 'BaseCase'
										WHEN 1 THEN 'PushCase'
										WHEN 2 THEN 'Bid'
										WHEN 3 THEN 'CustomName'
										END
			,[Value] = 125000000
			,[CreatedBy] = 'SEEDDATA'
			,[DealValuationRiskProfile_Code] = CASE Random % 4
										WHEN 0 THEN 'Trophy'
										WHEN 1 THEN 'Core'
										WHEN 2 THEN 'Core-Plus'
										WHEN 3 THEN 'Value-Add'
										END
		FROM Data
		) AS src([DealValuation_Key],[Deal_Key],[BidStatusCode],[PercentOverPitchValue],[PercentOverLastBid],[ValuationName],[DealValuationType_Code],[Value],[CreatedBy],[DealValuationRiskProfile_Code])
ON
	trgt.[DealValuation_Key] = src.[DealValuation_Key]
WHEN MATCHED THEN
	UPDATE SET
		  [Deal_Key]                      = src.[Deal_Key]
		, [BidStatusCode]                 = src.[BidStatusCode]
		, [PercentOverPitchValue]         = src.[PercentOverPitchValue]
		, [PercentOverLastBid]            = src.[PercentOverLastBid]
		, [ValuationName]                 = src.[ValuationName]
		, [DealValuationType_Code]        = src.[DealValuationType_Code]
		, [Value]                         = src.[Value]
		, [CreatedBy]                     = src.[CreatedBy]
		, [DealValuationRiskProfile_Code] = src.[DealValuationRiskProfile_Code]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealValuation_Key],[Deal_Key],[BidStatusCode],[PercentOverPitchValue],[PercentOverLastBid],[ValuationName],[DealValuationType_Code],[Value],[CreatedBy], [UpdatedDate],[DealValuationRiskProfile_Code])
	VALUES ([DealValuation_Key],[Deal_Key],[BidStatusCode],[PercentOverPitchValue],[PercentOverLastBid],[ValuationName],[DealValuationType_Code],[Value],[CreatedBy], '2006-01-01', [DealValuationRiskProfile_Code])
;
SET IDENTITY_INSERT [dESx].[DealValuation] OFF;

PRINT N'Seeding of DealValuation complete.'

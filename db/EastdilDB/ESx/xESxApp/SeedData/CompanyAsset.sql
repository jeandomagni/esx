
PRINT N'Seeding [CompanyAsset].'

-- assign 2 properties and 1 loan to each company

SET IDENTITY_INSERT [dESx].[CompanyAsset] ON;

;WITH Data AS (
	SELECT TOP 300
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[CompanyAsset] AS trgt
USING	(
		SELECT 
			-1 * RowNumber ,
			-1 * RowNumber % 100 -1,
			-1 * RowNumber,
			100,
			NULL,
			'SEEDDATA'
		FROM Data
		) AS src([CompanyAsset_Key],[Company_Key],[Asset_Key],[OwnershipPercent],[EndDate],[CreatedBy])
ON
	trgt.[CompanyAsset_Key] = src.[CompanyAsset_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Company_Key] = src.[Company_Key]
		, [Asset_Key] = src.[Asset_Key]
		, [OwnershipPercent] = src.[OwnershipPercent]
		, [EndDate] = src.[EndDate]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([CompanyAsset_Key],[Company_Key],[Asset_Key],[OwnershipPercent],[EndDate],[CreatedBy], [UpdatedDate])
	VALUES ([CompanyAsset_Key],[Company_Key],[Asset_Key],[OwnershipPercent],[EndDate],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[CompanyAsset] OFF;

PRINT N'Seeding [CompanyAsset] complete.'


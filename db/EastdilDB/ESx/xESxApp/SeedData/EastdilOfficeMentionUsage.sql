--PRINT N'Seeding [EastdilOfficeMentionUsage].'

--SET IDENTITY_INSERT [dESx].[EastdilOfficeMentionUsage] ON;

--MERGE INTO [dESx].[EastdilOfficeMentionUsage] AS trgt
--USING	(VALUES
--		(add values here)
--		) AS src([EastdilOfficeMentionUsage_Key],[EastdilOffice_Key],[Mention_key],[UseCount],[LastUseDate],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
--ON
--	trgt.[EastdilOfficeMentionUsage_Key] = src.[EastdilOfficeMentionUsage_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[EastdilOffice_Key] = src.[EastdilOffice_Key]
--		, [Mention_key] = src.[Mention_key]
--		, [UseCount] = src.[UseCount]
--		, [LastUseDate] = src.[LastUseDate]
--		, [CreatedDate] = src.[CreatedDate]
--		, [CreatedBy] = src.[CreatedBy]
--		, [UpdatedDate] = src.[UpdatedDate]
--		, [UpdatedBy] = src.[UpdatedBy]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([EastdilOfficeMentionUsage_Key],[EastdilOffice_Key],[Mention_key],[UseCount],[LastUseDate],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
--	VALUES ([EastdilOfficeMentionUsage_Key],[EastdilOffice_Key],[Mention_key],[UseCount],[LastUseDate],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])

--;
--SET IDENTITY_INSERT [dESx].[EastdilOfficeMentionUsage] OFF;

--PRINT N'Seeding [EastdilOfficeMentionUsage] complete.'


PRINT N'Seeding [AssetValue].'

SET IDENTITY_INSERT [dESx].[AssetValue] ON;

;WITH Data AS (
	SELECT TOP 250
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[AssetValue] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			12500000 + (15000 * RowNumber),
			NULL,
			'SEEDDATA',
			(RowNumber % 10) + 1,
			'units'
		FROM Data
		) AS src([AssetValue_Key],[Asset_Key],[AssetValue],[EndDate],[CreatedBy], [Spaces], [SpaceType])
ON
	trgt.[AssetValue_Key] = src.[AssetValue_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Asset_Key] = src.[Asset_Key]
		, [AssetValue] = src.[AssetValue]
		, [EndDate] = src.[EndDate]
		, [CreatedBy] = src.[CreatedBy]
		, [Spaces] = src.[Spaces]
		, [SpaceType] = src.[SpaceType]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AssetValue_Key],[Asset_Key],[AssetValue],[EndDate],[CreatedBy], [Spaces], [SpaceType], [UpdatedDate])
	VALUES ([AssetValue_Key],[Asset_Key],[AssetValue],[EndDate],[CreatedBy], [Spaces], [SpaceType], '2006-01-01')

;
SET IDENTITY_INSERT [dESx].[AssetValue] OFF;

PRINT N'Seeding [AssetValue] complete.'
--PRINT N'Seeding [Comment_Mention].'

--SET IDENTITY_INSERT [dESx].[Comment_Mention] ON;

--MERGE INTO [dESx].[Comment_Mention] AS trgt
--USING	(VALUES
--		(add values here)
--		) AS src([Comment_Mention_Key],[Comment_Key],[Mention_Key])
--ON
--	trgt.[Comment_Mention_Key] = src.[Comment_Mention_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[Comment_Key] = src.[Comment_Key]
--		, [Mention_Key] = src.[Mention_Key]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([Comment_Mention_Key],[Comment_Key],[Mention_Key])
--	VALUES ([Comment_Mention_Key],[Comment_Key],[Mention_Key])

--;
--SET IDENTITY_INSERT [dESx].[Comment_Mention] OFF;

--PRINT N'Seeding [Comment_Mention] complete.'


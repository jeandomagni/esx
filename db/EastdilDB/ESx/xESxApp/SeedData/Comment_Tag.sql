--PRINT N'Seeding [Comment_Tag].'

--SET IDENTITY_INSERT [dESx].[Comment_Tag] ON;

--MERGE INTO [dESx].[Comment_Tag] AS trgt
--USING	(VALUES
--		(add values here)
--		) AS src([Comment_Tag_Key],[Comment_Key],[Tag_Key])
--ON
--	trgt.[Comment_Tag_Key] = src.[Comment_Tag_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[Comment_Key] = src.[Comment_Key]
--		, [Tag_Key] = src.[Tag_Key]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([Comment_Tag_Key],[Comment_Key],[Tag_Key])
--	VALUES ([Comment_Tag_Key],[Comment_Key],[Tag_Key])

--;
--SET IDENTITY_INSERT [dESx].[Comment_Tag] OFF;

--PRINT N'Seeding [Comment_Tag] complete.'


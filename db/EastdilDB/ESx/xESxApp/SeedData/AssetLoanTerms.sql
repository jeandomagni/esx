PRINT N'Seeding [AssetLoanTerms].'

;WITH Data AS (
	SELECT TOP 200
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[AssetLoanTerms] AS trgt
USING	(
		SELECT 
			-1 * RowNumber -200,
			'Loan ' + RowLabel,
			1000 + (250000 * RowNumber * RowNumber),
			'1/1/2013',
			'1/1/2018',
			'DB',
			'A',
			'DF',
			[LoanRateType_Code] = CASE WHEN RowNumber < 50 THEN 'E' ELSE 'T' END,
			'L',
			'DY',
			'SEEDDATA',
			 (ABS(CHECKSUM(NewId())) % 20) + 1,
			 [LoanDuration] = CASE WHEN RowNumber < 50 THEN (ABS(CHECKSUM(NewId())) % 200) + 1 ELSE NULL END,
			 [CreditSpread] = CASE WHEN RowNumber < 50 THEN (ABS(CHECKSUM(NewId())) % 250) + 1 ELSE NULL END,
			 [LoanToValue] = RAND()*(100-1)+1,
			 [DebtYield] = RAND()*(100-1)+1
		FROM Data
		) AS src([Asset_Key],[LoanName],[LoanAmount],[OriginationDate],[MaturationDate],[LoanLenderType_Code],
		[LoanSourceType_Code],[LoanPaymentType_Code],[LoanRateType_Code],[LoanBenchmarkType_Code],
		[LoanCashMgmtType_Code],[CreatedBy], [LoanRate], [LoanDuration],[CreditSpread],[LoanToValue],[DebtYield])
ON
	trgt.[Asset_Key] = src.[Asset_Key]
WHEN MATCHED THEN
	UPDATE SET
		[LoanName] = src.[LoanName]
		, [LoanAmount] = src.[LoanAmount]
		, [OriginationDate] = src.[OriginationDate]
		, [MaturationDate] = src.[MaturationDate]
		, [LoanLenderType_Code] = src.[LoanLenderType_Code]
		, [LoanSourceType_Code] = src.[LoanSourceType_Code]
		, [LoanPaymentType_Code] = src.[LoanPaymentType_Code]
		, [LoanRateType_Code] = src.[LoanRateType_Code]
		, [LoanBenchmarkType_Code] = src.[LoanBenchmarkType_Code]
		, [LoanCashMgmtType_Code] = src.[LoanCashMgmtType_Code]
		, [CreatedBy] = src.[CreatedBy]
		, [LoanRate] = src.[LoanRate]
		, [LoanDuration] = src.[LoanDuration]
		, [CreditSpread] = src.[CreditSpread]
		, [LoanToValue] = src.[LoanToValue]
		, [DebtYield] = src.[DebtYield]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Asset_Key],[LoanName],[LoanAmount],[OriginationDate],[MaturationDate],[LoanLenderType_Code],[LoanSourceType_Code],[LoanPaymentType_Code],[LoanRateType_Code],[LoanBenchmarkType_Code],[LoanCashMgmtType_Code],[CreatedBy],[LoanRate], [LoanDuration], [CreditSpread],[LoanToValue],[DebtYield], [UpdatedDate])
	VALUES ([Asset_Key],[LoanName],[LoanAmount],[OriginationDate],[MaturationDate],[LoanLenderType_Code],[LoanSourceType_Code],[LoanPaymentType_Code],[LoanRateType_Code],[LoanBenchmarkType_Code],[LoanCashMgmtType_Code],[CreatedBy],[LoanRate], [LoanDuration], [CreditSpread],[LoanToValue],[DebtYield], '2006-01-01')
;

PRINT N'Seeding [AssetLoanTerms] complete.'
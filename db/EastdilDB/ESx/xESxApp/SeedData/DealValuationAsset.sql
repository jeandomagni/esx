﻿PRINT N'Seeding DealValuationAsset.'

SET IDENTITY_INSERT [dESx].[DealValuationAsset] ON;
;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[DealValuationAsset] AS trgt
USING	(
		SELECT
			[DealValuationAsset_Key] = -1 * RowNumber, 
			[DealValuation_Key] = -1 * RowNumber, 
			[DealAsset_Key] = -1 * RowNumber,
			'SEEDDATA'
		FROM Data
		) AS src([DealValuationAsset_Key],[DealValuation_Key],[DealAsset_Key],[CreatedBy])
ON
	trgt.[DealValuationAsset_Key] = src.[DealValuationAsset_Key]
WHEN MATCHED THEN
	UPDATE SET
		[DealValuation_Key] = src.[DealValuation_Key]
		, [DealAsset_Key] = src.[DealAsset_Key]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealValuationAsset_Key],[DealValuation_Key],[DealAsset_Key],[CreatedBy], [UpdatedDate])
	VALUES ([DealValuationAsset_Key],[DealValuation_Key],[DealAsset_Key],[CreatedBy], '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[DealValuationAsset] OFF;

PRINT N'Seeding of DealValuationAsset complete.'

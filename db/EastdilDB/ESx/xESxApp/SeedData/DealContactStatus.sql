﻿PRINT N'Seeding DealContactStatus.'

SET IDENTITY_INSERT [dESx].[DealContactStatus] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[DealContactStatus] AS trgt
USING	(
		SELECT
			-1 * RowNumber AS DealContactStatus_Key, 
			-1 * RowNumber AS DealContact_Key, 
			'STS' AS ContactStatus_Code,
			'1/1/2000' AS StatusDate,
			'SEEDDATA' AS CreatedBy
		FROM Data
		) AS src([DealContactStatus_Key],[DealContact_Key],[ContactStatus_Code],[StatusDate],[CreatedBy])
ON
	trgt.[DealContactStatus_Key] = src.[DealContactStatus_Key]
WHEN MATCHED THEN
	UPDATE SET
		[DealContact_Key] = src.[DealContact_Key]
		, [ContactStatus_Code] = src.[ContactStatus_Code]
		, [StatusDate] = src.[StatusDate]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealContactStatus_Key],[DealContact_Key],[ContactStatus_Code],[StatusDate],[CreatedBy], [UpdatedDate])
	VALUES ([DealContactStatus_Key],[DealContact_Key],[ContactStatus_Code],[StatusDate],[CreatedBy], '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[DealContactStatus] OFF;

PRINT N'Seeding of DealContactStatus complete.'

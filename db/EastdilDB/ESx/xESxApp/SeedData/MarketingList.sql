﻿PRINT N'Seeding [MarketingList].'

SET IDENTITY_INSERT [dESx].[MarketingList] ON;

;WITH Data AS (
	SELECT TOP 20
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[MarketingList] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			'SEEDDATA'
		FROM Data
		) AS src([MarketingList_Key],[Deal_Key],[CreatedBy])
ON
	trgt.[MarketingList_Key] = src.[MarketingList_Key]
WHEN MATCHED THEN
	UPDATE SET
        [Deal_Key] = src.[Deal_Key]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([MarketingList_Key],[Deal_Key],[CreatedBy], [UpdatedDate])
	VALUES ([MarketingList_Key],[Deal_Key],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[MarketingList] OFF;

PRINT N'Seeding [MarketingList] complete.'


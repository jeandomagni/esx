﻿PRINT N'Seeding FeatureRole.'

MERGE INTO [DESX].[FeatureRole] AS trgt
USING	(VALUES
		(N'application',N'DTCA_WSD_ESX_USERS', 1),
		(N'Fees',N'DTCG_WSD_ESPREPORTS_FEES', 1),
		(N'Bids',N'DTCA_WSD_ESX_USERS', 1)
		) AS src([Feature_Code],[Role_Code], [HasAccess])
ON
	trgt.[Feature_Code] = src.[Feature_Code] AND trgt.[Role_Code] = src.[Role_Code]
WHEN MATCHED THEN
	UPDATE SET
		[Feature_Code] = src.[Feature_Code]
		,[Role_Code] = src.[Role_Code]
		, [HasAccess] = src.[HasAccess]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Feature_Code],[Role_Code], [HasAccess])
	VALUES ([Feature_Code],[Role_Code], [HasAccess])
WHEN NOT MATCHED BY SOURCE THEN
	DELETE
;

PRINT N'Seeding of FeatureRole complete.'

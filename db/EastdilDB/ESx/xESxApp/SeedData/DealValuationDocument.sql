
--PRINT N'Seeding [DealValuationDocument].'

--SET IDENTITY_INSERT [dESx].[DealValuationDocument] ON;

--;WITH Data AS (
--	SELECT TOP 100
--		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
--		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
--	FROM sys.columns 
--)
--MERGE INTO [dESx].[DealValuationDocument] AS trgt
--USING	(
--		SELECT 
--			-1 * RowNumber,
--			-1 * RowNumber,
--			-1 * RowNumber,
--			'SEEDDATA'
--		FROM Data
--		) AS src([DealValuationDocument_Key],[DealValuation_Key],[Document_Key],[CreatedBy])
--ON
--	trgt.[DealValuationDocument_Key] = src.[DealValuationDocument_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[DealValuation_Key] = src.[DealValuation_Key]
--		, [Document_Key] = src.[Document_Key]
--		, [CreatedBy] = src.[CreatedBy]
--		, [UpdatedDate] = '2006-01-01'
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([DealValuationDocument_Key],[DealValuation_Key],[Document_Key],[CreatedBy], [UpdatedDate])
--	VALUES ([DealValuationDocument_Key],[DealValuation_Key],[Document_Key],[CreatedBy], '2006-01-01');

--SET IDENTITY_INSERT [dESx].[DealValuationDocument] OFF;

--PRINT N'Seeding [DealValuationDocument] complete.'


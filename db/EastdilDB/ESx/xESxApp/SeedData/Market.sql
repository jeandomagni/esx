PRINT N'Seeding [Market].'

SET IDENTITY_INSERT [dESx].[Market] ON;

DECLARE @MarketData Table(
	[Market_Key] int,
	[MarketName] NVARCHAR (100),
	[MarketType_Code] NVARCHAR (25),
	[MarketBoundaries] [sys].[geography],
	[CreatedBy]        VARCHAR (20) 
)
INSERT INTO @MarketData([Market_Key],[MarketName],[MarketType_Code],[MarketBoundaries],[CreatedBy])
VALUES ( -1, 'West', 'Region',	 geography::STGeomFromText('POLYGON((-125 50, -125 24, -90 24, -90 50, -125 50))', 4326), 'SEEDDATA'),
	   ( -2, 'East', 'Region',	 geography::STGeomFromText('POLYGON((-90 24, -65 24, -65 50, -90 50, -90 24))', 4326), 'SEEDDATA')

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
INSERT INTO @MarketData([Market_Key],[MarketName],[MarketType_Code],[MarketBoundaries],[CreatedBy])
SELECT 
	-1 * RowNumber,
	'Market ' + RowLabel,
	'State',
	NULL,
	'SEEDDATA'
FROM Data
WHERE RowNumber > 2


MERGE INTO [dESx].[Market] AS trgt
USING @MarketData AS src
ON
	trgt.[Market_Key] = src.[Market_Key]
WHEN MATCHED THEN
	UPDATE SET
		[MarketName] = src.[MarketName]
		, [MarketType_Code] = src.[MarketType_Code]
		, [MarketBoundaries] = src.[MarketBoundaries]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Market_Key],[MarketName],[MarketType_Code],[MarketBoundaries],[CreatedBy], [UpdatedDate])
	VALUES ([Market_Key],[MarketName],[MarketType_Code],[MarketBoundaries],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[Market] OFF;

PRINT N'Seeding [Market] complete.'


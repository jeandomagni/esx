﻿PRINT N'Seeding DealValuationAssetUsageValue.'

SET IDENTITY_INSERT [dESx].[DealValuationAssetUsageValue] ON;
;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel,
		ABS(CHECKSUM(name)) AS Random
	FROM sys.columns 
)
MERGE INTO [dESx].[DealValuationAssetUsageValue] AS trgt
USING	(
		SELECT
			[DealValuationAssetUsageValue_Key] = -1 * RowNumber, 
			[DealValuation_Key] = -1 * RowNumber, 
			[DealAssetUsageType_Key] = (Random % 7) + 2,
			[Spaces] = Random % 1000,
			[SpaceType] = CASE Random % 7
						  WHEN 0 THEN 'SF'
						  WHEN 1 THEN 'SF'
						  WHEN 2 THEN 'Keys'
						  WHEN 3 THEN 'Units'
						  WHEN 4 THEN 'SF'
						  WHEN 5 THEN 'Acres'
						  WHEN 6 THEN 'Spaces'
						  END,
			[Value] = Random % 1000000,
			'SEEDDATA'
		FROM Data
		) AS src([DealValuationAssetUsageValue_Key],[DealValuation_Key],[DealAssetUsageType_Key],[Spaces],[SpaceType],[Value],[CreatedBy])
ON
	trgt.[DealValuationAssetUsageValue_Key] = src.[DealValuationAssetUsageValue_Key]
WHEN MATCHED THEN
	UPDATE SET
		[DealValuation_Key] = src.[DealValuation_Key]
		, [DealAssetUsageType_Key] = src.[DealAssetUsageType_Key]
		, [Spaces]	  = src.[Spaces]	 
		, [SpaceType] = src.[SpaceType]
		, [Value]	  = src.[Value]	 
		, [CreatedBy] = src.[CreatedBy]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealValuationAsset_Key],[DealValuation_Key],[DealAssetUsageType_Key],[Spaces],[SpaceType],[Value],[CreatedBy])
	VALUES ([DealValuationAsset_Key],[DealValuation_Key],[DealAssetUsageType_Key],[Spaces],[SpaceType],[Value],[CreatedBy])
;
SET IDENTITY_INSERT [dESx].[DealValuationAsset] OFF;

PRINT N'Seeding of DealValuationAssetUsageValue complete.'

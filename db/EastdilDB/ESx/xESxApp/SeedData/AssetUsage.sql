PRINT N'Seeding [AssetUsage].'

SET IDENTITY_INSERT [dESx].[AssetUsage] ON;

;WITH Data AS (
	SELECT TOP 250
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[AssetUsage] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber % 200 -1,
			CASE WHEN RowNumber < 200 THEN RowNumber % 5 +2 ELSE (RowNumber+1) % 5 +2 END, -- assets 1-50 get two usage types
			NULL,
			CASE WHEN RowNumber < 200 THEN 1 ELSE 0 END,
			'SEEDDATA'
		FROM Data
		) AS src([AssetUsage_Key],[Asset_Key],[AssetUsageType_Key],[EndDate],[IsPrimary],[CreatedBy])
ON
	trgt.[AssetUsage_Key] = src.[AssetUsage_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Asset_Key] = src.[Asset_Key]
		, [AssetUsageType_Key] = src.[AssetUsageType_Key]
		, [EndDate] = src.[EndDate]
		, [IsPrimary] = src.[IsPrimary]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AssetUsage_Key],[Asset_Key],[AssetUsageType_Key],[EndDate],[IsPrimary],[CreatedBy], [UpdatedDate])
	VALUES ([AssetUsage_Key],[Asset_Key],[AssetUsageType_Key],[EndDate],[IsPrimary],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[AssetUsage] OFF;

PRINT N'Seeding [AssetUsage] complete.'
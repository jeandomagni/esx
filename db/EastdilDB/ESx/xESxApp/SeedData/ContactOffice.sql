PRINT N'Seeding [ContactOffice].'

SET IDENTITY_INSERT [dESx].[ContactOffice] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[ContactOffice] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			-1 * RowNumber,
			(RowNumber % 6) + 1,
			NULL,
			'SEEDDATA'
		FROM Data
		) AS src([ContactOffice_Key],[Contact_Key],[Office_Key],[ContactOfficeRole_Code],[EndDate],[CreatedBy])
ON
	trgt.[ContactOffice_Key] = src.[ContactOffice_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Contact_Key] = src.[Contact_Key]
		, [Office_Key] = src.[Office_Key]
		, [ContactOfficeRole_Code] = src.[ContactOfficeRole_Code]
		, [EndDate] = src.[EndDate]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([ContactOffice_Key],[Contact_Key],[Office_Key],[ContactOfficeRole_Code],[EndDate],[CreatedBy], [UpdatedDate])
	VALUES ([ContactOffice_Key],[Contact_Key],[Office_Key],[ContactOfficeRole_Code],[EndDate],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[ContactOffice] OFF;

PRINT N'Seeding [ContactOffice] complete.'


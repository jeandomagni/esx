﻿PRINT N'Seeding [MarketingListContact].'

SET IDENTITY_INSERT [dESx].[MarketingListContact] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[MarketingListContact] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber  % 20 - 1,
			-1 * RowNumber  ,
			'SEEDDATA',
			1
		FROM Data
		) AS src([MarketingListContact_Key],[MarketingList_Key],[Contact_Key],[CreatedBy], [IsSolicitable])
ON
	trgt.[MarketingListContact_Key] = src.[MarketingListContact_Key]
WHEN MATCHED THEN
	UPDATE SET
        [MarketingList_Key] = src.[MarketingList_Key]
        , [Contact_Key] = src.[Contact_Key]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
        ,[IsSolicitable] = src.[IsSolicitable]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([MarketingListContact_Key],[MarketingList_Key],[Contact_Key],[CreatedBy], [UpdatedDate],[IsSolicitable])
	VALUES ([MarketingListContact_Key],[MarketingList_Key],[Contact_Key],[CreatedBy], '2006-01-01',[IsSolicitable])
;

SET IDENTITY_INSERT [dESx].[MarketingListContact] OFF;

PRINT N'Seeding [MarketingListContact] complete.'


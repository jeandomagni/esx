--PRINT N'Seeding [Notice_Mention].'

--SET IDENTITY_INSERT [dESx].[Notice_Mention] ON;

--MERGE INTO [dESx].[Notice_Mention] AS trgt
--USING	(VALUES
--		(add values here)
--		) AS src([Notice_Mention_Key],[Notice_Key],[Mention_Key])
--ON
--	trgt.[Notice_Mention_Key] = src.[Notice_Mention_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[Notice_Key] = src.[Notice_Key]
--		, [Mention_Key] = src.[Mention_Key]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([Notice_Mention_Key],[Notice_Key],[Mention_Key])
--	VALUES ([Notice_Mention_Key],[Notice_Key],[Mention_Key])

--;
--SET IDENTITY_INSERT [dESx].[Notice_Mention] OFF;

--PRINT N'Seeding [Notice_Mention] complete.'


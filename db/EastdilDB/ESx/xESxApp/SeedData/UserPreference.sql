--PRINT N'Seeding [UserPreference].'

--SET IDENTITY_INSERT [dESx].[UserPreference] ON;

--MERGE INTO [dESx].[UserPreference] AS trgt
--USING	(VALUES
--		(add values here)
--		) AS src([UserPreference_Key],[User_Key],[PreferenceType_Code],[PreferenceValue])
--ON
--	trgt.[UserPreference_Key] = src.[UserPreference_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[User_Key] = src.[User_Key]
--		, [PreferenceType_Code] = src.[PreferenceType_Code]
--		, [PreferenceValue] = src.[PreferenceValue]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([UserPreference_Key],[User_Key],[PreferenceType_Code],[PreferenceValue])
--	VALUES ([UserPreference_Key],[User_Key],[PreferenceType_Code],[PreferenceValue])

--;
--PRINT N'Seeding [UserPreference] complete.'

--SET IDENTITY_INSERT [dESx].[UserPreference] OFF;

--PRINT N'Seeding [UserPreference] complete.'


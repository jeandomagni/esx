PRINT N'Seeding [CompanyAlias].'

SET IDENTITY_INSERT [dESx].[CompanyAlias] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[CompanyAlias] AS trgt
USING	(
			SELECT 
				-1 * RowNumber,
				-1 * RowNumber,
				'Alias ' + RowLabel,
				1,
				1,
				0,
				'SEEDDATA'
			FROM Data
		) AS src([CompanyAlias_Key],[Company_Key],[AliasName],[IsLegal],[IsPrimary],[IsDefunct],[CreatedBy])
ON
	trgt.[CompanyAlias_Key] = src.[CompanyAlias_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Company_Key] = src.[Company_Key]
		, [AliasName] = src.[AliasName]
		, [IsLegal] = src.[IsLegal]
		, [IsPrimary] = src.[IsPrimary]
		, [IsDefunct] = src.[IsDefunct]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([CompanyAlias_Key],[Company_Key],[AliasName],[IsLegal],[IsPrimary],[IsDefunct],[CreatedBy], [UpdatedDate])
	VALUES ([CompanyAlias_Key],[Company_Key],[AliasName],[IsLegal],[IsPrimary],[IsDefunct],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[CompanyAlias] OFF;

PRINT N'Seeding [CompanyAlias] complete.'


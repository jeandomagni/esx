
PRINT N'Seeding [ContactMarket].'

SET IDENTITY_INSERT [dESx].[ContactMarket] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[ContactMarket] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			-1 * RowNumber,
			NULL,
			'SEEDDATA'
		FROM Data
		) AS src([ContactMarket_Key],[Contact_Key],[Market_Key],[EndDate],[CreatedBy])
ON
	trgt.[ContactMarket_Key] = src.[ContactMarket_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Contact_Key] = src.[Contact_Key]
		, [Market_Key] = src.[Market_Key]
		, [EndDate] = src.[EndDate]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([ContactMarket_Key],[Contact_Key],[Market_Key],[EndDate],[CreatedBy], [UpdatedDate])
	VALUES ([ContactMarket_Key],[Contact_Key],[Market_Key],[EndDate],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[ContactMarket] OFF;

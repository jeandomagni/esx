SET IDENTITY_INSERT [dESx].[AssetLoanProperty] ON;

;WITH Data AS (
	SELECT TOP 200
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)

MERGE INTO [dESx].[AssetLoanProperty] AS trgt
USING	(SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			-1 * RowNumber -200
		FROM Data
		) AS src([AssetLoanProperty_Key],[PropertyAsset_Key],[LoanAsset_Key])
ON
	trgt.[AssetLoanProperty_Key] = src.[AssetLoanProperty_Key]
WHEN MATCHED THEN
	UPDATE SET
		 [PropertyAsset_Key] = src.[PropertyAsset_Key],
		[LoanAsset_Key] = src.[LoanAsset_Key]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AssetLoanProperty_Key],[PropertyAsset_Key],[LoanAsset_Key])
	VALUES ([AssetLoanProperty_Key],[PropertyAsset_Key],[LoanAsset_Key])

;
SET IDENTITY_INSERT [dESx].[AssetLoanProperty] OFF;

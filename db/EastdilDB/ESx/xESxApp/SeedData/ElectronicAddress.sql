﻿PRINT N'Seeding ElectronicAddress.'

SET IDENTITY_INSERT [dESx].[ElectronicAddress] ON;
;WITH Data AS (
	SELECT TOP 1000 
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber
	FROM sys.columns 
)
MERGE INTO [dESx].[ElectronicAddress] AS trgt
USING	(
		SELECT
			-1 * RowNumber AS ElectronicAddress_Key, 
			CASE WHEN RowNUmber <= 100 THEN 'EmailAddress' ELSE 'PhoneNumber' END AS ElectronicAddressType_Code, 
			CASE WHEN RowNumber > 100 and RowNumber < 500 THEN 'Mobile' ELSE 'Main' END AS AddressUsageType_Code,
			NULL AS Description, CASE WHEN RowNumber <= 100 THEN Convert(varchar(4), RowNumber) + 'tom@gmail.com' ELSE Convert(varchar(10), 6122300000 + RowNumber) END AS Address, NULL AS LegacyTable, NULL AS LegacyRowID
		FROM Data
		) AS src([ElectronicAddress_Key],[ElectronicAddressType_Code],[AddressUsageType_Code],[Description],[Address],[LegacyTable],[LegacyRowID])
ON
	trgt.[ElectronicAddress_Key] = src.[ElectronicAddress_Key]
WHEN MATCHED THEN
	UPDATE SET
		[ElectronicAddressType_Code] = src.[ElectronicAddressType_Code]
		, [AddressUsageType_Code] = src.[AddressUsageType_Code]
		, [Description] = src.[Description]
		, [Address] = src.[Address]
		, [CreatedBy] = 'SEEDDATA'
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([ElectronicAddress_Key],[ElectronicAddressType_Code],[AddressUsageType_Code],[Description],[Address],[CreatedBy], [UpdatedDate])
	VALUES ([ElectronicAddress_Key],[ElectronicAddressType_Code],[AddressUsageType_Code],[Description],[Address],'SEEDDATA', '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[ElectronicAddress] OFF;

PRINT N'Seeding of ElectronicAddress complete.'

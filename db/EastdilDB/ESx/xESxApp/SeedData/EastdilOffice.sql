
PRINT N'Seeding [EastdilOffice].'

SET IDENTITY_INSERT [dESx].[EastdilOffice] ON;

;WITH Data AS (
	SELECT TOP 25
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[EastdilOffice] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			'Eastdil Office ' + RowLabel,
			(-1 * RowNumber) - 400,
			'SEEDDATA'
		FROM Data
		) AS src([EastdilOffice_Key],[EastdilOfficeName],[PhysicalAddress_Key],[CreatedBy])
ON
	trgt.[EastdilOffice_Key] = src.[EastdilOffice_Key]
WHEN MATCHED THEN
	UPDATE SET
		[EastdilOfficeName] = src.[EastdilOfficeName]
		, [PhysicalAddress_Key] = src.[PhysicalAddress_Key]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([EastdilOffice_Key],[EastdilOfficeName],[PhysicalAddress_Key],[CreatedBy], [UpdatedDate])
	VALUES ([EastdilOffice_Key],[EastdilOfficeName],[PhysicalAddress_Key],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[EastdilOffice] OFF;

PRINT N'Seeding [EastdilOffice] complete.'


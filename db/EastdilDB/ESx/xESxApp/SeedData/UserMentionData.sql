PRINT N'Seeding [UserMentionData].'

SET IDENTITY_INSERT [dESx].[UserMentionData] ON;

;WITH Data AS (
	SELECT TOP 683 -- total number of seeded items
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[UserMentionData] AS trgt
USING	(SELECT
			 [UserMentionData_Key] = -1 *  RowNumber
			,[User_Key] = 1
			,[Mention_Key] = RowNumber
			,[IsWatched] = CASE WHEN RowNumber % 20 = 0 THEN 1 ELSE 0 END
			,[UseCount] = 0
			,[LastUseDate] = getutcdate()
		 FROM Data
		) AS src([UserMentionData_Key],[User_Key],[Mention_Key],[IsWatched],[UseCount],[LastUseDate])
ON
	trgt.[UserMentionData_Key] = src.[UserMentionData_Key]
WHEN MATCHED THEN
	UPDATE SET
		[User_Key] = src.[User_Key]
		, [Mention_Key] = src.[Mention_Key]
		, [IsWatched] = src.[IsWatched]
		, [UseCount] = src.[UseCount]
		, [LastUseDate] = src.[LastUseDate]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([UserMentionData_Key],[User_Key],[Mention_Key],[IsWatched],[UseCount],[LastUseDate])
	VALUES ([UserMentionData_Key],[User_Key],[Mention_Key],[IsWatched],[UseCount],[LastUseDate])

;
SET IDENTITY_INSERT [dESx].[UserMentionData] OFF;

PRINT N'Seeding [UserMentionData] complete.'


﻿PRINT N'Seeding DealContact.'

SET IDENTITY_INSERT [dESx].[DealContact] ON;
;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[DealContact] AS trgt
USING	(
		SELECT
			-1 * RowNumber AS DealContactKey_Key, 
			-1 * RowNumber AS DealKey_Key, 
			-1 * RowNumber AS ContactKey_Key,
			'Acquisitions' AS [DealContactType_Code],
			'SEEDDATA'
		FROM Data
		) AS src([DealContact_Key],[Deal_Key],[Contact_Key],[DealContactType_Code],[CreatedBy])
ON
	trgt.[DealContact_Key] = src.[DealContact_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Deal_Key] = src.[Deal_Key]
		, [Contact_Key] = src.[Contact_Key]
		, [DealContactType_Code] = src.[DealContactType_Code]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealContact_Key],[Deal_Key],[Contact_Key],[DealContactType_Code],[CreatedBy], [UpdatedDate])
	VALUES ([DealContact_Key],[Deal_Key],[Contact_Key],[DealContactType_Code],[CreatedBy], '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[DealContact] OFF;

PRINT N'Seeding of DealContact complete.'

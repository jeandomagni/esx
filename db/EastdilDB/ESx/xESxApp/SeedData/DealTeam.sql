﻿PRINT N'Seeding DealTeam.'

SET IDENTITY_INSERT [dESx].[DealTeam] ON;
;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[DealTeam] AS trgt
USING	(
		SELECT
			-1 * RowNumber AS DealTeam_Key, 
			-1 * RowNumber AS Deal_Key, 
			-1 * RowNumber AS Contact_Key,
			'PRIMARY' AS DealTeamRole_Code,
			'SEEDDATA'
		FROM Data
		) AS src([DealTeam_Key],[Deal_Key],[Contact_Key],[DealTeamRole_Code],[CreatedBy])
ON
	trgt.[DealTeam_Key] = src.[DealTeam_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Deal_Key] = src.[Deal_Key]
		, [Contact_Key] = src.[Contact_Key]
		, [DealTeamRole_Code] = src.[DealTeamRole_Code]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealTeam_Key],[Deal_Key],[Contact_Key],[DealTeamRole_Code],[CreatedBy], [UpdatedDate])
	VALUES ([DealTeam_Key],[Deal_Key],[Contact_Key],[DealTeamRole_Code],[CreatedBy], '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[DealTeam] OFF;

PRINT N'Seeding of DealTeam complete.'

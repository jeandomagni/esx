﻿PRINT N'Seeding Deal.'

SET IDENTITY_INSERT [dESx].[Deal] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel,
		ABS(CHECKSUM(name)) AS Random
	FROM sys.columns 
)
MERGE INTO [dESx].[Deal] AS trgt
USING	(
			SELECT 
				-1 * RowNumber,
				CASE WHEN RowNumber BETWEEN 50 AND 100 THEN 'Test Pitch #' + RowLabel ELSE 'Test Deal #' + RowLabel END,
				'TESTCDE' + RowLabel,
				[DealType_Code] =	CASE Random % 6
									WHEN 0 THEN 'Equity Sale 100%'
									WHEN 1 THEN 'Equity Sale <100%'
									WHEN 2 THEN 'Debt Placement'
									WHEN 3 THEN 'Loan Sale'
									WHEN 4 THEN 'Investment Banking'
									ELSE 'Non-transactional Advisory' END,
				'LGL',
				[CreatedDate] = DateAdd(d, RowNumber, getutcdate()),
				'SEEDDATA',
				[UpdatedDate] = DateAdd(d, RowNumber, getutcdate()),
				'SEEDDATA',
				[Office_Key] = -1 * RowNumber,
				[LaunchDate] = DateAdd(d, RowNumber, getutcdate()),
				[DealFee] = RowNumber * 1000,
				[FinalTransactionAmount] = RowNumber * 10000
			FROM Data
		) AS src([Deal_Key],[DealName],[DealCode],[DealType_Code],[LegalEntity_Code],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy],[Office_Key],[LaunchDate],[DealFee],[FinalTransactionAmount])
ON
	trgt.[Deal_Key] = src.[Deal_Key]
WHEN MATCHED THEN
	UPDATE SET
		[DealName] = src.[DealName]
		, [DealCode] = src.[DealCode]
		, [DealType_Code] = src.[DealType_Code]
		, [LegalEntity_Code] = src.[LegalEntity_Code]
		, [CreatedDate]	= src.[CreatedDate]
		, [CreatedBy]	= src.[CreatedBy]
		, [UpdatedDate]	= src.[UpdatedDate]
		, [UpdatedBy]	= src.[UpdatedBy]
		, [Office_Key] = src.[Office_Key]
		, [LaunchDate] = src.[LaunchDate]
		, [DealFee] = src.[DealFee]
		, [FinalTransactionAmount] = src.[FinalTransactionAmount]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Deal_Key],[DealName],[DealCode],[DealType_Code],[LegalEntity_Code],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy],[Office_Key],[LaunchDate],[DealFee],[FinalTransactionAmount])
	VALUES ([Deal_Key],[DealName],[DealCode],[DealType_Code],[LegalEntity_Code],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy],[Office_Key],[LaunchDate],[DealFee],[FinalTransactionAmount])

;
SET IDENTITY_INSERT [dESx].[Deal] OFF;

PRINT N'Seeding of Deal complete.'

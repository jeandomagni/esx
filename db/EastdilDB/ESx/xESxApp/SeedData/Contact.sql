﻿PRINT N'Seeding Contact.'

SET IDENTITY_INSERT [dESx].[Contact] ON;

MERGE INTO [dESx].[Contact] AS trgt
USING	(VALUES
(-1,'Jason Paul Manuel','Paul Manuel','Jason',NULL,'Paul Manuel, Jason',NULL,NULL,NULL,0,NULL,NULL,1),
(-2,'Tan Check Yin','Yin','Tan Check',NULL,'Yin, Tan Check',NULL,NULL,'Investment Manager',0,NULL,NULL,1),
(-3,'Teresa Wuest','Wuest','Teresa',NULL,'Wuest, Teresa',NULL,NULL,'Associate Broker',0,NULL,NULL,1),
(-4,'Gary Brown','Brown','Gary',NULL,'Brown, Gary',NULL,NULL,'President',0,NULL,NULL,1),
(-5,'Egzon Thaqi','Thaqi','Egzon',NULL,'Thaqi, Egzon',NULL,NULL,NULL,0,NULL,NULL,1),
(-6,'Kevin Davis','Davis','Kevin',NULL,'Davis, Kevin',NULL,NULL,NULL,0,NULL,NULL,1),
(-7,'Nathan Zinn','Zinn','Nathan',NULL,'Zinn, Nathan',NULL,NULL,NULL,0,NULL,NULL,1),
(-8,'Jonas Woods','Woods','Jonas',NULL,'Woods, Jonas',NULL,NULL,'CEO',0,NULL,NULL,1),
(-9,'Sarah Gater','Gater','Sarah',NULL,'Gater, Sarah',NULL,NULL,NULL,0,NULL,NULL,1),
(-10,'Jackson  Lapin','Lapin','Jackson ',NULL,'Lapin, Jackson ',NULL,NULL,'Director | Asset Management',0,NULL,NULL,1),
(-11,'Patrick  Lawler','Lawler','Patrick ',NULL,'Lawler, Patrick ',NULL,NULL,'Senior Investment Analyst',0,NULL,NULL,1),
(-12,'Luke Colbert','Colbert','Luke',NULL,'Colbert, Luke',NULL,NULL,NULL,0,NULL,NULL,1),
(-13,'Cyrus Rafael','Rafael','Cyrus',NULL,'Rafael, Cyrus',NULL,NULL,NULL,0,NULL,NULL,1),
(-14,'Cathy Dempsey','Dempsey','Cathy',NULL,'Dempsey, Cathy',NULL,NULL,'Director – Development',0,NULL,NULL,1),
(-15,'Brooks Castelew','Castelew','Brooks',NULL,'Castelew, Brooks',NULL,NULL,NULL,0,NULL,NULL,1),
(-16,'Byron Cocke','Cocke','Byron',NULL,'Cocke, Byron',NULL,NULL,NULL,0,NULL,NULL,1),
(-17,'Matt Fain','Fain','Matt',NULL,'Fain, Matt',NULL,NULL,NULL,0,NULL,NULL,1),
(-18,'Joe Hollmeyer','Hollmeyer','Joe',NULL,'Hollmeyer, Joe',NULL,NULL,NULL,0,NULL,NULL,1),
(-19,'Hollmeyer Joe','Joe','Hollmeyer',NULL,'Joe, Hollmeyer',NULL,NULL,NULL,0,NULL,NULL,1),
(-20,'Gary Feldman','Feldman','Gary',NULL,'Feldman, Gary',NULL,NULL,'CEO',0,NULL,NULL,1),
(-21,'Dianne  Magsari','Magsari','Dianne ',NULL,'Magsari, Dianne ',NULL,NULL,'CEO ',0,NULL,NULL,1),
(-22,'Zil Huma','Huma','Zil',NULL,'Huma, Zil',NULL,NULL,NULL,0,NULL,NULL,1),
(-23,'Mark Wan','Wan','Mark',NULL,'Wan, Mark',NULL,NULL,NULL,0,NULL,NULL,1),
(-24,'Axel Brinkmann','Brinkmann','Axel',NULL,'Brinkmann, Axel',NULL,NULL,'Managing Director',0,NULL,NULL,1),
(-25,'Tim Wilkinson ','Wilkinson ','Tim',NULL,'Wilkinson , Tim',NULL,NULL,'Head of Products ',0,NULL,NULL,1),
(-26,'David Hess','Hess','David',NULL,'Hess, David',NULL,NULL,NULL,0,NULL,NULL,1),
(-27,'Benjamin Tompkins','Tompkins','Benjamin',NULL,'Tompkins, Benjamin',NULL,NULL,NULL,0,NULL,NULL,1),
(-28,'Lauren Bosso','Bosso','Lauren',NULL,'Bosso, Lauren',NULL,NULL,NULL,0,NULL,NULL,1),
(-29,'Hector Itturralde','Itturralde','Hector',NULL,'Itturralde, Hector',NULL,NULL,NULL,0,NULL,NULL,1),
(-30,'Jonathan Pollack','Pollack','Jonathan',NULL,'Pollack, Jonathan',NULL,NULL,NULL,0,NULL,NULL,1),
(-31,'John Miron','Miron','John',NULL,'Miron, John',NULL,NULL,NULL,0,NULL,NULL,1),
(-32,'Angelina Taylor','Taylor','Angelina',NULL,'Taylor, Angelina',NULL,NULL,'Administrative Assistant',0,NULL,NULL,1),
(-33,'Mark Stoneburgh','Stoneburgh','Mark',NULL,'Stoneburgh, Mark',NULL,NULL,NULL,0,NULL,NULL,1),
(-34,'Rob Garner','Garner','Rob','W.','Garner, Rob',NULL,NULL,NULL,0,NULL,NULL,1),
(-35,'Gayathri Gunasekaran','Gunasekaran','Gayathri',NULL,'Gunasekaran, Gayathri',NULL,NULL,NULL,0,NULL,NULL,1),
(-36,'Nicolas Massiere','Massiere','Nicolas',NULL,'Massiere, Nicolas',NULL,NULL,'Principal',0,NULL,NULL,1),
(-37,'Ariel Nakash','Nakash','Ariel',NULL,'Nakash, Ariel',NULL,NULL,NULL,0,NULL,NULL,1),
(-38,'Scott Meyer','Meyer','Scott',NULL,'Meyer, Scott',NULL,NULL,NULL,0,NULL,NULL,1),
(-39,'Alicia Balwah','Balwah','Alicia',NULL,'Balwah, Alicia',NULL,NULL,NULL,0,NULL,NULL,1),
(-40,'David Mannion','Mannion','David',NULL,'Mannion, David',NULL,NULL,NULL,0,NULL,NULL,1),
(-41,'Tess Shepard','Shepard','Tess',NULL,'Shepard, Tess',NULL,NULL,NULL,0,NULL,NULL,1),
(-42,'Peter Quinn','Quinn','Peter',NULL,'Quinn, Peter',NULL,NULL,NULL,0,NULL,NULL,1),
(-43,'Nadia Podkopova','Podkopova','Nadia',NULL,'Podkopova, Nadia',NULL,NULL,NULL,0,NULL,NULL,1),
(-44,'Cecilia Chuang','Chuang','Cecilia',NULL,'Chuang, Cecilia',NULL,NULL,'AVP-Portfolio Officer',0,NULL,NULL,1),
(-45,'Elle Patterson','Patterson','Elle',NULL,'Patterson, Elle',NULL,NULL,'Senior Consultant',0,NULL,NULL,1),
(-46,'Daniel Kane','Kane','Daniel',NULL,'Kane, Daniel',NULL,NULL,NULL,0,NULL,NULL,1),
(-47,'Tim Johnson','Johnson','Tim',NULL,'Johnson, Tim',NULL,NULL,NULL,0,NULL,NULL,1),
(-48,'K. Taylor White','White','K. Taylor',NULL,'White, K. Taylor',NULL,NULL,NULL,0,NULL,NULL,1),
(-49,'Jon Walton','Walton','Jon',NULL,'Walton, Jon',NULL,NULL,'Managing Director',0,NULL,NULL,1),
(-50,'Dan Doty','Doty','Dan',NULL,'Doty, Dan',NULL,NULL,'Managing Director',0,NULL,NULL,1),
(-51,'Michael Pedulla','Pedulla','Michael',NULL,'Pedulla, Michael',NULL,NULL,NULL,0,NULL,NULL,1),
(-52,'Bob Ray','Ray','Bob',NULL,'Ray, Bob',NULL,NULL,'President',0,NULL,NULL,1),
(-53,'Erik Storz','Storz','Erik',NULL,'Storz, Erik',NULL,NULL,'Sr. Mortgage Banker',0,NULL,NULL,1),
(-54,'Justin  Nguyen','Nguyen','Justin ',NULL,'Nguyen, Justin ',NULL,NULL,NULL,0,NULL,NULL,1),
(-55,'Dale Ten','Ten','Dale',NULL,'Ten, Dale',NULL,NULL,NULL,0,NULL,NULL,1),
(-56,'Stewart Deluca','Deluca','Stewart',NULL,'Deluca, Stewart',NULL,NULL,NULL,0,NULL,NULL,1),
(-57,'Eric Aronsophn','Aronsophn','Eric',NULL,'Aronsophn, Eric',NULL,NULL,NULL,0,NULL,NULL,1),
(-58,'Alex Lachman','Lachman','Alex',NULL,'Lachman, Alex',NULL,NULL,NULL,0,NULL,NULL,1),
(-59,'Mike Fantl','Fantl','Mike',NULL,'Fantl, Mike',NULL,NULL,'Retail Asset Manager',0,NULL,NULL,1),
(-60,'Stephen Kennelly','Kennelly','Stephen',NULL,'Kennelly, Stephen',NULL,NULL,NULL,0,NULL,NULL,1),
(-61,'William Weeks','Weeks','William',NULL,'Weeks, William',NULL,NULL,NULL,0,NULL,NULL,1),
(-62,'Scott Eschelman','Eschelman','Scott',NULL,'Eschelman, Scott',NULL,NULL,NULL,0,NULL,NULL,1),
(-63,'Tyler Kepler','Kepler','Tyler',NULL,'Kepler, Tyler',NULL,NULL,NULL,0,NULL,NULL,1),
(-64,'Brett Oliver','Oliver','Brett',NULL,'Oliver, Brett',NULL,NULL,NULL,0,NULL,NULL,1),
(-65,'Sylvie Mizrahi','Mizrahi','Sylvie',NULL,'Mizrahi, Sylvie',NULL,NULL,NULL,0,NULL,NULL,1),
(-66,'Jeff Saul','Saul','Jeff',NULL,'Saul, Jeff',NULL,NULL,NULL,0,NULL,NULL,1),
(-67,'Gregory Cohen','Cohen','Gregory',NULL,'Cohen, Gregory',NULL,NULL,NULL,0,NULL,NULL,1),
(-68,'Simon Betty','Betty','Simon',NULL,'Betty, Simon',NULL,NULL,'Director of Business Management',0,NULL,NULL,1),
(-69,'David Tombors','Tombors','David','M.','Tombors, David',NULL,NULL,'Acquisitions Associate ',0,NULL,NULL,1),
(-70,'Elizabeth Bright','Bright','Elizabeth',NULL,'Bright, Elizabeth',NULL,NULL,NULL,0,NULL,NULL,1),
(-71,'Rogerio Souza','Souza','Rogerio',NULL,'Souza, Rogerio',NULL,NULL,'Senior Vice President',0,NULL,NULL,1),
(-72,'Tadeu Avallone','Avallone','Tadeu',NULL,'Avallone, Tadeu',NULL,NULL,NULL,0,NULL,NULL,1),
(-73,'Stephen Kallaher','Kallaher','Stephen',NULL,'Kallaher, Stephen',NULL,NULL,'Vice PResiden, Corporate Real Estate Development',0,NULL,NULL,1),
(-74,'Joseph Hamam','Hamam','Joseph',NULL,'Hamam, Joseph',NULL,NULL,NULL,0,NULL,NULL,1),
(-75,'Dane Neilson','Neilson','Dane','C.','Neilson, Dane',NULL,NULL,'Senior Director, Multifamily Asset Manamgement',0,NULL,NULL,1),
(-76,'Amanda Johnson','Johnson','Amanda',NULL,'Johnson, Amanda',NULL,NULL,'Outreach Representative',0,NULL,NULL,1),
(-77,'Joshua  Zierten','Zierten','Joshua ',NULL,'Zierten, Joshua ',NULL,NULL,NULL,0,NULL,NULL,1),
(-78,'Justin C.  Anstey','Anstey','Justin C. ',NULL,'Anstey, Justin C. ',NULL,NULL,'Financial Analyst',0,NULL,NULL,1),
(-79,'Brett Finkelstein','Finkelstein','Brett','L','Finkelstein, Brett',NULL,NULL,'Co-CEO',0,NULL,NULL,1),
(-80,'Woochul Jung','Jung','Woochul',NULL,'Jung, Woochul',NULL,NULL,'Director-Structured Products Team',0,NULL,NULL,1),
(-81,'Sean Mckinley','Mckinley','Sean',NULL,'Mckinley, Sean',NULL,NULL,NULL,0,NULL,NULL,1),
(-82,'Andrew Owen','Owen','Andrew',NULL,'Owen, Andrew',NULL,NULL,'Senior Manager - Strategy',0,NULL,NULL,1),
(-83,'Allison Helminski','Helminski','Allison',NULL,'Helminski, Allison',NULL,NULL,'VP, Development',0,NULL,NULL,1),
(-84,'Peter Edelmann','Edelmann','Peter',NULL,'Edelmann, Peter',NULL,NULL,NULL,0,NULL,NULL,1),
(-85,'Robert Brvenik','Brvenik','Robert',NULL,'Brvenik, Robert',NULL,NULL,'Principal',0,NULL,NULL,1),
(-86,'Laurence Oster','Oster','Laurence',NULL,'Oster, Laurence',NULL,NULL,'Partner',0,NULL,NULL,1),
(-87,'Christopher Stephen','Stephen','Christopher',NULL,'Stephen, Christopher',NULL,NULL,'Senior Global Property Consultant',0,NULL,NULL,1),
(-88,'Ross Netherway','Netherway','Ross',NULL,'Netherway, Ross',NULL,NULL,NULL,0,NULL,NULL,1),
(-89,'Stefano Mazzoni','Mazzoni','Stefano',NULL,'Mazzoni, Stefano',NULL,NULL,'Portfolio Manager',0,NULL,NULL,1),
(-90,'Jonathan Harley','Harley','Jonathan',NULL,'Harley, Jonathan',NULL,NULL,'Associate - Asset Management',0,NULL,NULL,1),
(-91,'Mark Geraghty','Geraghty','Mark',NULL,'Geraghty, Mark',NULL,NULL,'Analyst, Corporate Finance',0,NULL,NULL,1),
(-92,'Tyler Darden','Darden','Tyler',NULL,'Darden, Tyler',NULL,NULL,NULL,0,NULL,NULL,1),
(-93,'Kellan Florio','Florio','Kellan',NULL,'Florio, Kellan',NULL,NULL,NULL,0,NULL,NULL,1),
(-94,'Tyler Rindler','Rindler','Tyler',NULL,'Rindler, Tyler',NULL,NULL,NULL,0,NULL,NULL,1),
(-95,'Terry Wilson','Wilson','Terry',NULL,'Wilson, Terry',NULL,NULL,'Director',0,NULL,NULL,1),
(-96,'Wonseok Lim','Lim','Wonseok',NULL,'Lim, Wonseok',NULL,NULL,'General Manager',0,NULL,NULL,1),
(-97,'Dong Jin Jung','Jung','Dong Jin',NULL,'Jung, Dong Jin',NULL,NULL,NULL,0,NULL,NULL,1),
(-98,'Chris Vanheerden','Vanheerden','Chris',NULL,'Vanheerden, Chris',NULL,NULL,NULL,0,NULL,NULL,1),
(-99,'Brandon Winter','Winter','Brandon',NULL,'Winter, Brandon',NULL,NULL,'Associate',0,NULL,NULL,1),
(-100,'David Walter','Walter','David',NULL,'Walter, David',NULL,NULL,'Associate',0,NULL,NULL,1),
(-101,'Jean Domagni','Jean','K',NULL,'Domagni, Jean',NULL,NULL,'Developer',0,NULL,NULL,1),
(-102,'Jon Raymer','Raymer','Jon',NULL,'Raymer, Jon',NULL,NULL,'Dev',0,NULL,NULL,1),
(-103,'Sean VD','VD','Sean',NULL,'VD, Sean',NULL,NULL,'Dev',0,NULL,NULL,1),
(-104,'Rick Bielawski','Bielawski','Rick',NULL,'Rick, Bielawski',NULL,NULL,'Dev',0,NULL,NULL,1),
(-105,'Tim Michlaski','Michlaski','Tim',NULL,'Tim, Michlaski',NULL,NULL,'Dev',0,NULL,NULL,1),
(-106,'Paul Harmon','Harmon ','Paul ',NULL,'Paul , Harmon ',NULL,NULL,'Dev',0,NULL,NULL,1),
(-107,'Josh Pierro','Pierro','Josh',NULL,'Josh , Pierro',NULL,NULL,'Dev',0,NULL,NULL,1),
(-108,'Ryan Voronyak','Voronyak','Ryan',NULL,'Ryan, Voronyak',NULL,NULL,'Dev',0,NULL,NULL,1),
(-109,'Adam Eslinger','Eslinger','Adam',NULL,'Adam, Eslinger',NULL,NULL,'Dev',0,NULL,NULL,1),
(-110,'Ron Wilson','Wilson','Ron',NULL,'Wilson, Ron',NULL,NULL,'Dev',0,NULL,NULL,1),
(-111,'Matt Marino','Marino','Matt',NULL,'Marino, Matt',NULL,NULL,'Dev',0,NULL,NULL,1),
(-112,'Grahame Beresford','Beresford','Grahame',NULL,'Beresford, Grahame',NULL,NULL,'Dev',0,NULL,NULL,1),
(-113,'Tim Goodman','Goodman','Tim',NULL,'Goodman, Tim',NULL,NULL,'Dev',0,NULL,NULL,1)
		) AS src([Contact_Key],[FullName],[LastName],[FirstName],[MiddleName],[LastnameFirstname],[Nickname],[SpouseName],[Title],[IsDefunct],[DefunctReason],[LegacyRowID],[IsSolicitable])
ON
	trgt.[Contact_Key] = src.[Contact_Key]
WHEN MATCHED THEN
	UPDATE SET
		[FullName] = src.[FullName]
		, [LastName] = src.[LastName]
		, [FirstName] = src.[FirstName]
		, [MiddleName] = src.[MiddleName]
		, [LastnameFirstname] = src.[LastnameFirstname]
		, [Nickname] = src.[Nickname]
		, [SpouseName] = src.[SpouseName]
		, [Title] = src.[Title]
		, [IsDefunct] = src.[IsDefunct]
		, [DefunctReason] = src.[DefunctReason]
		, [CreatedBy] = 'SEEDDATA'
        ,[IsSolicitable] = src.[IsSolicitable]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Contact_Key],[FullName],[LastName],[FirstName],[MiddleName],[LastnameFirstname],[Nickname],[SpouseName],[Title],[IsDefunct],[DefunctReason],[CreatedBy], [IsSolicitable])
	VALUES ([Contact_Key],[FullName],[LastName],[FirstName],[MiddleName],[LastnameFirstname],[Nickname],[SpouseName],[Title],[IsDefunct],[DefunctReason],'SEEDDATA', [IsSolicitable])
;
SET IDENTITY_INSERT [dESx].[Contact] OFF;

PRINT N'Seeding of Contact complete.'

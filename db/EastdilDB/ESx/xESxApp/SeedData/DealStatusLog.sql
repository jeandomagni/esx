﻿PRINT N'Seeding DealStatusLog.'

SET IDENTITY_INSERT [dESx].[DealStatusLog] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel,
		ABS(CHECKSUM(name)) AS Random
	FROM sys.columns 
)
MERGE INTO [dESx].[DealStatusLog] AS trgt
USING	(
			SELECT 
				[DealStatusLog_Key] = -1 * RowNumber,
				[Deal_Key] = -1 * RowNumber,
				[DealStatus_Code] = CASE Random % 12
									WHEN 0 THEN	 'Targeting'
									WHEN 1 THEN  'Pitching'
									WHEN 2 THEN  'Pitched and Lost'
									WHEN 3 THEN  'On Hold'
									WHEN 4 THEN  'Packaging'
									WHEN 5 THEN  'Marketing'
									WHEN 6 THEN  'Bid Receipt'
									WHEN 7 THEN  'Under Agreement'
									WHEN 8 THEN  'Non-Refundable'
									WHEN 9 THEN  'Withdrawn by Client'
									WHEN 10 THEN 'Withdrawn by Counterparty'
									WHEN 11 THEN 'Closed'
									ELSE NULL END,
				[CreatedDate] = getutcdate(),
				[CreatedBy] = 'SEEDDATA',
				[UpdatedDate] = getutcdate(),
				[UpdatedBy] = 'SEEDDATA'
			FROM Data
		) AS src([DealStatusLog_Key],[Deal_Key],[DealStatus_Code],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
ON
	trgt.[DealStatusLog_Key] = src.[DealStatusLog_Key]
WHEN MATCHED THEN
	UPDATE SET
		 [Deal_Key]			 = src.[Deal_Key]
		,[DealStatus_Code]	 = src.[DealStatus_Code]
		,[CreatedDate]		 = src.[CreatedDate]
		,[CreatedBy]		 = src.[CreatedBy]
		,[UpdatedDate]		 = src.[UpdatedDate]
		,[UpdatedBy]		 = src.[UpdatedBy]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealStatusLog_Key],[Deal_Key],[DealStatus_Code],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
	VALUES ([DealStatusLog_Key],[Deal_Key],[DealStatus_Code],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])

;
SET IDENTITY_INSERT [dESx].[DealStatusLog] OFF;

PRINT N'Seeding of Deal completeStatusLog.'

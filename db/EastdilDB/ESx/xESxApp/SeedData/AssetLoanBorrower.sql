PRINT N'Seeding AssetLoanBorrower.'

SET IDENTITY_INSERT [dESx].[AssetLoanBorrower] ON;

;WITH Data AS (
	SELECT TOP 200
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[AssetLoanBorrower] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			(-1 * RowNumber *13) % 100 -1, -- This is to somewhat randomize
			-1 * RowNumber -200,
			'SEEDDATA'
		FROM Data
		) AS src([LoanBorrower_Key],[Company_Key],[Asset_Key],[CreatedBy])
ON
	trgt.[LoanBorrower_Key] = src.[LoanBorrower_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Company_Key] = src.[Company_Key]
		, [Asset_Key] = src.[Asset_Key]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([LoanBorrower_Key],[Company_Key],[Asset_Key],[CreatedBy], [UpdatedDate])
	VALUES ([LoanBorrower_Key],[Company_Key],[Asset_Key],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[AssetLoanBorrower] OFF;

PRINT N'Seeding AssetLoanBorrower complete.'

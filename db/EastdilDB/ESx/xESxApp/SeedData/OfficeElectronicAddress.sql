﻿PRINT N'Seeding OfficeElectronicAddress.'

SET IDENTITY_INSERT [dESx].[Office_ElectronicAddress] ON;
MERGE INTO [dESx].[Office_ElectronicAddress] AS trgt
USING	(
		SELECT Office_Key AS Office_ElectronicAddress_Key, Office_Key, Office_Key AS ElectronicAddress_Key 
		FROM dESx.Office WHERE Office_Key < 0
		) AS src([Office_ElectronicAddress_Key],[Office_Key],[ElectronicAddress_Key])
ON
	trgt.[Office_ElectronicAddress_Key] = src.[Office_ElectronicAddress_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Office_Key] = src.[Office_Key]
		, [ElectronicAddress_Key] = src.[ElectronicAddress_Key]
		, [CreatedBy] = 'SEEDDATA'
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Office_ElectronicAddress_Key],[Office_Key],[ElectronicAddress_Key],[CreatedBy], [UpdatedDate])
	VALUES ([Office_ElectronicAddress_Key],[Office_Key],[ElectronicAddress_Key],'SEEDDATA', '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[Office_ElectronicAddress] OFF;

PRINT N'Seeding of OfficeElectronicAddress complete.'

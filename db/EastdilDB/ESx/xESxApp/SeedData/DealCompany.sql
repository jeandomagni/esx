﻿PRINT N'Seeding DealCompany.'

SET IDENTITY_INSERT [dESx].[DealCompany] ON;
;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel,
		ABS(CHECKSUM(name)) AS Random
	FROM sys.columns 
)
MERGE INTO [dESx].[DealCompany] AS trgt
USING	(
		SELECT
			-1 * RowNumber AS DealClient_Key, 
			-1 * RowNumber AS Deal_Key, 
			-1 * RowNumber AS Company_Key,
			[DealParticipationType_Code] =	CASE Random % 5
											WHEN 0 THEN 'Seller'
											WHEN 1 THEN 'Buyer'
											WHEN 2 THEN 'Lender'
											WHEN 3 THEN 'Borrower'
											ELSE 'Broker' END,
			0 AS IsClient,
			'SEEDDATA'
		FROM Data
		) AS src([DealCompany_Key],[Deal_Key],[Company_Key],[DealParticipationType_Code],[IsClient],[CreatedBy])
ON
	trgt.[DealCompany_Key] = src.[DealCompany_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Deal_Key] = src.[Deal_Key]
		, [Company_Key] = src.[Company_Key]
		, [DealParticipationType_Code] = src.[DealParticipationType_Code]
		, [IsClient] = src.[IsClient]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([DealCompany_Key],[Deal_Key],[Company_Key],[DealParticipationType_Code],[IsClient],[CreatedBy], [UpdatedDate])
	VALUES ([DealCompany_Key],[Deal_Key],[Company_Key],[DealParticipationType_Code],[IsClient],[CreatedBy], '2006-01-01')
;
SET IDENTITY_INSERT [dESx].[DealCompany] OFF;

PRINT N'Seeding of DealCompany complete.'

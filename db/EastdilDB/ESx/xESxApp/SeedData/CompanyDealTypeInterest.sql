
PRINT N'Seeding [CompanyDealTypeInterest].'

SET IDENTITY_INSERT [dESx].[CompanyDealTypeInterest] ON;

;WITH Data AS (
	SELECT TOP 100
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[CompanyDealTypeInterest] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			'Investment Banking',
			'SEEDDATA'
		FROM Data
		) AS src([CompanyDealTypeInterest_Key],[Company_Key],[DealType_Code],[CreatedBy])
ON
	trgt.[CompanyDealTypeInterest_Key] = src.[CompanyDealTypeInterest_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Company_Key] = src.[Company_Key]
		, [DealType_Code] = src.[DealType_Code]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'

WHEN NOT MATCHED BY TARGET THEN
	INSERT ([CompanyDealTypeInterest_Key],[Company_Key],[DealType_Code],[CreatedBy], [UpdatedDate])
	VALUES ([CompanyDealTypeInterest_Key],[Company_Key],[DealType_Code],[CreatedBy], '2006-01-01')

;
SET IDENTITY_INSERT [dESx].[CompanyDealTypeInterest] OFF;

PRINT N'Seeding [CompanyDealTypeInterest] complete.'


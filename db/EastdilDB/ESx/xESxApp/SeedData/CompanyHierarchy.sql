--PRINT N'Seeding [CompanyHierarchy].'

--SET IDENTITY_INSERT [dESx].[CompanyHierarchy] ON;

--MERGE INTO [dESx].[CompanyHierarchy] AS trgt
--USING	(VALUES
--		(add values here)
--		) AS src([CompanyHierarchy_Key],[Child__Company_Key],[Parent__Company_Key],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
--ON
--	trgt.[CompanyHierarchy_Key] = src.[CompanyHierarchy_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[Child__Company_Key] = src.[Child__Company_Key]
--		, [Parent__Company_Key] = src.[Parent__Company_Key]
--		, [CreatedDate] = src.[CreatedDate]
--		, [CreatedBy] = src.[CreatedBy]
--		, [UpdatedDate] = src.[UpdatedDate]
--		, [UpdatedBy] = src.[UpdatedBy]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([CompanyHierarchy_Key],[Child__Company_Key],[Parent__Company_Key],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])
--	VALUES ([CompanyHierarchy_Key],[Child__Company_Key],[Parent__Company_Key],[CreatedDate],[CreatedBy],[UpdatedDate],[UpdatedBy])

--;
--SET IDENTITY_INSERT [dESx].[CompanyHierarchy] OFF;

--PRINT N'Seeding [CompanyHierarchy] complete.'


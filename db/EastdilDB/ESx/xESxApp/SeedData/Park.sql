PRINT N'Seeding Park.'

SET IDENTITY_INSERT [dESx].[Park] ON;

MERGE INTO [dESx].[Park] AS trgt
USING	(VALUES
		(-1, 'Legends at Village West'),
		(-2, 'The Outlet Shoppes at Gettysburg'),
		(-3, 'Milford Plaza'),
		(-4, 'Fashion Outlets of Las Vegas'),
		(-5, 'Project Sandcastle'),
		(-6, 'Palm Beach Outlets'),
		(-7, 'Freeport Outlet Centers')
		) AS src([Park_Key],[Name])
ON
	trgt.[Park_Key] = src.[Park_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Name] = src.[Name]
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Park_Key],[Name])
	VALUES ([Park_Key],[Name])

;
SET IDENTITY_INSERT [dESx].[Park] OFF;

PRINT N'Seeding Park complete.'
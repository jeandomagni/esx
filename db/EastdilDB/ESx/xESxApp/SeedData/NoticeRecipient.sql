--PRINT N'Seeding [NoticeRecipient].'

--SET IDENTITY_INSERT [dESx].[NoticeRecipient] ON;

--MERGE INTO [dESx].[NoticeRecipient] AS trgt
--USING	(VALUES
--		(add values here)
--		) AS src([NoticeRecipient_Key],[Notice_Key],[User_Key],[IsAcknowledged],[SnoozeUntilTime])
--ON
--	trgt.[NoticeRecipient_Key] = src.[NoticeRecipient_Key]
--WHEN MATCHED THEN
--	UPDATE SET
--		[Notice_Key] = src.[Notice_Key]
--		, [User_Key] = src.[User_Key]
--		, [IsAcknowledged] = src.[IsAcknowledged]
--		, [SnoozeUntilTime] = src.[SnoozeUntilTime]
--WHEN NOT MATCHED BY TARGET THEN
--	INSERT ([NoticeRecipient_Key],[Notice_Key],[User_Key],[IsAcknowledged],[SnoozeUntilTime])
--	VALUES ([NoticeRecipient_Key],[Notice_Key],[User_Key],[IsAcknowledged],[SnoozeUntilTime])

--;
--SET IDENTITY_INSERT [dESx].[NoticeRecipient] OFF;

--PRINT N'Seeding [NoticeRecipient] complete.'


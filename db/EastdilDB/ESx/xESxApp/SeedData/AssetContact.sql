PRINT N'Seeding AssetContact.'

SET IDENTITY_INSERT [dESx].[AssetContact] ON;

;WITH Data AS (
	SELECT TOP 250
		ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS RowNumber, 
		CAST(ROW_NUMBER() OVER(ORDER BY OBJECT_ID) AS VARCHAR(10)) AS RowLabel
	FROM sys.columns 
)
MERGE INTO [dESx].[AssetContact] AS trgt
USING	(
		SELECT 
			-1 * RowNumber,
			-1 * RowNumber,
			-1 * RowNumber %100 -1,
			NULL,
			'Unknown',
			'SEEDDATA'
		FROM Data
		) AS src([AssetContact_Key],[Asset_Key],[Contact_Key],[EndDate],[AssetContactType_Code],[CreatedBy])
ON
	trgt.[AssetContact_Key] = src.[AssetContact_Key]
WHEN MATCHED THEN
	UPDATE SET
		[Asset_Key] = src.[Asset_Key]
		, [Contact_Key] = src.[Contact_Key]
		, [EndDate] = src.[EndDate]
		, [AssetContactType_Code] = src.[AssetContactType_Code]
		, [CreatedBy] = src.[CreatedBy]
		, [UpdatedDate] = '2006-01-01'
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([AssetContact_Key],[Asset_Key],[Contact_Key],[EndDate],[AssetContactType_Code],[CreatedBy], [UpdatedDate])
	VALUES ([AssetContact_Key],[Asset_Key],[Contact_Key],[EndDate],[AssetContactType_Code],[CreatedBy], '2006-01-01')
;

SET IDENTITY_INSERT [dESx].[AssetContact] OFF;

PRINT N'Seeding AssetContact complete.'

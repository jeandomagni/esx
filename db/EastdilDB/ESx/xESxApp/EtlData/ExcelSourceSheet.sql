-- Script Generated ON DTCJR90G9CK2
SET IDENTITY_INSERT dExcelETL.ExcelSourceSheet ON;
INSERT dExcelETL.ExcelSourceSheet([ExcelSourceSheet_Key],[ExcelSourceFile_Key],[SheetName],[ColumnCount],[IsComplete],[SheetFormat])
SELECT * FROM (VALUES
 (1, 1, N'''1995$''', 15, 1, N'ClosedTransactions')
,(2, 1, N'''1996$''', 18, 1, N'ClosedTransactions')
,(3, 1, N'''1997$''', 18, 1, N'ClosedTransactions')
,(4, 1, N'''1998$''', 18, 1, N'ClosedTransactions')
,(5, 1, N'''1999$''', 18, 1, N'ClosedTransactions')
,(6, 1, N'''2000$''', 18, 1, N'ClosedTransactions')
,(7, 1, N'''2001$''', 12, 1, N'ClosedTransactions')
,(8, 1, N'''2002$''', 16, 1, N'ClosedTransactions')
,(9, 1, N'''2003$''', 12, 1, N'ClosedTransactions')
,(10, 1, N'''2004$''', 15, 1, N'ClosedTransactions')
,(11, 1, N'''2005$''', 15, 1, N'ClosedTransactions')
,(12, 1, N'''2006$''', 26, 1, N'ClosedTransactions')
,(13, 1, N'''2007$''', 13, 1, N'ClosedTransactions')
,(14, 1, N'''2008$''', 14, 1, N'ClosedTransactions')
,(15, 1, N'''2009$''', 13, 1, N'ClosedTransactions')
,(16, 1, N'''2010$''', 12, 1, N'ClosedTransactions')
,(17, 1, N'''2011$''', 12, 1, N'ClosedTransactions')
,(18, 1, N'''2012$''', 13, 1, N'ClosedTransactions')
,(19, 1, N'''2013$''', 12, 1, N'ClosedTransactions')
,(20, 1, N'''2014$''', 15, 1, N'ClosedTransactions')
,(21, 1, N'''2015$''', 13, 1, N'ClosedTransactions')
,(22, 2, N'''ES 2006$''', 25, 1, N'ClosedProperties')
,(23, 2, N'''ES 2007$''', 91, 1, N'ClosedProperties')
,(24, 2, N'''ES 2008$''', 93, 1, N'ClosedProperties')
,(25, 2, N'''ES 2009$''', 71, 1, N'ClosedProperties')
,(26, 2, N'''ES 2010$''', 71, 1, N'ClosedProperties')
,(27, 2, N'''ES 2011$''', 145, 1, N'ClosedProperties')
,(28, 2, N'''ES 2012$''', 71, 1, N'ClosedProperties')
,(29, 2, N'''ES 2013$''', 71, 1, N'ClosedProperties')
,(30, 2, N'''ES 2014$''', 54, 1, N'ClosedProperties')
,(31, 2, N'''ES 2015$''', 54, 1, N'ClosedProperties')
,(32, 2, N'''SCC 2002$''', 22, 1, N'ClosedProperties')
,(33, 2, N'''SCC 2003$''', 22, 1, N'ClosedProperties')
,(34, 2, N'''SCC 2004$''', 21, 1, N'ClosedProperties')
,(35, 2, N'''SCC 2005$''', 40, 1, N'ClosedProperties')
,(36, 3, N'''2000-15$''', 207, 1, N'FinancingTransactions')
,(37, 3, N'Pivot$', 10, 1, N'Unknown')
,(38, 4, N'BlackBox$', 6, 1, N'Unknown')
,(39, 4, N'''ESP TABLES >>>$''', 2, 1, N'Unknown')
,(40, 4, N'ESPBranchEntities$', 6, 1, N'ESPBranchEntities')
,(41, 4, N'ESPDealid$', 4, 1, N'ESPDealid')
,(42, 4, N'''JOINING SL TABLES >>>$''', 2, 1, N'Unknown')
,(43, 4, N'''SL TABLES >>>$''', 2, 1, N'Unknown')
,(44, 4, N'SLAliases$', 3, 1, N'SLAliases')
,(45, 4, N'SLBranchCanonicals$', 5, 1, N'SLBranchCanonicals')
,(46, 4, N'SLCanonicalAliases$', 5, 1, N'SLCanonicalAliases')
,(47, 4, N'SLCanonicals$', 3, 1, N'SLCanonicals')
,(48, 4, N'SLCanonicalTickers$', 7, 1, N'SLCanonicalTickers')
,(49, 4, N'SLDealCanonicals$', 8, 1, N'SLDealCanonicals')
,(50, 4, N'SLDealPhotos$', 8, 1, N'SLDealPhotos')
,(51, 4, N'SLDealProps$', 22, 1, N'SLDealProps')
,(52, 4, N'SLDealRoles$', 3, 1, N'SLDealRoles')
,(53, 4, N'SLProperties$', 7, 1, N'SLProperties')
,(54, 4, N'SLTickers$', 3, 1, N'SLTickers')
,(55, 5, N'Sheet1$', 7, 1, N'EastdilOffices')
,(56, 5, N'Sheet2$', 2, 1, N'Unknown')
,(57, 5, N'Sheet3$', 2, 1, N'Unknown')
)x([ExcelSourceSheet_Key],[ExcelSourceFile_Key],[SheetName],[ColumnCount],[IsComplete],[SheetFormat]);
PRINT CAST(@@RowCount as nvarchar)+' rows inserted into dExcelETL.ExcelSourceSheet';
SET IDENTITY_INSERT dExcelETL.ExcelSourceSheet OFF;

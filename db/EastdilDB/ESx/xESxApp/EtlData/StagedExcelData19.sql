-- Script Generated ON DTCJR90G9CK2
SET IDENTITY_INSERT dExcelETL.StagedExcelData ON;
INSERT dExcelETL.StagedExcelData([StagedExcelData_Key],[ExcelSourceSheet_Key],[RowNumber],[RowData])
SELECT * FROM (VALUES
 (90001, 51, 363, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>433</F4><F5>Mercedes-Benz of North Olmsted
28450 Lorain Rd
North Olmsted, OH</F5></Row>')
,(90002, 51, 364, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>434</F4><F5>Mercedes-Benz CPO of North Olmsted
28595 Lorain Rd
North Olmsted, OH</F5></Row>')
,(90003, 51, 365, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>440</F4><F5>Coggin Honda of St. Augustine
2925 US Highway 1 S
St. Augustine, FL</F5></Row>')
,(90004, 51, 366, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>448</F4><F5>RSM Honda
29961 Santa Margarita Pkwy
Rancho Santa Marg, CA</F5></Row>')
,(90005, 51, 367, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>460</F4><F5>Maxwell Buick GMC
3000 Interstate 35
Round Rock, TX</F5></Row>')
,(90006, 51, 368, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>461</F4><F5>Heritage Honda
3001 E Ave
Baltimore, MD</F5></Row>')
,(90007, 51, 369, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>469</F4><F5>Orr Toyota of Searcy
301 S Poplar St
Searcy, AR</F5></Row>')
,(90008, 51, 370, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>473</F4><F5>Roundtree Pre-Owned Supercenter
3016 Government Blvd
Mobile, AL</F5></Row>')
,(90009, 51, 371, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>479</F4><F5>Round Rock Nissan
3050 Interstate 35
Round Rock, TX</F5></Row>')
,(90010, 51, 372, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>483</F4><F5>Action Nissan
307 Thompson Ln
Nashville, TN</F5></Row>')
,(90011, 51, 373, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>495</F4><F5>Hendrick Chevrolet Cadillac of Monroe
3112 Hwy 74 W
Monroe, NC</F5></Row>')
,(90012, 51, 374, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>496</F4><F5>Roundtree Chrysler Dodge Jeep
3118 Government Blvd
Mobile, AL</F5></Row>')
,(90013, 51, 375, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>498</F4><F5>Palm Harbor Honda
31200 US Highway 19 N
Palm Harbor, FL</F5></Row>')
,(90014, 51, 376, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>499</F4><F5>Herb Gordon Volvo
3121 Automobile Blvd
Silver Spring, MD</F5></Row>')
,(90015, 51, 377, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>501</F4><F5>Herb Gordon Nissan
3131 Automobile Blvd
Silver Spring, MD</F5></Row>')
,(90016, 51, 378, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>505</F4><F5>Hall Chrysler Dodge Jeep Ram
3152 Virginia Beach Blvd
Virginia Beach, VA</F5></Row>')
,(90017, 51, 379, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>507</F4><F5>Herb Gordon Subaru
3161 Automobile Blvd
Silver Spring, MD</F5></Row>')
,(90018, 51, 380, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>510</F4><F5>Hall Acura Virginia Beach
3200 Virginia Beach Blvd
Virginia Beach, VA</F5></Row>')
,(90019, 51, 381, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>525</F4><F5>Ira Toyota of Manchester
33 Auto Center Rd
Manchester, NH</F5></Row>')
,(90020, 51, 382, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>538</F4><F5>Turnersville Automall
3400 New Jersey 42
Blackwood, NJ</F5></Row>')
,(90021, 51, 383, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>540</F4><F5>CarMax Greensboro
3412 W Wendover Ave
Greensboro, NC</F5></Row>')
,(90022, 51, 384, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>541</F4><F5>Hall Chevrolet/Hyundai (TID010&amp;012)
3412 Western Branch Rd
Chesapeake, VA</F5></Row>')
,(90023, 51, 385, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>542</F4><F5>Hall Nissan Chesapeake
3417 Western Branch Blvd
Chesapeake, VA</F5></Row>')
,(90024, 51, 386, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>548</F4><F5>Hall Mitsubishi (TID008&amp;009)
3496 Holland Rd
Virginia Beach, VA</F5></Row>')
,(90025, 51, 387, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>549</F4><F5>Woodfield Lexus
350 E Golf Rd
Schaumburg, IL</F5></Row>')
,(90026, 51, 388, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>551</F4><F5>Scottsdale Body Shop
350 N Hayden Rd
Scottsdale, AZ</F5></Row>')
,(90027, 51, 389, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>552</F4><F5>East Madison Toyota
3501 Lancaster Dr
Madison, WI</F5></Row>')
,(90028, 51, 390, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>554</F4><F5>Hall Honda
3516 Virginia Beach Blvd
Virginia Beach, VA</F5></Row>')
,(90029, 51, 391, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>560</F4><F5>Crown Volvo
3604 W Wendover Ave
Greensboro, NC</F5></Row>')
,(90030, 51, 392, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>561</F4><F5>Bohn Brothers Toyota Scion
3611 Lapalco Blvd
Harvey, LA</F5></Row>')
,(90031, 51, 393, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>564</F4><F5>Nissan of Clovis
370 W Herndon Ave
Clovis, CA</F5></Row>')
,(90032, 51, 394, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>565</F4><F5>Future Nissan of Alvin
3700 Alvin Bypass
Alvin, TX</F5></Row>')
,(90033, 51, 395, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>566</F4><F5>Former Massey Cadillac of Sanford
3700 S Hwy 17-92
Sanford, FL</F5></Row>')
,(90034, 51, 396, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>569</F4><F5>Lujack''s Honda/Hyundai
3707 N Harrison St
Davenport, IA</F5></Row>')
,(90035, 51, 397, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>572</F4><F5>Don Bohn Ford
3737 Lapalco Blvd
Harvey, LA</F5></Row>')
,(90036, 51, 398, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>574</F4><F5>Hall Nissan Virginia Beach
3757 Bonney Rd
Virginia Beach, VA</F5></Row>')
,(90037, 51, 399, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>575</F4><F5>Mark Dodge Chrysler Jeep
3777 Gerstner Memorial Dr
Lake Charles, LA</F5></Row>')
,(90038, 51, 400, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>579</F4><F5>David McDavid Honda Used Car Supercenter
3800 W Airport Fwy
Irving, TX</F5></Row>')
,(90039, 51, 401, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>580</F4><F5>Don Bohn Buick GMC
3801 Lapalco Blvd
Harvey, LA</F5></Row>')
,(90040, 51, 402, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>589</F4><F5>Mercedes Benz of Houston - Greenway
3900 SW Fwy
Houston, TX</F5></Row>')
,(90041, 51, 403, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>598</F4><F5>Momentum/Advantage BMW
400 Gulf Fwy S
League City, TX</F5></Row>')
,(90042, 51, 404, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>599</F4><F5>Gurley-Leep Audi Mercedes VW Kia
4004 N Grape Rd
Mishawaka, IN</F5></Row>')
,(90043, 51, 405, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>606</F4><F5>Don Massey Cadillac
40475 E Ann Arbor Rd
Plymouth, MI</F5></Row>')
,(90044, 51, 406, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>616</F4><F5>Bill Knight Lincoln Volvo
4111 S Memorial Dr
Tulsa, OK</F5></Row>')
,(90045, 51, 407, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>617</F4><F5>Ron Craft Chevrolet Cadillac/Baytown Ford
4114 Interstate 10 E
Baytown, TX</F5></Row>')
,(90046, 51, 408, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>618</F4><F5>Baytown Honda
4141 Interstate 10 E
Baytown, TX</F5></Row>')
,(90047, 51, 409, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>619</F4><F5>Audi of Fairfield
415-435 Commerce Dr
Fairfield, CT</F5></Row>')
,(90048, 51, 410, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>621</F4><F5>Szott M-59 Toyota Scion
4178 Highland Rd
Waterford Township, MI</F5></Row>')
,(90049, 51, 411, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>626</F4><F5>Massey Cadillac
4241 N John Young Pkwy
Orlando, FL</F5></Row>')
,(90050, 51, 412, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>629</F4><F5>Stone Mountain Nissan
4275 U.S. 78
Lilburn, GA</F5></Row>')
,(90051, 51, 413, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>631</F4><F5>All American Chrysler Dodge Jeep
4310 Sherwood Way
San Angelo, TX</F5></Row>')
,(90052, 51, 414, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>632</F4><F5>San Angelo Parking Lot
4346 Sherwood Way
San Angelo, TX</F5></Row>')
,(90053, 51, 415, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>633</F4><F5>McCluskey Used Cars &amp; Body Shop
435 E Galbraith
Cincinnati, OH</F5></Row>')
,(90054, 51, 416, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>635</F4><F5>Hall Mazda
4372 Holland Rd
Virginia Beach, VA</F5></Row>')
,(90055, 51, 417, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>638</F4><F5>Hyundai City
4395 U.S. 130
Burlington, NJ</F5></Row>')
,(90056, 51, 418, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>639</F4><F5>Chrysler Dodge Jeep City
4395 U.S. 130
Burlington, NJ</F5></Row>')
,(90057, 51, 419, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>643</F4><F5>Classic Stateline Kia
4411 State Line Ave
Texarkana, TX</F5></Row>')
,(90058, 51, 420, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>644</F4><F5>Coggin BMW Treasure Coast
4429 S US Highway 1
Fort Pierce, FL</F5></Row>')
,(90059, 51, 421, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>648</F4><F5>Mercedes-Benz of Fairfield Service/Warehouse
450 Scofield Ave
Fairfield, CT</F5></Row>')
,(90060, 51, 422, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>652</F4><F5>Ferrari Maserati of Washington
45235 Towlern Pl
Sterling, VA</F5></Row>')
,(90061, 51, 423, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>655</F4><F5>Pohanka Used Cars
4608 St Barnabas Rd
Marlow Heights, MD</F5></Row>')
,(90062, 51, 424, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>660</F4><F5>Victory Buick/GMC
4702 N Navarro St
Victoria, TX</F5></Row>')
,(90063, 51, 425, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>662</F4><F5>Future Toyota Scion Gorham
471 Main St
Gorham, NH</F5></Row>')
,(90064, 51, 426, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>663</F4><F5>Porsche of Fairfield
475 Commerce Dr
Fairfield, CT</F5></Row>')
,(90065, 51, 427, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>664</F4><F5>Mercedes-Benz of San Diego
4750 Kearny Mesa Rd
San Diego, CA</F5></Row>')
,(90066, 51, 428, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>665</F4><F5>Former Pohanka Body Shop
4756 Stamp Rd
Marlow Heights, MD</F5></Row>')
,(90067, 51, 429, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>667</F4><F5>Toyota Pre-Owned of San Diego
4812 Kearny Mesa Rd
San Diego, CA</F5></Row>')
,(90068, 51, 430, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>668</F4><F5>Berlin City Ford Lincoln
485 Main St
Gorham, NH</F5></Row>')
,(90069, 51, 431, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>670</F4><F5>Kearny Mesa Toyota
4910 Kearny Mesa Rd
San Diego, CA</F5></Row>')
,(90070, 51, 432, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>671</F4><F5>Lexus San Diego
4970 Kearny Mesa Rd
San Diego, CA</F5></Row>')
,(90071, 51, 433, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>680</F4><F5>Kelley Chevrolet
500 E State Blvd
Fort Wayne, IN</F5></Row>')
,(90072, 51, 434, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>681</F4><F5>Global BMW
500 Interstate N Pkwy
Atlanta, GA</F5></Row>')
,(90073, 51, 435, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>684</F4><F5>MAG Excess Land
5000 Block of Venture Dr
Dublin, OH</F5></Row>')
,(90074, 51, 436, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>687</F4><F5>Former City Chevrolet
5000 W Reno Ave
Oklahoma City, OK</F5></Row>')
,(90075, 51, 437, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>688</F4><F5>BMW of Ann Arbor
501 Auto Mall Dr
Ann Arbor, MI</F5></Row>')
,(90076, 51, 438, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>690</F4><F5>Lexus Pre-Owned of San Diego
5010 Kearny Mesa Rd
San Diego, CA</F5></Row>')
,(90077, 51, 439, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>692</F4><F5>Chevrolet of Dublin
5016 Post Rd
Dublin, OH</F5></Row>')
,(90078, 51, 440, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>695</F4><F5>Midwestern Auto Group: Ferrari
5035 Post Rd
Dublin, OH</F5></Row>')
,(90079, 51, 441, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>696</F4><F5>Former Saturn of Fort Wayne (Automall)
505 Avenue of Autos
Fort Wayne, IN</F5></Row>')
,(90080, 51, 442, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>697</F4><F5>BMW Pre-Owned of San Diego
5050 Kearny Mesa Rd
San Diego, CA</F5></Row>')
,(90081, 51, 443, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>698</F4><F5>BMW of San Diego
5090 Kearny Mesa Rd
San Diego, CA</F5></Row>')
,(90082, 51, 444, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>703</F4><F5>Lexus of Marin
513 E Francisco Blvd
San Rafael, CA</F5></Row>')
,(90083, 51, 445, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>704</F4><F5>Infiniti of Ann Arbor
515 Auto Mall Dr
Ann Arbor, MI</F5></Row>')
,(90084, 51, 446, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>706</F4><F5>HW Marine - Bossier City
517 Benton Rd
Bossier City, LA</F5></Row>')
,(90085, 51, 447, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>707</F4><F5>Pohanka Hyundai/Nissan
5200 Jefferson Davis Hwy
Fredericksburg, VA</F5></Row>')
,(90086, 51, 448, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>708</F4><F5>Mark Mitsubishi
5200 San Mateo Blvd NE
Albequerque, NM</F5></Row>')
,(90087, 51, 449, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>709</F4><F5>Sheehy Ford of Marlow Heights
5201 Auth Rd
Marlow Heights, MD</F5></Row>')
,(90088, 51, 450, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>711</F4><F5>Gurley-Leep Nissan
5210 N Grape Rd
Mishawaka, IN</F5></Row>')
,(90089, 51, 451, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>716</F4><F5>Gurley-Leep GM Hyundai Subaru
5302 N Grape Rd
Mishawaka, IN</F5></Row>')
,(90090, 51, 452, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>718</F4><F5>Jackson Auto Imports
5320 Interstate 55 N
Jackson, MS</F5></Row>')
,(90091, 51, 453, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>723</F4><F5>Berlin City Chevrolet GMC Buick
545 Main St
Gorham, NH</F5></Row>')
,(90092, 51, 454, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>725</F4><F5>Palm Beach Toyota
551 S Millitary Trl
West Palm Beach, FL</F5></Row>')
,(90093, 51, 455, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>728</F4><F5>Pensacola Honda
5600 Pensacola Blvd
Pensacola, FL</F5></Row>')
,(90094, 51, 456, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>729</F4><F5>Ken Garff Richmond Toyota
5601 National Rd E
Richmond, IN</F5></Row>')
,(90095, 51, 457, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>733</F4><F5>Mercedes-Benz of Ann Arbor
570 Auto Mall Dr
Ann Arbor, MI</F5></Row>')
,(90096, 51, 458, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>735</F4><F5>Honda of Westminster
580 Baltimore Blvd
Westminster, MD</F5></Row>')
,(90097, 51, 459, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>736</F4><F5>Midwestern Auto Group: BMW
5825 Venture Dr
Dublin, OH</F5></Row>')
,(90098, 51, 460, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>737</F4><F5>Chrysler Dodge Jeep of Renton
585 Rainier Ave S
Renton, WA</F5></Row>')
,(90099, 51, 461, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>738</F4><F5>Berlin City Kia
586 Marshall Ave
Williston, VT</F5></Row>')
,(90100, 51, 462, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>739</F4><F5>Pensacola Honda Parking Lot
5900 N Old Palafox Hwy
Pensacola, FL</F5></Row>')
,(90101, 51, 463, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>743</F4><F5>Capitol Cadillac
5901 S Pennsylvania Ave
Lansing, MI</F5></Row>')
,(90102, 51, 464, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>751</F4><F5>Lithia BMW/Nissan
600 N Central Ave
Medford, OR</F5></Row>')
,(90103, 51, 465, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>752</F4><F5>Victoria Nissan
6003 N Navarro St
Victoria, TX</F5></Row>')
,(90104, 51, 466, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>761</F4><F5>Ferrari of Houston
6100 SW Fwy
Houston, TX</F5></Row>')
,(90105, 51, 467, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>763</F4><F5>Park Place Mercedes-Benz
6113 Lemmon Ave
Dallas, TX</F5></Row>')
,(90106, 51, 468, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>766</F4><F5>Rosenthal Acura/Mazda
619-625 N Frederick Ave
Gaithersburg, MD</F5></Row>')
,(90107, 51, 469, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>770</F4><F5>Landers Auto Body
6301 S University Ave
Little Rock, AR</F5></Row>')
,(90108, 51, 470, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>771</F4><F5>ADESA Nashville
631 Burnett Rd
Nashville, TN</F5></Row>')
,(90109, 51, 471, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>772</F4><F5>Tom Kelley Buick-GMC (Automall)
633 Avenue of Autos
Fort Wayne, IN</F5></Row>')
,(90110, 51, 472, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>773</F4><F5>Midwestern Auto Group
6335 Perimeter Loop Rd
Dublin, OH</F5></Row>')
,(90111, 51, 473, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>775</F4><F5>Bedford Collision Center
65 Broadway
Bedford, OH</F5></Row>')
,(90112, 51, 474, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>786</F4><F5>CarMax Oaklawn
6540 95th St
Oak Lawn, IL</F5></Row>')
,(90113, 51, 475, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>793</F4><F5>Germain Honda of Dublin
6715 Sawmill Rd
Dublin, OH</F5></Row>')
,(90114, 51, 476, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>794</F4><F5>Sheehy Ford-Nissan-Subaru of Springfield
6727 Loisdale Rd
Springfield, VA</F5></Row>')
,(90115, 51, 477, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>798</F4><F5>Sport Hyundai Dodge
6831 Black Horse Pike
Egg Harbor Township, NJ</F5></Row>')
,(90116, 51, 478, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>799</F4><F5>Momentum Toyota Scion of Tulsa
6868 E Broken Arrow Expy Frontage Rd
Tulsa, OK</F5></Row>')
,(90117, 51, 479, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>802</F4><F5>Clay Cooley Fiat
698 E Airport Fwy
Irving, TX</F5></Row>')
,(90118, 51, 480, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>808</F4><F5>Clay Cooley Chrysler Jeep Dodge Ram
700 E Airport Fwy
Irving, TX</F5></Row>')
,(90119, 51, 481, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>809</F4><F5>Jack Maxton Chevrolet
700 E Dublin Granville Rd
Columbus, OH</F5></Row>')
,(90120, 51, 482, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>811</F4><F5>Lithia Honda
700 N Central
Medford, OR</F5></Row>')
,(90121, 51, 483, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>812</F4><F5>Nix Auto Center
700 S George Nigh Expy
McAlester, OK</F5></Row>')
,(90122, 51, 484, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>815</F4><F5>Land Rover of Central Houston
7019 Old Katy Rd
Houston, TX</F5></Row>')
,(90123, 51, 485, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>816</F4><F5>Mercedes-Benz of Fresno
7055 N Palm Ave
Fresno, CA</F5></Row>')
,(90124, 51, 486, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>818</F4><F5>Fort Myers Parking Lot
7081 Pettys Way
Fort Myers, FL</F5></Row>')
,(90125, 51, 487, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>821</F4><F5>North Hills Toyota Service
711 Browns Ln
Pittsburgh, PA</F5></Row>')
,(90126, 51, 488, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>823</F4><F5>Capitol Chevrolet/Capitol BMW
711 Eastern Blvd
Montgomery, AL</F5></Row>')
,(90127, 51, 489, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>824</F4><F5>Lexus of Edison
711 U.S. 1
Edison, NJ</F5></Row>')
,(90128, 51, 490, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>826</F4><F5>14/69 Carwash (Automall)
714 Avenue of Autos
Fort Wayne, IN</F5></Row>')
,(90129, 51, 491, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>832</F4><F5>ABC St. Louis
721 S 45th St
Centreville, IL</F5></Row>')
,(90130, 51, 492, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>833</F4><F5>Coggin Buick-GMC &amp; Collision Center
7245 Blanding Blvd
Jacksonville, FL</F5></Row>')
,(90131, 51, 493, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>834</F4><F5>Coggin Used Car Lot
7245 Blanding Blvd
Jacksonville, FL</F5></Row>')
,(90132, 51, 494, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>837</F4><F5>West Palm Beach Kia
735 S Military Trl
West Palm Beach, FL</F5></Row>')
,(90133, 51, 495, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>838</F4><F5>North Hills Toyota Dealership
7401 McKnight Rd
Pittsburgh, PA</F5></Row>')
,(90134, 51, 496, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>839</F4><F5>Chandler Auto Mall
7430 W Orchid Ln
Chandler, AZ</F5></Row>')
,(90135, 51, 497, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>840</F4><F5>Chandler Auto Mall
7450 W Orchid Ln
Chandler, AZ</F5></Row>')
,(90136, 51, 498, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>841</F4><F5>Chandler Auto Mall
7460 W Orchid Ln
Chandler, AZ</F5></Row>')
,(90137, 51, 499, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>845</F4><F5>Honda West
7615 W Sahara Ave
Las Vegas, NV</F5></Row>')
,(90138, 51, 500, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>854</F4><F5>Mercedes-Benz Pre-Owned of Fairfield
80 Commerce Dr
Fairfield, CT</F5></Row>')
,(90139, 51, 501, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>857</F4><F5>Freedom Chevy Buick GMC
8008 Marvin D Love Fwy
Dallas, TX</F5></Row>')
,(90140, 51, 502, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>858</F4><F5>Fort Mill Auto Mall
801 Gold Hill Rd
Fort Mill, SC</F5></Row>')
,(90141, 51, 503, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>860</F4><F5>Lithia Volkswagen
801 N Riverside
Medford, OR</F5></Row>')
,(90142, 51, 504, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>862</F4><F5>Heritage Hyundai
801 York Rd
Towson, MD</F5></Row>')
,(90143, 51, 505, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>867</F4><F5>Honda of Tempe
8030 S Autoplex Loop
Tempe, AZ</F5></Row>')
,(90144, 51, 506, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>868</F4><F5>Porsche of Destin
808 Airport Rd
Destin, FL</F5></Row>')
,(90145, 51, 507, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>869</F4><F5>Tom Kelley Cadillac
811 Avenue of Autos
Fort Wayne, IN</F5></Row>')
,(90146, 51, 508, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>872</F4><F5>Freedom Chrysler Dodge Jeep Ram
815 E Camp Wisdom Rd
Duncanville, TX</F5></Row>')
,(90147, 51, 509, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>874</F4><F5>Roundtree Used
8188 Mansfield Rd
Shreveport, LA</F5></Row>')
,(90148, 51, 510, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>882</F4><F5>Kearny Mesa Rd Parking Lot
8330 Engineer Rd
San Diego, CA</F5></Row>')
,(90149, 51, 511, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>883</F4><F5>Kearny Mesa Rd Parking Lot
8361 Buckhorn St
San Diego, CA</F5></Row>')
,(90150, 51, 512, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>884</F4><F5>BMW of Roxbury
840 U.S. 46
Kenvil, NJ</F5></Row>')
,(90151, 51, 513, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>886</F4><F5>BMW of Fairfax
8427 Lee Hwy
Fairfax, VA</F5></Row>')
,(90152, 51, 514, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>887</F4><F5>BMW of Fairfax Body Shop
8439 Lee Hwy
Fairfax, VA</F5></Row>')
,(90153, 51, 515, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>888</F4><F5>Lone Star Ford
8477 N Fwy
Houston, TX</F5></Row>')
,(90154, 51, 516, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>890</F4><F5>Rosenthal Mazda/Nissan &amp; Sonic-Infiniti of Tysons Corner
8525 Leesburg Pike
Vienna, VA</F5></Row>')
,(90155, 51, 517, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>891</F4><F5>Mercedes-Benz/smart of Tysons Corner
8545 Leesburg Pike
Vienna, VA</F5></Row>')
,(90156, 51, 518, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>893</F4><F5>Audi Tysons Corner
8598 Leesburg Pike
Vienna, VA</F5></Row>')
,(90157, 51, 519, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>894</F4><F5>Porsche of Tysons Corner
8601 Westwood Center Dr
Vienna, VA</F5></Row>')
,(90158, 51, 520, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>895</F4><F5>Orr Infiniti
8727 Business Park Dr
Shreveport, LA</F5></Row>')
,(90159, 51, 521, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>896</F4><F5>Orr Acura
8730 Business Park Dr
Shreveport, LA</F5></Row>')
,(90160, 51, 522, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>897</F4><F5>Orr Pre-Owned SuperCenter
8730 Business Park Dr
Shreveport, LA</F5></Row>')
,(90161, 51, 523, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>898</F4><F5>Shreveport Mitsubishi
8748 Quimper Pl
Shreveport, LA</F5></Row>')
,(90162, 51, 524, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>899</F4><F5>Orr Cadillac
8750 Business Park Dr
Shreveport, LA</F5></Row>')
,(90163, 51, 525, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>901</F4><F5>Massey Cadillac
8819 S Orange Blossom Trl
Orlando, FL</F5></Row>')
,(90164, 51, 526, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>902</F4><F5>Auto Trim Design (Roundtree)
8855 Quimper Pl
Shreveport, LA</F5></Row>')
,(90165, 51, 527, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>904</F4><F5>Toyota of Ft. Worth Used Cars
8901 Camp Bowie W Blvd
Fort Worth, TX</F5></Row>')
,(90166, 51, 528, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>909</F4><F5>Orr Motors North
900 Truman Baker Dr
Searcy, AR</F5></Row>')
,(90167, 51, 529, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>910</F4><F5>Toyota/Scion of Fort Worth
9001 Camp Bowie W Blvd
Fort Worth, TX</F5></Row>')
,(90168, 51, 530, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>911</F4><F5>Infiniti of Charlotte Parking Lot
9009 E Independence Blvd
Mathews, NC</F5></Row>')
,(90169, 51, 531, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>912</F4><F5>Auffenberg Ford
901 S Illinois St
Belleville, IL</F5></Row>')
,(90170, 51, 532, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>917</F4><F5>Tom Kelley Volvo (Automall)
910 Avenue of Autos
Fort Wayne, IN</F5></Row>')
,(90171, 51, 533, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>918</F4><F5>Town &amp; Country Toyota/Scion
9101 S Blvd
Charlotte, NC</F5></Row>')
,(90172, 51, 534, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>919</F4><F5>Infiniti of Charlotte
9103 E Independence Blvd
Matthews, NC</F5></Row>')
,(90173, 51, 535, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>920</F4><F5>Tom Kelley Used Cars (Automall)
910A Avenue of Autos
Fort Wayne, IN</F5></Row>')
,(90174, 51, 536, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>923</F4><F5>Courtesy Chrysler, Jeep, Dodge
9207 Adamo Dr
Tampa, FL</F5></Row>')
,(90175, 51, 537, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>924</F4><F5>Heritage Chrysler
9213-9219 Harford Rd
Baltimore, MD</F5></Row>')
,(90176, 51, 538, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>925</F4><F5>Mesa Harley-Davidson
922 S Country Club Dr
Mesa, AZ</F5></Row>')
,(90177, 51, 539, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>926</F4><F5>Nissan of Garden Grove
9222 Trask Ave
Garden Grove, CA</F5></Row>')
,(90178, 51, 540, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>934</F4><F5>Orr Classic Chevrolet of Ashdown
941 N Constitution Ave
Ashdown, AR</F5></Row>')
,(90179, 51, 541, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>942</F4><F5>Toyota of Garden Grove
9670 Trask Ave
Garden Grove, CA</F5></Row>')
,(90180, 51, 542, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>946</F4><F5>BMW of Tulsa
9702 S Memorial Dr
Tulsa, OK</F5></Row>')
,(90181, 51, 543, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>949</F4><F5>ABC Detroit/Toledo
9797 Freemont Pike
Perrysburg, OH</F5></Row>')
,(90182, 51, 544, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>952</F4><F5>Momentum Paint &amp; Body
9911 Centre Pkwy
Houston, TX</F5></Row>')
,(90183, 51, 545, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>956</F4><F5>Inskip Parking Lot
Bald Hill Rd
Warwick, RI</F5></Row>')
,(90184, 51, 546, N'<Row><F1>9774</F1><F2>CRR3</F2><F3>Capital Automotive Portfolio</F3><F4>965</F4><F5>Orr Motors South Parking Lot
Hwy 67/167 &amp; Truman Baker Dr
Searcy, AR</F5></Row>')
,(90185, 51, 547, N'<Row><F1>9186</F1><F2>CRS3</F2><F3>Crossroads Plaza </F3><F4>343</F4><F5>Crossroads Plaza
213-405 Crossroads Blvd
Cary, NC</F5></Row>')
,(90186, 51, 548, N'<Row><F1>10567</F1><F2>CRT4</F2><F3>1310 North Courthouse Rd. (Financing)</F3><F4>164</F4><F5>1310 N Courthouse Rd
Arlington, VA</F5></Row>')
,(90187, 51, 549, N'<Row><F1>10367</F1><F2>CSF4</F2><F3>City Square </F3><F4>1109</F4><F5>3800 N Central Ave
Phoenix, AZ</F5></Row>')
,(90188, 51, 550, N'<Row><F1>10367</F1><F2>CSF4</F2><F3>City Square </F3><F4>1110</F4><F5>3838 N Central Ave
Phoenix, AZ</F5></Row>')
,(90189, 51, 551, N'<Row><F1>10367</F1><F2>CSF4</F2><F3>City Square </F3><F4>1111</F4><F5>4000 N Central Ave
Phoenix, AZ</F5></Row>')
,(90190, 51, 552, N'<Row><F1>9658</F1><F2>CST3</F2><F3>Project Coastline </F3><F4>359</F4><F5>Royal Hawaiian Center
2201 Kalakaua Ave
Honolulu, HI</F5></Row>')
,(90191, 51, 553, N'<Row><F1>9405</F1><F2>CTN3</F2><F3>101 Continental</F3><F4>44</F4><F5>Continental Tower
101 Continental Blvd
El Segundo, CA</F5></Row>')
,(90192, 51, 554, N'<Row><F1>9342</F1><F2>CUL3</F2><F3>Culver Studios </F3><F4>930</F4><F5>The Culver Studios
9336 W Washington Blvd
Culver City, CA</F5></Row>')
,(90193, 51, 555, N'<Row><F1>10010</F1><F2>CWF4</F2><F3>Cottonwood Corporate Center Financing</F3><F4>1124</F4><F5>2755 E Cottonwood Pkwy
Salt Lake City, UT</F5></Row>')
,(90194, 51, 556, N'<Row><F1>10010</F1><F2>CWF4</F2><F3>Cottonwood Corporate Center Financing</F3><F4>1125</F4><F5>2795 E Cottonwood Pkwy
Salt Lake City, UT</F5></Row>')
,(90195, 51, 557, N'<Row><F1>10010</F1><F2>CWF4</F2><F3>Cottonwood Corporate Center Financing</F3><F4>1126</F4><F5>2825 E Cottonwood Pkwy
Salt Lake City, UT</F5></Row>')
,(90196, 51, 558, N'<Row><F1>10010</F1><F2>CWF4</F2><F3>Cottonwood Corporate Center Financing</F3><F4>1127</F4><F5>2855 E Cottonwood Pkwy
Salt Lake City, UT</F5></Row>')
,(90197, 51, 559, N'<Row><F1>10833</F1><F2>DCF4</F2><F3>1175 North Main Street (Data Center) </F3><F4>1035</F4><F5>1175 N Main St
Harrisonburg, VA</F5></Row>')
,(90198, 51, 560, N'<Row><F1>10336</F1><F2>DEF4</F2><F3>Moanalua Hillside Apartments</F3><F4>1201</F4><F5>Moanalua Hillside Apartments
1229 Ala Kapuna St
Honolulu, HI</F5></Row>')
,(90199, 51, 561, N'<Row><F1>10366</F1><F2>DEX4</F2><F3>1000 &amp; 1100 Dexter Ave.</F3><F4>1033</F4><F5>1000 Dexter Ave N
Seattle, WA</F5></Row>')
,(90200, 51, 562, N'<Row><F1>10366</F1><F2>DEX4</F2><F3>1000 &amp; 1100 Dexter Ave.</F3><F4>1034</F4><F5>1100 Dexter Ave N
Seattle, WA</F5></Row>')
,(90201, 51, 563, N'<Row><F1>10864</F1><F2>DKF4</F2><F3>200 Discovery Park (Financing)</F3><F4>745</F4><F5>60 Acorn Park Dr
Cambridge, MA</F5></Row>')
,(90202, 51, 564, N'<Row><F1>10484</F1><F2>DPK4</F2><F3>200 Discovery Park</F3><F4>745</F4><F5>60 Acorn Park Dr
Cambridge, MA</F5></Row>')
,(90203, 51, 565, N'<Row><F1>10762</F1><F2>DTC4</F2><F3>Downtown Crown</F3><F4>1307</F4><F5>Downtown Crown
Sam Eig Hwy &amp; Fields Rd
Gaithersburg, MD</F5></Row>')
,(90204, 51, 566, N'<Row><F1>10916</F1><F2>DTL4</F2><F3>Doubletree Portland (Leased Fee)</F3><F4>1128</F4><F5>Doubletree Portland
1000 NE Multnomah St
Portland, OR</F5></Row>')
,(90205, 51, 567, N'<Row><F1>9562</F1><F2>DTN3</F2><F3>Doubletree Nashville</F3><F4>502</F4><F5>Doubletree Nashville
315 4th Ave N
Nashville, TN</F5></Row>')
,(90206, 51, 568, N'<Row><F1>8490</F1><F2>DUS3</F2><F3>Project Vault </F3><F4>957</F4><F5>Breite Strasse 20
Dusseldorf, Germany</F5></Row>')
,(90207, 51, 569, N'<Row><F1>8490</F1><F2>DUS3</F2><F3>Project Vault </F3><F4>958</F4><F5>Breite Strasse 28
Dusseldorf, Germany</F5></Row>')
,(90208, 51, 570, N'<Row><F1>8490</F1><F2>DUS3</F2><F3>Project Vault </F3><F4>959</F4><F5>Breite Strasse 30-34
Dusseldorf, Germany</F5></Row>')
,(90209, 51, 571, N'<Row><F1>8490</F1><F2>DUS3</F2><F3>Project Vault </F3><F4>968</F4><F5>Koenigsallee 45
Dusseldorf, Germany</F5></Row>')
,(90210, 51, 572, N'<Row><F1>8490</F1><F2>DUS3</F2><F3>Project Vault </F3><F4>969</F4><F5>Koenigsallee 47
Dusseldorf, Germany</F5></Row>')
,(90211, 51, 573, N'<Row><F1>8490</F1><F2>DUS3</F2><F3>Project Vault </F3><F4>970</F4><F5>Koenigsallee 49-51
Dusseldorf, Germany</F5></Row>')
,(90212, 51, 574, N'<Row><F1>8490</F1><F2>DUS3</F2><F3>Project Vault </F3><F4>971</F4><F5>Koenigsallee 53
Dusseldorf, Germany</F5></Row>')
,(90213, 51, 575, N'<Row><F1>8490</F1><F2>DUS3</F2><F3>Project Vault </F3><F4>972</F4><F5>Koenigsallee 55
Dusseldorf, Germany</F5></Row>')
,(90214, 51, 576, N'<Row><F1>11045</F1><F2>EBF4</F2><F3>85 Broad St. (Project Sachs)</F3><F4>1019</F4><F5>85 Broad St
New York, NY</F5></Row>')
,(90215, 51, 577, N'<Row><F1>9210</F1><F2>ECC3</F2><F3>El Con Center</F3><F4>559</F4><F5>El Con Center
3601 E Broadway Blvd
Tucson,  AZ</F5></Row>')
,(90216, 51, 578, N'<Row><F1>9338</F1><F2>EDG3</F2><F3>Edgewater Business Park</F3><F4>412</F4><F5>260 Littlefield Ave
San Francisco, CA</F5></Row>')
,(90217, 51, 579, N'<Row><F1>9338</F1><F2>EDG3</F2><F3>Edgewater Business Park</F3><F4>420</F4><F5>270 Littlefield Ave
San Francisco, CA</F5></Row>')
,(90218, 51, 580, N'<Row><F1>9338</F1><F2>EDG3</F2><F3>Edgewater Business Park</F3><F4>428</F4><F5>280 Utah Ave
San Francisco, CA</F5></Row>')
,(90219, 51, 581, N'<Row><F1>9338</F1><F2>EDG3</F2><F3>Edgewater Business Park</F3><F4>438</F4><F5>290 Utah Ave
San Francisco, CA</F5></Row>')
,(90220, 51, 582, N'<Row><F1>9338</F1><F2>EDG3</F2><F3>Edgewater Business Park</F3><F4>458</F4><F5>300 Utah Ave
San Francisco, CA</F5></Row>')
,(90221, 51, 583, N'<Row><F1>9338</F1><F2>EDG3</F2><F3>Edgewater Business Park</F3><F4>488</F4><F5>310 Utah Ave
San Francisco, CA</F5></Row>')
,(90222, 51, 584, N'<Row><F1>10258</F1><F2>EDG4</F2><F3>Edgewater Business Park (Financing)</F3><F4>412</F4><F5>260 Littlefield Ave
San Francisco, CA</F5></Row>')
,(90223, 51, 585, N'<Row><F1>10258</F1><F2>EDG4</F2><F3>Edgewater Business Park (Financing)</F3><F4>420</F4><F5>270 Littlefield Ave
San Francisco, CA</F5></Row>')
,(90224, 51, 586, N'<Row><F1>10258</F1><F2>EDG4</F2><F3>Edgewater Business Park (Financing)</F3><F4>428</F4><F5>280 Utah Ave
San Francisco, CA</F5></Row>')
,(90225, 51, 587, N'<Row><F1>10258</F1><F2>EDG4</F2><F3>Edgewater Business Park (Financing)</F3><F4>438</F4><F5>290 Utah Ave
San Francisco, CA</F5></Row>')
,(90226, 51, 588, N'<Row><F1>10258</F1><F2>EDG4</F2><F3>Edgewater Business Park (Financing)</F3><F4>458</F4><F5>300 Utah Ave
San Francisco, CA</F5></Row>')
,(90227, 51, 589, N'<Row><F1>10258</F1><F2>EDG4</F2><F3>Edgewater Business Park (Financing)</F3><F4>488</F4><F5>310 Utah Ave
San Francisco, CA</F5></Row>')
,(90228, 51, 590, N'<Row><F1>8631</F1><F2>EEE3</F2><F3>888 Brannan</F3><F4>903</F4><F5>888 Brannan St
San Francisco, CA</F5></Row>')
,(90229, 51, 591, N'<Row><F1>10227</F1><F2>EEF4</F2><F3>888 Brannan (Financing)</F3><F4>903</F4><F5>888 Brannan St
San Francisco, CA</F5></Row>')
,(90230, 51, 592, N'<Row><F1>10263</F1><F2>EFS4</F2><F3>888 First Street, NE - FERC HQ</F3><F4>1032</F4><F5>888 First St NE
Washington, DC</F5></Row>')
,(90231, 51, 593, N'<Row><F1>10231</F1><F2>ELL4</F2><F3>1620 L Street, NW</F3><F4>233</F4><F5>1620 L St NW
Washington, DC</F5></Row>')
,(90232, 51, 594, N'<Row><F1>11222</F1><F2>EMB4</F2><F3>Embarcadero Square (Financing)</F3><F4>1255</F4><F5>560 Davis St
San Francisco, CA</F5></Row>')
,(90233, 51, 595, N'<Row><F1>11222</F1><F2>EMB4</F2><F3>Embarcadero Square (Financing)</F3><F4>1256</F4><F5>650 Davis St
San Francisco, CA</F5></Row>')
,(90234, 51, 596, N'<Row><F1>11222</F1><F2>EMB4</F2><F3>Embarcadero Square (Financing)</F3><F4>1257</F4><F5>75 Broadway
San Francisco, CA</F5></Row>')
,(90235, 51, 597, N'<Row><F1>9835</F1><F2>EMS3</F2><F3>Empire Stores (55 Water Street)</F3><F4>1129</F4><F5>Empire Stores
55 Water St
Brooklyn, NY</F5></Row>')
,(90236, 51, 598, N'<Row><F1>9139</F1><F2>EOS3</F2><F3>1107 Broadway Retail Condominium </F3><F4>105</F4><F5>1107 Broadway
New York, NY</F5></Row>')
,(90237, 51, 599, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>55</F4><F5>1025 Westchester Ave
White Plains, NY</F5></Row>')
,(90238, 51, 600, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>57</F4><F5>103 Corporate Park Dr
Harrison, NY</F5></Row>')
,(90239, 51, 601, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>68</F4><F5>105 Corporate Park Dr
Harrison, NY</F5></Row>')
,(90240, 51, 602, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>75</F4><F5>106 Corporate Park Dr
White Plains, NY</F5></Row>')
,(90241, 51, 603, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>81</F4><F5>108 Corporate Park Dr
White Plains, NY</F5></Row>')
,(90242, 51, 604, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>95</F4><F5>110 Corporate Park Dr
Harrison, NY</F5></Row>')
,(90243, 51, 605, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>398</F4><F5>2500 Westchester Ave
Harrison, NY</F5></Row>')
,(90244, 51, 606, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>421</F4><F5>2700 Westchester Ave
Harrison, NY</F5></Row>')
,(90245, 51, 607, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>814</F4><F5>701 Westchester Ave
White Plains, NY</F5></Row>')
,(90246, 51, 608, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>817</F4><F5>707 Westchester Ave
White Plains, NY</F5></Row>')
,(90247, 51, 609, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>819</F4><F5>709 Westchester Ave
White Plains, NY</F5></Row>')
,(90248, 51, 610, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>825</F4><F5>711 Westchester Ave
White Plains, NY</F5></Row>')
,(90249, 51, 611, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>848</F4><F5>777 Westchester Ave
White Plains, NY</F5></Row>')
,(90250, 51, 612, N'<Row><F1>9429</F1><F2>EPF3</F2><F3>The Exchange Portfolio Financing </F3><F4>928</F4><F5>925 Westchester Ave
White Plains, NY</F5></Row>')
,(90251, 51, 613, N'<Row><F1>9502</F1><F2>ERP3</F2><F3>880 Ridder Park Drive </F3><F4>900</F4><F5>880 Ridder Park Dr
San Jose, CA</F5></Row>')
,(90252, 51, 614, N'<Row><F1>10295</F1><F2>ETV4</F2><F3>Eaton Vance Chantilly Portfolio</F3><F4>204</F4><F5>Liberty Center I
14660 Lee Rd
Chantilly, VA</F5></Row>')
,(90253, 51, 615, N'<Row><F1>10295</F1><F2>ETV4</F2><F3>Eaton Vance Chantilly Portfolio</F3><F4>205</F4><F5>Liberty Center III
14668 Lee Rd
Chantilly, VA</F5></Row>')
,(90254, 51, 616, N'<Row><F1>10295</F1><F2>ETV4</F2><F3>Eaton Vance Chantilly Portfolio</F3><F4>206</F4><F5>Liberty Center II
14672 Lee Rd
Chantilly, VA</F5></Row>')
,(90255, 51, 617, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>55</F4><F5>1025 Westchester Ave
White Plains, NY</F5></Row>')
,(90256, 51, 618, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>57</F4><F5>103 Corporate Park Dr
Harrison, NY</F5></Row>')
,(90257, 51, 619, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>68</F4><F5>105 Corporate Park Dr
Harrison, NY</F5></Row>')
,(90258, 51, 620, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>75</F4><F5>106 Corporate Park Dr
White Plains, NY</F5></Row>')
,(90259, 51, 621, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>81</F4><F5>108 Corporate Park Dr
White Plains, NY</F5></Row>')
,(90260, 51, 622, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>95</F4><F5>110 Corporate Park Dr
Harrison, NY</F5></Row>')
,(90261, 51, 623, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>398</F4><F5>2500 Westchester Ave
Harrison, NY</F5></Row>')
,(90262, 51, 624, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>421</F4><F5>2700 Westchester Ave
Harrison, NY</F5></Row>')
,(90263, 51, 625, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>814</F4><F5>701 Westchester Ave
White Plains, NY</F5></Row>')
,(90264, 51, 626, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>817</F4><F5>707 Westchester Ave
White Plains, NY</F5></Row>')
,(90265, 51, 627, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>819</F4><F5>709 Westchester Ave
White Plains, NY</F5></Row>')
,(90266, 51, 628, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>825</F4><F5>711 Westchester Ave
White Plains, NY</F5></Row>')
,(90267, 51, 629, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>848</F4><F5>777 Westchester Ave
White Plains, NY</F5></Row>')
,(90268, 51, 630, N'<Row><F1>9270</F1><F2>EXP3</F2><F3>Exchange Portfolio</F3><F4>928</F4><F5>925 Westchester Ave
White Plains, NY</F5></Row>')
,(90269, 51, 631, N'<Row><F1>9160</F1><F2>FAL3</F2><F3>Project Falcon (11-15 Grosvenor Crescent)</F3><F4>1238</F4><F5>11-15 Grosvenor Crescent
Belgravia, London SW1X 7EE, UK</F5></Row>')
,(90270, 51, 632, N'<Row><F1>9662</F1><F2>FAV3</F2><F3>800 5th Avenue </F3><F4>1030</F4><F5>800 5th Ave
Seattle, WA</F5></Row>')
,(90271, 51, 633, N'<Row><F1>10151</F1><F2>FBC4</F2><F3>Freeway Business Center</F3><F4>1133</F4><F5>Freeway Business Center
1500 Hughes Way
Long Beach, CA</F5></Row>')
,(90272, 51, 634, N'<Row><F1>10304</F1><F2>FCF4</F2><F3>40 Court Street</F3><F4>595</F4><F5>40 Court St
Boston, MA</F5></Row>')
,(90273, 51, 635, N'<Row><F1>8558</F1><F2>FCP4</F2><F3>Fountainhead Corporate Park</F3><F4>1117</F4><F5>1501 W Fountainhead Pkwy
Tempe, AZ</F5></Row>')
,(90274, 51, 636, N'<Row><F1>8558</F1><F2>FCP4</F2><F3>Fountainhead Corporate Park</F3><F4>1118</F4><F5>1540 W Fountainhead Pkwy
Tempe, AZ</F5></Row>')
,(90275, 51, 637, N'<Row><F1>8558</F1><F2>FCP4</F2><F3>Fountainhead Corporate Park</F3><F4>1119</F4><F5>1560 W Fountainhead Pkwy
Tempe, AZ</F5></Row>')
,(90276, 51, 638, N'<Row><F1>8558</F1><F2>FCP4</F2><F3>Fountainhead Corporate Park</F3><F4>1120</F4><F5>1620 W Fountainhead Pkwy
Tempe, AZ</F5></Row>')
,(90277, 51, 639, N'<Row><F1>8558</F1><F2>FCP4</F2><F3>Fountainhead Corporate Park</F3><F4>1121</F4><F5>S Plaza Dr
Tempe, AZ</F5></Row>')
,(90278, 51, 640, N'<Row><F1>9863</F1><F2>FCT3</F2><F3>40 Court Street </F3><F4>595</F4><F5>40 Court St
Boston, MA</F5></Row>')
,(90279, 51, 641, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>18</F4><F5>Family Dollar
100 East Fisher Ave
Sabinal, TX</F5></Row>')
,(90280, 51, 642, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>25</F4><F5>Family Dollar
100 N Dr. MLK Jr. Blvd
Grenada, MS</F5></Row>')
,(90281, 51, 643, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>48</F4><F5>Family Dollar
1015 Jefferson St
Nashville, TN</F5></Row>')
,(90282, 51, 644, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>58</F4><F5>Family Dollar
103 Maysville Rd
Millersburg, KY</F5></Row>')
,(90283, 51, 645, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>59</F4><F5>Family Dollar
103 Nueces St
Camp Wood, TX</F5></Row>')
,(90284, 51, 646, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>63</F4><F5>Family Dollar
104 W Wetherbee Rd
Orlando, FL</F5></Row>')
,(90285, 51, 647, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>78</F4><F5>Family Dollar
107 E Musquiz Dr
Fort Davis, TX</F5></Row>')
,(90286, 51, 648, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>102</F4><F5>Family Dollar
1103 Richland Avenue E
Aiken, SC</F5></Row>')
,(90287, 51, 649, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>106</F4><F5>Family Dollar
111 East Kennedy Blvd
Eatonville, FL</F5></Row>')
,(90288, 51, 650, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>110</F4><F5>Family Dollar
1110 Roxboro St
Haw River, NC</F5></Row>')
,(90289, 51, 651, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>116</F4><F5>Family Dollar
1125 Wayne Ave
Dayton, OH</F5></Row>')
,(90290, 51, 652, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>118</F4><F5>Family Dollar
1131 W Peace St
Canton, MS</F5></Row>')
,(90291, 51, 653, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>127</F4><F5>Family Dollar
117 W Church St
Lexington, TN</F5></Row>')
,(90292, 51, 654, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>131</F4><F5>Family Dollar
119 N Main St
Madisonville, KY</F5></Row>')
,(90293, 51, 655, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>146</F4><F5>Family Dollar
12420 Conant St
Detroit, MI</F5></Row>')
,(90294, 51, 656, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>155</F4><F5>Family Dollar
130 Main St
Hamilton, OH</F5></Row>')
,(90295, 51, 657, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>159</F4><F5>Family Dollar
1306 W Main St
Mitchell, IN</F5></Row>')
,(90296, 51, 658, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>167</F4><F5>Family Dollar
1316 S Main St
Greenwood, SC</F5></Row>')
,(90297, 51, 659, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>189</F4><F5>Family Dollar
1400 4th St
Westwego, LA</F5></Row>')
,(90298, 51, 660, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>193</F4><F5>Family Dollar
1403 E Gentry Pkwy
Tyler, TX</F5></Row>')
,(90299, 51, 661, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>197</F4><F5>Family Dollar
1412 Main St
Columbus, MS</F5></Row>')
,(90300, 51, 662, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>200</F4><F5>Family Dollar
144 E Park Ave
Drew, MS</F5></Row>')
,(90301, 51, 663, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>217</F4><F5>Family Dollar
15013 N US Highway 83
La Pryor, TX</F5></Row>')
,(90302, 51, 664, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>218</F4><F5>Family Dollar
1505 Anderson Dr
Williamston, SC</F5></Row>')
,(90303, 51, 665, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>221</F4><F5>Family Dollar
1513 Charlotte Hwy
Lancaster, SC</F5></Row>')
,(90304, 51, 666, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>226</F4><F5>Family Dollar
155 W Park Ave
Ash Fork, AZ</F5></Row>')
,(90305, 51, 667, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>237</F4><F5>Family Dollar
1628 MLK Jr. Pkwy
Griffin, GA</F5></Row>')
,(90306, 51, 668, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>244</F4><F5>Family Dollar
1683 Appling Rd
Cordova, TN</F5></Row>')
,(90307, 51, 669, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>252</F4><F5>Family Dollar
1707 Simpson Hwy
Mendenhall, MS</F5></Row>')
,(90308, 51, 670, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>277</F4><F5>Family Dollar
1809 Jefferson Davis Hwy
Camden, SC</F5></Row>')
,(90309, 51, 671, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>282</F4><F5>Family Dollar
18215 N US Highway 301
Citra, FL</F5></Row>')
,(90310, 51, 672, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>288</F4><F5>Family Dollar
18815 Highway 231
Fountain, FL</F5></Row>')
,(90311, 51, 673, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>297</F4><F5>Family Dollar
1932 E Magnolia Ave
Knoxville, TN</F5></Row>')
,(90312, 51, 674, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>326</F4><F5>Family Dollar
203 North Main St
Boiling Springs, NC</F5></Row>')
,(90313, 51, 675, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>329</F4><F5>Family Dollar
204 11th Ave
Nampa, ID</F5></Row>')
,(90314, 51, 676, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>330</F4><F5>Family Dollar
2041 American Blvd
Orlando, FL</F5></Row>')
,(90315, 51, 677, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>338</F4><F5>Family Dollar
209 E Main St
Ore City, TX</F5></Row>')
,(90316, 51, 678, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>344</F4><F5>Family Dollar
214 Kirkland St
Abbeville, AL</F5></Row>')
,(90317, 51, 679, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>348</F4><F5>Family Dollar
21669 Whyte Hardee Blvd
Hardeeville, SC</F5></Row>')
,(90318, 51, 680, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>349</F4><F5>Family Dollar
2170 Bailey Rd
Mulberry, FL</F5></Row>')
,(90319, 51, 681, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>350</F4><F5>Family Dollar
2178 Campbellton Rd SW
Atlanta, GA</F5></Row>')
,(90320, 51, 682, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>361</F4><F5>Family Dollar
2204 Maple Ave
Burlington, NC</F5></Row>')
,(90321, 51, 683, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>381</F4><F5>Family Dollar
2342 Barksdale Blvd
Bossier City, LA</F5></Row>')
,(90322, 51, 684, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>382</F4><F5>Family Dollar
2348 N Fayetteville St
Asheboro, NC</F5></Row>')
,(90323, 51, 685, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>387</F4><F5>Family Dollar
2445 Al Hwy 202
Anniston, AL</F5></Row>')
,(90324, 51, 686, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>393</F4><F5>Family Dollar
2467 Kershaw Camden Hwy
Lancaster (Elgin), SC</F5></Row>')
,(90325, 51, 687, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>395</F4><F5>Family Dollar
250 S Main St
Woodruff, SC</F5></Row>')
,(90326, 51, 688, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>400</F4><F5>Family Dollar
2515 Stone Station Rd
Roebuck, SC</F5></Row>')
,(90327, 51, 689, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>403</F4><F5>Family Dollar
2548 N 32nd St
Phoenix, AZ</F5></Row>')
,(90328, 51, 690, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>418</F4><F5>Family Dollar
2681 Ne 10th
Oklahoma City, OK</F5></Row>')
,(90329, 51, 691, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>432</F4><F5>Family Dollar
2826 W Silver Springs Blvd
Ocala, FL</F5></Row>')
,(90330, 51, 692, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>436</F4><F5>Family Dollar
29 Colonel Hollow Rd
Rockholds, KY</F5></Row>')
,(90331, 51, 693, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>441</F4><F5>Family Dollar
2932 Highway 90 W
Avondale, LA</F5></Row>')
,(90332, 51, 694, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>464</F4><F5>Family Dollar
3002 Shurling Dr
Macon, GA</F5></Row>')
,(90333, 51, 695, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>468</F4><F5>Family Dollar
301 S Main St
La Feria, TX</F5></Row>')
,(90334, 51, 696, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>471</F4><F5>Family Dollar
3013 N Chadbourne St
San Angelo, TX</F5></Row>')
,(90335, 51, 697, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>477</F4><F5>Family Dollar
304 State Highway 185 N
Seadrift, TX</F5></Row>')
,(90336, 51, 698, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>478</F4><F5>Family Dollar
305 S Alamo St
Refugio, TX</F5></Row>')
,(90337, 51, 699, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>485</F4><F5>Family Dollar
308 W Main St
Hallsville, TX</F5></Row>')
,(90338, 51, 700, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>486</F4><F5>Family Dollar
3082 Cypress Gardens Rd
Winter Haven, FL</F5></Row>')
,(90339, 51, 701, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>489</F4><F5>Family Dollar
310 W Swannanoa Ave
Liberty, NC</F5></Row>')
,(90340, 51, 702, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>490</F4><F5>Family Dollar
3100 E Business Hwy 83
Donna, TX</F5></Row>')
,(90341, 51, 703, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>492</F4><F5>Family Dollar
3107 Springs Rd NE
Hickory, NC</F5></Row>')
,(90342, 51, 704, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>514</F4><F5>Family Dollar
3212 Mobile Hwy
Montgomery, AL</F5></Row>')
,(90343, 51, 705, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>524</F4><F5>Family Dollar
3250 W Indian School Rd
Phoenix, AZ</F5></Row>')
,(90344, 51, 706, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>529</F4><F5>Family Dollar
3312 East Main Ave
Alton, TX</F5></Row>')
,(90345, 51, 707, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>532</F4><F5>Family Dollar
336 Main St
Acadia, LA</F5></Row>')
,(90346, 51, 708, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>544</F4><F5>Family Dollar
34224 US 98
Lillian, AL</F5></Row>')
,(90347, 51, 709, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>545</F4><F5>Family Dollar
3435 Highway 14
Millbrook, AL</F5></Row>')
,(90348, 51, 710, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>546</F4><F5>Family Dollar
3454 Baker Rd
Acworth, GA</F5></Row>')
,(90349, 51, 711, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>547</F4><F5>Family Dollar
3485 N Lecanto Hwy
Beverly Hills, FL</F5></Row>')
,(90350, 51, 712, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>562</F4><F5>Family Dollar
3618 S Newcastle Rd
Oklahoma City, OK</F5></Row>')
,(90351, 51, 713, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>568</F4><F5>Family Dollar
3707 N Dixie Hwy
Monroe, MI</F5></Row>')
,(90352, 51, 714, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>570</F4><F5>Family Dollar
3723 US Highway 29
Danville, VA</F5></Row>')
,(90353, 51, 715, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>577</F4><F5>Family Dollar
379726 Arizona 75
Duncan, AZ</F5></Row>')
,(90354, 51, 716, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>582</F4><F5>Family Dollar
3812 N 9th Ave
Pensacola, FL</F5></Row>')
,(90355, 51, 717, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>592</F4><F5>Family Dollar
395 John R Junkin Dr
Natchez, MS</F5></Row>')
,(90356, 51, 718, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>603</F4><F5>Family Dollar
401 South Church Ave
Louisville, MS</F5></Row>')
,(90357, 51, 719, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>611</F4><F5>Family Dollar
408 N Broad St
Mobile, AL</F5></Row>')
,(90358, 51, 720, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>614</F4><F5>Family Dollar
410 W Forgey
Blooming Grove, TX</F5></Row>')
,(90359, 51, 721, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>622</F4><F5>Family Dollar
42 S Dean Rd
Orlando, FL</F5></Row>')
,(90360, 51, 722, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>623</F4><F5>Family Dollar
420 Person St
Fayetteville, NC</F5></Row>')
,(90361, 51, 723, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>625</F4><F5>Family Dollar
424 N West Ave
El Dorado, AR</F5></Row>')
,(90362, 51, 724, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>627</F4><F5>Family Dollar
425 Gordon Ave
Bowling Green, KY</F5></Row>')
,(90363, 51, 725, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>630</F4><F5>Family Dollar
4275 US Highway 68
Golden Valley, AZ</F5></Row>')
,(90364, 51, 726, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>637</F4><F5>Family Dollar
4380 Moncrief Rd
Jacksonville, FL</F5></Row>')
,(90365, 51, 727, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>642</F4><F5>Family Dollar
4405 W Aloha Dr
Diamond Head, MS</F5></Row>')
,(90366, 51, 728, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>650</F4><F5>Family Dollar
451 S High St
Cortland, OH</F5></Row>')
,(90367, 51, 729, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>651</F4><F5>Family Dollar
4515 Avenue O
Fort Madison, IA</F5></Row>')
,(90368, 51, 730, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>653</F4><F5>Family Dollar
4601 E Truman Rd
Kansas City, MO</F5></Row>')
,(90369, 51, 731, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>654</F4><F5>Family Dollar
4603 S 23rd St
Mcallen, TX</F5></Row>')
,(90370, 51, 732, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>656</F4><F5>Family Dollar
466 Bypass Rd
Brandenburg, KY</F5></Row>')
,(90371, 51, 733, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>677</F4><F5>Family Dollar
5 W Big Chino Rd
Paulden, AZ</F5></Row>')
,(90372, 51, 734, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>686</F4><F5>Family Dollar
5000 Union Blvd
St Louis, MO</F5></Row>')
,(90373, 51, 735, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>689</F4><F5>Family Dollar
501 Tahoka Rd
Brownfield, TX</F5></Row>')
,(90374, 51, 736, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>691</F4><F5>Family Dollar
5012 US Highway 92 W
Auburndale, FL</F5></Row>')
,(90375, 51, 737, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>694</F4><F5>Family Dollar
5025 Nc Hwy 90 E
Hiddenite, NC</F5></Row>')
,(90376, 51, 738, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>710</F4><F5>Family Dollar
521 Rodeo Rd
N Platte, NE</F5></Row>')
,(90377, 51, 739, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>713</F4><F5>Family Dollar
5260 S 3rd St
Memphis, TN</F5></Row>')
,(90378, 51, 740, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>719</F4><F5>Family Dollar
5355 Elvis Presley Blvd
Memphis, TN</F5></Row>')
,(90379, 51, 741, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>721</F4><F5>Family Dollar
5419 Saufley Field Rd
Pensacola, FL</F5></Row>')
,(90380, 51, 742, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>732</F4><F5>Family Dollar
567 Cobb Pkwy N
Marietta, GA</F5></Row>')
,(90381, 51, 743, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>754</F4><F5>Family Dollar
602 N St Marys St
Falfurrias, TX</F5></Row>')
,(90382, 51, 744, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>757</F4><F5>Family Dollar
607 E Moody Blvd
Bunnell, FL</F5></Row>')
,(90383, 51, 745, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>765</F4><F5>Family Dollar
613 Rifle Range Rd S
Winter Haven, FL</F5></Row>')
,(90384, 51, 746, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>767</F4><F5>Family Dollar
621 South Pearson Rd
Pearl, MS</F5></Row>')
,(90385, 51, 747, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>769</F4><F5>Family Dollar
6290 W Third St
Dayton, OH</F5></Row>')
,(90386, 51, 748, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>774</F4><F5>Family Dollar
6480 Winchester Pike
Canal Winchester, OH</F5></Row>')
,(90387, 51, 749, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>787</F4><F5>Family Dollar
6541 Montgomery Rd
Cincinnati, OH</F5></Row>')
,(90388, 51, 750, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>790</F4><F5>Family Dollar
6634 Bristol Hwy
Piney Flats, TN</F5></Row>')
,(90389, 51, 751, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>813</F4><F5>Family Dollar
700 Sutton St
Sonora, TX</F5></Row>')
,(90390, 51, 752, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>827</F4><F5>Family Dollar
715 Broadway
Rockford, IL</F5></Row>')
,(90391, 51, 753, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>835</F4><F5>Family Dollar
7255 Booker T Washington Hwy
Wirtz, VA</F5></Row>')
,(90392, 51, 754, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>844</F4><F5>Family Dollar
7567 State Highway M123
Newberry, MI</F5></Row>')
,(90393, 51, 755, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>846</F4><F5>Family Dollar
771 Nicholson St
Richland, GA</F5></Row>')
,(90394, 51, 756, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>849</F4><F5>Family Dollar
7790 Dorchester Rd
North Charleston, SC</F5></Row>')
,(90395, 51, 757, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>855</F4><F5>Family Dollar
800 E Main St
Philadelphia, MS</F5></Row>')
,(90396, 51, 758, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>856</F4><F5>Family Dollar
8000 Michigan Ave
Detroit, MI</F5></Row>')
,(90397, 51, 759, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>863</F4><F5>Family Dollar
8010 Preston Hwy
Louisville, KY</F5></Row>')
,(90398, 51, 760, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>866</F4><F5>Family Dollar
803 E Jackson Rd
Union, MS</F5></Row>')
,(90399, 51, 761, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>870</F4><F5>Family Dollar
811 N 1st Ave
Durant, OK</F5></Row>')
,(90400, 51, 762, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>871</F4><F5>Family Dollar
811 S Burlington Ave
Hastings, NE</F5></Row>')
,(90401, 51, 763, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>889</F4><F5>Family Dollar
85 Park Ave
Lindale, GA</F5></Row>')
,(90402, 51, 764, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>905</F4><F5>Family Dollar
8909 Two Notch Rd
Columbia, SC</F5></Row>')
,(90403, 51, 765, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>906</F4><F5>Family Dollar
8920 S Highway 95
Mohave Valley, AZ</F5></Row>')
,(90404, 51, 766, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>907</F4><F5>Family Dollar
8973 University Blvd
North Charleston, SC</F5></Row>')
,(90405, 51, 767, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>913</F4><F5>Family Dollar
9018 US 82
Alapaha, GA</F5></Row>')
,(90406, 51, 768, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>914</F4><F5>Family Dollar
902 S Pierce St
Alma, GA</F5></Row>')
,(90407, 51, 769, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>916</F4><F5>Family Dollar
9044 N State Highway 16
Poteet, TX</F5></Row>')
,(90408, 51, 770, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>933</F4><F5>Family Dollar
936 S Woodlawn
Wichita, KS</F5></Row>')
,(90409, 51, 771, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>938</F4><F5>Family Dollar
9605 Fm 1732
Brownsville, TX</F5></Row>')
,(90410, 51, 772, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>940</F4><F5>Family Dollar
9610 S Orange Ave
Orlando, FL</F5></Row>')
,(90411, 51, 773, N'<Row><F1>10154</F1><F2>FDR4</F2><F3>Family Dollar Sale-Leaseback Review</F3><F4>953</F4><F5>Family Dollar
9976 Wolfe St
Caledonia, MS</F5></Row>')
,(90412, 51, 774, N'<Row><F1>10189</F1><F2>FDY4</F2><F3>405 Howard</F3><F4>608</F4><F5>405 Howard St
San Francisco, CA</F5></Row>')
,(90413, 51, 775, N'<Row><F1>9861</F1><F2>FET3</F2><F3>15 East 26th St.</F3><F4>1016</F4><F5>15 E 26th St
New York, NY</F5></Row>')
,(90414, 51, 776, N'<Row><F1>10225</F1><F2>FFM4</F2><F3>1455 Market Street</F3><F4>1308</F4><F5>1455 Market St
San Francisco, CA</F5></Row>')
,(90415, 51, 777, N'<Row><F1>9530</F1><F2>FFP3</F2><F3>450 Park Avenue</F3><F4>647</F4><F5>450 Park Ave
New York, NY</F5></Row>')
,(90416, 51, 778, N'<Row><F1>9372</F1><F2>FFS3</F2><F3>55 Second Street </F3><F4>724</F4><F5>55 Second St
San Francisco, CA</F5></Row>')
,(90417, 51, 779, N'<Row><F1>10634</F1><F2>FFT4</F2><F3>515 North State Street</F3><F4>1300</F4><F5>515 N State St
Chicago, IL</F5></Row>')
,(90418, 51, 780, N'<Row><F1>10691</F1><F2>FGR4</F2><F3>801 Tower</F3><F4>861</F4><F5>801 Tower
801 S Figueroa St
Los Angeles, CA</F5></Row>')
,(90419, 51, 781, N'<Row><F1>10569</F1><F2>FIF4</F2><F3>Fifth Third Center (Financing)</F3><F4>624</F4><F5>Fifth Third Center
424 Church St
Nashville, TN</F5></Row>')
,(90420, 51, 782, N'<Row><F1>10333</F1><F2>FIG4</F2><F3>Hotel Figueroa </F3><F4>1142</F4><F5>Hotel Figueroa
939 S Figueroa St
Los Angeles, CA</F5></Row>')
,(90421, 51, 783, N'<Row><F1>8214</F1><F2>FIR3</F2><F3>First Tower (Mezz)</F3><F4>1131</F4><F5>First Tower
1 Place des Saisons
92400 Courbevoie, France</F5></Row>')
,(90422, 51, 784, N'<Row><F1>9900</F1><F2>FLS3</F2><F3>Flagler South Florida Office Collection </F3><F4>66</F4><F5>Flagler Station
10451 NW 117th Ave
Miami, FL</F5></Row>')
,(90423, 51, 785, N'<Row><F1>9900</F1><F2>FLS3</F2><F3>Flagler South Florida Office Collection </F3><F4>885</F4><F5>Doral Concourse
8400 NW 36th St
Doral, FL</F5></Row>')
,(90424, 51, 786, N'<Row><F1>10216</F1><F2>FOF4</F2><F3>Fair Oaks Plaza (Financing)</F3><F4>1088</F4><F5>Fair Oaks Plaza
11350 Random Hills Rd
Fairfax, VA</F5></Row>')
,(90425, 51, 787, N'<Row><F1>10035</F1><F2>FOP4</F2><F3>Fair Oaks Plaza </F3><F4>1088</F4><F5>Fair Oaks Plaza
11350 Random Hills Rd
Fairfax, VA</F5></Row>')
,(90426, 51, 788, N'<Row><F1>9884</F1><F2>FPP3</F2><F3>The Forum Peachtree Parkway </F3><F4>705</F4><F5>The Forum on Peachtree Parkway
5155 Peachtree Pkwy
Norcross, GA</F5></Row>')
,(90427, 51, 789, N'<Row><F1>10167</F1><F2>FPS4</F2><F3>Fairfield Inn &amp; Suites Penn Station</F3><F4>523</F4><F5>Fairfield Inn &amp; Suites New York Midtown Manhattan/Penn Station
325 W 33rd St
New York, NY</F5></Row>')
,(90428, 51, 790, N'<Row><F1>10717</F1><F2>FPT4</F2><F3>Freeport Outlet Centers </F3><F4>955</F4><F5>Freeport Alcochete
Avenida Euro 2004 2890-154
Alcochete, Portugal</F5></Row>')
,(90429, 51, 791, N'<Row><F1>10717</F1><F2>FPT4</F2><F3>Freeport Outlet Centers </F3><F4>961</F4><F5>Freeport Excalibur
Chvalovice 196 669 02
Chvalovice, Czech Republic</F5></Row>')
,(90430, 51, 792, N'<Row><F1>10717</F1><F2>FPT4</F2><F3>Freeport Outlet Centers </F3><F4>973</F4><F5>Freeport Kungsbacka
Kungsparksvägen 80 434 39
Kungsbacka, Sweden</F5></Row>')
,(90431, 51, 793, N'<Row><F1>6231</F1><F2>FRB1</F2><F3>541 Fairbanks </F3><F4>720</F4><F5>541 N Fairbanks Ct
Chicago, IL</F5></Row>')
,(90432, 51, 794, N'<Row><F1>10282</F1><F2>FRE4</F2><F3>Frenchgate Centre</F3><F4>1134</F4><F5>Frenchgate Centre
West Mall Frenchgate Centre
Doncaster, South Yorkshire DN1 1SR, UK</F5></Row>')
,(90433, 51, 795, N'<Row><F1>10537</F1><F2>FSB4</F2><F3>1460 Broadway</F3><F4>1037</F4><F5>1460 Broadway
New York, NY</F5></Row>')
,(90434, 51, 796, N'<Row><F1>9217</F1><F2>FSF3</F2><F3>Four Seasons Maui at Wailea </F3><F4>1132</F4><F5>Four Seasons Resort Maui at Wailea
3900 Wailea Alanui Dr
Wailea, HI</F5></Row>')
,(90435, 51, 797, N'<Row><F1>9971</F1><F2>FSF4</F2><F3>Fairmont San Francisco </F3><F4>1130</F4><F5>The Fairmont San Francisco
950 Mason St
San Francisco, CA</F5></Row>')
,(90436, 51, 798, N'<Row><F1>8288</F1><F2>FTB2</F2><F3>530 Broadway </F3><F4>714</F4><F5>530 Broadway
New York, NY</F5></Row>')
,(90437, 51, 799, N'<Row><F1>10120</F1><F2>FTC4</F2><F3>Fifth Third Center</F3><F4>624</F4><F5>Fifth Third Center
424 Church St
Nashville, TN</F5></Row>')
,(90438, 51, 800, N'<Row><F1>8338</F1><F2>FTS3</F2><F3>555 12th St</F3><F4>727</F4><F5>555 12th St NW
Washington, DC</F5></Row>')
,(90439, 51, 801, N'<Row><F1>9862</F1><F2>FWB3</F2><F3>575 Washington Boulevard</F3><F4>734</F4><F5>575 Washington Blvd
Jersey City, NJ</F5></Row>')
,(90440, 51, 802, N'<Row><F1>9442</F1><F2>FWF3</F2><F3>401 West 14th Street </F3><F4>604</F4><F5>401 W 14th St
New York, NY</F5></Row>')
,(90441, 51, 803, N'<Row><F1>10022</F1><F2>GAS4</F2><F3>Mobil Building</F3><F4>211</F4><F5>150 E 42nd St
New York, NY</F5></Row>')
,(90442, 51, 804, N'<Row><F1>9466</F1><F2>GCR3</F2><F3>The Gallery at Capitol Riverfront </F3><F4>113</F4><F5>The Gallery at Capitol Riverfront 
1111 New Jersey Avenue SE
Washington, DC</F5></Row>')
,(90443, 51, 805, N'<Row><F1>10091</F1><F2>GOH4</F2><F3>Gardens on Havana</F3><F4>72</F4><F5>The Gardens on Havana
10551 E Garden Dr
Aurora, CO</F5></Row>')
,(90444, 51, 806, N'<Row><F1>10408</F1><F2>GRE4</F2><F3>627 Greenwich Street </F3><F4>768</F4><F5>627 Greenwich St
New York, NY</F5></Row>')
,(90445, 51, 807, N'<Row><F1>10638</F1><F2>GRF4</F2><F3>33 Grosvenor (Financing)</F3><F4>526</F4><F5>33 Grosvenor Pl
London, UK</F5></Row>')
,(90446, 51, 808, N'<Row><F1>10839</F1><F2>GRN4</F2><F3>627 Greenwich Street (Financing) </F3><F4>768</F4><F5>627 Greenwich St
New York, NY</F5></Row>')
,(90447, 51, 809, N'<Row><F1>10301</F1><F2>GRO4</F2><F3>33 Grosvenor Place</F3><F4>526</F4><F5>33 Grosvenor Pl
London, UK</F5></Row>')
,(90448, 51, 810, N'<Row><F1>10679</F1><F2>GTF4</F2><F3>Georgetown Park (Financing)</F3><F4>517</F4><F5>Georgetown Park
3222 M Street NW
Washington, DC</F5></Row>')
,(90449, 51, 811, N'<Row><F1>10054</F1><F2>GTP4</F2><F3>The Shops at Georgetown Park</F3><F4>517</F4><F5>Georgetown Park
3222 M Street NW
Washington, DC</F5></Row>')
,(90450, 51, 812, N'<Row><F1>9228</F1><F2>HAL3</F2><F3>Hilton Arlington</F3><F4>937</F4><F5>Hilton Arlington
950 N Stafford St
Alexandria, VA</F5></Row>')
,(90451, 51, 813, N'<Row><F1>9887</F1><F2>HAM3</F2><F3>Hamburg Pavilion </F3><F4>305</F4><F5>Hamburg Pavilion
2308 Sir Barton Way
Lexington, KY</F5></Row>')
,(90452, 51, 814, N'<Row><F1>9221</F1><F2>HAR3</F2><F3>Project Tom (The Harpo Campus)</F3><F4>97</F4><F5>110 N Carpenter St
Chicago, IL</F5></Row>')
,(90453, 51, 815, N'<Row><F1>9221</F1><F2>HAR3</F2><F3>Project Tom (The Harpo Campus)</F3><F4>114</F4><F5>1115 W Washington Blvd
Chicago, IL</F5></Row>')
,(90454, 51, 816, N'<Row><F1>9221</F1><F2>HAR3</F2><F3>Project Tom (The Harpo Campus)</F3><F4>117</F4><F5>113 N May St
Chicago, IL</F5></Row>')
,(90455, 51, 817, N'<Row><F1>9221</F1><F2>HAR3</F2><F3>Project Tom (The Harpo Campus)</F3><F4>129</F4><F5>118-122 N Aberdeen St
Chicago, IL</F5></Row>')
,(90456, 51, 818, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1217</F4><F5>Hotel Russell
1-8 Russell Sq
London WC1B 5BE, UK</F5></Row>')
,(90457, 51, 819, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1218</F4><F5>Beaumont Estate
Burfield Road, Old Windsor
West Berkshire SL4 2JJ, UK</F5></Row>')
,(90458, 51, 820, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1219</F4><F5>The George Hotel
19-21 George St
Edinburgh EH2 2PB, UK</F5></Row>')
,(90459, 51, 821, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1220</F4><F5>Grand Connaught Rooms
61-63 Great Queen St
London WC2B 5DA, UK</F5></Row>')
,(90460, 51, 822, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1221</F4><F5>Grand Central Hotel
99 Gordon St
Glasgow City G1 3SF, UK</F5></Row>')
,(90461, 51, 823, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1222</F4><F5>St. David''s Hotel &amp; Spa 
Havannah St
Cardiff CF10 5SD, UK</F5></Row>')
,(90462, 51, 824, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1223</F4><F5>Royal York Hotel
Station Rd
North Yorkshire YO24 1AA, UK</F5></Row>')
,(90463, 51, 825, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1224</F4><F5>Palace Hotel
Oxford St
Manchester M60 7HA, UK</F5></Row>')
,(90464, 51, 826, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1225</F4><F5>Cranage Hall
Cranage Holmes Chapel, Byley Ln
Cranage, Crewe CW4 8EW, UK</F5></Row>')
,(90465, 51, 827, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1226</F4><F5>Wotton House
Guilford Rd
Dorking, Surrey RH5 6HS, UK</F5></Row>')
,(90466, 51, 828, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1227</F4><F5>Eastwood Hall
Mansfield Rd
Eastwood, Nottingham NG16 3SS, UK</F5></Row>')
,(90467, 51, 829, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1228</F4><F5>Selsdon Park Hotel
126 Addington Rd
South Croydon, Surrey CR2 8YA, UK</F5></Row>')
,(90468, 51, 830, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1229</F4><F5>Horwood House
Mursley Rd
Little Horwood, Milton Keynes MK17 0PH, UK</F5></Row>')
,(90469, 51, 831, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1230</F4><F5>St. John''s Hotel
651 Warwick Rd
Solihull, West Midlands B91 1AT, UK</F5></Row>')
,(90470, 51, 832, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1231</F4><F5>The Derbyshire Hotel
Carter Ln E
South Normanton, Alfreton DE55 2EH, UK</F5></Row>')
,(90471, 51, 833, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1232</F4><F5>Alexandra House
Whittingham Dr
Wroughton, Swindon SN4 0QJ, UK</F5></Row>')
,(90472, 51, 834, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1233</F4><F5>The Met Hotel
King St
Leeds, West Yorkshire LS1 2HQ, UK</F5></Row>')
,(90473, 51, 835, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1234</F4><F5>Ettington Chase
Banbury Rd
Ettington, Stratford-upon-Avon CV37 7NZ, UK</F5></Row>')
,(90474, 51, 836, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1235</F4><F5>Sedgebrook Hall
Chapel Brampton
Northampton NN6 8BD, UK</F5></Row>')
,(90475, 51, 837, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1236</F4><F5>Kenwood Hall
Kenwood Rd
Sheffield, South Yorkshire S7 1NQ, UK</F5></Row>')
,(90476, 51, 838, N'<Row><F1>10172</F1><F2>HAY4</F2><F3>Principal Hayley </F3><F4>1237</F4><F5>Hawkstone Park
Shrewsbury, Shropshire SY4 5UY, UK</F5></Row>')
,(90477, 51, 839, N'<Row><F1>8975</F1><F2>HCM3</F2><F3>Shoppes at Hotel Commonwealth </F3><F4>679</F4><F5>Hotel Commonwealth
500 Commonwealth Ave
Boston, MA</F5></Row>')
,(90478, 51, 840, N'<Row><F1>9895</F1><F2>HGL4</F2><F3>Hilton Glendale</F3><F4>29</F4><F5>Hilton Glendale
100 W. Glenoaks Blvd
Glendale, CA</F5></Row>')
,(90479, 51, 841, N'<Row><F1>9415</F1><F2>HHC3</F2><F3>Howard Hughes Center </F3><F4>755</F4><F5>Howard Hughes Center 
6060 Center Dr
Los Angeles, CA</F5></Row>')
,(90480, 51, 842, N'<Row><F1>9415</F1><F2>HHC3</F2><F3>Howard Hughes Center </F3><F4>758</F4><F5>Howard Hughes Center 
6080 Center Dr
Los Angeles, CA</F5></Row>')
,(90481, 51, 843, N'<Row><F1>9415</F1><F2>HHC3</F2><F3>Howard Hughes Center </F3><F4>760</F4><F5>Howard Hughes Center 
6100 Center Dr
Los Angeles, CA</F5></Row>')
,(90482, 51, 844, N'<Row><F1>9415</F1><F2>HHC3</F2><F3>Howard Hughes Center </F3><F4>789</F4><F5>Howard Hughes Center 
6601 Center Dr
Los Angeles, CA</F5></Row>')
,(90483, 51, 845, N'<Row><F1>9415</F1><F2>HHC3</F2><F3>Howard Hughes Center </F3><F4>792</F4><F5>Howard Hughes Center 
6701 Center Dr
Los Angeles, CA</F5></Row>')
,(90484, 51, 846, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>5</F4><F5>Residence Inn Harrisburg Carlisle
1 Hampton Ct
Carlisle, PA</F5></Row>')
,(90485, 51, 847, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>33</F4><F5>Element Ewing Princeton
1000 Sam Weinroth Rd
Ewing, NJ</F5></Row>')
,(90486, 51, 848, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>209</F4><F5>Residence Inn Philadelphia Langhorne
15 Cabot Blvd E
Langhorne, PA</F5></Row>')
,(90487, 51, 849, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>313</F4><F5>Hampton Inn Long Island Brookhaven
2000 N Ocean Ave
Farmingville, NY</F5></Row>')
,(90488, 51, 850, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>333</F4><F5>Holiday Inn Express Hauppauge Long Island
2050 Express Dr S
Hauppauge, NY</F5></Row>')
,(90489, 51, 851, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>413</F4><F5>Holiday Inn Express &amp; Suites King of Prussia
260 N Gulph Rd
King of Prussia, PA</F5></Row>')
,(90490, 51, 852, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>456</F4><F5>Inn at Wilmington
300 Rocky Run Pkwy
Wilmington, DE</F5></Row>')
,(90491, 51, 853, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>491</F4><F5>Holiday Inn Express Langhorne Oxford Valley
3101 Cabot Blvd W
Langhorne, PA</F5></Row>')
,(90492, 51, 854, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>509</F4><F5>Courtyard Wilmington Brandywine
320 Rocky Run Pkwy
Wilmington, DE</F5></Row>')
,(90493, 51, 855, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>558</F4><F5>Courtyard Ewing Hopewell
360 Scotch Rd
Ewing, NJ</F5></Row>')
,(90494, 51, 856, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>646</F4><F5>TownePlace Suites Harrisburg
450 Friendship Rd
Harrisburg, PA</F5></Row>')
,(90495, 51, 857, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>675</F4><F5>Courtyard Philadelphia Langhorne
5 Industrial Blvd
Langhorne, PA</F5></Row>')
,(90496, 51, 858, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>699</F4><F5>Hampton Inn &amp; Suites New Haven South West Haven
510 Saw Mill Rd
West Haven, CT</F5></Row>')
,(90497, 51, 859, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>715</F4><F5>Hyatt House Bridgewater
530 U.S. 22
Bridgewater, NJ</F5></Row>')
,(90498, 51, 860, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>759</F4><F5>Holiday Inn Express Hershey Harrisburg Area
610 Walton Ave
Hummelstown, PA</F5></Row>')
,(90499, 51, 861, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>842</F4><F5>Hampton Inn &amp; Suites Hershey
749 E Chocolate Ave
Hershey, PA</F5></Row>')
,(90500, 51, 862, N'<Row><F1>9347</F1><F2>HHP3</F2><F3>Hersha Select Service Hotel Portfolio </F3><F4>935</F4><F5>Hampton Inn &amp; Suites Smithfield
945 Douglas Pike
Smithfield, RI</F5></Row>')
,(90501, 51, 863, N'<Row><F1>9539</F1><F2>HIS3</F2><F3>Holiday Inn Soho</F3><F4>180</F4><F5>Holiday Inn New York-Soho
138 Lafayette St
New York, NY</F5></Row>')
,(90502, 51, 864, N'<Row><F1>9969</F1><F2>HKC4</F2><F3>Hawaii Kai Towne Center</F3><F4>530</F4><F5>Hawaii Kai Towne Center
333 Keahole St
Honolulu, HI</F5></Row>')
,(90503, 51, 865, N'<Row><F1>10879</F1><F2>HKF4</F2><F3>Harman Kardon Campus </F3><F4>1302</F4><F5>8510 Balboa Blvd
Los Angeles, CA</F5></Row>')
,(90504, 51, 866, N'<Row><F1>10879</F1><F2>HKF4</F2><F3>Harman Kardon Campus </F3><F4>1303</F4><F5>8550 Balboa Blvd
Los Angeles, CA</F5></Row>')
,(90505, 51, 867, N'<Row><F1>10879</F1><F2>HKF4</F2><F3>Harman Kardon Campus </F3><F4>1304</F4><F5>8400 Balboa Blvd
Los Angeles, CA</F5></Row>')
,(90506, 51, 868, N'<Row><F1>10879</F1><F2>HKF4</F2><F3>Harman Kardon Campus </F3><F4>1305</F4><F5>8500 Balboa Blvd
Los Angeles, CA</F5></Row>')
,(90507, 51, 869, N'<Row><F1>9945</F1><F2>HON4</F2><F3>Honda - 10 S Van Ness </F3><F4>16</F4><F5>10 S Van Ness
San Francisco, CA</F5></Row>')
,(90508, 51, 870, N'<Row><F1>10684</F1><F2>HOS4</F2><F3>Hall of the States Ground Lease</F3><F4>600</F4><F5>Hall of States Building
400-444 N Capitol St NW
Washington, DC</F5></Row>')
,(90509, 51, 871, N'<Row><F1>9500</F1><F2>HOT3</F2><F3>Hilton Alexandria Old Town </F3><F4>266</F4><F5>Hilton Alexandria Old Town 
1767 King St
Alexandria, VA</F5></Row>')
,(90510, 51, 872, N'<Row><F1>9501</F1><F2>HPC3</F2><F3>Hines Peninsula Collection </F3><F4>20</F4><F5>Bayside Corporate Center II
100 Foster City Blvd
Foster City, CA</F5></Row>')
,(90511, 51, 873, N'<Row><F1>9501</F1><F2>HPC3</F2><F3>Hines Peninsula Collection </F3><F4>96</F4><F5>Bayside Corporate Center I
110 Marsh Dr
Foster City, CA</F5></Row>')
,(90512, 51, 874, N'<Row><F1>9501</F1><F2>HPC3</F2><F3>Hines Peninsula Collection </F3><F4>520</F4><F5>323 Vintage Park Dr
Foster City, CA</F5></Row>')
,(90513, 51, 875, N'<Row><F1>9501</F1><F2>HPC3</F2><F3>Hines Peninsula Collection </F3><F4>555</F4><F5>353 Vintage Park Dr
Foster City, CA</F5></Row>')
,(90514, 51, 876, N'<Row><F1>9501</F1><F2>HPC3</F2><F3>Hines Peninsula Collection </F3><F4>563</F4><F5>363 Vintage Park Dr
Foster City, CA</F5></Row>')
,(90515, 51, 877, N'<Row><F1>9501</F1><F2>HPC3</F2><F3>Hines Peninsula Collection </F3><F4>571</F4><F5>373 Vintage Park Dr
Foster City, CA</F5></Row>')
,(90516, 51, 878, N'<Row><F1>9501</F1><F2>HPC3</F2><F3>Hines Peninsula Collection </F3><F4>576</F4><F5>378 Vintage Park Dr
Foster City, CA</F5></Row>')
,(90517, 51, 879, N'<Row><F1>9501</F1><F2>HPC3</F2><F3>Hines Peninsula Collection </F3><F4>584</F4><F5>383 Vintage Park Dr
Foster City, CA</F5></Row>')
,(90518, 51, 880, N'<Row><F1>9501</F1><F2>HPC3</F2><F3>Hines Peninsula Collection </F3><F4>591</F4><F5>393 Vintage Park Dr
Foster City, CA</F5></Row>')
,(90519, 51, 881, N'<Row><F1>10563</F1><F2>HPF4</F2><F3>Hamburg Pavilion (Financing)</F3><F4>305</F4><F5>Hamburg Pavilion
2308 Sir Barton Way
Lexington, KY</F5></Row>')
,(90520, 51, 882, N'<Row><F1>9006</F1><F2>HRR3</F2><F3>Harrogate Portfolio</F3><F4>1138</F4><F5>Kingsgate
Kingsgate Shopping Centre
Dunfermline KY12 7QU, UK</F5></Row>')
,(90521, 51, 883, N'<Row><F1>9006</F1><F2>HRR3</F2><F3>Harrogate Portfolio</F3><F4>1139</F4><F5>Vancouver Quarter Shopping Centre
24 Broad St
King''s Lynn PE30 1DP, UK</F5></Row>')
,(90522, 51, 884, N'<Row><F1>9006</F1><F2>HRR3</F2><F3>Harrogate Portfolio</F3><F4>1140</F4><F5>The Rushes Shopping Centre
The Rushes
Loughborough, Leicestershire LE11 5BE, UK</F5></Row>')
,(90523, 51, 885, N'<Row><F1>10428</F1><F2>HRS4</F2><F3>Hard Rock Hotel (Leased Fee)</F3><F4>1137</F4><F5>Hard Rock Hotel San Diego
207 5th Ave
San Diego, CA</F5></Row>')
,(90524, 51, 886, N'<Row><F1>8082</F1><F2>HRV2</F2><F3>Harvest Inn</F3><F4>6</F4><F5>Harvest Inn
1 Main St
St. Helena, CA</F5></Row>')
,(90525, 51, 887, N'<Row><F1>10818</F1><F2>HRY4</F2><F3>Hialeah Rail Yard </F3><F4>1292</F4><F5>Hialeah Rail Yard
6875 NW 58th St
Miami, FL</F5></Row>')
,(90526, 51, 888, N'<Row><F1>10847</F1><F2>HSF4</F2><F3>Hyatt Regency St. Louis at The Arch </F3><F4>503</F4><F5>Hyatt Regency St. Louis at the Arch
315 Chestnut St
St. Louis, MO</F5></Row>')
,(90527, 51, 889, N'<Row><F1>10507</F1><F2>HSJ4</F2><F3>Hyatt Place San Jose</F3><F4>1123</F4><F5>Hyatt Place San Jose/Downtown
282 S Almaden Blvd
San Jose, CA</F5></Row>')
,(90528, 51, 890, N'<Row><F1>9431</F1><F2>HSL4</F2><F3>Hyatt Regency St. Louis</F3><F4>503</F4><F5>Hyatt Regency St. Louis at the Arch
315 Chestnut St
St. Louis, MO</F5></Row>')
,(90529, 51, 891, N'<Row><F1>9468</F1><F2>HTP3</F2><F3>Heritage Plaza</F3><F4>111</F4><F5>Heritage Plaza
1111 Bagby St
Houston, TX</F5></Row>')
,(90530, 51, 892, N'<Row><F1>10468</F1><F2>HUB4</F2><F3>Project Hub</F3><F4>1136</F4><F5>45 E Freedom Way
Cincinnati, OH</F5></Row>')
,(90531, 51, 893, N'<Row><F1>9992</F1><F2>HUD4</F2><F3>330 Hudson Street</F3><F4>527</F4><F5>330 Hudson St
New York, NY</F5></Row>')
,(90532, 51, 894, N'<Row><F1>7895</F1><F2>HVT2</F2><F3>Hotel Vitale </F3><F4>852</F4><F5>Hotel Vitale 
8 Mission St
San Francisco, CA</F5></Row>')
,(90533, 51, 895, N'<Row><F1>10976</F1><F2>HYF4</F2><F3>Hyatt Place San Jose Financing</F3><F4>1123</F4><F5>Hyatt Place San Jose/Downtown
282 S Almaden Blvd
San Jose, CA</F5></Row>')
,(90534, 51, 896, N'<Row><F1>9591</F1><F2>IKW3</F2><F3>The Inn at Key West</F3><F4>543</F4><F5>The Inn at Key West
3420 N Roosevelt Blvd
Key West, FL</F5></Row>')
,(90535, 51, 897, N'<Row><F1>8335</F1><F2>IMH4</F2><F3>Intercontinental Mark Hopkins San Francisco</F3><F4>10</F4><F5>Intercontinental Mark Hopkins San Francisco
1 Nob Hill Pl
San Francisco, CA</F5></Row>')
,(90536, 51, 898, N'<Row><F1>10568</F1><F2>INF4</F2><F3>Inverness Corners &amp; Plaza (Financing) </F3><F4>22</F4><F5>Inverness Corners
100 Inverness Cors
Birmingham, AL</F5></Row>')
,(90537, 51, 899, N'<Row><F1>10568</F1><F2>INF4</F2><F3>Inverness Corners &amp; Plaza (Financing) </F3><F4>966</F4><F5>Inverness Plaza
Inverness Plaza
Birmingham, AL</F5></Row>')
,(90538, 51, 900, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>992</F4><F5>Grand Twin Towers West
234 Goodwin Crest Dr
Homewood, AL</F5></Row>')
,(90539, 51, 901, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>993</F4><F5>Grand Twin Towers East
236 Goodwin Crest Dr
Homewood, AL</F5></Row>')
,(90540, 51, 902, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>994</F4><F5>Rivergate Tower
400 N Ashley Dr
Tampa, FL</F5></Row>')
,(90541, 51, 903, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>995</F4><F5>Courthouse Place
12 SE 7th St
Fort Lauderdale, FL</F5></Row>')
,(90542, 51, 904, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>996</F4><F5>The Reflections
7775 Baymeadows Way
Jacksonville, FL</F5></Row>')
,(90543, 51, 905, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>997</F4><F5>The Reflections
7785 Baymeadows Way
Jacksonville, FL</F5></Row>')
,(90544, 51, 906, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>998</F4><F5>Crystal River Plaza
408 N Suncoast Blvd
Crystal River, FL</F5></Row>')
,(90545, 51, 907, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>999</F4><F5>South Pine Island Building
100 S Pine Island Rd
Plantation, FL</F5></Row>')
,(90546, 51, 908, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1000</F4><F5>Promenades Mall
3280 N Tamiami Trail
Port Charlotte, FL</F5></Row>')
,(90547, 51, 909, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1001</F4><F5>Miami Gardens Square
180 NW 183rd St
Miami, FL</F5></Row>')
,(90548, 51, 910, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1002</F4><F5>Welleby Square Shopping Center
10018 W Oakland Park Blvd
Sunrise, FL</F5></Row>')
,(90549, 51, 911, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1003</F4><F5>Oakbrook Plaza
7157 W Oakland Park Blvd
Lauderhill, FL</F5></Row>')
,(90550, 51, 912, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1004</F4><F5>Highlands Plaza
2900 Lakeland Highlands Rd
Lakeland, FL</F5></Row>')
,(90551, 51, 913, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1005</F4><F5>Lantana West Plaza
5891 S Military Trail
Lake Worth, FL</F5></Row>')
,(90552, 51, 914, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1006</F4><F5>Pompano Plaza
1602 S Cypress Rd
Pompano Beach , FL</F5></Row>')
,(90553, 51, 915, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1007</F4><F5>Tower at 1301 Gervais
1301 Gervais St
Columbia, SC</F5></Row>')
,(90554, 51, 916, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1008</F4><F5>Airways Plaza I
1281 Murfreesboro Pike
Nashville, TN</F5></Row>')
,(90555, 51, 917, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1009</F4><F5>Airways Plaza II
1283 Murfreesboro Pike
Nashville, TN</F5></Row>')
,(90556, 51, 918, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1010</F4><F5>Poplar Towers
6263 Poplar Ave
Memphis, TN</F5></Row>')
,(90557, 51, 919, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1011</F4><F5>Lipscomb &amp; Pitts Building
2670 Union Avenue Extended
Memphis, TN</F5></Row>')
,(90558, 51, 920, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1012</F4><F5>Donelson Plaza I
2710 Old Lebanon Rd
Nashville, TN</F5></Row>')
,(90559, 51, 921, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1013</F4><F5>Donelson Plaza II
2720 Old Lebanon Rd
Nashville, TN</F5></Row>')
,(90560, 51, 922, N'<Row><F1>10203</F1><F2>INR4</F2><F3>Southeast Office and Retail Portfolio</F3><F4>1014</F4><F5>Two Rivers Center
668 N Riverside Dr
Clarksville, TN</F5></Row>')
,(90561, 51, 923, N'<Row><F1>9871</F1><F2>INV4</F2><F3>Inverness Corners &amp; Plaza</F3><F4>22</F4><F5>Inverness Corners
100 Inverness Cors
Birmingham, AL</F5></Row>')
,(90562, 51, 924, N'<Row><F1>9871</F1><F2>INV4</F2><F3>Inverness Corners &amp; Plaza</F3><F4>966</F4><F5>Inverness Plaza
Inverness Plaza
Birmingham, AL</F5></Row>')
,(90563, 51, 925, N'<Row><F1>9142</F1><F2>JWM3</F2><F3>JW Marriott San Francisco</F3><F4>683</F4><F5>JW Marriott San Francisco
500 Post St
San Francisco, CA</F5></Row>')
,(90564, 51, 926, N'<Row><F1>10340</F1><F2>KBF4</F2><F3>Hotel Kabuki (Financing)</F3><F4>236</F4><F5>Hotel Kabuki
1625 Post St
San Francisco, CA</F5></Row>')
,(90565, 51, 927, N'<Row><F1>10078</F1><F2>KBK4</F2><F3>Hotel Kabuki</F3><F4>236</F4><F5>Hotel Kabuki
1625 Post St
San Francisco, CA</F5></Row>')
,(90566, 51, 928, N'<Row><F1>10676</F1><F2>KEN4</F2><F3>Kensington Retail Portfolio </F3><F4>1250</F4><F5>100 Brompton Rd
Knightsbridge, London SW3 1ER, UK</F5></Row>')
,(90567, 51, 929, N'<Row><F1>10676</F1><F2>KEN4</F2><F3>Kensington Retail Portfolio </F3><F4>1251</F4><F5>47-53 Kensington High St
Kensington, London W8 5ED, UK</F5></Row>')
,(90568, 51, 930, N'<Row><F1>10676</F1><F2>KEN4</F2><F3>Kensington Retail Portfolio </F3><F4>1252</F4><F5>92 Kensington High St
Kensington, London W8 5ED, UK</F5></Row>')
,(90569, 51, 931, N'<Row><F1>6857</F1><F2>KMF3</F2><F3>2040 Main Street</F3><F4>1040</F4><F5>2040 Main St
Irvine, CA</F5></Row>')
,(90570, 51, 932, N'<Row><F1>10251</F1><F2>KNG4</F2><F3>King and Grove New York (Martha Washington Hotel)</F3><F4>1146</F4><F5>Martha Washington Hotel
29 E 29th St
New York, NY</F5></Row>')
,(90571, 51, 933, N'<Row><F1>10141</F1><F2>KOL4</F2><F3>Hyatt Place Delray Beach &amp; Fort Lauderdale </F3><F4>1143</F4><F5>Hyatt Place Delray Beach
104 NE 2nd Ave
Delray Beach, FL</F5></Row>')
,(90572, 51, 934, N'<Row><F1>10141</F1><F2>KOL4</F2><F3>Hyatt Place Delray Beach &amp; Fort Lauderdale </F3><F4>1144</F4><F5>Hyatt Place Fort Lauderdale
1851 SE 10th Ave
Fort Lauderdale, FL</F5></Row>')
,(90573, 51, 935, N'<Row><F1>10675</F1><F2>KSF4</F2><F3>Kings Shops </F3><F4>1147</F4><F5>Kings'' Shops
69-250 Waikoloa Beach Dr
Waikoloa Village, HI</F5></Row>')
,(90574, 51, 936, N'<Row><F1>8962</F1><F2>KUA3</F2><F3>2100 Kalakaua Avenue </F3><F4>1041</F4><F5>2100 Kalakaua Ave
Honolulu, HI</F5></Row>')
,(90575, 51, 937, N'<Row><F1>9788</F1><F2>KYA3</F2><F3>Kyo-Ya Hotel Portfolio </F3><F4>1148</F4><F5>Sheraton Waikiki
2255 Kalakaua Ave
Honolulu, HI</F5></Row>')
,(90576, 51, 938, N'<Row><F1>9788</F1><F2>KYA3</F2><F3>Kyo-Ya Hotel Portfolio </F3><F4>1149</F4><F5>The Royal Hawaiian
2259 Kalakaua Ave
Honolulu, HI</F5></Row>')
,(90577, 51, 939, N'<Row><F1>9788</F1><F2>KYA3</F2><F3>Kyo-Ya Hotel Portfolio </F3><F4>1150</F4><F5>Westin Moana Surfrider
2365 Kalakaua Ave
Honolulu, HI</F5></Row>')
,(90578, 51, 940, N'<Row><F1>9788</F1><F2>KYA3</F2><F3>Kyo-Ya Hotel Portfolio </F3><F4>1151</F4><F5>Sheraton Maui Resort
2605 Kaanapali Pkwy
Kaanapali, HI</F5></Row>')
,(90579, 51, 941, N'<Row><F1>9788</F1><F2>KYA3</F2><F3>Kyo-Ya Hotel Portfolio </F3><F4>1152</F4><F5>The Palace Hotel
2 New Montgomery St
San Francisco, CA</F5></Row>')
,(90580, 51, 942, N'<Row><F1>8992</F1><F2>LAC3</F2><F3>90 Long Acre (Financing)</F3><F4>1020</F4><F5>90 Long Acre
London, UK</F5></Row>')
,(90581, 51, 943, N'<Row><F1>10001</F1><F2>LAD4</F2><F3>The Lodge at Doonbeg </F3><F4>962</F4><F5>Trump International Golf Links &amp; Hotel Ireland
Doonbeg
Co. Claire, Ireland</F5></Row>')
,(90582, 51, 944, N'<Row><F1>9428</F1><F2>LAK3</F2><F3>Lake Park</F3><F4>463</F4><F5>3001 Tasman Dr
Santa Clara, CA</F5></Row>')
,(90583, 51, 945, N'<Row><F1>9428</F1><F2>LAK3</F2><F3>Lake Park</F3><F4>465</F4><F5>3003 Tasman Dr
Santa Clara, CA</F5></Row>')
,(90584, 51, 946, N'<Row><F1>9428</F1><F2>LAK3</F2><F3>Lake Park</F3><F4>466</F4><F5>3005 Tasman Dr
Santa Clara, CA</F5></Row>')
,(90585, 51, 947, N'<Row><F1>9428</F1><F2>LAK3</F2><F3>Lake Park</F3><F4>476</F4><F5>3032 Bunker Hill Dr
Santa Clara, CA</F5></Row>')
,(90586, 51, 948, N'<Row><F1>9428</F1><F2>LAK3</F2><F3>Lake Park</F3><F4>481</F4><F5>3052 Bunker Hill Dr
Santa Clara, CA</F5></Row>')
,(90587, 51, 949, N'<Row><F1>9428</F1><F2>LAK3</F2><F3>Lake Park</F3><F4>700</F4><F5>5101 Patrick Henry Dr
Santa Clara, CA</F5></Row>')
,(90588, 51, 950, N'<Row><F1>9428</F1><F2>LAK3</F2><F3>Lake Park</F3><F4>701</F4><F5>5104 Old Ironsides Dr
Santa Clara, CA</F5></Row>')
,(90589, 51, 951, N'<Row><F1>7781</F1><F2>LAS2</F2><F3>Las Olas Centre</F3><F4>550</F4><F5>Las Olas Centre
350 Las Olas Blvd
Fort Lauderdale, FL</F5></Row>')
,(90590, 51, 952, N'<Row><F1>10342</F1><F2>LAT4</F2><F3>Latitude 34 </F3><F4>1154</F4><F5>12130 Millennium
12130 Millenum Dr
Los Angeles, CA</F5></Row>')
,(90591, 51, 953, N'<Row><F1>10342</F1><F2>LAT4</F2><F3>Latitude 34 </F3><F4>1155</F4><F5>12180 Millennium
12180 E Waterfront Dr
Los Angeles, CA</F5></Row>')
,(90592, 51, 954, N'<Row><F1>9912</F1><F2>LBF3</F2><F3>LBA IV Portfolio</F3><F4>1169</F4><F5>2425 Whipple Rd
Hayward, CA</F5></Row>')
,(90593, 51, 955, N'<Row><F1>9912</F1><F2>LBF3</F2><F3>LBA IV Portfolio</F3><F4>1170</F4><F5>6520 S 190th St
Kent, WA</F5></Row>')
,(90594, 51, 956, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1157</F4><F5>Redmond Commerce Center - Building 1
18340 NE 76th St
Redmond, WA</F5></Row>')
,(90595, 51, 957, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1158</F4><F5>Redmond Commerce Center - Building 3
18460 NE 76th St
Redmond, WA</F5></Row>')
,(90596, 51, 958, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1159</F4><F5>611 Reyes Dr
Walnut, CA</F5></Row>')
,(90597, 51, 959, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1160</F4><F5>2899 Mead Ave
Santa Clara, CA</F5></Row>')
,(90598, 51, 960, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1161</F4><F5>3919 E Guasti Rd
Ontario, CA</F5></Row>')
,(90599, 51, 961, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1162</F4><F5>3929 E Guasti Rd
Ontario, CA</F5></Row>')
,(90600, 51, 962, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1163</F4><F5>3939 E Guasti Rd
Ontario, CA</F5></Row>')
,(90601, 51, 963, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1164</F4><F5>3949 E Guasti Rd
Ontario, CA</F5></Row>')
,(90602, 51, 964, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1165</F4><F5>3959 E Guasti Rd
Ontario, CA</F5></Row>')
,(90603, 51, 965, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1166</F4><F5>3969 E Guasti Rd
Ontario, CA</F5></Row>')
,(90604, 51, 966, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1167</F4><F5>3979 E Guasti Rd
Ontario, CA</F5></Row>')
,(90605, 51, 967, N'<Row><F1>10798</F1><F2>LBF4</F2><F3>LBA Realty Fund V Industrial Portfolio </F3><F4>1168</F4><F5>5450 W Kiest Blvd
Dallas, TX</F5></Row>')
,(90606, 51, 968, N'<Row><F1>10616</F1><F2>LCF4</F2><F3>La Costa Town Center </F3><F4>1153</F4><F5>La Costa Towne Center
7710 El Camino Real
Carlsbad, CA</F5></Row>')
,(90607, 51, 969, N'<Row><F1>10109</F1><F2>LEG4</F2><F3>Project Legacy</F3><F4>60</F4><F5>The Mall at Wellington Green
10300 W Forest Hill Blvd
Wellington, FL</F5></Row>')
,(90608, 51, 970, N'<Row><F1>10109</F1><F2>LEG4</F2><F3>Project Legacy</F3><F4>261</F4><F5>The Mall at Partridge Creek
17420 Hall Rd
Charter Township of Clinton, MI</F5></Row>')
,(90609, 51, 971, N'<Row><F1>10109</F1><F2>LEG4</F2><F3>Project Legacy</F3><F4>289</F4><F5>Fairlane Town Center
18900 Michigan Ave
Dearborn, MI</F5></Row>')
,(90610, 51, 972, N'<Row><F1>10109</F1><F2>LEG4</F2><F3>Project Legacy</F3><F4>455</F4><F5>MacArthur Center
300 Monticello Ave
Norfolk, VA</F5></Row>')
,(90611, 51, 973, N'<Row><F1>10109</F1><F2>LEG4</F2><F3>Project Legacy</F3><F4>764</F4><F5>The Shops at Willow Bend
6121 W Park Blvd
Plano, TX</F5></Row>')
,(90612, 51, 974, N'<Row><F1>10109</F1><F2>LEG4</F2><F3>Project Legacy</F3><F4>796</F4><F5>Northlake Mall
6801 Northlake Mall Dr
Charlotte, NC</F5></Row>')
,(90613, 51, 975, N'<Row><F1>10109</F1><F2>LEG4</F2><F3>Project Legacy</F3><F4>922</F4><F5>Stony Point Fashion Park
9200 Stony Point Pkwy
Richmond, VA</F5></Row>')
,(90614, 51, 976, N'<Row><F1>10550</F1><F2>LMF4</F2><F3>Lenox Marketplace </F3><F4>1171</F4><F5>Lenox Marketplace
3535 Peachtree Rd
Atlanta, GA</F5></Row>')
,(90615, 51, 977, N'<Row><F1>10313</F1><F2>LMO4</F2><F3>Landmark One</F3><F4>13</F4><F5>Landmark One
1 Van de Graaff Dr
Burlington, MA</F5></Row>')
,(90616, 51, 978, N'<Row><F1>9545</F1><F2>LMP3</F2><F3>1999 Harrison (Lake Merritt Plaza)</F3><F4>301</F4><F5>1956 Weber St
Oakland, CA</F5></Row>')
,(90617, 51, 979, N'<Row><F1>9545</F1><F2>LMP3</F2><F3>1999 Harrison (Lake Merritt Plaza)</F3><F4>306</F4><F5>1999 Harrison St
Oakland, CA</F5></Row>')
,(90618, 51, 980, N'<Row><F1>9938</F1><F2>LMR3</F2><F3>1999 Harrison (Financing)</F3><F4>306</F4><F5>1999 Harrison St
Oakland, CA</F5></Row>')
,(90619, 51, 981, N'<Row><F1>11005</F1><F2>LNS4</F2><F3>100 M Street, SE (Financing)</F3><F4>24</F4><F5>100 M St SE
Washington, DC</F5></Row>')
,(90620, 51, 982, N'<Row><F1>11315</F1><F2>LRC4</F2><F3>Legacy Redhill Center</F3><F4>444</F4><F5>2955 Red Hill Ave
Costa Mesa, CA</F5></Row>')
,(90621, 51, 983, N'<Row><F1>11315</F1><F2>LRC4</F2><F3>Legacy Redhill Center</F3><F4>445</F4><F5>2975 Red Hill Ave
Costa Mesa, CA</F5></Row>')
,(90622, 51, 984, N'<Row><F1>11315</F1><F2>LRC4</F2><F3>Legacy Redhill Center</F3><F4>447</F4><F5>2995 Red Hill Ave
Costa Mesa, CA</F5></Row>')
,(90623, 51, 985, N'<Row><F1>9505</F1><F2>LRF3</F2><F3>245 South Los Robles (Ameron Center)</F3><F4>388</F4><F5>245 S Los Robles
Pasadena, CA</F5></Row>')
,(90624, 51, 986, N'<Row><F1>10071</F1><F2>LSF4</F2><F3>705 Union Station</F3><F4>1029</F4><F5>705 Union Station
705 5th Ave S
Seattle, WA</F5></Row>')
,(90625, 51, 987, N'<Row><F1>9492</F1><F2>LUX4</F2><F3>Luxe City Center Hotel</F3><F4>51</F4><F5>Luxe City Center Hotel
1020 S Figueroa
Los Angeles, CA</F5></Row>')
,(90626, 51, 988, N'<Row><F1>9899</F1><F2>LVF3</F2><F3>Laulani Village (Financing)</F3><F4>1156</F4><F5>Laulani Village
91-1061 Keaunui Dr
Ewa Beach, HI</F5></Row>')
,(90627, 51, 989, N'<Row><F1>10810</F1><F2>LWF4</F2><F3>Loop West</F3><F4>1299</F4><F5>Loop West
2001-2699 W Osceola Pkwy
Kissimmee, FL</F5></Row>')
,(90628, 51, 990, N'<Row><F1>8430</F1><F2>MAC2</F2><F3>Macerich Mall Portfolio</F3><F4>65</F4><F5>South Towne Center
10450 S State St
Sandy, UT</F5></Row>')
,(90629, 51, 991, N'<Row><F1>8430</F1><F2>MAC2</F2><F3>Macerich Mall Portfolio</F3><F4>77</F4><F5>South Towne Center
10600 S State St
Sandy, UT</F5></Row>')
,(90630, 51, 992, N'<Row><F1>9803</F1><F2>MAT3</F2><F3>Marriott &amp; SpringHill Suites Atlanta Airport</F3><F4>323</F4><F5>Atlanta Airport Marriott Gateway
2020 Convention Center Concourse
Atlanta, GA</F5></Row>')
,(90631, 51, 993, N'<Row><F1>9803</F1><F2>MAT3</F2><F3>Marriott &amp; SpringHill Suites Atlanta Airport</F3><F4>339</F4><F5>Springhill Suites Atlanta Airport Gateway
2091 Convention Center Concourse
College Park, GA</F5></Row>')
,(90632, 51, 994, N'<Row><F1>7958</F1><F2>MBV2</F2><F3>Malibu Village </F3><F4>583</F4><F5>Malibu Village 
3822-3898 Cross Creek Rd
Malibu, CA</F5></Row>')
,(90633, 51, 995, N'<Row><F1>9952</F1><F2>MCP4</F2><F3>Mid City Place</F3><F4>820</F4><F5>Mid City Place
71 High Holborn
London, UK</F5></Row>')
,(90634, 51, 996, N'<Row><F1>9719</F1><F2>MDP4</F2><F3>Murdock Plaza</F3><F4>84</F4><F5>Murdock Plaza
10900 Wilshire Blvd
Westwood, CA</F5></Row>')
,(90635, 51, 997, N'<Row><F1>10061</F1><F2>MHF4</F2><F3>Mark Hopkins San Francisco (Financing)</F3><F4>10</F4><F5>Intercontinental Mark Hopkins San Francisco
1 Nob Hill Pl
San Francisco, CA</F5></Row>')
,(90636, 51, 998, N'<Row><F1>11213</F1><F2>MLF4</F2><F3>Towne Center of Mililani (Financing)</F3><F4>1293</F4><F5>Town Center of Mililani
95-1249 Meheula Pkwy #193
Mililani, HI</F5></Row>')
,(90637, 51, 999, N'<Row><F1>9737</F1><F2>MLP3</F2><F3>Merrill Place</F3><F4>615</F4><F5>Merrill Place
411 1st Ave
Seattle, WA</F5></Row>')
,(90638, 51, 1000, N'<Row><F1>10493</F1><F2>MOA4</F2><F3>Mall of America </F3><F4>1172</F4><F5>Mall of America
60 E Broadway
Bloomington, MN</F5></Row>')
,(90639, 51, 1001, N'<Row><F1>9752</F1><F2>MPD3</F2><F3>Marriott Philadelphia Downtown</F3><F4>141</F4><F5>Marriott Philadelphia Downtown
1201 Market St
Philadelphia, PA</F5></Row>')
,(90640, 51, 1002, N'<Row><F1>10603</F1><F2>MPF4</F2><F3>Murdock Plaza (Financing)</F3><F4>84</F4><F5>Murdock Plaza
10900 Wilshire Blvd
Westwood, CA</F5></Row>')
,(90641, 51, 1003, N'<Row><F1>8937</F1><F2>MPH3</F2><F3>Marriott Philadelphia Downtown</F3><F4>141</F4><F5>Marriott Philadelphia Downtown
1201 Market St
Philadelphia, PA</F5></Row>')
,(90642, 51, 1004, N'<Row><F1>9377</F1><F2>MRC3</F2><F3>Milford Plaza Retail Condo </F3><F4>807</F4><F5>700 8th Ave
New York, NY</F5></Row>')
,(90643, 51, 1005, N'<Row><F1>9705</F1><F2>MRF3</F2><F3>The Astor &amp; Metro Retail Condominium </F3><F4>1280</F4><F5>The Metro
301 W 53rd St
New York, NY</F5></Row>')
,(90644, 51, 1006, N'<Row><F1>10768</F1><F2>MRF4</F2><F3>Indian Hills Plaza</F3><F4>1145</F4><F5>Indian Hills Plaza
4208 E Blue Grass Rd
Mount Pleasant, MI</F5></Row>')
,(90645, 51, 1007, N'<Row><F1>10639</F1><F2>MRI4</F2><F3>Residence Inn World Trade Center </F3><F4>247</F4><F5>170 Broadway
New York, NY</F5></Row>')
,(90646, 51, 1008, N'<Row><F1>9526</F1><F2>MTF3</F2><F3>Millenium Tower </F3><F4>1200</F4><F5>Millennium Tower
10375 Richmond Ave
Houston, TX</F5></Row>')
,(90647, 51, 1009, N'<Row><F1>9424</F1><F2>MTI3</F2><F3>Mission Towers I</F3><F4>593</F4><F5>Mission Towers I
3975 Freedom Cir
Santa Clara, CA</F5></Row>')
,(90648, 51, 1010, N'<Row><F1>10084</F1><F2>MTT4</F2><F3>Mission Towers II</F3><F4>594</F4><F5>Mission Towers II
3979-3985 Freedom Cir
Santa Clara, CA</F5></Row>')
,(90649, 51, 1011, N'<Row><F1>10020</F1><F2>MVC4</F2><F3>Mountain View Corporate Center</F3><F4>467</F4><F5>301 E Evelyn Ave
Mountain View, CA</F5></Row>')
,(90650, 51, 1012, N'<Row><F1>10020</F1><F2>MVC4</F2><F3>Mountain View Corporate Center</F3><F4>513</F4><F5>321 E Evelyn Ave
Mountain View, CA</F5></Row>')
,(90651, 51, 1013, N'<Row><F1>10020</F1><F2>MVC4</F2><F3>Mountain View Corporate Center</F3><F4>528</F4><F5>331 E Evelyn Ave
Mountain View, CA</F5></Row>')
,(90652, 51, 1014, N'<Row><F1>10020</F1><F2>MVC4</F2><F3>Mountain View Corporate Center</F3><F4>553</F4><F5>351 E Evelyn Ave
Mountain View, CA</F5></Row>')
,(90653, 51, 1015, N'<Row><F1>10020</F1><F2>MVC4</F2><F3>Mountain View Corporate Center</F3><F4>581</F4><F5>381 E Evelyn Ave
Mountain View, CA</F5></Row>')
,(90654, 51, 1016, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1173</F4><F5>1050 Marina Village Pkwy
Alameda, CA</F5></Row>')
,(90655, 51, 1017, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1174</F4><F5>1080 Marina Village Pkwy
Alameda, CA</F5></Row>')
,(90656, 51, 1018, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1175</F4><F5>1001 Marina Village Pkwy
Alameda, CA</F5></Row>')
,(90657, 51, 1019, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1176</F4><F5>1101 Marina Village Pkwy
Alameda, CA</F5></Row>')
,(90658, 51, 1020, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1177</F4><F5>1151 Marina Village Pkwy
Alameda, CA</F5></Row>')
,(90659, 51, 1021, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1178</F4><F5>1201 Marina Village Pkwy
Alameda, CA</F5></Row>')
,(90660, 51, 1022, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1179</F4><F5>1301 Marina Village Pkwy
Alameda, CA</F5></Row>')
,(90661, 51, 1023, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1180</F4><F5>850 Marina Village Pkwy
Alameda, CA</F5></Row>')
,(90662, 51, 1024, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1181</F4><F5>950 Marina Village Pkwy
Alameda, CA</F5></Row>')
,(90663, 51, 1025, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1182</F4><F5>815 Atlantic Ave
Alameda, CA</F5></Row>')
,(90664, 51, 1026, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1183</F4><F5>960 Atlantic Ave
Alameda, CA</F5></Row>')
,(90665, 51, 1027, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1184</F4><F5>965 Atlantic Ave
Alameda, CA</F5></Row>')
,(90666, 51, 1028, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1185</F4><F5>980 Atlantic Ave
Alameda, CA</F5></Row>')
,(90667, 51, 1029, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1186</F4><F5>1000 Atlantic Ave
Alameda, CA</F5></Row>')
,(90668, 51, 1030, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1187</F4><F5>1010 Atlantic Ave
Alameda, CA</F5></Row>')
,(90669, 51, 1031, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1188</F4><F5>1015 Atlantic Ave
Alameda, CA</F5></Row>')
,(90670, 51, 1032, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1189</F4><F5>1020 Atlantic Ave
Alameda, CA</F5></Row>')
,(90671, 51, 1033, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1190</F4><F5>1025 Atlantic Ave
Alameda, CA</F5></Row>')
,(90672, 51, 1034, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1191</F4><F5>1105 Atlantic Ave
Alameda, CA</F5></Row>')
,(90673, 51, 1035, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1192</F4><F5>1125 Atlantic Ave
Alameda, CA</F5></Row>')
,(90674, 51, 1036, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1193</F4><F5>1135 Atlantic Ave
Alameda, CA</F5></Row>')
,(90675, 51, 1037, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1194</F4><F5>1145 Atlantic Ave
Alameda, CA</F5></Row>')
,(90676, 51, 1038, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1195</F4><F5>2021 Challenger Dr
Alameda, CA</F5></Row>')
,(90677, 51, 1039, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1196</F4><F5>2020 Challenger Dr
Alameda, CA</F5></Row>')
,(90678, 51, 1040, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1197</F4><F5>2060 Challenger Dr
Alameda, CA</F5></Row>')
,(90679, 51, 1041, N'<Row><F1>9105</F1><F2>MVF3</F2><F3>Marina Village</F3><F4>1198</F4><F5>2061 Challenger Dr
Alameda, CA</F5></Row>')
,(90680, 51, 1042, N'<Row><F1>10318</F1><F2>MVF4</F2><F3>Malibu Village</F3><F4>583</F4><F5>Malibu Village 
3822-3898 Cross Creek Rd
Malibu, CA</F5></Row>')
,(90681, 51, 1043, N'<Row><F1>9676</F1><F2>NBD3</F2><F3>90 Broad Street</F3><F4>908</F4><F5>90 Broad St
New York, NY</F5></Row>')
,(90682, 51, 1044, N'<Row><F1>9890</F1><F2>NCO3</F2><F3>1310 N. Courthouse</F3><F4>164</F4><F5>1310 N Courthouse Rd
Arlington, VA</F5></Row>')
,(90683, 51, 1045, N'<Row><F1>10036</F1><F2>NEW4</F2><F3>Meridian Residences</F3><F4>1199</F4><F5>Meridian Residences
1001 Santa Barbara Dr
Newport Beach, CA</F5></Row>')
,(90684, 51, 1046, N'<Row><F1>9102</F1><F2>NHA3</F2><F3>North Hills </F3><F4>634</F4><F5>North Hills 
4351 The Cir at N Hills St
Raleigh, NC</F5></Row>')
,(90685, 51, 1047, N'<Row><F1>9572</F1><F2>NLS3</F2><F3>180 North LaSalle </F3><F4>1024</F4><F5>180 N LaSalle St
Chicago, IL</F5></Row>')
,(90686, 51, 1048, N'<Row><F1>9642</F1><F2>NOB3</F2><F3>North Bethesda Market Retail </F3><F4>119</F4><F5>North Bethesda Market
11351 Woodglen Dr
North Bethesda, MD</F5></Row>')
,(90687, 51, 1049, N'<Row><F1>10100</F1><F2>NPP4</F2><F3>North Park Plaza </F3><F4>251</F4><F5>North Park Plaza
1702-1712 Oakland Rd
San Jose, CA</F5></Row>')
,(90688, 51, 1050, N'<Row><F1>10432</F1><F2>NTB4</F2><F3>920 Broadway</F3><F4>921</F4><F5>920 Broadway
New York, NY</F5></Row>')
,(90689, 51, 1051, N'<Row><F1>10113</F1><F2>NTC4</F2><F3>Northpark Town Center</F3><F4>30</F4><F5>1000 Abernathy Rd NE
Atlanta, GA</F5></Row>')
,(90690, 51, 1052, N'<Row><F1>10113</F1><F2>NTC4</F2><F3>Northpark Town Center</F3><F4>99</F4><F5>1100 Abernathy Rd NE
Atlanta, GA</F5></Row>')
,(90691, 51, 1053, N'<Row><F1>10113</F1><F2>NTC4</F2><F3>Northpark Town Center</F3><F4>136</F4><F5>1200 Abernathy Rd NE
Atlanta, GA</F5></Row>')
,(90692, 51, 1054, N'<Row><F1>9977</F1><F2>NWM4</F2><F3>Marin &amp; Sonoma County Office Portfolio </F3><F4>1089</F4><F5>2320 Marinship Way
Sausalito, CA</F5></Row>')
,(90693, 51, 1055, N'<Row><F1>9977</F1><F2>NWM4</F2><F3>Marin &amp; Sonoma County Office Portfolio </F3><F4>1090</F4><F5>2330 Marinship Way
Sausalito, CA</F5></Row>')
,(90694, 51, 1056, N'<Row><F1>9977</F1><F2>NWM4</F2><F3>Marin &amp; Sonoma County Office Portfolio </F3><F4>1091</F4><F5>One Thorndale
1 Thorndale Dr
San Rafael, CA</F5></Row>')
,(90695, 51, 1057, N'<Row><F1>9977</F1><F2>NWM4</F2><F3>Marin &amp; Sonoma County Office Portfolio </F3><F4>1092</F4><F5>The Courtyard Building
Santa Rosa, CA</F5></Row>')
,(90696, 51, 1058, N'<Row><F1>9977</F1><F2>NWM4</F2><F3>Marin &amp; Sonoma County Office Portfolio </F3><F4>1093</F4><F5>149 Stony Point Cir
Santa Rosa, CA</F5></Row>')
,(90697, 51, 1059, N'<Row><F1>9977</F1><F2>NWM4</F2><F3>Marin &amp; Sonoma County Office Portfolio </F3><F4>1094</F4><F5>130 Stony Point Rd
Santa Rosa, CA</F5></Row>')
,(90698, 51, 1060, N'<Row><F1>9977</F1><F2>NWM4</F2><F3>Marin &amp; Sonoma County Office Portfolio </F3><F4>1095</F4><F5>100 B St
Santa Rosa, CA</F5></Row>')
,(90699, 51, 1061, N'<Row><F1>9977</F1><F2>NWM4</F2><F3>Marin &amp; Sonoma County Office Portfolio </F3><F4>1096</F4><F5>140 Stony Point Rd
Santa Rosa, CA</F5></Row>')
,(90700, 51, 1062, N'<Row><F1>10341</F1><F2>OBP4</F2><F3>One Buckhead Plaza</F3><F4>1296</F4><F5>One Buckhead Plaza
3060 Peachtree Rd NW
Atlanta, GA</F5></Row>')
,(90701, 51, 1063, N'<Row><F1>10044</F1><F2>OBS4</F2><F3>One Beacon Street </F3><F4>2</F4><F5>One Beacon Street
1 Beacon St
Boston, MA</F5></Row>')
,(90702, 51, 1064, N'<Row><F1>9654</F1><F2>OCP3</F2><F3>O''Connor Northeast Retail Portfolio </F3><F4>1203</F4><F5>Village Shoppes
95 Washington St
Canton, MA</F5></Row>')
,(90703, 51, 1065, N'<Row><F1>9654</F1><F2>OCP3</F2><F3>O''Connor Northeast Retail Portfolio </F3><F4>1204</F4><F5>Dedham Mall
300 Providence Hwy
Dedham, MA</F5></Row>')
,(90704, 51, 1066, N'<Row><F1>9654</F1><F2>OCP3</F2><F3>O''Connor Northeast Retail Portfolio </F3><F4>1205</F4><F5>Falmouth Mall
137 Teaticket Hwy
East Falmouth, MA</F5></Row>')
,(90705, 51, 1067, N'<Row><F1>9654</F1><F2>OCP3</F2><F3>O''Connor Northeast Retail Portfolio </F3><F4>1206</F4><F5>Southwind Plaza
65 Independence Dr
Hyannis, MA</F5></Row>')
,(90706, 51, 1068, N'<Row><F1>9654</F1><F2>OCP3</F2><F3>O''Connor Northeast Retail Portfolio </F3><F4>1207</F4><F5>Medfield Shops
230 Main St
Medfield, MA</F5></Row>')
,(90707, 51, 1069, N'<Row><F1>9654</F1><F2>OCP3</F2><F3>O''Connor Northeast Retail Portfolio </F3><F4>1208</F4><F5>Royal Ridge Center
213 Daniel Webster Hwy
Nashua, NH</F5></Row>')
,(90708, 51, 1070, N'<Row><F1>9654</F1><F2>OCP3</F2><F3>O''Connor Northeast Retail Portfolio </F3><F4>1209</F4><F5>Portsmouth Circle
500 Spaulding Tpk
Portsmouth, NH</F5></Row>')
,(90709, 51, 1071, N'<Row><F1>9654</F1><F2>OCP3</F2><F3>O''Connor Northeast Retail Portfolio </F3><F4>1210</F4><F5>Rochester Crossing
160 Washington St
Rochester, NH</F5></Row>')
,(90710, 51, 1072, N'<Row><F1>9654</F1><F2>OCP3</F2><F3>O''Connor Northeast Retail Portfolio </F3><F4>1211</F4><F5>Belknap Mall
96 Daniel Webster Hwy
Belmont, NH</F5></Row>')
,(90711, 51, 1073, N'<Row><F1>9510</F1><F2>OEB3</F2><F3>180 Broadway</F3><F4>270</F4><F5>180 Broadway
New York, NY</F5></Row>')
,(90712, 51, 1074, N'<Row><F1>9653</F1><F2>OFP3</F2><F3>114 Prince </F3><F4>120</F4><F5>114 Prince St
New York, NY</F5></Row>')
,(90713, 51, 1075, N'<Row><F1>9983</F1><F2>OFW4</F2><F3>150 West 34th Street </F3><F4>213</F4><F5>150 W 34th St
New York, NY</F5></Row>')
,(90714, 51, 1076, N'<Row><F1>10039</F1><F2>OHM4</F2><F3>100 M Street, SE</F3><F4>24</F4><F5>100 M St SE
Washington, DC</F5></Row>')
,(90715, 51, 1077, N'<Row><F1>10391</F1><F2>OLV4</F2><F3>720 Olive (Financing)</F3><F4>831</F4><F5>720 Olive Way
Seattle, WA</F5></Row>')
,(90716, 51, 1078, N'<Row><F1>9141</F1><F2>OMP3</F2><F3>Project One</F3><F4>7</F4><F5>One Market Plaza
1 Market St
San Francisco, CA</F5></Row>')
,(90717, 51, 1079, N'<Row><F1>9944</F1><F2>OPA4</F2><F3>One Park Avenue </F3><F4>11</F4><F5>One Park Avenue
1 Park Ave
New York, NY</F5></Row>')
,(90718, 51, 1080, N'<Row><F1>10187</F1><F2>OPF4</F2><F3>One Canal Park (Financing) </F3><F4>1212</F4><F5>One Canal Park
1 Canal Park
Cambridge, MA</F5></Row>')
,(90719, 51, 1081, N'<Row><F1>9825</F1><F2>OPK3</F2><F3>One Canal Park</F3><F4>1212</F4><F5>One Canal Park
1 Canal Park
Cambridge, MA</F5></Row>')
,(90720, 51, 1082, N'<Row><F1>10202</F1><F2>OPP4</F2><F3>One Pacific Plaza</F3><F4>847</F4><F5>One Pacific Plaza
7711-7979 Center Ave
Huntington Beach, CA</F5></Row>')
,(90721, 51, 1083, N'<Row><F1>8307</F1><F2>ORD3</F2><F3>One Research Drive</F3><F4>12</F4><F5>One Research Drive
1 Research Dr
Westborough, MA</F5></Row>')
,(90722, 51, 1084, N'<Row><F1>9140</F1><F2>OSB3</F2><F3>170 Broadway Retail Condominium</F3><F4>247</F4><F5>170 Broadway
New York, NY</F5></Row>')
,(90723, 51, 1085, N'<Row><F1>9436</F1><F2>OSC3</F2><F3>700 13th Street (Project Oscar Whitehall)</F3><F4>806</F4><F5>700 13th St NW
Washington, DC</F5></Row>')
,(90724, 51, 1086, N'<Row><F1>10201</F1><F2>OSH4</F2><F3>Orchard Supply Hardware Distribution Center</F3><F4>416</F4><F5>2650 N MacArthur Dr
Tracy, CA</F5></Row>')
,(90725, 51, 1087, N'<Row><F1>8260</F1><F2>OSO4</F2><F3>171 17th Street</F3><F4>255</F4><F5>171 17th St
Atlanta, GA</F5></Row>')
,(90726, 51, 1088, N'<Row><F1>9877</F1><F2>OTF3</F2><F3>Hilton Alexandria Old Town (Financing)</F3><F4>266</F4><F5>Hilton Alexandria Old Town 
1767 King St
Alexandria, VA</F5></Row>')
,(90727, 51, 1089, N'<Row><F1>9443</F1><F2>OVS3</F2><F3>175 Varick Street </F3><F4>262</F4><F5>175 Varick St
New York, NY</F5></Row>')
,(90728, 51, 1090, N'<Row><F1>9462</F1><F2>OWT3</F2><F3>150 W 34th St </F3><F4>213</F4><F5>150 W 34th St
New York, NY</F5></Row>')
,(90729, 51, 1091, N'<Row><F1>9688</F1><F2>OXF3</F2><F3>Gallery Place</F3><F4>1135</F4><F5>Gallery Place
616 H St NW
Washington, DC</F5></Row>')
,(90730, 51, 1092, N'<Row><F1>9477</F1><F2>OXS3</F2><F3>431-451 Oxford Street </F3><F4>1026</F4><F5>431-451 Oxford St
London W1C, UK</F5></Row>')
,(90731, 51, 1093, N'<Row><F1>11327</F1><F2>P10183</F2><F3>1633 Broadway</F3><F4>239</F4><F5>1633 Broadway
New York, NY</F5></Row>')
,(90732, 51, 1094, N'<Row><F1>11433</F1><F2>P10289</F2><F3>158 West 27th Street</F3><F4>1122</F4><F5>158 W 27th St
New York, NY</F5></Row>')
,(90733, 51, 1095, N'<Row><F1>9030</F1><F2>P7898</F2><F3>Milford Plaza Retail Condominium </F3><F4>807</F4><F5>700 8th Ave
New York, NY</F5></Row>')
,(90734, 51, 1096, N'<Row><F1>9208</F1><F2>P8074</F2><F3>1107 Broadway Retail Condo</F3><F4>105</F4><F5>1107 Broadway
New York, NY</F5></Row>')
,(90735, 51, 1097, N'<Row><F1>9749</F1><F2>P8606</F2><F3>Santa Susanna Plaza</F3><F4>353</F4><F5>Santa Susana Plaza
2196 Tapo St
Simi Valley, CA</F5></Row>')
,(90736, 51, 1098, N'<Row><F1>9773</F1><F2>P8630</F2><F3>500 Lake Shore Drive</F3><F4>1027</F4><F5>500 N Lake Shore Dr
Chicago, IL</F5></Row>')
,(90737, 51, 1099, N'<Row><F1>9782</F1><F2>P8638</F2><F3>4085 Campbell Ave</F3><F4>612</F4><F5>4085 Campbell Ave
Menlo Park, CA</F5></Row>')
,(90738, 51, 1100, N'<Row><F1>10048</F1><F2>P8904</F2><F3>Five Times Square</F3><F4>676</F4><F5>Five Times Square
5 Times Sq
New York, NY</F5></Row>')
,(90739, 51, 1101, N'<Row><F1>10339</F1><F2>P9195</F2><F3>28 State Street</F3><F4>427</F4><F5>28 State St
Boston, MA</F5></Row>')
,(90740, 51, 1102, N'<Row><F1>10446</F1><F2>P9302</F2><F3>Torrey Reserve West</F3><F4>534</F4><F5>3390 Carmel Mountain Rd
San Diego, CA</F5></Row>')
,(90741, 51, 1103, N'<Row><F1>10446</F1><F2>P9302</F2><F3>Torrey Reserve West</F3><F4>535</F4><F5>3394 Carmel Mountain Rd
San Diego, CA</F5></Row>')
,(90742, 51, 1104, N'<Row><F1>10446</F1><F2>P9302</F2><F3>Torrey Reserve West</F3><F4>536</F4><F5>3398 Carmel Mountain Rd
San Diego, CA</F5></Row>')
,(90743, 51, 1105, N'<Row><F1>10547</F1><F2>P9403</F2><F3>388-390 Greenwich St. (Advisory)</F3><F4>586</F4><F5>388-390 Greenwich St
New York, NY</F5></Row>')
,(90744, 51, 1106, N'<Row><F1>10365</F1><F2>PAC4</F2><F3>Pacific Building </F3><F4>830</F4><F5>720 3rd Ave
Seattle, WA</F5></Row>')
,(90745, 51, 1107, N'<Row><F1>10764</F1><F2>PAF4</F2><F3>Palisades (Financing)</F3><F4>740</F4><F5>Palisades - Building A
5901 Peachtree Dunwoody Rd NE
Atlanta, GA</F5></Row>')
,(90746, 51, 1108, N'<Row><F1>10764</F1><F2>PAF4</F2><F3>Palisades (Financing)</F3><F4>741</F4><F5>Palisades - Building B
5901 Peachtree Dunwoody Rd NE
Atlanta, GA</F5></Row>')
,(90747, 51, 1109, N'<Row><F1>10764</F1><F2>PAF4</F2><F3>Palisades (Financing)</F3><F4>742</F4><F5>Palisades - Building C
5901 Peachtree Dunwoody Rd NE
Atlanta, GA</F5></Row>')
,(90748, 51, 1110, N'<Row><F1>10764</F1><F2>PAF4</F2><F3>Palisades (Financing)</F3><F4>744</F4><F5>Palisades - Building D
5909 Peachtree Dunwoody Rd NE
Atlanta, GA</F5></Row>')
,(90749, 51, 1111, N'<Row><F1>9450</F1><F2>PAL3</F2><F3>1 Palace St.</F3><F4>1015</F4><F5>1 Palace St
London, UK</F5></Row>')
,(90750, 51, 1112, N'<Row><F1>10076</F1><F2>PAL4</F2><F3>Palisades</F3><F4>740</F4><F5>Palisades - Building A
5901 Peachtree Dunwoody Rd NE
Atlanta, GA</F5></Row>')
,(90751, 51, 1113, N'<Row><F1>10076</F1><F2>PAL4</F2><F3>Palisades</F3><F4>741</F4><F5>Palisades - Building B
5901 Peachtree Dunwoody Rd NE
Atlanta, GA</F5></Row>')
,(90752, 51, 1114, N'<Row><F1>10076</F1><F2>PAL4</F2><F3>Palisades</F3><F4>742</F4><F5>Palisades - Building C
5901 Peachtree Dunwoody Rd NE
Atlanta, GA</F5></Row>')
,(90753, 51, 1115, N'<Row><F1>10076</F1><F2>PAL4</F2><F3>Palisades</F3><F4>744</F4><F5>Palisades - Building D
5909 Peachtree Dunwoody Rd NE
Atlanta, GA</F5></Row>')
,(90754, 51, 1116, N'<Row><F1>8612</F1><F2>PAM3</F2><F3>Plaza American Shopping Center</F3><F4>125</F4><F5>The Plaza America Shopping Center
11610-11694 Plaza America Dr
Reston, VA</F5></Row>')
,(90755, 51, 1117, N'<Row><F1>8599</F1><F2>PAT3</F2><F3>Park Avenue Tower</F3><F4>776</F4><F5>Park Avenue Tower
65 E 55th St
New York, NY</F5></Row>')
,(90756, 51, 1118, N'<Row><F1>8890</F1><F2>PAZ3</F2><F3>Palazzo Aporti </F3><F4>1214</F4><F5>Palazzo Aporti 
Via Ferrante Aporti 8
Milan, Italy</F5></Row>')
,(90757, 51, 1119, N'<Row><F1>8468</F1><F2>PBH2</F2><F3>The Peninsula Beverly Hills</F3><F4>1215</F4><F5>The Peninsula Beverly Hills
9882 S Santa Monica Blvd
Beverly Hills, CA</F5></Row>')
,(90758, 51, 1120, N'<Row><F1>8965</F1><F2>PCC3</F2><F3>Pacific City </F3><F4>954</F4><F5>Pacific City
Atlanta Ave &amp; Huntington St
Huntington Beach, CA</F5></Row>')
,(90759, 51, 1121, N'<Row><F1>10936</F1><F2>PCF4</F2><F3>Pacific Building (Financing)</F3><F4>830</F4><F5>720 3rd Ave
Seattle, WA</F5></Row>')
,(90760, 51, 1122, N'<Row><F1>8854</F1><F2>PEN3</F2><F3>Peninsula Town Center</F3><F4>275</F4><F5>Peninsula Town Center
1800 W Mercury Blvd
Hampton, VA</F5></Row>')
,(90761, 51, 1123, N'<Row><F1>9903</F1><F2>PFP4</F2><F3>Pacific Place</F3><F4>132</F4><F5>12 4th St
San Francisco, CA</F5></Row>')
,(90762, 51, 1124, N'<Row><F1>9903</F1><F2>PFP4</F2><F3>Pacific Place</F3><F4>355</F4><F5>22 4th St
San Francisco, CA</F5></Row>')
,(90763, 51, 1125, N'<Row><F1>9903</F1><F2>PFP4</F2><F3>Pacific Place</F3><F4>409</F4><F5>26 4th St
San Francisco, CA</F5></Row>')
,(90764, 51, 1126, N'<Row><F1>9903</F1><F2>PFP4</F2><F3>Pacific Place</F3><F4>859</F4><F5>801 Market St
San Francisco, CA</F5></Row>')
,(90765, 51, 1127, N'<Row><F1>9903</F1><F2>PFP4</F2><F3>Pacific Place</F3><F4>873</F4><F5>815 Market St
San Francisco, CA</F5></Row>')
,(90766, 51, 1128, N'<Row><F1>9558</F1><F2>PGF3</F2><F3>1400 Page Mill </F3><F4>1036</F4><F5>1400 Page Mill Rd
Palo Alto, CA</F5></Row>')
,(90767, 51, 1129, N'<Row><F1>10615</F1><F2>PHF4</F2><F3>Park Hyatt Washington (Financing)</F3><F4>138</F4><F5>Park Hyatt Washington
1201 24th St NW
Washington, DC</F5></Row>')
,(90768, 51, 1130, N'<Row><F1>10292</F1><F2>PHL4</F2><F3>3535 Market Street</F3><F4>556</F4><F5>3535 Market St
Philadelphia, PA</F5></Row>')
,(90769, 51, 1131, N'<Row><F1>9972</F1><F2>PHW4</F2><F3>Park Hyatt Washington DC </F3><F4>138</F4><F5>Park Hyatt Washington
1201 24th St NW
Washington, DC</F5></Row>')
,(90770, 51, 1132, N'<Row><F1>10233</F1><F2>PIN4</F2><F3>Pinole Vista Crossing (Financing)</F3><F4>137</F4><F5>Pinole Vista Crossing 
1200-1398 Fitzgerald Dr
Pinole, CA</F5></Row>')
,(90771, 51, 1133, N'<Row><F1>10125</F1><F2>PJD4</F2><F3>Project Fenway</F3><F4>8</F4><F5>One Memorial Drive
1 Memorial Dr
Cambridge, MA</F5></Row>')
,(90772, 51, 1134, N'<Row><F1>10125</F1><F2>PJD4</F2><F3>Project Fenway</F3><F4>21</F4><F5>100 High St
Boston, MA</F5></Row>')
,(90773, 51, 1135, N'<Row><F1>10125</F1><F2>PJD4</F2><F3>Project Fenway</F3><F4>148</F4><F5>125 Summer St
Boston, MA</F5></Row>')
,(90774, 51, 1136, N'<Row><F1>10125</F1><F2>PJD4</F2><F3>Project Fenway</F3><F4>367</F4><F5>225 Franklin St
Boston, MA</F5></Row>')
,(90775, 51, 1137, N'<Row><F1>10125</F1><F2>PJD4</F2><F3>Project Fenway</F3><F4>748</F4><F5>60 State St
Boston, MA</F5></Row>')
,(90776, 51, 1138, N'<Row><F1>10551</F1><F2>PJF4</F2><F3>Project Fenway (Financing)</F3><F4>8</F4><F5>One Memorial Drive
1 Memorial Dr
Cambridge, MA</F5></Row>')
,(90777, 51, 1139, N'<Row><F1>10551</F1><F2>PJF4</F2><F3>Project Fenway (Financing)</F3><F4>367</F4><F5>225 Franklin St
Boston, MA</F5></Row>')
,(90778, 51, 1140, N'<Row><F1>10551</F1><F2>PJF4</F2><F3>Project Fenway (Financing)</F3><F4>748</F4><F5>60 State St
Boston, MA</F5></Row>')
,(90779, 51, 1141, N'<Row><F1>10602</F1><F2>PJS4</F2><F3>Project Acela (Sprout)</F3><F4>19</F4><F5>100 Federal St
Boston, MA</F5></Row>')
,(90780, 51, 1142, N'<Row><F1>10602</F1><F2>PJS4</F2><F3>Project Acela (Sprout)</F3><F4>437</F4><F5>Atlantic Wharf
290 Congress St
Boston, MA</F5></Row>')
,(90781, 51, 1143, N'<Row><F1>10602</F1><F2>PJS4</F2><F3>Project Acela (Sprout)</F3><F4>753</F4><F5>601 Lexington Ave
New York, NY</F5></Row>')
,(90782, 51, 1144, N'<Row><F1>10561</F1><F2>PJT4</F2><F3>Project 702 </F3><F4>47</F4><F5>101-121 Daggett Dr
San Jose, CA</F5></Row>')
,(90783, 51, 1145, N'<Row><F1>10561</F1><F2>PJT4</F2><F3>Project 702 </F3><F4>439</F4><F5>2911-2951 Zanker Rd
San Jose, CA</F5></Row>')
,(90784, 51, 1146, N'<Row><F1>10561</F1><F2>PJT4</F2><F3>Project 702 </F3><F4>443</F4><F5>2940-2960 N First St
San Jose, CA</F5></Row>')
,(90785, 51, 1147, N'<Row><F1>10561</F1><F2>PJT4</F2><F3>Project 702 </F3><F4>470</F4><F5>3010-3040 N First St
San Jose, CA</F5></Row>')
,(90786, 51, 1148, N'<Row><F1>10561</F1><F2>PJT4</F2><F3>Project 702 </F3><F4>620</F4><F5>41-61 Daggett Dr
San Jose, CA</F5></Row>')
,(90787, 51, 1149, N'<Row><F1>10561</F1><F2>PJT4</F2><F3>Project 702 </F3><F4>829</F4><F5>71-81 Daggett Dr
San Jose, CA</F5></Row>')
,(90788, 51, 1150, N'<Row><F1>10558</F1><F2>PLC4</F2><F3>The Park &amp; Lakewood Center</F3><F4>795</F4><F5>Lakewood Center
6801 N Capital of Texas Hwy
Austin, TX</F5></Row>')
,(90789, 51, 1151, N'<Row><F1>10558</F1><F2>PLC4</F2><F3>The Park &amp; Lakewood Center</F3><F4>881</F4><F5>The Park
8300 N Mopac Expy
Austin, TX</F5></Row>')
,(90790, 51, 1152, N'<Row><F1>10804</F1><F2>PLL4</F2><F3>The Park &amp; Lakewood I &amp; II (Financing)</F3><F4>795</F4><F5>Lakewood Center
6801 N Capital of Texas Hwy
Austin, TX</F5></Row>')
,(90791, 51, 1153, N'<Row><F1>10804</F1><F2>PLL4</F2><F3>The Park &amp; Lakewood I &amp; II (Financing)</F3><F4>881</F4><F5>The Park
8300 N Mopac Expy
Austin, TX</F5></Row>')
,(90792, 51, 1154, N'<Row><F1>9769</F1><F2>PLY3</F2><F3>Twelve555 Jefferson </F3><F4>1289</F4><F5>Twelve|555 Playa Vista
12555 W Jefferson Blvd
Los Angeles, CA</F5></Row>')
,(90793, 51, 1155, N'<Row><F1>9822</F1><F2>PMB3</F2><F3>Promenade Bolingbrook</F3><F4>1239</F4><F5>The Promenade Bolingbrook
631 E Boughton Rd
Bolingbrook, IL</F5></Row>')
,(90794, 51, 1156, N'<Row><F1>9070</F1><F2>PNF4</F2><F3>2001 Pennsylvania Ave, NW</F3><F4>314</F4><F5>2001 Pennsylvania Ave NW
Washington, DC</F5></Row>')
,(90795, 51, 1157, N'<Row><F1>10107</F1><F2>PNR4</F2><F3>PGA National Resort &amp; Spa </F3><F4>1216</F4><F5>PGA National Resort &amp; Spa
400 Ave of the Champions
Palm Beach Gardens, FL</F5></Row>')
,(90796, 51, 1158, N'<Row><F1>9550</F1><F2>PNZ3</F2><F3>3003 Washington Blvd.</F3><F4>1249</F4><F5>3003 Washington Blvd
Arlington, VA</F5></Row>')
,(90797, 51, 1159, N'<Row><F1>10146</F1><F2>POL4</F2><F3>One Poultry</F3><F4>1213</F4><F5>One Poultry
1 Poultry
London EC2R 8EJ, UK</F5></Row>')
,(90798, 51, 1160, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>73</F4><F5>10574 N 90th St
Scottsdale, AZ</F5></Row>')
,(90799, 51, 1161, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>76</F4><F5>1060 Frontenac Rd
Aurora, IL</F5></Row>')
,(90800, 51, 1162, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>124</F4><F5>1155 Brook Forest Ave
Shorewood, IL</F5></Row>')
,(90801, 51, 1163, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>215</F4><F5>1500 Busse Hwy
Elk Grove, IL</F5></Row>')
,(90802, 51, 1164, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>248</F4><F5>170 W North Ave
Northlake, IL</F5></Row>')
,(90803, 51, 1165, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>292</F4><F5>1900 W Golf Rd
Rolling Meadows, IL</F5></Row>')
,(90804, 51, 1166, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>303</F4><F5>1992 W Jefferson Ave
Naperville, IL</F5></Row>')
,(90805, 51, 1167, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>362</F4><F5>221 E Town Line Rd
Vernon Hills, IL</F5></Row>')
,(90806, 51, 1168, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>377</F4><F5>2306 E Lincoln Hwy
New Lenox, IL</F5></Row>')
,(90807, 51, 1169, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>383</F4><F5>235 E North Ave
Glendale Heights, IL</F5></Row>')
,(90808, 51, 1170, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>430</F4><F5>2810 US-34
Oswego, IL</F5></Row>')
,(90809, 51, 1171, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>578</F4><F5>380 S Rohlwing Rd
Addison, IL</F5></Row>')
,(90810, 51, 1172, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>588</F4><F5>3895 E Main St
St. Charles, IL</F5></Row>')
,(90811, 51, 1173, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>605</F4><F5>4020 W 95th St
Oak Lawn, IL</F5></Row>')
,(90812, 51, 1174, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>717</F4><F5>531 N Randall Rd
Batavia, IL</F5></Row>')
,(90813, 51, 1175, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>726</F4><F5>5532 S Harlem Ave
Summit, IL</F5></Row>')
,(90814, 51, 1176, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>762</F4><F5>611 E Golf Rd
Schaumburg, IL</F5></Row>')
,(90815, 51, 1177, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>777</F4><F5>65 S McClintock Dr
Tempe, AZ</F5></Row>')
,(90816, 51, 1178, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>892</F4><F5>855 Cog Cir
Crystal Lake, IL</F5></Row>')
,(90817, 51, 1179, N'<Row><F1>10980</F1><F2>POR4</F2><F3>Portillo''s Retail Sale-Leaseback </F3><F4>936</F4><F5>950 E Ogden Ave
Naperville, IL</F5></Row>')
,(90818, 51, 1180, N'<Row><F1>10398</F1><F2>PPA4</F2><F3>Pecan Park</F3><F4>83</F4><F5>10800 Pecan Park Blvd
Austin, TX</F5></Row>')
,(90819, 51, 1181, N'<Row><F1>10016</F1><F2>PPP4</F2><F3>Post Properties NY Portfolio</F3><F4>585</F4><F5>Post Luminaria
385 1st Ave
New York, NY</F5></Row>')
,(90820, 51, 1182, N'<Row><F1>10016</F1><F2>PPP4</F2><F3>Post Properties NY Portfolio</F3><F4>587</F4><F5>Post Toscana
389 E 89th St
New York, NY</F5></Row>')
,(90821, 51, 1183, N'<Row><F1>9820</F1><F2>PPS4</F2><F3>Pacific Place </F3><F4>788</F4><F5>Pacific Place 
660 Pine St
Seattle, WA</F5></Row>')
,(90822, 51, 1184, N'<Row><F1>9455</F1><F2>PPZ3</F2><F3>110 William Street</F3><F4>98</F4><F5>110 William St
New York, NY</F5></Row>')
,(90823, 51, 1185, N'<Row><F1>9441</F1><F2>PRK3</F2><F3>Parkside Towers </F3><F4>37</F4><F5>1001 E Hillside Blvd
Foster City, CA</F5></Row>')
,(90824, 51, 1186, N'<Row><F1>9441</F1><F2>PRK3</F2><F3>Parkside Towers </F3><F4>69</F4><F5>1051 E Hillside Blvd
Foster City, CA</F5></Row>')
,(90825, 51, 1187, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>1</F4><F5>1 Appold St
London, UK</F5></Row>')
,(90826, 51, 1188, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>3</F4><F5>Broadgate Club
1 Exchange Pl
London, UK</F5></Row>')
,(90827, 51, 1189, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>4</F4><F5>1 Finsbury Ave
London, UK</F5></Row>')
,(90828, 51, 1190, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>15</F4><F5>10 Exchange Sq
London, UK</F5></Row>')
,(90829, 51, 1191, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>23</F4><F5>100 Liverpool St
London, UK</F5></Row>')
,(90830, 51, 1192, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>133</F4><F5>1-2 Broadgate
London, UK</F5></Row>')
,(90831, 51, 1193, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>172</F4><F5>135 Bishopsgate
London, UK</F5></Row>')
,(90832, 51, 1194, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>225</F4><F5>155 Bishopsgate
London, UK</F5></Row>')
,(90833, 51, 1195, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>302</F4><F5>199 Bishopsgate
London, UK</F5></Row>')
,(90834, 51, 1196, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>308</F4><F5>2 Finsbury Ave
London, UK</F5></Row>')
,(90835, 51, 1197, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>318</F4><F5>201 Bishopsgate
London, UK</F5></Row>')
,(90836, 51, 1198, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>319</F4><F5>Broadgate Tower
201 Bishopsgate
London, UK</F5></Row>')
,(90837, 51, 1199, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>449</F4><F5>3 Finsbury Ave
London, UK</F5></Row>')
,(90838, 51, 1200, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>673</F4><F5>Broadwalk House
5 Appold St
London, UK</F5></Row>')
,(90839, 51, 1201, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>674</F4><F5>5 Broadgate
London, UK</F5></Row>')
,(90840, 51, 1202, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>960</F4><F5>Broadgate Cir
London, UK</F5></Row>')
,(90841, 51, 1203, N'<Row><F1>8592</F1><F2>PRL3</F2><F3>Broadgate (Project Pearl)</F3><F4>963</F4><F5>Exchange House, Primrose St
London, UK</F5></Row>')
,(90842, 51, 1204, N'<Row><F1>9281</F1><F2>PSC3</F2><F3>Princeville Center</F3><F4>722</F4><F5>Princeville Center
5-4280 Kuhio Hwy
Princeville, HI</F5></Row>')
,(90843, 51, 1205, N'<Row><F1>10627</F1><F2>PSN4</F2><F3>Plaza at Scripps Northridge</F3><F4>1058</F4><F5>The Plaza at Scripps Northridge
12121 Scripps Summit Dr
San Diego, CA</F5></Row>')
,(90844, 51, 1206, N'<Row><F1>9720</F1><F2>PST4</F2><F3>The Park at San Tan</F3><F4>484</F4><F5>3075 W Ray Rd
Chandler, AZ</F5></Row>')
,(90845, 51, 1207, N'<Row><F1>9720</F1><F2>PST4</F2><F3>The Park at San Tan</F3><F4>519</F4><F5>3225 W Ray Rd
Chandler, AZ</F5></Row>')
,(90846, 51, 1208, N'<Row><F1>9720</F1><F2>PST4</F2><F3>The Park at San Tan</F3><F4>521</F4><F5>3235 W Ray Rd
Chandler, AZ</F5></Row>')
,(90847, 51, 1209, N'<Row><F1>9720</F1><F2>PST4</F2><F3>The Park at San Tan</F3><F4>522</F4><F5>3245 W Ray Rd
Chandler, AZ</F5></Row>')
,(90848, 51, 1210, N'<Row><F1>9902</F1><F2>PTF3</F2><F3>Parkside Towers (Financing)</F3><F4>37</F4><F5>1001 E Hillside Blvd
Foster City, CA</F5></Row>')
,(90849, 51, 1211, N'<Row><F1>9902</F1><F2>PTF3</F2><F3>Parkside Towers (Financing)</F3><F4>69</F4><F5>1051 E Hillside Blvd
Foster City, CA</F5></Row>')
,(90850, 51, 1212, N'<Row><F1>9842</F1><F2>PUT3</F2><F3>1700 East Putnam Avenue </F3><F4>1039</F4><F5>1700 E Putnam Ave
Old Greenwich, CT</F5></Row>')
,(90851, 51, 1213, N'<Row><F1>9376</F1><F2>PVC3</F2><F3>Pinole Vista Crossing </F3><F4>137</F4><F5>Pinole Vista Crossing 
1200-1398 Fitzgerald Dr
Pinole, CA</F5></Row>')
,(90852, 51, 1214, N'<Row><F1>11191</F1><F2>PWP4</F2><F3>Project Whisper</F3><F4>1306</F4><F5>Three Bryant Park
1095 Avenue of the Americas
New York, NY</F5></Row>')
,(90853, 51, 1215, N'<Row><F1>10009</F1><F2>PYD4</F2><F3>The Pruneyard</F3><F4>286</F4><F5>1875 S Bascom Ave
Campbell, CA</F5></Row>')
,(90854, 51, 1216, N'<Row><F1>10009</F1><F2>PYD4</F2><F3>The Pruneyard</F3><F4>293</F4><F5>1901 S Bascom Ave
Campbell, CA</F5></Row>')
,(90855, 51, 1217, N'<Row><F1>10009</F1><F2>PYD4</F2><F3>The Pruneyard</F3><F4>296</F4><F5>1919 S Bascom Ave
Campbell, CA</F5></Row>')
,(90856, 51, 1218, N'<Row><F1>10009</F1><F2>PYD4</F2><F3>The Pruneyard</F3><F4>304</F4><F5>1995 S Bascom Ave
Campbell, CA</F5></Row>')
,(90857, 51, 1219, N'<Row><F1>10009</F1><F2>PYD4</F2><F3>The Pruneyard</F3><F4>307</F4><F5>1999 S Bascom Ave
Campbell, CA</F5></Row>')
,(90858, 51, 1220, N'<Row><F1>10576</F1><F2>QKF4</F2><F3>Queen Ka''ahumanu Center </F3><F4>1240</F4><F5>Queen Ka''ahumanu Center
275 W Kaahumanu Ave
Kahului, HI</F5></Row>')
,(90859, 51, 1221, N'<Row><F1>9931</F1><F2>RBH4</F2><F3>Rowes Wharf Office/Boston Harbor Hotel</F3><F4>480</F4><F5>30-50 Rowes Wharf
Boston, MA</F5></Row>')
,(90860, 51, 1222, N'<Row><F1>9931</F1><F2>RBH4</F2><F3>Rowes Wharf Office/Boston Harbor Hotel</F3><F4>804</F4><F5>Boston Harbor Hotel
70 Rowes Wharf
Boston, MA</F5></Row>')
,(90861, 51, 1223, N'<Row><F1>9004</F1><F2>RBP3</F2><F3>Radisson Blu Philadelphia </F3><F4>357</F4><F5>Radisson Blu Philadelphia
220 S 17th St
Philadelphia, PA</F5></Row>')
,(90862, 51, 1224, N'<Row><F1>8525</F1><F2>RDW2</F2><F3>Radisson Warwick (Radisson Blue Philadelphia)</F3><F4>357</F4><F5>Radisson Blu Philadelphia
220 S 17th St
Philadelphia, PA</F5></Row>')
,(90863, 51, 1225, N'<Row><F1>9686</F1><F2>RIO3</F2><F3>Rio Robles Technology Park</F3><F4>1241</F4><F5>40-50 Rio Robles
San Jose, CA</F5></Row>')
,(90864, 51, 1226, N'<Row><F1>9686</F1><F2>RIO3</F2><F3>Rio Robles Technology Park</F3><F4>1242</F4><F5>70 Rio Robles
San Jose, CA</F5></Row>')
,(90865, 51, 1227, N'<Row><F1>9686</F1><F2>RIO3</F2><F3>Rio Robles Technology Park</F3><F4>1243</F4><F5>3553 N 1st St
San Jose, CA</F5></Row>')
,(90866, 51, 1228, N'<Row><F1>9686</F1><F2>RIO3</F2><F3>Rio Robles Technology Park</F3><F4>1244</F4><F5>130-134 Rio Robles
San Jose, CA</F5></Row>')
,(90867, 51, 1229, N'<Row><F1>9686</F1><F2>RIO3</F2><F3>Rio Robles Technology Park</F3><F4>1245</F4><F5>110 Rio Robles
San Jose, CA</F5></Row>')
,(90868, 51, 1230, N'<Row><F1>9686</F1><F2>RIO3</F2><F3>Rio Robles Technology Park</F3><F4>1246</F4><F5>90 Rio Robles
San Jose, CA</F5></Row>')
,(90869, 51, 1231, N'<Row><F1>9686</F1><F2>RIO3</F2><F3>Rio Robles Technology Park</F3><F4>1247</F4><F5>30 Rio Robles
San Jose, CA</F5></Row>')
,(90870, 51, 1232, N'<Row><F1>9686</F1><F2>RIO3</F2><F3>Rio Robles Technology Park</F3><F4>1248</F4><F5>3545 N 1st St
San Jose, CA</F5></Row>')
,(90871, 51, 1233, N'<Row><F1>8404</F1><F2>RLP2</F2><F3>Rancho Las Palmas </F3><F4>628</F4><F5>Rancho Las Palmas Shopping Center
42-540 Bob Hope Dr
Rancho Mirage, CA</F5></Row>')
,(90872, 51, 1234, N'<Row><F1>10079</F1><F2>ROP4</F2><F3>River Oaks Parkway</F3><F4>340</F4><F5>211 River Oaks Pkwy
San Jose, CA</F5></Row>')
,(90873, 51, 1235, N'<Row><F1>10079</F1><F2>ROP4</F2><F3>River Oaks Parkway</F3><F4>399</F4><F5>251 River Oaks Pkwy
San Jose, CA</F5></Row>')
,(90874, 51, 1236, N'<Row><F1>10079</F1><F2>ROP4</F2><F3>River Oaks Parkway</F3><F4>429</F4><F5>281 River Oaks Pkwy
San Jose, CA</F5></Row>')
,(90875, 51, 1237, N'<Row><F1>10034</F1><F2>RPF4</F2><F3>Riverpark II </F3><F4>1253</F4><F5>Riverpark Tower II
300 Park Ave
San Jose, CA</F5></Row>')
,(90876, 51, 1238, N'<Row><F1>10448</F1><F2>RPL4</F2><F3>River Place Corporate Center </F3><F4>779</F4><F5>River Place Corporate Center - Building I
6500 River Place Blvd
Austin, TX</F5></Row>')
,(90877, 51, 1239, N'<Row><F1>10448</F1><F2>RPL4</F2><F3>River Place Corporate Center </F3><F4>780</F4><F5>River Place Corporate Center - Building II
6500 River Place Blvd
Austin, TX</F5></Row>')
,(90878, 51, 1240, N'<Row><F1>10448</F1><F2>RPL4</F2><F3>River Place Corporate Center </F3><F4>781</F4><F5>River Place Corporate Center - Building III
6500 River Place Blvd
Austin, TX</F5></Row>')
,(90879, 51, 1241, N'<Row><F1>10448</F1><F2>RPL4</F2><F3>River Place Corporate Center </F3><F4>782</F4><F5>River Place Corporate Center - Building IV
6500 River Place Blvd
Austin, TX</F5></Row>')
,(90880, 51, 1242, N'<Row><F1>10448</F1><F2>RPL4</F2><F3>River Place Corporate Center </F3><F4>783</F4><F5>River Place Corporate Center - Building V
6500 River Place Blvd
Austin, TX</F5></Row>')
,(90881, 51, 1243, N'<Row><F1>10448</F1><F2>RPL4</F2><F3>River Place Corporate Center </F3><F4>784</F4><F5>River Place Corporate Center - Building VI
6500 River Place Blvd
Austin, TX</F5></Row>')
,(90882, 51, 1244, N'<Row><F1>10448</F1><F2>RPL4</F2><F3>River Place Corporate Center </F3><F4>785</F4><F5>River Place Corporate Center - Building VII
6500 River Place Blvd
Austin, TX</F5></Row>')
,(90883, 51, 1245, N'<Row><F1>9126</F1><F2>RSC3</F2><F3>Royal Saint Charles</F3><F4>174</F4><F5>Royal St. Charles Hotel
135 St Charles Ave
New Orleans, LA</F5></Row>')
,(90884, 51, 1246, N'<Row><F1>10944</F1><F2>RSQ4</F2><F3>Reston Square</F3><F4>1049</F4><F5>Reston Square
11790 Sunrise Valley Dr
Reston, VA</F5></Row>')
,(90885, 51, 1247, N'<Row><F1>10211</F1><F2>RWC4</F2><F3>River West Office Campus (Harpo Campus)</F3><F4>97</F4><F5>110 N Carpenter St
Chicago, IL</F5></Row>')
,(90886, 51, 1248, N'<Row><F1>10211</F1><F2>RWC4</F2><F3>River West Office Campus (Harpo Campus)</F3><F4>114</F4><F5>1115 W Washington Blvd
Chicago, IL</F5></Row>')
,(90887, 51, 1249, N'<Row><F1>10211</F1><F2>RWC4</F2><F3>River West Office Campus (Harpo Campus)</F3><F4>117</F4><F5>113 N May St
Chicago, IL</F5></Row>')
,(90888, 51, 1250, N'<Row><F1>10211</F1><F2>RWC4</F2><F3>River West Office Campus (Harpo Campus)</F3><F4>129</F4><F5>118-122 N Aberdeen St
Chicago, IL</F5></Row>')
,(90889, 51, 1251, N'<Row><F1>10591</F1><F2>RWF4</F2><F3>Riverworks</F3><F4>1254</F4><F5>Riverworks
480 Pleasant St
Watertown, MA</F5></Row>')
,(90890, 51, 1252, N'<Row><F1>10232</F1><F2>SAC4</F2><F3>300 Capitol Mall</F3><F4>1025</F4><F5>300 Capitol Mall
Sacramento, CA</F5></Row>')
,(90891, 51, 1253, N'<Row><F1>10648</F1><F2>SAV4</F2><F3>The Savoy London </F3><F4>1281</F4><F5>The Savoy Hotel
Strand, Savoy Way
London WC2R 0EU, UK</F5></Row>')
,(90892, 51, 1254, N'<Row><F1>10019</F1><F2>SAW4</F2><F3>Marriott Sawgrass </F3><F4>32</F4><F5>Marriott Sawgrass Golf Resort &amp; Spa
1000 PGA Tour Blvd
Ponte Vedra Beach, FL</F5></Row>')
,(90893, 51, 1255, N'<Row><F1>9547</F1><F2>SBP3</F2><F3>70 Battery Place (RiverWatch)</F3><F4>803</F4><F5>Riverwatch
70 Battery Pl
New York, NY</F5></Row>')
,(90894, 51, 1256, N'<Row><F1>9691</F1><F2>SBS3</F2><F3>1075 Southbridge St.</F3><F4>80</F4><F5>1075 Southbridge St
Worcester, MA</F5></Row>')
,(90895, 51, 1257, N'<Row><F1>10399</F1><F2>SDC4</F2><F3>San Diego Creative Collection</F3><F4>85</F4><F5>10905 Technology Pl
San Diego, CA</F5></Row>')
,(90896, 51, 1258, N'<Row><F1>10399</F1><F2>SDC4</F2><F3>San Diego Creative Collection</F3><F4>86</F4><F5>10907 Technology Pl
San Diego, CA</F5></Row>')
,(90897, 51, 1259, N'<Row><F1>10399</F1><F2>SDC4</F2><F3>San Diego Creative Collection</F3><F4>87</F4><F5>10908 Technology Pl
San Diego, CA</F5></Row>')
,(90898, 51, 1260, N'<Row><F1>10399</F1><F2>SDC4</F2><F3>San Diego Creative Collection</F3><F4>88</F4><F5>10911 Technology Pl
San Diego, CA</F5></Row>')
,(90899, 51, 1261, N'<Row><F1>10399</F1><F2>SDC4</F2><F3>San Diego Creative Collection</F3><F4>89</F4><F5>10915 Technology Pl
San Diego, CA</F5></Row>')
,(90900, 51, 1262, N'<Row><F1>10399</F1><F2>SDC4</F2><F3>San Diego Creative Collection</F3><F4>90</F4><F5>10918 Technology Pl
San Diego, CA</F5></Row>')
,(90901, 51, 1263, N'<Row><F1>10399</F1><F2>SDC4</F2><F3>San Diego Creative Collection</F3><F4>91</F4><F5>10919 Technology Pl
San Diego, CA</F5></Row>')
,(90902, 51, 1264, N'<Row><F1>10399</F1><F2>SDC4</F2><F3>San Diego Creative Collection</F3><F4>92</F4><F5>10929 Technology Pl
San Diego, CA</F5></Row>')
,(90903, 51, 1265, N'<Row><F1>10399</F1><F2>SDC4</F2><F3>San Diego Creative Collection</F3><F4>93</F4><F5>10939 Technology Pl
San Diego, CA</F5></Row>')
,(90904, 51, 1266, N'<Row><F1>10399</F1><F2>SDC4</F2><F3>San Diego Creative Collection</F3><F4>94</F4><F5>10949 Technology Pl
San Diego, CA</F5></Row>')
,(90905, 51, 1267, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>41</F4><F5>10055 Barnes Canyon Rd
San Diego, CA</F5></Row>')
,(90906, 51, 1268, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>42</F4><F5>10065 Barnes Canyon Rd
San Diego, CA</F5></Row>')
,(90907, 51, 1269, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>43</F4><F5>10075 Barnes Canyon Rd
San Diego, CA</F5></Row>')
,(90908, 51, 1270, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>939</F4><F5>9605 Scranton Rd
San Diego, CA</F5></Row>')
,(90909, 51, 1271, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>941</F4><F5>9645 Scranton Rd
San Diego, CA</F5></Row>')
,(90910, 51, 1272, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>943</F4><F5>9675 Scranton Rd
San Diego, CA</F5></Row>')
,(90911, 51, 1273, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>944</F4><F5>9685 Scranton Rd
San Diego, CA</F5></Row>')
,(90912, 51, 1274, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>947</F4><F5>9725 Scranton Rd
San Diego, CA</F5></Row>')
,(90913, 51, 1275, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>948</F4><F5>9735 Scranton Rd
San Diego, CA</F5></Row>')
,(90914, 51, 1276, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>950</F4><F5>9805 Scranton Rd
San Diego, CA</F5></Row>')
,(90915, 51, 1277, N'<Row><F1>9499</F1><F2>SDT4</F2><F3>San Diego Tech Center</F3><F4>951</F4><F5>9855 Scranton Rd
San Diego, CA</F5></Row>')
,(90916, 51, 1278, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>115</F4><F5>11201 SE 8th St
Bellevue, WA</F5></Row>')
,(90917, 51, 1279, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>123</F4><F5>1150 114th Ave SE
Bellevue, WA</F5></Row>')
,(90918, 51, 1280, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>143</F4><F5>1203 114th Ave SE
Bellevue, WA</F5></Row>')
,(90919, 51, 1281, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>145</F4><F5>1215 114th Ave SE
Bellevue, WA</F5></Row>')
,(90920, 51, 1282, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>156</F4><F5>1300 114th Ave SE
Bellevue, WA</F5></Row>')
,(90921, 51, 1283, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>161</F4><F5>1309 114th Ave SE
Bellevue, WA</F5></Row>')
,(90922, 51, 1284, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>188</F4><F5>1400 12th Ave SE
Bellevue, WA</F5></Row>')
,(90923, 51, 1285, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>202</F4><F5>1450 114th Ave SE
Bellevue, WA</F5></Row>')
,(90924, 51, 1286, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>214</F4><F5>1500 114th Ave SE
Bellevue, WA</F5></Row>')
,(90925, 51, 1287, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>230</F4><F5>1601 114th Ave SE
Bellevue, WA</F5></Row>')
,(90926, 51, 1288, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>234</F4><F5>1621 114th Ave SE
Bellevue, WA</F5></Row>')
,(90927, 51, 1289, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>245</F4><F5>1687 114th Ave SE
Bellevue, WA</F5></Row>')
,(90928, 51, 1290, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>257</F4><F5>1715 114th Ave SE
Bellevue, WA</F5></Row>')
,(90929, 51, 1291, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>264</F4><F5>1756 114th Ave SE
Bellevue, WA</F5></Row>')
,(90930, 51, 1292, N'<Row><F1>10529</F1><F2>SEA4</F2><F3>Walton Street Seattle Portfolio</F3><F4>271</F4><F5>1800 114th Ave SE
Bellevue, WA</F5></Row>')
,(90931, 51, 1293, N'<Row><F1>10307</F1><F2>SEV4</F2><F3>70 Park Avenue Hotel </F3><F4>1018</F4><F5>70 Park Ave
New York, NY</F5></Row>')
,(90932, 51, 1294, N'<Row><F1>9655</F1><F2>SFP3</F2><F3>Sheraton Four Points </F3><F4>140</F4><F5>Twelve &amp; K Hotel Washington DC
1201 K St NW
Washington, DC</F5></Row>')
,(90933, 51, 1295, N'<Row><F1>10881</F1><F2>SHF4</F2><F3>South Towne Center (Financing) </F3><F4>65</F4><F5>South Towne Center
10450 S State St
Sandy, UT</F5></Row>')
,(90934, 51, 1296, N'<Row><F1>9000</F1><F2>SHH3</F2><F3>SoHo House Miami Beach </F3><F4>1260</F4><F5>Soho Beach House
4385 Collins Ave
Miami Beach, FL</F5></Row>')
,(90935, 51, 1297, N'<Row><F1>10983</F1><F2>SHH4</F2><F3>600 California</F3><F4>749</F4><F5>600 California St
San Francisco, CA</F5></Row>')
,(90936, 51, 1298, N'<Row><F1>9827</F1><F2>SHL3</F2><F3>Shoreline Center </F3><F4>158</F4><F5>130-150 Shoreline Dr
Redwood City, CA</F5></Row>')
,(90937, 51, 1299, N'<Row><F1>9343</F1><F2>SHV3</F2><F3>Shops at Highland Village </F3><F4>250</F4><F5>The Shops at Highland Village
1701 Shoal Creek
Highland Village, TX</F5></Row>')
,(90938, 51, 1300, N'<Row><F1>9035</F1><F2>SJC4</F2><F3>St. Johns Town Center</F3><F4>657</F4><F5>St. Johns Town Center
4663 River City Dr
Jacksonville, FL</F5></Row>')
,(90939, 51, 1301, N'<Row><F1>10443</F1><F2>SMC4</F2><F3>San Mateo Centre</F3><F4>272</F4><F5>San Mateo Centre
1800 Gateway Dr
San Mateo, CA</F5></Row>')
,(90940, 51, 1302, N'<Row><F1>10443</F1><F2>SMC4</F2><F3>San Mateo Centre</F3><F4>278</F4><F5>San Mateo Centre
1810 Gateway Dr
San Mateo, CA</F5></Row>')
,(90941, 51, 1303, N'<Row><F1>10443</F1><F2>SMC4</F2><F3>San Mateo Centre</F3><F4>281</F4><F5>San Mateo Centre
1820 Gateway Dr
San Mateo, CA</F5></Row>')
,(90942, 51, 1304, N'<Row><F1>10786</F1><F2>SMF4</F2><F3>San Mateo Center (Financing)</F3><F4>272</F4><F5>San Mateo Centre
1800 Gateway Dr
San Mateo, CA</F5></Row>')
,(90943, 51, 1305, N'<Row><F1>10786</F1><F2>SMF4</F2><F3>San Mateo Center (Financing)</F3><F4>278</F4><F5>San Mateo Centre
1810 Gateway Dr
San Mateo, CA</F5></Row>')
,(90944, 51, 1306, N'<Row><F1>10786</F1><F2>SMF4</F2><F3>San Mateo Center (Financing)</F3><F4>281</F4><F5>San Mateo Centre
1820 Gateway Dr
San Mateo, CA</F5></Row>')
,(90945, 51, 1307, N'<Row><F1>10683</F1><F2>SMP4</F2><F3>Project Legacy Financing</F3><F4>60</F4><F5>The Mall at Wellington Green
10300 W Forest Hill Blvd
Wellington, FL</F5></Row>')
,(90946, 51, 1308, N'<Row><F1>10683</F1><F2>SMP4</F2><F3>Project Legacy Financing</F3><F4>261</F4><F5>The Mall at Partridge Creek
17420 Hall Rd
Charter Township of Clinton, MI</F5></Row>')
,(90947, 51, 1309, N'<Row><F1>10683</F1><F2>SMP4</F2><F3>Project Legacy Financing</F3><F4>289</F4><F5>Fairlane Town Center
18900 Michigan Ave
Dearborn, MI</F5></Row>')
,(90948, 51, 1310, N'<Row><F1>10683</F1><F2>SMP4</F2><F3>Project Legacy Financing</F3><F4>455</F4><F5>MacArthur Center
300 Monticello Ave
Norfolk, VA</F5></Row>')
,(90949, 51, 1311, N'<Row><F1>10683</F1><F2>SMP4</F2><F3>Project Legacy Financing</F3><F4>764</F4><F5>The Shops at Willow Bend
6121 W Park Blvd
Plano, TX</F5></Row>')
,(90950, 51, 1312, N'<Row><F1>10683</F1><F2>SMP4</F2><F3>Project Legacy Financing</F3><F4>796</F4><F5>Northlake Mall
6801 Northlake Mall Dr
Charlotte, NC</F5></Row>')
,(90951, 51, 1313, N'<Row><F1>10683</F1><F2>SMP4</F2><F3>Project Legacy Financing</F3><F4>922</F4><F5>Stony Point Fashion Park
9200 Stony Point Pkwy
Richmond, VA</F5></Row>')
,(90952, 51, 1314, N'<Row><F1>10217</F1><F2>SNY4</F2><F3>Sofitel New York</F3><F4>640</F4><F5>Hotel Sofitel New York
44 W 44th St
New York, NY</F5></Row>')
,(90953, 51, 1315, N'<Row><F1>8771</F1><F2>SOB3</F2><F3>Shutters on the Beach &amp; Casa Del Mar</F3><F4>1100</F4><F5>Casa del Mar
1910 Ocean Way
Santa Monica, CA</F5></Row>')
,(90954, 51, 1316, N'<Row><F1>8771</F1><F2>SOB3</F2><F3>Shutters on the Beach &amp; Casa Del Mar</F3><F4>1101</F4><F5>Shutters on the Beach
Pico Blvd
Santa Monica, CA</F5></Row>')
,(90955, 51, 1317, N'<Row><F1>10856</F1><F2>SOF4</F2><F3>605 Third Ave.</F3><F4>1310</F4><F5>605 Third Ave
New York, NY</F5></Row>')
,(90956, 51, 1318, N'<Row><F1>10662</F1><F2>SPB4</F2><F3>San Pedro Bank Lofts </F3><F4>610</F4><F5>407 W 7th St
San Pedro, CA</F5></Row>')
,(90957, 51, 1319, N'<Row><F1>10656</F1><F2>SPC4</F2><F3>Seaport Center </F3><F4>649</F4><F5>Seaport Center
451 D St
Boston, MA</F5></Row>')
,(90958, 51, 1320, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1261</F4><F5>1918 8th Ave
Seattle, WA</F5></Row>')
,(90959, 51, 1321, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1262</F4><F5>818 Stewart Ave
Seattle, WA</F5></Row>')
,(90960, 51, 1322, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1263</F4><F5>Advanta Office Commons - Building A
3007 160th Ave SE
Bellevue, WA</F5></Row>')
,(90961, 51, 1323, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1264</F4><F5>Advanta Office Commons - Building B
3009 160th Ave SE
Bellevue, WA</F5></Row>')
,(90962, 51, 1324, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1265</F4><F5>Advanta Office Commons - Building C
3003 160th Ave SE
Bellevue, WA</F5></Row>')
,(90963, 51, 1325, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1266</F4><F5>The Louisa
123 NW 12th Ave
Portland, OR</F5></Row>')
,(90964, 51, 1326, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1267</F4><F5>Brewery Block 1
1207-1235 W Burnside St
Portland, OR</F5></Row>')
,(90965, 51, 1327, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1268</F4><F5>Brewery Block 2
1120 NW Couch St
Portland, OR</F5></Row>')
,(90966, 51, 1328, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1269</F4><F5>Brewery Block 4
1115-1139 NW Couch St
Portland, OR</F5></Row>')
,(90967, 51, 1329, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1270</F4><F5>Brewery Block 3
1001-1039 NW Couch St
Portland, OR</F5></Row>')
,(90968, 51, 1330, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1271</F4><F5>Sunnyvale City Center - Building 1
190 Mathilda Pl
Sunnyvale, CA</F5></Row>')
,(90969, 51, 1331, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1272</F4><F5>Sunnyvale City Center - Building 2
150 Mathilda Pl
Sunnyvale, CA</F5></Row>')
,(90970, 51, 1332, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1273</F4><F5>Sunnyvale City Center - Building 3
100 Mathilda Pl
Sunnyvale, CA</F5></Row>')
,(90971, 51, 1333, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1274</F4><F5>700 Concar Dr
San Mateo, CA</F5></Row>')
,(90972, 51, 1334, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1275</F4><F5>800 Concar Dr
San Mateo, CA</F5></Row>')
,(90973, 51, 1335, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1276</F4><F5>900 Concar Dr
San Mateo, CA</F5></Row>')
,(90974, 51, 1336, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1277</F4><F5>The Amalfi at Hermann Park
3 Hermann Museum Circle Dr
Houston, TX</F5></Row>')
,(90975, 51, 1337, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1278</F4><F5>The Esplanade
3 Hermann Museum Circle Dr
Houston, TX</F5></Row>')
,(90976, 51, 1338, N'<Row><F1>9356</F1><F2>SPP3</F2><F3>SPF Portfolio</F3><F4>1279</F4><F5>Gaslight Commons
28 W 3rd St
South Orange, NJ</F5></Row>')
,(90977, 51, 1339, N'<Row><F1>6222</F1><F2>SRM1</F2><F3>St Regis Monarch Beach Resort </F3><F4>9</F4><F5>St. Regis Monarch Beach Resort &amp; Spa
1 Monarch Beach Resort N
Dana Point, CA</F5></Row>')
,(90978, 51, 1340, N'<Row><F1>10357</F1><F2>SRM4</F2><F3>St. Regis Monarch Beach (Financing)</F3><F4>9</F4><F5>St. Regis Monarch Beach Resort &amp; Spa
1 Monarch Beach Resort N
Dana Point, CA</F5></Row>')
,(90979, 51, 1341, N'<Row><F1>10801</F1><F2>SSF4</F2><F3>7700 Parmer </F3><F4>1043</F4><F5>7700 Parmer - Building A
7700 W Parmer Ln
Austin, TX</F5></Row>')
,(90980, 51, 1342, N'<Row><F1>10801</F1><F2>SSF4</F2><F3>7700 Parmer </F3><F4>1044</F4><F5>7700 Parmer - Building B
7700 W Parmer Ln
Austin, TX</F5></Row>')
,(90981, 51, 1343, N'<Row><F1>10801</F1><F2>SSF4</F2><F3>7700 Parmer </F3><F4>1045</F4><F5>7700 Parmer - Building C
7700 W Parmer Ln
Austin, TX</F5></Row>')
,(90982, 51, 1344, N'<Row><F1>10801</F1><F2>SSF4</F2><F3>7700 Parmer </F3><F4>1046</F4><F5>7700 Parmer - Building D
7700 W Parmer Ln
Austin, TX</F5></Row>')
,(90983, 51, 1345, N'<Row><F1>9919</F1><F2>SSV3</F2><F3>The Shops at SkyView Center </F3><F4>1258</F4><F5>The Shops at Skyview Center
40-24 College Point Blvd
Flushing, NY</F5></Row>')
,(90984, 51, 1346, N'<Row><F1>9795</F1><F2>STO3</F2><F3>720 Olive</F3><F4>831</F4><F5>720 Olive Way
Seattle, WA</F5></Row>')
,(90985, 51, 1347, N'<Row><F1>9402</F1><F2>STR3</F2><F3>Strawberry Creek </F3><F4>875</F4><F5>8211 Bruceville Rd
Sacramento, CA</F5></Row>')
,(90986, 51, 1348, N'<Row><F1>9402</F1><F2>STR3</F2><F3>Strawberry Creek </F3><F4>876</F4><F5>8221 Timberlake Way
Sacramento, CA</F5></Row>')
,(90987, 51, 1349, N'<Row><F1>9402</F1><F2>STR3</F2><F3>Strawberry Creek </F3><F4>877</F4><F5>8231 Timberlake Way
Sacramento, CA</F5></Row>')
,(90988, 51, 1350, N'<Row><F1>9402</F1><F2>STR3</F2><F3>Strawberry Creek </F3><F4>879</F4><F5>8241 Bruceville Rd
Sacramento, CA</F5></Row>')
,(90989, 51, 1351, N'<Row><F1>9402</F1><F2>STR3</F2><F3>Strawberry Creek </F3><F4>880</F4><F5>8251 Bruceville Rd
Sacramento, CA</F5></Row>')
,(90990, 51, 1352, N'<Row><F1>10133</F1><F2>STS4</F2><F3>28 State Street</F3><F4>427</F4><F5>28 State St
Boston, MA</F5></Row>')
,(90991, 51, 1353, N'<Row><F1>10087</F1><F2>SWG4</F2><F3>The Shoppes at Webb Gin</F3><F4>175</F4><F5>The Shoppes at Webb Gin
1350 Scenic Hwy
Snellville, GA</F5></Row>')
,(90992, 51, 1354, N'<Row><F1>10250</F1><F2>SYN4</F2><F3>700 E. Middlefield Rd &amp; 1101 W. Maude Ave </F3><F4>100</F4><F5>1101 W Maude Ave
Sunnyvale, CA</F5></Row>')
,(90993, 51, 1355, N'<Row><F1>10250</F1><F2>SYN4</F2><F3>700 E. Middlefield Rd &amp; 1101 W. Maude Ave </F3><F4>810</F4><F5>700 E Middlefield Rd
Mountain View, CA</F5></Row>')
,(90994, 51, 1356, N'<Row><F1>9883</F1><F2>TAF3</F2><F3>The Avallon (Financing)</F3><F4>1082</F4><F5>The Avallon - Building I
10415 Morado Cir
Austin, TX</F5></Row>')
,(90995, 51, 1357, N'<Row><F1>9883</F1><F2>TAF3</F2><F3>The Avallon (Financing)</F3><F4>1083</F4><F5>The Avallon - Building II
10415 Morado Cir
Austin, TX</F5></Row>')
,(90996, 51, 1358, N'<Row><F1>9883</F1><F2>TAF3</F2><F3>The Avallon (Financing)</F3><F4>1084</F4><F5>The Avallon - Building III
10415 Morado Cir
Austin, TX</F5></Row>')
,(90997, 51, 1359, N'<Row><F1>9883</F1><F2>TAF3</F2><F3>The Avallon (Financing)</F3><F4>1085</F4><F5>The Avallon - Building IV
10814 Jollyville Rd
Austin, TX</F5></Row>')
,(90998, 51, 1360, N'<Row><F1>9883</F1><F2>TAF3</F2><F3>The Avallon (Financing)</F3><F4>1086</F4><F5>The Avallon - Building V
10431 Morado Cir
Austin, TX</F5></Row>')
,(90999, 51, 1361, N'<Row><F1>9883</F1><F2>TAF3</F2><F3>The Avallon (Financing)</F3><F4>1087</F4><F5>The Avallon - Building VI
10415 Morado Cir
Austin, TX</F5></Row>')
,(91000, 51, 1362, N'<Row><F1>9189</F1><F2>TBB3</F2><F3>Tower Burbank</F3><F4>590</F4><F5>Tower Burbank
3900 W Alameda Ave
Burbank, CA</F5></Row>')
,(91001, 51, 1363, N'<Row><F1>9471</F1><F2>TCF3</F2><F3>Carneros Inn (Financing)</F3><F4>607</F4><F5>Carneros Inn
4048 Sonoma Hwy
Napa, CA</F5></Row>')
,(91002, 51, 1364, N'<Row><F1>10736</F1><F2>TCF4</F2><F3>201 California (Financing)</F3><F4>320</F4><F5>201 California St
San Francisco, CA</F5></Row>')
,(91003, 51, 1365, N'<Row><F1>8936</F1><F2>TCH3</F2><F3>The Claremont Hotel Club &amp; Spa</F3><F4>613</F4><F5>Claremont Hotel Club &amp; Spa
41 Tunnel Road
Berkeley, CA</F5></Row>')
,(91004, 51, 1366, N'<Row><F1>8803</F1><F2>TCI3</F2><F3>Carneros Inn</F3><F4>607</F4><F5>Carneros Inn
4048 Sonoma Hwy
Napa, CA</F5></Row>')
,(91005, 51, 1367, N'<Row><F1>10779</F1><F2>TCM4</F2><F3>Town Center of Mililani</F3><F4>1293</F4><F5>Town Center of Mililani
95-1249 Meheula Pkwy #193
Mililani, HI</F5></Row>')
,(91006, 51, 1368, N'<Row><F1>9876</F1><F2>TEE4</F2><F3>388-390 Greenwich St. (Project Umbrella)</F3><F4>586</F4><F5>388-390 Greenwich St
New York, NY</F5></Row>')
,(91007, 51, 1369, N'<Row><F1>9022</F1><F2>TEM4</F2><F3>311 West Monroe</F3><F4>494</F4><F5>311 W Monroe
Chicago, IL</F5></Row>')
,(91008, 51, 1370, N'<Row><F1>8318</F1><F2>TEW3</F2><F3>311 South Wacker</F3><F4>493</F4><F5>311 S Wacker Dr
Chicago, IL</F5></Row>')
,(91009, 51, 1371, N'<Row><F1>10750</F1><F2>TFA4</F2><F3>1345 Avenue of the Americas </F3><F4>1309</F4><F5>1345 Avenue of the Americas
New York, NY</F5></Row>')
,(91010, 51, 1372, N'<Row><F1>10541</F1><F2>TFF4</F2><F3>Three First National Plaza (Financing)</F3><F4>805</F4><F5>Three First National Plaza
70 West Madison St
Chicago, IL</F5></Row>')
,(91011, 51, 1373, N'<Row><F1>9237</F1><F2>TFN4</F2><F3>Three First National Plaza</F3><F4>805</F4><F5>Three First National Plaza
70 West Madison St
Chicago, IL</F5></Row>')
,(91012, 51, 1374, N'<Row><F1>9274</F1><F2>TFP3</F2><F3>2941 Fairview Park</F3><F4>1042</F4><F5>2941 Fairview Park Dr
Falls Church, VA</F5></Row>')
,(91013, 51, 1375, N'<Row><F1>10477</F1><F2>TFP4</F2><F3>235 Pine</F3><F4>384</F4><F5>235 Pine St
San Francisco, CA</F5></Row>')
,(91014, 51, 1376, N'<Row><F1>10528</F1><F2>TGP4</F2><F3>2025 Gateway Place </F3><F4>324</F4><F5>2025 Gateway Pl
San Jose, CA</F5></Row>')
,(91015, 51, 1377, N'<Row><F1>9641</F1><F2>TMS3</F2><F3>31 Milk Street </F3><F4>487</F4><F5>31 Milk St
Boston, MA</F5></Row>')
,(91016, 51, 1378, N'<Row><F1>10733</F1><F2>TOF4</F2><F3>Hotel Tomo (Financing)</F3><F4>274</F4><F5>Hotel Tomo
1800 Sutter Street
San Francisco, CA</F5></Row>')
,(91017, 51, 1379, N'<Row><F1>10260</F1><F2>TOL4</F2><F3>Central Park at Toluca Lake</F3><F4>1102</F4><F5>Central Park at Toluca Lake
3500 W Olive Way
Burbank, CA</F5></Row>')
,(91018, 51, 1380, N'<Row><F1>10454</F1><F2>TOM4</F2><F3>Hotel Tomo</F3><F4>274</F4><F5>Hotel Tomo
1800 Sutter Street
San Francisco, CA</F5></Row>')
,(91019, 51, 1381, N'<Row><F1>10140</F1><F2>TOO4</F2><F3>201 California Street</F3><F4>320</F4><F5>201 California St
San Francisco, CA</F5></Row>')
,(91020, 51, 1382, N'<Row><F1>9987</F1><F2>TOW4</F2><F3>Tower Burbank </F3><F4>590</F4><F5>Tower Burbank
3900 W Alameda Ave
Burbank, CA</F5></Row>')
,(91021, 51, 1383, N'<Row><F1>9730</F1><F2>TPC3</F2><F3>Torrey Pines Court </F3><F4>1282</F4><F5>10350 N Torrey Pines Rd
La Jolla, CA</F5></Row>')
,(91022, 51, 1384, N'<Row><F1>9730</F1><F2>TPC3</F2><F3>Torrey Pines Court </F3><F4>1283</F4><F5>3333 N Torrey Pines Ct
La Jolla, CA</F5></Row>')
,(91023, 51, 1385, N'<Row><F1>9730</F1><F2>TPC3</F2><F3>Torrey Pines Court </F3><F4>1284</F4><F5>3344 N Torrey Pines Ct
La Jolla, CA</F5></Row>')
,(91024, 51, 1386, N'<Row><F1>9730</F1><F2>TPC3</F2><F3>Torrey Pines Court </F3><F4>1285</F4><F5>3366 N Torrey Pines Ct
La Jolla, CA</F5></Row>')
,(91025, 51, 1387, N'<Row><F1>9730</F1><F2>TPC3</F2><F3>Torrey Pines Court </F3><F4>1286</F4><F5>3377 N Torrey Pines Ct
La Jolla, CA</F5></Row>')
,(91026, 51, 1388, N'<Row><F1>9599</F1><F2>TPN3</F2><F3>2001 Pennsylvania, NW</F3><F4>314</F4><F5>2001 Pennsylvania Ave NW
Washington, DC</F5></Row>')
,(91027, 51, 1389, N'<Row><F1>9885</F1><F2>TRM3</F2><F3>The Rim</F3><F4>263</F4><F5>The Rim
17503 Interstate 10 Frontage Rd
San Antonio, TX</F5></Row>')
,(91028, 51, 1390, N'<Row><F1>7395</F1><F2>TSQ2</F2><F3>Town Square Las Vegas  </F3><F4>1287</F4><F5>Town Square Las Vegas
6605 S Las Vegas Blvd
Las Vegas, NV</F5></Row>')
,(91029, 51, 1391, N'<Row><F1>9925</F1><F2>TTF4</F2><F3>234 E 46th St.</F3><F4>380</F4><F5>234 E 46th St
New York, NY</F5></Row>')
,(91030, 51, 1392, N'<Row><F1>9733</F1><F2>TTH3</F2><F3>330 Hudson</F3><F4>527</F4><F5>330 Hudson St
New York, NY</F5></Row>')
,(91031, 51, 1393, N'<Row><F1>8575</F1><F2>TTM3</F2><F3>221 Main Street</F3><F4>363</F4><F5>221 Main St
San Francisco, CA</F5></Row>')
,(91032, 51, 1394, N'<Row><F1>9544</F1><F2>TUS3</F2><F3>Tuscan Inn</F3><F4>1288</F4><F5>The Tuscan
425 North Point St
San Francisco, CA</F5></Row>')
,(91033, 51, 1395, N'<Row><F1>9777</F1><F2>TWE3</F2><F3>Tysons West</F3><F4>216</F4><F5>1500 Cornerside Blvd
Vienna, VA</F5></Row>')
,(91034, 51, 1396, N'<Row><F1>8781</F1><F2>TWS3</F2><F3>30 Winter Street</F3><F4>452</F4><F5>30 Winter St
Boston, MA</F5></Row>')
,(91035, 51, 1397, N'<Row><F1>10526</F1><F2>TWT4</F2><F3>245-249 West 17th Street </F3><F4>390</F4><F5>245-249 W 17th St
New York, NY</F5></Row>')
,(91036, 51, 1398, N'<Row><F1>9452</F1><F2>UBC3</F2><F3>US Bank Center</F3><F4>45</F4><F5>101 N First Ave
Phoenix, AZ</F5></Row>')
,(91037, 51, 1399, N'<Row><F1>10749</F1><F2>UBC4</F2><F3>US Bank Center </F3><F4>45</F4><F5>101 N First Ave
Phoenix, AZ</F5></Row>')
,(91038, 51, 1400, N'<Row><F1>8295</F1><F2>UPN3</F2><F3>Uptown Newport</F3><F4>645</F4><F5>4450 Jamboree Rd
Newport Beach, CA</F5></Row>')
,(91039, 51, 1401, N'<Row><F1>9703</F1><F2>UTA4</F2><F3>UTA Plaza &amp; The Ice House</F3><F4>929</F4><F5>9336 Civic Center Dr
Beverly Hills, CA</F5></Row>')
,(91040, 51, 1402, N'<Row><F1>9703</F1><F2>UTA4</F2><F3>UTA Plaza &amp; The Ice House</F3><F4>931</F4><F5>9346 Civic Center Dr
Beverly Hills, CA</F5></Row>')
,(91041, 51, 1403, N'<Row><F1>9703</F1><F2>UTA4</F2><F3>UTA Plaza &amp; The Ice House</F3><F4>932</F4><F5>9348 Civic Center Dr
Beverly Hills, CA</F5></Row>')
,(91042, 51, 1404, N'<Row><F1>10667</F1><F2>UTF4</F2><F3>UTA Plaza </F3><F4>929</F4><F5>9336 Civic Center Dr
Beverly Hills, CA</F5></Row>')
,(91043, 51, 1405, N'<Row><F1>10667</F1><F2>UTF4</F2><F3>UTA Plaza </F3><F4>931</F4><F5>9346 Civic Center Dr
Beverly Hills, CA</F5></Row>')
,(91044, 51, 1406, N'<Row><F1>10337</F1><F2>UWS4</F2><F3>219-223 West 77th Street </F3><F4>352</F4><F5>219-223 W 77th St
New York, NY</F5></Row>')
,(91045, 51, 1407, N'<Row><F1>9143</F1><F2>VAN3</F2><F3>525 West Van Buren </F3><F4>1028</F4><F5>525 W Van Buren St
Chicago, IL</F5></Row>')
,(91046, 51, 1408, N'<Row><F1>8187</F1><F2>VBT2</F2><F3>470 Vanderbilt Avenue </F3><F4>659</F4><F5>470 Vanderbilt Ave
Brooklyn, NY</F5></Row>')
,(91047, 51, 1409, N'<Row><F1>8920</F1><F2>WAI3</F2><F3>Marriott Wailea </F3><F4>567</F4><F5>Marriott Wailea Beach Resort &amp; Spa
3700 Wailea Alanui Dr
Kihei, HI</F5></Row>')
,(91048, 51, 1410, N'<Row><F1>10358</F1><F2>WAI4</F2><F3>Shops at Wailea</F3><F4>1259</F4><F5>The Shops at Wailea
3750 Wailea Alanui Dr
Kihei, HI</F5></Row>')
,(91049, 51, 1411, N'<Row><F1>9797</F1><F2>WAL3</F2><F3>2450 and 2500 Walsh Avenue </F3><F4>389</F4><F5>2450 Walsh Ave
Santa Clara, CA</F5></Row>')
,(91050, 51, 1412, N'<Row><F1>9797</F1><F2>WAL3</F2><F3>2450 and 2500 Walsh Avenue </F3><F4>397</F4><F5>2500 Walsh Ave
Santa Clara, CA</F5></Row>')
,(91051, 51, 1413, N'<Row><F1>6773</F1><F2>WAR2</F2><F3>Westin Aruba Resort &amp; Casino</F3><F4>967</F4><F5>J.E. Irausquin Blvd 77
Aruba</F5></Row>')
,(91052, 51, 1414, N'<Row><F1>10195</F1><F2>WAR4</F2><F3>One Warrington Place </F3><F4>14</F4><F5>One Warrington Place 
1 Warrington Pl
Dublin, Ireland</F5></Row>')
,(91053, 51, 1415, N'<Row><F1>8507</F1><F2>WAT2</F2><F3>Wateridge Plaza </F3><F4>52</F4><F5>10201 Wateridge Cir
San Diego, CA</F5></Row>')
,(91054, 51, 1416, N'<Row><F1>8507</F1><F2>WAT2</F2><F3>Wateridge Plaza </F3><F4>53</F4><F5>10221 Wateridge Cir
San Diego, CA</F5></Row>')
,(91055, 51, 1417, N'<Row><F1>8507</F1><F2>WAT2</F2><F3>Wateridge Plaza </F3><F4>54</F4><F5>10241 Wateridge Cir
San Diego, CA</F5></Row>')
,(91056, 51, 1418, N'<Row><F1>11007</F1><F2>WBF4</F2><F3>3001 Washington Blvd.</F3><F4>1249</F4><F5>3003 Washington Blvd
Arlington, VA</F5></Row>')
,(91057, 51, 1419, N'<Row><F1>8001</F1><F2>WBL4</F2><F3>Woodbury Lakes</F3><F4>915</F4><F5>Woodbury Lakes
9020 Hudson Rd
St Paul, MN</F5></Row>')
,(91058, 51, 1420, N'<Row><F1>10494</F1><F2>WBW4</F2><F3>375 West Broadway (Financing)</F3><F4>573</F4><F5>375 W Broadway
New York, NY</F5></Row>')
,(91059, 51, 1421, N'<Row><F1>9734</F1><F2>WBY3</F2><F3>375 West Broadway</F3><F4>573</F4><F5>375 W Broadway
New York, NY</F5></Row>')
,(91060, 51, 1422, N'<Row><F1>9589</F1><F2>WCM3</F2><F3>Warner Center Marriott </F3><F4>351</F4><F5>Warner Center Marriott Woodland Hills
21850 Oxnard St
Woodland Hills, CA</F5></Row>')
,(91061, 51, 1423, N'<Row><F1>10297</F1><F2>WDF4</F2><F3>Westin Diplomat (Financing)</F3><F4>1291</F4><F5>Diplomat Resort &amp; Spa Hollywood
3555 S Ocean Dr
Hollywood, FL</F5></Row>')
,(91062, 51, 1424, N'<Row><F1>8774</F1><F2>WEM3</F2><F3>Wembley Student House</F3><F4>974</F4><F5>Victoria Hill London
N End Rd
London, UK</F5></Row>')
,(91063, 51, 1425, N'<Row><F1>7813</F1><F2>WES4</F2><F3>Shops at West End</F3><F4>1298</F4><F5>The Shops at West End
1621 West End Blvd
St. Louis Park, MN</F5></Row>')
,(91064, 51, 1426, N'<Row><F1>10387</F1><F2>WFD4</F2><F3>Wells Fargo Center</F3><F4>1297</F4><F5>Wells Fargo Center
1700 Lincoln St
Denver, CO</F5></Row>')
,(91065, 51, 1427, N'<Row><F1>10170</F1><F2>WFF4</F2><F3>114 West 41st Street </F3><F4>1022</F4><F5>114 W 41st St
New York, NY</F5></Row>')
,(91066, 51, 1428, N'<Row><F1>9058</F1><F2>WIL3</F2><F3>Willowsford</F3><F4>964</F4><F5>Founders Dr
Ashburn, VA</F5></Row>')
,(91067, 51, 1429, N'<Row><F1>8538</F1><F2>WMF3</F2><F3>Watermark Seaport (Financing) </F3><F4>975</F4><F5>Watermark Seaport
Seaport Blvd &amp; Boston Wharf Rd
Boston, MA</F5></Row>')
,(91068, 51, 1430, N'<Row><F1>8496</F1><F2>WMK3</F2><F3>Watermark Seaport Equity </F3><F4>975</F4><F5>Watermark Seaport
Seaport Blvd &amp; Boston Wharf Rd
Boston, MA</F5></Row>')
,(91069, 51, 1431, N'<Row><F1>10409</F1><F2>WOO4</F2><F3>Woolgate Exchange</F3><F4>394</F4><F5>Woolgate Exchange
25 Basinghall Ave
London, UK</F5></Row>')
,(91070, 51, 1432, N'<Row><F1>10033</F1><F2>WPB4</F2><F3>CityPlace Tower </F3><F4>712</F4><F5>CityPlace Tower
525 Okeechobee Blvd
West Palm Beach, FL</F5></Row>')
,(91071, 51, 1433, N'<Row><F1>9905</F1><F2>WPF3</F2><F3>Washington Post HQ </F3><F4>1290</F4><F5>1150 15th St NW
Washington, DC</F5></Row>')
,(91072, 51, 1434, N'<Row><F1>9110</F1><F2>WRD4</F2><F3>Weeks Robinson Dallas Portfolio </F3><F4>511</F4><F5>3201 E Arkansas Ln
Arlington, TX</F5></Row>')
,(91073, 51, 1435, N'<Row><F1>9110</F1><F2>WRD4</F2><F3>Weeks Robinson Dallas Portfolio </F3><F4>515</F4><F5>3221 E Arkansas Ln
Arlington, TX</F5></Row>')
,(91074, 51, 1436, N'<Row><F1>9110</F1><F2>WRD4</F2><F3>Weeks Robinson Dallas Portfolio </F3><F4>658</F4><F5>4675 Railhead Rd
Fort Worth, TX</F5></Row>')
,(91075, 51, 1437, N'<Row><F1>9110</F1><F2>WRD4</F2><F3>Weeks Robinson Dallas Portfolio </F3><F4>666</F4><F5>4800 Langdon Rd
Dallas, TX</F5></Row>')
,(91076, 51, 1438, N'<Row><F1>9110</F1><F2>WRD4</F2><F3>Weeks Robinson Dallas Portfolio </F3><F4>669</F4><F5>4900 Langdon Rd
Dallas, TX</F5></Row>')
,(91077, 51, 1439, N'<Row><F1>9110</F1><F2>WRD4</F2><F3>Weeks Robinson Dallas Portfolio </F3><F4>864</F4><F5>801-821 Railhead Rd
Fort Worth, TX</F5></Row>')
,(91078, 51, 1440, N'<Row><F1>9151</F1><F2>WSH3</F2><F3>Washington Square</F3><F4>28</F4><F5>100 S Washington Ave
Minneapolis, MN</F5></Row>')
,(91079, 51, 1441, N'<Row><F1>9151</F1><F2>WSH3</F2><F3>Washington Square</F3><F4>108</F4><F5>111 S Washington Ave
Minneapolis, MN</F5></Row>')
,(91080, 51, 1442, N'<Row><F1>9151</F1><F2>WSH3</F2><F3>Washington Square</F3><F4>310</F4><F5>20 S Washington Ave
Minneapolis, MN</F5></Row>')
,(91081, 51, 1443, N'<Row><F1>9478</F1><F2>WTC3</F2><F3>World Trade Center East</F3><F4>364</F4><F5>World Trade Center East
2211 Elloitt Ave
Seattle, WA</F5></Row>')
,(91082, 51, 1444, N'<Row><F1>8815</F1><F2>WTE3</F2><F3>Water''s Edge</F3><F4>1050</F4><F5>5510 Lincoln Blvd
Playa Vista, CA</F5></Row>')
,(91083, 51, 1445, N'<Row><F1>8815</F1><F2>WTE3</F2><F3>Water''s Edge</F3><F4>1051</F4><F5>5570 Lincoln Blvd
Playa Vista, CA</F5></Row>')
,(91084, 51, 1446, N'<Row><F1>9783</F1><F2>WTF3</F2><F3>Wateridge (Financing)</F3><F4>52</F4><F5>10201 Wateridge Cir
San Diego, CA</F5></Row>')
,(91085, 51, 1447, N'<Row><F1>9783</F1><F2>WTF3</F2><F3>Wateridge (Financing)</F3><F4>53</F4><F5>10221 Wateridge Cir
San Diego, CA</F5></Row>')
,(91086, 51, 1448, N'<Row><F1>9783</F1><F2>WTF3</F2><F3>Wateridge (Financing)</F3><F4>54</F4><F5>10241 Wateridge Cir
San Diego, CA</F5></Row>')
,(91087, 51, 1449, N'<Row><F1>10040</F1><F2>WTF4</F2><F3>Waterfront Plaza</F3><F4>249</F4><F5>1700 Montgomery St
San Francisco, CA</F5></Row>')
,(91088, 51, 1450, N'<Row><F1>10040</F1><F2>WTF4</F2><F3>Waterfront Plaza</F3><F4>273</F4><F5>1800 Montgomery St
San Francisco, CA</F5></Row>')
,(91089, 51, 1451, N'<Row><F1>10040</F1><F2>WTF4</F2><F3>Waterfront Plaza</F3><F4>678</F4><F5>50 Francisco St
San Francisco, CA</F5></Row>')
,(91090, 51, 1452, N'<Row><F1>10040</F1><F2>WTF4</F2><F3>Waterfront Plaza</F3><F4>746</F4><F5>60 Francisco St
San Francisco, CA</F5></Row>')
,(91091, 51, 1453, N'<Row><F1>10209</F1><F2>WTR4</F2><F3>Waterstone Shopping Center </F3><F4>945</F4><F5>Waterstone Shopping Center 
9691 Waterstone Blvd
Cincinnati, OH</F5></Row>')
,(91092, 51, 1454, N'<Row><F1>8792</F1><F2>INP3</F2><F3>International Plaza</F3><F4>1311</F4><F5>International Plaza
2223 North Westshore Blvd
Tampa, FL</F5></Row>')
,(91093, 51, 1455, N'<Row><F1>8847</F1><F2>STS3</F2><F3>7 Times Square (Times Square Tower)</F3><F4>1312</F4><F5>Times Square Tower
7 Times Sq
New York, NY</F5></Row>')
,(91094, 51, 1456, N'<Row><F1>6237</F1><F2>FOP1</F2><F3>Four Oaks Place </F3><F4>1313</F4><F5>1300 Post Oak Blvd
Houston, TX</F5></Row>')
,(91095, 51, 1457, N'<Row><F1>6237</F1><F2>FOP1</F2><F3>Four Oaks Place </F3><F4>1314</F4><F5>1330 Post Oak Blvd
Houston, TX</F5></Row>')
,(91096, 51, 1458, N'<Row><F1>6237</F1><F2>FOP1</F2><F3>Four Oaks Place </F3><F4>1315</F4><F5>1360 Post Oak Blvd
Houston, TX</F5></Row>')
,(91097, 51, 1459, N'<Row><F1>6237</F1><F2>FOP1</F2><F3>Four Oaks Place </F3><F4>1316</F4><F5>1400 Post Oak Blvd
Houston, TX</F5></Row>')
,(91098, 51, 1460, N'<Row><F1>8660</F1><F2>TWC3</F2><F3>Time Warner Center</F3><F4>1317</F4><F5>Time Warner Center
10 Columbus Cir
New York, NY</F5></Row>')
,(91099, 51, 1461, N'<Row><F1>10556</F1><F2>PPR4</F2><F3>Project Prom </F3><F4>1318</F4><F5>Waldorf Astoria New York
301 Park Ave
New York, NY</F5></Row>')
,(91100, 51, 1462, N'<Row><F1>10143</F1><F2>SLR4</F2><F3>199 South Los Robles</F3><F4>1319</F4><F5>199 S Los Robles
Pasadena, CA</F5></Row>')
,(91101, 51, 1463, N'<Row><F1>11168</F1><F2>TAC4</F2><F3>837 Washington </F3><F4>1320</F4><F5>837 Washington St
New York, NY</F5></Row>')
,(91102, 51, 1464, N'<Row><F1>10660</F1><F2>AVE4</F2><F3>1095 Avenue of the Americas</F3><F4>1306</F4><F5>Three Bryant Park
1095 Avenue of the Americas
New York, NY</F5></Row>')
,(91103, 51, 1465, N'<Row><F1>8720</F1><F2>HTC3</F2><F3>Hotel Carter</F3><F4>1321</F4><F5>Hotel Carter
250 W 43rd St
New York, NY</F5></Row>')
,(91104, 51, 1466, N'<Row><F1>10210</F1><F2>PMP4</F2><F3>Prominent Pointe</F3><F4>1322</F4><F5>Prominent Pointe - Building 1
8310 N Capital of Texas Hwy
Austin, TX</F5></Row>')
,(91105, 51, 1467, N'<Row><F1>10210</F1><F2>PMP4</F2><F3>Prominent Pointe</F3><F4>1323</F4><F5>Prominent Pointe - Building 2
8310 N Capital of Texas Hwy
Austin, TX</F5></Row>')
,(91106, 51, 1468, N'<Row><F1>11082</F1><F2>HIF4</F2><F3>Hyatt Regency Indianapolis</F3><F4>1324</F4><F5>Hyatt Regency Indianapolis
1 S Capitol Ave
Indianapolis, IN</F5></Row>')
,(91107, 51, 1469, N'<Row><F1>11047</F1><F2>RIV4</F2><F3>River Place Corporate Center (Financing)</F3><F4>779</F4><F5>River Place Corporate Center - Building I
6500 River Place Blvd
Austin, TX</F5></Row>')
,(91108, 51, 1470, N'<Row><F1>11047</F1><F2>RIV4</F2><F3>River Place Corporate Center (Financing)</F3><F4>780</F4><F5>River Place Corporate Center - Building II
6500 River Place Blvd
Austin, TX</F5></Row>')
,(91109, 51, 1471, N'<Row><F1>11047</F1><F2>RIV4</F2><F3>River Place Corporate Center (Financing)</F3><F4>781</F4><F5>River Place Corporate Center - Building III
6500 River Place Blvd
Austin, TX</F5></Row>')
,(91110, 51, 1472, N'<Row><F1>11047</F1><F2>RIV4</F2><F3>River Place Corporate Center (Financing)</F3><F4>782</F4><F5>River Place Corporate Center - Building IV
6500 River Place Blvd
Austin, TX</F5></Row>')
,(91111, 51, 1473, N'<Row><F1>11047</F1><F2>RIV4</F2><F3>River Place Corporate Center (Financing)</F3><F4>783</F4><F5>River Place Corporate Center - Building V
6500 River Place Blvd
Austin, TX</F5></Row>')
,(91112, 51, 1474, N'<Row><F1>11047</F1><F2>RIV4</F2><F3>River Place Corporate Center (Financing)</F3><F4>784</F4><F5>River Place Corporate Center - Building VI
6500 River Place Blvd
Austin, TX</F5></Row>')
,(91113, 51, 1475, N'<Row><F1>11047</F1><F2>RIV4</F2><F3>River Place Corporate Center (Financing)</F3><F4>785</F4><F5>River Place Corporate Center - Building VII
6500 River Place Blvd
Austin, TX</F5></Row>')
,(91114, 51, 1476, N'<Row><F1>11201</F1><F2>SPF4</F2><F3>Sammamish Parkplace (Financing)</F3><F4>1325</F4><F5>Sammamish Parkplace - Building C
21925 SE 51st St
Issaquah, WA</F5></Row>')
,(91115, 51, 1477, N'<Row><F1>11201</F1><F2>SPF4</F2><F3>Sammamish Parkplace (Financing)</F3><F4>1326</F4><F5>Sammamish Parkplace - Building D
21933 SE 51st St
Issaquah, WA</F5></Row>')
,(91116, 51, 1478, N'<Row><F1>11201</F1><F2>SPF4</F2><F3>Sammamish Parkplace (Financing)</F3><F4>1327</F4><F5>Sammamish Parkplace - Building E
21930 SE 51st St
Issaquah, WA</F5></Row>')
,(91117, 52, 1, N'<Row><F1>DealRoleid</F1><F2>DealRole</F2></Row>')
,(91118, 52, 2, N'<Row><F1>1</F1><F2>Seller</F2></Row>')
,(91119, 52, 3, N'<Row><F1>2</F1><F2>Buyer</F2></Row>')
,(91120, 52, 4, N'<Row><F1>3</F1><F2>Borrower</F2></Row>')
,(91121, 52, 5, N'<Row><F1>4</F1><F2>Lender</F2></Row>')
,(91122, 53, 1, N'<Row><F1>Propid</F1><F2>PropName</F2><F3>Address</F3><F4>City</F4><F5>StateCountry</F5><F6>PropString</F6></Row>')
,(91123, 53, 2, N'<Row><F1>1</F1><F3>1 Appold St</F3><F4>London</F4><F5>UK</F5><F6>1 Appold St
London, UK</F6></Row>')
,(91124, 53, 3, N'<Row><F1>2</F1><F2>One Beacon Street</F2><F3>1 Beacon St</F3><F4>Boston</F4><F5>MA</F5><F6>One Beacon Street
1 Beacon St
Boston, MA</F6></Row>')
,(91125, 53, 4, N'<Row><F1>1212</F1><F2>One Canal Park</F2><F3>1 Canal Park</F3><F4>Cambridge</F4><F5>MA</F5><F6>One Canal Park
1 Canal Park
Cambridge, MA</F6></Row>')
,(91126, 53, 5, N'<Row><F1>1081</F1><F2>The Atlantis Resort</F2><F3>1 Casino Dr</F3><F4>Paradise Island</F4><F5>Bahamas</F5><F6>The Atlantis Resort
1 Casino Dr
Paradise Island, Bahamas</F6></Row>')
,(91127, 53, 6, N'<Row><F1>3</F1><F2>Broadgate Club</F2><F3>1 Exchange Pl</F3><F4>London</F4><F5>UK</F5><F6>Broadgate Club
1 Exchange Pl
London, UK</F6></Row>')
,(91128, 53, 7, N'<Row><F1>4</F1><F3>1 Finsbury Ave</F3><F4>London</F4><F5>UK</F5><F6>1 Finsbury Ave
London, UK</F6></Row>')
,(91129, 53, 8, N'<Row><F1>5</F1><F2>Residence Inn Harrisburg Carlisle</F2><F3>1 Hampton Ct</F3><F4>Carlisle</F4><F5>PA</F5><F6>Residence Inn Harrisburg Carlisle
1 Hampton Ct
Carlisle, PA</F6></Row>')
,(91130, 53, 9, N'<Row><F1>6</F1><F2>Harvest Inn</F2><F3>1 Main St</F3><F4>St. Helena</F4><F5>CA</F5><F6>Harvest Inn
1 Main St
St. Helena, CA</F6></Row>')
,(91131, 53, 10, N'<Row><F1>7</F1><F2>One Market Plaza</F2><F3>1 Market St</F3><F4>San Francisco</F4><F5>CA</F5><F6>One Market Plaza
1 Market St
San Francisco, CA</F6></Row>')
,(91132, 53, 11, N'<Row><F1>8</F1><F2>One Memorial Drive</F2><F3>1 Memorial Dr</F3><F4>Cambridge</F4><F5>MA</F5><F6>One Memorial Drive
1 Memorial Dr
Cambridge, MA</F6></Row>')
,(91133, 53, 12, N'<Row><F1>9</F1><F2>St. Regis Monarch Beach Resort &amp; Spa</F2><F3>1 Monarch Beach Resort N</F3><F4>Dana Point</F4><F5>CA</F5><F6>St. Regis Monarch Beach Resort &amp; Spa
1 Monarch Beach Resort N
Dana Point, CA</F6></Row>')
,(91134, 53, 13, N'<Row><F1>10</F1><F2>Intercontinental Mark Hopkins San Francisco</F2><F3>1 Nob Hill Pl</F3><F4>San Francisco</F4><F5>CA</F5><F6>Intercontinental Mark Hopkins San Francisco
1 Nob Hill Pl
San Francisco, CA</F6></Row>')
,(91135, 53, 14, N'<Row><F1>1062</F1><F2>One Ocean Resort &amp; Spa</F2><F3>1 Ocean Blvd</F3><F4>Atlantic Beach </F4><F5>FL</F5><F6>One Ocean Resort &amp; Spa
1 Ocean Blvd
Atlantic Beach , FL</F6></Row>')
,(91136, 53, 15, N'<Row><F1>1015</F1><F3>1 Palace St</F3><F4>London</F4><F5>UK</F5><F6>1 Palace St
London, UK</F6></Row>')
,(91137, 53, 16, N'<Row><F1>11</F1><F2>One Park Avenue</F2><F3>1 Park Ave</F3><F4>New York</F4><F5>NY</F5><F6>One Park Avenue
1 Park Ave
New York, NY</F6></Row>')
,(91138, 53, 17, N'<Row><F1>1131</F1><F2>First Tower</F2><F3>1 Place des Saisons</F3><F4>92400 Courbevoie</F4><F5>France</F5><F6>First Tower
1 Place des Saisons
92400 Courbevoie, France</F6></Row>')
,(91139, 53, 18, N'<Row><F1>1213</F1><F2>One Poultry</F2><F3>1 Poultry</F3><F4>London EC2R 8EJ</F4><F5>UK</F5><F6>One Poultry
1 Poultry
London EC2R 8EJ, UK</F6></Row>')
,(91140, 53, 19, N'<Row><F1>12</F1><F2>One Research Drive</F2><F3>1 Research Dr</F3><F4>Westborough</F4><F5>MA</F5><F6>One Research Drive
1 Research Dr
Westborough, MA</F6></Row>')
,(91141, 53, 20, N'<Row><F1>1091</F1><F2>One Thorndale</F2><F3>1 Thorndale Dr</F3><F4>San Rafael</F4><F5>CA</F5><F6>One Thorndale
1 Thorndale Dr
San Rafael, CA</F6></Row>')
,(91142, 53, 21, N'<Row><F1>13</F1><F2>Landmark One</F2><F3>1 Van de Graaff Dr</F3><F4>Burlington</F4><F5>MA</F5><F6>Landmark One
1 Van de Graaff Dr
Burlington, MA</F6></Row>')
,(91143, 53, 22, N'<Row><F1>14</F1><F2>One Warrington Place </F2><F3>1 Warrington Pl</F3><F4>Dublin</F4><F5>Ireland</F5><F6>One Warrington Place 
1 Warrington Pl
Dublin, Ireland</F6></Row>')
,(91144, 53, 23, N'<Row><F1>15</F1><F3>10 Exchange Sq</F3><F4>London</F4><F5>UK</F5><F6>10 Exchange Sq
London, UK</F6></Row>')
,(91145, 53, 24, N'<Row><F1>16</F1><F3>10 S Van Ness</F3><F4>San Francisco</F4><F5>CA</F5><F6>10 S Van Ness
San Francisco, CA</F6></Row>')
,(91146, 53, 25, N'<Row><F1>1095</F1><F3>100 B St</F3><F4>Santa Rosa</F4><F5>CA</F5><F6>100 B St
Santa Rosa, CA</F6></Row>')
,(91147, 53, 26, N'<Row><F1>1250</F1><F3>100 Brompton Rd</F3><F4>Knightsbridge, London SW3 1ER</F4><F5>UK</F5><F6>100 Brompton Rd
Knightsbridge, London SW3 1ER, UK</F6></Row>')
,(91148, 53, 27, N'<Row><F1>17</F1><F3>100 California St</F3><F4>San Francisco</F4><F5>CA</F5><F6>100 California St
San Francisco, CA</F6></Row>')
,(91149, 53, 28, N'<Row><F1>18</F1><F2>Family Dollar</F2><F3>100 East Fisher Ave</F3><F4>Sabinal</F4><F5>TX</F5><F6>Family Dollar
100 East Fisher Ave
Sabinal, TX</F6></Row>')
,(91150, 53, 29, N'<Row><F1>19</F1><F3>100 Federal St</F3><F4>Boston</F4><F5>MA</F5><F6>100 Federal St
Boston, MA</F6></Row>')
,(91151, 53, 30, N'<Row><F1>20</F1><F2>Bayside Corporate Center II</F2><F3>100 Foster City Blvd</F3><F4>Foster City</F4><F5>CA</F5><F6>Bayside Corporate Center II
100 Foster City Blvd
Foster City, CA</F6></Row>')
,(91152, 53, 31, N'<Row><F1>21</F1><F3>100 High St</F3><F4>Boston</F4><F5>MA</F5><F6>100 High St
Boston, MA</F6></Row>')
,(91153, 53, 32, N'<Row><F1>22</F1><F2>Inverness Corners</F2><F3>100 Inverness Cors</F3><F4>Birmingham</F4><F5>AL</F5><F6>Inverness Corners
100 Inverness Cors
Birmingham, AL</F6></Row>')
,(91154, 53, 33, N'<Row><F1>23</F1><F3>100 Liverpool St</F3><F4>London</F4><F5>UK</F5><F6>100 Liverpool St
London, UK</F6></Row>')
,(91155, 53, 34, N'<Row><F1>24</F1><F3>100 M St SE</F3><F4>Washington</F4><F5>DC</F5><F6>100 M St SE
Washington, DC</F6></Row>')
,(91156, 53, 35, N'<Row><F1>1273</F1><F2>Sunnyvale City Center - Building 3</F2><F3>100 Mathilda Pl</F3><F4>Sunnyvale</F4><F5>CA</F5><F6>Sunnyvale City Center - Building 3
100 Mathilda Pl
Sunnyvale, CA</F6></Row>')
,(91157, 53, 36, N'<Row><F1>25</F1><F2>Family Dollar</F2><F3>100 N Dr. MLK Jr. Blvd</F3><F4>Grenada</F4><F5>MS</F5><F6>Family Dollar
100 N Dr. MLK Jr. Blvd
Grenada, MS</F6></Row>')
,(91158, 53, 37, N'<Row><F1>26</F1><F3>100 N La Cienega Blvd</F3><F4>Los Angeles</F4><F5>CA</F5><F6>100 N La Cienega Blvd
Los Angeles, CA</F6></Row>')
,(91159, 53, 38, N'<Row><F1>999</F1><F2>South Pine Island Building</F2><F3>100 S Pine Island Rd</F3><F4>Plantation</F4><F5>FL</F5><F6>South Pine Island Building
100 S Pine Island Rd
Plantation, FL</F6></Row>')
,(91160, 53, 39, N'<Row><F1>27</F1><F3>100 S State College Blvd</F3><F4>Brea</F4><F5>CA</F5><F6>100 S State College Blvd
Brea, CA</F6></Row>')
,(91161, 53, 40, N'<Row><F1>28</F1><F3>100 S Washington Ave</F3><F4>Minneapolis</F4><F5>MN</F5><F6>100 S Washington Ave
Minneapolis, MN</F6></Row>')
,(91162, 53, 41, N'<Row><F1>1065</F1><F2>Hilton Santa Fe Historic Plaza</F2><F3>100 Sandoval St</F3><F4>Santa Fe</F4><F5>NM</F5><F6>Hilton Santa Fe Historic Plaza
100 Sandoval St
Santa Fe, NM</F6></Row>')
,(91163, 53, 42, N'<Row><F1>29</F1><F2>Hilton Glendale</F2><F3>100 W. Glenoaks Blvd</F3><F4>Glendale</F4><F5>CA</F5><F6>Hilton Glendale
100 W. Glenoaks Blvd
Glendale, CA</F6></Row>')
,(91164, 53, 43, N'<Row><F1>30</F1><F3>1000 Abernathy Rd NE</F3><F4>Atlanta</F4><F5>GA</F5><F6>1000 Abernathy Rd NE
Atlanta, GA</F6></Row>')
,(91165, 53, 44, N'<Row><F1>1186</F1><F3>1000 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>1000 Atlantic Ave
Alameda, CA</F6></Row>')
,(91166, 53, 45, N'<Row><F1>1033</F1><F3>1000 Dexter Ave N</F3><F4>Seattle</F4><F5>WA</F5><F6>1000 Dexter Ave N
Seattle, WA</F6></Row>')
,(91167, 53, 46, N'<Row><F1>31</F1><F2>Clay Cooley Service</F2><F3>1000 E Airport Fwy</F3><F4>Irving</F4><F5>TX</F5><F6>Clay Cooley Service
1000 E Airport Fwy
Irving, TX</F6></Row>')
,(91168, 53, 47, N'<Row><F1>1128</F1><F2>Doubletree Portland</F2><F3>1000 NE Multnomah St</F3><F4>Portland</F4><F5>OR</F5><F6>Doubletree Portland
1000 NE Multnomah St
Portland, OR</F6></Row>')
,(91169, 53, 48, N'<Row><F1>32</F1><F2>Marriott Sawgrass Golf Resort &amp; Spa</F2><F3>1000 PGA Tour Blvd</F3><F4>Ponte Vedra Beach</F4><F5>FL</F5><F6>Marriott Sawgrass Golf Resort &amp; Spa
1000 PGA Tour Blvd
Ponte Vedra Beach, FL</F6></Row>')
,(91170, 53, 49, N'<Row><F1>33</F1><F2>Element Ewing Princeton</F2><F3>1000 Sam Weinroth Rd</F3><F4>Ewing</F4><F5>NJ</F5><F6>Element Ewing Princeton
1000 Sam Weinroth Rd
Ewing, NJ</F6></Row>')
,(91171, 53, 50, N'<Row><F1>34</F1><F2>Orr Motors South</F2><F3>1000 Truman Baker Dr</F3><F4>Searcy</F4><F5>AR</F5><F6>Orr Motors South
1000 Truman Baker Dr
Searcy, AR</F6></Row>')
,(91172, 53, 51, N'<Row><F1>35</F1><F2>Momentum BMW</F2><F3>10002 SW Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Momentum BMW
10002 SW Fwy
Houston, TX</F6></Row>')
,(91173, 53, 52, N'<Row><F1>36</F1><F2>Avondale Toyota Scion</F2><F3>10005 W Papago Fwy</F3><F4>Avondale</F4><F5>AZ</F5><F6>Avondale Toyota Scion
10005 W Papago Fwy
Avondale, AZ</F6></Row>')
,(91174, 53, 53, N'<Row><F1>37</F1><F3>1001 E Hillside Blvd</F3><F4>Foster City</F4><F5>CA</F5><F6>1001 E Hillside Blvd
Foster City, CA</F6></Row>')
,(91175, 53, 54, N'<Row><F1>1175</F1><F3>1001 Marina Village Pkwy</F3><F4>Alameda</F4><F5>CA</F5><F6>1001 Marina Village Pkwy
Alameda, CA</F6></Row>')
,(91176, 53, 55, N'<Row><F1>1199</F1><F2>Meridian Residences</F2><F3>1001 Santa Barbara Dr</F3><F4>Newport Beach</F4><F5>CA</F5><F6>Meridian Residences
1001 Santa Barbara Dr
Newport Beach, CA</F6></Row>')
,(91177, 53, 56, N'<Row><F1>38</F1><F2>Crown Honda at Southpoint</F2><F3>1001 Southpoint Auto Park Blvd</F3><F4>Durham</F4><F5>NC</F5><F6>Crown Honda at Southpoint
1001 Southpoint Auto Park Blvd
Durham, NC</F6></Row>')
,(91178, 53, 57, N'<Row><F1>39</F1><F2>Chuck E. Cheese''s</F2><F3>1001 W Hampden Ave</F3><F4>Englewood</F4><F5>CO</F5><F6>Chuck E. Cheese''s
1001 W Hampden Ave
Englewood, CO</F6></Row>')
,(91179, 53, 58, N'<Row><F1>1270</F1><F2>Brewery Block 3</F2><F3>1001-1039 NW Couch St</F3><F4>Portland</F4><F5>OR</F5><F6>Brewery Block 3
1001-1039 NW Couch St
Portland, OR</F6></Row>')
,(91180, 53, 59, N'<Row><F1>1002</F1><F2>Welleby Square Shopping Center</F2><F3>10018 W Oakland Park Blvd</F3><F4>Sunrise</F4><F5>FL</F5><F6>Welleby Square Shopping Center
10018 W Oakland Park Blvd
Sunrise, FL</F6></Row>')
,(91181, 53, 60, N'<Row><F1>40</F1><F2>Groove Ford</F2><F3>10039 E Arapahoe Rd</F3><F4>Centennial</F4><F5>CO</F5><F6>Groove Ford
10039 E Arapahoe Rd
Centennial, CO</F6></Row>')
,(91182, 53, 61, N'<Row><F1>41</F1><F3>10055 Barnes Canyon Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>10055 Barnes Canyon Rd
San Diego, CA</F6></Row>')
,(91183, 53, 62, N'<Row><F1>42</F1><F3>10065 Barnes Canyon Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>10065 Barnes Canyon Rd
San Diego, CA</F6></Row>')
,(91184, 53, 63, N'<Row><F1>43</F1><F3>10075 Barnes Canyon Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>10075 Barnes Canyon Rd
San Diego, CA</F6></Row>')
,(91185, 53, 64, N'<Row><F1>44</F1><F2>Continental Tower</F2><F3>101 Continental Blvd</F3><F4>El Segundo</F4><F5>CA</F5><F6>Continental Tower
101 Continental Blvd
El Segundo, CA</F6></Row>')
,(91186, 53, 65, N'<Row><F1>1021</F1><F3>101 Lytton Ave</F3><F4>Palo Alto</F4><F5>CA</F5><F6>101 Lytton Ave
Palo Alto, CA</F6></Row>')
,(91187, 53, 66, N'<Row><F1>45</F1><F3>101 N First Ave</F3><F4>Phoenix</F4><F5>AZ</F5><F6>101 N First Ave
Phoenix, AZ</F6></Row>')
,(91188, 53, 67, N'<Row><F1>46</F1><F2>Lexus of Quad Cities</F2><F3>101 W Kimberly Rd</F3><F4>Davenport</F4><F5>IA</F5><F6>Lexus of Quad Cities
101 W Kimberly Rd
Davenport, IA</F6></Row>')
,(91189, 53, 68, N'<Row><F1>1187</F1><F3>1010 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>1010 Atlantic Ave
Alameda, CA</F6></Row>')
,(91190, 53, 69, N'<Row><F1>47</F1><F3>101-121 Daggett Dr</F3><F4>San Jose</F4><F5>CA</F5><F6>101-121 Daggett Dr
San Jose, CA</F6></Row>')
,(91191, 53, 70, N'<Row><F1>1188</F1><F3>1015 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>1015 Atlantic Ave
Alameda, CA</F6></Row>')
,(91192, 53, 71, N'<Row><F1>48</F1><F2>Family Dollar</F2><F3>1015 Jefferson St</F3><F4>Nashville</F4><F5>TN</F5><F6>Family Dollar
1015 Jefferson St
Nashville, TN</F6></Row>')
,(91193, 53, 72, N'<Row><F1>49</F1><F2>Momentum Jaguar Porsche Volvo Mini</F2><F3>10150 SW Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Momentum Jaguar Porsche Volvo Mini
10150 SW Fwy
Houston, TX</F6></Row>')
,(91194, 53, 73, N'<Row><F1>50</F1><F2>Fairfield Motors Collision Center</F2><F3>102 Linwood Ave</F3><F4>Fairfield</F4><F5>CT</F5><F6>Fairfield Motors Collision Center
102 Linwood Ave
Fairfield, CT</F6></Row>')
,(91195, 53, 74, N'<Row><F1>1189</F1><F3>1020 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>1020 Atlantic Ave
Alameda, CA</F6></Row>')
,(91196, 53, 75, N'<Row><F1>51</F1><F2>Luxe City Center Hotel</F2><F3>1020 S Figueroa</F3><F4>Los Angeles</F4><F5>CA</F5><F6>Luxe City Center Hotel
1020 S Figueroa
Los Angeles, CA</F6></Row>')
,(91197, 53, 76, N'<Row><F1>52</F1><F3>10201 Wateridge Cir</F3><F4>San Diego</F4><F5>CA</F5><F6>10201 Wateridge Cir
San Diego, CA</F6></Row>')
,(91198, 53, 77, N'<Row><F1>53</F1><F3>10221 Wateridge Cir</F3><F4>San Diego</F4><F5>CA</F5><F6>10221 Wateridge Cir
San Diego, CA</F6></Row>')
,(91199, 53, 78, N'<Row><F1>54</F1><F3>10241 Wateridge Cir</F3><F4>San Diego</F4><F5>CA</F5><F6>10241 Wateridge Cir
San Diego, CA</F6></Row>')
,(91200, 53, 79, N'<Row><F1>1190</F1><F3>1025 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>1025 Atlantic Ave
Alameda, CA</F6></Row>')
,(91201, 53, 80, N'<Row><F1>55</F1><F3>1025 Westchester Ave</F3><F4>White Plains</F4><F5>NY</F5><F6>1025 Westchester Ave
White Plains, NY</F6></Row>')
,(91202, 53, 81, N'<Row><F1>56</F1><F2>Dealer''s Auto Auction</F2><F3>1028 S Portland Ave</F3><F4>Oklahoma City</F4><F5>OK</F5><F6>Dealer''s Auto Auction
1028 S Portland Ave
Oklahoma City, OK</F6></Row>')
,(91203, 53, 82, N'<Row><F1>57</F1><F3>103 Corporate Park Dr</F3><F4>Harrison</F4><F5>NY</F5><F6>103 Corporate Park Dr
Harrison, NY</F6></Row>')
,(91204, 53, 83, N'<Row><F1>58</F1><F2>Family Dollar</F2><F3>103 Maysville Rd</F3><F4>Millersburg</F4><F5>KY</F5><F6>Family Dollar
103 Maysville Rd
Millersburg, KY</F6></Row>')
,(91205, 53, 84, N'<Row><F1>59</F1><F2>Family Dollar</F2><F3>103 Nueces St</F3><F4>Camp Wood</F4><F5>TX</F5><F6>Family Dollar
103 Nueces St
Camp Wood, TX</F6></Row>')
,(91206, 53, 85, N'<Row><F1>60</F1><F2>The Mall at Wellington Green</F2><F3>10300 W Forest Hill Blvd</F3><F4>Wellington</F4><F5>FL</F5><F6>The Mall at Wellington Green
10300 W Forest Hill Blvd
Wellington, FL</F6></Row>')
,(91207, 53, 86, N'<Row><F1>61</F1><F2>Groove Mazda</F2><F3>10301 E Arapahoe Rd</F3><F4>Centennial</F4><F5>CO</F5><F6>Groove Mazda
10301 E Arapahoe Rd
Centennial, CO</F6></Row>')
,(91208, 53, 87, N'<Row><F1>1282</F1><F3>10350 N Torrey Pines Rd</F3><F4>La Jolla</F4><F5>CA</F5><F6>10350 N Torrey Pines Rd
La Jolla, CA</F6></Row>')
,(91209, 53, 88, N'<Row><F1>1200</F1><F2>Millennium Tower</F2><F3>10375 Richmond Ave</F3><F4>Houston</F4><F5>TX</F5><F6>Millennium Tower
10375 Richmond Ave
Houston, TX</F6></Row>')
,(91210, 53, 89, N'<Row><F1>62</F1><F2>Gray Daniels Toyota</F2><F3>104 Gray Daniels Blvd</F3><F4>Brandon</F4><F5>MS</F5><F6>Gray Daniels Toyota
104 Gray Daniels Blvd
Brandon, MS</F6></Row>')
,(91211, 53, 90, N'<Row><F1>1143</F1><F2>Hyatt Place Delray Beach</F2><F3>104 NE 2nd Ave</F3><F4>Delray Beach</F4><F5>FL</F5><F6>Hyatt Place Delray Beach
104 NE 2nd Ave
Delray Beach, FL</F6></Row>')
,(91212, 53, 91, N'<Row><F1>63</F1><F2>Family Dollar</F2><F3>104 W Wetherbee Rd</F3><F4>Orlando</F4><F5>FL</F5><F6>Family Dollar
104 W Wetherbee Rd
Orlando, FL</F6></Row>')
,(91213, 53, 92, N'<Row><F1>1082</F1><F2>The Avallon - Building I</F2><F3>10415 Morado Cir</F3><F4>Austin</F4><F5>TX</F5><F6>The Avallon - Building I
10415 Morado Cir
Austin, TX</F6></Row>')
,(91214, 53, 93, N'<Row><F1>1083</F1><F2>The Avallon - Building II</F2><F3>10415 Morado Cir</F3><F4>Austin</F4><F5>TX</F5><F6>The Avallon - Building II
10415 Morado Cir
Austin, TX</F6></Row>')
,(91215, 53, 94, N'<Row><F1>1084</F1><F2>The Avallon - Building III</F2><F3>10415 Morado Cir</F3><F4>Austin</F4><F5>TX</F5><F6>The Avallon - Building III
10415 Morado Cir
Austin, TX</F6></Row>')
,(91216, 53, 95, N'<Row><F1>1087</F1><F2>The Avallon - Building VI</F2><F3>10415 Morado Cir</F3><F4>Austin</F4><F5>TX</F5><F6>The Avallon - Building VI
10415 Morado Cir
Austin, TX</F6></Row>')
,(91217, 53, 96, N'<Row><F1>64</F1><F2>David Taylor Cadillac Buick GMC</F2><F3>10422 SW Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>David Taylor Cadillac Buick GMC
10422 SW Fwy
Houston, TX</F6></Row>')
,(91218, 53, 97, N'<Row><F1>1086</F1><F2>The Avallon - Building V</F2><F3>10431 Morado Cir</F3><F4>Austin</F4><F5>TX</F5><F6>The Avallon - Building V
10431 Morado Cir
Austin, TX</F6></Row>')
,(91219, 53, 98, N'<Row><F1>65</F1><F2>South Towne Center</F2><F3>10450 S State St</F3><F4>Sandy</F4><F5>UT</F5><F6>South Towne Center
10450 S State St
Sandy, UT</F6></Row>')
,(91220, 53, 99, N'<Row><F1>66</F1><F2>Flagler Station</F2><F3>10451 NW 117th Ave</F3><F4>Miami</F4><F5>FL</F5><F6>Flagler Station
10451 NW 117th Ave
Miami, FL</F6></Row>')
,(91221, 53, 100, N'<Row><F1>67</F1><F2>Service King Paint and Body</F2><F3>10475 SW Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Service King Paint and Body
10475 SW Fwy
Houston, TX</F6></Row>')
,(91222, 53, 101, N'<Row><F1>68</F1><F3>105 Corporate Park Dr</F3><F4>Harrison</F4><F5>NY</F5><F6>105 Corporate Park Dr
Harrison, NY</F6></Row>')
,(91223, 53, 102, N'<Row><F1>1173</F1><F3>1050 Marina Village Pkwy</F3><F4>Alameda</F4><F5>CA</F5><F6>1050 Marina Village Pkwy
Alameda, CA</F6></Row>')
,(91224, 53, 103, N'<Row><F1>69</F1><F3>1051 E Hillside Blvd</F3><F4>Foster City</F4><F5>CA</F5><F6>1051 E Hillside Blvd
Foster City, CA</F6></Row>')
,(91225, 53, 104, N'<Row><F1>70</F1><F2>Chuck E. Cheese''s</F2><F3>10510 Coors Blvd</F3><F4>Albuquerque</F4><F5>NM</F5><F6>Chuck E. Cheese''s
10510 Coors Blvd
Albuquerque, NM</F6></Row>')
,(91226, 53, 105, N'<Row><F1>71</F1><F2>McDavid Nissan Trucks/Used Car Supercenter</F2><F3>10540 Gulf Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>McDavid Nissan Trucks/Used Car Supercenter
10540 Gulf Fwy
Houston, TX</F6></Row>')
,(91227, 53, 106, N'<Row><F1>72</F1><F2>The Gardens on Havana</F2><F3>10551 E Garden Dr</F3><F4>Aurora</F4><F5>CO</F5><F6>The Gardens on Havana
10551 E Garden Dr
Aurora, CO</F6></Row>')
,(91228, 53, 107, N'<Row><F1>73</F1><F3>10574 N 90th St</F3><F4>Scottsdale</F4><F5>AZ</F5><F6>10574 N 90th St
Scottsdale, AZ</F6></Row>')
,(91229, 53, 108, N'<Row><F1>74</F1><F2>St. Clair Imports</F2><F3>106 Auto Ct</F3><F4>O''Fallon</F4><F5>IL</F5><F6>St. Clair Imports
106 Auto Ct
O''Fallon, IL</F6></Row>')
,(91230, 53, 109, N'<Row><F1>75</F1><F3>106 Corporate Park Dr</F3><F4>White Plains</F4><F5>NY</F5><F6>106 Corporate Park Dr
White Plains, NY</F6></Row>')
,(91231, 53, 110, N'<Row><F1>76</F1><F3>1060 Frontenac Rd</F3><F4>Aurora</F4><F5>IL</F5><F6>1060 Frontenac Rd
Aurora, IL</F6></Row>')
,(91232, 53, 111, N'<Row><F1>77</F1><F2>South Towne Center</F2><F3>10600 S State St</F3><F4>Sandy</F4><F5>UT</F5><F6>South Towne Center
10600 S State St
Sandy, UT</F6></Row>')
,(91233, 53, 112, N'<Row><F1>78</F1><F2>Family Dollar</F2><F3>107 E Musquiz Dr</F3><F4>Fort Davis</F4><F5>TX</F5><F6>Family Dollar
107 E Musquiz Dr
Fort Davis, TX</F6></Row>')
,(91234, 53, 113, N'<Row><F1>79</F1><F2>Cadillac of South Charlotte</F2><F3>10725 Pineville Rd</F3><F4>Pineville</F4><F5>NC</F5><F6>Cadillac of South Charlotte
10725 Pineville Rd
Pineville, NC</F6></Row>')
,(91235, 53, 114, N'<Row><F1>80</F1><F3>1075 Southbridge St</F3><F4>Worcester</F4><F5>MA</F5><F6>1075 Southbridge St
Worcester, MA</F6></Row>')
,(91236, 53, 115, N'<Row><F1>81</F1><F3>108 Corporate Park Dr</F3><F4>White Plains</F4><F5>NY</F5><F6>108 Corporate Park Dr
White Plains, NY</F6></Row>')
,(91237, 53, 116, N'<Row><F1>82</F1><F2>Gray Daniels Nissan</F2><F3>108 Gray Daniels Blvd</F3><F4>Brandon</F4><F5>MS</F5><F6>Gray Daniels Nissan
108 Gray Daniels Blvd
Brandon, MS</F6></Row>')
,(91238, 53, 117, N'<Row><F1>1174</F1><F3>1080 Marina Village Pkwy</F3><F4>Alameda</F4><F5>CA</F5><F6>1080 Marina Village Pkwy
Alameda, CA</F6></Row>')
,(91239, 53, 118, N'<Row><F1>83</F1><F3>10800 Pecan Park Blvd</F3><F4>Austin</F4><F5>TX</F5><F6>10800 Pecan Park Blvd
Austin, TX</F6></Row>')
,(91240, 53, 119, N'<Row><F1>1085</F1><F2>The Avallon - Building IV</F2><F3>10814 Jollyville Rd</F3><F4>Austin</F4><F5>TX</F5><F6>The Avallon - Building IV
10814 Jollyville Rd
Austin, TX</F6></Row>')
,(91241, 53, 120, N'<Row><F1>84</F1><F2>Murdock Plaza</F2><F3>10900 Wilshire Blvd</F3><F4>Westwood</F4><F5>CA</F5><F6>Murdock Plaza
10900 Wilshire Blvd
Westwood, CA</F6></Row>')
,(91242, 53, 121, N'<Row><F1>85</F1><F3>10905 Technology Pl</F3><F4>San Diego</F4><F5>CA</F5><F6>10905 Technology Pl
San Diego, CA</F6></Row>')
,(91243, 53, 122, N'<Row><F1>86</F1><F3>10907 Technology Pl</F3><F4>San Diego</F4><F5>CA</F5><F6>10907 Technology Pl
San Diego, CA</F6></Row>')
,(91244, 53, 123, N'<Row><F1>87</F1><F3>10908 Technology Pl</F3><F4>San Diego</F4><F5>CA</F5><F6>10908 Technology Pl
San Diego, CA</F6></Row>')
,(91245, 53, 124, N'<Row><F1>88</F1><F3>10911 Technology Pl</F3><F4>San Diego</F4><F5>CA</F5><F6>10911 Technology Pl
San Diego, CA</F6></Row>')
,(91246, 53, 125, N'<Row><F1>89</F1><F3>10915 Technology Pl</F3><F4>San Diego</F4><F5>CA</F5><F6>10915 Technology Pl
San Diego, CA</F6></Row>')
,(91247, 53, 126, N'<Row><F1>90</F1><F3>10918 Technology Pl</F3><F4>San Diego</F4><F5>CA</F5><F6>10918 Technology Pl
San Diego, CA</F6></Row>')
,(91248, 53, 127, N'<Row><F1>91</F1><F3>10919 Technology Pl</F3><F4>San Diego</F4><F5>CA</F5><F6>10919 Technology Pl
San Diego, CA</F6></Row>')
,(91249, 53, 128, N'<Row><F1>92</F1><F3>10929 Technology Pl</F3><F4>San Diego</F4><F5>CA</F5><F6>10929 Technology Pl
San Diego, CA</F6></Row>')
,(91250, 53, 129, N'<Row><F1>93</F1><F3>10939 Technology Pl</F3><F4>San Diego</F4><F5>CA</F5><F6>10939 Technology Pl
San Diego, CA</F6></Row>')
,(91251, 53, 130, N'<Row><F1>94</F1><F3>10949 Technology Pl</F3><F4>San Diego</F4><F5>CA</F5><F6>10949 Technology Pl
San Diego, CA</F6></Row>')
,(91252, 53, 131, N'<Row><F1>982</F1><F3>11 Norfolk St</F3><F4>Mansfield</F4><F5>MA</F5><F6>11 Norfolk St
Mansfield, MA</F6></Row>')
,(91253, 53, 132, N'<Row><F1>95</F1><F3>110 Corporate Park Dr</F3><F4>Harrison</F4><F5>NY</F5><F6>110 Corporate Park Dr
Harrison, NY</F6></Row>')
,(91254, 53, 133, N'<Row><F1>976</F1><F3>110 Forbes Blvd</F3><F4>Mansfield</F4><F5>MA</F5><F6>110 Forbes Blvd
Mansfield, MA</F6></Row>')
,(91255, 53, 134, N'<Row><F1>96</F1><F2>Bayside Corporate Center I</F2><F3>110 Marsh Dr</F3><F4>Foster City</F4><F5>CA</F5><F6>Bayside Corporate Center I
110 Marsh Dr
Foster City, CA</F6></Row>')
,(91256, 53, 135, N'<Row><F1>97</F1><F3>110 N Carpenter St</F3><F4>Chicago</F4><F5>IL</F5><F6>110 N Carpenter St
Chicago, IL</F6></Row>')
,(91257, 53, 136, N'<Row><F1>1245</F1><F3>110 Rio Robles</F3><F4>San Jose</F4><F5>CA</F5><F6>110 Rio Robles
San Jose, CA</F6></Row>')
,(91258, 53, 137, N'<Row><F1>98</F1><F3>110 William St</F3><F4>New York</F4><F5>NY</F5><F6>110 William St
New York, NY</F6></Row>')
,(91259, 53, 138, N'<Row><F1>99</F1><F3>1100 Abernathy Rd NE</F3><F4>Atlanta</F4><F5>GA</F5><F6>1100 Abernathy Rd NE
Atlanta, GA</F6></Row>')
,(91260, 53, 139, N'<Row><F1>1034</F1><F3>1100 Dexter Ave N</F3><F4>Seattle</F4><F5>WA</F5><F6>1100 Dexter Ave N
Seattle, WA</F6></Row>')
,(91261, 53, 140, N'<Row><F1>1176</F1><F3>1101 Marina Village Pkwy</F3><F4>Alameda</F4><F5>CA</F5><F6>1101 Marina Village Pkwy
Alameda, CA</F6></Row>')
,(91262, 53, 141, N'<Row><F1>100</F1><F3>1101 W Maude Ave</F3><F4>Sunnyvale</F4><F5>CA</F5><F6>1101 W Maude Ave
Sunnyvale, CA</F6></Row>')
,(91263, 53, 142, N'<Row><F1>101</F1><F2>Central Florida Toyota/Scion</F2><F3>11020 S Orange Blossom Trl</F3><F4>Orlando</F4><F5>FL</F5><F6>Central Florida Toyota/Scion
11020 S Orange Blossom Trl
Orlando, FL</F6></Row>')
,(91264, 53, 143, N'<Row><F1>102</F1><F2>Family Dollar</F2><F3>1103 Richland Avenue E</F3><F4>Aiken</F4><F5>SC</F5><F6>Family Dollar
1103 Richland Avenue E
Aiken, SC</F6></Row>')
,(91265, 53, 144, N'<Row><F1>1191</F1><F3>1105 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>1105 Atlantic Ave
Alameda, CA</F6></Row>')
,(91266, 53, 145, N'<Row><F1>103</F1><F2>Coggin Honda of Orlando</F2><F3>11050 S Orange Blossom</F3><F4>Orlando</F4><F5>FL</F5><F6>Coggin Honda of Orlando
11050 S Orange Blossom
Orlando, FL</F6></Row>')
,(91267, 53, 146, N'<Row><F1>104</F1><F2>Coggin Honda of Orlando</F2><F3>11051 S Orange Blossom</F3><F4>Orlando</F4><F5>FL</F5><F6>Coggin Honda of Orlando
11051 S Orange Blossom
Orlando, FL</F6></Row>')
,(91268, 53, 147, N'<Row><F1>105</F1><F3>1107 Broadway</F3><F4>New York</F4><F5>NY</F5><F6>1107 Broadway
New York, NY</F6></Row>')
,(91269, 53, 148, N'<Row><F1>106</F1><F2>Family Dollar</F2><F3>111 East Kennedy Blvd</F3><F4>Eatonville</F4><F5>FL</F5><F6>Family Dollar
111 East Kennedy Blvd
Eatonville, FL</F6></Row>')
,(91270, 53, 149, N'<Row><F1>107</F1><F2>Capitol Chevrolet/Hyundai</F2><F3>111 Newland Rd</F3><F4>Columbia</F4><F5>SC</F5><F6>Capitol Chevrolet/Hyundai
111 Newland Rd
Columbia, SC</F6></Row>')
,(91271, 53, 150, N'<Row><F1>108</F1><F3>111 S Washington Ave</F3><F4>Minneapolis</F4><F5>MN</F5><F6>111 S Washington Ave
Minneapolis, MN</F6></Row>')
,(91272, 53, 151, N'<Row><F1>109</F1><F2>North Houston Infiniti Body Shop</F2><F3>111 Stage Runn Dr</F3><F4>Houston</F4><F5>TX</F5><F6>North Houston Infiniti Body Shop
111 Stage Runn Dr
Houston, TX</F6></Row>')
,(91273, 53, 152, N'<Row><F1>110</F1><F2>Family Dollar</F2><F3>1110 Roxboro St</F3><F4>Haw River</F4><F5>NC</F5><F6>Family Dollar
1110 Roxboro St
Haw River, NC</F6></Row>')
,(91274, 53, 153, N'<Row><F1>111</F1><F2>Heritage Plaza</F2><F3>1111 Bagby St</F3><F4>Houston</F4><F5>TX</F5><F6>Heritage Plaza
1111 Bagby St
Houston, TX</F6></Row>')
,(91275, 53, 154, N'<Row><F1>112</F1><F2>Advantage BMW Used Car and Parking Lot</F2><F3>1111 Gray St</F3><F4>Houston</F4><F5>TX</F5><F6>Advantage BMW Used Car and Parking Lot
1111 Gray St
Houston, TX</F6></Row>')
,(91276, 53, 155, N'<Row><F1>113</F1><F2>The Gallery at Capitol Riverfront </F2><F3>1111 New Jersey Avenue SE</F3><F4>Washington</F4><F5>DC</F5><F6>The Gallery at Capitol Riverfront 
1111 New Jersey Avenue SE
Washington, DC</F6></Row>')
,(91277, 53, 156, N'<Row><F1>1238</F1><F3>11-15 Grosvenor Crescent</F3><F4>Belgravia, London SW1X 7EE</F4><F5>UK</F5><F6>11-15 Grosvenor Crescent
Belgravia, London SW1X 7EE, UK</F6></Row>')
,(91278, 53, 157, N'<Row><F1>114</F1><F3>1115 W Washington Blvd</F3><F4>Chicago</F4><F5>IL</F5><F6>1115 W Washington Blvd
Chicago, IL</F6></Row>')
,(91279, 53, 158, N'<Row><F1>1269</F1><F2>Brewery Block 4</F2><F3>1115-1139 NW Couch St</F3><F4>Portland</F4><F5>OR</F5><F6>Brewery Block 4
1115-1139 NW Couch St
Portland, OR</F6></Row>')
,(91280, 53, 159, N'<Row><F1>1268</F1><F2>Brewery Block 2</F2><F3>1120 NW Couch St</F3><F4>Portland</F4><F5>OR</F5><F6>Brewery Block 2
1120 NW Couch St
Portland, OR</F6></Row>')
,(91281, 53, 160, N'<Row><F1>115</F1><F3>11201 SE 8th St</F3><F4>Bellevue</F4><F5>WA</F5><F6>11201 SE 8th St
Bellevue, WA</F6></Row>')
,(91282, 53, 161, N'<Row><F1>1192</F1><F3>1125 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>1125 Atlantic Ave
Alameda, CA</F6></Row>')
,(91283, 53, 162, N'<Row><F1>116</F1><F2>Family Dollar</F2><F3>1125 Wayne Ave</F3><F4>Dayton</F4><F5>OH</F5><F6>Family Dollar
1125 Wayne Ave
Dayton, OH</F6></Row>')
,(91284, 53, 163, N'<Row><F1>117</F1><F3>113 N May St</F3><F4>Chicago</F4><F5>IL</F5><F6>113 N May St
Chicago, IL</F6></Row>')
,(91285, 53, 164, N'<Row><F1>118</F1><F2>Family Dollar</F2><F3>1131 W Peace St</F3><F4>Canton</F4><F5>MS</F5><F6>Family Dollar
1131 W Peace St
Canton, MS</F6></Row>')
,(91286, 53, 165, N'<Row><F1>1193</F1><F3>1135 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>1135 Atlantic Ave
Alameda, CA</F6></Row>')
,(91287, 53, 166, N'<Row><F1>1078</F1><F2>Hampton Inn Lawrenceville</F2><F3>1135 Lakes Pkwy</F3><F4>Lawrenceville</F4><F5>GA</F5><F6>Hampton Inn Lawrenceville
1135 Lakes Pkwy
Lawrenceville, GA</F6></Row>')
,(91288, 53, 167, N'<Row><F1>1088</F1><F2>Fair Oaks Plaza</F2><F3>11350 Random Hills Rd</F3><F4>Fairfax</F4><F5>VA</F5><F6>Fair Oaks Plaza
11350 Random Hills Rd
Fairfax, VA</F6></Row>')
,(91289, 53, 168, N'<Row><F1>119</F1><F2>North Bethesda Market</F2><F3>11351 Woodglen Dr</F3><F4>North Bethesda</F4><F5>MD</F5><F6>North Bethesda Market
11351 Woodglen Dr
North Bethesda, MD</F6></Row>')
,(91290, 53, 169, N'<Row><F1>120</F1><F3>114 Prince St</F3><F4>New York</F4><F5>NY</F5><F6>114 Prince St
New York, NY</F6></Row>')
,(91291, 53, 170, N'<Row><F1>1022</F1><F3>114 W 41st St</F3><F4>New York</F4><F5>NY</F5><F6>114 W 41st St
New York, NY</F6></Row>')
,(91292, 53, 171, N'<Row><F1>1194</F1><F3>1145 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>1145 Atlantic Ave
Alameda, CA</F6></Row>')
,(91293, 53, 172, N'<Row><F1>1071</F1><F2>Residence Inn Orlando Lake Buena Vista</F2><F3>11450 Marbella Palm Ct</F3><F4>Orlando</F4><F5>FL</F5><F6>Residence Inn Orlando Lake Buena Vista
11450 Marbella Palm Ct
Orlando, FL</F6></Row>')
,(91294, 53, 173, N'<Row><F1>121</F1><F2>United BMW of Roswell</F2><F3>11458 Alpharetta Hwy</F3><F4>Roswell</F4><F5>GA</F5><F6>United BMW of Roswell
11458 Alpharetta Hwy
Roswell, GA</F6></Row>')
,(91295, 53, 174, N'<Row><F1>122</F1><F2>Auffenburg Ford North</F2><F3>115 Regency Park Dr</F3><F4>O''Fallon</F4><F5>IL</F5><F6>Auffenburg Ford North
115 Regency Park Dr
O''Fallon, IL</F6></Row>')
,(91296, 53, 175, N'<Row><F1>123</F1><F3>1150 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1150 114th Ave SE
Bellevue, WA</F6></Row>')
,(91297, 53, 176, N'<Row><F1>1290</F1><F3>1150 15th St NW</F3><F4>Washington</F4><F5>DC</F5><F6>1150 15th St NW
Washington, DC</F6></Row>')
,(91298, 53, 177, N'<Row><F1>1059</F1><F2>Crowne Plaza Beverly Hills</F2><F3>1150 S Beverly Dr</F3><F4>Los Angeles</F4><F5>CA</F5><F6>Crowne Plaza Beverly Hills
1150 S Beverly Dr
Los Angeles, CA</F6></Row>')
,(91299, 53, 178, N'<Row><F1>1177</F1><F3>1151 Marina Village Pkwy</F3><F4>Alameda</F4><F5>CA</F5><F6>1151 Marina Village Pkwy
Alameda, CA</F6></Row>')
,(91300, 53, 179, N'<Row><F1>124</F1><F3>1155 Brook Forest Ave</F3><F4>Shorewood</F4><F5>IL</F5><F6>1155 Brook Forest Ave
Shorewood, IL</F6></Row>')
,(91301, 53, 180, N'<Row><F1>125</F1><F2>The Plaza America Shopping Center</F2><F3>11610-11694 Plaza America Dr</F3><F4>Reston</F4><F5>VA</F5><F6>The Plaza America Shopping Center
11610-11694 Plaza America Dr
Reston, VA</F6></Row>')
,(91302, 53, 181, N'<Row><F1>126</F1><F2>Massey Cadillac</F2><F3>11675 Lyndon B Johnson Fwy</F3><F4>Garland</F4><F5>TX</F5><F6>Massey Cadillac
11675 Lyndon B Johnson Fwy
Garland, TX</F6></Row>')
,(91303, 53, 182, N'<Row><F1>127</F1><F2>Family Dollar</F2><F3>117 W Church St</F3><F4>Lexington</F4><F5>TN</F5><F6>Family Dollar
117 W Church St
Lexington, TN</F6></Row>')
,(91304, 53, 183, N'<Row><F1>128</F1><F2>Chuck E. Cheese''s</F2><F3>11735 Bandera Road</F3><F4>San Antonio</F4><F5>TX</F5><F6>Chuck E. Cheese''s
11735 Bandera Road
San Antonio, TX</F6></Row>')
,(91305, 53, 184, N'<Row><F1>1035</F1><F3>1175 N Main St</F3><F4>Harrisonburg</F4><F5>VA</F5><F6>1175 N Main St
Harrisonburg, VA</F6></Row>')
,(91306, 53, 185, N'<Row><F1>1049</F1><F2>Reston Square</F2><F3>11790 Sunrise Valley Dr</F3><F4>Reston</F4><F5>VA</F5><F6>Reston Square
11790 Sunrise Valley Dr
Reston, VA</F6></Row>')
,(91307, 53, 186, N'<Row><F1>129</F1><F3>118-122 N Aberdeen St</F3><F4>Chicago</F4><F5>IL</F5><F6>118-122 N Aberdeen St
Chicago, IL</F6></Row>')
,(91308, 53, 187, N'<Row><F1>130</F1><F2>Pence Nissan Kia Subaru</F2><F3>11841 Midlothian Tpk</F3><F4>Midlothian</F4><F5>VA</F5><F6>Pence Nissan Kia Subaru
11841 Midlothian Tpk
Midlothian, VA</F6></Row>')
,(91309, 53, 188, N'<Row><F1>131</F1><F2>Family Dollar</F2><F3>119 N Main St</F3><F4>Madisonville</F4><F5>KY</F5><F6>Family Dollar
119 N Main St
Madisonville, KY</F6></Row>')
,(91310, 53, 189, N'<Row><F1>132</F1><F3>12 4th St</F3><F4>San Francisco</F4><F5>CA</F5><F6>12 4th St
San Francisco, CA</F6></Row>')
,(91311, 53, 190, N'<Row><F1>133</F1><F3>1-2 Broadgate</F3><F4>London</F4><F5>UK</F5><F6>1-2 Broadgate
London, UK</F6></Row>')
,(91312, 53, 191, N'<Row><F1>995</F1><F2>Courthouse Place</F2><F3>12 SE 7th St</F3><F4>Fort Lauderdale</F4><F5>FL</F5><F6>Courthouse Place
12 SE 7th St
Fort Lauderdale, FL</F6></Row>')
,(91313, 53, 192, N'<Row><F1>134</F1><F3>120 S State College Blvd</F3><F4>Brea</F4><F5>CA</F5><F6>120 S State College Blvd
Brea, CA</F6></Row>')
,(91314, 53, 193, N'<Row><F1>135</F1><F3>120 S West St</F3><F4>Raleigh</F4><F5>NC</F5><F6>120 S West St
Raleigh, NC</F6></Row>')
,(91315, 53, 194, N'<Row><F1>136</F1><F3>1200 Abernathy Rd NE</F3><F4>Atlanta</F4><F5>GA</F5><F6>1200 Abernathy Rd NE
Atlanta, GA</F6></Row>')
,(91316, 53, 195, N'<Row><F1>1056</F1><F3>1200 Ashwood Pkwy</F3><F4>Atlanta</F4><F5>GA</F5><F6>1200 Ashwood Pkwy
Atlanta, GA</F6></Row>')
,(91317, 53, 196, N'<Row><F1>137</F1><F2>Pinole Vista Crossing </F2><F3>1200-1398 Fitzgerald Dr</F3><F4>Pinole</F4><F5>CA</F5><F6>Pinole Vista Crossing 
1200-1398 Fitzgerald Dr
Pinole, CA</F6></Row>')
,(91318, 53, 197, N'<Row><F1>138</F1><F2>Park Hyatt Washington</F2><F3>1201 24th St NW</F3><F4>Washington</F4><F5>DC</F5><F6>Park Hyatt Washington
1201 24th St NW
Washington, DC</F6></Row>')
,(91319, 53, 198, N'<Row><F1>139</F1><F2>David McDavid Collision Center</F2><F3>1201 Commerce Dr</F3><F4>Plano</F4><F5>TX</F5><F6>David McDavid Collision Center
1201 Commerce Dr
Plano, TX</F6></Row>')
,(91320, 53, 199, N'<Row><F1>140</F1><F2>Twelve &amp; K Hotel Washington DC</F2><F3>1201 K St NW</F3><F4>Washington</F4><F5>DC</F5><F6>Twelve &amp; K Hotel Washington DC
1201 K St NW
Washington, DC</F6></Row>')
,(91321, 53, 200, N'<Row><F1>1178</F1><F3>1201 Marina Village Pkwy</F3><F4>Alameda</F4><F5>CA</F5><F6>1201 Marina Village Pkwy
Alameda, CA</F6></Row>')
,(91322, 53, 201, N'<Row><F1>141</F1><F2>Marriott Philadelphia Downtown</F2><F3>1201 Market St</F3><F4>Philadelphia</F4><F5>PA</F5><F6>Marriott Philadelphia Downtown
1201 Market St
Philadelphia, PA</F6></Row>')
,(91323, 53, 202, N'<Row><F1>142</F1><F2>Dade City Chrysler Dodge Jeep Ram</F2><F3>12020 US Highway 301</F3><F4>Dade City</F4><F5>FL</F5><F6>Dade City Chrysler Dodge Jeep Ram
12020 US Highway 301
Dade City, FL</F6></Row>')
,(91324, 53, 203, N'<Row><F1>143</F1><F3>1203 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1203 114th Ave SE
Bellevue, WA</F6></Row>')
,(91325, 53, 204, N'<Row><F1>144</F1><F2>Advantage BMW Used Car and Parking Lot</F2><F3>1205 Gray St</F3><F4>Houston</F4><F5>TX</F5><F6>Advantage BMW Used Car and Parking Lot
1205 Gray St
Houston, TX</F6></Row>')
,(91326, 53, 205, N'<Row><F1>1267</F1><F2>Brewery Block 1</F2><F3>1207-1235 W Burnside St</F3><F4>Portland</F4><F5>OR</F5><F6>Brewery Block 1
1207-1235 W Burnside St
Portland, OR</F6></Row>')
,(91327, 53, 206, N'<Row><F1>1058</F1><F2>The Plaza at Scripps Northridge</F2><F3>12121 Scripps Summit Dr</F3><F4>San Diego</F4><F5>CA</F5><F6>The Plaza at Scripps Northridge
12121 Scripps Summit Dr
San Diego, CA</F6></Row>')
,(91328, 53, 207, N'<Row><F1>1154</F1><F2>12130 Millennium</F2><F3>12130 Millenum Dr</F3><F4>Los Angeles</F4><F5>CA</F5><F6>12130 Millennium
12130 Millenum Dr
Los Angeles, CA</F6></Row>')
,(91329, 53, 208, N'<Row><F1>145</F1><F3>1215 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1215 114th Ave SE
Bellevue, WA</F6></Row>')
,(91330, 53, 209, N'<Row><F1>1155</F1><F2>12180 Millennium</F2><F3>12180 E Waterfront Dr</F3><F4>Los Angeles</F4><F5>CA</F5><F6>12180 Millennium
12180 E Waterfront Dr
Los Angeles, CA</F6></Row>')
,(91331, 53, 210, N'<Row><F1>1201</F1><F2>Moanalua Hillside Apartments</F2><F3>1229 Ala Kapuna St</F3><F4>Honolulu</F4><F5>HI</F5><F6>Moanalua Hillside Apartments
1229 Ala Kapuna St
Honolulu, HI</F6></Row>')
,(91332, 53, 211, N'<Row><F1>1023</F1><F2>123 Buckingham Palace</F2><F3>123 Buckingham Palace Rd</F3><F4>London</F4><F5>UK</F5><F6>123 Buckingham Palace
123 Buckingham Palace Rd
London, UK</F6></Row>')
,(91333, 53, 212, N'<Row><F1>1266</F1><F2>The Louisa</F2><F3>123 NW 12th Ave</F3><F4>Portland</F4><F5>OR</F5><F6>The Louisa
123 NW 12th Ave
Portland, OR</F6></Row>')
,(91334, 53, 213, N'<Row><F1>146</F1><F2>Family Dollar</F2><F3>12420 Conant St</F3><F4>Detroit</F4><F5>MI</F5><F6>Family Dollar
12420 Conant St
Detroit, MI</F6></Row>')
,(91335, 53, 214, N'<Row><F1>147</F1><F2>Alban Gate</F2><F3>125 London Wall</F3><F4>London</F4><F5>UK</F5><F6>Alban Gate
125 London Wall
London, UK</F6></Row>')
,(91336, 53, 215, N'<Row><F1>148</F1><F3>125 Summer St</F3><F4>Boston</F4><F5>MA</F5><F6>125 Summer St
Boston, MA</F6></Row>')
,(91337, 53, 216, N'<Row><F1>149</F1><F2>Hall Acura Newport News</F2><F3>12501 Jefferson Ave</F3><F4>Newport News</F4><F5>VA</F5><F6>Hall Acura Newport News
12501 Jefferson Ave
Newport News, VA</F6></Row>')
,(91338, 53, 217, N'<Row><F1>1289</F1><F2>Twelve|555 Playa Vista</F2><F3>12555 W Jefferson Blvd</F3><F4>Los Angeles</F4><F5>CA</F5><F6>Twelve|555 Playa Vista
12555 W Jefferson Blvd
Los Angeles, CA</F6></Row>')
,(91339, 53, 218, N'<Row><F1>1228</F1><F2>Selsdon Park Hotel</F2><F3>126 Addington Rd</F3><F4>South Croydon, Surrey CR2 8YA</F4><F5>UK</F5><F6>Selsdon Park Hotel
126 Addington Rd
South Croydon, Surrey CR2 8YA, UK</F6></Row>')
,(91340, 53, 219, N'<Row><F1>150</F1><F2>Mercedes Benz of West Chester</F2><F3>1260 Wilmington Pike</F3><F4>West Chester</F4><F5>PA</F5><F6>Mercedes Benz of West Chester
1260 Wilmington Pike
West Chester, PA</F6></Row>')
,(91341, 53, 220, N'<Row><F1>1008</F1><F2>Airways Plaza I</F2><F3>1281 Murfreesboro Pike</F3><F4>Nashville</F4><F5>TN</F5><F6>Airways Plaza I
1281 Murfreesboro Pike
Nashville, TN</F6></Row>')
,(91342, 53, 221, N'<Row><F1>151</F1><F2>Huntersville Honda</F2><F3>12815 Statesville Rd</F3><F4>Huntersville</F4><F5>NC</F5><F6>Huntersville Honda
12815 Statesville Rd
Huntersville, NC</F6></Row>')
,(91343, 53, 222, N'<Row><F1>1009</F1><F2>Airways Plaza II</F2><F3>1283 Murfreesboro Pike</F3><F4>Nashville</F4><F5>TN</F5><F6>Airways Plaza II
1283 Murfreesboro Pike
Nashville, TN</F6></Row>')
,(91344, 53, 223, N'<Row><F1>152</F1><F2>Hall Priority Hyundai</F2><F3>12872 Jefferson Ave</F3><F4>Newport News</F4><F5>VA</F5><F6>Hall Priority Hyundai
12872 Jefferson Ave
Newport News, VA</F6></Row>')
,(91345, 53, 224, N'<Row><F1>153</F1><F2>Hall Ford Lincoln Used Car Lot</F2><F3>12880 Jefferson Ave</F3><F4>Newport News</F4><F5>VA</F5><F6>Hall Ford Lincoln Used Car Lot
12880 Jefferson Ave
Newport News, VA</F6></Row>')
,(91346, 53, 225, N'<Row><F1>154</F1><F2>Hall Ford Lincoln</F2><F3>12896 Jefferson Ave</F3><F4>Newport News</F4><F5>VA</F5><F6>Hall Ford Lincoln
12896 Jefferson Ave
Newport News, VA</F6></Row>')
,(91347, 53, 226, N'<Row><F1>155</F1><F2>Family Dollar</F2><F3>130 Main St</F3><F4>Hamilton</F4><F5>OH</F5><F6>Family Dollar
130 Main St
Hamilton, OH</F6></Row>')
,(91348, 53, 227, N'<Row><F1>1094</F1><F3>130 Stony Point Rd</F3><F4>Santa Rosa</F4><F5>CA</F5><F6>130 Stony Point Rd
Santa Rosa, CA</F6></Row>')
,(91349, 53, 228, N'<Row><F1>156</F1><F3>1300 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1300 114th Ave SE
Bellevue, WA</F6></Row>')
,(91350, 53, 229, N'<Row><F1>157</F1><F2>Auffenberg Hyundai</F2><F3>1300 Central Park Pl</F3><F4>O''Fallon</F4><F5>IL</F5><F6>Auffenberg Hyundai
1300 Central Park Pl
O''Fallon, IL</F6></Row>')
,(91351, 53, 230, N'<Row><F1>1007</F1><F2>Tower at 1301 Gervais</F2><F3>1301 Gervais St</F3><F4>Columbia</F4><F5>SC</F5><F6>Tower at 1301 Gervais
1301 Gervais St
Columbia, SC</F6></Row>')
,(91352, 53, 231, N'<Row><F1>1179</F1><F3>1301 Marina Village Pkwy</F3><F4>Alameda</F4><F5>CA</F5><F6>1301 Marina Village Pkwy
Alameda, CA</F6></Row>')
,(91353, 53, 232, N'<Row><F1>1244</F1><F3>130-134 Rio Robles</F3><F4>San Jose</F4><F5>CA</F5><F6>130-134 Rio Robles
San Jose, CA</F6></Row>')
,(91354, 53, 233, N'<Row><F1>158</F1><F3>130-150 Shoreline Dr</F3><F4>Redwood City</F4><F5>CA</F5><F6>130-150 Shoreline Dr
Redwood City, CA</F6></Row>')
,(91355, 53, 234, N'<Row><F1>159</F1><F2>Family Dollar</F2><F3>1306 W Main St</F3><F4>Mitchell</F4><F5>IN</F5><F6>Family Dollar
1306 W Main St
Mitchell, IN</F6></Row>')
,(91356, 53, 235, N'<Row><F1>160</F1><F2>Former Mullinax Ford-Mercury</F2><F3>1307 N Dixie Fwy</F3><F4>New Smyrna Beach</F4><F5>FL</F5><F6>Former Mullinax Ford-Mercury
1307 N Dixie Fwy
New Smyrna Beach, FL</F6></Row>')
,(91357, 53, 236, N'<Row><F1>161</F1><F3>1309 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1309 114th Ave SE
Bellevue, WA</F6></Row>')
,(91358, 53, 237, N'<Row><F1>162</F1><F2>Stevenson Honda of Goldsboro</F2><F3>1309 W Grantham St</F3><F4>Goldsboro</F4><F5>NC</F5><F6>Stevenson Honda of Goldsboro
1309 W Grantham St
Goldsboro, NC</F6></Row>')
,(91359, 53, 238, N'<Row><F1>163</F1><F3>1310 Chesapeake Ter</F3><F4>Sunnyvale</F4><F5>CA</F5><F6>1310 Chesapeake Ter
Sunnyvale, CA</F6></Row>')
,(91360, 53, 239, N'<Row><F1>164</F1><F3>1310 N Courthouse Rd</F3><F4>Arlington</F4><F5>VA</F5><F6>1310 N Courthouse Rd
Arlington, VA</F6></Row>')
,(91361, 53, 240, N'<Row><F1>165</F1><F2>Courtesy Motors (Chevrolet/Ford)</F2><F3>1313 S 13th St</F3><F4>Decatur</F4><F5>IN</F5><F6>Courtesy Motors (Chevrolet/Ford)
1313 S 13th St
Decatur, IN</F6></Row>')
,(91362, 53, 241, N'<Row><F1>166</F1><F3>1315 Chesapeake Ter</F3><F4>Sunnyvale</F4><F5>CA</F5><F6>1315 Chesapeake Ter
Sunnyvale, CA</F6></Row>')
,(91363, 53, 242, N'<Row><F1>167</F1><F2>Family Dollar</F2><F3>1316 S Main St</F3><F4>Greenwood</F4><F5>SC</F5><F6>Family Dollar
1316 S Main St
Greenwood, SC</F6></Row>')
,(91364, 53, 243, N'<Row><F1>168</F1><F3>1320-1324 Chesapeake Ter</F3><F4>Sunnyvale</F4><F5>CA</F5><F6>1320-1324 Chesapeake Ter
Sunnyvale, CA</F6></Row>')
,(91365, 53, 244, N'<Row><F1>169</F1><F3>1325-1327 Chesapeake Ter</F3><F4>Sunnyvale</F4><F5>CA</F5><F6>1325-1327 Chesapeake Ter
Sunnyvale, CA</F6></Row>')
,(91366, 53, 245, N'<Row><F1>170</F1><F2>Lute Riley Honda</F2><F3>1331 North Central Expy</F3><F4>Richardson</F4><F5>TX</F5><F6>Lute Riley Honda
1331 North Central Expy
Richardson, TX</F6></Row>')
,(91367, 53, 246, N'<Row><F1>1064</F1><F2>Embassy Suites Dulles Airport</F2><F3>13341 Woodland Park Rd</F3><F4>Herndon</F4><F5>VA</F5><F6>Embassy Suites Dulles Airport
13341 Woodland Park Rd
Herndon, VA</F6></Row>')
,(91368, 53, 247, N'<Row><F1>171</F1><F2>Chuck E. Cheese''s</F2><F3>1348 S. Yuma Palms Parkway</F3><F4>Yuma</F4><F5>AZ</F5><F6>Chuck E. Cheese''s
1348 S. Yuma Palms Parkway
Yuma, AZ</F6></Row>')
,(91369, 53, 248, N'<Row><F1>172</F1><F3>135 Bishopsgate</F3><F4>London</F4><F5>UK</F5><F6>135 Bishopsgate
London, UK</F6></Row>')
,(91370, 53, 249, N'<Row><F1>173</F1><F3>135 S State College Blvd</F3><F4>Brea</F4><F5>CA</F5><F6>135 S State College Blvd
Brea, CA</F6></Row>')
,(91371, 53, 250, N'<Row><F1>174</F1><F2>Royal St. Charles Hotel</F2><F3>135 St Charles Ave</F3><F4>New Orleans</F4><F5>LA</F5><F6>Royal St. Charles Hotel
135 St Charles Ave
New Orleans, LA</F6></Row>')
,(91372, 53, 251, N'<Row><F1>175</F1><F2>The Shoppes at Webb Gin</F2><F3>1350 Scenic Hwy</F3><F4>Snellville</F4><F5>GA</F5><F6>The Shoppes at Webb Gin
1350 Scenic Hwy
Snellville, GA</F6></Row>')
,(91373, 53, 252, N'<Row><F1>176</F1><F2>Billy Bender Chrysler Jeep Dodge</F2><F3>13500 Winchester Rd</F3><F4>Cumberland</F4><F5>MD</F5><F6>Billy Bender Chrysler Jeep Dodge
13500 Winchester Rd
Cumberland, MD</F6></Row>')
,(91374, 53, 253, N'<Row><F1>177</F1><F2>Lute Riley Honda Body Shop</F2><F3>13561 Goldmark Dr</F3><F4>Dallas</F4><F5>TX</F5><F6>Lute Riley Honda Body Shop
13561 Goldmark Dr
Dallas, TX</F6></Row>')
,(91375, 53, 254, N'<Row><F1>1205</F1><F2>Falmouth Mall</F2><F3>137 Teaticket Hwy</F3><F4>East Falmouth</F4><F5>MA</F5><F6>Falmouth Mall
137 Teaticket Hwy
East Falmouth, MA</F6></Row>')
,(91376, 53, 255, N'<Row><F1>178</F1><F2>CarMax Merrillville</F2><F3>1370 E 79th Pl</F3><F4>Merrillville</F4><F5>IN</F5><F6>CarMax Merrillville
1370 E 79th Pl
Merrillville, IN</F6></Row>')
,(91377, 53, 256, N'<Row><F1>179</F1><F2>Chuck E. Cheese''s</F2><F3>13745 Lakeside Cir</F3><F4>Sterling Heights</F4><F5>MI</F5><F6>Chuck E. Cheese''s
13745 Lakeside Cir
Sterling Heights, MI</F6></Row>')
,(91378, 53, 257, N'<Row><F1>180</F1><F2>Holiday Inn New York-Soho</F2><F3>138 Lafayette St</F3><F4>New York</F4><F5>NY</F5><F6>Holiday Inn New York-Soho
138 Lafayette St
New York, NY</F6></Row>')
,(91379, 53, 258, N'<Row><F1>181</F1><F2>Mini of Fort Myers</F2><F3>13880 S Tamiami Trl</F3><F4>Ft. Myers</F4><F5>FL</F5><F6>Mini of Fort Myers
13880 S Tamiami Trl
Ft. Myers, FL</F6></Row>')
,(91380, 53, 259, N'<Row><F1>182</F1><F2>Pohanka Lexus</F2><F3>13909 Lee Jackson Hwy</F3><F4>Chantilly</F4><F5>VA</F5><F6>Pohanka Lexus
13909 Lee Jackson Hwy
Chantilly, VA</F6></Row>')
,(91381, 53, 260, N'<Row><F1>183</F1><F2>Pohanka Acura &amp; Chevy</F2><F3>13911 Lee Jackson Hwy</F3><F4>Chantilly</F4><F5>VA</F5><F6>Pohanka Acura &amp; Chevy
13911 Lee Jackson Hwy
Chantilly, VA</F6></Row>')
,(91382, 53, 261, N'<Row><F1>184</F1><F2>Pohanka Used Cars and Service Center</F2><F3>13915 Lee Jackson Hwy</F3><F4>Chantilly</F4><F5>VA</F5><F6>Pohanka Used Cars and Service Center
13915 Lee Jackson Hwy
Chantilly, VA</F6></Row>')
,(91383, 53, 262, N'<Row><F1>185</F1><F2>Airport Acura</F2><F3>13930 Brookpark Rd</F3><F4>Cleveland</F4><F5>OH</F5><F6>Airport Acura
13930 Brookpark Rd
Cleveland, OH</F6></Row>')
,(91384, 53, 263, N'<Row><F1>186</F1><F2>Former BMW of Fort Myers (vacant but tenanted)</F2><F3>13950 S Tamiami Trl</F3><F4>Ft. Myers</F4><F5>FL</F5><F6>Former BMW of Fort Myers (vacant but tenanted)
13950 S Tamiami Trl
Ft. Myers, FL</F6></Row>')
,(91385, 53, 264, N'<Row><F1>187</F1><F3>140 S State College Blvd</F3><F4>Brea</F4><F5>CA</F5><F6>140 S State College Blvd
Brea, CA</F6></Row>')
,(91386, 53, 265, N'<Row><F1>1096</F1><F3>140 Stony Point Rd</F3><F4>Santa Rosa</F4><F5>CA</F5><F6>140 Stony Point Rd
Santa Rosa, CA</F6></Row>')
,(91387, 53, 266, N'<Row><F1>188</F1><F3>1400 12th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1400 12th Ave SE
Bellevue, WA</F6></Row>')
,(91388, 53, 267, N'<Row><F1>189</F1><F2>Family Dollar</F2><F3>1400 4th St</F3><F4>Westwego</F4><F5>LA</F5><F6>Family Dollar
1400 4th St
Westwego, LA</F6></Row>')
,(91389, 53, 268, N'<Row><F1>190</F1><F2>Orr BMW</F2><F3>1400 E 70th St</F3><F4>Shreveport</F4><F5>LA</F5><F6>Orr BMW
1400 E 70th St
Shreveport, LA</F6></Row>')
,(91390, 53, 269, N'<Row><F1>191</F1><F2>Century Centre II</F2><F3>1400 Fashion Island Blvd</F3><F4>San Mateo</F4><F5>CA</F5><F6>Century Centre II
1400 Fashion Island Blvd
San Mateo, CA</F6></Row>')
,(91391, 53, 270, N'<Row><F1>1036</F1><F3>1400 Page Mill Rd</F3><F4>Palo Alto</F4><F5>CA</F5><F6>1400 Page Mill Rd
Palo Alto, CA</F6></Row>')
,(91392, 53, 271, N'<Row><F1>192</F1><F2>Honda/VW of Fort Myers</F2><F3>14020 S Tamiami Trl</F3><F4>Ft. Myers</F4><F5>FL</F5><F6>Honda/VW of Fort Myers
14020 S Tamiami Trl
Ft. Myers, FL</F6></Row>')
,(91393, 53, 272, N'<Row><F1>193</F1><F2>Family Dollar</F2><F3>1403 E Gentry Pkwy</F3><F4>Tyler</F4><F5>TX</F5><F6>Family Dollar
1403 E Gentry Pkwy
Tyler, TX</F6></Row>')
,(91394, 53, 273, N'<Row><F1>194</F1><F2>Airport Complex Lot</F2><F3>14080 Brookpark Rd</F3><F4>Brookpark</F4><F5>OH</F5><F6>Airport Complex Lot
14080 Brookpark Rd
Brookpark, OH</F6></Row>')
,(91395, 53, 274, N'<Row><F1>195</F1><F2>Fort Myers Parking Lot</F2><F3>14080 Don Jacobs Ct</F3><F4>Fort Myers</F4><F5>FL</F5><F6>Fort Myers Parking Lot
14080 Don Jacobs Ct
Fort Myers, FL</F6></Row>')
,(91396, 53, 275, N'<Row><F1>196</F1><F2>Hall Automotive Body and Paint Shop</F2><F3>141 Spruce St</F3><F4>Virginia Beach</F4><F5>VA</F5><F6>Hall Automotive Body and Paint Shop
141 Spruce St
Virginia Beach, VA</F6></Row>')
,(91397, 53, 276, N'<Row><F1>197</F1><F2>Family Dollar</F2><F3>1412 Main St</F3><F4>Columbus</F4><F5>MS</F5><F6>Family Dollar
1412 Main St
Columbus, MS</F6></Row>')
,(91398, 53, 277, N'<Row><F1>198</F1><F2>Lithia CDJ Grants Pass</F2><F3>1421 NE 6th St</F3><F4>Grants Pass</F4><F5>OR</F5><F6>Lithia CDJ Grants Pass
1421 NE 6th St
Grants Pass, OR</F6></Row>')
,(91399, 53, 278, N'<Row><F1>199</F1><F2>Chuck E. Cheese''s</F2><F3>1429 E Kemper Rd</F3><F4>Sharonville</F4><F5>OH</F5><F6>Chuck E. Cheese''s
1429 E Kemper Rd
Sharonville, OH</F6></Row>')
,(91400, 53, 279, N'<Row><F1>200</F1><F2>Family Dollar</F2><F3>144 E Park Ave</F3><F4>Drew</F4><F5>MS</F5><F6>Family Dollar
144 E Park Ave
Drew, MS</F6></Row>')
,(91401, 53, 280, N'<Row><F1>201</F1><F3>145 S State College Blvd</F3><F4>Brea</F4><F5>CA</F5><F6>145 S State College Blvd
Brea, CA</F6></Row>')
,(91402, 53, 281, N'<Row><F1>202</F1><F3>1450 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1450 114th Ave SE
Bellevue, WA</F6></Row>')
,(91403, 53, 282, N'<Row><F1>1047</F1><F2>Ala Moana Center</F2><F3>1450 Ala Moana Blvd</F3><F4>Honolulu</F4><F5>HI</F5><F6>Ala Moana Center
1450 Ala Moana Blvd
Honolulu, HI</F6></Row>')
,(91404, 53, 283, N'<Row><F1>203</F1><F2>Century Centre I</F2><F3>1450 Fashion Island Blvd</F3><F4>San Mateo</F4><F5>CA</F5><F6>Century Centre I
1450 Fashion Island Blvd
San Mateo, CA</F6></Row>')
,(91405, 53, 284, N'<Row><F1>1037</F1><F3>1460 Broadway</F3><F4>New York</F4><F5>NY</F5><F6>1460 Broadway
New York, NY</F6></Row>')
,(91406, 53, 285, N'<Row><F1>204</F1><F2>Liberty Center I</F2><F3>14660 Lee Rd</F3><F4>Chantilly</F4><F5>VA</F5><F6>Liberty Center I
14660 Lee Rd
Chantilly, VA</F6></Row>')
,(91407, 53, 286, N'<Row><F1>205</F1><F2>Liberty Center III</F2><F3>14668 Lee Rd</F3><F4>Chantilly</F4><F5>VA</F5><F6>Liberty Center III
14668 Lee Rd
Chantilly, VA</F6></Row>')
,(91408, 53, 287, N'<Row><F1>206</F1><F2>Liberty Center II</F2><F3>14672 Lee Rd</F3><F4>Chantilly</F4><F5>VA</F5><F6>Liberty Center II
14672 Lee Rd
Chantilly, VA</F6></Row>')
,(91409, 53, 288, N'<Row><F1>207</F1><F2>Toyota of West County</F2><F3>14700 Manchester Rd</F3><F4>Ballwin</F4><F5>MO</F5><F6>Toyota of West County
14700 Manchester Rd
Ballwin, MO</F6></Row>')
,(91410, 53, 289, N'<Row><F1>1093</F1><F3>149 Stony Point Cir</F3><F4>Santa Rosa</F4><F5>CA</F5><F6>149 Stony Point Cir
Santa Rosa, CA</F6></Row>')
,(91411, 53, 290, N'<Row><F1>208</F1><F2>Hatfield Volkswagen</F2><F3>1495 Auto Mall Dr</F3><F4>Columbus</F4><F5>OH</F5><F6>Hatfield Volkswagen
1495 Auto Mall Dr
Columbus, OH</F6></Row>')
,(91412, 53, 291, N'<Row><F1>209</F1><F2>Residence Inn Philadelphia Langhorne</F2><F3>15 Cabot Blvd E</F3><F4>Langhorne</F4><F5>PA</F5><F6>Residence Inn Philadelphia Langhorne
15 Cabot Blvd E
Langhorne, PA</F6></Row>')
,(91413, 53, 292, N'<Row><F1>1016</F1><F3>15 E 26th St</F3><F4>New York</F4><F5>NY</F5><F6>15 E 26th St
New York, NY</F6></Row>')
,(91414, 53, 293, N'<Row><F1>210</F1><F3>150 Bear Hill Rd</F3><F4>Waltham</F4><F5>MA</F5><F6>150 Bear Hill Rd
Waltham, MA</F6></Row>')
,(91415, 53, 294, N'<Row><F1>211</F1><F3>150 E 42nd St</F3><F4>New York</F4><F5>NY</F5><F6>150 E 42nd St
New York, NY</F6></Row>')
,(91416, 53, 295, N'<Row><F1>1272</F1><F2>Sunnyvale City Center - Building 2</F2><F3>150 Mathilda Pl</F3><F4>Sunnyvale</F4><F5>CA</F5><F6>Sunnyvale City Center - Building 2
150 Mathilda Pl
Sunnyvale, CA</F6></Row>')
,(91417, 53, 296, N'<Row><F1>212</F1><F2>MotorWorld</F2><F3>150 MotorWorld Dr</F3><F4>Wilkes-Barre</F4><F5>PA</F5><F6>MotorWorld
150 MotorWorld Dr
Wilkes-Barre, PA</F6></Row>')
,(91418, 53, 297, N'<Row><F1>213</F1><F3>150 W 34th St</F3><F4>New York</F4><F5>NY</F5><F6>150 W 34th St
New York, NY</F6></Row>')
,(91419, 53, 298, N'<Row><F1>214</F1><F3>1500 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1500 114th Ave SE
Bellevue, WA</F6></Row>')
,(91420, 53, 299, N'<Row><F1>1038</F1><F3>1500 Broadway</F3><F4>New York</F4><F5>NY</F5><F6>1500 Broadway
New York, NY</F6></Row>')
,(91421, 53, 300, N'<Row><F1>215</F1><F3>1500 Busse Hwy</F3><F4>Elk Grove</F4><F5>IL</F5><F6>1500 Busse Hwy
Elk Grove, IL</F6></Row>')
,(91422, 53, 301, N'<Row><F1>216</F1><F3>1500 Cornerside Blvd</F3><F4>Vienna</F4><F5>VA</F5><F6>1500 Cornerside Blvd
Vienna, VA</F6></Row>')
,(91423, 53, 302, N'<Row><F1>1133</F1><F2>Freeway Business Center</F2><F3>1500 Hughes Way</F3><F4>Long Beach</F4><F5>CA</F5><F6>Freeway Business Center
1500 Hughes Way
Long Beach, CA</F6></Row>')
,(91424, 53, 303, N'<Row><F1>1117</F1><F3>1501 W Fountainhead Pkwy</F3><F4>Tempe</F4><F5>AZ</F5><F6>1501 W Fountainhead Pkwy
Tempe, AZ</F6></Row>')
,(91425, 53, 304, N'<Row><F1>217</F1><F2>Family Dollar</F2><F3>15013 N US Highway 83</F3><F4>La Pryor</F4><F5>TX</F5><F6>Family Dollar
15013 N US Highway 83
La Pryor, TX</F6></Row>')
,(91426, 53, 305, N'<Row><F1>218</F1><F2>Family Dollar</F2><F3>1505 Anderson Dr</F3><F4>Williamston</F4><F5>SC</F5><F6>Family Dollar
1505 Anderson Dr
Williamston, SC</F6></Row>')
,(91427, 53, 306, N'<Row><F1>219</F1><F2>Clear Lake Volkswagen</F2><F3>15100 Gulf Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Clear Lake Volkswagen
15100 Gulf Fwy
Houston, TX</F6></Row>')
,(91428, 53, 307, N'<Row><F1>220</F1><F2>Rocket Harley-Davidson</F2><F3>15100 Hwy 20 W</F3><F4>Madison</F4><F5>AL</F5><F6>Rocket Harley-Davidson
15100 Hwy 20 W
Madison, AL</F6></Row>')
,(91429, 53, 308, N'<Row><F1>221</F1><F2>Family Dollar</F2><F3>1513 Charlotte Hwy</F3><F4>Lancaster</F4><F5>SC</F5><F6>Family Dollar
1513 Charlotte Hwy
Lancaster, SC</F6></Row>')
,(91430, 53, 309, N'<Row><F1>222</F1><F2>Inskip Auto Mall</F2><F3>1515 Bald Hill Rd</F3><F4>Warwick</F4><F5>RI</F5><F6>Inskip Auto Mall
1515 Bald Hill Rd
Warwick, RI</F6></Row>')
,(91431, 53, 310, N'<Row><F1>223</F1><F2>Chuck E. Cheese''s</F2><F3>15225 W 134th Street</F3><F4>Olathe</F4><F5>KS</F5><F6>Chuck E. Cheese''s
15225 W 134th Street
Olathe, KS</F6></Row>')
,(91432, 53, 311, N'<Row><F1>1118</F1><F3>1540 W Fountainhead Pkwy</F3><F4>Tempe</F4><F5>AZ</F5><F6>1540 W Fountainhead Pkwy
Tempe, AZ</F6></Row>')
,(91433, 53, 312, N'<Row><F1>224</F1><F2>Sonic Body Shop</F2><F3>1548 Spring Hill Rd</F3><F4>McLean</F4><F5>VA</F5><F6>Sonic Body Shop
1548 Spring Hill Rd
McLean, VA</F6></Row>')
,(91434, 53, 313, N'<Row><F1>225</F1><F3>155 Bishopsgate</F3><F4>London</F4><F5>UK</F5><F6>155 Bishopsgate
London, UK</F6></Row>')
,(91435, 53, 314, N'<Row><F1>226</F1><F2>Family Dollar</F2><F3>155 W Park Ave</F3><F4>Ash Fork</F4><F5>AZ</F5><F6>Family Dollar
155 W Park Ave
Ash Fork, AZ</F6></Row>')
,(91436, 53, 315, N'<Row><F1>1119</F1><F3>1560 W Fountainhead Pkwy</F3><F4>Tempe</F4><F5>AZ</F5><F6>1560 W Fountainhead Pkwy
Tempe, AZ</F6></Row>')
,(91437, 53, 316, N'<Row><F1>227</F1><F2>JSI Collision Center - CL (JSI#33)</F2><F3>15703 Puritas Ave</F3><F4>Cleveland</F4><F5>OH</F5><F6>JSI Collision Center - CL (JSI#33)
15703 Puritas Ave
Cleveland, OH</F6></Row>')
,(91438, 53, 317, N'<Row><F1>1122</F1><F3>158 W 27th St</F3><F4>New York</F4><F5>NY</F5><F6>158 W 27th St
New York, NY</F6></Row>')
,(91439, 53, 318, N'<Row><F1>228</F1><F2>Honda of Tysons Corner/Rosenthal Jaguar</F2><F3>1580 Spring Hill Rd</F3><F4>Vienna</F4><F5>VA</F5><F6>Honda of Tysons Corner/Rosenthal Jaguar
1580 Spring Hill Rd
Vienna, VA</F6></Row>')
,(91440, 53, 319, N'<Row><F1>229</F1><F2>Momentum BMW West                                           </F2><F3>15865 Katy Fwy                 </F3><F4>Houston                       </F4><F5>TX </F5><F6>Momentum BMW West                                           
15865 Katy Fwy                 
Houston                       , TX </F6></Row>')
,(91441, 53, 320, N'<Row><F1>1210</F1><F2>Rochester Crossing</F2><F3>160 Washington St</F3><F4>Rochester</F4><F5>NH</F5><F6>Rochester Crossing
160 Washington St
Rochester, NH</F6></Row>')
,(91442, 53, 321, N'<Row><F1>230</F1><F3>1601 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1601 114th Ave SE
Bellevue, WA</F6></Row>')
,(91443, 53, 322, N'<Row><F1>231</F1><F2>Greg May Honda</F2><F3>1601 Texas 340 Loop</F3><F4>Waco</F4><F5>TX</F5><F6>Greg May Honda
1601 Texas 340 Loop
Waco, TX</F6></Row>')
,(91444, 53, 323, N'<Row><F1>1006</F1><F2>Pompano Plaza</F2><F3>1602 S Cypress Rd</F3><F4>Pompano Beach </F4><F5>FL</F5><F6>Pompano Plaza
1602 S Cypress Rd
Pompano Beach , FL</F6></Row>')
,(91445, 53, 324, N'<Row><F1>232</F1><F2>Nalley Infiniti Decatur</F2><F3>1609 Church St</F3><F4>Decatur</F4><F5>GA</F5><F6>Nalley Infiniti Decatur
1609 Church St
Decatur, GA</F6></Row>')
,(91446, 53, 325, N'<Row><F1>233</F1><F3>1620 L St NW</F3><F4>Washington</F4><F5>DC</F5><F6>1620 L St NW
Washington, DC</F6></Row>')
,(91447, 53, 326, N'<Row><F1>1120</F1><F3>1620 W Fountainhead Pkwy</F3><F4>Tempe</F4><F5>AZ</F5><F6>1620 W Fountainhead Pkwy
Tempe, AZ</F6></Row>')
,(91448, 53, 327, N'<Row><F1>234</F1><F3>1621 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1621 114th Ave SE
Bellevue, WA</F6></Row>')
,(91449, 53, 328, N'<Row><F1>235</F1><F2>Nalley Nissan</F2><F3>1625 Church St</F3><F4>Decatur</F4><F5>GA</F5><F6>Nalley Nissan
1625 Church St
Decatur, GA</F6></Row>')
,(91450, 53, 329, N'<Row><F1>236</F1><F2>Hotel Kabuki</F2><F3>1625 Post St</F3><F4>San Francisco</F4><F5>CA</F5><F6>Hotel Kabuki
1625 Post St
San Francisco, CA</F6></Row>')
,(91451, 53, 330, N'<Row><F1>237</F1><F2>Family Dollar</F2><F3>1628 MLK Jr. Pkwy</F3><F4>Griffin</F4><F5>GA</F5><F6>Family Dollar
1628 MLK Jr. Pkwy
Griffin, GA</F6></Row>')
,(91452, 53, 331, N'<Row><F1>238</F1><F2>Lithia Toyota Scion of Springfield</F2><F3>163 S 9th St</F3><F4>Springfield</F4><F5>OR</F5><F6>Lithia Toyota Scion of Springfield
163 S 9th St
Springfield, OR</F6></Row>')
,(91453, 53, 332, N'<Row><F1>239</F1><F3>1633 Broadway</F3><F4>New York</F4><F5>NY</F5><F6>1633 Broadway
New York, NY</F6></Row>')
,(91454, 53, 333, N'<Row><F1>240</F1><F2>Chuck E. Cheese''s</F2><F3>1636 N Germantown Pkwy</F3><F4>Cordova</F4><F5>TN</F5><F6>Chuck E. Cheese''s
1636 N Germantown Pkwy
Cordova, TN</F6></Row>')
,(91455, 53, 334, N'<Row><F1>241</F1><F2>Mercedes-Benz of Fairfield</F2><F3>165 Commerce Dr</F3><F4>Fairfield</F4><F5>CT</F5><F6>Mercedes-Benz of Fairfield
165 Commerce Dr
Fairfield, CT</F6></Row>')
,(91456, 53, 335, N'<Row><F1>242</F1><F2>Land Rover South Dade</F2><F3>16750 S Dixie Hwy</F3><F4>Miami</F4><F5>FL</F5><F6>Land Rover South Dade
16750 S Dixie Hwy
Miami, FL</F6></Row>')
,(91457, 53, 336, N'<Row><F1>243</F1><F2>Chuck E. Cheese''s</F2><F3>16790 Interstate 45 South</F3><F4>Conroe</F4><F5>TX</F5><F6>Chuck E. Cheese''s
16790 Interstate 45 South
Conroe, TX</F6></Row>')
,(91458, 53, 337, N'<Row><F1>244</F1><F2>Family Dollar</F2><F3>1683 Appling Rd</F3><F4>Cordova</F4><F5>TN</F5><F6>Family Dollar
1683 Appling Rd
Cordova, TN</F6></Row>')
,(91459, 53, 338, N'<Row><F1>245</F1><F3>1687 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1687 114th Ave SE
Bellevue, WA</F6></Row>')
,(91460, 53, 339, N'<Row><F1>246</F1><F2>Auffenberg Nissan</F2><F3>1690 New Car Dr</F3><F4>O''Fallon</F4><F5>IL</F5><F6>Auffenberg Nissan
1690 New Car Dr
O''Fallon, IL</F6></Row>')
,(91461, 53, 340, N'<Row><F1>247</F1><F3>170 Broadway</F3><F4>New York</F4><F5>NY</F5><F6>170 Broadway
New York, NY</F6></Row>')
,(91462, 53, 341, N'<Row><F1>248</F1><F3>170 W North Ave</F3><F4>Northlake</F4><F5>IL</F5><F6>170 W North Ave
Northlake, IL</F6></Row>')
,(91463, 53, 342, N'<Row><F1>1039</F1><F3>1700 E Putnam Ave</F3><F4>Old Greenwich</F4><F5>CT</F5><F6>1700 E Putnam Ave
Old Greenwich, CT</F6></Row>')
,(91464, 53, 343, N'<Row><F1>249</F1><F3>1700 Montgomery St</F3><F4>San Francisco</F4><F5>CA</F5><F6>1700 Montgomery St
San Francisco, CA</F6></Row>')
,(91465, 53, 344, N'<Row><F1>250</F1><F2>The Shops at Highland Village</F2><F3>1701 Shoal Creek</F3><F4>Highland Village</F4><F5>TX</F5><F6>The Shops at Highland Village
1701 Shoal Creek
Highland Village, TX</F6></Row>')
,(91466, 53, 345, N'<Row><F1>251</F1><F2>North Park Plaza</F2><F3>1702-1712 Oakland Rd</F3><F4>San Jose</F4><F5>CA</F5><F6>North Park Plaza
1702-1712 Oakland Rd
San Jose, CA</F6></Row>')
,(91467, 53, 346, N'<Row><F1>252</F1><F2>Family Dollar</F2><F3>1707 Simpson Hwy</F3><F4>Mendenhall</F4><F5>MS</F5><F6>Family Dollar
1707 Simpson Hwy
Mendenhall, MS</F6></Row>')
,(91468, 53, 347, N'<Row><F1>253</F1><F2>St. Clair Auto Mall</F2><F3>1708 New Car Dr</F3><F4>O''Fallon</F4><F5>IL</F5><F6>St. Clair Auto Mall
1708 New Car Dr
O''Fallon, IL</F6></Row>')
,(91469, 53, 348, N'<Row><F1>254</F1><F2>Auffenberg Volkswagen</F2><F3>1708 New Car Dr</F3><F4>O''Fallon</F4><F5>IL</F5><F6>Auffenberg Volkswagen
1708 New Car Dr
O''Fallon, IL</F6></Row>')
,(91470, 53, 349, N'<Row><F1>255</F1><F3>171 17th St</F3><F4>Atlanta</F4><F5>GA</F5><F6>171 17th St
Atlanta, GA</F6></Row>')
,(91471, 53, 350, N'<Row><F1>256</F1><F2>West Houston Volkswagen</F2><F3>17113 Katy Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>West Houston Volkswagen
17113 Katy Fwy
Houston, TX</F6></Row>')
,(91472, 53, 351, N'<Row><F1>257</F1><F3>1715 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1715 114th Ave SE
Bellevue, WA</F6></Row>')
,(91473, 53, 352, N'<Row><F1>258</F1><F2>Former Mercedes-Benz of Daytona Beach</F2><F3>1720 Mason Ave</F3><F4>Daytona Beach</F4><F5>FL</F5><F6>Former Mercedes-Benz of Daytona Beach
1720 Mason Ave
Daytona Beach, FL</F6></Row>')
,(91474, 53, 353, N'<Row><F1>259</F1><F2>Chuck E. Cheese''s</F2><F3>1725 Metro Dr</F3><F4>Alexandria</F4><F5>LA</F5><F6>Chuck E. Cheese''s
1725 Metro Dr
Alexandria, LA</F6></Row>')
,(91475, 53, 354, N'<Row><F1>260</F1><F2>Honda of Spring</F2><F3>17350 N Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Honda of Spring
17350 N Fwy
Houston, TX</F6></Row>')
,(91476, 53, 355, N'<Row><F1>261</F1><F2>The Mall at Partridge Creek</F2><F3>17420 Hall Rd</F3><F4>Charter Township of Clinton</F4><F5>MI</F5><F6>The Mall at Partridge Creek
17420 Hall Rd
Charter Township of Clinton, MI</F6></Row>')
,(91477, 53, 356, N'<Row><F1>262</F1><F3>175 Varick St</F3><F4>New York</F4><F5>NY</F5><F6>175 Varick St
New York, NY</F6></Row>')
,(91478, 53, 357, N'<Row><F1>263</F1><F2>The Rim</F2><F3>17503 Interstate 10 Frontage Rd</F3><F4>San Antonio</F4><F5>TX</F5><F6>The Rim
17503 Interstate 10 Frontage Rd
San Antonio, TX</F6></Row>')
,(91479, 53, 358, N'<Row><F1>264</F1><F3>1756 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1756 114th Ave SE
Bellevue, WA</F6></Row>')
,(91480, 53, 359, N'<Row><F1>265</F1><F2>Future Victory Motorcycle (former smart Cars of Bloomfield)</F2><F3>1765 S Telegraph Rd</F3><F4>Bloomfield Hills</F4><F5>MI</F5><F6>Future Victory Motorcycle (former smart Cars of Bloomfield)
1765 S Telegraph Rd
Bloomfield Hills, MI</F6></Row>')
,(91481, 53, 360, N'<Row><F1>266</F1><F2>Hilton Alexandria Old Town </F2><F3>1767 King St</F3><F4>Alexandria</F4><F5>VA</F5><F6>Hilton Alexandria Old Town 
1767 King St
Alexandria, VA</F6></Row>')
,(91482, 53, 361, N'<Row><F1>267</F1><F2>Chuck E. Cheese''s</F2><F3>1775 Burning Tree Dr</F3><F4>Columbia</F4><F5>SC</F5><F6>Chuck E. Cheese''s
1775 Burning Tree Dr
Columbia, SC</F6></Row>')
,(91483, 53, 362, N'<Row><F1>268</F1><F2>North Houston Infiniti</F2><F3>17825 N Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>North Houston Infiniti
17825 N Fwy
Houston, TX</F6></Row>')
,(91484, 53, 363, N'<Row><F1>269</F1><F2>Oracle Tower </F2><F3>17901 Von Karman Ave</F3><F4>Irvine</F4><F5>CA</F5><F6>Oracle Tower 
17901 Von Karman Ave
Irvine, CA</F6></Row>')
,(91485, 53, 364, N'<Row><F1>1217</F1><F2>Hotel Russell</F2><F3>1-8 Russell Sq</F3><F4>London WC1B 5BE</F4><F5>UK</F5><F6>Hotel Russell
1-8 Russell Sq
London WC1B 5BE, UK</F6></Row>')
,(91486, 53, 365, N'<Row><F1>270</F1><F3>180 Broadway</F3><F4>New York</F4><F5>NY</F5><F6>180 Broadway
New York, NY</F6></Row>')
,(91487, 53, 366, N'<Row><F1>1024</F1><F3>180 N LaSalle St</F3><F4>Chicago</F4><F5>IL</F5><F6>180 N LaSalle St
Chicago, IL</F6></Row>')
,(91488, 53, 367, N'<Row><F1>1001</F1><F2>Miami Gardens Square</F2><F3>180 NW 183rd St</F3><F4>Miami</F4><F5>FL</F5><F6>Miami Gardens Square
180 NW 183rd St
Miami, FL</F6></Row>')
,(91489, 53, 368, N'<Row><F1>271</F1><F3>1800 114th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>1800 114th Ave SE
Bellevue, WA</F6></Row>')
,(91490, 53, 369, N'<Row><F1>272</F1><F2>San Mateo Centre</F2><F3>1800 Gateway Dr</F3><F4>San Mateo</F4><F5>CA</F5><F6>San Mateo Centre
1800 Gateway Dr
San Mateo, CA</F6></Row>')
,(91491, 53, 370, N'<Row><F1>273</F1><F3>1800 Montgomery St</F3><F4>San Francisco</F4><F5>CA</F5><F6>1800 Montgomery St
San Francisco, CA</F6></Row>')
,(91492, 53, 371, N'<Row><F1>274</F1><F2>Hotel Tomo</F2><F3>1800 Sutter Street</F3><F4>San Francisco</F4><F5>CA</F5><F6>Hotel Tomo
1800 Sutter Street
San Francisco, CA</F6></Row>')
,(91493, 53, 372, N'<Row><F1>275</F1><F2>Peninsula Town Center</F2><F3>1800 W Mercury Blvd</F3><F4>Hampton</F4><F5>VA</F5><F6>Peninsula Town Center
1800 W Mercury Blvd
Hampton, VA</F6></Row>')
,(91494, 53, 373, N'<Row><F1>276</F1><F2>Scottsdale 101 Auto Collection</F2><F3>18000 N Scottsdale Rd</F3><F4>Phoenix</F4><F5>AZ</F5><F6>Scottsdale 101 Auto Collection
18000 N Scottsdale Rd
Phoenix, AZ</F6></Row>')
,(91495, 53, 374, N'<Row><F1>277</F1><F2>Family Dollar</F2><F3>1809 Jefferson Davis Hwy</F3><F4>Camden</F4><F5>SC</F5><F6>Family Dollar
1809 Jefferson Davis Hwy
Camden, SC</F6></Row>')
,(91496, 53, 375, N'<Row><F1>278</F1><F2>San Mateo Centre</F2><F3>1810 Gateway Dr</F3><F4>San Mateo</F4><F5>CA</F5><F6>San Mateo Centre
1810 Gateway Dr
San Mateo, CA</F6></Row>')
,(91497, 53, 376, N'<Row><F1>279</F1><F2>Toyota/Scion of Bedford</F2><F3>18151 Rockside Rd</F3><F4>Bedford</F4><F5>OH</F5><F6>Toyota/Scion of Bedford
18151 Rockside Rd
Bedford, OH</F6></Row>')
,(91498, 53, 377, N'<Row><F1>280</F1><F2>Clear Lake Lexus</F2><F3>18160 Gulf Fwy</F3><F4>Friendswood</F4><F5>TX</F5><F6>Clear Lake Lexus
18160 Gulf Fwy
Friendswood, TX</F6></Row>')
,(91499, 53, 378, N'<Row><F1>281</F1><F2>San Mateo Centre</F2><F3>1820 Gateway Dr</F3><F4>San Mateo</F4><F5>CA</F5><F6>San Mateo Centre
1820 Gateway Dr
San Mateo, CA</F6></Row>')
,(91500, 53, 379, N'<Row><F1>282</F1><F2>Family Dollar</F2><F3>18215 N US Highway 301</F3><F4>Citra</F4><F5>FL</F5><F6>Family Dollar
18215 N US Highway 301
Citra, FL</F6></Row>')
,(91501, 53, 380, N'<Row><F1>283</F1><F2>ADESA Minneapolis</F2><F3>18270 Territorial Rd</F3><F4>Dayton</F4><F5>MN</F5><F6>ADESA Minneapolis
18270 Territorial Rd
Dayton, MN</F6></Row>')
,(91502, 53, 381, N'<Row><F1>1157</F1><F2>Redmond Commerce Center - Building 1</F2><F3>18340 NE 76th St</F3><F4>Redmond</F4><F5>WA</F5><F6>Redmond Commerce Center - Building 1
18340 NE 76th St
Redmond, WA</F6></Row>')
,(91503, 53, 382, N'<Row><F1>1158</F1><F2>Redmond Commerce Center - Building 3</F2><F3>18460 NE 76th St</F3><F4>Redmond</F4><F5>WA</F5><F6>Redmond Commerce Center - Building 3
18460 NE 76th St
Redmond, WA</F6></Row>')
,(91504, 53, 383, N'<Row><F1>1144</F1><F2>Hyatt Place Fort Lauderdale</F2><F3>1851 SE 10th Ave</F3><F4>Fort Lauderdale</F4><F5>FL</F5><F6>Hyatt Place Fort Lauderdale
1851 SE 10th Ave
Fort Lauderdale, FL</F6></Row>')
,(91505, 53, 384, N'<Row><F1>284</F1><F2>Mike Smith Autoplex</F2><F3>1855 Interstate 10 S</F3><F4>Beaumont</F4><F5>TX</F5><F6>Mike Smith Autoplex
1855 Interstate 10 S
Beaumont, TX</F6></Row>')
,(91506, 53, 385, N'<Row><F1>285</F1><F2>Mike Smith Autoplex</F2><F3>1865 Interstate 10 S</F3><F4>Beaumont</F4><F5>TX</F5><F6>Mike Smith Autoplex
1865 Interstate 10 S
Beaumont, TX</F6></Row>')
,(91507, 53, 386, N'<Row><F1>286</F1><F3>1875 S Bascom Ave</F3><F4>Campbell</F4><F5>CA</F5><F6>1875 S Bascom Ave
Campbell, CA</F6></Row>')
,(91508, 53, 387, N'<Row><F1>287</F1><F2>Lonestar Chevrolet</F2><F3>18800 Northwest Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Lonestar Chevrolet
18800 Northwest Fwy
Houston, TX</F6></Row>')
,(91509, 53, 388, N'<Row><F1>288</F1><F2>Family Dollar</F2><F3>18815 Highway 231</F3><F4>Fountain</F4><F5>FL</F5><F6>Family Dollar
18815 Highway 231
Fountain, FL</F6></Row>')
,(91510, 53, 389, N'<Row><F1>289</F1><F2>Fairlane Town Center</F2><F3>18900 Michigan Ave</F3><F4>Dearborn</F4><F5>MI</F5><F6>Fairlane Town Center
18900 Michigan Ave
Dearborn, MI</F6></Row>')
,(91511, 53, 390, N'<Row><F1>290</F1><F2>Lonestar Chevrolet Parking Lot</F2><F3>18990 Northwest Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Lonestar Chevrolet Parking Lot
18990 Northwest Fwy
Houston, TX</F6></Row>')
,(91512, 53, 391, N'<Row><F1>1271</F1><F2>Sunnyvale City Center - Building 1</F2><F3>190 Mathilda Pl</F3><F4>Sunnyvale</F4><F5>CA</F5><F6>Sunnyvale City Center - Building 1
190 Mathilda Pl
Sunnyvale, CA</F6></Row>')
,(91513, 53, 392, N'<Row><F1>291</F1><F3>190 S LaSalle St</F3><F4>Chicago</F4><F5>IL</F5><F6>190 S LaSalle St
Chicago, IL</F6></Row>')
,(91514, 53, 393, N'<Row><F1>292</F1><F3>1900 W Golf Rd</F3><F4>Rolling Meadows</F4><F5>IL</F5><F6>1900 W Golf Rd
Rolling Meadows, IL</F6></Row>')
,(91515, 53, 394, N'<Row><F1>293</F1><F3>1901 S Bascom Ave</F3><F4>Campbell</F4><F5>CA</F5><F6>1901 S Bascom Ave
Campbell, CA</F6></Row>')
,(91516, 53, 395, N'<Row><F1>294</F1><F2>Berlin City TSL Portland</F2><F3>191 Riverside St</F3><F4>Portland</F4><F5>ME</F5><F6>Berlin City TSL Portland
191 Riverside St
Portland, ME</F6></Row>')
,(91517, 53, 396, N'<Row><F1>1100</F1><F2>Casa del Mar</F2><F3>1910 Ocean Way</F3><F4>Santa Monica</F4><F5>CA</F5><F6>Casa del Mar
1910 Ocean Way
Santa Monica, CA</F6></Row>')
,(91518, 53, 397, N'<Row><F1>1261</F1><F3>1918 8th Ave</F3><F4>Seattle</F4><F5>WA</F5><F6>1918 8th Ave
Seattle, WA</F6></Row>')
,(91519, 53, 398, N'<Row><F1>295</F1><F2>New Smyrna Chevrolet</F2><F3>1919 N Dixie Fwy</F3><F4>New Smyrna Beach</F4><F5>FL</F5><F6>New Smyrna Chevrolet
1919 N Dixie Fwy
New Smyrna Beach, FL</F6></Row>')
,(91520, 53, 399, N'<Row><F1>296</F1><F3>1919 S Bascom Ave</F3><F4>Campbell</F4><F5>CA</F5><F6>1919 S Bascom Ave
Campbell, CA</F6></Row>')
,(91521, 53, 400, N'<Row><F1>1219</F1><F2>The George Hotel</F2><F3>19-21 George St</F3><F4>Edinburgh EH2 2PB</F4><F5>UK</F5><F6>The George Hotel
19-21 George St
Edinburgh EH2 2PB, UK</F6></Row>')
,(91522, 53, 401, N'<Row><F1>297</F1><F2>Family Dollar</F2><F3>1932 E Magnolia Ave</F3><F4>Knoxville</F4><F5>TN</F5><F6>Family Dollar
1932 E Magnolia Ave
Knoxville, TN</F6></Row>')
,(91523, 53, 402, N'<Row><F1>298</F1><F2>Mike Smith Autoplex</F2><F3>1945 Interstate 10 S</F3><F4>Beaumont</F4><F5>TX</F5><F6>Mike Smith Autoplex
1945 Interstate 10 S
Beaumont, TX</F6></Row>')
,(91524, 53, 403, N'<Row><F1>299</F1><F2>Lithia Ford Lincoln of Fresno</F2><F3>195 E Auto Center Dr</F3><F4>Fresno</F4><F5>CA</F5><F6>Lithia Ford Lincoln of Fresno
195 E Auto Center Dr
Fresno, CA</F6></Row>')
,(91525, 53, 404, N'<Row><F1>300</F1><F2>Momentum VW of Jersey Village</F2><F3>19550 Northwest Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Momentum VW of Jersey Village
19550 Northwest Fwy
Houston, TX</F6></Row>')
,(91526, 53, 405, N'<Row><F1>301</F1><F3>1956 Weber St</F3><F4>Oakland</F4><F5>CA</F5><F6>1956 Weber St
Oakland, CA</F6></Row>')
,(91527, 53, 406, N'<Row><F1>302</F1><F3>199 Bishopsgate</F3><F4>London</F4><F5>UK</F5><F6>199 Bishopsgate
London, UK</F6></Row>')
,(91528, 53, 407, N'<Row><F1>303</F1><F3>1992 W Jefferson Ave</F3><F4>Naperville</F4><F5>IL</F5><F6>1992 W Jefferson Ave
Naperville, IL</F6></Row>')
,(91529, 53, 408, N'<Row><F1>304</F1><F3>1995 S Bascom Ave</F3><F4>Campbell</F4><F5>CA</F5><F6>1995 S Bascom Ave
Campbell, CA</F6></Row>')
,(91530, 53, 409, N'<Row><F1>306</F1><F3>1999 Harrison St</F3><F4>Oakland</F4><F5>CA</F5><F6>1999 Harrison St
Oakland, CA</F6></Row>')
,(91531, 53, 410, N'<Row><F1>307</F1><F3>1999 S Bascom Ave</F3><F4>Campbell</F4><F5>CA</F5><F6>1999 S Bascom Ave
Campbell, CA</F6></Row>')
,(91532, 53, 411, N'<Row><F1>308</F1><F3>2 Finsbury Ave</F3><F4>London</F4><F5>UK</F5><F6>2 Finsbury Ave
London, UK</F6></Row>')
,(91533, 53, 412, N'<Row><F1>1152</F1><F2>The Palace Hotel</F2><F3>2 New Montgomery St</F3><F4>San Francisco</F4><F5>CA</F5><F6>The Palace Hotel
2 New Montgomery St
San Francisco, CA</F6></Row>')
,(91534, 53, 413, N'<Row><F1>309</F1><F3>20 Crosby Dr</F3><F4>Bedford</F4><F5>MA</F5><F6>20 Crosby Dr
Bedford, MA</F6></Row>')
,(91535, 53, 414, N'<Row><F1>310</F1><F3>20 S Washington Ave</F3><F4>Minneapolis</F4><F5>MN</F5><F6>20 S Washington Ave
Minneapolis, MN</F6></Row>')
,(91536, 53, 415, N'<Row><F1>311</F1><F2>Future Kia of Covington</F2><F3>200 Holiday Square Service Rd</F3><F4>Covington</F4><F5>LA</F5><F6>Future Kia of Covington
200 Holiday Square Service Rd
Covington, LA</F6></Row>')
,(91537, 53, 416, N'<Row><F1>312</F1><F3>2000 Avenue of the Stars</F3><F4>Century City</F4><F5>CA</F5><F6>2000 Avenue of the Stars
Century City, CA</F6></Row>')
,(91538, 53, 417, N'<Row><F1>1072</F1><F2>Courtyard Fort Lauderdale Weston</F2><F3>2000 N Commerce Pkwy</F3><F4>Fort Lauderdale</F4><F5>FL</F5><F6>Courtyard Fort Lauderdale Weston
2000 N Commerce Pkwy
Fort Lauderdale, FL</F6></Row>')
,(91539, 53, 418, N'<Row><F1>313</F1><F2>Hampton Inn Long Island Brookhaven</F2><F3>2000 N Ocean Ave</F3><F4>Farmingville</F4><F5>NY</F5><F6>Hampton Inn Long Island Brookhaven
2000 N Ocean Ave
Farmingville, NY</F6></Row>')
,(91540, 53, 419, N'<Row><F1>314</F1><F3>2001 Pennsylvania Ave NW</F3><F4>Washington</F4><F5>DC</F5><F6>2001 Pennsylvania Ave NW
Washington, DC</F6></Row>')
,(91541, 53, 420, N'<Row><F1>315</F1><F2>Blue Ridge Harley-Davidson</F2><F3>2002 13th Ave Dr SE</F3><F4>Hickory</F4><F5>NC</F5><F6>Blue Ridge Harley-Davidson
2002 13th Ave Dr SE
Hickory, NC</F6></Row>')
,(91542, 53, 421, N'<Row><F1>316</F1><F2>Chuck E. Cheese''s</F2><F3>2002 Gulfmont Dr</F3><F4>Katy</F4><F5>TX</F5><F6>Chuck E. Cheese''s
2002 Gulfmont Dr
Katy, TX</F6></Row>')
,(91543, 53, 422, N'<Row><F1>317</F1><F3>20070 Ashbrook Commons Pl</F3><F4>Ashburn</F4><F5>VA</F5><F6>20070 Ashbrook Commons Pl
Ashburn, VA</F6></Row>')
,(91544, 53, 423, N'<Row><F1>318</F1><F3>201 Bishopsgate</F3><F4>London</F4><F5>UK</F5><F6>201 Bishopsgate
London, UK</F6></Row>')
,(91545, 53, 424, N'<Row><F1>319</F1><F2>Broadgate Tower</F2><F3>201 Bishopsgate</F3><F4>London</F4><F5>UK</F5><F6>Broadgate Tower
201 Bishopsgate
London, UK</F6></Row>')
,(91546, 53, 425, N'<Row><F1>320</F1><F3>201 California St</F3><F4>San Francisco</F4><F5>CA</F5><F6>201 California St
San Francisco, CA</F6></Row>')
,(91547, 53, 426, N'<Row><F1>321</F1><F2>Sunrise Harley-Davidson</F2><F3>201 International Pkwy</F3><F4>Sunrise</F4><F5>FL</F5><F6>Sunrise Harley-Davidson
201 International Pkwy
Sunrise, FL</F6></Row>')
,(91548, 53, 427, N'<Row><F1>322</F1><F2>Pohanka of Salisbury</F2><F3>2012 N Salisbury Blvd</F3><F4>Salisbury</F4><F5>MD</F5><F6>Pohanka of Salisbury
2012 N Salisbury Blvd
Salisbury, MD</F6></Row>')
,(91549, 53, 428, N'<Row><F1>1196</F1><F3>2020 Challenger Dr</F3><F4>Alameda</F4><F5>CA</F5><F6>2020 Challenger Dr
Alameda, CA</F6></Row>')
,(91550, 53, 429, N'<Row><F1>323</F1><F2>Atlanta Airport Marriott Gateway</F2><F3>2020 Convention Center Concourse</F3><F4>Atlanta</F4><F5>GA</F5><F6>Atlanta Airport Marriott Gateway
2020 Convention Center Concourse
Atlanta, GA</F6></Row>')
,(91551, 53, 430, N'<Row><F1>1195</F1><F3>2021 Challenger Dr</F3><F4>Alameda</F4><F5>CA</F5><F6>2021 Challenger Dr
Alameda, CA</F6></Row>')
,(91552, 53, 431, N'<Row><F1>324</F1><F3>2025 Gateway Pl</F3><F4>San Jose</F4><F5>CA</F5><F6>2025 Gateway Pl
San Jose, CA</F6></Row>')
,(91553, 53, 432, N'<Row><F1>325</F1><F3>2029 Century Park E</F3><F4>Century City</F4><F5>CA</F5><F6>2029 Century Park E
Century City, CA</F6></Row>')
,(91554, 53, 433, N'<Row><F1>326</F1><F2>Family Dollar</F2><F3>203 North Main St</F3><F4>Boiling Springs</F4><F5>NC</F5><F6>Family Dollar
203 North Main St
Boiling Springs, NC</F6></Row>')
,(91555, 53, 434, N'<Row><F1>327</F1><F2>Chuck E. Cheese''s</F2><F3>2032 Catawba Valley Blvd</F3><F4>Hickory</F4><F5>NC</F5><F6>Chuck E. Cheese''s
2032 Catawba Valley Blvd
Hickory, NC</F6></Row>')
,(91556, 53, 435, N'<Row><F1>328</F1><F2>Emich Chevrolet</F2><F3>2033 S Wadsworth Blvd</F3><F4>Lakewood</F4><F5>CO</F5><F6>Emich Chevrolet
2033 S Wadsworth Blvd
Lakewood, CO</F6></Row>')
,(91557, 53, 436, N'<Row><F1>329</F1><F2>Family Dollar</F2><F3>204 11th Ave</F3><F4>Nampa</F4><F5>ID</F5><F6>Family Dollar
204 11th Ave
Nampa, ID</F6></Row>')
,(91558, 53, 437, N'<Row><F1>1040</F1><F3>2040 Main St</F3><F4>Irvine</F4><F5>CA</F5><F6>2040 Main St
Irvine, CA</F6></Row>')
,(91559, 53, 438, N'<Row><F1>330</F1><F2>Family Dollar</F2><F3>2041 American Blvd</F3><F4>Orlando</F4><F5>FL</F5><F6>Family Dollar
2041 American Blvd
Orlando, FL</F6></Row>')
,(91560, 53, 439, N'<Row><F1>331</F1><F2>Fort Bend Toyota/Scion</F2><F3>20465 SW Fwy</F3><F4>Richmond</F4><F5>TX</F5><F6>Fort Bend Toyota/Scion
20465 SW Fwy
Richmond, TX</F6></Row>')
,(91561, 53, 440, N'<Row><F1>332</F1><F3>2049 Century Park E</F3><F4>Century City</F4><F5>CA</F5><F6>2049 Century Park E
Century City, CA</F6></Row>')
,(91562, 53, 441, N'<Row><F1>333</F1><F2>Holiday Inn Express Hauppauge Long Island</F2><F3>2050 Express Dr S</F3><F4>Hauppauge</F4><F5>NY</F5><F6>Holiday Inn Express Hauppauge Long Island
2050 Express Dr S
Hauppauge, NY</F6></Row>')
,(91563, 53, 442, N'<Row><F1>334</F1><F2>Don Bohn Ford</F2><F3>2052 Bonn St</F3><F4>Harvey</F4><F5>LA</F5><F6>Don Bohn Ford
2052 Bonn St
Harvey, LA</F6></Row>')
,(91564, 53, 443, N'<Row><F1>1197</F1><F3>2060 Challenger Dr</F3><F4>Alameda</F4><F5>CA</F5><F6>2060 Challenger Dr
Alameda, CA</F6></Row>')
,(91565, 53, 444, N'<Row><F1>1198</F1><F3>2061 Challenger Dr</F3><F4>Alameda</F4><F5>CA</F5><F6>2061 Challenger Dr
Alameda, CA</F6></Row>')
,(91566, 53, 445, N'<Row><F1>1137</F1><F2>Hard Rock Hotel San Diego</F2><F3>207 5th Ave</F3><F4>San Diego</F4><F5>CA</F5><F6>Hard Rock Hotel San Diego
207 5th Ave
San Diego, CA</F6></Row>')
,(91567, 53, 446, N'<Row><F1>335</F1><F2>Lake Norman Chrysler Jeep Dodge</F2><F3>20700 Torrence Chapel Rd</F3><F4>Cornelius</F4><F5>NC</F5><F6>Lake Norman Chrysler Jeep Dodge
20700 Torrence Chapel Rd
Cornelius, NC</F6></Row>')
,(91568, 53, 447, N'<Row><F1>336</F1><F2>Warren Henry Jaguar</F2><F3>20800 NW 2nd Ave</F3><F4>Miami</F4><F5>FL</F5><F6>Warren Henry Jaguar
20800 NW 2nd Ave
Miami, FL</F6></Row>')
,(91569, 53, 448, N'<Row><F1>337</F1><F2>Warren Henry Infiniti</F2><F3>20850 NW 2nd Ave</F3><F4>Miami</F4><F5>FL</F5><F6>Warren Henry Infiniti
20850 NW 2nd Ave
Miami, FL</F6></Row>')
,(91570, 53, 449, N'<Row><F1>338</F1><F2>Family Dollar</F2><F3>209 E Main St</F3><F4>Ore City</F4><F5>TX</F5><F6>Family Dollar
209 E Main St
Ore City, TX</F6></Row>')
,(91571, 53, 450, N'<Row><F1>339</F1><F2>Springhill Suites Atlanta Airport Gateway</F2><F3>2091 Convention Center Concourse</F3><F4>College Park</F4><F5>GA</F5><F6>Springhill Suites Atlanta Airport Gateway
2091 Convention Center Concourse
College Park, GA</F6></Row>')
,(91572, 53, 451, N'<Row><F1>983</F1><F3>21 Oxford Rd</F3><F4>Mansfield</F4><F5>MA</F5><F6>21 Oxford Rd
Mansfield, MA</F6></Row>')
,(91573, 53, 452, N'<Row><F1>1041</F1><F3>2100 Kalakaua Ave</F3><F4>Honolulu</F4><F5>HI</F5><F6>2100 Kalakaua Ave
Honolulu, HI</F6></Row>')
,(91574, 53, 453, N'<Row><F1>1099</F1><F2>Cameron Village</F2><F3>2108 Clark Ave</F3><F4>Raleigh</F4><F5>NC</F5><F6>Cameron Village
2108 Clark Ave
Raleigh, NC</F6></Row>')
,(91575, 53, 454, N'<Row><F1>340</F1><F3>211 River Oaks Pkwy</F3><F4>San Jose</F4><F5>CA</F5><F6>211 River Oaks Pkwy
San Jose, CA</F6></Row>')
,(91576, 53, 455, N'<Row><F1>341</F1><F2>Lithia Chrysler Dodge of Eugene</F2><F3>2121 Martin Luther King Blvd</F3><F4>Eugene</F4><F5>OR</F5><F6>Lithia Chrysler Dodge of Eugene
2121 Martin Luther King Blvd
Eugene, OR</F6></Row>')
,(91577, 53, 456, N'<Row><F1>342</F1><F2>Crest Cadillac</F2><F3>2121 Metrocenter Blvd</F3><F4>Nashville</F4><F5>TN</F5><F6>Crest Cadillac
2121 Metrocenter Blvd
Nashville, TN</F6></Row>')
,(91578, 53, 457, N'<Row><F1>1208</F1><F2>Royal Ridge Center</F2><F3>213 Daniel Webster Hwy</F3><F4>Nashua</F4><F5>NH</F5><F6>Royal Ridge Center
213 Daniel Webster Hwy
Nashua, NH</F6></Row>')
,(91579, 53, 458, N'<Row><F1>343</F1><F2>Crossroads Plaza</F2><F3>213-405 Crossroads Blvd</F3><F4>Cary</F4><F5>NC</F5><F6>Crossroads Plaza
213-405 Crossroads Blvd
Cary, NC</F6></Row>')
,(91580, 53, 459, N'<Row><F1>344</F1><F2>Family Dollar</F2><F3>214 Kirkland St</F3><F4>Abbeville</F4><F5>AL</F5><F6>Family Dollar
214 Kirkland St
Abbeville, AL</F6></Row>')
,(91581, 53, 460, N'<Row><F1>345</F1><F3>21410 N 15th Ln</F3><F4>Phoenix</F4><F5>AZ</F5><F6>21410 N 15th Ln
Phoenix, AZ</F6></Row>')
,(91582, 53, 461, N'<Row><F1>346</F1><F3>21415 N 15th Ln</F3><F4>Phoenix</F4><F5>AZ</F5><F6>21415 N 15th Ln
Phoenix, AZ</F6></Row>')
,(91583, 53, 462, N'<Row><F1>347</F1><F2>Clear Lake Nissan                                           </F2><F3>2150 Gulf Fwy                  </F3><F4>League City                   </F4><F5>TX </F5><F6>Clear Lake Nissan                                           
2150 Gulf Fwy                  
League City                   , TX </F6></Row>')
,(91584, 53, 463, N'<Row><F1>348</F1><F2>Family Dollar</F2><F3>21669 Whyte Hardee Blvd</F3><F4>Hardeeville</F4><F5>SC</F5><F6>Family Dollar
21669 Whyte Hardee Blvd
Hardeeville, SC</F6></Row>')
,(91585, 53, 464, N'<Row><F1>349</F1><F2>Family Dollar</F2><F3>2170 Bailey Rd</F3><F4>Mulberry</F4><F5>FL</F5><F6>Family Dollar
2170 Bailey Rd
Mulberry, FL</F6></Row>')
,(91586, 53, 465, N'<Row><F1>350</F1><F2>Family Dollar</F2><F3>2178 Campbellton Rd SW</F3><F4>Atlanta</F4><F5>GA</F5><F6>Family Dollar
2178 Campbellton Rd SW
Atlanta, GA</F6></Row>')
,(91587, 53, 466, N'<Row><F1>351</F1><F2>Warner Center Marriott Woodland Hills</F2><F3>21850 Oxnard St</F3><F4>Woodland Hills</F4><F5>CA</F5><F6>Warner Center Marriott Woodland Hills
21850 Oxnard St
Woodland Hills, CA</F6></Row>')
,(91588, 53, 467, N'<Row><F1>352</F1><F3>219-223 W 77th St</F3><F4>New York</F4><F5>NY</F5><F6>219-223 W 77th St
New York, NY</F6></Row>')
,(91589, 53, 468, N'<Row><F1>353</F1><F2>Santa Susana Plaza</F2><F3>2196 Tapo St</F3><F4>Simi Valley</F4><F5>CA</F5><F6>Santa Susana Plaza
2196 Tapo St
Simi Valley, CA</F6></Row>')
,(91590, 53, 469, N'<Row><F1>354</F1><F2>Mike Smith Autoplex</F2><F3>2197 Interstate 10 S</F3><F4>Beaumont</F4><F5>TX</F5><F6>Mike Smith Autoplex
2197 Interstate 10 S
Beaumont, TX</F6></Row>')
,(91591, 53, 470, N'<Row><F1>355</F1><F3>22 4th St</F3><F4>San Francisco</F4><F5>CA</F5><F6>22 4th St
San Francisco, CA</F6></Row>')
,(91592, 53, 471, N'<Row><F1>356</F1><F3>22 Crosby Dr</F3><F4>Bedford</F4><F5>MA</F5><F6>22 Crosby Dr
Bedford, MA</F6></Row>')
,(91593, 53, 472, N'<Row><F1>357</F1><F2>Radisson Blu Philadelphia</F2><F3>220 S 17th St</F3><F4>Philadelphia</F4><F5>PA</F5><F6>Radisson Blu Philadelphia
220 S 17th St
Philadelphia, PA</F6></Row>')
,(91594, 53, 473, N'<Row><F1>358</F1><F2>Gurley-Leep Honda of Elkhart</F2><F3>2200 Bypass Rd</F3><F4>Elkhart</F4><F5>IN</F5><F6>Gurley-Leep Honda of Elkhart
2200 Bypass Rd
Elkhart, IN</F6></Row>')
,(91595, 53, 474, N'<Row><F1>359</F1><F2>Royal Hawaiian Center</F2><F3>2201 Kalakaua Ave</F3><F4>Honolulu</F4><F5>HI</F5><F6>Royal Hawaiian Center
2201 Kalakaua Ave
Honolulu, HI</F6></Row>')
,(91596, 53, 475, N'<Row><F1>360</F1><F2>Wolfchase Toyota/Scion</F2><F3>2201 N Germantown Pkwy</F3><F4>Memphis</F4><F5>TN</F5><F6>Wolfchase Toyota/Scion
2201 N Germantown Pkwy
Memphis, TN</F6></Row>')
,(91597, 53, 476, N'<Row><F1>361</F1><F2>Family Dollar</F2><F3>2204 Maple Ave</F3><F4>Burlington</F4><F5>NC</F5><F6>Family Dollar
2204 Maple Ave
Burlington, NC</F6></Row>')
,(91598, 53, 477, N'<Row><F1>362</F1><F3>221 E Town Line Rd</F3><F4>Vernon Hills</F4><F5>IL</F5><F6>221 E Town Line Rd
Vernon Hills, IL</F6></Row>')
,(91599, 53, 478, N'<Row><F1>363</F1><F3>221 Main St</F3><F4>San Francisco</F4><F5>CA</F5><F6>221 Main St
San Francisco, CA</F6></Row>')
,(91600, 53, 479, N'<Row><F1>364</F1><F2>World Trade Center East</F2><F3>2211 Elloitt Ave</F3><F4>Seattle</F4><F5>WA</F5><F6>World Trade Center East
2211 Elloitt Ave
Seattle, WA</F6></Row>')
,(91601, 53, 480, N'<Row><F1>365</F1><F2>Crest Honda World</F2><F3>2215 Metrocenter Blvd</F3><F4>Nashville</F4><F5>TN</F5><F6>Crest Honda World
2215 Metrocenter Blvd
Nashville, TN</F6></Row>')
,(91602, 53, 481, N'<Row><F1>366</F1><F2>Chuck E. Cheese''s</F2><F3>2215 SW Wanamaker Rd</F3><F4>Topeka</F4><F5>KS</F5><F6>Chuck E. Cheese''s
2215 SW Wanamaker Rd
Topeka, KS</F6></Row>')
,(91603, 53, 482, N'<Row><F1>367</F1><F3>225 Franklin St</F3><F4>Boston</F4><F5>MA</F5><F6>225 Franklin St
Boston, MA</F6></Row>')
,(91604, 53, 483, N'<Row><F1>368</F1><F2>Greg May Chevrolet</F2><F3>225 T M West Pkwy</F3><F4>West</F4><F5>TX</F5><F6>Greg May Chevrolet
225 T M West Pkwy
West, TX</F6></Row>')
,(91605, 53, 484, N'<Row><F1>1148</F1><F2>Sheraton Waikiki</F2><F3>2255 Kalakaua Ave</F3><F4>Honolulu</F4><F5>HI</F5><F6>Sheraton Waikiki
2255 Kalakaua Ave
Honolulu, HI</F6></Row>')
,(91606, 53, 485, N'<Row><F1>369</F1><F2>Sterling McCall Honda</F2><F3>22575 Hwy 59 N</F3><F4>Kingwood</F4><F5>TX</F5><F6>Sterling McCall Honda
22575 Hwy 59 N
Kingwood, TX</F6></Row>')
,(91607, 53, 486, N'<Row><F1>1149</F1><F2>The Royal Hawaiian</F2><F3>2259 Kalakaua Ave</F3><F4>Honolulu</F4><F5>HI</F5><F6>The Royal Hawaiian
2259 Kalakaua Ave
Honolulu, HI</F6></Row>')
,(91608, 53, 487, N'<Row><F1>1202</F1><F2>Hotel Novotel New York Times Square</F2><F3>226 W 52nd St</F3><F4>New York</F4><F5>NY</F5><F6>Hotel Novotel New York Times Square
226 W 52nd St
New York, NY</F6></Row>')
,(91609, 53, 488, N'<Row><F1>370</F1><F2>Berlin City Nissan of Portland</F2><F3>227 Maine Mall Rd</F3><F4>South Portland</F4><F5>ME</F5><F6>Berlin City Nissan of Portland
227 Maine Mall Rd
South Portland, ME</F6></Row>')
,(91610, 53, 489, N'<Row><F1>371</F1><F2>Waldorf Honda</F2><F3>2294 Crain Hwy</F3><F4>Waldorf</F4><F5>MD</F5><F6>Waldorf Honda
2294 Crain Hwy
Waldorf, MD</F6></Row>')
,(91611, 53, 490, N'<Row><F1>372</F1><F2>Waldorf Chevrolet Cadillac</F2><F3>2298 Crain Hwy</F3><F4>Waldorf</F4><F5>MD</F5><F6>Waldorf Chevrolet Cadillac
2298 Crain Hwy
Waldorf, MD</F6></Row>')
,(91612, 53, 491, N'<Row><F1>1207</F1><F2>Medfield Shops</F2><F3>230 Main St</F3><F4>Medfield</F4><F5>MA</F5><F6>Medfield Shops
230 Main St
Medfield, MA</F6></Row>')
,(91613, 53, 492, N'<Row><F1>373</F1><F2>Clearwater Collision Center</F2><F3>2300 Drew St</F3><F4>Clearwater</F4><F5>FL</F5><F6>Clearwater Collision Center
2300 Drew St
Clearwater, FL</F6></Row>')
,(91614, 53, 493, N'<Row><F1>374</F1><F2>Orr Hyundai</F2><F3>2300 St. Michael Dr</F3><F4>Texarkana</F4><F5>TX</F5><F6>Orr Hyundai
2300 St. Michael Dr
Texarkana, TX</F6></Row>')
,(91615, 53, 494, N'<Row><F1>375</F1><F2>Chuck E. Cheese''s</F2><F3>2303 E Central Texas Expy</F3><F4>Killeen</F4><F5>TX</F5><F6>Chuck E. Cheese''s
2303 E Central Texas Expy
Killeen, TX</F6></Row>')
,(91616, 53, 495, N'<Row><F1>376</F1><F2>Chuck E. Cheese''s</F2><F3>2303 Town Center Dr</F3><F4>Sugarland</F4><F5>TX</F5><F6>Chuck E. Cheese''s
2303 Town Center Dr
Sugarland, TX</F6></Row>')
,(91617, 53, 496, N'<Row><F1>377</F1><F3>2306 E Lincoln Hwy</F3><F4>New Lenox</F4><F5>IL</F5><F6>2306 E Lincoln Hwy
New Lenox, IL</F6></Row>')
,(91618, 53, 497, N'<Row><F1>305</F1><F2>Hamburg Pavilion</F2><F3>2308 Sir Barton Way</F3><F4>Lexington</F4><F5>KY</F5><F6>Hamburg Pavilion
2308 Sir Barton Way
Lexington, KY</F6></Row>')
,(91619, 53, 498, N'<Row><F1>378</F1><F2>Former Momentum Audi</F2><F3>2315 Richmond Ave</F3><F4>Houston</F4><F5>TX</F5><F6>Former Momentum Audi
2315 Richmond Ave
Houston, TX</F6></Row>')
,(91620, 53, 499, N'<Row><F1>1089</F1><F3>2320 Marinship Way</F3><F4>Sausalito</F4><F5>CA</F5><F6>2320 Marinship Way
Sausalito, CA</F6></Row>')
,(91621, 53, 500, N'<Row><F1>379</F1><F2>The Bellevue</F2><F3>2323 S Shepherd Dr</F3><F4>Houston</F4><F5>TX</F5><F6>The Bellevue
2323 S Shepherd Dr
Houston, TX</F6></Row>')
,(91622, 53, 501, N'<Row><F1>1090</F1><F3>2330 Marinship Way</F3><F4>Sausalito</F4><F5>CA</F5><F6>2330 Marinship Way
Sausalito, CA</F6></Row>')
,(91623, 53, 502, N'<Row><F1>380</F1><F3>234 E 46th St</F3><F4>New York</F4><F5>NY</F5><F6>234 E 46th St
New York, NY</F6></Row>')
,(91624, 53, 503, N'<Row><F1>992</F1><F2>Grand Twin Towers West</F2><F3>234 Goodwin Crest Dr</F3><F4>Homewood</F4><F5>AL</F5><F6>Grand Twin Towers West
234 Goodwin Crest Dr
Homewood, AL</F6></Row>')
,(91625, 53, 504, N'<Row><F1>381</F1><F2>Family Dollar</F2><F3>2342 Barksdale Blvd</F3><F4>Bossier City</F4><F5>LA</F5><F6>Family Dollar
2342 Barksdale Blvd
Bossier City, LA</F6></Row>')
,(91626, 53, 505, N'<Row><F1>382</F1><F2>Family Dollar</F2><F3>2348 N Fayetteville St</F3><F4>Asheboro</F4><F5>NC</F5><F6>Family Dollar
2348 N Fayetteville St
Asheboro, NC</F6></Row>')
,(91627, 53, 506, N'<Row><F1>383</F1><F3>235 E North Ave</F3><F4>Glendale Heights</F4><F5>IL</F5><F6>235 E North Ave
Glendale Heights, IL</F6></Row>')
,(91628, 53, 507, N'<Row><F1>384</F1><F3>235 Pine St</F3><F4>San Francisco</F4><F5>CA</F5><F6>235 Pine St
San Francisco, CA</F6></Row>')
,(91629, 53, 508, N'<Row><F1>993</F1><F2>Grand Twin Towers East</F2><F3>236 Goodwin Crest Dr</F3><F4>Homewood</F4><F5>AL</F5><F6>Grand Twin Towers East
236 Goodwin Crest Dr
Homewood, AL</F6></Row>')
,(91630, 53, 509, N'<Row><F1>1150</F1><F2>Westin Moana Surfrider</F2><F3>2365 Kalakaua Ave</F3><F4>Honolulu</F4><F5>HI</F5><F6>Westin Moana Surfrider
2365 Kalakaua Ave
Honolulu, HI</F6></Row>')
,(91631, 53, 510, N'<Row><F1>1139</F1><F2>Vancouver Quarter Shopping Centre</F2><F3>24 Broad St</F3><F4>King''s Lynn PE30 1DP</F4><F5>UK</F5><F6>Vancouver Quarter Shopping Centre
24 Broad St
King''s Lynn PE30 1DP, UK</F6></Row>')
,(91632, 53, 511, N'<Row><F1>385</F1><F3>24 Crosby Dr</F3><F4>Bedford</F4><F5>MA</F5><F6>24 Crosby Dr
Bedford, MA</F6></Row>')
,(91633, 53, 512, N'<Row><F1>1169</F1><F3>2425 Whipple Rd</F3><F4>Hayward</F4><F5>CA</F5><F6>2425 Whipple Rd
Hayward, CA</F6></Row>')
,(91634, 53, 513, N'<Row><F1>386</F1><F2>Former Massey Cadillac Body Shop</F2><F3>24440 Telegraph Rd</F3><F4>Southfield</F4><F5>MI</F5><F6>Former Massey Cadillac Body Shop
24440 Telegraph Rd
Southfield, MI</F6></Row>')
,(91635, 53, 514, N'<Row><F1>387</F1><F2>Family Dollar</F2><F3>2445 Al Hwy 202</F3><F4>Anniston</F4><F5>AL</F5><F6>Family Dollar
2445 Al Hwy 202
Anniston, AL</F6></Row>')
,(91636, 53, 515, N'<Row><F1>388</F1><F3>245 S Los Robles</F3><F4>Pasadena</F4><F5>CA</F5><F6>245 S Los Robles
Pasadena, CA</F6></Row>')
,(91637, 53, 516, N'<Row><F1>389</F1><F3>2450 Walsh Ave</F3><F4>Santa Clara</F4><F5>CA</F5><F6>2450 Walsh Ave
Santa Clara, CA</F6></Row>')
,(91638, 53, 517, N'<Row><F1>390</F1><F3>245-249 W 17th St</F3><F4>New York</F4><F5>NY</F5><F6>245-249 W 17th St
New York, NY</F6></Row>')
,(91639, 53, 518, N'<Row><F1>391</F1><F2>Former Massey Cadillac</F2><F3>24600 Grand River Ave</F3><F4>Detroit</F4><F5>MI</F5><F6>Former Massey Cadillac
24600 Grand River Ave
Detroit, MI</F6></Row>')
,(91640, 53, 519, N'<Row><F1>392</F1><F2>Acura 101 West</F2><F3>24650 Calabasas Rd</F3><F4>Calabasas</F4><F5>CA</F5><F6>Acura 101 West
24650 Calabasas Rd
Calabasas, CA</F6></Row>')
,(91641, 53, 520, N'<Row><F1>393</F1><F2>Family Dollar</F2><F3>2467 Kershaw Camden Hwy</F3><F4>Lancaster (Elgin)</F4><F5>SC</F5><F6>Family Dollar
2467 Kershaw Camden Hwy
Lancaster (Elgin), SC</F6></Row>')
,(91642, 53, 521, N'<Row><F1>394</F1><F2>Woolgate Exchange</F2><F3>25 Basinghall Ave</F3><F4>London</F4><F5>UK</F5><F6>Woolgate Exchange
25 Basinghall Ave
London, UK</F6></Row>')
,(91643, 53, 522, N'<Row><F1>395</F1><F2>Family Dollar</F2><F3>250 S Main St</F3><F4>Woodruff</F4><F5>SC</F5><F6>Family Dollar
250 S Main St
Woodruff, SC</F6></Row>')
,(91644, 53, 523, N'<Row><F1>396</F1><F2>Don Chalmers Ford</F2><F3>2500 Rio Rancho Blvd</F3><F4>Rio Rancho</F4><F5>NM</F5><F6>Don Chalmers Ford
2500 Rio Rancho Blvd
Rio Rancho, NM</F6></Row>')
,(91645, 53, 524, N'<Row><F1>397</F1><F3>2500 Walsh Ave</F3><F4>Santa Clara</F4><F5>CA</F5><F6>2500 Walsh Ave
Santa Clara, CA</F6></Row>')
,(91646, 53, 525, N'<Row><F1>398</F1><F3>2500 Westchester Ave</F3><F4>Harrison</F4><F5>NY</F5><F6>2500 Westchester Ave
Harrison, NY</F6></Row>')
,(91647, 53, 526, N'<Row><F1>399</F1><F3>251 River Oaks Pkwy</F3><F4>San Jose</F4><F5>CA</F5><F6>251 River Oaks Pkwy
San Jose, CA</F6></Row>')
,(91648, 53, 527, N'<Row><F1>400</F1><F2>Family Dollar</F2><F3>2515 Stone Station Rd</F3><F4>Roebuck</F4><F5>SC</F5><F6>Family Dollar
2515 Stone Station Rd
Roebuck, SC</F6></Row>')
,(91649, 53, 528, N'<Row><F1>401</F1><F2>Chuck E. Cheese''s</F2><F3>253 Congaree Rd</F3><F4>Greenville</F4><F5>SC</F5><F6>Chuck E. Cheese''s
253 Congaree Rd
Greenville, SC</F6></Row>')
,(91650, 53, 529, N'<Row><F1>402</F1><F3>254 Second Ave</F3><F4>Needham</F4><F5>MA</F5><F6>254 Second Ave
Needham, MA</F6></Row>')
,(91651, 53, 530, N'<Row><F1>403</F1><F2>Family Dollar</F2><F3>2548 N 32nd St</F3><F4>Phoenix</F4><F5>AZ</F5><F6>Family Dollar
2548 N 32nd St
Phoenix, AZ</F6></Row>')
,(91652, 53, 531, N'<Row><F1>404</F1><F3>255 Bear Hill Rd</F3><F4>Waltham</F4><F5>MA</F5><F6>255 Bear Hill Rd
Waltham, MA</F6></Row>')
,(91653, 53, 532, N'<Row><F1>405</F1><F2>Berlin City Honda of Portland</F2><F3>255 Maine Mall Rd</F3><F4>South Portland</F4><F5>ME</F5><F6>Berlin City Honda of Portland
255 Maine Mall Rd
South Portland, ME</F6></Row>')
,(91654, 53, 533, N'<Row><F1>406</F1><F2>Penske Corp./PAG Headquarters</F2><F3>2555 Telegraph Rd</F3><F4>Bloomfield Hills</F4><F5>MI</F5><F6>Penske Corp./PAG Headquarters
2555 Telegraph Rd
Bloomfield Hills, MI</F6></Row>')
,(91655, 53, 534, N'<Row><F1>407</F1><F3>2570 Kalakaua Ave</F3><F4>Honolulu</F4><F5>HI</F5><F6>2570 Kalakaua Ave
Honolulu, HI</F6></Row>')
,(91656, 53, 535, N'<Row><F1>408</F1><F2>Porsche, VW, &amp; Audi of Ann Arbor</F2><F3>2575 S State St</F3><F4>Ann Arbor</F4><F5>MI</F5><F6>Porsche, VW, &amp; Audi of Ann Arbor
2575 S State St
Ann Arbor, MI</F6></Row>')
,(91657, 53, 536, N'<Row><F1>409</F1><F3>26 4th St</F3><F4>San Francisco</F4><F5>CA</F5><F6>26 4th St
San Francisco, CA</F6></Row>')
,(91658, 53, 537, N'<Row><F1>410</F1><F3>26 Crosby Dr</F3><F4>Bedford</F4><F5>MA</F5><F6>26 Crosby Dr
Bedford, MA</F6></Row>')
,(91659, 53, 538, N'<Row><F1>411</F1><F2>JSI Collision Center - CF (JSI #34)</F2><F3>26 E Steels Corners Rd</F3><F4>Cuyahoga Falls</F4><F5>OH</F5><F6>JSI Collision Center - CF (JSI #34)
26 E Steels Corners Rd
Cuyahoga Falls, OH</F6></Row>')
,(91660, 53, 539, N'<Row><F1>412</F1><F3>260 Littlefield Ave</F3><F4>San Francisco</F4><F5>CA</F5><F6>260 Littlefield Ave
San Francisco, CA</F6></Row>')
,(91661, 53, 540, N'<Row><F1>413</F1><F2>Holiday Inn Express &amp; Suites King of Prussia</F2><F3>260 N Gulph Rd</F3><F4>King of Prussia</F4><F5>PA</F5><F6>Holiday Inn Express &amp; Suites King of Prussia
260 N Gulph Rd
King of Prussia, PA</F6></Row>')
,(91662, 53, 541, N'<Row><F1>414</F1><F2>Nalley Collision Center</F2><F3>2600 Chestnut Dr</F3><F4>Doraville</F4><F5>GA</F5><F6>Nalley Collision Center
2600 Chestnut Dr
Doraville, GA</F6></Row>')
,(91663, 53, 542, N'<Row><F1>415</F1><F2>Germain Honda of Ann Arbor</F2><F3>2601 S State St</F3><F4>Ann Arbor</F4><F5>MI</F5><F6>Germain Honda of Ann Arbor
2601 S State St
Ann Arbor, MI</F6></Row>')
,(91664, 53, 543, N'<Row><F1>1151</F1><F2>Sheraton Maui Resort</F2><F3>2605 Kaanapali Pkwy</F3><F4>Kaanapali</F4><F5>HI</F5><F6>Sheraton Maui Resort
2605 Kaanapali Pkwy
Kaanapali, HI</F6></Row>')
,(91665, 53, 544, N'<Row><F1>416</F1><F3>2650 N MacArthur Dr</F3><F4>Tracy</F4><F5>CA</F5><F6>2650 N MacArthur Dr
Tracy, CA</F6></Row>')
,(91666, 53, 545, N'<Row><F1>417</F1><F2>Chuck E. Cheese''s</F2><F3>26562 Towne Centre Dr</F3><F4>Foothill Ranch</F4><F5>CA</F5><F6>Chuck E. Cheese''s
26562 Towne Centre Dr
Foothill Ranch, CA</F6></Row>')
,(91667, 53, 546, N'<Row><F1>1011</F1><F2>Lipscomb &amp; Pitts Building</F2><F3>2670 Union Avenue Extended</F3><F4>Memphis</F4><F5>TN</F5><F6>Lipscomb &amp; Pitts Building
2670 Union Avenue Extended
Memphis, TN</F6></Row>')
,(91668, 53, 547, N'<Row><F1>418</F1><F2>Family Dollar</F2><F3>2681 Ne 10th</F3><F4>Oklahoma City</F4><F5>OK</F5><F6>Family Dollar
2681 Ne 10th
Oklahoma City, OK</F6></Row>')
,(91669, 53, 548, N'<Row><F1>419</F1><F2>Chuck E. Cheese''s</F2><F3>2699 Lishelle Place</F3><F4>Virginia Beach</F4><F5>VA</F5><F6>Chuck E. Cheese''s
2699 Lishelle Place
Virginia Beach, VA</F6></Row>')
,(91670, 53, 549, N'<Row><F1>420</F1><F3>270 Littlefield Ave</F3><F4>San Francisco</F4><F5>CA</F5><F6>270 Littlefield Ave
San Francisco, CA</F6></Row>')
,(91671, 53, 550, N'<Row><F1>421</F1><F3>2700 Westchester Ave</F3><F4>Harrison</F4><F5>NY</F5><F6>2700 Westchester Ave
Harrison, NY</F6></Row>')
,(91672, 53, 551, N'<Row><F1>1012</F1><F2>Donelson Plaza I</F2><F3>2710 Old Lebanon Rd</F3><F4>Nashville</F4><F5>TN</F5><F6>Donelson Plaza I
2710 Old Lebanon Rd
Nashville, TN</F6></Row>')
,(91673, 53, 552, N'<Row><F1>1013</F1><F2>Donelson Plaza II</F2><F3>2720 Old Lebanon Rd</F3><F4>Nashville</F4><F5>TN</F5><F6>Donelson Plaza II
2720 Old Lebanon Rd
Nashville, TN</F6></Row>')
,(91674, 53, 553, N'<Row><F1>1240</F1><F2>Queen Ka''ahumanu Center</F2><F3>275 W Kaahumanu Ave</F3><F4>Kahului</F4><F5>HI</F5><F6>Queen Ka''ahumanu Center
275 W Kaahumanu Ave
Kahului, HI</F6></Row>')
,(91675, 53, 554, N'<Row><F1>422</F1><F2>Nalley Lexus</F2><F3>2750 Cobb Pkwy</F3><F4>Smyrna</F4><F5>GA</F5><F6>Nalley Lexus
2750 Cobb Pkwy
Smyrna, GA</F6></Row>')
,(91676, 53, 555, N'<Row><F1>423</F1><F2>Former Century BMW</F2><F3>2750 Laurens Rd</F3><F4>Greenville</F4><F5>SC</F5><F6>Former Century BMW
2750 Laurens Rd
Greenville, SC</F6></Row>')
,(91677, 53, 556, N'<Row><F1>1124</F1><F3>2755 E Cottonwood Pkwy</F3><F4>Salt Lake City</F4><F5>UT</F5><F6>2755 E Cottonwood Pkwy
Salt Lake City, UT</F6></Row>')
,(91678, 53, 557, N'<Row><F1>424</F1><F2>Chuck E. Cheese''s</F2><F3>2755 E Grapevine Mills Cir</F3><F4>Grapevine</F4><F5>TX</F5><F6>Chuck E. Cheese''s
2755 E Grapevine Mills Cir
Grapevine, TX</F6></Row>')
,(91679, 53, 558, N'<Row><F1>425</F1><F2>Chuck E. Cheese''s</F2><F3>2760 I-20 Frontage Rd</F3><F4>Grand Prairie</F4><F5>TX</F5><F6>Chuck E. Cheese''s
2760 I-20 Frontage Rd
Grand Prairie, TX</F6></Row>')
,(91680, 53, 559, N'<Row><F1>1125</F1><F3>2795 E Cottonwood Pkwy</F3><F4>Salt Lake City</F4><F5>UT</F5><F6>2795 E Cottonwood Pkwy
Salt Lake City, UT</F6></Row>')
,(91681, 53, 560, N'<Row><F1>426</F1><F3>28 Crosby Dr</F3><F4>Bedford</F4><F5>MA</F5><F6>28 Crosby Dr
Bedford, MA</F6></Row>')
,(91682, 53, 561, N'<Row><F1>427</F1><F3>28 State St</F3><F4>Boston</F4><F5>MA</F5><F6>28 State St
Boston, MA</F6></Row>')
,(91683, 53, 562, N'<Row><F1>1279</F1><F2>Gaslight Commons</F2><F3>28 W 3rd St</F3><F4>South Orange</F4><F5>NJ</F5><F6>Gaslight Commons
28 W 3rd St
South Orange, NJ</F6></Row>')
,(91684, 53, 563, N'<Row><F1>1057</F1><F3>280 Interstate N Cir SE</F3><F4>Atlanta</F4><F5>GA</F5><F6>280 Interstate N Cir SE
Atlanta, GA</F6></Row>')
,(91685, 53, 564, N'<Row><F1>428</F1><F3>280 Utah Ave</F3><F4>San Francisco</F4><F5>CA</F5><F6>280 Utah Ave
San Francisco, CA</F6></Row>')
,(91686, 53, 565, N'<Row><F1>429</F1><F3>281 River Oaks Pkwy</F3><F4>San Jose</F4><F5>CA</F5><F6>281 River Oaks Pkwy
San Jose, CA</F6></Row>')
,(91687, 53, 566, N'<Row><F1>430</F1><F3>2810 US-34</F3><F4>Oswego</F4><F5>IL</F5><F6>2810 US-34
Oswego, IL</F6></Row>')
,(91688, 53, 567, N'<Row><F1>1123</F1><F2>Hyatt Place San Jose/Downtown</F2><F3>282 S Almaden Blvd</F3><F4>San Jose</F4><F5>CA</F5><F6>Hyatt Place San Jose/Downtown
282 S Almaden Blvd
San Jose, CA</F6></Row>')
,(91689, 53, 568, N'<Row><F1>431</F1><F2>Capitol Hyundai</F2><F3>2820 Eastern Blvd</F3><F4>Montgomery</F4><F5>AL</F5><F6>Capitol Hyundai
2820 Eastern Blvd
Montgomery, AL</F6></Row>')
,(91690, 53, 569, N'<Row><F1>1126</F1><F3>2825 E Cottonwood Pkwy</F3><F4>Salt Lake City</F4><F5>UT</F5><F6>2825 E Cottonwood Pkwy
Salt Lake City, UT</F6></Row>')
,(91691, 53, 570, N'<Row><F1>432</F1><F2>Family Dollar</F2><F3>2826 W Silver Springs Blvd</F3><F4>Ocala</F4><F5>FL</F5><F6>Family Dollar
2826 W Silver Springs Blvd
Ocala, FL</F6></Row>')
,(91692, 53, 571, N'<Row><F1>433</F1><F2>Mercedes-Benz of North Olmsted</F2><F3>28450 Lorain Rd</F3><F4>North Olmsted</F4><F5>OH</F5><F6>Mercedes-Benz of North Olmsted
28450 Lorain Rd
North Olmsted, OH</F6></Row>')
,(91693, 53, 572, N'<Row><F1>1127</F1><F3>2855 E Cottonwood Pkwy</F3><F4>Salt Lake City</F4><F5>UT</F5><F6>2855 E Cottonwood Pkwy
Salt Lake City, UT</F6></Row>')
,(91694, 53, 573, N'<Row><F1>434</F1><F2>Mercedes-Benz CPO of North Olmsted</F2><F3>28595 Lorain Rd</F3><F4>North Olmsted</F4><F5>OH</F5><F6>Mercedes-Benz CPO of North Olmsted
28595 Lorain Rd
North Olmsted, OH</F6></Row>')
,(91695, 53, 574, N'<Row><F1>435</F1><F3>2880 Lakeside Dr</F3><F4>Santa Clara</F4><F5>CA</F5><F6>2880 Lakeside Dr
Santa Clara, CA</F6></Row>')
,(91696, 53, 575, N'<Row><F1>1160</F1><F3>2899 Mead Ave</F3><F4>Santa Clara</F4><F5>CA</F5><F6>2899 Mead Ave
Santa Clara, CA</F6></Row>')
,(91697, 53, 576, N'<Row><F1>436</F1><F2>Family Dollar</F2><F3>29 Colonel Hollow Rd</F3><F4>Rockholds</F4><F5>KY</F5><F6>Family Dollar
29 Colonel Hollow Rd
Rockholds, KY</F6></Row>')
,(91698, 53, 577, N'<Row><F1>1146</F1><F2>Martha Washington Hotel</F2><F3>29 E 29th St</F3><F4>New York</F4><F5>NY</F5><F6>Martha Washington Hotel
29 E 29th St
New York, NY</F6></Row>')
,(91699, 53, 578, N'<Row><F1>437</F1><F2>Atlantic Wharf</F2><F3>290 Congress St</F3><F4>Boston</F4><F5>MA</F5><F6>Atlantic Wharf
290 Congress St
Boston, MA</F6></Row>')
,(91700, 53, 579, N'<Row><F1>977</F1><F3>290 Forbes Blvd</F3><F4>Mansfield</F4><F5>MA</F5><F6>290 Forbes Blvd
Mansfield, MA</F6></Row>')
,(91701, 53, 580, N'<Row><F1>438</F1><F3>290 Utah Ave</F3><F4>San Francisco</F4><F5>CA</F5><F6>290 Utah Ave
San Francisco, CA</F6></Row>')
,(91702, 53, 581, N'<Row><F1>1004</F1><F2>Highlands Plaza</F2><F3>2900 Lakeland Highlands Rd</F3><F4>Lakeland</F4><F5>FL</F5><F6>Highlands Plaza
2900 Lakeland Highlands Rd
Lakeland, FL</F6></Row>')
,(91703, 53, 582, N'<Row><F1>439</F1><F3>2911-2951 Zanker Rd</F3><F4>San Jose</F4><F5>CA</F5><F6>2911-2951 Zanker Rd
San Jose, CA</F6></Row>')
,(91704, 53, 583, N'<Row><F1>440</F1><F2>Coggin Honda of St. Augustine</F2><F3>2925 US Highway 1 S</F3><F4>St. Augustine</F4><F5>FL</F5><F6>Coggin Honda of St. Augustine
2925 US Highway 1 S
St. Augustine, FL</F6></Row>')
,(91705, 53, 584, N'<Row><F1>441</F1><F2>Family Dollar</F2><F3>2932 Highway 90 W</F3><F4>Avondale</F4><F5>LA</F5><F6>Family Dollar
2932 Highway 90 W
Avondale, LA</F6></Row>')
,(91706, 53, 585, N'<Row><F1>442</F1><F2>Chuck E. Cheese''s</F2><F3>2935 Southwest Pkwy</F3><F4>Wichita Falls</F4><F5>TX</F5><F6>Chuck E. Cheese''s
2935 Southwest Pkwy
Wichita Falls, TX</F6></Row>')
,(91707, 53, 586, N'<Row><F1>443</F1><F3>2940-2960 N First St</F3><F4>San Jose</F4><F5>CA</F5><F6>2940-2960 N First St
San Jose, CA</F6></Row>')
,(91708, 53, 587, N'<Row><F1>1042</F1><F3>2941 Fairview Park Dr</F3><F4>Falls Church</F4><F5>VA</F5><F6>2941 Fairview Park Dr
Falls Church, VA</F6></Row>')
,(91709, 53, 588, N'<Row><F1>444</F1><F3>2955 Red Hill Ave</F3><F4>Costa Mesa</F4><F5>CA</F5><F6>2955 Red Hill Ave
Costa Mesa, CA</F6></Row>')
,(91710, 53, 589, N'<Row><F1>445</F1><F3>2975 Red Hill Ave</F3><F4>Costa Mesa</F4><F5>CA</F5><F6>2975 Red Hill Ave
Costa Mesa, CA</F6></Row>')
,(91711, 53, 590, N'<Row><F1>446</F1><F2>Chuck E. Cheese''s</F2><F3>2990 Cumberland Blvd</F3><F4>Atlanta</F4><F5>GA</F5><F6>Chuck E. Cheese''s
2990 Cumberland Blvd
Atlanta, GA</F6></Row>')
,(91712, 53, 591, N'<Row><F1>447</F1><F3>2995 Red Hill Ave</F3><F4>Costa Mesa</F4><F5>CA</F5><F6>2995 Red Hill Ave
Costa Mesa, CA</F6></Row>')
,(91713, 53, 592, N'<Row><F1>448</F1><F2>RSM Honda</F2><F3>29961 Santa Margarita Pkwy</F3><F4>Rancho Santa Marg</F4><F5>CA</F5><F6>RSM Honda
29961 Santa Margarita Pkwy
Rancho Santa Marg, CA</F6></Row>')
,(91714, 53, 593, N'<Row><F1>449</F1><F3>3 Finsbury Ave</F3><F4>London</F4><F5>UK</F5><F6>3 Finsbury Ave
London, UK</F6></Row>')
,(91715, 53, 594, N'<Row><F1>1277</F1><F2>The Amalfi at Hermann Park</F2><F3>3 Hermann Museum Circle Dr</F3><F4>Houston</F4><F5>TX</F5><F6>The Amalfi at Hermann Park
3 Hermann Museum Circle Dr
Houston, TX</F6></Row>')
,(91716, 53, 595, N'<Row><F1>1278</F1><F2>The Esplanade</F2><F3>3 Hermann Museum Circle Dr</F3><F4>Houston</F4><F5>TX</F5><F6>The Esplanade
3 Hermann Museum Circle Dr
Houston, TX</F6></Row>')
,(91717, 53, 596, N'<Row><F1>450</F1><F3>30 Crosby Dr</F3><F4>Bedford</F4><F5>MA</F5><F6>30 Crosby Dr
Bedford, MA</F6></Row>')
,(91718, 53, 597, N'<Row><F1>451</F1><F2>Chuck E. Cheese''s</F2><F3>30 Prestige Pl</F3><F4>Miamisburg</F4><F5>OH</F5><F6>Chuck E. Cheese''s
30 Prestige Pl
Miamisburg, OH</F6></Row>')
,(91719, 53, 598, N'<Row><F1>1247</F1><F3>30 Rio Robles</F3><F4>San Jose</F4><F5>CA</F5><F6>30 Rio Robles
San Jose, CA</F6></Row>')
,(91720, 53, 599, N'<Row><F1>452</F1><F3>30 Winter St</F3><F4>Boston</F4><F5>MA</F5><F6>30 Winter St
Boston, MA</F6></Row>')
,(91721, 53, 600, N'<Row><F1>453</F1><F3>300 Bear Hill Rd</F3><F4>Waltham</F4><F5>MA</F5><F6>300 Bear Hill Rd
Waltham, MA</F6></Row>')
,(91722, 53, 601, N'<Row><F1>1025</F1><F3>300 Capitol Mall</F3><F4>Sacramento</F4><F5>CA</F5><F6>300 Capitol Mall
Sacramento, CA</F6></Row>')
,(91723, 53, 602, N'<Row><F1>454</F1><F3>300 Continental Blvd</F3><F4>El Segundo</F4><F5>CA</F5><F6>300 Continental Blvd
El Segundo, CA</F6></Row>')
,(91724, 53, 603, N'<Row><F1>455</F1><F2>MacArthur Center</F2><F3>300 Monticello Ave</F3><F4>Norfolk</F4><F5>VA</F5><F6>MacArthur Center
300 Monticello Ave
Norfolk, VA</F6></Row>')
,(91725, 53, 604, N'<Row><F1>1253</F1><F2>Riverpark Tower II</F2><F3>300 Park Ave</F3><F4>San Jose</F4><F5>CA</F5><F6>Riverpark Tower II
300 Park Ave
San Jose, CA</F6></Row>')
,(91726, 53, 605, N'<Row><F1>1204</F1><F2>Dedham Mall</F2><F3>300 Providence Hwy</F3><F4>Dedham</F4><F5>MA</F5><F6>Dedham Mall
300 Providence Hwy
Dedham, MA</F6></Row>')
,(91727, 53, 606, N'<Row><F1>456</F1><F2>Inn at Wilmington</F2><F3>300 Rocky Run Pkwy</F3><F4>Wilmington</F4><F5>DE</F5><F6>Inn at Wilmington
300 Rocky Run Pkwy
Wilmington, DE</F6></Row>')
,(91728, 53, 607, N'<Row><F1>457</F1><F3>300 Second Ave</F3><F4>Waltham</F4><F5>MA</F5><F6>300 Second Ave
Waltham, MA</F6></Row>')
,(91729, 53, 608, N'<Row><F1>458</F1><F3>300 Utah Ave</F3><F4>San Francisco</F4><F5>CA</F5><F6>300 Utah Ave
San Francisco, CA</F6></Row>')
,(91730, 53, 609, N'<Row><F1>459</F1><F3>3000 Clearview Way</F3><F4>San Mateo</F4><F5>CA</F5><F6>3000 Clearview Way
San Mateo, CA</F6></Row>')
,(91731, 53, 610, N'<Row><F1>460</F1><F2>Maxwell Buick GMC</F2><F3>3000 Interstate 35</F3><F4>Round Rock</F4><F5>TX</F5><F6>Maxwell Buick GMC
3000 Interstate 35
Round Rock, TX</F6></Row>')
,(91732, 53, 611, N'<Row><F1>461</F1><F2>Heritage Honda</F2><F3>3001 E Ave</F3><F4>Baltimore</F4><F5>MD</F5><F6>Heritage Honda
3001 E Ave
Baltimore, MD</F6></Row>')
,(91733, 53, 612, N'<Row><F1>462</F1><F2>Calhoun Square</F2><F3>3001 Hennepin Ave</F3><F4>Minneapolis</F4><F5>MN</F5><F6>Calhoun Square
3001 Hennepin Ave
Minneapolis, MN</F6></Row>')
,(91734, 53, 613, N'<Row><F1>463</F1><F3>3001 Tasman Dr</F3><F4>Santa Clara</F4><F5>CA</F5><F6>3001 Tasman Dr
Santa Clara, CA</F6></Row>')
,(91735, 53, 614, N'<Row><F1>464</F1><F2>Family Dollar</F2><F3>3002 Shurling Dr</F3><F4>Macon</F4><F5>GA</F5><F6>Family Dollar
3002 Shurling Dr
Macon, GA</F6></Row>')
,(91736, 53, 615, N'<Row><F1>1265</F1><F2>Advanta Office Commons - Building C</F2><F3>3003 160th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>Advanta Office Commons - Building C
3003 160th Ave SE
Bellevue, WA</F6></Row>')
,(91737, 53, 616, N'<Row><F1>465</F1><F3>3003 Tasman Dr</F3><F4>Santa Clara</F4><F5>CA</F5><F6>3003 Tasman Dr
Santa Clara, CA</F6></Row>')
,(91738, 53, 617, N'<Row><F1>1249</F1><F3>3003 Washington Blvd</F3><F4>Arlington</F4><F5>VA</F5><F6>3003 Washington Blvd
Arlington, VA</F6></Row>')
,(91739, 53, 618, N'<Row><F1>466</F1><F3>3005 Tasman Dr</F3><F4>Santa Clara</F4><F5>CA</F5><F6>3005 Tasman Dr
Santa Clara, CA</F6></Row>')
,(91740, 53, 619, N'<Row><F1>1263</F1><F2>Advanta Office Commons - Building A</F2><F3>3007 160th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>Advanta Office Commons - Building A
3007 160th Ave SE
Bellevue, WA</F6></Row>')
,(91741, 53, 620, N'<Row><F1>1264</F1><F2>Advanta Office Commons - Building B</F2><F3>3009 160th Ave SE</F3><F4>Bellevue</F4><F5>WA</F5><F6>Advanta Office Commons - Building B
3009 160th Ave SE
Bellevue, WA</F6></Row>')
,(91742, 53, 621, N'<Row><F1>467</F1><F3>301 E Evelyn Ave</F3><F4>Mountain View</F4><F5>CA</F5><F6>301 E Evelyn Ave
Mountain View, CA</F6></Row>')
,(91743, 53, 622, N'<Row><F1>468</F1><F2>Family Dollar</F2><F3>301 S Main St</F3><F4>La Feria</F4><F5>TX</F5><F6>Family Dollar
301 S Main St
La Feria, TX</F6></Row>')
,(91744, 53, 623, N'<Row><F1>469</F1><F2>Orr Toyota of Searcy</F2><F3>301 S Poplar St</F3><F4>Searcy</F4><F5>AR</F5><F6>Orr Toyota of Searcy
301 S Poplar St
Searcy, AR</F6></Row>')
,(91745, 53, 624, N'<Row><F1>1280</F1><F2>The Metro</F2><F3>301 W 53rd St</F3><F4>New York</F4><F5>NY</F5><F6>The Metro
301 W 53rd St
New York, NY</F6></Row>')
,(91746, 53, 625, N'<Row><F1>470</F1><F3>3010-3040 N First St</F3><F4>San Jose</F4><F5>CA</F5><F6>3010-3040 N First St
San Jose, CA</F6></Row>')
,(91747, 53, 626, N'<Row><F1>471</F1><F2>Family Dollar</F2><F3>3013 N Chadbourne St</F3><F4>San Angelo</F4><F5>TX</F5><F6>Family Dollar
3013 N Chadbourne St
San Angelo, TX</F6></Row>')
,(91748, 53, 627, N'<Row><F1>472</F1><F3>3015 Clearview Way</F3><F4>San Mateo</F4><F5>CA</F5><F6>3015 Clearview Way
San Mateo, CA</F6></Row>')
,(91749, 53, 628, N'<Row><F1>473</F1><F2>Roundtree Pre-Owned Supercenter</F2><F3>3016 Government Blvd</F3><F4>Mobile</F4><F5>AL</F5><F6>Roundtree Pre-Owned Supercenter
3016 Government Blvd
Mobile, AL</F6></Row>')
,(91750, 53, 629, N'<Row><F1>474</F1><F3>3025 Clearview Way</F3><F4>San Mateo</F4><F5>CA</F5><F6>3025 Clearview Way
San Mateo, CA</F6></Row>')
,(91751, 53, 630, N'<Row><F1>475</F1><F3>303 Bear Hill Rd</F3><F4>Waltham</F4><F5>MA</F5><F6>303 Bear Hill Rd
Waltham, MA</F6></Row>')
,(91752, 53, 631, N'<Row><F1>476</F1><F3>3032 Bunker Hill Dr</F3><F4>Santa Clara</F4><F5>CA</F5><F6>3032 Bunker Hill Dr
Santa Clara, CA</F6></Row>')
,(91753, 53, 632, N'<Row><F1>477</F1><F2>Family Dollar</F2><F3>304 State Highway 185 N</F3><F4>Seadrift</F4><F5>TX</F5><F6>Family Dollar
304 State Highway 185 N
Seadrift, TX</F6></Row>')
,(91754, 53, 633, N'<Row><F1>478</F1><F2>Family Dollar</F2><F3>305 S Alamo St</F3><F4>Refugio</F4><F5>TX</F5><F6>Family Dollar
305 S Alamo St
Refugio, TX</F6></Row>')
,(91755, 53, 634, N'<Row><F1>479</F1><F2>Round Rock Nissan</F2><F3>3050 Interstate 35</F3><F4>Round Rock</F4><F5>TX</F5><F6>Round Rock Nissan
3050 Interstate 35
Round Rock, TX</F6></Row>')
,(91756, 53, 635, N'<Row><F1>480</F1><F3>30-50 Rowes Wharf</F3><F4>Boston</F4><F5>MA</F5><F6>30-50 Rowes Wharf
Boston, MA</F6></Row>')
,(91757, 53, 636, N'<Row><F1>1097</F1><F2>Bay Meadows Station 4</F2><F3>3050 S Delaware St</F3><F4>San Mateo</F4><F5>CA</F5><F6>Bay Meadows Station 4
3050 S Delaware St
San Mateo, CA</F6></Row>')
,(91758, 53, 637, N'<Row><F1>481</F1><F3>3052 Bunker Hill Dr</F3><F4>Santa Clara</F4><F5>CA</F5><F6>3052 Bunker Hill Dr
Santa Clara, CA</F6></Row>')
,(91759, 53, 638, N'<Row><F1>482</F1><F3>3055 Clearview Way</F3><F4>San Mateo</F4><F5>CA</F5><F6>3055 Clearview Way
San Mateo, CA</F6></Row>')
,(91760, 53, 639, N'<Row><F1>483</F1><F2>Action Nissan</F2><F3>307 Thompson Ln</F3><F4>Nashville</F4><F5>TN</F5><F6>Action Nissan
307 Thompson Ln
Nashville, TN</F6></Row>')
,(91761, 53, 640, N'<Row><F1>484</F1><F3>3075 W Ray Rd</F3><F4>Chandler</F4><F5>AZ</F5><F6>3075 W Ray Rd
Chandler, AZ</F6></Row>')
,(91762, 53, 641, N'<Row><F1>485</F1><F2>Family Dollar</F2><F3>308 W Main St</F3><F4>Hallsville</F4><F5>TX</F5><F6>Family Dollar
308 W Main St
Hallsville, TX</F6></Row>')
,(91763, 53, 642, N'<Row><F1>486</F1><F2>Family Dollar</F2><F3>3082 Cypress Gardens Rd</F3><F4>Winter Haven</F4><F5>FL</F5><F6>Family Dollar
3082 Cypress Gardens Rd
Winter Haven, FL</F6></Row>')
,(91764, 53, 643, N'<Row><F1>487</F1><F3>31 Milk St</F3><F4>Boston</F4><F5>MA</F5><F6>31 Milk St
Boston, MA</F6></Row>')
,(91765, 53, 644, N'<Row><F1>984</F1><F3>31 Plymouth St</F3><F4>Mansfield</F4><F5>MA</F5><F6>31 Plymouth St
Mansfield, MA</F6></Row>')
,(91766, 53, 645, N'<Row><F1>985</F1><F3>31 Suffolk Rd</F3><F4>Mansfield</F4><F5>MA</F5><F6>31 Suffolk Rd
Mansfield, MA</F6></Row>')
,(91767, 53, 646, N'<Row><F1>488</F1><F3>310 Utah Ave</F3><F4>San Francisco</F4><F5>CA</F5><F6>310 Utah Ave
San Francisco, CA</F6></Row>')
,(91768, 53, 647, N'<Row><F1>489</F1><F2>Family Dollar</F2><F3>310 W Swannanoa Ave</F3><F4>Liberty</F4><F5>NC</F5><F6>Family Dollar
310 W Swannanoa Ave
Liberty, NC</F6></Row>')
,(91769, 53, 648, N'<Row><F1>490</F1><F2>Family Dollar</F2><F3>3100 E Business Hwy 83</F3><F4>Donna</F4><F5>TX</F5><F6>Family Dollar
3100 E Business Hwy 83
Donna, TX</F6></Row>')
,(91770, 53, 649, N'<Row><F1>491</F1><F2>Holiday Inn Express Langhorne Oxford Valley</F2><F3>3101 Cabot Blvd W</F3><F4>Langhorne</F4><F5>PA</F5><F6>Holiday Inn Express Langhorne Oxford Valley
3101 Cabot Blvd W
Langhorne, PA</F6></Row>')
,(91771, 53, 650, N'<Row><F1>492</F1><F2>Family Dollar</F2><F3>3107 Springs Rd NE</F3><F4>Hickory</F4><F5>NC</F5><F6>Family Dollar
3107 Springs Rd NE
Hickory, NC</F6></Row>')
,(91772, 53, 651, N'<Row><F1>493</F1><F3>311 S Wacker Dr</F3><F4>Chicago</F4><F5>IL</F5><F6>311 S Wacker Dr
Chicago, IL</F6></Row>')
,(91773, 53, 652, N'<Row><F1>494</F1><F3>311 W Monroe</F3><F4>Chicago</F4><F5>IL</F5><F6>311 W Monroe
Chicago, IL</F6></Row>')
,(91774, 53, 653, N'<Row><F1>495</F1><F2>Hendrick Chevrolet Cadillac of Monroe</F2><F3>3112 Hwy 74 W</F3><F4>Monroe</F4><F5>NC</F5><F6>Hendrick Chevrolet Cadillac of Monroe
3112 Hwy 74 W
Monroe, NC</F6></Row>')
,(91775, 53, 654, N'<Row><F1>496</F1><F2>Roundtree Chrysler Dodge Jeep</F2><F3>3118 Government Blvd</F3><F4>Mobile</F4><F5>AL</F5><F6>Roundtree Chrysler Dodge Jeep
3118 Government Blvd
Mobile, AL</F6></Row>')
,(91776, 53, 655, N'<Row><F1>497</F1><F2>Chuck E. Cheese''s</F2><F3>312 W Loop 281</F3><F4>Longview</F4><F5>TX</F5><F6>Chuck E. Cheese''s
312 W Loop 281
Longview, TX</F6></Row>')
,(91777, 53, 656, N'<Row><F1>498</F1><F2>Palm Harbor Honda</F2><F3>31200 US Highway 19 N</F3><F4>Palm Harbor</F4><F5>FL</F5><F6>Palm Harbor Honda
31200 US Highway 19 N
Palm Harbor, FL</F6></Row>')
,(91778, 53, 657, N'<Row><F1>499</F1><F2>Herb Gordon Volvo</F2><F3>3121 Automobile Blvd</F3><F4>Silver Spring</F4><F5>MD</F5><F6>Herb Gordon Volvo
3121 Automobile Blvd
Silver Spring, MD</F6></Row>')
,(91779, 53, 658, N'<Row><F1>500</F1><F3>3125 Clearview Way</F3><F4>San Mateo</F4><F5>CA</F5><F6>3125 Clearview Way
San Mateo, CA</F6></Row>')
,(91780, 53, 659, N'<Row><F1>501</F1><F2>Herb Gordon Nissan</F2><F3>3131 Automobile Blvd</F3><F4>Silver Spring</F4><F5>MD</F5><F6>Herb Gordon Nissan
3131 Automobile Blvd
Silver Spring, MD</F6></Row>')
,(91781, 53, 660, N'<Row><F1>502</F1><F2>Doubletree Nashville</F2><F3>315 4th Ave N</F3><F4>Nashville</F4><F5>TN</F5><F6>Doubletree Nashville
315 4th Ave N
Nashville, TN</F6></Row>')
,(91782, 53, 661, N'<Row><F1>503</F1><F2>Hyatt Regency St. Louis at the Arch</F2><F3>315 Chestnut St</F3><F4>St. Louis</F4><F5>MO</F5><F6>Hyatt Regency St. Louis at the Arch
315 Chestnut St
St. Louis, MO</F6></Row>')
,(91783, 53, 662, N'<Row><F1>504</F1><F2>Country Inn &amp; Suites New Orleans </F2><F3>315 Magazine St</F3><F4>New Orleans</F4><F5>LA</F5><F6>Country Inn &amp; Suites New Orleans 
315 Magazine St
New Orleans, LA</F6></Row>')
,(91784, 53, 663, N'<Row><F1>505</F1><F2>Hall Chrysler Dodge Jeep Ram</F2><F3>3152 Virginia Beach Blvd</F3><F4>Virginia Beach</F4><F5>VA</F5><F6>Hall Chrysler Dodge Jeep Ram
3152 Virginia Beach Blvd
Virginia Beach, VA</F6></Row>')
,(91785, 53, 664, N'<Row><F1>506</F1><F3>3155 Clearview Way</F3><F4>San Mateo</F4><F5>CA</F5><F6>3155 Clearview Way
San Mateo, CA</F6></Row>')
,(91786, 53, 665, N'<Row><F1>507</F1><F2>Herb Gordon Subaru</F2><F3>3161 Automobile Blvd</F3><F4>Silver Spring</F4><F5>MD</F5><F6>Herb Gordon Subaru
3161 Automobile Blvd
Silver Spring, MD</F6></Row>')
,(91787, 53, 666, N'<Row><F1>508</F1><F3>32 Crosby Dr</F3><F4>Bedford</F4><F5>MA</F5><F6>32 Crosby Dr
Bedford, MA</F6></Row>')
,(91788, 53, 667, N'<Row><F1>509</F1><F2>Courtyard Wilmington Brandywine</F2><F3>320 Rocky Run Pkwy</F3><F4>Wilmington</F4><F5>DE</F5><F6>Courtyard Wilmington Brandywine
320 Rocky Run Pkwy
Wilmington, DE</F6></Row>')
,(91789, 53, 668, N'<Row><F1>510</F1><F2>Hall Acura Virginia Beach</F2><F3>3200 Virginia Beach Blvd</F3><F4>Virginia Beach</F4><F5>VA</F5><F6>Hall Acura Virginia Beach
3200 Virginia Beach Blvd
Virginia Beach, VA</F6></Row>')
,(91790, 53, 669, N'<Row><F1>511</F1><F3>3201 E Arkansas Ln</F3><F4>Arlington</F4><F5>TX</F5><F6>3201 E Arkansas Ln
Arlington, TX</F6></Row>')
,(91791, 53, 670, N'<Row><F1>512</F1><F2>Brown Palace Hotel</F2><F3>321 17th St</F3><F4>Denver</F4><F5>CO</F5><F6>Brown Palace Hotel
321 17th St
Denver, CO</F6></Row>')
,(91792, 53, 671, N'<Row><F1>513</F1><F3>321 E Evelyn Ave</F3><F4>Mountain View</F4><F5>CA</F5><F6>321 E Evelyn Ave
Mountain View, CA</F6></Row>')
,(91793, 53, 672, N'<Row><F1>514</F1><F2>Family Dollar</F2><F3>3212 Mobile Hwy</F3><F4>Montgomery</F4><F5>AL</F5><F6>Family Dollar
3212 Mobile Hwy
Montgomery, AL</F6></Row>')
,(91794, 53, 673, N'<Row><F1>515</F1><F3>3221 E Arkansas Ln</F3><F4>Arlington</F4><F5>TX</F5><F6>3221 E Arkansas Ln
Arlington, TX</F6></Row>')
,(91795, 53, 674, N'<Row><F1>516</F1><F2>Chuck E. Cheese''s</F2><F3>3221 E Prien Lake Rd</F3><F4>Lake Charles</F4><F5>LA</F5><F6>Chuck E. Cheese''s
3221 E Prien Lake Rd
Lake Charles, LA</F6></Row>')
,(91796, 53, 675, N'<Row><F1>517</F1><F2>Georgetown Park</F2><F3>3222 M Street NW</F3><F4>Washington</F4><F5>DC</F5><F6>Georgetown Park
3222 M Street NW
Washington, DC</F6></Row>')
,(91797, 53, 676, N'<Row><F1>518</F1><F2>Chuck E. Cheese''s</F2><F3>3223 N Rock Rd</F3><F4>Wichita</F4><F5>KS</F5><F6>Chuck E. Cheese''s
3223 N Rock Rd
Wichita, KS</F6></Row>')
,(91798, 53, 677, N'<Row><F1>519</F1><F3>3225 W Ray Rd</F3><F4>Chandler</F4><F5>AZ</F5><F6>3225 W Ray Rd
Chandler, AZ</F6></Row>')
,(91799, 53, 678, N'<Row><F1>520</F1><F3>323 Vintage Park Dr</F3><F4>Foster City</F4><F5>CA</F5><F6>323 Vintage Park Dr
Foster City, CA</F6></Row>')
,(91800, 53, 679, N'<Row><F1>521</F1><F3>3235 W Ray Rd</F3><F4>Chandler</F4><F5>AZ</F5><F6>3235 W Ray Rd
Chandler, AZ</F6></Row>')
,(91801, 53, 680, N'<Row><F1>1076</F1><F2>Hampton Inn Atlanta-Mall of Georgia</F2><F3>3240 Buford Dr</F3><F4>Buford</F4><F5>GA</F5><F6>Hampton Inn Atlanta-Mall of Georgia
3240 Buford Dr
Buford, GA</F6></Row>')
,(91802, 53, 681, N'<Row><F1>522</F1><F3>3245 W Ray Rd</F3><F4>Chandler</F4><F5>AZ</F5><F6>3245 W Ray Rd
Chandler, AZ</F6></Row>')
,(91803, 53, 682, N'<Row><F1>523</F1><F2>Fairfield Inn &amp; Suites New York Midtown Manhattan/Penn Station</F2><F3>325 W 33rd St</F3><F4>New York</F4><F5>NY</F5><F6>Fairfield Inn &amp; Suites New York Midtown Manhattan/Penn Station
325 W 33rd St
New York, NY</F6></Row>')
,(91804, 53, 683, N'<Row><F1>1077</F1><F2>SpringHill Suites Atlanta Buford/Mall of Georgia</F2><F3>3250 Buford Dr</F3><F4>Buford</F4><F5>GA</F5><F6>SpringHill Suites Atlanta Buford/Mall of Georgia
3250 Buford Dr
Buford, GA</F6></Row>')
,(91805, 53, 684, N'<Row><F1>524</F1><F2>Family Dollar</F2><F3>3250 W Indian School Rd</F3><F4>Phoenix</F4><F5>AZ</F5><F6>Family Dollar
3250 W Indian School Rd
Phoenix, AZ</F6></Row>')
,(91806, 53, 685, N'<Row><F1>1000</F1><F2>Promenades Mall</F2><F3>3280 N Tamiami Trail</F3><F4>Port Charlotte</F4><F5>FL</F5><F6>Promenades Mall
3280 N Tamiami Trail
Port Charlotte, FL</F6></Row>')
,(91807, 53, 686, N'<Row><F1>525</F1><F2>Ira Toyota of Manchester</F2><F3>33 Auto Center Rd</F3><F4>Manchester</F4><F5>NH</F5><F6>Ira Toyota of Manchester
33 Auto Center Rd
Manchester, NH</F6></Row>')
,(91808, 53, 687, N'<Row><F1>526</F1><F3>33 Grosvenor Pl</F3><F4>London</F4><F5>UK</F5><F6>33 Grosvenor Pl
London, UK</F6></Row>')
,(91809, 53, 688, N'<Row><F1>986</F1><F3>33 Suffolk Rd</F3><F4>Mansfield</F4><F5>MA</F5><F6>33 Suffolk Rd
Mansfield, MA</F6></Row>')
,(91810, 53, 689, N'<Row><F1>527</F1><F3>330 Hudson St</F3><F4>New York</F4><F5>NY</F5><F6>330 Hudson St
New York, NY</F6></Row>')
,(91811, 53, 690, N'<Row><F1>1079</F1><F3>3301 El Camino Real</F3><F4>Atherton</F4><F5>CA</F5><F6>3301 El Camino Real
Atherton, CA</F6></Row>')
,(91812, 53, 691, N'<Row><F1>528</F1><F3>331 E Evelyn Ave</F3><F4>Mountain View</F4><F5>CA</F5><F6>331 E Evelyn Ave
Mountain View, CA</F6></Row>')
,(91813, 53, 692, N'<Row><F1>529</F1><F2>Family Dollar</F2><F3>3312 East Main Ave</F3><F4>Alton</F4><F5>TX</F5><F6>Family Dollar
3312 East Main Ave
Alton, TX</F6></Row>')
,(91814, 53, 693, N'<Row><F1>530</F1><F2>Hawaii Kai Towne Center</F2><F3>333 Keahole St</F3><F4>Honolulu</F4><F5>HI</F5><F6>Hawaii Kai Towne Center
333 Keahole St
Honolulu, HI</F6></Row>')
,(91815, 53, 694, N'<Row><F1>1283</F1><F3>3333 N Torrey Pines Ct</F3><F4>La Jolla</F4><F5>CA</F5><F6>3333 N Torrey Pines Ct
La Jolla, CA</F6></Row>')
,(91816, 53, 695, N'<Row><F1>1284</F1><F3>3344 N Torrey Pines Ct</F3><F4>La Jolla</F4><F5>CA</F5><F6>3344 N Torrey Pines Ct
La Jolla, CA</F6></Row>')
,(91817, 53, 696, N'<Row><F1>531</F1><F3>335 Bear Hill Rd</F3><F4>Waltham</F4><F5>MA</F5><F6>335 Bear Hill Rd
Waltham, MA</F6></Row>')
,(91818, 53, 697, N'<Row><F1>1080</F1><F3>3351 El Camino Real</F3><F4>Atherton</F4><F5>CA</F5><F6>3351 El Camino Real
Atherton, CA</F6></Row>')
,(91819, 53, 698, N'<Row><F1>532</F1><F2>Family Dollar</F2><F3>336 Main St</F3><F4>Acadia</F4><F5>LA</F5><F6>Family Dollar
336 Main St
Acadia, LA</F6></Row>')
,(91820, 53, 699, N'<Row><F1>1285</F1><F3>3366 N Torrey Pines Ct</F3><F4>La Jolla</F4><F5>CA</F5><F6>3366 N Torrey Pines Ct
La Jolla, CA</F6></Row>')
,(91821, 53, 700, N'<Row><F1>1286</F1><F3>3377 N Torrey Pines Ct</F3><F4>La Jolla</F4><F5>CA</F5><F6>3377 N Torrey Pines Ct
La Jolla, CA</F6></Row>')
,(91822, 53, 701, N'<Row><F1>533</F1><F3>3385 Princeton Rd</F3><F4>Hamilton</F4><F5>OH</F5><F6>3385 Princeton Rd
Hamilton, OH</F6></Row>')
,(91823, 53, 702, N'<Row><F1>534</F1><F3>3390 Carmel Mountain Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>3390 Carmel Mountain Rd
San Diego, CA</F6></Row>')
,(91824, 53, 703, N'<Row><F1>535</F1><F3>3394 Carmel Mountain Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>3394 Carmel Mountain Rd
San Diego, CA</F6></Row>')
,(91825, 53, 704, N'<Row><F1>536</F1><F3>3398 Carmel Mountain Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>3398 Carmel Mountain Rd
San Diego, CA</F6></Row>')
,(91826, 53, 705, N'<Row><F1>1074</F1><F2>SpringHill Suites Atlanta Kennesaw</F2><F3>3399 Town Point Rd</F3><F4>Kennesaw</F4><F5>GA</F5><F6>SpringHill Suites Atlanta Kennesaw
3399 Town Point Rd
Kennesaw, GA</F6></Row>')
,(91827, 53, 706, N'<Row><F1>537</F1><F3>34 Crosby Dr</F3><F4>Bedford</F4><F5>MA</F5><F6>34 Crosby Dr
Bedford, MA</F6></Row>')
,(91828, 53, 707, N'<Row><F1>538</F1><F2>Turnersville Automall</F2><F3>3400 New Jersey 42</F3><F4>Blackwood</F4><F5>NJ</F5><F6>Turnersville Automall
3400 New Jersey 42
Blackwood, NJ</F6></Row>')
,(91829, 53, 708, N'<Row><F1>539</F1><F2>Chuck E. Cheese''s</F2><F3>341 South Interstate 35 E</F3><F4>Denton</F4><F5>TX</F5><F6>Chuck E. Cheese''s
341 South Interstate 35 E
Denton, TX</F6></Row>')
,(91830, 53, 709, N'<Row><F1>540</F1><F2>CarMax Greensboro</F2><F3>3412 W Wendover Ave</F3><F4>Greensboro</F4><F5>NC</F5><F6>CarMax Greensboro
3412 W Wendover Ave
Greensboro, NC</F6></Row>')
,(91831, 53, 710, N'<Row><F1>541</F1><F2>Hall Chevrolet/Hyundai (TID010&amp;012)</F2><F3>3412 Western Branch Rd</F3><F4>Chesapeake</F4><F5>VA</F5><F6>Hall Chevrolet/Hyundai (TID010&amp;012)
3412 Western Branch Rd
Chesapeake, VA</F6></Row>')
,(91832, 53, 711, N'<Row><F1>542</F1><F2>Hall Nissan Chesapeake</F2><F3>3417 Western Branch Blvd</F3><F4>Chesapeake</F4><F5>VA</F5><F6>Hall Nissan Chesapeake
3417 Western Branch Blvd
Chesapeake, VA</F6></Row>')
,(91833, 53, 712, N'<Row><F1>543</F1><F2>The Inn at Key West</F2><F3>3420 N Roosevelt Blvd</F3><F4>Key West</F4><F5>FL</F5><F6>The Inn at Key West
3420 N Roosevelt Blvd
Key West, FL</F6></Row>')
,(91834, 53, 713, N'<Row><F1>544</F1><F2>Family Dollar</F2><F3>34224 US 98</F3><F4>Lillian</F4><F5>AL</F5><F6>Family Dollar
34224 US 98
Lillian, AL</F6></Row>')
,(91835, 53, 714, N'<Row><F1>1075</F1><F2>Fairfield Inn &amp; Suites Atlanta Kennesaw</F2><F3>3425 Busbee Dr NW</F3><F4>Kennesaw</F4><F5>GA</F5><F6>Fairfield Inn &amp; Suites Atlanta Kennesaw
3425 Busbee Dr NW
Kennesaw, GA</F6></Row>')
,(91836, 53, 715, N'<Row><F1>545</F1><F2>Family Dollar</F2><F3>3435 Highway 14</F3><F4>Millbrook</F4><F5>AL</F5><F6>Family Dollar
3435 Highway 14
Millbrook, AL</F6></Row>')
,(91837, 53, 716, N'<Row><F1>546</F1><F2>Family Dollar</F2><F3>3454 Baker Rd</F3><F4>Acworth</F4><F5>GA</F5><F6>Family Dollar
3454 Baker Rd
Acworth, GA</F6></Row>')
,(91838, 53, 717, N'<Row><F1>547</F1><F2>Family Dollar</F2><F3>3485 N Lecanto Hwy</F3><F4>Beverly Hills</F4><F5>FL</F5><F6>Family Dollar
3485 N Lecanto Hwy
Beverly Hills, FL</F6></Row>')
,(91839, 53, 718, N'<Row><F1>548</F1><F2>Hall Mitsubishi (TID008&amp;009)</F2><F3>3496 Holland Rd</F3><F4>Virginia Beach</F4><F5>VA</F5><F6>Hall Mitsubishi (TID008&amp;009)
3496 Holland Rd
Virginia Beach, VA</F6></Row>')
,(91840, 53, 719, N'<Row><F1>1070</F1><F2>Courtyard Columbus Downtown</F2><F3>35 W Spring St</F3><F4>Columbus</F4><F5>OH</F5><F6>Courtyard Columbus Downtown
35 W Spring St
Columbus, OH</F6></Row>')
,(91841, 53, 720, N'<Row><F1>549</F1><F2>Woodfield Lexus</F2><F3>350 E Golf Rd</F3><F4>Schaumburg</F4><F5>IL</F5><F6>Woodfield Lexus
350 E Golf Rd
Schaumburg, IL</F6></Row>')
,(91842, 53, 721, N'<Row><F1>550</F1><F2>Las Olas Centre</F2><F3>350 Las Olas Blvd</F3><F4>Fort Lauderdale</F4><F5>FL</F5><F6>Las Olas Centre
350 Las Olas Blvd
Fort Lauderdale, FL</F6></Row>')
,(91843, 53, 722, N'<Row><F1>551</F1><F2>Scottsdale Body Shop</F2><F3>350 N Hayden Rd</F3><F4>Scottsdale</F4><F5>AZ</F5><F6>Scottsdale Body Shop
350 N Hayden Rd
Scottsdale, AZ</F6></Row>')
,(91844, 53, 723, N'<Row><F1>1102</F1><F2>Central Park at Toluca Lake</F2><F3>3500 W Olive Way</F3><F4>Burbank</F4><F5>CA</F5><F6>Central Park at Toluca Lake
3500 W Olive Way
Burbank, CA</F6></Row>')
,(91845, 53, 724, N'<Row><F1>552</F1><F2>East Madison Toyota</F2><F3>3501 Lancaster Dr</F3><F4>Madison</F4><F5>WI</F5><F6>East Madison Toyota
3501 Lancaster Dr
Madison, WI</F6></Row>')
,(91846, 53, 725, N'<Row><F1>553</F1><F3>351 E Evelyn Ave</F3><F4>Mountain View</F4><F5>CA</F5><F6>351 E Evelyn Ave
Mountain View, CA</F6></Row>')
,(91847, 53, 726, N'<Row><F1>554</F1><F2>Hall Honda</F2><F3>3516 Virginia Beach Blvd</F3><F4>Virginia Beach</F4><F5>VA</F5><F6>Hall Honda
3516 Virginia Beach Blvd
Virginia Beach, VA</F6></Row>')
,(91848, 53, 727, N'<Row><F1>555</F1><F3>353 Vintage Park Dr</F3><F4>Foster City</F4><F5>CA</F5><F6>353 Vintage Park Dr
Foster City, CA</F6></Row>')
,(91849, 53, 728, N'<Row><F1>556</F1><F3>3535 Market St</F3><F4>Philadelphia</F4><F5>PA</F5><F6>3535 Market St
Philadelphia, PA</F6></Row>')
,(91850, 53, 729, N'<Row><F1>1171</F1><F2>Lenox Marketplace</F2><F3>3535 Peachtree Rd</F3><F4>Atlanta</F4><F5>GA</F5><F6>Lenox Marketplace
3535 Peachtree Rd
Atlanta, GA</F6></Row>')
,(91851, 53, 730, N'<Row><F1>979</F1><F3>35-41 Hampden Rd</F3><F4>Mansfield</F4><F5>MA</F5><F6>35-41 Hampden Rd
Mansfield, MA</F6></Row>')
,(91852, 53, 731, N'<Row><F1>1248</F1><F3>3545 N 1st St</F3><F4>San Jose</F4><F5>CA</F5><F6>3545 N 1st St
San Jose, CA</F6></Row>')
,(91853, 53, 732, N'<Row><F1>1243</F1><F3>3553 N 1st St</F3><F4>San Jose</F4><F5>CA</F5><F6>3553 N 1st St
San Jose, CA</F6></Row>')
,(91854, 53, 733, N'<Row><F1>1291</F1><F2>Diplomat Resort &amp; Spa Hollywood</F2><F3>3555 S Ocean Dr</F3><F4>Hollywood</F4><F5>FL</F5><F6>Diplomat Resort &amp; Spa Hollywood
3555 S Ocean Dr
Hollywood, FL</F6></Row>')
,(91855, 53, 734, N'<Row><F1>557</F1><F3>36 Crosby Dr</F3><F4>Bedford</F4><F5>MA</F5><F6>36 Crosby Dr
Bedford, MA</F6></Row>')
,(91856, 53, 735, N'<Row><F1>558</F1><F2>Courtyard Ewing Hopewell</F2><F3>360 Scotch Rd</F3><F4>Ewing</F4><F5>NJ</F5><F6>Courtyard Ewing Hopewell
360 Scotch Rd
Ewing, NJ</F6></Row>')
,(91857, 53, 736, N'<Row><F1>559</F1><F2>El Con Center</F2><F3>3601 E Broadway Blvd</F3><F4>Tucson</F4><F5> AZ</F5><F6>El Con Center
3601 E Broadway Blvd
Tucson,  AZ</F6></Row>')
,(91858, 53, 737, N'<Row><F1>560</F1><F2>Crown Volvo</F2><F3>3604 W Wendover Ave</F3><F4>Greensboro</F4><F5>NC</F5><F6>Crown Volvo
3604 W Wendover Ave
Greensboro, NC</F6></Row>')
,(91859, 53, 738, N'<Row><F1>561</F1><F2>Bohn Brothers Toyota Scion</F2><F3>3611 Lapalco Blvd</F3><F4>Harvey</F4><F5>LA</F5><F6>Bohn Brothers Toyota Scion
3611 Lapalco Blvd
Harvey, LA</F6></Row>')
,(91860, 53, 739, N'<Row><F1>562</F1><F2>Family Dollar</F2><F3>3618 S Newcastle Rd</F3><F4>Oklahoma City</F4><F5>OK</F5><F6>Family Dollar
3618 S Newcastle Rd
Oklahoma City, OK</F6></Row>')
,(91861, 53, 740, N'<Row><F1>563</F1><F3>363 Vintage Park Dr</F3><F4>Foster City</F4><F5>CA</F5><F6>363 Vintage Park Dr
Foster City, CA</F6></Row>')
,(91862, 53, 741, N'<Row><F1>1066</F1><F2>Residence Inn Las Vegas Hughes Center</F2><F3>370 Hughes Center Dr</F3><F4>Las Vegas</F4><F5>NV</F5><F6>Residence Inn Las Vegas Hughes Center
370 Hughes Center Dr
Las Vegas, NV</F6></Row>')
,(91863, 53, 742, N'<Row><F1>564</F1><F2>Nissan of Clovis</F2><F3>370 W Herndon Ave</F3><F4>Clovis</F4><F5>CA</F5><F6>Nissan of Clovis
370 W Herndon Ave
Clovis, CA</F6></Row>')
,(91864, 53, 743, N'<Row><F1>565</F1><F2>Future Nissan of Alvin</F2><F3>3700 Alvin Bypass</F3><F4>Alvin</F4><F5>TX</F5><F6>Future Nissan of Alvin
3700 Alvin Bypass
Alvin, TX</F6></Row>')
,(91865, 53, 744, N'<Row><F1>566</F1><F2>Former Massey Cadillac of Sanford</F2><F3>3700 S Hwy 17-92</F3><F4>Sanford</F4><F5>FL</F5><F6>Former Massey Cadillac of Sanford
3700 S Hwy 17-92
Sanford, FL</F6></Row>')
,(91866, 53, 745, N'<Row><F1>567</F1><F2>Marriott Wailea Beach Resort &amp; Spa</F2><F3>3700 Wailea Alanui Dr</F3><F4>Kihei</F4><F5>HI</F5><F6>Marriott Wailea Beach Resort &amp; Spa
3700 Wailea Alanui Dr
Kihei, HI</F6></Row>')
,(91867, 53, 746, N'<Row><F1>568</F1><F2>Family Dollar</F2><F3>3707 N Dixie Hwy</F3><F4>Monroe</F4><F5>MI</F5><F6>Family Dollar
3707 N Dixie Hwy
Monroe, MI</F6></Row>')
,(91868, 53, 747, N'<Row><F1>569</F1><F2>Lujack''s Honda/Hyundai</F2><F3>3707 N Harrison St</F3><F4>Davenport</F4><F5>IA</F5><F6>Lujack''s Honda/Hyundai
3707 N Harrison St
Davenport, IA</F6></Row>')
,(91869, 53, 748, N'<Row><F1>570</F1><F2>Family Dollar</F2><F3>3723 US Highway 29</F3><F4>Danville</F4><F5>VA</F5><F6>Family Dollar
3723 US Highway 29
Danville, VA</F6></Row>')
,(91870, 53, 749, N'<Row><F1>571</F1><F3>373 Vintage Park Dr</F3><F4>Foster City</F4><F5>CA</F5><F6>373 Vintage Park Dr
Foster City, CA</F6></Row>')
,(91871, 53, 750, N'<Row><F1>572</F1><F2>Don Bohn Ford</F2><F3>3737 Lapalco Blvd</F3><F4>Harvey</F4><F5>LA</F5><F6>Don Bohn Ford
3737 Lapalco Blvd
Harvey, LA</F6></Row>')
,(91872, 53, 751, N'<Row><F1>978</F1><F3>375 Forbes Blvd</F3><F4>Mansfield</F4><F5>MA</F5><F6>375 Forbes Blvd
Mansfield, MA</F6></Row>')
,(91873, 53, 752, N'<Row><F1>573</F1><F3>375 W Broadway</F3><F4>New York</F4><F5>NY</F5><F6>375 W Broadway
New York, NY</F6></Row>')
,(91874, 53, 753, N'<Row><F1>1259</F1><F2>The Shops at Wailea</F2><F3>3750 Wailea Alanui Dr</F3><F4>Kihei</F4><F5>HI</F5><F6>The Shops at Wailea
3750 Wailea Alanui Dr
Kihei, HI</F6></Row>')
,(91875, 53, 754, N'<Row><F1>574</F1><F2>Hall Nissan Virginia Beach</F2><F3>3757 Bonney Rd</F3><F4>Virginia Beach</F4><F5>VA</F5><F6>Hall Nissan Virginia Beach
3757 Bonney Rd
Virginia Beach, VA</F6></Row>')
,(91876, 53, 755, N'<Row><F1>575</F1><F2>Mark Dodge Chrysler Jeep</F2><F3>3777 Gerstner Memorial Dr</F3><F4>Lake Charles</F4><F5>LA</F5><F6>Mark Dodge Chrysler Jeep
3777 Gerstner Memorial Dr
Lake Charles, LA</F6></Row>')
,(91877, 53, 756, N'<Row><F1>576</F1><F3>378 Vintage Park Dr</F3><F4>Foster City</F4><F5>CA</F5><F6>378 Vintage Park Dr
Foster City, CA</F6></Row>')
,(91878, 53, 757, N'<Row><F1>577</F1><F2>Family Dollar</F2><F3>379726 Arizona 75</F3><F4>Duncan</F4><F5>AZ</F5><F6>Family Dollar
379726 Arizona 75
Duncan, AZ</F6></Row>')
,(91879, 53, 758, N'<Row><F1>578</F1><F3>380 S Rohlwing Rd</F3><F4>Addison</F4><F5>IL</F5><F6>380 S Rohlwing Rd
Addison, IL</F6></Row>')
,(91880, 53, 759, N'<Row><F1>1109</F1><F3>3800 N Central Ave</F3><F4>Phoenix</F4><F5>AZ</F5><F6>3800 N Central Ave
Phoenix, AZ</F6></Row>')
,(91881, 53, 760, N'<Row><F1>579</F1><F2>David McDavid Honda Used Car Supercenter</F2><F3>3800 W Airport Fwy</F3><F4>Irving</F4><F5>TX</F5><F6>David McDavid Honda Used Car Supercenter
3800 W Airport Fwy
Irving, TX</F6></Row>')
,(91882, 53, 761, N'<Row><F1>580</F1><F2>Don Bohn Buick GMC</F2><F3>3801 Lapalco Blvd</F3><F4>Harvey</F4><F5>LA</F5><F6>Don Bohn Buick GMC
3801 Lapalco Blvd
Harvey, LA</F6></Row>')
,(91883, 53, 762, N'<Row><F1>581</F1><F3>381 E Evelyn Ave</F3><F4>Mountain View</F4><F5>CA</F5><F6>381 E Evelyn Ave
Mountain View, CA</F6></Row>')
,(91884, 53, 763, N'<Row><F1>582</F1><F2>Family Dollar</F2><F3>3812 N 9th Ave</F3><F4>Pensacola</F4><F5>FL</F5><F6>Family Dollar
3812 N 9th Ave
Pensacola, FL</F6></Row>')
,(91885, 53, 764, N'<Row><F1>583</F1><F2>Malibu Village </F2><F3>3822-3898 Cross Creek Rd</F3><F4>Malibu</F4><F5>CA</F5><F6>Malibu Village 
3822-3898 Cross Creek Rd
Malibu, CA</F6></Row>')
,(91886, 53, 765, N'<Row><F1>584</F1><F3>383 Vintage Park Dr</F3><F4>Foster City</F4><F5>CA</F5><F6>383 Vintage Park Dr
Foster City, CA</F6></Row>')
,(91887, 53, 766, N'<Row><F1>1110</F1><F3>3838 N Central Ave</F3><F4>Phoenix</F4><F5>AZ</F5><F6>3838 N Central Ave
Phoenix, AZ</F6></Row>')
,(91888, 53, 767, N'<Row><F1>585</F1><F2>Post Luminaria</F2><F3>385 1st Ave</F3><F4>New York</F4><F5>NY</F5><F6>Post Luminaria
385 1st Ave
New York, NY</F6></Row>')
,(91889, 53, 768, N'<Row><F1>586</F1><F3>388-390 Greenwich St</F3><F4>New York</F4><F5>NY</F5><F6>388-390 Greenwich St
New York, NY</F6></Row>')
,(91890, 53, 769, N'<Row><F1>587</F1><F2>Post Toscana</F2><F3>389 E 89th St</F3><F4>New York</F4><F5>NY</F5><F6>Post Toscana
389 E 89th St
New York, NY</F6></Row>')
,(91891, 53, 770, N'<Row><F1>588</F1><F3>3895 E Main St</F3><F4>St. Charles</F4><F5>IL</F5><F6>3895 E Main St
St. Charles, IL</F6></Row>')
,(91892, 53, 771, N'<Row><F1>589</F1><F2>Mercedes Benz of Houston - Greenway</F2><F3>3900 SW Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Mercedes Benz of Houston - Greenway
3900 SW Fwy
Houston, TX</F6></Row>')
,(91893, 53, 772, N'<Row><F1>590</F1><F2>Tower Burbank</F2><F3>3900 W Alameda Ave</F3><F4>Burbank</F4><F5>CA</F5><F6>Tower Burbank
3900 W Alameda Ave
Burbank, CA</F6></Row>')
,(91894, 53, 773, N'<Row><F1>1132</F1><F2>Four Seasons Resort Maui at Wailea</F2><F3>3900 Wailea Alanui Dr</F3><F4>Wailea</F4><F5>HI</F5><F6>Four Seasons Resort Maui at Wailea
3900 Wailea Alanui Dr
Wailea, HI</F6></Row>')
,(91895, 53, 774, N'<Row><F1>1161</F1><F3>3919 E Guasti Rd</F3><F4>Ontario</F4><F5>CA</F5><F6>3919 E Guasti Rd
Ontario, CA</F6></Row>')
,(91896, 53, 775, N'<Row><F1>1162</F1><F3>3929 E Guasti Rd</F3><F4>Ontario</F4><F5>CA</F5><F6>3929 E Guasti Rd
Ontario, CA</F6></Row>')
,(91897, 53, 776, N'<Row><F1>591</F1><F3>393 Vintage Park Dr</F3><F4>Foster City</F4><F5>CA</F5><F6>393 Vintage Park Dr
Foster City, CA</F6></Row>')
,(91898, 53, 777, N'<Row><F1>1163</F1><F3>3939 E Guasti Rd</F3><F4>Ontario</F4><F5>CA</F5><F6>3939 E Guasti Rd
Ontario, CA</F6></Row>')
,(91899, 53, 778, N'<Row><F1>1164</F1><F3>3949 E Guasti Rd</F3><F4>Ontario</F4><F5>CA</F5><F6>3949 E Guasti Rd
Ontario, CA</F6></Row>')
,(91900, 53, 779, N'<Row><F1>592</F1><F2>Family Dollar</F2><F3>395 John R Junkin Dr</F3><F4>Natchez</F4><F5>MS</F5><F6>Family Dollar
395 John R Junkin Dr
Natchez, MS</F6></Row>')
,(91901, 53, 780, N'<Row><F1>1165</F1><F3>3959 E Guasti Rd</F3><F4>Ontario</F4><F5>CA</F5><F6>3959 E Guasti Rd
Ontario, CA</F6></Row>')
,(91902, 53, 781, N'<Row><F1>1166</F1><F3>3969 E Guasti Rd</F3><F4>Ontario</F4><F5>CA</F5><F6>3969 E Guasti Rd
Ontario, CA</F6></Row>')
,(91903, 53, 782, N'<Row><F1>593</F1><F2>Mission Towers I</F2><F3>3975 Freedom Cir</F3><F4>Santa Clara</F4><F5>CA</F5><F6>Mission Towers I
3975 Freedom Cir
Santa Clara, CA</F6></Row>')
,(91904, 53, 783, N'<Row><F1>1167</F1><F3>3979 E Guasti Rd</F3><F4>Ontario</F4><F5>CA</F5><F6>3979 E Guasti Rd
Ontario, CA</F6></Row>')
,(91905, 53, 784, N'<Row><F1>594</F1><F2>Mission Towers II</F2><F3>3979-3985 Freedom Cir</F3><F4>Santa Clara</F4><F5>CA</F5><F6>Mission Towers II
3979-3985 Freedom Cir
Santa Clara, CA</F6></Row>')
,(91906, 53, 785, N'<Row><F1>595</F1><F3>40 Court St</F3><F4>Boston</F4><F5>MA</F5><F6>40 Court St
Boston, MA</F6></Row>')
,(91907, 53, 786, N'<Row><F1>596</F1><F3>400 5th Ave</F3><F4>Waltham</F4><F5>MA</F5><F6>400 5th Ave
Waltham, MA</F6></Row>')
,(91908, 53, 787, N'<Row><F1>1216</F1><F2>PGA National Resort &amp; Spa</F2><F3>400 Ave of the Champions</F3><F4>Palm Beach Gardens</F4><F5>FL</F5><F6>PGA National Resort &amp; Spa
400 Ave of the Champions
Palm Beach Gardens, FL</F6></Row>')
,(91909, 53, 788, N'<Row><F1>597</F1><F3>400 Continental Blvd</F3><F4>El Segundo</F4><F5>CA</F5><F6>400 Continental Blvd
El Segundo, CA</F6></Row>')
,(91910, 53, 789, N'<Row><F1>598</F1><F2>Momentum/Advantage BMW</F2><F3>400 Gulf Fwy S</F3><F4>League City</F4><F5>TX</F5><F6>Momentum/Advantage BMW
400 Gulf Fwy S
League City, TX</F6></Row>')
,(91911, 53, 790, N'<Row><F1>994</F1><F2>Rivergate Tower</F2><F3>400 N Ashley Dr</F3><F4>Tampa</F4><F5>FL</F5><F6>Rivergate Tower
400 N Ashley Dr
Tampa, FL</F6></Row>')
,(91912, 53, 791, N'<Row><F1>1111</F1><F3>4000 N Central Ave</F3><F4>Phoenix</F4><F5>AZ</F5><F6>4000 N Central Ave
Phoenix, AZ</F6></Row>')
,(91913, 53, 792, N'<Row><F1>599</F1><F2>Gurley-Leep Audi Mercedes VW Kia</F2><F3>4004 N Grape Rd</F3><F4>Mishawaka</F4><F5>IN</F5><F6>Gurley-Leep Audi Mercedes VW Kia
4004 N Grape Rd
Mishawaka, IN</F6></Row>')
,(91914, 53, 793, N'<Row><F1>600</F1><F2>Hall of States Building</F2><F3>400-444 N Capitol St NW</F3><F4>Washington</F4><F5>DC</F5><F6>Hall of States Building
400-444 N Capitol St NW
Washington, DC</F6></Row>')
,(91915, 53, 794, N'<Row><F1>601</F1><F2>Comfort Inn Downtown</F2><F3>401 17th St</F3><F4>Denver</F4><F5>CO</F5><F6>Comfort Inn Downtown
401 17th St
Denver, CO</F6></Row>')
,(91916, 53, 795, N'<Row><F1>602</F1><F2>Chuck E. Cheese''s</F2><F3>401 Louis Henna Blvd</F3><F4>Round Rock</F4><F5>TX</F5><F6>Chuck E. Cheese''s
401 Louis Henna Blvd
Round Rock, TX</F6></Row>')
,(91917, 53, 796, N'<Row><F1>603</F1><F2>Family Dollar</F2><F3>401 South Church Ave</F3><F4>Louisville</F4><F5>MS</F5><F6>Family Dollar
401 South Church Ave
Louisville, MS</F6></Row>')
,(91918, 53, 797, N'<Row><F1>604</F1><F3>401 W 14th St</F3><F4>New York</F4><F5>NY</F5><F6>401 W 14th St
New York, NY</F6></Row>')
,(91919, 53, 798, N'<Row><F1>605</F1><F3>4020 W 95th St</F3><F4>Oak Lawn</F4><F5>IL</F5><F6>4020 W 95th St
Oak Lawn, IL</F6></Row>')
,(91920, 53, 799, N'<Row><F1>1258</F1><F2>The Shops at Skyview Center</F2><F3>40-24 College Point Blvd</F3><F4>Flushing</F4><F5>NY</F5><F6>The Shops at Skyview Center
40-24 College Point Blvd
Flushing, NY</F6></Row>')
,(91921, 53, 800, N'<Row><F1>606</F1><F2>Don Massey Cadillac</F2><F3>40475 E Ann Arbor Rd</F3><F4>Plymouth</F4><F5>MI</F5><F6>Don Massey Cadillac
40475 E Ann Arbor Rd
Plymouth, MI</F6></Row>')
,(91922, 53, 801, N'<Row><F1>607</F1><F2>Carneros Inn</F2><F3>4048 Sonoma Hwy</F3><F4>Napa</F4><F5>CA</F5><F6>Carneros Inn
4048 Sonoma Hwy
Napa, CA</F6></Row>')
,(91923, 53, 802, N'<Row><F1>608</F1><F3>405 Howard St</F3><F4>San Francisco</F4><F5>CA</F5><F6>405 Howard St
San Francisco, CA</F6></Row>')
,(91924, 53, 803, N'<Row><F1>1241</F1><F3>40-50 Rio Robles</F3><F4>San Jose</F4><F5>CA</F5><F6>40-50 Rio Robles
San Jose, CA</F6></Row>')
,(91925, 53, 804, N'<Row><F1>609</F1><F2>Chuck E. Cheese''s</F2><F3>4059 Electric Rd</F3><F4>Roanoke</F4><F5>VA</F5><F6>Chuck E. Cheese''s
4059 Electric Rd
Roanoke, VA</F6></Row>')
,(91926, 53, 805, N'<Row><F1>610</F1><F3>407 W 7th St</F3><F4>San Pedro</F4><F5>CA</F5><F6>407 W 7th St
San Pedro, CA</F6></Row>')
,(91927, 53, 806, N'<Row><F1>611</F1><F2>Family Dollar</F2><F3>408 N Broad St</F3><F4>Mobile</F4><F5>AL</F5><F6>Family Dollar
408 N Broad St
Mobile, AL</F6></Row>')
,(91928, 53, 807, N'<Row><F1>998</F1><F2>Crystal River Plaza</F2><F3>408 N Suncoast Blvd</F3><F4>Crystal River</F4><F5>FL</F5><F6>Crystal River Plaza
408 N Suncoast Blvd
Crystal River, FL</F6></Row>')
,(91929, 53, 808, N'<Row><F1>612</F1><F3>4085 Campbell Ave</F3><F4>Menlo Park</F4><F5>CA</F5><F6>4085 Campbell Ave
Menlo Park, CA</F6></Row>')
,(91930, 53, 809, N'<Row><F1>1053</F1><F3>41 Perimeter Center E</F3><F4>Atlanta</F4><F5>GA</F5><F6>41 Perimeter Center E
Atlanta, GA</F6></Row>')
,(91931, 53, 810, N'<Row><F1>613</F1><F2>Claremont Hotel Club &amp; Spa</F2><F3>41 Tunnel Road</F3><F4>Berkeley</F4><F5>CA</F5><F6>Claremont Hotel Club &amp; Spa
41 Tunnel Road
Berkeley, CA</F6></Row>')
,(91932, 53, 811, N'<Row><F1>614</F1><F2>Family Dollar</F2><F3>410 W Forgey</F3><F4>Blooming Grove</F4><F5>TX</F5><F6>Family Dollar
410 W Forgey
Blooming Grove, TX</F6></Row>')
,(91933, 53, 812, N'<Row><F1>615</F1><F2>Merrill Place</F2><F3>411 1st Ave</F3><F4>Seattle</F4><F5>WA</F5><F6>Merrill Place
411 1st Ave
Seattle, WA</F6></Row>')
,(91934, 53, 813, N'<Row><F1>616</F1><F2>Bill Knight Lincoln Volvo</F2><F3>4111 S Memorial Dr</F3><F4>Tulsa</F4><F5>OK</F5><F6>Bill Knight Lincoln Volvo
4111 S Memorial Dr
Tulsa, OK</F6></Row>')
,(91935, 53, 814, N'<Row><F1>617</F1><F2>Ron Craft Chevrolet Cadillac/Baytown Ford</F2><F3>4114 Interstate 10 E</F3><F4>Baytown</F4><F5>TX</F5><F6>Ron Craft Chevrolet Cadillac/Baytown Ford
4114 Interstate 10 E
Baytown, TX</F6></Row>')
,(91936, 53, 815, N'<Row><F1>618</F1><F2>Baytown Honda</F2><F3>4141 Interstate 10 E</F3><F4>Baytown</F4><F5>TX</F5><F6>Baytown Honda
4141 Interstate 10 E
Baytown, TX</F6></Row>')
,(91937, 53, 816, N'<Row><F1>619</F1><F2>Audi of Fairfield</F2><F3>415-435 Commerce Dr</F3><F4>Fairfield</F4><F5>CT</F5><F6>Audi of Fairfield
415-435 Commerce Dr
Fairfield, CT</F6></Row>')
,(91938, 53, 817, N'<Row><F1>620</F1><F3>41-61 Daggett Dr</F3><F4>San Jose</F4><F5>CA</F5><F6>41-61 Daggett Dr
San Jose, CA</F6></Row>')
,(91939, 53, 818, N'<Row><F1>621</F1><F2>Szott M-59 Toyota Scion</F2><F3>4178 Highland Rd</F3><F4>Waterford Township</F4><F5>MI</F5><F6>Szott M-59 Toyota Scion
4178 Highland Rd
Waterford Township, MI</F6></Row>')
,(91940, 53, 819, N'<Row><F1>622</F1><F2>Family Dollar</F2><F3>42 S Dean Rd</F3><F4>Orlando</F4><F5>FL</F5><F6>Family Dollar
42 S Dean Rd
Orlando, FL</F6></Row>')
,(91941, 53, 820, N'<Row><F1>623</F1><F2>Family Dollar</F2><F3>420 Person St</F3><F4>Fayetteville</F4><F5>NC</F5><F6>Family Dollar
420 Person St
Fayetteville, NC</F6></Row>')
,(91942, 53, 821, N'<Row><F1>1145</F1><F2>Indian Hills Plaza</F2><F3>4208 E Blue Grass Rd</F3><F4>Mount Pleasant</F4><F5>MI</F5><F6>Indian Hills Plaza
4208 E Blue Grass Rd
Mount Pleasant, MI</F6></Row>')
,(91943, 53, 822, N'<Row><F1>624</F1><F2>Fifth Third Center</F2><F3>424 Church St</F3><F4>Nashville</F4><F5>TN</F5><F6>Fifth Third Center
424 Church St
Nashville, TN</F6></Row>')
,(91944, 53, 823, N'<Row><F1>625</F1><F2>Family Dollar</F2><F3>424 N West Ave</F3><F4>El Dorado</F4><F5>AR</F5><F6>Family Dollar
424 N West Ave
El Dorado, AR</F6></Row>')
,(91945, 53, 824, N'<Row><F1>626</F1><F2>Massey Cadillac</F2><F3>4241 N John Young Pkwy</F3><F4>Orlando</F4><F5>FL</F5><F6>Massey Cadillac
4241 N John Young Pkwy
Orlando, FL</F6></Row>')
,(91946, 53, 825, N'<Row><F1>627</F1><F2>Family Dollar</F2><F3>425 Gordon Ave</F3><F4>Bowling Green</F4><F5>KY</F5><F6>Family Dollar
425 Gordon Ave
Bowling Green, KY</F6></Row>')
,(91947, 53, 826, N'<Row><F1>1288</F1><F2>The Tuscan</F2><F3>425 North Point St</F3><F4>San Francisco</F4><F5>CA</F5><F6>The Tuscan
425 North Point St
San Francisco, CA</F6></Row>')
,(91948, 53, 827, N'<Row><F1>628</F1><F2>Rancho Las Palmas Shopping Center</F2><F3>42-540 Bob Hope Dr</F3><F4>Rancho Mirage</F4><F5>CA</F5><F6>Rancho Las Palmas Shopping Center
42-540 Bob Hope Dr
Rancho Mirage, CA</F6></Row>')
,(91949, 53, 828, N'<Row><F1>629</F1><F2>Stone Mountain Nissan</F2><F3>4275 U.S. 78</F3><F4>Lilburn</F4><F5>GA</F5><F6>Stone Mountain Nissan
4275 U.S. 78
Lilburn, GA</F6></Row>')
,(91950, 53, 829, N'<Row><F1>630</F1><F2>Family Dollar</F2><F3>4275 US Highway 68</F3><F4>Golden Valley</F4><F5>AZ</F5><F6>Family Dollar
4275 US Highway 68
Golden Valley, AZ</F6></Row>')
,(91951, 53, 830, N'<Row><F1>631</F1><F2>All American Chrysler Dodge Jeep</F2><F3>4310 Sherwood Way</F3><F4>San Angelo</F4><F5>TX</F5><F6>All American Chrysler Dodge Jeep
4310 Sherwood Way
San Angelo, TX</F6></Row>')
,(91952, 53, 831, N'<Row><F1>1026</F1><F3>431-451 Oxford St</F3><F4>London W1C</F4><F5>UK</F5><F6>431-451 Oxford St
London W1C, UK</F6></Row>')
,(91953, 53, 832, N'<Row><F1>632</F1><F2>San Angelo Parking Lot</F2><F3>4346 Sherwood Way</F3><F4>San Angelo</F4><F5>TX</F5><F6>San Angelo Parking Lot
4346 Sherwood Way
San Angelo, TX</F6></Row>')
,(91954, 53, 833, N'<Row><F1>633</F1><F2>McCluskey Used Cars &amp; Body Shop</F2><F3>435 E Galbraith</F3><F4>Cincinnati</F4><F5>OH</F5><F6>McCluskey Used Cars &amp; Body Shop
435 E Galbraith
Cincinnati, OH</F6></Row>')
,(91955, 53, 834, N'<Row><F1>634</F1><F2>North Hills </F2><F3>4351 The Cir at N Hills St</F3><F4>Raleigh</F4><F5>NC</F5><F6>North Hills 
4351 The Cir at N Hills St
Raleigh, NC</F6></Row>')
,(91956, 53, 835, N'<Row><F1>635</F1><F2>Hall Mazda</F2><F3>4372 Holland Rd</F3><F4>Virginia Beach</F4><F5>VA</F5><F6>Hall Mazda
4372 Holland Rd
Virginia Beach, VA</F6></Row>')
,(91957, 53, 836, N'<Row><F1>636</F1><F2>Chuck E. Cheese''s</F2><F3>438 Grand Canyon Dr</F3><F4>Madison</F4><F5>WI</F5><F6>Chuck E. Cheese''s
438 Grand Canyon Dr
Madison, WI</F6></Row>')
,(91958, 53, 837, N'<Row><F1>637</F1><F2>Family Dollar</F2><F3>4380 Moncrief Rd</F3><F4>Jacksonville</F4><F5>FL</F5><F6>Family Dollar
4380 Moncrief Rd
Jacksonville, FL</F6></Row>')
,(91959, 53, 838, N'<Row><F1>1260</F1><F2>Soho Beach House</F2><F3>4385 Collins Ave</F3><F4>Miami Beach</F4><F5>FL</F5><F6>Soho Beach House
4385 Collins Ave
Miami Beach, FL</F6></Row>')
,(91960, 53, 839, N'<Row><F1>638</F1><F2>Hyundai City</F2><F3>4395 U.S. 130</F3><F4>Burlington</F4><F5>NJ</F5><F6>Hyundai City
4395 U.S. 130
Burlington, NJ</F6></Row>')
,(91961, 53, 840, N'<Row><F1>639</F1><F2>Chrysler Dodge Jeep City</F2><F3>4395 U.S. 130</F3><F4>Burlington</F4><F5>NJ</F5><F6>Chrysler Dodge Jeep City
4395 U.S. 130
Burlington, NJ</F6></Row>')
,(91962, 53, 841, N'<Row><F1>640</F1><F2>Hotel Sofitel New York</F2><F3>44 W 44th St</F3><F4>New York</F4><F5>NY</F5><F6>Hotel Sofitel New York
44 W 44th St
New York, NY</F6></Row>')
,(91963, 53, 842, N'<Row><F1>641</F1><F3>440 S Church St</F3><F4>Charlotte</F4><F5>NC</F5><F6>440 S Church St
Charlotte, NC</F6></Row>')
,(91964, 53, 843, N'<Row><F1>642</F1><F2>Family Dollar</F2><F3>4405 W Aloha Dr</F3><F4>Diamond Head</F4><F5>MS</F5><F6>Family Dollar
4405 W Aloha Dr
Diamond Head, MS</F6></Row>')
,(91965, 53, 844, N'<Row><F1>643</F1><F2>Classic Stateline Kia</F2><F3>4411 State Line Ave</F3><F4>Texarkana</F4><F5>TX</F5><F6>Classic Stateline Kia
4411 State Line Ave
Texarkana, TX</F6></Row>')
,(91966, 53, 845, N'<Row><F1>644</F1><F2>Coggin BMW Treasure Coast</F2><F3>4429 S US Highway 1</F3><F4>Fort Pierce</F4><F5>FL</F5><F6>Coggin BMW Treasure Coast
4429 S US Highway 1
Fort Pierce, FL</F6></Row>')
,(91967, 53, 846, N'<Row><F1>645</F1><F3>4450 Jamboree Rd</F3><F4>Newport Beach</F4><F5>CA</F5><F6>4450 Jamboree Rd
Newport Beach, CA</F6></Row>')
,(91968, 53, 847, N'<Row><F1>1136</F1><F3>45 E Freedom Way</F3><F4>Cincinnati</F4><F5>OH</F5><F6>45 E Freedom Way
Cincinnati, OH</F6></Row>')
,(91969, 53, 848, N'<Row><F1>646</F1><F2>TownePlace Suites Harrisburg</F2><F3>450 Friendship Rd</F3><F4>Harrisburg</F4><F5>PA</F5><F6>TownePlace Suites Harrisburg
450 Friendship Rd
Harrisburg, PA</F6></Row>')
,(91970, 53, 849, N'<Row><F1>647</F1><F3>450 Park Ave</F3><F4>New York</F4><F5>NY</F5><F6>450 Park Ave
New York, NY</F6></Row>')
,(91971, 53, 850, N'<Row><F1>648</F1><F2>Mercedes-Benz of Fairfield Service/Warehouse</F2><F3>450 Scofield Ave</F3><F4>Fairfield</F4><F5>CT</F5><F6>Mercedes-Benz of Fairfield Service/Warehouse
450 Scofield Ave
Fairfield, CT</F6></Row>')
,(91972, 53, 851, N'<Row><F1>649</F1><F2>Seaport Center</F2><F3>451 D St</F3><F4>Boston</F4><F5>MA</F5><F6>Seaport Center
451 D St
Boston, MA</F6></Row>')
,(91973, 53, 852, N'<Row><F1>650</F1><F2>Family Dollar</F2><F3>451 S High St</F3><F4>Cortland</F4><F5>OH</F5><F6>Family Dollar
451 S High St
Cortland, OH</F6></Row>')
,(91974, 53, 853, N'<Row><F1>651</F1><F2>Family Dollar</F2><F3>4515 Avenue O</F3><F4>Fort Madison</F4><F5>IA</F5><F6>Family Dollar
4515 Avenue O
Fort Madison, IA</F6></Row>')
,(91975, 53, 854, N'<Row><F1>652</F1><F2>Ferrari Maserati of Washington</F2><F3>45235 Towlern Pl</F3><F4>Sterling</F4><F5>VA</F5><F6>Ferrari Maserati of Washington
45235 Towlern Pl
Sterling, VA</F6></Row>')
,(91976, 53, 855, N'<Row><F1>653</F1><F2>Family Dollar</F2><F3>4601 E Truman Rd</F3><F4>Kansas City</F4><F5>MO</F5><F6>Family Dollar
4601 E Truman Rd
Kansas City, MO</F6></Row>')
,(91977, 53, 856, N'<Row><F1>654</F1><F2>Family Dollar</F2><F3>4603 S 23rd St</F3><F4>Mcallen</F4><F5>TX</F5><F6>Family Dollar
4603 S 23rd St
Mcallen, TX</F6></Row>')
,(91978, 53, 857, N'<Row><F1>655</F1><F2>Pohanka Used Cars</F2><F3>4608 St Barnabas Rd</F3><F4>Marlow Heights</F4><F5>MD</F5><F6>Pohanka Used Cars
4608 St Barnabas Rd
Marlow Heights, MD</F6></Row>')
,(91979, 53, 858, N'<Row><F1>656</F1><F2>Family Dollar</F2><F3>466 Bypass Rd</F3><F4>Brandenburg</F4><F5>KY</F5><F6>Family Dollar
466 Bypass Rd
Brandenburg, KY</F6></Row>')
,(91980, 53, 859, N'<Row><F1>657</F1><F2>St. Johns Town Center</F2><F3>4663 River City Dr</F3><F4>Jacksonville</F4><F5>FL</F5><F6>St. Johns Town Center
4663 River City Dr
Jacksonville, FL</F6></Row>')
,(91981, 53, 860, N'<Row><F1>658</F1><F3>4675 Railhead Rd</F3><F4>Fort Worth</F4><F5>TX</F5><F6>4675 Railhead Rd
Fort Worth, TX</F6></Row>')
,(91982, 53, 861, N'<Row><F1>1054</F1><F3>47 Perimeter Center E</F3><F4>Atlanta</F4><F5>GA</F5><F6>47 Perimeter Center E
Atlanta, GA</F6></Row>')
,(91983, 53, 862, N'<Row><F1>659</F1><F3>470 Vanderbilt Ave</F3><F4>Brooklyn</F4><F5>NY</F5><F6>470 Vanderbilt Ave
Brooklyn, NY</F6></Row>')
,(91984, 53, 863, N'<Row><F1>660</F1><F2>Victory Buick/GMC</F2><F3>4702 N Navarro St</F3><F4>Victoria</F4><F5>TX</F5><F6>Victory Buick/GMC
4702 N Navarro St
Victoria, TX</F6></Row>')
,(91985, 53, 864, N'<Row><F1>661</F1><F2>Chuck E. Cheese''s</F2><F3>4703 W Loop 250 N</F3><F4>Midland</F4><F5>TX</F5><F6>Chuck E. Cheese''s
4703 W Loop 250 N
Midland, TX</F6></Row>')
,(91986, 53, 865, N'<Row><F1>662</F1><F2>Future Toyota Scion Gorham</F2><F3>471 Main St</F3><F4>Gorham</F4><F5>NH</F5><F6>Future Toyota Scion Gorham
471 Main St
Gorham, NH</F6></Row>')
,(91987, 53, 866, N'<Row><F1>663</F1><F2>Porsche of Fairfield</F2><F3>475 Commerce Dr</F3><F4>Fairfield</F4><F5>CT</F5><F6>Porsche of Fairfield
475 Commerce Dr
Fairfield, CT</F6></Row>')
,(91988, 53, 867, N'<Row><F1>664</F1><F2>Mercedes-Benz of San Diego</F2><F3>4750 Kearny Mesa Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>Mercedes-Benz of San Diego
4750 Kearny Mesa Rd
San Diego, CA</F6></Row>')
,(91989, 53, 868, N'<Row><F1>1251</F1><F3>47-53 Kensington High St</F3><F4>Kensington, London W8 5ED</F4><F5>UK</F5><F6>47-53 Kensington High St
Kensington, London W8 5ED, UK</F6></Row>')
,(91990, 53, 869, N'<Row><F1>665</F1><F2>Former Pohanka Body Shop</F2><F3>4756 Stamp Rd</F3><F4>Marlow Heights</F4><F5>MD</F5><F6>Former Pohanka Body Shop
4756 Stamp Rd
Marlow Heights, MD</F6></Row>')
,(91991, 53, 870, N'<Row><F1>1254</F1><F2>Riverworks</F2><F3>480 Pleasant St</F3><F4>Watertown</F4><F5>MA</F5><F6>Riverworks
480 Pleasant St
Watertown, MA</F6></Row>')
,(91992, 53, 871, N'<Row><F1>666</F1><F3>4800 Langdon Rd</F3><F4>Dallas</F4><F5>TX</F5><F6>4800 Langdon Rd
Dallas, TX</F6></Row>')
,(91993, 53, 872, N'<Row><F1>667</F1><F2>Toyota Pre-Owned of San Diego</F2><F3>4812 Kearny Mesa Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>Toyota Pre-Owned of San Diego
4812 Kearny Mesa Rd
San Diego, CA</F6></Row>')
,(91994, 53, 873, N'<Row><F1>668</F1><F2>Berlin City Ford Lincoln</F2><F3>485 Main St</F3><F4>Gorham</F4><F5>NH</F5><F6>Berlin City Ford Lincoln
485 Main St
Gorham, NH</F6></Row>')
,(91995, 53, 874, N'<Row><F1>669</F1><F3>4900 Langdon Rd</F3><F4>Dallas</F4><F5>TX</F5><F6>4900 Langdon Rd
Dallas, TX</F6></Row>')
,(91996, 53, 875, N'<Row><F1>670</F1><F2>Kearny Mesa Toyota</F2><F3>4910 Kearny Mesa Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>Kearny Mesa Toyota
4910 Kearny Mesa Rd
San Diego, CA</F6></Row>')
,(91997, 53, 876, N'<Row><F1>671</F1><F2>Lexus San Diego</F2><F3>4970 Kearny Mesa Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>Lexus San Diego
4970 Kearny Mesa Rd
San Diego, CA</F6></Row>')
,(91998, 53, 877, N'<Row><F1>672</F1><F2>Chuck E. Cheese''s</F2><F3>4992 N President George Bush Hwy</F3><F4>Garland</F4><F5>TX</F5><F6>Chuck E. Cheese''s
4992 N President George Bush Hwy
Garland, TX</F6></Row>')
,(91999, 53, 878, N'<Row><F1>673</F1><F2>Broadwalk House</F2><F3>5 Appold St</F3><F4>London</F4><F5>UK</F5><F6>Broadwalk House
5 Appold St
London, UK</F6></Row>')
,(92000, 53, 879, N'<Row><F1>674</F1><F3>5 Broadgate</F3><F4>London</F4><F5>UK</F5><F6>5 Broadgate
London, UK</F6></Row>')
,(92001, 53, 880, N'<Row><F1>675</F1><F2>Courtyard Philadelphia Langhorne</F2><F3>5 Industrial Blvd</F3><F4>Langhorne</F4><F5>PA</F5><F6>Courtyard Philadelphia Langhorne
5 Industrial Blvd
Langhorne, PA</F6></Row>')
,(92002, 53, 881, N'<Row><F1>676</F1><F2>Five Times Square</F2><F3>5 Times Sq</F3><F4>New York</F4><F5>NY</F5><F6>Five Times Square
5 Times Sq
New York, NY</F6></Row>')
,(92003, 53, 882, N'<Row><F1>677</F1><F2>Family Dollar</F2><F3>5 W Big Chino Rd</F3><F4>Paulden</F4><F5>AZ</F5><F6>Family Dollar
5 W Big Chino Rd
Paulden, AZ</F6></Row>')
,(92004, 53, 883, N'<Row><F1>1060</F1><F2>Hyatt Regency Coral Gables</F2><F3>50 Alhambra Plaza</F3><F4>Coral Gables</F4><F5>FL</F5><F6>Hyatt Regency Coral Gables
50 Alhambra Plaza
Coral Gables, FL</F6></Row>')
,(92005, 53, 884, N'<Row><F1>1017</F1><F3>50 F St NW</F3><F4>Washington</F4><F5>DC</F5><F6>50 F St NW
Washington, DC</F6></Row>')
,(92006, 53, 885, N'<Row><F1>678</F1><F3>50 Francisco St</F3><F4>San Francisco</F4><F5>CA</F5><F6>50 Francisco St
San Francisco, CA</F6></Row>')
,(92007, 53, 886, N'<Row><F1>980</F1><F3>50 Hampden Rd</F3><F4>Mansfield</F4><F5>MA</F5><F6>50 Hampden Rd
Mansfield, MA</F6></Row>')
,(92008, 53, 887, N'<Row><F1>987</F1><F3>50 Suffolk Rd</F3><F4>Mansfield</F4><F5>MA</F5><F6>50 Suffolk Rd
Mansfield, MA</F6></Row>')
,(92009, 53, 888, N'<Row><F1>679</F1><F2>Hotel Commonwealth</F2><F3>500 Commonwealth Ave</F3><F4>Boston</F4><F5>MA</F5><F6>Hotel Commonwealth
500 Commonwealth Ave
Boston, MA</F6></Row>')
,(92010, 53, 889, N'<Row><F1>680</F1><F2>Kelley Chevrolet</F2><F3>500 E State Blvd</F3><F4>Fort Wayne</F4><F5>IN</F5><F6>Kelley Chevrolet
500 E State Blvd
Fort Wayne, IN</F6></Row>')
,(92011, 53, 890, N'<Row><F1>681</F1><F2>Global BMW</F2><F3>500 Interstate N Pkwy</F3><F4>Atlanta</F4><F5>GA</F5><F6>Global BMW
500 Interstate N Pkwy
Atlanta, GA</F6></Row>')
,(92012, 53, 891, N'<Row><F1>1027</F1><F3>500 N Lake Shore Dr</F3><F4>Chicago</F4><F5>IL</F5><F6>500 N Lake Shore Dr
Chicago, IL</F6></Row>')
,(92013, 53, 892, N'<Row><F1>682</F1><F2>Chuck E. Cheese''s</F2><F3>500 Old Town Rd</F3><F4>Birmingham</F4><F5>AL</F5><F6>Chuck E. Cheese''s
500 Old Town Rd
Birmingham, AL</F6></Row>')
,(92014, 53, 893, N'<Row><F1>683</F1><F2>JW Marriott San Francisco</F2><F3>500 Post St</F3><F4>San Francisco</F4><F5>CA</F5><F6>JW Marriott San Francisco
500 Post St
San Francisco, CA</F6></Row>')
,(92015, 53, 894, N'<Row><F1>1209</F1><F2>Portsmouth Circle</F2><F3>500 Spaulding Tpk</F3><F4>Portsmouth</F4><F5>NH</F5><F6>Portsmouth Circle
500 Spaulding Tpk
Portsmouth, NH</F6></Row>')
,(92016, 53, 895, N'<Row><F1>684</F1><F2>MAG Excess Land</F2><F3>5000 Block of Venture Dr</F3><F4>Dublin</F4><F5>OH</F5><F6>MAG Excess Land
5000 Block of Venture Dr
Dublin, OH</F6></Row>')
,(92017, 53, 896, N'<Row><F1>685</F1><F3>5000 S Arizona Mills Cir</F3><F4>Tempe</F4><F5>AZ</F5><F6>5000 S Arizona Mills Cir
Tempe, AZ</F6></Row>')
,(92018, 53, 897, N'<Row><F1>686</F1><F2>Family Dollar</F2><F3>5000 Union Blvd</F3><F4>St Louis</F4><F5>MO</F5><F6>Family Dollar
5000 Union Blvd
St Louis, MO</F6></Row>')
,(92019, 53, 898, N'<Row><F1>687</F1><F2>Former City Chevrolet</F2><F3>5000 W Reno Ave</F3><F4>Oklahoma City</F4><F5>OK</F5><F6>Former City Chevrolet
5000 W Reno Ave
Oklahoma City, OK</F6></Row>')
,(92020, 53, 899, N'<Row><F1>688</F1><F2>BMW of Ann Arbor</F2><F3>501 Auto Mall Dr</F3><F4>Ann Arbor</F4><F5>MI</F5><F6>BMW of Ann Arbor
501 Auto Mall Dr
Ann Arbor, MI</F6></Row>')
,(92021, 53, 900, N'<Row><F1>689</F1><F2>Family Dollar</F2><F3>501 Tahoka Rd</F3><F4>Brownfield</F4><F5>TX</F5><F6>Family Dollar
501 Tahoka Rd
Brownfield, TX</F6></Row>')
,(92022, 53, 901, N'<Row><F1>690</F1><F2>Lexus Pre-Owned of San Diego</F2><F3>5010 Kearny Mesa Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>Lexus Pre-Owned of San Diego
5010 Kearny Mesa Rd
San Diego, CA</F6></Row>')
,(92023, 53, 902, N'<Row><F1>691</F1><F2>Family Dollar</F2><F3>5012 US Highway 92 W</F3><F4>Auburndale</F4><F5>FL</F5><F6>Family Dollar
5012 US Highway 92 W
Auburndale, FL</F6></Row>')
,(92024, 53, 903, N'<Row><F1>692</F1><F2>Chevrolet of Dublin</F2><F3>5016 Post Rd</F3><F4>Dublin</F4><F5>OH</F5><F6>Chevrolet of Dublin
5016 Post Rd
Dublin, OH</F6></Row>')
,(92025, 53, 904, N'<Row><F1>693</F1><F2>Chuck E. Cheese''s</F2><F3>5019 Jimmy Carter Blvd</F3><F4>Norcross</F4><F5>GA</F5><F6>Chuck E. Cheese''s
5019 Jimmy Carter Blvd
Norcross, GA</F6></Row>')
,(92026, 53, 905, N'<Row><F1>694</F1><F2>Family Dollar</F2><F3>5025 Nc Hwy 90 E</F3><F4>Hiddenite</F4><F5>NC</F5><F6>Family Dollar
5025 Nc Hwy 90 E
Hiddenite, NC</F6></Row>')
,(92027, 53, 906, N'<Row><F1>695</F1><F2>Midwestern Auto Group: Ferrari</F2><F3>5035 Post Rd</F3><F4>Dublin</F4><F5>OH</F5><F6>Midwestern Auto Group: Ferrari
5035 Post Rd
Dublin, OH</F6></Row>')
,(92028, 53, 907, N'<Row><F1>696</F1><F2>Former Saturn of Fort Wayne (Automall)</F2><F3>505 Avenue of Autos</F3><F4>Fort Wayne</F4><F5>IN</F5><F6>Former Saturn of Fort Wayne (Automall)
505 Avenue of Autos
Fort Wayne, IN</F6></Row>')
,(92029, 53, 908, N'<Row><F1>697</F1><F2>BMW Pre-Owned of San Diego</F2><F3>5050 Kearny Mesa Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>BMW Pre-Owned of San Diego
5050 Kearny Mesa Rd
San Diego, CA</F6></Row>')
,(92030, 53, 909, N'<Row><F1>698</F1><F2>BMW of San Diego</F2><F3>5090 Kearny Mesa Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>BMW of San Diego
5090 Kearny Mesa Rd
San Diego, CA</F6></Row>')
,(92031, 53, 910, N'<Row><F1>699</F1><F2>Hampton Inn &amp; Suites New Haven South West Haven</F2><F3>510 Saw Mill Rd</F3><F4>West Haven</F4><F5>CT</F5><F6>Hampton Inn &amp; Suites New Haven South West Haven
510 Saw Mill Rd
West Haven, CT</F6></Row>')
,(92032, 53, 911, N'<Row><F1>700</F1><F3>5101 Patrick Henry Dr</F3><F4>Santa Clara</F4><F5>CA</F5><F6>5101 Patrick Henry Dr
Santa Clara, CA</F6></Row>')
,(92033, 53, 912, N'<Row><F1>701</F1><F3>5104 Old Ironsides Dr</F3><F4>Santa Clara</F4><F5>CA</F5><F6>5104 Old Ironsides Dr
Santa Clara, CA</F6></Row>')
,(92034, 53, 913, N'<Row><F1>702</F1><F2>Chuck E. Cheese''s</F2><F3>511 N Randall Rd</F3><F4>Batavia</F4><F5>IL</F5><F6>Chuck E. Cheese''s
511 N Randall Rd
Batavia, IL</F6></Row>')
,(92035, 53, 914, N'<Row><F1>703</F1><F2>Lexus of Marin</F2><F3>513 E Francisco Blvd</F3><F4>San Rafael</F4><F5>CA</F5><F6>Lexus of Marin
513 E Francisco Blvd
San Rafael, CA</F6></Row>')
,(92036, 53, 915, N'<Row><F1>704</F1><F2>Infiniti of Ann Arbor</F2><F3>515 Auto Mall Dr</F3><F4>Ann Arbor</F4><F5>MI</F5><F6>Infiniti of Ann Arbor
515 Auto Mall Dr
Ann Arbor, MI</F6></Row>')
,(92037, 53, 916, N'<Row><F1>988</F1><F3>515 West St</F3><F4>Mansfield</F4><F5>MA</F5><F6>515 West St
Mansfield, MA</F6></Row>')
,(92038, 53, 917, N'<Row><F1>705</F1><F2>The Forum on Peachtree Parkway</F2><F3>5155 Peachtree Pkwy</F3><F4>Norcross</F4><F5>GA</F5><F6>The Forum on Peachtree Parkway
5155 Peachtree Pkwy
Norcross, GA</F6></Row>')
,(92039, 53, 918, N'<Row><F1>706</F1><F2>HW Marine - Bossier City</F2><F3>517 Benton Rd</F3><F4>Bossier City</F4><F5>LA</F5><F6>HW Marine - Bossier City
517 Benton Rd
Bossier City, LA</F6></Row>')
,(92040, 53, 919, N'<Row><F1>707</F1><F2>Pohanka Hyundai/Nissan</F2><F3>5200 Jefferson Davis Hwy</F3><F4>Fredericksburg</F4><F5>VA</F5><F6>Pohanka Hyundai/Nissan
5200 Jefferson Davis Hwy
Fredericksburg, VA</F6></Row>')
,(92041, 53, 920, N'<Row><F1>708</F1><F2>Mark Mitsubishi</F2><F3>5200 San Mateo Blvd NE</F3><F4>Albequerque</F4><F5>NM</F5><F6>Mark Mitsubishi
5200 San Mateo Blvd NE
Albequerque, NM</F6></Row>')
,(92042, 53, 921, N'<Row><F1>709</F1><F2>Sheehy Ford of Marlow Heights</F2><F3>5201 Auth Rd</F3><F4>Marlow Heights</F4><F5>MD</F5><F6>Sheehy Ford of Marlow Heights
5201 Auth Rd
Marlow Heights, MD</F6></Row>')
,(92043, 53, 922, N'<Row><F1>710</F1><F2>Family Dollar</F2><F3>521 Rodeo Rd</F3><F4>N Platte</F4><F5>NE</F5><F6>Family Dollar
521 Rodeo Rd
N Platte, NE</F6></Row>')
,(92044, 53, 923, N'<Row><F1>711</F1><F2>Gurley-Leep Nissan</F2><F3>5210 N Grape Rd</F3><F4>Mishawaka</F4><F5>IN</F5><F6>Gurley-Leep Nissan
5210 N Grape Rd
Mishawaka, IN</F6></Row>')
,(92045, 53, 924, N'<Row><F1>990</F1><F3>525 Campanelli Industrial Dr</F3><F4>Brockton</F4><F5>MA</F5><F6>525 Campanelli Industrial Dr
Brockton, MA</F6></Row>')
,(92046, 53, 925, N'<Row><F1>712</F1><F2>CityPlace Tower</F2><F3>525 Okeechobee Blvd</F3><F4>West Palm Beach</F4><F5>FL</F5><F6>CityPlace Tower
525 Okeechobee Blvd
West Palm Beach, FL</F6></Row>')
,(92047, 53, 926, N'<Row><F1>1028</F1><F3>525 W Van Buren St</F3><F4>Chicago</F4><F5>IL</F5><F6>525 W Van Buren St
Chicago, IL</F6></Row>')
,(92048, 53, 927, N'<Row><F1>713</F1><F2>Family Dollar</F2><F3>5260 S 3rd St</F3><F4>Memphis</F4><F5>TN</F5><F6>Family Dollar
5260 S 3rd St
Memphis, TN</F6></Row>')
,(92049, 53, 928, N'<Row><F1>1055</F1><F3>53 Perimeter Center E</F3><F4>Atlanta</F4><F5>GA</F5><F6>53 Perimeter Center E
Atlanta, GA</F6></Row>')
,(92050, 53, 929, N'<Row><F1>714</F1><F3>530 Broadway</F3><F4>New York</F4><F5>NY</F5><F6>530 Broadway
New York, NY</F6></Row>')
,(92051, 53, 930, N'<Row><F1>715</F1><F2>Hyatt House Bridgewater</F2><F3>530 U.S. 22</F3><F4>Bridgewater</F4><F5>NJ</F5><F6>Hyatt House Bridgewater
530 U.S. 22
Bridgewater, NJ</F6></Row>')
,(92052, 53, 931, N'<Row><F1>716</F1><F2>Gurley-Leep GM Hyundai Subaru</F2><F3>5302 N Grape Rd</F3><F4>Mishawaka</F4><F5>IN</F5><F6>Gurley-Leep GM Hyundai Subaru
5302 N Grape Rd
Mishawaka, IN</F6></Row>')
,(92053, 53, 932, N'<Row><F1>717</F1><F3>531 N Randall Rd</F3><F4>Batavia</F4><F5>IL</F5><F6>531 N Randall Rd
Batavia, IL</F6></Row>')
,(92054, 53, 933, N'<Row><F1>718</F1><F2>Jackson Auto Imports</F2><F3>5320 Interstate 55 N</F3><F4>Jackson</F4><F5>MS</F5><F6>Jackson Auto Imports
5320 Interstate 55 N
Jackson, MS</F6></Row>')
,(92055, 53, 934, N'<Row><F1>719</F1><F2>Family Dollar</F2><F3>5355 Elvis Presley Blvd</F3><F4>Memphis</F4><F5>TN</F5><F6>Family Dollar
5355 Elvis Presley Blvd
Memphis, TN</F6></Row>')
,(92056, 53, 935, N'<Row><F1>720</F1><F3>541 N Fairbanks Ct</F3><F4>Chicago</F4><F5>IL</F5><F6>541 N Fairbanks Ct
Chicago, IL</F6></Row>')
,(92057, 53, 936, N'<Row><F1>721</F1><F2>Family Dollar</F2><F3>5419 Saufley Field Rd</F3><F4>Pensacola</F4><F5>FL</F5><F6>Family Dollar
5419 Saufley Field Rd
Pensacola, FL</F6></Row>')
,(92058, 53, 937, N'<Row><F1>722</F1><F2>Princeville Center</F2><F3>5-4280 Kuhio Hwy</F3><F4>Princeville</F4><F5>HI</F5><F6>Princeville Center
5-4280 Kuhio Hwy
Princeville, HI</F6></Row>')
,(92059, 53, 938, N'<Row><F1>723</F1><F2>Berlin City Chevrolet GMC Buick</F2><F3>545 Main St</F3><F4>Gorham</F4><F5>NH</F5><F6>Berlin City Chevrolet GMC Buick
545 Main St
Gorham, NH</F6></Row>')
,(92060, 53, 939, N'<Row><F1>1168</F1><F3>5450 W Kiest Blvd</F3><F4>Dallas</F4><F5>TX</F5><F6>5450 W Kiest Blvd
Dallas, TX</F6></Row>')
,(92061, 53, 940, N'<Row><F1>724</F1><F3>55 Second St</F3><F4>San Francisco</F4><F5>CA</F5><F6>55 Second St
San Francisco, CA</F6></Row>')
,(92062, 53, 941, N'<Row><F1>1129</F1><F2>Empire Stores</F2><F3>55 Water St</F3><F4>Brooklyn</F4><F5>NY</F5><F6>Empire Stores
55 Water St
Brooklyn, NY</F6></Row>')
,(92063, 53, 942, N'<Row><F1>725</F1><F2>Palm Beach Toyota</F2><F3>551 S Millitary Trl</F3><F4>West Palm Beach</F4><F5>FL</F5><F6>Palm Beach Toyota
551 S Millitary Trl
West Palm Beach, FL</F6></Row>')
,(92064, 53, 943, N'<Row><F1>1050</F1><F3>5510 Lincoln Blvd</F3><F4>Playa Vista</F4><F5>CA</F5><F6>5510 Lincoln Blvd
Playa Vista, CA</F6></Row>')
,(92065, 53, 944, N'<Row><F1>726</F1><F3>5532 S Harlem Ave</F3><F4>Summit</F4><F5>IL</F5><F6>5532 S Harlem Ave
Summit, IL</F6></Row>')
,(92066, 53, 945, N'<Row><F1>727</F1><F3>555 12th St NW</F3><F4>Washington</F4><F5>DC</F5><F6>555 12th St NW
Washington, DC</F6></Row>')
,(92067, 53, 946, N'<Row><F1>1051</F1><F3>5570 Lincoln Blvd</F3><F4>Playa Vista</F4><F5>CA</F5><F6>5570 Lincoln Blvd
Playa Vista, CA</F6></Row>')
,(92068, 53, 947, N'<Row><F1>1255</F1><F3>560 Davis St</F3><F4>San Francisco</F4><F5>CA</F5><F6>560 Davis St
San Francisco, CA</F6></Row>')
,(92069, 53, 948, N'<Row><F1>728</F1><F2>Pensacola Honda</F2><F3>5600 Pensacola Blvd</F3><F4>Pensacola</F4><F5>FL</F5><F6>Pensacola Honda
5600 Pensacola Blvd
Pensacola, FL</F6></Row>')
,(92070, 53, 949, N'<Row><F1>729</F1><F2>Ken Garff Richmond Toyota</F2><F3>5601 National Rd E</F3><F4>Richmond</F4><F5>IN</F5><F6>Ken Garff Richmond Toyota
5601 National Rd E
Richmond, IN</F6></Row>')
,(92071, 53, 950, N'<Row><F1>730</F1><F2>Chuck E. Cheese''s</F2><F3>5612 Durand Ave</F3><F4>Racine</F4><F5>WI</F5><F6>Chuck E. Cheese''s
5612 Durand Ave
Racine, WI</F6></Row>')
,(92072, 53, 951, N'<Row><F1>731</F1><F3>5616 Bay St</F3><F4>Emeryville</F4><F5>CA</F5><F6>5616 Bay St
Emeryville, CA</F6></Row>')
,(92073, 53, 952, N'<Row><F1>732</F1><F2>Family Dollar</F2><F3>567 Cobb Pkwy N</F3><F4>Marietta</F4><F5>GA</F5><F6>Family Dollar
567 Cobb Pkwy N
Marietta, GA</F6></Row>')
,(92074, 53, 953, N'<Row><F1>733</F1><F2>Mercedes-Benz of Ann Arbor</F2><F3>570 Auto Mall Dr</F3><F4>Ann Arbor</F4><F5>MI</F5><F6>Mercedes-Benz of Ann Arbor
570 Auto Mall Dr
Ann Arbor, MI</F6></Row>')
,(92075, 53, 954, N'<Row><F1>989</F1><F3>574 West St</F3><F4>Mansfield</F4><F5>MA</F5><F6>574 West St
Mansfield, MA</F6></Row>')
,(92076, 53, 955, N'<Row><F1>734</F1><F3>575 Washington Blvd</F3><F4>Jersey City</F4><F5>NJ</F5><F6>575 Washington Blvd
Jersey City, NJ</F6></Row>')
,(92077, 53, 956, N'<Row><F1>735</F1><F2>Honda of Westminster</F2><F3>580 Baltimore Blvd</F3><F4>Westminster</F4><F5>MD</F5><F6>Honda of Westminster
580 Baltimore Blvd
Westminster, MD</F6></Row>')
,(92078, 53, 957, N'<Row><F1>736</F1><F2>Midwestern Auto Group: BMW</F2><F3>5825 Venture Dr</F3><F4>Dublin</F4><F5>OH</F5><F6>Midwestern Auto Group: BMW
5825 Venture Dr
Dublin, OH</F6></Row>')
,(92079, 53, 958, N'<Row><F1>737</F1><F2>Chrysler Dodge Jeep of Renton</F2><F3>585 Rainier Ave S</F3><F4>Renton</F4><F5>WA</F5><F6>Chrysler Dodge Jeep of Renton
585 Rainier Ave S
Renton, WA</F6></Row>')
,(92080, 53, 959, N'<Row><F1>738</F1><F2>Berlin City Kia</F2><F3>586 Marshall Ave</F3><F4>Williston</F4><F5>VT</F5><F6>Berlin City Kia
586 Marshall Ave
Williston, VT</F6></Row>')
,(92081, 53, 960, N'<Row><F1>1005</F1><F2>Lantana West Plaza</F2><F3>5891 S Military Trail</F3><F4>Lake Worth</F4><F5>FL</F5><F6>Lantana West Plaza
5891 S Military Trail
Lake Worth, FL</F6></Row>')
,(92082, 53, 961, N'<Row><F1>739</F1><F2>Pensacola Honda Parking Lot</F2><F3>5900 N Old Palafox Hwy</F3><F4>Pensacola</F4><F5>FL</F5><F6>Pensacola Honda Parking Lot
5900 N Old Palafox Hwy
Pensacola, FL</F6></Row>')
,(92083, 53, 962, N'<Row><F1>740</F1><F2>Palisades - Building A</F2><F3>5901 Peachtree Dunwoody Rd NE</F3><F4>Atlanta</F4><F5>GA</F5><F6>Palisades - Building A
5901 Peachtree Dunwoody Rd NE
Atlanta, GA</F6></Row>')
,(92084, 53, 963, N'<Row><F1>741</F1><F2>Palisades - Building B</F2><F3>5901 Peachtree Dunwoody Rd NE</F3><F4>Atlanta</F4><F5>GA</F5><F6>Palisades - Building B
5901 Peachtree Dunwoody Rd NE
Atlanta, GA</F6></Row>')
,(92085, 53, 964, N'<Row><F1>742</F1><F2>Palisades - Building C</F2><F3>5901 Peachtree Dunwoody Rd NE</F3><F4>Atlanta</F4><F5>GA</F5><F6>Palisades - Building C
5901 Peachtree Dunwoody Rd NE
Atlanta, GA</F6></Row>')
,(92086, 53, 965, N'<Row><F1>743</F1><F2>Capitol Cadillac</F2><F3>5901 S Pennsylvania Ave</F3><F4>Lansing</F4><F5>MI</F5><F6>Capitol Cadillac
5901 S Pennsylvania Ave
Lansing, MI</F6></Row>')
,(92087, 53, 966, N'<Row><F1>744</F1><F2>Palisades - Building D</F2><F3>5909 Peachtree Dunwoody Rd NE</F3><F4>Atlanta</F4><F5>GA</F5><F6>Palisades - Building D
5909 Peachtree Dunwoody Rd NE
Atlanta, GA</F6></Row>')
,(92088, 53, 967, N'<Row><F1>1069</F1><F2>SpringHill Suites Centreville Chantilly</F2><F3>5920 Trinity Pkwy</F3><F4>Centreville</F4><F5>VA</F5><F6>SpringHill Suites Centreville Chantilly
5920 Trinity Pkwy
Centreville, VA</F6></Row>')
,(92089, 53, 968, N'<Row><F1>745</F1><F3>60 Acorn Park Dr</F3><F4>Cambridge</F4><F5>MA</F5><F6>60 Acorn Park Dr
Cambridge, MA</F6></Row>')
,(92090, 53, 969, N'<Row><F1>1172</F1><F2>Mall of America</F2><F3>60 E Broadway</F3><F4>Bloomington</F4><F5>MN</F5><F6>Mall of America
60 E Broadway
Bloomington, MN</F6></Row>')
,(92091, 53, 970, N'<Row><F1>991</F1><F3>60 Forbes Blvd</F3><F4>Mansfield</F4><F5>MA</F5><F6>60 Forbes Blvd
Mansfield, MA</F6></Row>')
,(92092, 53, 971, N'<Row><F1>746</F1><F3>60 Francisco St</F3><F4>San Francisco</F4><F5>CA</F5><F6>60 Francisco St
San Francisco, CA</F6></Row>')
,(92093, 53, 972, N'<Row><F1>747</F1><F3>60 Hickory Dr</F3><F4>Waltham</F4><F5>MA</F5><F6>60 Hickory Dr
Waltham, MA</F6></Row>')
,(92094, 53, 973, N'<Row><F1>748</F1><F3>60 State St</F3><F4>Boston</F4><F5>MA</F5><F6>60 State St
Boston, MA</F6></Row>')
,(92095, 53, 974, N'<Row><F1>749</F1><F3>600 California St</F3><F4>San Francisco</F4><F5>CA</F5><F6>600 California St
San Francisco, CA</F6></Row>')
,(92096, 53, 975, N'<Row><F1>750</F1><F3>600 Clipper Dr</F3><F4>Belmont</F4><F5>CA</F5><F6>600 Clipper Dr
Belmont, CA</F6></Row>')
,(92097, 53, 976, N'<Row><F1>751</F1><F2>Lithia BMW/Nissan</F2><F3>600 N Central Ave</F3><F4>Medford</F4><F5>OR</F5><F6>Lithia BMW/Nissan
600 N Central Ave
Medford, OR</F6></Row>')
,(92098, 53, 977, N'<Row><F1>1141</F1><F2>Hilton Orlando</F2><F3>6001 Destination Pkwy</F3><F4>Orlando</F4><F5>FL</F5><F6>Hilton Orlando
6001 Destination Pkwy
Orlando, FL</F6></Row>')
,(92099, 53, 978, N'<Row><F1>752</F1><F2>Victoria Nissan</F2><F3>6003 N Navarro St</F3><F4>Victoria</F4><F5>TX</F5><F6>Victoria Nissan
6003 N Navarro St
Victoria, TX</F6></Row>')
,(92100, 53, 979, N'<Row><F1>753</F1><F3>601 Lexington Ave</F3><F4>New York</F4><F5>NY</F5><F6>601 Lexington Ave
New York, NY</F6></Row>')
,(92101, 53, 980, N'<Row><F1>754</F1><F2>Family Dollar</F2><F3>602 N St Marys St</F3><F4>Falfurrias</F4><F5>TX</F5><F6>Family Dollar
602 N St Marys St
Falfurrias, TX</F6></Row>')
,(92102, 53, 981, N'<Row><F1>755</F1><F2>Howard Hughes Center </F2><F3>6060 Center Dr</F3><F4>Los Angeles</F4><F5>CA</F5><F6>Howard Hughes Center 
6060 Center Dr
Los Angeles, CA</F6></Row>')
,(92103, 53, 982, N'<Row><F1>756</F1><F2>Chuck E. Cheese''s</F2><F3>6065 Youngerman Cir</F3><F4>Jacksonville</F4><F5>FL</F5><F6>Chuck E. Cheese''s
6065 Youngerman Cir
Jacksonville, FL</F6></Row>')
,(92104, 53, 983, N'<Row><F1>757</F1><F2>Family Dollar</F2><F3>607 E Moody Blvd</F3><F4>Bunnell</F4><F5>FL</F5><F6>Family Dollar
607 E Moody Blvd
Bunnell, FL</F6></Row>')
,(92105, 53, 984, N'<Row><F1>758</F1><F2>Howard Hughes Center </F2><F3>6080 Center Dr</F3><F4>Los Angeles</F4><F5>CA</F5><F6>Howard Hughes Center 
6080 Center Dr
Los Angeles, CA</F6></Row>')
,(92106, 53, 985, N'<Row><F1>1098</F1><F2>Bronx Terminal Market</F2><F3>610 Gateway Center Blvd</F3><F4>Bronx</F4><F5>NY</F5><F6>Bronx Terminal Market
610 Gateway Center Blvd
Bronx, NY</F6></Row>')
,(92107, 53, 986, N'<Row><F1>759</F1><F2>Holiday Inn Express Hershey Harrisburg Area</F2><F3>610 Walton Ave</F3><F4>Hummelstown</F4><F5>PA</F5><F6>Holiday Inn Express Hershey Harrisburg Area
610 Walton Ave
Hummelstown, PA</F6></Row>')
,(92108, 53, 987, N'<Row><F1>760</F1><F2>Howard Hughes Center </F2><F3>6100 Center Dr</F3><F4>Los Angeles</F4><F5>CA</F5><F6>Howard Hughes Center 
6100 Center Dr
Los Angeles, CA</F6></Row>')
,(92109, 53, 988, N'<Row><F1>761</F1><F2>Ferrari of Houston</F2><F3>6100 SW Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Ferrari of Houston
6100 SW Fwy
Houston, TX</F6></Row>')
,(92110, 53, 989, N'<Row><F1>762</F1><F3>611 E Golf Rd</F3><F4>Schaumburg</F4><F5>IL</F5><F6>611 E Golf Rd
Schaumburg, IL</F6></Row>')
,(92111, 53, 990, N'<Row><F1>1159</F1><F3>611 Reyes Dr</F3><F4>Walnut</F4><F5>CA</F5><F6>611 Reyes Dr
Walnut, CA</F6></Row>')
,(92112, 53, 991, N'<Row><F1>763</F1><F2>Park Place Mercedes-Benz</F2><F3>6113 Lemmon Ave</F3><F4>Dallas</F4><F5>TX</F5><F6>Park Place Mercedes-Benz
6113 Lemmon Ave
Dallas, TX</F6></Row>')
,(92113, 53, 992, N'<Row><F1>764</F1><F2>The Shops at Willow Bend</F2><F3>6121 W Park Blvd</F3><F4>Plano</F4><F5>TX</F5><F6>The Shops at Willow Bend
6121 W Park Blvd
Plano, TX</F6></Row>')
,(92114, 53, 993, N'<Row><F1>765</F1><F2>Family Dollar</F2><F3>613 Rifle Range Rd S</F3><F4>Winter Haven</F4><F5>FL</F5><F6>Family Dollar
613 Rifle Range Rd S
Winter Haven, FL</F6></Row>')
,(92115, 53, 994, N'<Row><F1>1135</F1><F2>Gallery Place</F2><F3>616 H St NW</F3><F4>Washington</F4><F5>DC</F5><F6>Gallery Place
616 H St NW
Washington, DC</F6></Row>')
,(92116, 53, 995, N'<Row><F1>1220</F1><F2>Grand Connaught Rooms</F2><F3>61-63 Great Queen St</F3><F4>London WC2B 5DA</F4><F5>UK</F5><F6>Grand Connaught Rooms
61-63 Great Queen St
London WC2B 5DA, UK</F6></Row>')
,(92117, 53, 996, N'<Row><F1>766</F1><F2>Rosenthal Acura/Mazda</F2><F3>619-625 N Frederick Ave</F3><F4>Gaithersburg</F4><F5>MD</F5><F6>Rosenthal Acura/Mazda
619-625 N Frederick Ave
Gaithersburg, MD</F6></Row>')
,(92118, 53, 997, N'<Row><F1>767</F1><F2>Family Dollar</F2><F3>621 South Pearson Rd</F3><F4>Pearl</F4><F5>MS</F5><F6>Family Dollar
621 South Pearson Rd
Pearl, MS</F6></Row>')
,(92119, 53, 998, N'<Row><F1>1010</F1><F2>Poplar Towers</F2><F3>6263 Poplar Ave</F3><F4>Memphis</F4><F5>TN</F5><F6>Poplar Towers
6263 Poplar Ave
Memphis, TN</F6></Row>')
,(92120, 53, 999, N'<Row><F1>768</F1><F3>627 Greenwich St</F3><F4>New York</F4><F5>NY</F5><F6>627 Greenwich St
New York, NY</F6></Row>')
,(92121, 53, 1000, N'<Row><F1>769</F1><F2>Family Dollar</F2><F3>6290 W Third St</F3><F4>Dayton</F4><F5>OH</F5><F6>Family Dollar
6290 W Third St
Dayton, OH</F6></Row>')
,(92122, 53, 1001, N'<Row><F1>770</F1><F2>Landers Auto Body</F2><F3>6301 S University Ave</F3><F4>Little Rock</F4><F5>AR</F5><F6>Landers Auto Body
6301 S University Ave
Little Rock, AR</F6></Row>')
,(92123, 53, 1002, N'<Row><F1>771</F1><F2>ADESA Nashville</F2><F3>631 Burnett Rd</F3><F4>Nashville</F4><F5>TN</F5><F6>ADESA Nashville
631 Burnett Rd
Nashville, TN</F6></Row>')
,(92124, 53, 1003, N'<Row><F1>1239</F1><F2>The Promenade Bolingbrook</F2><F3>631 E Boughton Rd</F3><F4>Bolingbrook</F4><F5>IL</F5><F6>The Promenade Bolingbrook
631 E Boughton Rd
Bolingbrook, IL</F6></Row>')
,(92125, 53, 1004, N'<Row><F1>772</F1><F2>Tom Kelley Buick-GMC (Automall)</F2><F3>633 Avenue of Autos</F3><F4>Fort Wayne</F4><F5>IN</F5><F6>Tom Kelley Buick-GMC (Automall)
633 Avenue of Autos
Fort Wayne, IN</F6></Row>')
,(92126, 53, 1005, N'<Row><F1>773</F1><F2>Midwestern Auto Group</F2><F3>6335 Perimeter Loop Rd</F3><F4>Dublin</F4><F5>OH</F5><F6>Midwestern Auto Group
6335 Perimeter Loop Rd
Dublin, OH</F6></Row>')
,(92127, 53, 1006, N'<Row><F1>774</F1><F2>Family Dollar</F2><F3>6480 Winchester Pike</F3><F4>Canal Winchester</F4><F5>OH</F5><F6>Family Dollar
6480 Winchester Pike
Canal Winchester, OH</F6></Row>')
,(92128, 53, 1007, N'<Row><F1>775</F1><F2>Bedford Collision Center</F2><F3>65 Broadway</F3><F4>Bedford</F4><F5>OH</F5><F6>Bedford Collision Center
65 Broadway
Bedford, OH</F6></Row>')
,(92129, 53, 1008, N'<Row><F1>776</F1><F2>Park Avenue Tower</F2><F3>65 E 55th St</F3><F4>New York</F4><F5>NY</F5><F6>Park Avenue Tower
65 E 55th St
New York, NY</F6></Row>')
,(92130, 53, 1009, N'<Row><F1>1206</F1><F2>Southwind Plaza</F2><F3>65 Independence Dr</F3><F4>Hyannis</F4><F5>MA</F5><F6>Southwind Plaza
65 Independence Dr
Hyannis, MA</F6></Row>')
,(92131, 53, 1010, N'<Row><F1>777</F1><F3>65 S McClintock Dr</F3><F4>Tempe</F4><F5>AZ</F5><F6>65 S McClintock Dr
Tempe, AZ</F6></Row>')
,(92132, 53, 1011, N'<Row><F1>778</F1><F3>650 California St</F3><F4>San Francisco</F4><F5>CA</F5><F6>650 California St
San Francisco, CA</F6></Row>')
,(92133, 53, 1012, N'<Row><F1>1256</F1><F3>650 Davis St</F3><F4>San Francisco</F4><F5>CA</F5><F6>650 Davis St
San Francisco, CA</F6></Row>')
,(92134, 53, 1013, N'<Row><F1>779</F1><F2>River Place Corporate Center - Building I</F2><F3>6500 River Place Blvd</F3><F4>Austin</F4><F5>TX</F5><F6>River Place Corporate Center - Building I
6500 River Place Blvd
Austin, TX</F6></Row>')
,(92135, 53, 1014, N'<Row><F1>780</F1><F2>River Place Corporate Center - Building II</F2><F3>6500 River Place Blvd</F3><F4>Austin</F4><F5>TX</F5><F6>River Place Corporate Center - Building II
6500 River Place Blvd
Austin, TX</F6></Row>')
,(92136, 53, 1015, N'<Row><F1>781</F1><F2>River Place Corporate Center - Building III</F2><F3>6500 River Place Blvd</F3><F4>Austin</F4><F5>TX</F5><F6>River Place Corporate Center - Building III
6500 River Place Blvd
Austin, TX</F6></Row>')
,(92137, 53, 1016, N'<Row><F1>782</F1><F2>River Place Corporate Center - Building IV</F2><F3>6500 River Place Blvd</F3><F4>Austin</F4><F5>TX</F5><F6>River Place Corporate Center - Building IV
6500 River Place Blvd
Austin, TX</F6></Row>')
,(92138, 53, 1017, N'<Row><F1>783</F1><F2>River Place Corporate Center - Building V</F2><F3>6500 River Place Blvd</F3><F4>Austin</F4><F5>TX</F5><F6>River Place Corporate Center - Building V
6500 River Place Blvd
Austin, TX</F6></Row>')
,(92139, 53, 1018, N'<Row><F1>784</F1><F2>River Place Corporate Center - Building VI</F2><F3>6500 River Place Blvd</F3><F4>Austin</F4><F5>TX</F5><F6>River Place Corporate Center - Building VI
6500 River Place Blvd
Austin, TX</F6></Row>')
,(92140, 53, 1019, N'<Row><F1>785</F1><F2>River Place Corporate Center - Building VII</F2><F3>6500 River Place Blvd</F3><F4>Austin</F4><F5>TX</F5><F6>River Place Corporate Center - Building VII
6500 River Place Blvd
Austin, TX</F6></Row>')
,(92141, 53, 1020, N'<Row><F1>1230</F1><F2>St. John''s Hotel</F2><F3>651 Warwick Rd</F3><F4>Solihull, West Midlands B91 1AT</F4><F5>UK</F5><F6>St. John''s Hotel
651 Warwick Rd
Solihull, West Midlands B91 1AT, UK</F6></Row>')
,(92142, 53, 1021, N'<Row><F1>1170</F1><F3>6520 S 190th St</F3><F4>Kent</F4><F5>WA</F5><F6>6520 S 190th St
Kent, WA</F6></Row>')
,(92143, 53, 1022, N'<Row><F1>786</F1><F2>CarMax Oaklawn</F2><F3>6540 95th St</F3><F4>Oak Lawn</F4><F5>IL</F5><F6>CarMax Oaklawn
6540 95th St
Oak Lawn, IL</F6></Row>')
,(92144, 53, 1023, N'<Row><F1>787</F1><F2>Family Dollar</F2><F3>6541 Montgomery Rd</F3><F4>Cincinnati</F4><F5>OH</F5><F6>Family Dollar
6541 Montgomery Rd
Cincinnati, OH</F6></Row>')
,(92145, 53, 1024, N'<Row><F1>788</F1><F2>Pacific Place </F2><F3>660 Pine St</F3><F4>Seattle</F4><F5>WA</F5><F6>Pacific Place 
660 Pine St
Seattle, WA</F6></Row>')
,(92146, 53, 1025, N'<Row><F1>789</F1><F2>Howard Hughes Center </F2><F3>6601 Center Dr</F3><F4>Los Angeles</F4><F5>CA</F5><F6>Howard Hughes Center 
6601 Center Dr
Los Angeles, CA</F6></Row>')
,(92147, 53, 1026, N'<Row><F1>1287</F1><F2>Town Square Las Vegas</F2><F3>6605 S Las Vegas Blvd</F3><F4>Las Vegas</F4><F5>NV</F5><F6>Town Square Las Vegas
6605 S Las Vegas Blvd
Las Vegas, NV</F6></Row>')
,(92148, 53, 1027, N'<Row><F1>790</F1><F2>Family Dollar</F2><F3>6634 Bristol Hwy</F3><F4>Piney Flats</F4><F5>TN</F5><F6>Family Dollar
6634 Bristol Hwy
Piney Flats, TN</F6></Row>')
,(92149, 53, 1028, N'<Row><F1>1063</F1><F2>Embassy Suites Syracuse Carrier Circle</F2><F3>6646 Old Collamer Rd S</F3><F4>East Syracuse</F4><F5>NY</F5><F6>Embassy Suites Syracuse Carrier Circle
6646 Old Collamer Rd S
East Syracuse, NY</F6></Row>')
,(92150, 53, 1029, N'<Row><F1>1014</F1><F2>Two Rivers Center</F2><F3>668 N Riverside Dr</F3><F4>Clarksville</F4><F5>TN</F5><F6>Two Rivers Center
668 N Riverside Dr
Clarksville, TN</F6></Row>')
,(92151, 53, 1030, N'<Row><F1>791</F1><F2>Chuck E. Cheese''s</F2><F3>6700 Abercorn</F3><F4>Savannah</F4><F5>GA</F5><F6>Chuck E. Cheese''s
6700 Abercorn
Savannah, GA</F6></Row>')
,(92152, 53, 1031, N'<Row><F1>792</F1><F2>Howard Hughes Center </F2><F3>6701 Center Dr</F3><F4>Los Angeles</F4><F5>CA</F5><F6>Howard Hughes Center 
6701 Center Dr
Los Angeles, CA</F6></Row>')
,(92153, 53, 1032, N'<Row><F1>793</F1><F2>Germain Honda of Dublin</F2><F3>6715 Sawmill Rd</F3><F4>Dublin</F4><F5>OH</F5><F6>Germain Honda of Dublin
6715 Sawmill Rd
Dublin, OH</F6></Row>')
,(92154, 53, 1033, N'<Row><F1>794</F1><F2>Sheehy Ford-Nissan-Subaru of Springfield</F2><F3>6727 Loisdale Rd</F3><F4>Springfield</F4><F5>VA</F5><F6>Sheehy Ford-Nissan-Subaru of Springfield
6727 Loisdale Rd
Springfield, VA</F6></Row>')
,(92155, 53, 1034, N'<Row><F1>795</F1><F2>Lakewood Center</F2><F3>6801 N Capital of Texas Hwy</F3><F4>Austin</F4><F5>TX</F5><F6>Lakewood Center
6801 N Capital of Texas Hwy
Austin, TX</F6></Row>')
,(92156, 53, 1035, N'<Row><F1>796</F1><F2>Northlake Mall</F2><F3>6801 Northlake Mall Dr</F3><F4>Charlotte</F4><F5>NC</F5><F6>Northlake Mall
6801 Northlake Mall Dr
Charlotte, NC</F6></Row>')
,(92157, 53, 1036, N'<Row><F1>797</F1><F2>Chuck E. Cheese''s</F2><F3>6817 Northwest Expy</F3><F4>Oklahoma City</F4><F5>OK</F5><F6>Chuck E. Cheese''s
6817 Northwest Expy
Oklahoma City, OK</F6></Row>')
,(92158, 53, 1037, N'<Row><F1>798</F1><F2>Sport Hyundai Dodge</F2><F3>6831 Black Horse Pike</F3><F4>Egg Harbor Township</F4><F5>NJ</F5><F6>Sport Hyundai Dodge
6831 Black Horse Pike
Egg Harbor Township, NJ</F6></Row>')
,(92159, 53, 1038, N'<Row><F1>799</F1><F2>Momentum Toyota Scion of Tulsa</F2><F3>6868 E Broken Arrow Expy Frontage Rd</F3><F4>Tulsa</F4><F5>OK</F5><F6>Momentum Toyota Scion of Tulsa
6868 E Broken Arrow Expy Frontage Rd
Tulsa, OK</F6></Row>')
,(92160, 53, 1039, N'<Row><F1>800</F1><F2>Chuck E. Cheese''s</F2><F3>6874 Ingram Dr</F3><F4>San Antonio</F4><F5>TX</F5><F6>Chuck E. Cheese''s
6874 Ingram Dr
San Antonio, TX</F6></Row>')
,(92161, 53, 1040, N'<Row><F1>1292</F1><F2>Hialeah Rail Yard</F2><F3>6875 NW 58th St</F3><F4>Miami</F4><F5>FL</F5><F6>Hialeah Rail Yard
6875 NW 58th St
Miami, FL</F6></Row>')
,(92162, 53, 1041, N'<Row><F1>801</F1><F3>69 Hickory Dr</F3><F4>Waltham</F4><F5>MA</F5><F6>69 Hickory Dr
Waltham, MA</F6></Row>')
,(92163, 53, 1042, N'<Row><F1>1147</F1><F2>Kings'' Shops</F2><F3>69-250 Waikoloa Beach Dr</F3><F4>Waikoloa Village</F4><F5>HI</F5><F6>Kings'' Shops
69-250 Waikoloa Beach Dr
Waikoloa Village, HI</F6></Row>')
,(92164, 53, 1043, N'<Row><F1>802</F1><F2>Clay Cooley Fiat</F2><F3>698 E Airport Fwy</F3><F4>Irving</F4><F5>TX</F5><F6>Clay Cooley Fiat
698 E Airport Fwy
Irving, TX</F6></Row>')
,(92165, 53, 1044, N'<Row><F1>803</F1><F2>Riverwatch</F2><F3>70 Battery Pl</F3><F4>New York</F4><F5>NY</F5><F6>Riverwatch
70 Battery Pl
New York, NY</F6></Row>')
,(92166, 53, 1045, N'<Row><F1>1018</F1><F3>70 Park Ave</F3><F4>New York</F4><F5>NY</F5><F6>70 Park Ave
New York, NY</F6></Row>')
,(92167, 53, 1046, N'<Row><F1>1242</F1><F3>70 Rio Robles</F3><F4>San Jose</F4><F5>CA</F5><F6>70 Rio Robles
San Jose, CA</F6></Row>')
,(92168, 53, 1047, N'<Row><F1>804</F1><F2>Boston Harbor Hotel</F2><F3>70 Rowes Wharf</F3><F4>Boston</F4><F5>MA</F5><F6>Boston Harbor Hotel
70 Rowes Wharf
Boston, MA</F6></Row>')
,(92169, 53, 1048, N'<Row><F1>805</F1><F2>Three First National Plaza</F2><F3>70 West Madison St</F3><F4>Chicago</F4><F5>IL</F5><F6>Three First National Plaza
70 West Madison St
Chicago, IL</F6></Row>')
,(92170, 53, 1049, N'<Row><F1>806</F1><F3>700 13th St NW</F3><F4>Washington</F4><F5>DC</F5><F6>700 13th St NW
Washington, DC</F6></Row>')
,(92171, 53, 1050, N'<Row><F1>807</F1><F3>700 8th Ave</F3><F4>New York</F4><F5>NY</F5><F6>700 8th Ave
New York, NY</F6></Row>')
,(92172, 53, 1051, N'<Row><F1>1274</F1><F3>700 Concar Dr</F3><F4>San Mateo</F4><F5>CA</F5><F6>700 Concar Dr
San Mateo, CA</F6></Row>')
,(92173, 53, 1052, N'<Row><F1>808</F1><F2>Clay Cooley Chrysler Jeep Dodge Ram</F2><F3>700 E Airport Fwy</F3><F4>Irving</F4><F5>TX</F5><F6>Clay Cooley Chrysler Jeep Dodge Ram
700 E Airport Fwy
Irving, TX</F6></Row>')
,(92174, 53, 1053, N'<Row><F1>809</F1><F2>Jack Maxton Chevrolet</F2><F3>700 E Dublin Granville Rd</F3><F4>Columbus</F4><F5>OH</F5><F6>Jack Maxton Chevrolet
700 E Dublin Granville Rd
Columbus, OH</F6></Row>')
,(92175, 53, 1054, N'<Row><F1>810</F1><F3>700 E Middlefield Rd</F3><F4>Mountain View</F4><F5>CA</F5><F6>700 E Middlefield Rd
Mountain View, CA</F6></Row>')
,(92176, 53, 1055, N'<Row><F1>811</F1><F2>Lithia Honda</F2><F3>700 N Central</F3><F4>Medford</F4><F5>OR</F5><F6>Lithia Honda
700 N Central
Medford, OR</F6></Row>')
,(92177, 53, 1056, N'<Row><F1>812</F1><F2>Nix Auto Center</F2><F3>700 S George Nigh Expy</F3><F4>McAlester</F4><F5>OK</F5><F6>Nix Auto Center
700 S George Nigh Expy
McAlester, OK</F6></Row>')
,(92178, 53, 1057, N'<Row><F1>813</F1><F2>Family Dollar</F2><F3>700 Sutton St</F3><F4>Sonora</F4><F5>TX</F5><F6>Family Dollar
700 Sutton St
Sonora, TX</F6></Row>')
,(92179, 53, 1058, N'<Row><F1>1052</F1><F2>Anaheim Marriott</F2><F3>700 W Convention Way</F3><F4>Anaheim</F4><F5>CA</F5><F6>Anaheim Marriott
700 W Convention Way
Anaheim, CA</F6></Row>')
,(92180, 53, 1059, N'<Row><F1>814</F1><F3>701 Westchester Ave</F3><F4>White Plains</F4><F5>NY</F5><F6>701 Westchester Ave
White Plains, NY</F6></Row>')
,(92181, 53, 1060, N'<Row><F1>815</F1><F2>Land Rover of Central Houston</F2><F3>7019 Old Katy Rd</F3><F4>Houston</F4><F5>TX</F5><F6>Land Rover of Central Houston
7019 Old Katy Rd
Houston, TX</F6></Row>')
,(92182, 53, 1061, N'<Row><F1>1029</F1><F2>705 Union Station</F2><F3>705 5th Ave S</F3><F4>Seattle</F4><F5>WA</F5><F6>705 Union Station
705 5th Ave S
Seattle, WA</F6></Row>')
,(92183, 53, 1062, N'<Row><F1>816</F1><F2>Mercedes-Benz of Fresno</F2><F3>7055 N Palm Ave</F3><F4>Fresno</F4><F5>CA</F5><F6>Mercedes-Benz of Fresno
7055 N Palm Ave
Fresno, CA</F6></Row>')
,(92184, 53, 1063, N'<Row><F1>817</F1><F3>707 Westchester Ave</F3><F4>White Plains</F4><F5>NY</F5><F6>707 Westchester Ave
White Plains, NY</F6></Row>')
,(92185, 53, 1064, N'<Row><F1>818</F1><F2>Fort Myers Parking Lot</F2><F3>7081 Pettys Way</F3><F4>Fort Myers</F4><F5>FL</F5><F6>Fort Myers Parking Lot
7081 Pettys Way
Fort Myers, FL</F6></Row>')
,(92186, 53, 1065, N'<Row><F1>819</F1><F3>709 Westchester Ave</F3><F4>White Plains</F4><F5>NY</F5><F6>709 Westchester Ave
White Plains, NY</F6></Row>')
,(92187, 53, 1066, N'<Row><F1>981</F1><F3>71 Hampden Rd</F3><F4>Mansfield</F4><F5>MA</F5><F6>71 Hampden Rd
Mansfield, MA</F6></Row>')
,(92188, 53, 1067, N'<Row><F1>820</F1><F2>Mid City Place</F2><F3>71 High Holborn</F3><F4>London</F4><F5>UK</F5><F6>Mid City Place
71 High Holborn
London, UK</F6></Row>')
,(92189, 53, 1068, N'<Row><F1>821</F1><F2>North Hills Toyota Service</F2><F3>711 Browns Ln</F3><F4>Pittsburgh</F4><F5>PA</F5><F6>North Hills Toyota Service
711 Browns Ln
Pittsburgh, PA</F6></Row>')
,(92190, 53, 1069, N'<Row><F1>822</F1><F2>Chuck E. Cheese''s</F2><F3>711 Coliseum Blvd W</F3><F4>Ft Wayne</F4><F5>IN</F5><F6>Chuck E. Cheese''s
711 Coliseum Blvd W
Ft Wayne, IN</F6></Row>')
,(92191, 53, 1070, N'<Row><F1>823</F1><F2>Capitol Chevrolet/Capitol BMW</F2><F3>711 Eastern Blvd</F3><F4>Montgomery</F4><F5>AL</F5><F6>Capitol Chevrolet/Capitol BMW
711 Eastern Blvd
Montgomery, AL</F6></Row>')
,(92192, 53, 1071, N'<Row><F1>824</F1><F2>Lexus of Edison</F2><F3>711 U.S. 1</F3><F4>Edison</F4><F5>NJ</F5><F6>Lexus of Edison
711 U.S. 1
Edison, NJ</F6></Row>')
,(92193, 53, 1072, N'<Row><F1>825</F1><F3>711 Westchester Ave</F3><F4>White Plains</F4><F5>NY</F5><F6>711 Westchester Ave
White Plains, NY</F6></Row>')
,(92194, 53, 1073, N'<Row><F1>826</F1><F2>14/69 Carwash (Automall)</F2><F3>714 Avenue of Autos</F3><F4>Fort Wayne</F4><F5>IN</F5><F6>14/69 Carwash (Automall)
714 Avenue of Autos
Fort Wayne, IN</F6></Row>')
,(92195, 53, 1074, N'<Row><F1>827</F1><F2>Family Dollar</F2><F3>715 Broadway</F3><F4>Rockford</F4><F5>IL</F5><F6>Family Dollar
715 Broadway
Rockford, IL</F6></Row>')
,(92196, 53, 1075, N'<Row><F1>1003</F1><F2>Oakbrook Plaza</F2><F3>7157 W Oakland Park Blvd</F3><F4>Lauderhill</F4><F5>FL</F5><F6>Oakbrook Plaza
7157 W Oakland Park Blvd
Lauderhill, FL</F6></Row>')
,(92197, 53, 1076, N'<Row><F1>828</F1><F2>Chuck E. Cheese''s</F2><F3>7178 DeSoto Cove</F3><F4>Horn Lake</F4><F5>MS</F5><F6>Chuck E. Cheese''s
7178 DeSoto Cove
Horn Lake, MS</F6></Row>')
,(92198, 53, 1077, N'<Row><F1>829</F1><F3>71-81 Daggett Dr</F3><F4>San Jose</F4><F5>CA</F5><F6>71-81 Daggett Dr
San Jose, CA</F6></Row>')
,(92199, 53, 1078, N'<Row><F1>830</F1><F3>720 3rd Ave</F3><F4>Seattle</F4><F5>WA</F5><F6>720 3rd Ave
Seattle, WA</F6></Row>')
,(92200, 53, 1079, N'<Row><F1>831</F1><F3>720 Olive Way</F3><F4>Seattle</F4><F5>WA</F5><F6>720 Olive Way
Seattle, WA</F6></Row>')
,(92201, 53, 1080, N'<Row><F1>832</F1><F2>ABC St. Louis</F2><F3>721 S 45th St</F3><F4>Centreville</F4><F5>IL</F5><F6>ABC St. Louis
721 S 45th St
Centreville, IL</F6></Row>')
,(92202, 53, 1081, N'<Row><F1>833</F1><F2>Coggin Buick-GMC &amp; Collision Center</F2><F3>7245 Blanding Blvd</F3><F4>Jacksonville</F4><F5>FL</F5><F6>Coggin Buick-GMC &amp; Collision Center
7245 Blanding Blvd
Jacksonville, FL</F6></Row>')
,(92203, 53, 1082, N'<Row><F1>834</F1><F2>Coggin Used Car Lot</F2><F3>7245 Blanding Blvd</F3><F4>Jacksonville</F4><F5>FL</F5><F6>Coggin Used Car Lot
7245 Blanding Blvd
Jacksonville, FL</F6></Row>')
,(92204, 53, 1083, N'<Row><F1>835</F1><F2>Family Dollar</F2><F3>7255 Booker T Washington Hwy</F3><F4>Wirtz</F4><F5>VA</F5><F6>Family Dollar
7255 Booker T Washington Hwy
Wirtz, VA</F6></Row>')
,(92205, 53, 1084, N'<Row><F1>836</F1><F2>Chuck E. Cheese''s</F2><F3>7258 Rivers Ave</F3><F4>North Charleston</F4><F5>SC</F5><F6>Chuck E. Cheese''s
7258 Rivers Ave
North Charleston, SC</F6></Row>')
,(92206, 53, 1085, N'<Row><F1>837</F1><F2>West Palm Beach Kia</F2><F3>735 S Military Trl</F3><F4>West Palm Beach</F4><F5>FL</F5><F6>West Palm Beach Kia
735 S Military Trl
West Palm Beach, FL</F6></Row>')
,(92207, 53, 1086, N'<Row><F1>838</F1><F2>North Hills Toyota Dealership</F2><F3>7401 McKnight Rd</F3><F4>Pittsburgh</F4><F5>PA</F5><F6>North Hills Toyota Dealership
7401 McKnight Rd
Pittsburgh, PA</F6></Row>')
,(92208, 53, 1087, N'<Row><F1>839</F1><F2>Chandler Auto Mall</F2><F3>7430 W Orchid Ln</F3><F4>Chandler</F4><F5>AZ</F5><F6>Chandler Auto Mall
7430 W Orchid Ln
Chandler, AZ</F6></Row>')
,(92209, 53, 1088, N'<Row><F1>840</F1><F2>Chandler Auto Mall</F2><F3>7450 W Orchid Ln</F3><F4>Chandler</F4><F5>AZ</F5><F6>Chandler Auto Mall
7450 W Orchid Ln
Chandler, AZ</F6></Row>')
,(92210, 53, 1089, N'<Row><F1>841</F1><F2>Chandler Auto Mall</F2><F3>7460 W Orchid Ln</F3><F4>Chandler</F4><F5>AZ</F5><F6>Chandler Auto Mall
7460 W Orchid Ln
Chandler, AZ</F6></Row>')
,(92211, 53, 1090, N'<Row><F1>842</F1><F2>Hampton Inn &amp; Suites Hershey</F2><F3>749 E Chocolate Ave</F3><F4>Hershey</F4><F5>PA</F5><F6>Hampton Inn &amp; Suites Hershey
749 E Chocolate Ave
Hershey, PA</F6></Row>')
,(92212, 53, 1091, N'<Row><F1>1257</F1><F3>75 Broadway</F3><F4>San Francisco</F4><F5>CA</F5><F6>75 Broadway
San Francisco, CA</F6></Row>')
,(92213, 53, 1092, N'<Row><F1>843</F1><F2>Chuck E. Cheese''s</F2><F3>7510 Parkway Dr</F3><F4>Littleton</F4><F5>CO</F5><F6>Chuck E. Cheese''s
7510 Parkway Dr
Littleton, CO</F6></Row>')
,(92214, 53, 1093, N'<Row><F1>844</F1><F2>Family Dollar</F2><F3>7567 State Highway M123</F3><F4>Newberry</F4><F5>MI</F5><F6>Family Dollar
7567 State Highway M123
Newberry, MI</F6></Row>')
,(92215, 53, 1094, N'<Row><F1>845</F1><F2>Honda West</F2><F3>7615 W Sahara Ave</F3><F4>Las Vegas</F4><F5>NV</F5><F6>Honda West
7615 W Sahara Ave
Las Vegas, NV</F6></Row>')
,(92216, 53, 1095, N'<Row><F1>1043</F1><F2>7700 Parmer - Building A</F2><F3>7700 W Parmer Ln</F3><F4>Austin</F4><F5>TX</F5><F6>7700 Parmer - Building A
7700 W Parmer Ln
Austin, TX</F6></Row>')
,(92217, 53, 1096, N'<Row><F1>1044</F1><F2>7700 Parmer - Building B</F2><F3>7700 W Parmer Ln</F3><F4>Austin</F4><F5>TX</F5><F6>7700 Parmer - Building B
7700 W Parmer Ln
Austin, TX</F6></Row>')
,(92218, 53, 1097, N'<Row><F1>1045</F1><F2>7700 Parmer - Building C</F2><F3>7700 W Parmer Ln</F3><F4>Austin</F4><F5>TX</F5><F6>7700 Parmer - Building C
7700 W Parmer Ln
Austin, TX</F6></Row>')
,(92219, 53, 1098, N'<Row><F1>1046</F1><F2>7700 Parmer - Building D</F2><F3>7700 W Parmer Ln</F3><F4>Austin</F4><F5>TX</F5><F6>7700 Parmer - Building D
7700 W Parmer Ln
Austin, TX</F6></Row>')
,(92220, 53, 1099, N'<Row><F1>846</F1><F2>Family Dollar</F2><F3>771 Nicholson St</F3><F4>Richland</F4><F5>GA</F5><F6>Family Dollar
771 Nicholson St
Richland, GA</F6></Row>')
,(92221, 53, 1100, N'<Row><F1>1153</F1><F2>La Costa Towne Center</F2><F3>7710 El Camino Real</F3><F4>Carlsbad</F4><F5>CA</F5><F6>La Costa Towne Center
7710 El Camino Real
Carlsbad, CA</F6></Row>')
,(92222, 53, 1101, N'<Row><F1>847</F1><F2>One Pacific Plaza</F2><F3>7711-7979 Center Ave</F3><F4>Huntington Beach</F4><F5>CA</F5><F6>One Pacific Plaza
7711-7979 Center Ave
Huntington Beach, CA</F6></Row>')
,(92223, 53, 1102, N'<Row><F1>1112</F1><F3>775 Columbus Ave</F3><F4>New York</F4><F5>NY</F5><F6>775 Columbus Ave
New York, NY</F6></Row>')
,(92224, 53, 1103, N'<Row><F1>848</F1><F3>777 Westchester Ave</F3><F4>White Plains</F4><F5>NY</F5><F6>777 Westchester Ave
White Plains, NY</F6></Row>')
,(92225, 53, 1104, N'<Row><F1>996</F1><F2>The Reflections</F2><F3>7775 Baymeadows Way</F3><F4>Jacksonville</F4><F5>FL</F5><F6>The Reflections
7775 Baymeadows Way
Jacksonville, FL</F6></Row>')
,(92226, 53, 1105, N'<Row><F1>997</F1><F2>The Reflections</F2><F3>7785 Baymeadows Way</F3><F4>Jacksonville</F4><F5>FL</F5><F6>The Reflections
7785 Baymeadows Way
Jacksonville, FL</F6></Row>')
,(92227, 53, 1106, N'<Row><F1>849</F1><F2>Family Dollar</F2><F3>7790 Dorchester Rd</F3><F4>North Charleston</F4><F5>SC</F5><F6>Family Dollar
7790 Dorchester Rd
North Charleston, SC</F6></Row>')
,(92228, 53, 1107, N'<Row><F1>850</F1><F2>Centrum at Glenridge</F2><F3>780 Johnson Ferry Rd</F3><F4>Atlanta</F4><F5>GA</F5><F6>Centrum at Glenridge
780 Johnson Ferry Rd
Atlanta, GA</F6></Row>')
,(92229, 53, 1108, N'<Row><F1>851</F1><F2>Chuck E. Cheese''s</F2><F3>7935 Grapevine Hwy</F3><F4>N Richland Hills</F4><F5>TX</F5><F6>Chuck E. Cheese''s
7935 Grapevine Hwy
N Richland Hills, TX</F6></Row>')
,(92230, 53, 1109, N'<Row><F1>1113</F1><F3>795 Columbus Ave</F3><F4>New York</F4><F5>NY</F5><F6>795 Columbus Ave
New York, NY</F6></Row>')
,(92231, 53, 1110, N'<Row><F1>852</F1><F2>Hotel Vitale </F2><F3>8 Mission St</F3><F4>San Francisco</F4><F5>CA</F5><F6>Hotel Vitale 
8 Mission St
San Francisco, CA</F6></Row>')
,(92232, 53, 1111, N'<Row><F1>853</F1><F3>80 Broad St</F3><F4>New York</F4><F5>NY</F5><F6>80 Broad St
New York, NY</F6></Row>')
,(92233, 53, 1112, N'<Row><F1>854</F1><F2>Mercedes-Benz Pre-Owned of Fairfield</F2><F3>80 Commerce Dr</F3><F4>Fairfield</F4><F5>CT</F5><F6>Mercedes-Benz Pre-Owned of Fairfield
80 Commerce Dr
Fairfield, CT</F6></Row>')
,(92234, 53, 1113, N'<Row><F1>1030</F1><F3>800 5th Ave</F3><F4>Seattle</F4><F5>WA</F5><F6>800 5th Ave
Seattle, WA</F6></Row>')
,(92235, 53, 1114, N'<Row><F1>1275</F1><F3>800 Concar Dr</F3><F4>San Mateo</F4><F5>CA</F5><F6>800 Concar Dr
San Mateo, CA</F6></Row>')
,(92236, 53, 1115, N'<Row><F1>855</F1><F2>Family Dollar</F2><F3>800 E Main St</F3><F4>Philadelphia</F4><F5>MS</F5><F6>Family Dollar
800 E Main St
Philadelphia, MS</F6></Row>')
,(92237, 53, 1116, N'<Row><F1>856</F1><F2>Family Dollar</F2><F3>8000 Michigan Ave</F3><F4>Detroit</F4><F5>MI</F5><F6>Family Dollar
8000 Michigan Ave
Detroit, MI</F6></Row>')
,(92238, 53, 1117, N'<Row><F1>857</F1><F2>Freedom Chevy Buick GMC</F2><F3>8008 Marvin D Love Fwy</F3><F4>Dallas</F4><F5>TX</F5><F6>Freedom Chevy Buick GMC
8008 Marvin D Love Fwy
Dallas, TX</F6></Row>')
,(92239, 53, 1118, N'<Row><F1>1114</F1><F3>801 Columbus Ave</F3><F4>New York</F4><F5>NY</F5><F6>801 Columbus Ave
New York, NY</F6></Row>')
,(92240, 53, 1119, N'<Row><F1>858</F1><F2>Fort Mill Auto Mall</F2><F3>801 Gold Hill Rd</F3><F4>Fort Mill</F4><F5>SC</F5><F6>Fort Mill Auto Mall
801 Gold Hill Rd
Fort Mill, SC</F6></Row>')
,(92241, 53, 1120, N'<Row><F1>859</F1><F3>801 Market St</F3><F4>San Francisco</F4><F5>CA</F5><F6>801 Market St
San Francisco, CA</F6></Row>')
,(92242, 53, 1121, N'<Row><F1>860</F1><F2>Lithia Volkswagen</F2><F3>801 N Riverside</F3><F4>Medford</F4><F5>OR</F5><F6>Lithia Volkswagen
801 N Riverside
Medford, OR</F6></Row>')
,(92243, 53, 1122, N'<Row><F1>861</F1><F2>801 Tower</F2><F3>801 S Figueroa St</F3><F4>Los Angeles</F4><F5>CA</F5><F6>801 Tower
801 S Figueroa St
Los Angeles, CA</F6></Row>')
,(92244, 53, 1123, N'<Row><F1>862</F1><F2>Heritage Hyundai</F2><F3>801 York Rd</F3><F4>Towson</F4><F5>MD</F5><F6>Heritage Hyundai
801 York Rd
Towson, MD</F6></Row>')
,(92245, 53, 1124, N'<Row><F1>863</F1><F2>Family Dollar</F2><F3>8010 Preston Hwy</F3><F4>Louisville</F4><F5>KY</F5><F6>Family Dollar
8010 Preston Hwy
Louisville, KY</F6></Row>')
,(92246, 53, 1125, N'<Row><F1>864</F1><F3>801-821 Railhead Rd</F3><F4>Fort Worth</F4><F5>TX</F5><F6>801-821 Railhead Rd
Fort Worth, TX</F6></Row>')
,(92247, 53, 1126, N'<Row><F1>865</F1><F2>Chuck E. Cheese''s</F2><F3>803 E Danenberg Dr</F3><F4>El Centro</F4><F5>CA</F5><F6>Chuck E. Cheese''s
803 E Danenberg Dr
El Centro, CA</F6></Row>')
,(92248, 53, 1127, N'<Row><F1>866</F1><F2>Family Dollar</F2><F3>803 E Jackson Rd</F3><F4>Union</F4><F5>MS</F5><F6>Family Dollar
803 E Jackson Rd
Union, MS</F6></Row>')
,(92249, 53, 1128, N'<Row><F1>867</F1><F2>Honda of Tempe</F2><F3>8030 S Autoplex Loop</F3><F4>Tempe</F4><F5>AZ</F5><F6>Honda of Tempe
8030 S Autoplex Loop
Tempe, AZ</F6></Row>')
,(92250, 53, 1129, N'<Row><F1>1103</F1><F2>Towers of Kenwood</F2><F3>8044 Montgomery Rd</F3><F4>Cincinnati</F4><F5>OH</F5><F6>Towers of Kenwood
8044 Montgomery Rd
Cincinnati, OH</F6></Row>')
,(92251, 53, 1130, N'<Row><F1>1115</F1><F3>805 Columbus Ave</F3><F4>New York</F4><F5>NY</F5><F6>805 Columbus Ave
New York, NY</F6></Row>')
,(92252, 53, 1131, N'<Row><F1>868</F1><F2>Porsche of Destin</F2><F3>808 Airport Rd</F3><F4>Destin</F4><F5>FL</F5><F6>Porsche of Destin
808 Airport Rd
Destin, FL</F6></Row>')
,(92253, 53, 1132, N'<Row><F1>1116</F1><F3>808 Columbus Ave</F3><F4>New York</F4><F5>NY</F5><F6>808 Columbus Ave
New York, NY</F6></Row>')
,(92254, 53, 1133, N'<Row><F1>869</F1><F2>Tom Kelley Cadillac</F2><F3>811 Avenue of Autos</F3><F4>Fort Wayne</F4><F5>IN</F5><F6>Tom Kelley Cadillac
811 Avenue of Autos
Fort Wayne, IN</F6></Row>')
,(92255, 53, 1134, N'<Row><F1>870</F1><F2>Family Dollar</F2><F3>811 N 1st Ave</F3><F4>Durant</F4><F5>OK</F5><F6>Family Dollar
811 N 1st Ave
Durant, OK</F6></Row>')
,(92256, 53, 1135, N'<Row><F1>871</F1><F2>Family Dollar</F2><F3>811 S Burlington Ave</F3><F4>Hastings</F4><F5>NE</F5><F6>Family Dollar
811 S Burlington Ave
Hastings, NE</F6></Row>')
,(92257, 53, 1136, N'<Row><F1>1182</F1><F3>815 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>815 Atlantic Ave
Alameda, CA</F6></Row>')
,(92258, 53, 1137, N'<Row><F1>872</F1><F2>Freedom Chrysler Dodge Jeep Ram</F2><F3>815 E Camp Wisdom Rd</F3><F4>Duncanville</F4><F5>TX</F5><F6>Freedom Chrysler Dodge Jeep Ram
815 E Camp Wisdom Rd
Duncanville, TX</F6></Row>')
,(92259, 53, 1138, N'<Row><F1>1061</F1><F2>Hilton Fort Worth</F2><F3>815 Main St</F3><F4>Fort Worth</F4><F5>TX</F5><F6>Hilton Fort Worth
815 Main St
Fort Worth, TX</F6></Row>')
,(92260, 53, 1139, N'<Row><F1>873</F1><F3>815 Market St</F3><F4>San Francisco</F4><F5>CA</F5><F6>815 Market St
San Francisco, CA</F6></Row>')
,(92261, 53, 1140, N'<Row><F1>1031</F1><F3>816 Congress Ave</F3><F4>Austin</F4><F5>TX</F5><F6>816 Congress Ave
Austin, TX</F6></Row>')
,(92262, 53, 1141, N'<Row><F1>1262</F1><F3>818 Stewart Ave</F3><F4>Seattle</F4><F5>WA</F5><F6>818 Stewart Ave
Seattle, WA</F6></Row>')
,(92263, 53, 1142, N'<Row><F1>874</F1><F2>Roundtree Used</F2><F3>8188 Mansfield Rd</F3><F4>Shreveport</F4><F5>LA</F5><F6>Roundtree Used
8188 Mansfield Rd
Shreveport, LA</F6></Row>')
,(92264, 53, 1143, N'<Row><F1>1073</F1><F2>Courtyard Louisville Airport</F2><F3>819 Phillips Ln</F3><F4>Louisville</F4><F5>KY</F5><F6>Courtyard Louisville Airport
819 Phillips Ln
Louisville, KY</F6></Row>')
,(92265, 53, 1144, N'<Row><F1>875</F1><F3>8211 Bruceville Rd</F3><F4>Sacramento</F4><F5>CA</F5><F6>8211 Bruceville Rd
Sacramento, CA</F6></Row>')
,(92266, 53, 1145, N'<Row><F1>876</F1><F3>8221 Timberlake Way</F3><F4>Sacramento</F4><F5>CA</F5><F6>8221 Timberlake Way
Sacramento, CA</F6></Row>')
,(92267, 53, 1146, N'<Row><F1>877</F1><F3>8231 Timberlake Way</F3><F4>Sacramento</F4><F5>CA</F5><F6>8231 Timberlake Way
Sacramento, CA</F6></Row>')
,(92268, 53, 1147, N'<Row><F1>878</F1><F2>Chuck E. Cheese''s</F2><F3>824 Earnest W. Barrett Pkwy</F3><F4>Kennesaw</F4><F5>GA</F5><F6>Chuck E. Cheese''s
824 Earnest W. Barrett Pkwy
Kennesaw, GA</F6></Row>')
,(92269, 53, 1148, N'<Row><F1>879</F1><F3>8241 Bruceville Rd</F3><F4>Sacramento</F4><F5>CA</F5><F6>8241 Bruceville Rd
Sacramento, CA</F6></Row>')
,(92270, 53, 1149, N'<Row><F1>880</F1><F3>8251 Bruceville Rd</F3><F4>Sacramento</F4><F5>CA</F5><F6>8251 Bruceville Rd
Sacramento, CA</F6></Row>')
,(92271, 53, 1150, N'<Row><F1>881</F1><F2>The Park</F2><F3>8300 N Mopac Expy</F3><F4>Austin</F4><F5>TX</F5><F6>The Park
8300 N Mopac Expy
Austin, TX</F6></Row>')
,(92272, 53, 1151, N'<Row><F1>882</F1><F2>Kearny Mesa Rd Parking Lot</F2><F3>8330 Engineer Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>Kearny Mesa Rd Parking Lot
8330 Engineer Rd
San Diego, CA</F6></Row>')
,(92273, 53, 1152, N'<Row><F1>883</F1><F2>Kearny Mesa Rd Parking Lot</F2><F3>8361 Buckhorn St</F3><F4>San Diego</F4><F5>CA</F5><F6>Kearny Mesa Rd Parking Lot
8361 Buckhorn St
San Diego, CA</F6></Row>')
,(92274, 53, 1153, N'<Row><F1>884</F1><F2>BMW of Roxbury</F2><F3>840 U.S. 46</F3><F4>Kenvil</F4><F5>NJ</F5><F6>BMW of Roxbury
840 U.S. 46
Kenvil, NJ</F6></Row>')
,(92275, 53, 1154, N'<Row><F1>885</F1><F2>Doral Concourse</F2><F3>8400 NW 36th St</F3><F4>Doral</F4><F5>FL</F5><F6>Doral Concourse
8400 NW 36th St
Doral, FL</F6></Row>')
,(92276, 53, 1155, N'<Row><F1>886</F1><F2>BMW of Fairfax</F2><F3>8427 Lee Hwy</F3><F4>Fairfax</F4><F5>VA</F5><F6>BMW of Fairfax
8427 Lee Hwy
Fairfax, VA</F6></Row>')
,(92277, 53, 1156, N'<Row><F1>887</F1><F2>BMW of Fairfax Body Shop</F2><F3>8439 Lee Hwy</F3><F4>Fairfax</F4><F5>VA</F5><F6>BMW of Fairfax Body Shop
8439 Lee Hwy
Fairfax, VA</F6></Row>')
,(92278, 53, 1157, N'<Row><F1>888</F1><F2>Lone Star Ford</F2><F3>8477 N Fwy</F3><F4>Houston</F4><F5>TX</F5><F6>Lone Star Ford
8477 N Fwy
Houston, TX</F6></Row>')
,(92279, 53, 1158, N'<Row><F1>1019</F1><F3>85 Broad St</F3><F4>New York</F4><F5>NY</F5><F6>85 Broad St
New York, NY</F6></Row>')
,(92280, 53, 1159, N'<Row><F1>889</F1><F2>Family Dollar</F2><F3>85 Park Ave</F3><F4>Lindale</F4><F5>GA</F5><F6>Family Dollar
85 Park Ave
Lindale, GA</F6></Row>')
,(92281, 53, 1160, N'<Row><F1>1180</F1><F3>850 Marina Village Pkwy</F3><F4>Alameda</F4><F5>CA</F5><F6>850 Marina Village Pkwy
Alameda, CA</F6></Row>')
,(92282, 53, 1161, N'<Row><F1>890</F1><F2>Rosenthal Mazda/Nissan &amp; Sonic-Infiniti of Tysons Corner</F2><F3>8525 Leesburg Pike</F3><F4>Vienna</F4><F5>VA</F5><F6>Rosenthal Mazda/Nissan &amp; Sonic-Infiniti of Tysons Corner
8525 Leesburg Pike
Vienna, VA</F6></Row>')
,(92283, 53, 1162, N'<Row><F1>891</F1><F2>Mercedes-Benz/smart of Tysons Corner</F2><F3>8545 Leesburg Pike</F3><F4>Vienna</F4><F5>VA</F5><F6>Mercedes-Benz/smart of Tysons Corner
8545 Leesburg Pike
Vienna, VA</F6></Row>')
,(92284, 53, 1163, N'<Row><F1>892</F1><F3>855 Cog Cir</F3><F4>Crystal Lake</F4><F5>IL</F5><F6>855 Cog Cir
Crystal Lake, IL</F6></Row>')
,(92285, 53, 1164, N'<Row><F1>893</F1><F2>Audi Tysons Corner</F2><F3>8598 Leesburg Pike</F3><F4>Vienna</F4><F5>VA</F5><F6>Audi Tysons Corner
8598 Leesburg Pike
Vienna, VA</F6></Row>')
,(92286, 53, 1165, N'<Row><F1>894</F1><F2>Porsche of Tysons Corner</F2><F3>8601 Westwood Center Dr</F3><F4>Vienna</F4><F5>VA</F5><F6>Porsche of Tysons Corner
8601 Westwood Center Dr
Vienna, VA</F6></Row>')
,(92287, 53, 1166, N'<Row><F1>895</F1><F2>Orr Infiniti</F2><F3>8727 Business Park Dr</F3><F4>Shreveport</F4><F5>LA</F5><F6>Orr Infiniti
8727 Business Park Dr
Shreveport, LA</F6></Row>')
,(92288, 53, 1167, N'<Row><F1>896</F1><F2>Orr Acura</F2><F3>8730 Business Park Dr</F3><F4>Shreveport</F4><F5>LA</F5><F6>Orr Acura
8730 Business Park Dr
Shreveport, LA</F6></Row>')
,(92289, 53, 1168, N'<Row><F1>897</F1><F2>Orr Pre-Owned SuperCenter</F2><F3>8730 Business Park Dr</F3><F4>Shreveport</F4><F5>LA</F5><F6>Orr Pre-Owned SuperCenter
8730 Business Park Dr
Shreveport, LA</F6></Row>')
,(92290, 53, 1169, N'<Row><F1>898</F1><F2>Shreveport Mitsubishi</F2><F3>8748 Quimper Pl</F3><F4>Shreveport</F4><F5>LA</F5><F6>Shreveport Mitsubishi
8748 Quimper Pl
Shreveport, LA</F6></Row>')
,(92291, 53, 1170, N'<Row><F1>899</F1><F2>Orr Cadillac</F2><F3>8750 Business Park Dr</F3><F4>Shreveport</F4><F5>LA</F5><F6>Orr Cadillac
8750 Business Park Dr
Shreveport, LA</F6></Row>')
,(92292, 53, 1171, N'<Row><F1>900</F1><F3>880 Ridder Park Dr</F3><F4>San Jose</F4><F5>CA</F5><F6>880 Ridder Park Dr
San Jose, CA</F6></Row>')
,(92293, 53, 1172, N'<Row><F1>901</F1><F2>Massey Cadillac</F2><F3>8819 S Orange Blossom Trl</F3><F4>Orlando</F4><F5>FL</F5><F6>Massey Cadillac
8819 S Orange Blossom Trl
Orlando, FL</F6></Row>')
,(92294, 53, 1173, N'<Row><F1>902</F1><F2>Auto Trim Design (Roundtree)</F2><F3>8855 Quimper Pl</F3><F4>Shreveport</F4><F5>LA</F5><F6>Auto Trim Design (Roundtree)
8855 Quimper Pl
Shreveport, LA</F6></Row>')
,(92295, 53, 1174, N'<Row><F1>903</F1><F3>888 Brannan St</F3><F4>San Francisco</F4><F5>CA</F5><F6>888 Brannan St
San Francisco, CA</F6></Row>')
,(92296, 53, 1175, N'<Row><F1>1032</F1><F3>888 First St NE</F3><F4>Washington</F4><F5>DC</F5><F6>888 First St NE
Washington, DC</F6></Row>')
,(92297, 53, 1176, N'<Row><F1>904</F1><F2>Toyota of Ft. Worth Used Cars</F2><F3>8901 Camp Bowie W Blvd</F3><F4>Fort Worth</F4><F5>TX</F5><F6>Toyota of Ft. Worth Used Cars
8901 Camp Bowie W Blvd
Fort Worth, TX</F6></Row>')
,(92298, 53, 1177, N'<Row><F1>905</F1><F2>Family Dollar</F2><F3>8909 Two Notch Rd</F3><F4>Columbia</F4><F5>SC</F5><F6>Family Dollar
8909 Two Notch Rd
Columbia, SC</F6></Row>')
,(92299, 53, 1178, N'<Row><F1>906</F1><F2>Family Dollar</F2><F3>8920 S Highway 95</F3><F4>Mohave Valley</F4><F5>AZ</F5><F6>Family Dollar
8920 S Highway 95
Mohave Valley, AZ</F6></Row>')
,(92300, 53, 1179, N'<Row><F1>907</F1><F2>Family Dollar</F2><F3>8973 University Blvd</F3><F4>North Charleston</F4><F5>SC</F5><F6>Family Dollar
8973 University Blvd
North Charleston, SC</F6></Row>')
,(92301, 53, 1180, N'<Row><F1>1067</F1><F2>SpringHill Suites Baltimore BWI Airport</F2><F3>899 Elkridge Landing Rd</F3><F4>Linthicum Heights</F4><F5>MD</F5><F6>SpringHill Suites Baltimore BWI Airport
899 Elkridge Landing Rd
Linthicum Heights, MD</F6></Row>')
,(92302, 53, 1181, N'<Row><F1>908</F1><F3>90 Broad St</F3><F4>New York</F4><F5>NY</F5><F6>90 Broad St
New York, NY</F6></Row>')
,(92303, 53, 1182, N'<Row><F1>1020</F1><F3>90 Long Acre</F3><F4>London</F4><F5>UK</F5><F6>90 Long Acre
London, UK</F6></Row>')
,(92304, 53, 1183, N'<Row><F1>1246</F1><F3>90 Rio Robles</F3><F4>San Jose</F4><F5>CA</F5><F6>90 Rio Robles
San Jose, CA</F6></Row>')
,(92305, 53, 1184, N'<Row><F1>1276</F1><F3>900 Concar Dr</F3><F4>San Mateo</F4><F5>CA</F5><F6>900 Concar Dr
San Mateo, CA</F6></Row>')
,(92306, 53, 1185, N'<Row><F1>909</F1><F2>Orr Motors North</F2><F3>900 Truman Baker Dr</F3><F4>Searcy</F4><F5>AR</F5><F6>Orr Motors North
900 Truman Baker Dr
Searcy, AR</F6></Row>')
,(92307, 53, 1186, N'<Row><F1>910</F1><F2>Toyota/Scion of Fort Worth</F2><F3>9001 Camp Bowie W Blvd</F3><F4>Fort Worth</F4><F5>TX</F5><F6>Toyota/Scion of Fort Worth
9001 Camp Bowie W Blvd
Fort Worth, TX</F6></Row>')
,(92308, 53, 1187, N'<Row><F1>911</F1><F2>Infiniti of Charlotte Parking Lot</F2><F3>9009 E Independence Blvd</F3><F4>Mathews</F4><F5>NC</F5><F6>Infiniti of Charlotte Parking Lot
9009 E Independence Blvd
Mathews, NC</F6></Row>')
,(92309, 53, 1188, N'<Row><F1>912</F1><F2>Auffenberg Ford</F2><F3>901 S Illinois St</F3><F4>Belleville</F4><F5>IL</F5><F6>Auffenberg Ford
901 S Illinois St
Belleville, IL</F6></Row>')
,(92310, 53, 1189, N'<Row><F1>913</F1><F2>Family Dollar</F2><F3>9018 US 82</F3><F4>Alapaha</F4><F5>GA</F5><F6>Family Dollar
9018 US 82
Alapaha, GA</F6></Row>')
,(92311, 53, 1190, N'<Row><F1>914</F1><F2>Family Dollar</F2><F3>902 S Pierce St</F3><F4>Alma</F4><F5>GA</F5><F6>Family Dollar
902 S Pierce St
Alma, GA</F6></Row>')
,(92312, 53, 1191, N'<Row><F1>915</F1><F2>Woodbury Lakes</F2><F3>9020 Hudson Rd</F3><F4>St Paul</F4><F5>MN</F5><F6>Woodbury Lakes
9020 Hudson Rd
St Paul, MN</F6></Row>')
,(92313, 53, 1192, N'<Row><F1>1107</F1><F2>Centre Pointe IV</F2><F3>9025 Centre Pointe Dr</F3><F4>West Chester Township</F4><F5>OH</F5><F6>Centre Pointe IV
9025 Centre Pointe Dr
West Chester Township, OH</F6></Row>')
,(92314, 53, 1193, N'<Row><F1>916</F1><F2>Family Dollar</F2><F3>9044 N State Highway 16</F3><F4>Poteet</F4><F5>TX</F5><F6>Family Dollar
9044 N State Highway 16
Poteet, TX</F6></Row>')
,(92315, 53, 1194, N'<Row><F1>1105</F1><F2>Centre Pointe II</F2><F3>9050 Centre Pointe Dr</F3><F4>West Chester Township</F4><F5>OH</F5><F6>Centre Pointe II
9050 Centre Pointe Dr
West Chester Township, OH</F6></Row>')
,(92316, 53, 1195, N'<Row><F1>1106</F1><F2>Centre Pointe III</F2><F3>9075 Centre Pointe Dr</F3><F4>West Chester Township</F4><F5>OH</F5><F6>Centre Pointe III
9075 Centre Pointe Dr
West Chester Township, OH</F6></Row>')
,(92317, 53, 1196, N'<Row><F1>1048</F1><F2>Aldwych House</F2><F3>91 Aldwych</F3><F4>London WC2B</F4><F5>UK</F5><F6>Aldwych House
91 Aldwych
London WC2B, UK</F6></Row>')
,(92318, 53, 1197, N'<Row><F1>917</F1><F2>Tom Kelley Volvo (Automall)</F2><F3>910 Avenue of Autos</F3><F4>Fort Wayne</F4><F5>IN</F5><F6>Tom Kelley Volvo (Automall)
910 Avenue of Autos
Fort Wayne, IN</F6></Row>')
,(92319, 53, 1198, N'<Row><F1>1104</F1><F2>Centre Pointe I</F2><F3>9100 Centre Pointe Dr</F3><F4>West Chester Township</F4><F5>OH</F5><F6>Centre Pointe I
9100 Centre Pointe Dr
West Chester Township, OH</F6></Row>')
,(92320, 53, 1199, N'<Row><F1>918</F1><F2>Town &amp; Country Toyota/Scion</F2><F3>9101 S Blvd</F3><F4>Charlotte</F4><F5>NC</F5><F6>Town &amp; Country Toyota/Scion
9101 S Blvd
Charlotte, NC</F6></Row>')
,(92321, 53, 1200, N'<Row><F1>919</F1><F2>Infiniti of Charlotte</F2><F3>9103 E Independence Blvd</F3><F4>Matthews</F4><F5>NC</F5><F6>Infiniti of Charlotte
9103 E Independence Blvd
Matthews, NC</F6></Row>')
,(92322, 53, 1201, N'<Row><F1>920</F1><F2>Tom Kelley Used Cars (Automall)</F2><F3>910A Avenue of Autos</F3><F4>Fort Wayne</F4><F5>IN</F5><F6>Tom Kelley Used Cars (Automall)
910A Avenue of Autos
Fort Wayne, IN</F6></Row>')
,(92323, 53, 1202, N'<Row><F1>1156</F1><F2>Laulani Village</F2><F3>91-1061 Keaunui Dr</F3><F4>Ewa Beach</F4><F5>HI</F5><F6>Laulani Village
91-1061 Keaunui Dr
Ewa Beach, HI</F6></Row>')
,(92324, 53, 1203, N'<Row><F1>1252</F1><F3>92 Kensington High St</F3><F4>Kensington, London W8 5ED</F4><F5>UK</F5><F6>92 Kensington High St
Kensington, London W8 5ED, UK</F6></Row>')
,(92325, 53, 1204, N'<Row><F1>921</F1><F3>920 Broadway</F3><F4>New York</F4><F5>NY</F5><F6>920 Broadway
New York, NY</F6></Row>')
,(92326, 53, 1205, N'<Row><F1>922</F1><F2>Stony Point Fashion Park</F2><F3>9200 Stony Point Pkwy</F3><F4>Richmond</F4><F5>VA</F5><F6>Stony Point Fashion Park
9200 Stony Point Pkwy
Richmond, VA</F6></Row>')
,(92327, 53, 1206, N'<Row><F1>923</F1><F2>Courtesy Chrysler, Jeep, Dodge</F2><F3>9207 Adamo Dr</F3><F4>Tampa</F4><F5>FL</F5><F6>Courtesy Chrysler, Jeep, Dodge
9207 Adamo Dr
Tampa, FL</F6></Row>')
,(92328, 53, 1207, N'<Row><F1>924</F1><F2>Heritage Chrysler</F2><F3>9213-9219 Harford Rd</F3><F4>Baltimore</F4><F5>MD</F5><F6>Heritage Chrysler
9213-9219 Harford Rd
Baltimore, MD</F6></Row>')
,(92329, 53, 1208, N'<Row><F1>925</F1><F2>Mesa Harley-Davidson</F2><F3>922 S Country Club Dr</F3><F4>Mesa</F4><F5>AZ</F5><F6>Mesa Harley-Davidson
922 S Country Club Dr
Mesa, AZ</F6></Row>')
,(92330, 53, 1209, N'<Row><F1>926</F1><F2>Nissan of Garden Grove</F2><F3>9222 Trask Ave</F3><F4>Garden Grove</F4><F5>CA</F5><F6>Nissan of Garden Grove
9222 Trask Ave
Garden Grove, CA</F6></Row>')
,(92331, 53, 1210, N'<Row><F1>927</F1><F2>Chuck E. Cheese''s</F2><F3>925 North Point Dr</F3><F4>Alpharetta</F4><F5>GA</F5><F6>Chuck E. Cheese''s
925 North Point Dr
Alpharetta, GA</F6></Row>')
,(92332, 53, 1211, N'<Row><F1>928</F1><F3>925 Westchester Ave</F3><F4>White Plains</F4><F5>NY</F5><F6>925 Westchester Ave
White Plains, NY</F6></Row>')
,(92333, 53, 1212, N'<Row><F1>1108</F1><F2>Centre Pointe VI</F2><F3>9277 Centre Pointe Dr</F3><F4>West Chester Township</F4><F5>OH</F5><F6>Centre Pointe VI
9277 Centre Pointe Dr
West Chester Township, OH</F6></Row>')
,(92334, 53, 1213, N'<Row><F1>929</F1><F3>9336 Civic Center Dr</F3><F4>Beverly Hills</F4><F5>CA</F5><F6>9336 Civic Center Dr
Beverly Hills, CA</F6></Row>')
,(92335, 53, 1214, N'<Row><F1>930</F1><F2>The Culver Studios</F2><F3>9336 W Washington Blvd</F3><F4>Culver City</F4><F5>CA</F5><F6>The Culver Studios
9336 W Washington Blvd
Culver City, CA</F6></Row>')
,(92336, 53, 1215, N'<Row><F1>931</F1><F3>9346 Civic Center Dr</F3><F4>Beverly Hills</F4><F5>CA</F5><F6>9346 Civic Center Dr
Beverly Hills, CA</F6></Row>')
,(92337, 53, 1216, N'<Row><F1>932</F1><F3>9348 Civic Center Dr</F3><F4>Beverly Hills</F4><F5>CA</F5><F6>9348 Civic Center Dr
Beverly Hills, CA</F6></Row>')
,(92338, 53, 1217, N'<Row><F1>933</F1><F2>Family Dollar</F2><F3>936 S Woodlawn</F3><F4>Wichita</F4><F5>KS</F5><F6>Family Dollar
936 S Woodlawn
Wichita, KS</F6></Row>')
,(92339, 53, 1218, N'<Row><F1>1142</F1><F2>Hotel Figueroa</F2><F3>939 S Figueroa St</F3><F4>Los Angeles</F4><F5>CA</F5><F6>Hotel Figueroa
939 S Figueroa St
Los Angeles, CA</F6></Row>')
,(92340, 53, 1219, N'<Row><F1>934</F1><F2>Orr Classic Chevrolet of Ashdown</F2><F3>941 N Constitution Ave</F3><F4>Ashdown</F4><F5>AR</F5><F6>Orr Classic Chevrolet of Ashdown
941 N Constitution Ave
Ashdown, AR</F6></Row>')
,(92341, 53, 1220, N'<Row><F1>935</F1><F2>Hampton Inn &amp; Suites Smithfield</F2><F3>945 Douglas Pike</F3><F4>Smithfield</F4><F5>RI</F5><F6>Hampton Inn &amp; Suites Smithfield
945 Douglas Pike
Smithfield, RI</F6></Row>')
,(92342, 53, 1221, N'<Row><F1>1203</F1><F2>Village Shoppes</F2><F3>95 Washington St</F3><F4>Canton</F4><F5>MA</F5><F6>Village Shoppes
95 Washington St
Canton, MA</F6></Row>')
,(92343, 53, 1222, N'<Row><F1>936</F1><F3>950 E Ogden Ave</F3><F4>Naperville</F4><F5>IL</F5><F6>950 E Ogden Ave
Naperville, IL</F6></Row>')
,(92344, 53, 1223, N'<Row><F1>1181</F1><F3>950 Marina Village Pkwy</F3><F4>Alameda</F4><F5>CA</F5><F6>950 Marina Village Pkwy
Alameda, CA</F6></Row>')
,(92345, 53, 1224, N'<Row><F1>1130</F1><F2>The Fairmont San Francisco</F2><F3>950 Mason St</F3><F4>San Francisco</F4><F5>CA</F5><F6>The Fairmont San Francisco
950 Mason St
San Francisco, CA</F6></Row>')
,(92346, 53, 1225, N'<Row><F1>937</F1><F2>Hilton Arlington</F2><F3>950 N Stafford St</F3><F4>Alexandria</F4><F5>VA</F5><F6>Hilton Arlington
950 N Stafford St
Alexandria, VA</F6></Row>')
,(92347, 53, 1226, N'<Row><F1>1293</F1><F2>Town Center of Mililani</F2><F3>95-1249 Meheula Pkwy #193</F3><F4>Mililani</F4><F5>HI</F5><F6>Town Center of Mililani
95-1249 Meheula Pkwy #193
Mililani, HI</F6></Row>')
,(92348, 53, 1227, N'<Row><F1>1211</F1><F2>Belknap Mall</F2><F3>96 Daniel Webster Hwy</F3><F4>Belmont</F4><F5>NH</F5><F6>Belknap Mall
96 Daniel Webster Hwy
Belmont, NH</F6></Row>')
,(92349, 53, 1228, N'<Row><F1>1183</F1><F3>960 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>960 Atlantic Ave
Alameda, CA</F6></Row>')
,(92350, 53, 1229, N'<Row><F1>938</F1><F2>Family Dollar</F2><F3>9605 Fm 1732</F3><F4>Brownsville</F4><F5>TX</F5><F6>Family Dollar
9605 Fm 1732
Brownsville, TX</F6></Row>')
,(92351, 53, 1230, N'<Row><F1>939</F1><F3>9605 Scranton Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>9605 Scranton Rd
San Diego, CA</F6></Row>')
,(92352, 53, 1231, N'<Row><F1>940</F1><F2>Family Dollar</F2><F3>9610 S Orange Ave</F3><F4>Orlando</F4><F5>FL</F5><F6>Family Dollar
9610 S Orange Ave
Orlando, FL</F6></Row>')
,(92353, 53, 1232, N'<Row><F1>941</F1><F3>9645 Scranton Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>9645 Scranton Rd
San Diego, CA</F6></Row>')
,(92354, 53, 1233, N'<Row><F1>1184</F1><F3>965 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>965 Atlantic Ave
Alameda, CA</F6></Row>')
,(92355, 53, 1234, N'<Row><F1>942</F1><F2>Toyota of Garden Grove</F2><F3>9670 Trask Ave</F3><F4>Garden Grove</F4><F5>CA</F5><F6>Toyota of Garden Grove
9670 Trask Ave
Garden Grove, CA</F6></Row>')
,(92356, 53, 1235, N'<Row><F1>943</F1><F3>9675 Scranton Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>9675 Scranton Rd
San Diego, CA</F6></Row>')
,(92357, 53, 1236, N'<Row><F1>944</F1><F3>9685 Scranton Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>9685 Scranton Rd
San Diego, CA</F6></Row>')
,(92358, 53, 1237, N'<Row><F1>945</F1><F2>Waterstone Shopping Center </F2><F3>9691 Waterstone Blvd</F3><F4>Cincinnati</F4><F5>OH</F5><F6>Waterstone Shopping Center 
9691 Waterstone Blvd
Cincinnati, OH</F6></Row>')
,(92359, 53, 1238, N'<Row><F1>946</F1><F2>BMW of Tulsa</F2><F3>9702 S Memorial Dr</F3><F4>Tulsa</F4><F5>OK</F5><F6>BMW of Tulsa
9702 S Memorial Dr
Tulsa, OK</F6></Row>')
,(92360, 53, 1239, N'<Row><F1>1068</F1><F2>SpringHill Suites Gaithersburg</F2><F3>9715 Washingtonian Blvd</F3><F4>Gaithersburg</F4><F5>MD</F5><F6>SpringHill Suites Gaithersburg
9715 Washingtonian Blvd
Gaithersburg, MD</F6></Row>')
,(92361, 53, 1240, N'<Row><F1>947</F1><F3>9725 Scranton Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>9725 Scranton Rd
San Diego, CA</F6></Row>')
,(92362, 53, 1241, N'<Row><F1>948</F1><F3>9735 Scranton Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>9735 Scranton Rd
San Diego, CA</F6></Row>')
,(92363, 53, 1242, N'<Row><F1>949</F1><F2>ABC Detroit/Toledo</F2><F3>9797 Freemont Pike</F3><F4>Perrysburg</F4><F5>OH</F5><F6>ABC Detroit/Toledo
9797 Freemont Pike
Perrysburg, OH</F6></Row>')
,(92364, 53, 1243, N'<Row><F1>1185</F1><F3>980 Atlantic Ave</F3><F4>Alameda</F4><F5>CA</F5><F6>980 Atlantic Ave
Alameda, CA</F6></Row>')
,(92365, 53, 1244, N'<Row><F1>950</F1><F3>9805 Scranton Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>9805 Scranton Rd
San Diego, CA</F6></Row>')
,(92366, 53, 1245, N'<Row><F1>951</F1><F3>9855 Scranton Rd</F3><F4>San Diego</F4><F5>CA</F5><F6>9855 Scranton Rd
San Diego, CA</F6></Row>')
,(92367, 53, 1246, N'<Row><F1>1215</F1><F2>The Peninsula Beverly Hills</F2><F3>9882 S Santa Monica Blvd</F3><F4>Beverly Hills</F4><F5>CA</F5><F6>The Peninsula Beverly Hills
9882 S Santa Monica Blvd
Beverly Hills, CA</F6></Row>')
,(92368, 53, 1247, N'<Row><F1>1221</F1><F2>Grand Central Hotel</F2><F3>99 Gordon St</F3><F4>Glasgow City G1 3SF</F4><F5>UK</F5><F6>Grand Central Hotel
99 Gordon St
Glasgow City G1 3SF, UK</F6></Row>')
,(92369, 53, 1248, N'<Row><F1>952</F1><F2>Momentum Paint &amp; Body</F2><F3>9911 Centre Pkwy</F3><F4>Houston</F4><F5>TX</F5><F6>Momentum Paint &amp; Body
9911 Centre Pkwy
Houston, TX</F6></Row>')
,(92370, 53, 1249, N'<Row><F1>953</F1><F2>Family Dollar</F2><F3>9976 Wolfe St</F3><F4>Caledonia</F4><F5>MS</F5><F6>Family Dollar
9976 Wolfe St
Caledonia, MS</F6></Row>')
,(92371, 53, 1250, N'<Row><F1>954</F1><F2>Pacific City</F2><F3>Atlanta Ave &amp; Huntington St</F3><F4>Huntington Beach</F4><F5>CA</F5><F6>Pacific City
Atlanta Ave &amp; Huntington St
Huntington Beach, CA</F6></Row>')
,(92372, 53, 1251, N'<Row><F1>955</F1><F2>Freeport Alcochete</F2><F3>Avenida Euro 2004 2890-154</F3><F4>Alcochete</F4><F5>Portugal</F5><F6>Freeport Alcochete
Avenida Euro 2004 2890-154
Alcochete, Portugal</F6></Row>')
,(92373, 53, 1252, N'<Row><F1>956</F1><F2>Inskip Parking Lot</F2><F3>Bald Hill Rd</F3><F4>Warwick</F4><F5>RI</F5><F6>Inskip Parking Lot
Bald Hill Rd
Warwick, RI</F6></Row>')
,(92374, 53, 1253, N'<Row><F1>1234</F1><F2>Ettington Chase</F2><F3>Banbury Rd</F3><F4>Ettington, Stratford-upon-Avon CV37 7NZ</F4><F5>UK</F5><F6>Ettington Chase
Banbury Rd
Ettington, Stratford-upon-Avon CV37 7NZ, UK</F6></Row>')
,(92375, 53, 1254, N'<Row><F1>957</F1><F3>Breite Strasse 20</F3><F4>Dusseldorf</F4><F5>Germany</F5><F6>Breite Strasse 20
Dusseldorf, Germany</F6></Row>')
,(92376, 53, 1255, N'<Row><F1>958</F1><F3>Breite Strasse 28</F3><F4>Dusseldorf</F4><F5>Germany</F5><F6>Breite Strasse 28
Dusseldorf, Germany</F6></Row>')
,(92377, 53, 1256, N'<Row><F1>959</F1><F3>Breite Strasse 30-34</F3><F4>Dusseldorf</F4><F5>Germany</F5><F6>Breite Strasse 30-34
Dusseldorf, Germany</F6></Row>')
,(92378, 53, 1257, N'<Row><F1>960</F1><F3>Broadgate Cir</F3><F4>London</F4><F5>UK</F5><F6>Broadgate Cir
London, UK</F6></Row>')
,(92379, 53, 1258, N'<Row><F1>1218</F1><F2>Beaumont Estate</F2><F3>Burfield Road, Old Windsor</F3><F4>West Berkshire SL4 2JJ</F4><F5>UK</F5><F6>Beaumont Estate
Burfield Road, Old Windsor
West Berkshire SL4 2JJ, UK</F6></Row>')
,(92380, 53, 1259, N'<Row><F1>1231</F1><F2>The Derbyshire Hotel</F2><F3>Carter Ln E</F3><F4>South Normanton, Alfreton DE55 2EH</F4><F5>UK</F5><F6>The Derbyshire Hotel
Carter Ln E
South Normanton, Alfreton DE55 2EH, UK</F6></Row>')
,(92381, 53, 1260, N'<Row><F1>1235</F1><F2>Sedgebrook Hall</F2><F3>Chapel Brampton</F3><F4>Northampton NN6 8BD</F4><F5>UK</F5><F6>Sedgebrook Hall
Chapel Brampton
Northampton NN6 8BD, UK</F6></Row>')
,(92382, 53, 1261, N'<Row><F1>961</F1><F2>Freeport Excalibur</F2><F3>Chvalovice 196 669 02</F3><F4>Chvalovice</F4><F5>Czech Republic</F5><F6>Freeport Excalibur
Chvalovice 196 669 02
Chvalovice, Czech Republic</F6></Row>')
,(92383, 53, 1262, N'<Row><F1>1225</F1><F2>Cranage Hall</F2><F3>Cranage Holmes Chapel, Byley Ln</F3><F4>Cranage, Crewe CW4 8EW</F4><F5>UK</F5><F6>Cranage Hall
Cranage Holmes Chapel, Byley Ln
Cranage, Crewe CW4 8EW, UK</F6></Row>')
,(92384, 53, 1263, N'<Row><F1>962</F1><F2>Trump International Golf Links &amp; Hotel Ireland</F2><F3>Doonbeg</F3><F4>Co. Claire</F4><F5>Ireland</F5><F6>Trump International Golf Links &amp; Hotel Ireland
Doonbeg
Co. Claire, Ireland</F6></Row>')
,(92385, 53, 1264, N'<Row><F1>963</F1><F3>Exchange House, Primrose St</F3><F4>London</F4><F5>UK</F5><F6>Exchange House, Primrose St
London, UK</F6></Row>')
,(92386, 53, 1265, N'<Row><F1>964</F1><F3>Founders Dr</F3><F4>Ashburn</F4><F5>VA</F5><F6>Founders Dr
Ashburn, VA</F6></Row>')
,(92387, 53, 1266, N'<Row><F1>1226</F1><F2>Wotton House</F2><F3>Guilford Rd</F3><F4>Dorking, Surrey RH5 6HS</F4><F5>UK</F5><F6>Wotton House
Guilford Rd
Dorking, Surrey RH5 6HS, UK</F6></Row>')
,(92388, 53, 1267, N'<Row><F1>1222</F1><F2>St. David''s Hotel &amp; Spa </F2><F3>Havannah St</F3><F4>Cardiff CF10 5SD</F4><F5>UK</F5><F6>St. David''s Hotel &amp; Spa 
Havannah St
Cardiff CF10 5SD, UK</F6></Row>')
,(92389, 53, 1268, N'<Row><F1>965</F1><F2>Orr Motors South Parking Lot</F2><F3>Hwy 67/167 &amp; Truman Baker Dr</F3><F4>Searcy</F4><F5>AR</F5><F6>Orr Motors South Parking Lot
Hwy 67/167 &amp; Truman Baker Dr
Searcy, AR</F6></Row>')
,(92390, 53, 1269, N'<Row><F1>966</F1><F2>Inverness Plaza</F2><F3>Inverness Plaza</F3><F4>Birmingham</F4><F5>AL</F5><F6>Inverness Plaza
Inverness Plaza
Birmingham, AL</F6></Row>')
,(92391, 53, 1270, N'<Row><F1>967</F1><F3>J.E. Irausquin Blvd 77</F3><F5>Aruba</F5><F6>J.E. Irausquin Blvd 77
Aruba</F6></Row>')
,(92392, 53, 1271, N'<Row><F1>1236</F1><F2>Kenwood Hall</F2><F3>Kenwood Rd</F3><F4>Sheffield, South Yorkshire S7 1NQ</F4><F5>UK</F5><F6>Kenwood Hall
Kenwood Rd
Sheffield, South Yorkshire S7 1NQ, UK</F6></Row>')
,(92393, 53, 1272, N'<Row><F1>1233</F1><F2>The Met Hotel</F2><F3>King St</F3><F4>Leeds, West Yorkshire LS1 2HQ</F4><F5>UK</F5><F6>The Met Hotel
King St
Leeds, West Yorkshire LS1 2HQ, UK</F6></Row>')
,(92394, 53, 1273, N'<Row><F1>1138</F1><F2>Kingsgate</F2><F3>Kingsgate Shopping Centre</F3><F4>Dunfermline KY12 7QU</F4><F5>UK</F5><F6>Kingsgate
Kingsgate Shopping Centre
Dunfermline KY12 7QU, UK</F6></Row>')
,(92395, 53, 1274, N'<Row><F1>968</F1><F3>Koenigsallee 45</F3><F4>Dusseldorf</F4><F5>Germany</F5><F6>Koenigsallee 45
Dusseldorf, Germany</F6></Row>')
,(92396, 53, 1275, N'<Row><F1>969</F1><F3>Koenigsallee 47</F3><F4>Dusseldorf</F4><F5>Germany</F5><F6>Koenigsallee 47
Dusseldorf, Germany</F6></Row>')
,(92397, 53, 1276, N'<Row><F1>970</F1><F3>Koenigsallee 49-51</F3><F4>Dusseldorf</F4><F5>Germany</F5><F6>Koenigsallee 49-51
Dusseldorf, Germany</F6></Row>')
,(92398, 53, 1277, N'<Row><F1>971</F1><F3>Koenigsallee 53</F3><F4>Dusseldorf</F4><F5>Germany</F5><F6>Koenigsallee 53
Dusseldorf, Germany</F6></Row>')
,(92399, 53, 1278, N'<Row><F1>972</F1><F3>Koenigsallee 55</F3><F4>Dusseldorf</F4><F5>Germany</F5><F6>Koenigsallee 55
Dusseldorf, Germany</F6></Row>')
,(92400, 53, 1279, N'<Row><F1>973</F1><F2>Freeport Kungsbacka</F2><F3>Kungsparksvägen 80 434 39</F3><F4>Kungsbacka</F4><F5>Sweden</F5><F6>Freeport Kungsbacka
Kungsparksvägen 80 434 39
Kungsbacka, Sweden</F6></Row>')
,(92401, 53, 1280, N'<Row><F1>1227</F1><F2>Eastwood Hall</F2><F3>Mansfield Rd</F3><F4>Eastwood, Nottingham NG16 3SS</F4><F5>UK</F5><F6>Eastwood Hall
Mansfield Rd
Eastwood, Nottingham NG16 3SS, UK</F6></Row>')
,(92402, 53, 1281, N'<Row><F1>1229</F1><F2>Horwood House</F2><F3>Mursley Rd</F3><F4>Little Horwood, Milton Keynes MK17 0PH</F4><F5>UK</F5><F6>Horwood House
Mursley Rd
Little Horwood, Milton Keynes MK17 0PH, UK</F6></Row>')
,(92403, 53, 1282, N'<Row><F1>974</F1><F2>Victoria Hill London</F2><F3>N End Rd</F3><F4>London</F4><F5>UK</F5><F6>Victoria Hill London
N End Rd
London, UK</F6></Row>')
,(92404, 53, 1283, N'<Row><F1>1224</F1><F2>Palace Hotel</F2><F3>Oxford St</F3><F4>Manchester M60 7HA</F4><F5>UK</F5><F6>Palace Hotel
Oxford St
Manchester M60 7HA, UK</F6></Row>')
,(92405, 53, 1284, N'<Row><F1>1101</F1><F2>Shutters on the Beach</F2><F3>Pico Blvd</F3><F4>Santa Monica</F4><F5>CA</F5><F6>Shutters on the Beach
Pico Blvd
Santa Monica, CA</F6></Row>')
,(92406, 53, 1285, N'<Row><F1>1121</F1><F3>S Plaza Dr</F3><F4>Tempe</F4><F5>AZ</F5><F6>S Plaza Dr
Tempe, AZ</F6></Row>')
,(92407, 53, 1286, N'<Row><F1>975</F1><F2>Watermark Seaport</F2><F3>Seaport Blvd &amp; Boston Wharf Rd</F3><F4>Boston</F4><F5>MA</F5><F6>Watermark Seaport
Seaport Blvd &amp; Boston Wharf Rd
Boston, MA</F6></Row>')
,(92408, 53, 1287, N'<Row><F1>1223</F1><F2>Royal York Hotel</F2><F3>Station Rd</F3><F4>North Yorkshire YO24 1AA</F4><F5>UK</F5><F6>Royal York Hotel
Station Rd
North Yorkshire YO24 1AA, UK</F6></Row>')
,(92409, 53, 1288, N'<Row><F1>1281</F1><F2>The Savoy Hotel</F2><F3>Strand, Savoy Way</F3><F4>London WC2R 0EU</F4><F5>UK</F5><F6>The Savoy Hotel
Strand, Savoy Way
London WC2R 0EU, UK</F6></Row>')
,(92410, 53, 1289, N'<Row><F1>1140</F1><F2>The Rushes Shopping Centre</F2><F3>The Rushes</F3><F4>Loughborough, Leicestershire LE11 5BE</F4><F5>UK</F5><F6>The Rushes Shopping Centre
The Rushes
Loughborough, Leicestershire LE11 5BE, UK</F6></Row>')
,(92411, 53, 1290, N'<Row><F1>1214</F1><F2>Palazzo Aporti </F2><F3>Via Ferrante Aporti 8</F3><F4>Milan</F4><F5>Italy</F5><F6>Palazzo Aporti 
Via Ferrante Aporti 8
Milan, Italy</F6></Row>')
,(92412, 53, 1291, N'<Row><F1>1134</F1><F2>Frenchgate Centre</F2><F3>West Mall Frenchgate Centre</F3><F4>Doncaster, South Yorkshire DN1 1SR</F4><F5>UK</F5><F6>Frenchgate Centre
West Mall Frenchgate Centre
Doncaster, South Yorkshire DN1 1SR, UK</F6></Row>')
,(92413, 53, 1292, N'<Row><F1>1232</F1><F2>Alexandra House</F2><F3>Whittingham Dr</F3><F4>Wroughton, Swindon SN4 0QJ</F4><F5>UK</F5><F6>Alexandra House
Whittingham Dr
Wroughton, Swindon SN4 0QJ, UK</F6></Row>')
,(92414, 53, 1293, N'<Row><F1>1092</F1><F2>The Courtyard Building</F2><F4>Santa Rosa</F4><F5>CA</F5><F6>The Courtyard Building
Santa Rosa, CA</F6></Row>')
,(92415, 53, 1294, N'<Row><F1>1237</F1><F2>Hawkstone Park</F2><F4>Shrewsbury, Shropshire SY4 5UY</F4><F5>UK</F5><F6>Hawkstone Park
Shrewsbury, Shropshire SY4 5UY, UK</F6></Row>')
,(92416, 53, 1295, N'<Row><F1>1294</F1><F3>315 Park Ave S</F3><F4>New York</F4><F5>NY</F5><F6>315 Park Ave S
New York, NY</F6></Row>')
,(92417, 53, 1296, N'<Row><F1>1295</F1><F3>1881 Campus Commons Dr</F3><F4>Reston</F4><F5>VA</F5><F6>1881 Campus Commons Dr
Reston, VA</F6></Row>')
,(92418, 53, 1297, N'<Row><F1>1296</F1><F2>One Buckhead Plaza</F2><F3>3060 Peachtree Rd NW</F3><F4>Atlanta</F4><F5>GA</F5><F6>One Buckhead Plaza
3060 Peachtree Rd NW
Atlanta, GA</F6></Row>')
,(92419, 53, 1298, N'<Row><F1>1297</F1><F2>Wells Fargo Center</F2><F3>1700 Lincoln St</F3><F4>Denver</F4><F5>CO</F5><F6>Wells Fargo Center
1700 Lincoln St
Denver, CO</F6></Row>')
,(92420, 53, 1299, N'<Row><F1>1298</F1><F2>The Shops at West End</F2><F3>1621 West End Blvd</F3><F4>St. Louis Park</F4><F5>MN</F5><F6>The Shops at West End
1621 West End Blvd
St. Louis Park, MN</F6></Row>')
,(92421, 53, 1300, N'<Row><F1>1299</F1><F2>Loop West</F2><F3>2001-2699 W Osceola Pkwy</F3><F4>Kissimmee</F4><F5>FL</F5><F6>Loop West
2001-2699 W Osceola Pkwy
Kissimmee, FL</F6></Row>')
,(92422, 53, 1301, N'<Row><F1>1300</F1><F3>515 N State St</F3><F4>Chicago</F4><F5>IL</F5><F6>515 N State St
Chicago, IL</F6></Row>')
,(92423, 53, 1302, N'<Row><F1>1301</F1><F2>Chicago Board of Trade Building</F2><F3>141 W Jackson Blvd</F3><F4>Chicago</F4><F5>IL</F5><F6>Chicago Board of Trade Building
141 W Jackson Blvd
Chicago, IL</F6></Row>')
,(92424, 53, 1303, N'<Row><F1>1302</F1><F3>8510 Balboa Blvd</F3><F4>Los Angeles</F4><F5>CA</F5><F6>8510 Balboa Blvd
Los Angeles, CA</F6></Row>')
,(92425, 53, 1304, N'<Row><F1>1303</F1><F3>8550 Balboa Blvd</F3><F4>Los Angeles</F4><F5>CA</F5><F6>8550 Balboa Blvd
Los Angeles, CA</F6></Row>')
,(92426, 53, 1305, N'<Row><F1>1304</F1><F3>8400 Balboa Blvd</F3><F4>Los Angeles</F4><F5>CA</F5><F6>8400 Balboa Blvd
Los Angeles, CA</F6></Row>')
,(92427, 53, 1306, N'<Row><F1>1305</F1><F3>8500 Balboa Blvd</F3><F4>Los Angeles</F4><F5>CA</F5><F6>8500 Balboa Blvd
Los Angeles, CA</F6></Row>')
,(92428, 53, 1307, N'<Row><F1>1306</F1><F2>Three Bryant Park</F2><F3>1095 Avenue of the Americas</F3><F4>New York</F4><F5>NY</F5><F6>Three Bryant Park
1095 Avenue of the Americas
New York, NY</F6></Row>')
,(92429, 53, 1308, N'<Row><F1>1307</F1><F2>Downtown Crown</F2><F3>Sam Eig Hwy &amp; Fields Rd</F3><F4>Gaithersburg</F4><F5>MD</F5><F6>Downtown Crown
Sam Eig Hwy &amp; Fields Rd
Gaithersburg, MD</F6></Row>')
,(92430, 53, 1309, N'<Row><F1>1308</F1><F3>1455 Market St</F3><F4>San Francisco</F4><F5>CA</F5><F6>1455 Market St
San Francisco, CA</F6></Row>')
,(92431, 53, 1310, N'<Row><F1>1309</F1><F3>1345 Avenue of the Americas</F3><F4>New York</F4><F5>NY</F5><F6>1345 Avenue of the Americas
New York, NY</F6></Row>')
,(92432, 53, 1311, N'<Row><F1>1310</F1><F3>605 Third Ave</F3><F4>New York</F4><F5>NY</F5><F6>605 Third Ave
New York, NY</F6></Row>')
,(92433, 53, 1312, N'<Row><F1>1311</F1><F2>International Plaza</F2><F3>2223 North Westshore Blvd</F3><F4>Tampa</F4><F5>FL</F5><F6>International Plaza
2223 North Westshore Blvd
Tampa, FL</F6></Row>')
,(92434, 53, 1313, N'<Row><F1>1312</F1><F2>Times Square Tower</F2><F3>7 Times Sq</F3><F4>New York</F4><F5>NY</F5><F6>Times Square Tower
7 Times Sq
New York, NY</F6></Row>')
,(92435, 53, 1314, N'<Row><F1>1313</F1><F3>1300 Post Oak Blvd</F3><F4>Houston</F4><F5>TX</F5><F6>1300 Post Oak Blvd
Houston, TX</F6></Row>')
,(92436, 53, 1315, N'<Row><F1>1314</F1><F3>1330 Post Oak Blvd</F3><F4>Houston</F4><F5>TX</F5><F6>1330 Post Oak Blvd
Houston, TX</F6></Row>')
,(92437, 53, 1316, N'<Row><F1>1315</F1><F3>1360 Post Oak Blvd</F3><F4>Houston</F4><F5>TX</F5><F6>1360 Post Oak Blvd
Houston, TX</F6></Row>')
,(92438, 53, 1317, N'<Row><F1>1316</F1><F3>1400 Post Oak Blvd</F3><F4>Houston</F4><F5>TX</F5><F6>1400 Post Oak Blvd
Houston, TX</F6></Row>')
,(92439, 53, 1318, N'<Row><F1>1317</F1><F2>Time Warner Center</F2><F3>10 Columbus Cir</F3><F4>New York</F4><F5>NY</F5><F6>Time Warner Center
10 Columbus Cir
New York, NY</F6></Row>')
,(92440, 53, 1319, N'<Row><F1>1318</F1><F2>Waldorf Astoria New York</F2><F3>301 Park Ave</F3><F4>New York</F4><F5>NY</F5><F6>Waldorf Astoria New York
301 Park Ave
New York, NY</F6></Row>')
,(92441, 53, 1320, N'<Row><F1>1319</F1><F3>199 S Los Robles</F3><F4>Pasadena</F4><F5>CA</F5><F6>199 S Los Robles
Pasadena, CA</F6></Row>')
,(92442, 53, 1321, N'<Row><F1>1320</F1><F3>837 Washington St</F3><F4>New York</F4><F5>NY</F5><F6>837 Washington St
New York, NY</F6></Row>')
,(92443, 53, 1322, N'<Row><F1>1321</F1><F2>Hotel Carter</F2><F3>250 W 43rd St</F3><F4>New York</F4><F5>NY</F5><F6>Hotel Carter
250 W 43rd St
New York, NY</F6></Row>')
,(92444, 53, 1323, N'<Row><F1>1322</F1><F2>Prominent Pointe - Building 1</F2><F3>8310 N Capital of Texas Hwy</F3><F4>Austin</F4><F5>TX</F5><F6>Prominent Pointe - Building 1
8310 N Capital of Texas Hwy
Austin, TX</F6></Row>')
,(92445, 53, 1324, N'<Row><F1>1323</F1><F2>Prominent Pointe - Building 2</F2><F3>8310 N Capital of Texas Hwy</F3><F4>Austin</F4><F5>TX</F5><F6>Prominent Pointe - Building 2
8310 N Capital of Texas Hwy
Austin, TX</F6></Row>')
,(92446, 53, 1325, N'<Row><F1>1324</F1><F2>Hyatt Regency Indianapolis</F2><F3>1 S Capitol Ave</F3><F4>Indianapolis</F4><F5>IN</F5><F6>Hyatt Regency Indianapolis
1 S Capitol Ave
Indianapolis, IN</F6></Row>')
,(92447, 53, 1326, N'<Row><F1>1325</F1><F2>Sammamish Parkplace - Building C</F2><F3>21925 SE 51st St</F3><F4>Issaquah</F4><F5>WA</F5><F6>Sammamish Parkplace - Building C
21925 SE 51st St
Issaquah, WA</F6></Row>')
,(92448, 53, 1327, N'<Row><F1>1326</F1><F2>Sammamish Parkplace - Building D</F2><F3>21933 SE 51st St</F3><F4>Issaquah</F4><F5>WA</F5><F6>Sammamish Parkplace - Building D
21933 SE 51st St
Issaquah, WA</F6></Row>')
,(92449, 53, 1328, N'<Row><F1>1327</F1><F2>Sammamish Parkplace - Building E</F2><F3>21930 SE 51st St</F3><F4>Issaquah</F4><F5>WA</F5><F6>Sammamish Parkplace - Building E
21930 SE 51st St
Issaquah, WA</F6></Row>')
,(92450, 53, 1329, N'<Row><F1>1327</F1><F6>1327</F6></Row>')
,(92451, 54, 1, N'<Row><F1>Tickerid</F1><F2>Ticker</F2></Row>')
,(92452, 54, 2, N'<Row><F1>1</F1><F2>BXP</F2></Row>')
,(92453, 54, 3, N'<Row><F1>2</F1><F2>BDN</F2></Row>')
,(92454, 54, 4, N'<Row><F1>3</F1><F2>CPT</F2></Row>')
,(92455, 54, 5, N'<Row><F1>4</F1><F2>CT</F2></Row>')
,(92456, 54, 6, N'<Row><F1>5</F1><F2>CNT</F2></Row>')
,(92457, 54, 7, N'<Row><F1>6</F1><F2>CLP</F2></Row>')
,(92458, 54, 8, N'<Row><F1>7</F1><F2>GGP</F2></Row>')
,(92459, 54, 9, N'<Row><F1>8</F1><F2>GRT</F2></Row>')
,(92460, 54, 10, N'<Row><F1>9</F1><F2>HR</F2></Row>')
,(92461, 54, 11, N'<Row><F1>10</F1><F2>HPT</F2></Row>')
,(92462, 54, 12, N'<Row><F1>11</F1><F2>LHO</F2></Row>')
,(92463, 54, 13, N'<Row><F1>12</F1><F2>LRY</F2></Row>')
,(92464, 54, 14, N'<Row><F1>13</F1><F2>PKY</F2></Row>')
,(92465, 54, 15, N'<Row><F1>14</F1><F2>PSB</F2></Row>')
,(92466, 54, 16, N'<Row><F1>15</F1><F2>O</F2></Row>')
,(92467, 54, 17, N'<Row><F1>16</F1><F2>SLG</F2></Row>')
,(92468, 54, 18, N'<Row><F1>17</F1><F2>VNO</F2></Row>')
,(92469, 54, 19, N'<Row><F1>18</F1><F2>WRI</F2></Row>')
,(92470, 54, 20, N'<Row><F1>19</F1><F2>PEI</F2></Row>')
,(92471, 54, 21, N'<Row><F1>20</F1><F2>SSS</F2></Row>')
,(92472, 54, 22, N'<Row><F1>21</F1><F2>HCN</F2></Row>')
,(92473, 54, 23, N'<Row><F1>22</F1><F2>SSI</F2></Row>')
,(92474, 54, 24, N'<Row><F1>23</F1><F2>OFC</F2></Row>')
,(92475, 54, 25, N'<Row><F1>24</F1><F2>NHP</F2></Row>')
,(92476, 54, 26, N'<Row><F1>25</F1><F2>EQY</F2></Row>')
,(92477, 54, 27, N'<Row><F1>26</F1><F2>ESS</F2></Row>')
,(92478, 54, 28, N'<Row><F1>27</F1><F2>PSA</F2></Row>')
,(92479, 54, 29, N'<Row><F1>28</F1><F2>BFS</F2></Row>')
,(92480, 54, 30, N'<Row><F1>29</F1><F2>TCO</F2></Row>')
,(92481, 54, 31, N'<Row><F1>30</F1><F2>CARS</F2></Row>')
,(92482, 54, 32, N'<Row><F1>31</F1><F2>GBP</F2></Row>')
,(92483, 54, 33, N'<Row><F1>32</F1><F2>BRE</F2></Row>')
,(92484, 54, 34, N'<Row><F1>33</F1><F2>CUZ</F2></Row>')
,(92485, 54, 35, N'<Row><F1>34</F1><F2>XEL</F2></Row>')
,(92486, 54, 36, N'<Row><F1>35</F1><F2>HIW</F2></Row>')
,(92487, 54, 37, N'<Row><F1>36</F1><F2>PPS</F2></Row>')
,(92488, 54, 38, N'<Row><F1>37</F1><F2>RPT</F2></Row>')
,(92489, 54, 39, N'<Row><F1>38</F1><F2>ARI</F2></Row>')
,(92490, 54, 40, N'<Row><F1>39</F1><F2>RWT</F2></Row>')
,(92491, 54, 41, N'<Row><F1>40</F1><F2>ATLRS</F2></Row>')
,(92492, 54, 42, N'<Row><F1>41</F1><F2>BRI</F2></Row>')
,(92493, 54, 43, N'<Row><F1>42</F1><F2>FR</F2></Row>')
,(92494, 54, 44, N'<Row><F1>43</F1><F2>CBL</F2></Row>')
,(92495, 54, 45, N'<Row><F1>44</F1><F2>FRW</F2></Row>')
,(92496, 54, 46, N'<Row><F1>45</F1><F2>MAA</F2></Row>')
,(92497, 54, 47, N'<Row><F1>46</F1><F2>AMB</F2></Row>')
,(92498, 54, 48, N'<Row><F1>47</F1><F2>AEC</F2></Row>')
,(92499, 54, 49, N'<Row><F1>48</F1><F2>GLB</F2></Row>')
,(92500, 54, 50, N'<Row><F1>49</F1><F2>RSE</F2></Row>')
,(92501, 54, 51, N'<Row><F1>50</F1><F2>EQR</F2></Row>')
,(92502, 54, 52, N'<Row><F1>51</F1><F2>CEI</F2></Row>')
,(92503, 54, 53, N'<Row><F1>52</F1><F2>SKT</F2></Row>')
,(92504, 54, 54, N'<Row><F1>53</F1><F2>KIM</F2></Row>')
,(92505, 54, 55, N'<Row><F1>54</F1><F2>FRT</F2></Row>')
,(92506, 54, 56, N'<Row><F1>55</F1><F2>KRC</F2></Row>')
,(92507, 54, 57, N'<Row><F1>56</F1><F2>MAC</F2></Row>')
,(92508, 54, 58, N'<Row><F1>57</F1><F2>AIV</F2></Row>')
,(92509, 54, 59, N'<Row><F1>58</F1><F2>HME</F2></Row>')
,(92510, 54, 60, N'<Row><F1>59</F1><F2>WRE</F2></Row>')
,(92511, 54, 61, N'<Row><F1>60</F1><F2>BX</F2></Row>')
,(92512, 54, 62, N'<Row><F1>61</F1><F2>MS</F2></Row>')
,(92513, 54, 63, N'<Row><F1>62</F1><F2>JPM</F2></Row>')
,(92514, 54, 64, N'<Row><F1>63</F1><F2>BAC</F2></Row>')
,(92515, 54, 65, N'<Row><F1>64</F1><F2>WFC</F2></Row>')
,(92516, 54, 66, N'<Row><F1>65</F1><F2>DB</F2></Row>')
,(92517, 54, 67, N'<Row><F1>66</F1><F2>MET</F2></Row>')
,(92518, 54, 68, N'<Row><F1>67</F1><F2>RPAI</F2></Row>')
,(92519, 55, 1, N'<Row><F1>New York</F1><F2>40 West 57th Street</F2><F3>22nd Floor</F3><F4>New York, NY 10019</F4><F5>(212) 315-7200</F5><F6>(212) 315-3602</F6></Row>')
,(92520, 55, 2, N'<Row><F1>Los Angeles</F1><F2>100 Wilshire Boulevard</F2><F3>Suite 1500</F3><F4>Santa Monica, CA 90401</F4><F5>(310) 526-9000</F5><F6>(310) 526-9050</F6></Row>')
,(92521, 55, 3, N'<Row><F1>Atlanta</F1><F2>3414 Peachtree Road, N.E.</F2><F3>Suite 1450</F3><F4>Atlanta, GA 30326</F4><F5>(404) 487-1100</F5><F6>(404) 487-1107</F6></Row>')
,(92522, 55, 4, N'<Row><F1>Boston</F1><F2>Two International Place</F2><F3>Fifth Floor</F3><F4>Boston, MA 02110</F4><F5>(617) 449-3410</F5><F6>(617) 449-3444</F6></Row>')
,(92523, 55, 5, N'<Row><F1>Chicago</F1><F2>111 South Wacker Drive</F2><F3>Suite 3340</F3><F4>Chicago, IL 60606</F4><F5>(312) 601-9000</F5><F6>(312) 601-9030</F6></Row>')
,(92524, 55, 6, N'<Row><F1>Dallas</F1><F2>3102 Oak Lawn Avenue</F2><F3>Suite 605</F3><F4>Dallas, TX 75219</F4><F5>(214) 218-1233</F5></Row>')
,(92525, 55, 7, N'<Row><F1>Orange County</F1><F2>3161 Michelson Drive</F2><F3>Suite 1650</F3><F4>Irvine, CA 92612</F4><F5>(949) 930-7474</F5><F6>(949) 930-7475</F6></Row>')
,(92526, 55, 8, N'<Row><F1>Las Vegas</F1><F2>9890 S. Maryland Parkway</F2><F3>Suite 200</F3><F4>Las Vegas, NV 89183</F4><F5>(702) 313 7003</F5><F6>(702) 933-0567</F6></Row>')
,(92527, 55, 9, N'<Row><F1>San Francisco</F1><F2>101 California Street</F2><F3>Suite 2950</F3><F4>San Francisco, CA 94111</F4><F5>(415) 228-2900</F5><F6>(415) 228-3023</F6></Row>')
,(92528, 55, 10, N'<Row><F1>Silicon Valley</F1><F2>160 West Santa Clara Street</F2><F3>Suite 1075</F3><F4>San Jose, CA 95113</F4><F5>(408) 533-9200</F5><F6>(408) 533-9205</F6></Row>')
,(92529, 55, 11, N'<Row><F1>Washington, D.C.</F1><F2>1730 Pennsylvania Avenue, NW</F2><F3>Suite 900</F3><F4>Washington, DC 20006</F4><F5>(202) 688-4000</F5><F6>(202) 688-4010</F6></Row>')
,(92530, 55, 12, N'<Row><F1>Hong Kong</F1><F2>27/F, Three Pacific Place</F2><F3>1 Queen''s Road East</F3><F4>Admiralty, Hong Kong SAR</F4><F5>+852.3650.8566 </F5><F6>+852.2105.5505</F6></Row>')
,(92531, 55, 13, N'<Row><F1>London</F1><F2>4 – 16 Berkeley Square</F2><F3>3rd Floor, Suite 5</F3><F4>London W1J 6BR</F4><F5>+44 20 7074 4950</F5><F6>+44 20 7074 4999</F6></Row>')
)x([StagedExcelData_Key],[ExcelSourceSheet_Key],[RowNumber],[RowData]);
PRINT CAST(@@RowCount as nvarchar)+' rows inserted into dExcelETL.StagedExcelData';
SET IDENTITY_INSERT dExcelETL.StagedExcelData OFF;

﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/10/15
-- Description: Inserts or updates a comment upon a specific object.
--              When @pCommentID is not null @pPrimaryMentionID is ignored.  You cannot change @pPrimaryMentionID.
--              When @pPrimaryMentionID is null and @pCommentID is null the comment is inserted onto the user of the access token
-- =============================================
CREATE PROCEDURE xESxApp.PutComment
     @pAccessToken       iESx.TYP_AccessToken      -- This token represents the user on whose behalf the Put is being made.
    ,@pPrimaryMentionID  iESx.TYP_Reference        -- A legal MentionID identifying the object upon which the comment should be created.
    ,@pCommentText          nvarchar(max)          -- The comment to attach to the specified row
    ,@pCommentID            int = NULL             -- When supplied, the comment must already exist and @pPrimaryMentionID must be correct or null	
	,@pCreateReminder		bit = 0
    ,@pTagTable xESxApp.TVP_Reference READONLY     -- A list of tags to associate with the comment
    ,@pMentionTable xESxApp.TVP_Reference READONLY -- A list of mentions to associate with the comment
	,@pDocumentTable xESxApp.TVP_Document READONLY -- A list of documents to associate with the comment
AS
BEGIN
    SET NOCOUNT ON;

    /*
    DECLARE @pAccessToken       iESx.TYP_AccessToken    = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
    DECLARE @pPrimaryMentionID  iESx.TYP_Reference      = '@AnyPerson|E'
    DECLARE @pCommentText          nvarchar(max)        = 'aNY oLD tRASH'
    DECLARE @pCommentID            int                  = NULL
    DECLARE @pTagTable          xESxApp.TVP_Reference   READONLY;
     INSERT INTO @pTagTable
     VALUES ('#sOMEtAG');
    DECLARE @pMentionTable      xESxApp.TVP_Reference   READONLY;
     INSERT INTO @pMentionTable
     VALUES ('@AnotherPerson|E');
    --*/

    -- *******************************************
    --  Validate arguments
    -- *******************************************

    -- SECURITY
    DECLARE @UserKey    int;

    SELECT @UserKey = GAcCx.User_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcCx;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

    -- No duplicate references to the same Tag
    IF EXISTS (SELECT 1
                 FROM @pTagTable
                GROUP BY ReferenceName
               HAVING COUNT(*) > 1)
    BEGIN
        RAISERROR ('Duplicate Tags on the same comment are not allowed',16,10);
        RETURN;
    END;

    -- Tags must start with #
    IF EXISTS (SELECT 1
                 FROM @pTagTable
                WHERE ReferenceName NOT LIKE '#%')
    BEGIN
        RAISERROR ('ALL Tags IN @pTagTable must begin with #',16,11);
        RETURN;
    END;

    -- No duplicate references to the same Mention
    --IF EXISTS (SELECT 1
    --             FROM (SELECT ReferenceName FROM @pMentionTable UNION ALL SELECT @pPrimaryMentionID) X
    --            GROUP BY X.ReferenceName
    --           HAVING COUNT(*) > 1)
    --BEGIN
    --    RAISERROR ('Duplicate Mentions on the same comment are not allowed',16,20);
    --    RETURN;
    --END;

    -- All text mentions must exist.  Jump some hoops to make the error tell the user which one(s).
    --                                     0        1         2         3
    --                                     1234567890123456789012345678901
    DECLARE @BadMentions nvarchar(1024) = 'Bad @pMentionTable rows exist: ';

    SELECT TOP(5)
           @BadMentions += (pMTa.ReferenceName + ', ')
      FROM @pMentionTable pMTa
      LEFT JOIN dESx.Mention M
        ON M.MentionID = pMTa.ReferenceName
     WHERE M.MentionID IS NULL;

     IF LEN(@BadMentions) > 31
    BEGIN
        RAISERROR (@BadMentions,16,21);
        RETURN;
    END;

    -- Validate CommentID
    IF @pCommentID IS NOT NULL
    AND NOT EXISTS (SELECT 1
                      FROM dESx.Comment Cm
                     WHERE Cm.Comment_Key = @pCommentID)
    BEGIN
        RAISERROR ('@pCommentID must exist when non-null',16,30);
        RETURN;
    END;

    -- Mention key is required any time the ID is supplied
    DECLARE @MentionKey int;
    IF @pPrimaryMentionID IS NOT NULL
        SELECT @MentionKey = M.Mention_Key
           FROM dESx.Mention M
          WHERE M.MentionID = @pPrimaryMentionID;

    -- When CommentID is not supplied then primary mention must be supplied and be valid
    IF @pPrimaryMentionID IS NOT NULL AND @MentionKey IS NULL
    BEGIN
        RAISERROR ('@pPrimaryMentionID supplied but does not exist',16,31);
        RETURN;
    END;

    -- CommentID is required when primary mention is null.  IOW primary mention is required unless we're updating text.
    IF @pCommentID IS NULL AND @pPrimaryMentionID IS NULL
    BEGIN
        RAISERROR ('@pPrimaryMentionID must be supplied when @pCommentID is null',16,32);
        RETURN;
    END;

    -- Determine if the operation is legal and, if so, should it be an insert or update
    DECLARE @Exists int = (SELECT CASE WHEN Cm.Implicit__Mention_Key = @MentionKey
                                       THEN 0
                                       ELSE 1 END
                             FROM dESx.Comment Cm
                            WHERE Cm.Comment_Key = @pCommentID);

    -- Only NULL (insert) and Zero are legal. On update MentionID cannot change.
    IF @Exists = 1
    BEGIN
        RAISERROR ('Attempt to update @pCommentID that does not apply to @pPrimaryMentionID',16,40);
        RETURN;
    END;

    -- *******************************************
    --          UpSert the Comment itself
    -- *******************************************

    -- Null means an insert.  Which means we must also capture the Comment_Key
    IF @Exists IS NULL BEGIN
        DECLARE @InsertedComments   table (Comment_Key int);

        INSERT INTO dESx.Comment (Implicit__Mention_Key,CommentText,Create__User_Key,Update__User_Key)
        OUTPUT inserted.Comment_Key
          INTO @InsertedComments
        VALUES (@MentionKey,@pCommentText,@UserKey,@UserKey);

        SELECT @pCommentID = Comment_Key FROM @InsertedComments;
    END;

    -- An update must delete existing references since passed references are cannonical & merge is impractical
    ELSE BEGIN
        DELETE dESx.Comment_Tag
         WHERE Comment_Key = @pCommentID;

        DELETE dESx.Comment_Mention
         WHERE Comment_Key = @pCommentID;

        UPDATE dESx.Comment
           SET CommentText = @pCommentText
              ,UpdateTime = GETUTCDATE()
         WHERE Comment_Key = @pCommentID;
    END;

    -- *******************************************
    --         Create new Tags as needed
    -- *******************************************

    INSERT dESx.Tag (Tag_Key)
    SELECT src.ReferenceName
      FROM @pTagTable src
     WHERE NOT EXISTS (SELECT T.Tag_Key
                         FROM dESx.Tag T
                        WHERE T.Tag_Key = src.ReferenceName);

    -- *******************************************
    --          Link tags to comment
    -- *******************************************

    DECLARE @InsertedCommentTags table (Tag_Key iESx.TYP_Reference);

    INSERT dESx.Comment_Tag (Comment_Key,Tag_Key)
    OUTPUT inserted.Tag_Key
      INTO @InsertedCommentTags
    SELECT @pCommentID, T.Tag_Key
      FROM @pTagTable src
      JOIN dESx.Tag T
        ON T.Tag_Key = src.ReferenceName;

    -- *******************************************
    --        Insure tag usage rows exist
    -- *******************************************

    INSERT dESx.TagUsage (User_Key,Tag_Key,UseCount,LastUseDate)
    SELECT @UserKey, src.Tag_Key, 1, GETUTCDATE()
      FROM @InsertedCommentTags src
     WHERE NOT EXISTS (SELECT 1
                         FROM dESx.TagUsage TUs
                        WHERE TUs.User_Key = @UserKey
                          AND TUs.Tag_Key = src.Tag_Key);

    -- *******************************************
    --          Bump tag usage counters
    -- *******************************************

    UPDATE dESx.Tag
       SET TotalUseCount += 1
          ,LastUseDate = GETUTCDATE()
     WHERE Tag_Key IN (SELECT Tag_Key FROM @InsertedCommentTags);

    UPDATE dESx.TagUsage
       SET UseCount += 1
          ,LastUseDate = GETUTCDATE()
     WHERE User_Key = @UserKey
       AND Tag_Key IN (SELECT Tag_Key FROM @InsertedCommentTags);

    -- *******************************************
    --          Link mentions to comment
    -- *******************************************

    DECLARE @InsertedCommentMentions table (Mention_Key int);

    INSERT dESx.Comment_Mention (Comment_Key,Mention_Key)
    OUTPUT inserted.Mention_Key
      INTO @InsertedCommentMentions
    SELECT @pCommentID, M.Mention_Key
      FROM @pMentionTable src
      JOIN dESx.Mention M
        ON M.MentionID = src.ReferenceName;


	-- *******************************************
    --          Link documents to a comment
    -- *******************************************
	INSERT dESx.Comment_Attachment(Comment_Key, Document_Key, Create__User_Key, Update__User_Key)
    SELECT @pCommentID, D.Document_Key, @UserKey, @UserKey
      FROM @pDocumentTable src
      JOIN dESx.Document D
        ON D.Document_Key = src.DocumentKey


	-- *******************************************
    --       Create Reminder if necessary
    -- *******************************************
	IF @pCreateReminder = 1
	BEGIN
		INSERT dESx.Reminder(Comment_Key, Create__User_Key, Update__User_Key)
		SELECT @pCommentID, @UserKey, @UserKey
	END


    -- *******************************************
    --        Insure mention usage rows exist
    -- *******************************************

    INSERT dESx.UserMentionData (User_Key,Mention_Key,UseCount,LastUseDate)
    SELECT @UserKey, src.Mention_Key, 1, GETUTCDATE()
      FROM @InsertedCommentMentions src
     WHERE NOT EXISTS (SELECT 1
                         FROM dESx.UserMentionData UMDa
                        WHERE UMDa.User_Key = @UserKey
                          AND UMDa.Mention_Key = src.Mention_Key);

    -- *******************************************
    --          Bump mention usage counters
    -- *******************************************

    UPDATE dESx.Mention
       SET TotalUseCount += 1
          ,LastUseDate = GETUTCDATE()
     WHERE MentionID IN (SELECT MentionID FROM @InsertedCommentMentions);

    UPDATE dESx.UserMentionData
       SET UseCount += 1
          ,LastUseDate = GETUTCDATE()
     WHERE User_Key = @UserKey
       AND Mention_Key IN (SELECT Mention_Key FROM @InsertedCommentMentions);

    -- *******************************************
    --          Return the comment's key
    -- *******************************************

    RETURN @pCommentID;
END;
/*Test
GO
select * from dESx.Mention;
SELECT * FROM dESx.Comment;
SELECT * FROM dESx.Tag;
SELECT * FROM dESx.Comment_Tag;
SELECT * FROM dESx.TagUsage;

DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;

DECLARE @TagList xESxApp.TVP_Reference;
DECLARE @MentionList xESxApp.TVP_Reference;
DECLARE @CommentKey int;
-- A COMMENT WITH NO TAGS
--EXEC @CommentKey = xESxApp.PutComment @pAccessToken,'@FirstUser|E','i DON''T THINK IT MATTERS',NULL,@TagList
-- A COMMENT WITH SEVERAL TAGS
INSERT INTO @TagList VALUES ('#tESTtAG1'),('#tESTtAG2');
--EXEC @CommentKey = xESxApp.PutComment @pAccessToken,'@FirstUser|E','aNOTHER COMMENT. sEVERAL TAGS.',NULL,@TagList
-- UPDATE THE COMMENT WITH SEVERAL TAGS SUCH THAT IT CONTAINS MORE NOW
INSERT INTO @TagList VALUES ('#tESTtAG3');
-- This should throw for mismatched keys
--EXEC @CommentKey = xESxApp.PutComment @pAccessToken,'@SecondUser|E','uPDATED COMMENT. dIFFERENT TAG LIST.',2,@TagList
EXEC xESxApp.Login 'User2',@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC @CommentKey = xESxApp.PutComment @AccessToken,'@SomeEmployee|E','uPDATED COMMENT. sHORT TAG LIST.',2,@TagList,@MentionList;
SELECT @CommentKey;
SELECT * FROM dESx.Comment;
SELECT * FROM dESx.Tag;
SELECT * FROM dESx.Comment_Tag;
SELECT * FROM dESx.TagUsage;
--*/
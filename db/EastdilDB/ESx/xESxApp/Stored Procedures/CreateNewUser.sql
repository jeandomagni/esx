﻿/*TODO
-look into reusing the createNewMention proc 
-look into validating the user to see if not already added, although this is done on the app
-look into returnig user asset output variables so no need to login again after adding new user
*/

CREATE PROCEDURE [xESxApp].[CreateNewUser]
     @UserName      sysname
	,@FullName		nvarchar(100)		
	,@pMentionKey   int OUTPUT 
AS
    SET NOCOUNT ON;

    DECLARE @ContactKey int;
    DECLARE @HasAccess  bit;
	DECLARE @RawMentionID       iESx.TYP_Reference;
    DECLARE @Result     int = 1;
	DECLARE @pObjectType   char(1) = 'C';
	DECLARE @DefaultMentionValue nvarchar(40) = (SELECT NEWID());
	DECLARE @ErrorText          nvarchar(100) = '';
	DECLARE @Uniquifier         varchar(3) = '';
	DECLARE @pAccessToken  iESx.TYP_AccessToken;
    DECLARE @pMentionID    iESx.TYP_Reference;
	DECLARE @pUserKey		int	;

    SET @pAccessToken   = NULL;
    SET @pMentionID     = NULL;

	BEGIN TRANSACTION;
	DECLARE @contactKeyT table (Contact_Key int);

	INSERT INTO dESx.[Contact](FullName,LastName, IsDefunct, IsSolicitable, CreatedDate,UpdatedDate, CreatedBy, AdEntID)
	OUTPUT inserted.Contact_Key into @contactKeyT
	VALUES(@FullName,@FullName,0,0, GETUTCDATE(), GETUTCDATE(), @UserName, @UserName)

	SET @ContactKey = (SELECT Contact_Key FROM @contactKeyT);

	DECLARE @userKeyT table (User_Key int);

	INSERT INTO dESx.[User](LoginName,LastLoginDate,HasAccess,Contact_Key)
	OUTPUT inserted.Contact_Key into @userKeyT
	VALUES(@UserName, GETUTCDATE(),1, @ContactKey);

	SET @pUserKey = (SELECT User_Key FROM @userKeyT); 
	SET @pAccessToken = NEWID();

	SET @RawMentionID = iESx.CalculateRawMentionName(@pObjectType, @ContactKey,@DefaultMentionValue);
    IF @RawMentionID IS NULL
    BEGIN
        SET @ErrorText = 'Key '+CAST(@ContactKey AS varchar)+' of type "'+@pObjectType+'" must exist but was not found';
        RAISERROR (@ErrorText,16,0);
        RETURN;
    END;
        
    SET @Uniquifier = iESx.GetMentionNameUniquifier(@pObjectType,@RawMentionID);
	SET @pMentionID = iESx.FormatMentionName(@pObjectType,@RawMentionID,@Uniquifier);

	
        DECLARE @mentionKeyT table (Mention_Key int);
        INSERT dESx.Mention(MentionID,IsPrimary,Contact_Key,LastUseDate,TotalUseCount,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy)
        OUTPUT Inserted.Mention_Key
          INTO @mentionKeyT
        VALUES  (@pMentionID,1 ,@ContactKey,GETUTCDATE(),0,GETUTCDATE(),@UserName,GETUTCDATE(),NULL);
    COMMIT TRANSACTION;

	SET @pMentionKey = (SELECT Mention_Key FROM @mentionKeyT);


/*
DECLARE @pMentionKey INT;
exec xESxApp.CreateNewUser 'User1', 'jean', @pMentionKey OUTPUT;
SELECT @pMentionKey;
*/
  
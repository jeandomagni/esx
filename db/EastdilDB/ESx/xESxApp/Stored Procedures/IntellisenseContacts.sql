﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/12/08
-- Description: Returns the top x Contacts that match the prefix passed, as sorted by relevance.
-- =============================================
CREATE PROCEDURE xESxApp.IntellisenseContacts
     @pAccessToken      iESx.TYP_AccessToken      -- This token represents the user on whose behalf the call is being made.
    ,@pNamePattern      iESx.TYP_Reference
    ,@pMaxRows          int                  = 10
AS
BEGIN
    SET NOCOUNT ON;
    /*
    DECLARE @pAccessToken iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
    DECLARE @pNamePattern iESx.TYP_Reference   = '';
    DECLARE @pMaxRows int                      = 4;
    --*/

    DECLARE @UserKey        int;
    DECLARE @SearchPattern  iESx.TYP_Reference  = +'%'+SUBSTRING(@pNamePattern,1,127)+'%';
    DECLARE @MentionPattern iESx.TYP_Reference  = '@'+SUBSTRING(@pNamePattern,1,127)+'%';
    DECLARE @Potentials     table(MentionID iESx.TYP_Reference,FullName nvarchar(50),LastUseDate date,UseCount int,GroupType int);

    SELECT @UserKey     = GAcTo.User_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcTo;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

    INSERT INTO @Potentials
    SELECT TOP (@pMaxRows)                -- Users personal Mentions
           M.MentionID
          ,CN.FullName
          ,UMDa.LastUseDate
          ,UMDa.UseCount
          ,1 GroupType                    -- Users Mentions are 'Most Relevant'
      FROM dESx.Mention M
      JOIN dESx.UserMentionData UMDa
        ON UMDa.Mention_Key = M.Mention_Key
      JOIN dESx.Contact CN
        ON CN.Contact_Key = M.Contact_Key
     WHERE (M.MentionID LIKE @MentionPattern
           OR CN.FullName LIKE @SearchPattern)
       AND UMDa.User_Key = @UserKey
     ORDER BY 2 DESC,3 DESC,1;

    INSERT INTO @Potentials
    SELECT TOP (@pMaxRows)                -- Recently created Mentions
           M.MentionID
          ,CN.FullName
          ,M.CreatedDate
          ,M.TotalUseCount
          ,2 GroupType                    -- listed 2nd
      FROM dESx.Mention M
      JOIN dESx.Contact CN
        ON CN.Contact_Key = M.Contact_Key
     WHERE (M.MentionID LIKE @MentionPattern
           OR CN.FullName LIKE @SearchPattern)
     ORDER BY 2 DESC,3 DESC,1;

    INSERT INTO @Potentials
    SELECT TOP (@pMaxRows)                -- Mentions recently used by others
           M.MentionID
          ,CN.FullName
          ,M.LastUseDate
          ,M.TotalUseCount
          ,3 GroupType                    -- Listed last
      FROM dESx.Mention M
      JOIN dESx.Contact CN
        ON CN.Contact_Key = M.Contact_Key
     WHERE (M.MentionID LIKE @MentionPattern
           OR CN.FullName LIKE @SearchPattern)
     ORDER BY 2 DESC,3 DESC,1;

--  DECLARE @pMaxRows int                 = 5;
    WITH X AS (   -- Order each GroupType by LastUsedDate and UseCount
        SELECT pT.MentionID
              ,pT.FullName
              ,pT.LastUseDate
              ,pT.UseCount
              ,pT.GroupType
              ,ROW_NUMBER() OVER(PARTITION BY pT.GroupType ORDER BY pT.LastUseDate DESC,pT.UseCount DESC) GroupRow
          FROM @Potentials pT
    ), Y AS (     -- Mark duplicates with NameRow > 1 where kept row is the one in the highest order group type
        SELECT X.MentionID
              ,X.FullName
              ,X.LastUseDate
              ,X.UseCount
              ,X.GroupType
              ,X.GroupRow
              ,ROW_NUMBER() OVER(PARTITION BY X.MentionID ORDER BY X.GroupRow,X.GroupType) NameRow
          FROM X
    ), Z AS (     -- Pick the first occurance of each relevant name
        SELECT TOP (@pMaxRows)
               Y.MentionID
              ,Y.FullName
              ,Y.LastUseDate
              ,Y.UseCount
              ,Y.GroupType
          FROM Y
         WHERE Y.NameRow = 1
         ORDER BY Y.GroupRow,Y.GroupType
    )
    -- Return the names with their corresponding Relevance
    SELECT Z.MentionID
          ,Z.FullName
          ,ROW_NUMBER() OVER(ORDER BY Z.GroupType ASC, Z.LastUseDate DESC, Z.UseCount DESC) Relevance
      FROM Z
     ORDER BY Relevance;

END;
/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC xESxApp.IntellisenseContacts @pAccessToken = @AccessToken,@pNamePattern = '' ,@pMaxRows = 4;
EXEC xESxApp.IntellisenseContacts @pAccessToken = @AccessToken,@pNamePattern = 's',@pMaxRows = 4;
EXEC xESxApp.IntellisenseContacts @pAccessToken = @AccessToken,@pNamePattern = 'a',@pMaxRows = 5;
EXEC xESxApp.IntellisenseContacts @pAccessToken = @AccessToken,@pNamePattern = 'a',@pMaxRows = 6;
EXEC xESxApp.IntellisenseContacts @pAccessToken = @AccessToken,@pNamePattern = 'a',@pMaxRows = 7;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/12/04
-- Description: Returns a single row with each column containing a count of a different entity type tied to the company.
--              This list of counts is for the company summary page.
--              See GetCompanyEntityCount for the counts on the company list page.
-- =============================================
CREATE PROCEDURE xESxApp.GetCompanySummaryEntityCount
     @pAccessToken              iESx.TYP_AccessToken    -- This token represents the user on whose behalf the request is being made.
    ,@pMentionID                iESx.TYP_Reference      -- The company whose data should be returned.
    ,@pPitchesSinceDays         int
    ,@pBidsSinceDays            int
    ,@pLoansMaturingBeforeDays  int
    ,@pHoldPeriodMonths         int
AS
    SET NOCOUNT ON;

/*
    DECLARE @pAccessToken   iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
    DECLARE @pMentionID     iESx.TYP_Reference   = '@BigAlias|A';
--*/
    DECLARE @UserKey        int;
    DECLARE @Pitches        int = 3;
    DECLARE @ActiveDeals    int = 4;
    DECLARE @DealsInPursuit int = 7;
    DECLARE @BidActivity    int = 6;
    DECLARE @Properties     int = 1;
    DECLARE @Loans          int = 0;
    DECLARE @LoansMaturing  int = 2;
    DECLARE @HoldPeriod     int = 5;
     
    SELECT @UserKey     = GAcTo.User_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcTo;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

    SELECT @Pitches        AS Pitches        
          ,@ActiveDeals    AS ActiveDeals   
          ,@DealsInPursuit AS DealsInPursuit
          ,@BidActivity    AS BidActivity   
          ,@Properties     AS Properties    
          ,@Loans          AS Loans         
          ,@LoansMaturing  AS LoansMaturing 
          ,@HoldPeriod     AS HoldPeriod;

/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC xESxApp.GetCompanySummaryEntityCount @AccessToken,'@BigAlias|A',1,1,1,36;
--*/
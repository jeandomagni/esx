﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/12/04
-- Description: Returns a single row with each column containing a count of a different entity type tied to the company.
--              This list of counts is for the GetCompanyListBriefWCount screen.  
--              See GetCompanySummaryEntityCount for the counts on the company summary page.
-- 
-- Updated 3/28/2016 to support new data model. Josh D Pierro 
-- 
-- =============================================
CREATE PROCEDURE [xESxApp].[GetCompanyEntityCount]
     @pAccessToken      iESx.TYP_AccessToken    -- This token represents the user on whose behalf the request is being made.
    ,@pMentionID        iESx.TYP_Reference      -- The company whose data should be returned.
AS
    SET NOCOUNT ON;

/*
    DECLARE @pAccessToken   iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
    DECLARE @pMentionID     iESx.TYP_Reference   = '@BigAlias|A';
--*/
    DECLARE @UserKey        int;
    DECLARE @Deals          int = 0;
    DECLARE @Investments    int = 2;
    DECLARE @BidVolume      int = 3;
    DECLARE @Locations      int = 0;
    DECLARE @Contacts       int = 0;
	DECLARE @Properties     int = 0; 
	DECLARE @Loans          int = 0;
     
    SELECT @UserKey     = GAcTo.User_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcTo;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

	--company 
	-- Joins Company to Alias on its canonical alias so a useable Name is available then to Mention so MentionID is available
	-- ported from  [iESx].[Company]  view

CREATE TABLE #Company
	(MentionID nvarchar(128), 
	 Mention_Key int, 
	 AliasName  nvarchar(128),
	 Company_Key int,
	 Division nvarchar(100),
	 TickerSymbol nvarchar(50),
	 CibosCode nvarchar(50),
	 FcpaName nvarchar(50),
	 CompanyType nvarchar(100),
	 EsClassification nvarchar(100),
	 InstitutionType nvarchar(100),
	 OfacCheckDate date,
	 IsSolicitableDate date,
	 IsCanonical bit,
	 IsDefunct bit,
	 IsSolicitable bit,
	 IsForeignGovernment bit,
	 IsTrackedByAsiaTeam bit,
	 HasInterestedInDeals bit,
	 HasWfsCorrelation bit,
	 HasRestrictedVisibility bit,
	 RealEstateAllocation numeric(5,2)) 

   INSERT INTO #Company
	SELECT M.MentionID,
		   M.Mention_Key,
		   Al.AliasName AS CompanyName,
		   Co.Company_Key,
		   Co.Division, 
		   Co.TickerSymbol, 
		   Co.CibosCode, 
		   Co.FcpaName, 
		   Co.CompanyType, 
		   Co.EsClassification, 
		   Co.InstitutionType, 
		   Co.OfacCheckDate, 
		   Co.IsSolicitableDate, 
		   Co.IsCanonical, 
		   Co.IsDefunct, 
		   Co.IsSolicitable, 
		   Co.IsForeignGovernment, 
		   Co.IsTrackedByAsiaTeam, 
		   Co.HasInterestedInDeals, 
		   Co.HasWfsCorrelation, 
		   Co.HasRestrictedVisibility, 
		   Co.RealEstateAllocation
      FROM dESx.Company Co
      JOIN dESx.CompanyAlias Al
        ON Al.Company_Key = Co.Company_Key
       AND Al.IsPrimary = 1
      JOIN dESx.Mention M
        ON M.Alias_Key = Al.CompanyAlias_Key
       AND M.IsPrimary = 1
	  AND M.MentionID = @pMentionID

	--deals
      SELECT @Deals = COUNT(d.Deal_Key)
	FROM  #Company Co
	JOIN  dESx.DealCompany dc
	ON co.Company_Key = dc.Company_Key
	JOIN dESx.Deal d
	JOIN dESx.DealStatusLog ds ON d.Deal_Key = ds.Deal_Key AND ds.IsCurrent = 1
	ON dc.Deal_Key = d.Deal_Key
	WHERE ds.DealStatus_Code = 'Active';

	
	--contacts and locations
    SELECT @Contacts = COUNT(*),
	       @Locations = COUNT(DISTINCT O.Office_Key)
      FROM  #Company Co
      JOIN dESx.Office O
        ON O.Company_Key = Co.Company_Key;

	 --properties
	SELECT @Properties = COUNT(a.Asset_Key)
	FROM   #Company Co
	JOIN dESx.CompanyAsset Ca
	ON Co.Company_Key = ca.Company_Key
	JOIN dESx.Asset a 
	ON ca.Asset_Key = a.Asset_Key
	JOIN dESx.AssetType at
	ON a.AssetType_Code = at.AssetType_Code
	WHERE ca.IsCurrent = 1
	AND at.[Description] = 'Property'

	--loans 
    SELECT @Loans  =  COUNT(a.Asset_Key)
	FROM   #Company Co
	JOIN dESx.CompanyAsset Ca
	ON Co.Company_Key = ca.Company_Key
	JOIN dESx.Asset a 
	ON ca.Asset_Key = a.Asset_Key
	JOIN dESx.AssetType at
	ON a.AssetType_Code = at.AssetType_Code
	WHERE ca.IsCurrent = 1
	AND at.[Description] = 'Loan'


     SELECT @Deals       AS Deals
          ,@Investments AS Investments
          ,@BidVolume   AS BidVolume
          ,@Locations   AS Locations
          ,@Contacts    AS Contacts
		  ,@Properties  AS Properties
		  ,@Loans       AS Loans;;

/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC xESxApp.GetCompanyEntityCount @AccessToken,'@BigAlias|A';
--*/
﻿CREATE PROCEDURE [xESxApp].[AppDev_RebuildAllMentions]
AS

DECLARE @seedDate as dateTime;
SELECT @seedDate = cast('1/1/2012' as date);

PRINT 'Companies mentions';
    WITH x AS (
        SELECT iESx.CalculateRawMentionName('A', Al.CompanyAlias_Key, '') calcName
              ,Al.CompanyAlias_Key
          FROM dESx.CompanyAlias Al
          JOIN dESx.Company Co
            ON Co.Company_Key = Al.Company_Key 
		 LEFT JOIN dESx.Mention m ON m.Alias_Key = Al.CompanyAlias_Key
         WHERE m.Mention_Key IS NULL
    )
    INSERT INTO dESx.Mention(Alias_Key,IsPrimary, MentionID, CreatedDate, CreatedBy,TotalUseCount, UpdatedDate )
    SELECT x.CompanyAlias_Key
          ,1
          ,iESx.FormatMentionName('A',x.calcName,ISNULL(NULLIF(CAST(ROW_NUMBER() OVER(PARTITION BY x.calcName ORDER BY x.CompanyAlias_Key) AS nvarchar),N'1'),N''))
		  , @seedDate
		  , 'SEEDDATA'
		  , 0
		  ,@seedDate
      FROM x
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to Mention from Alias';
    
PRINT 'Contacts mentions';
    WITH x AS (
        SELECT iESx.CalculateRawMentionName('C',Cn.Contact_Key, '') calcName
              ,Cn.Contact_Key
          FROM dESx.Contact Cn
		  LEFT JOIN dESx.Mention m ON m.Contact_Key = Cn.Contact_Key
		  WHERE m.Mention_Key IS NULL
    )
    INSERT INTO dESx.Mention(Contact_Key,IsPrimary,MentionID, CreatedDate, CreatedBy,TotalUseCount, UpdatedDate )
    SELECT x.Contact_Key
          ,1
          ,iESx.FormatMentionName('C',x.calcName,ISNULL(NULLIF(CAST(ROW_NUMBER() OVER(PARTITION BY x.calcName ORDER BY x.Contact_Key) AS nvarchar),N'1'),N''))
		  , @seedDate
		  , 'SEEDDATA'
		  , 0
		   ,@seedDate
	  FROM x;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to Mention from Contact';
    
PRINT 'Offices mentions';
    WITH x AS (
        SELECT iESx.CalculateRawMentionName('O', O.Office_Key, '') calcName
              ,O.Office_Key
          FROM dESx.Office O
		  LEFT JOIN dESx.Mention m ON m.Office_Key = O.Office_Key
		  WHERE m.Mention_Key IS NULL
    )
    INSERT INTO dESx.Mention(MentionID,IsPrimary,Office_Key, CreatedDate, CreatedBy,TotalUseCount, UpdatedDate )
    SELECT iESx.FormatMentionName('O',x.calcName,ISNULL(NULLIF(CAST(ROW_NUMBER() OVER(PARTITION BY x.calcName ORDER BY x.Office_Key) AS nvarchar),N'1'),N''))
          ,1
          ,x.Office_Key
		  , @seedDate
		  , 'SEEDDATA'
		  , 0
		   ,@seedDate
      FROM x;  -- test ones are inserted above
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to Mention from Office';

PRINT 'Deals mentions';
    WITH x AS (
        SELECT iESx.CalculateRawMentionName('D', D.Deal_Key, '') calcName
              ,D.Deal_Key
          FROM dESx.Deal D
		  LEFT JOIN dESx.Mention m ON m.Deal_Key = D.Deal_Key
		  WHERE m.Mention_Key IS NULL
    )
    INSERT INTO dESx.Mention(Deal_Key,IsPrimary,MentionID, CreatedDate, CreatedBy,TotalUseCount, UpdatedDate )
    SELECT x.Deal_Key
          ,1
          ,iESx.FormatMentionName('D',x.calcName,ISNULL(NULLIF(CAST(ROW_NUMBER() OVER(PARTITION BY x.calcName ORDER BY x.Deal_Key) AS nvarchar),N'1'),N''))
		  , @seedDate
		  , 'SEEDDATA'
		  , 0
		   ,@seedDate
      FROM x
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to Mention from Deal as Deals';

PRINT 'Properties mentions';
    WITH x AS (
        SELECT iESx.CalculateRawMentionName('P', AN.AssetName_Key, '') calcName ,AN.AssetName_Key
          FROM dESx.AssetName AN
		Join dESx.Asset A on A.Asset_Key = AN.Asset_Key
		LEFT JOIN dESx.Mention m ON m.PropertyAssetName_Key = AN.AssetName_Key
		Where A.AssetType_Code = 'Property'
		AND m.Mention_Key IS NULL
    )
    INSERT INTO dESx.Mention(PropertyAssetName_Key,IsPrimary,MentionID, CreatedDate, CreatedBy,TotalUseCount, UpdatedDate )
    SELECT x.AssetName_Key
          ,1
          ,iESx.FormatMentionName('P',x.calcName,ISNULL(NULLIF(CAST(ROW_NUMBER() OVER(PARTITION BY x.calcName ORDER BY x.AssetName_Key) AS nvarchar),N'1'),N''))
		  , @seedDate
		  , 'SEEDDATA'
		  , 0
		   ,@seedDate
      FROM x
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to Mention from Asset as Properties';

PRINT 'Loans mentions';
	WITH x AS (
        SELECT iESx.CalculateRawMentionName('L', AN.AssetName_Key, '') calcName ,AN.AssetName_Key
          FROM dESx.AssetName AN
		Join dESx.Asset A on A.Asset_Key = AN.Asset_Key
		LEFT JOIN dESx.Mention m ON m.LoanAssetName_Key = AN.AssetName_Key
		Where A.AssetType_Code = 'Loan'
		AND m.Mention_Key IS NULL
    )
    INSERT INTO dESx.Mention(LoanAssetName_Key,IsPrimary,MentionID, CreatedDate, CreatedBy,TotalUseCount, UpdatedDate )
    SELECT x.AssetName_Key
          ,1
          ,iESx.FormatMentionName('L',x.calcName,ISNULL(NULLIF(CAST(ROW_NUMBER() OVER(PARTITION BY x.calcName ORDER BY x.AssetName_Key) AS nvarchar),N'1'),N''))
		  , @seedDate
		  , 'SEEDDATA'
		  , 0
		   ,@seedDate
      FROM x
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to Mention from Asset as Loans';


RETURN 0

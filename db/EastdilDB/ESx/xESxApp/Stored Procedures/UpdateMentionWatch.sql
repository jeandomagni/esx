﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/11/16
-- Description: Sets the watch (AKA Favorite, AKA star) flag for a mentionable item to ON or OFF for the current user.
-- =============================================
CREATE PROCEDURE xESxApp.UpdateMentionWatch
     @pAccessToken      iESx.TYP_AccessToken      -- This token represents the user on whose behalf the update is being made.
    ,@pMentionID        iESx.TYP_Reference        -- A legal MentionID identifying the object whose watch status is to be set.
    ,@pOnOff            bit                       -- 1 = on (watch), 0 = off (stop watching)
AS
    SET NOCOUNT ON;
    DECLARE @UserKey    int;

    SELECT @UserKey     = GAcCx.User_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcCx;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

    DECLARE @MentionKey int = (SELECT M.Mention_Key FROM dESx.Mention M WHERE M.MentionID = @pMentionID);
    IF @MentionKey IS NULL
    BEGIN
        DECLARE @err nvarchar(500) = '@pMentionID is not an existing Mention reference: "'+@pMentionID+'"';
        RAISERROR (@err,16,0);
        RETURN;
    END;

    IF EXISTS (SELECT * 
                 FROM dESx.UserMentionData UMDa 
                WHERE UMDa.Mention_Key  = @MentionKey
                  AND UMDa.User_Key     = @UserKey)
        UPDATE dESx.UserMentionData
           SET IsWatched = @pOnOff
         WHERE Mention_Key  = @MentionKey
           AND User_Key     = @UserKey;
    ELSE
        INSERT dESx.UserMentionData(User_Key,Mention_Key,IsWatched,UseCount,LastUseDate)
        VALUES (@UserKey,@MentionKey,@pOnOff,0,GETUTCDATE());
/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
DECLARE @LoginName sysname = (SELECT U.LoginName FROM dESx.[User] U WHERE U.User_Key = 1)
DELETE dESx.UserMentionData WHERE User_Key = 1;     -- insure it doesn't exist
EXEC xESxApp.Login @LoginName,@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC xESxApp.UpdateMentionWatch @AccessToken,'@BigCompany|A',1;
SELECT * FROM dESx.UserMentionData WHERE User_Key = 1;     -- insure it gets created
EXEC xESxApp.UpdateMentionWatch @AccessToken,'@BigCompany|A',0;
SELECT * FROM dESx.UserMentionData WHERE User_Key = 1;     -- insure it changes
--*/
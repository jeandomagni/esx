﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/11/03
-- Description: Returns comments referring to a specific object identified by its MentionID
--              Optionally filtering for comments containing specific text
-- 
--  Modified 3/28/2016, Josh D Pierro - refactored and simplified for new data model 
-- =============================================
CREATE PROCEDURE [xESxApp].[GetMentionReferenceComments]
     @pAccessToken      iESx.TYP_AccessToken    -- This token represents the user on whose behalf the call is being made.
    ,@pMentionID        iESx.TYP_Reference      -- A legal MentionID identifying the object upon which the comment was created.
    ,@pFilterText       nvarchar(1000)       -- Filter comments to only those containing the specified text
    ,@pSortColumn       sysname = 'CreateTime'  -- The column to use as primary sort order.  Alternatives might be 'UpdateTime', 'CreateUser', 'UpdateUser'
    ,@pSortDescending   bit = 1                 -- The default (1) sorts descending (most recent first).  0 causes sort to be Ascending (earliest comment first)
    ,@pMaxRows          int = 100
    ,@pFirstRow         int = 0

AS
    SET NOCOUNT ON;


    DECLARE @UserMentionKey  int;
    SELECT @UserMentionKey   = GAcCx.Mention_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcCx;

    IF @UserMentionKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

-- port of [iESx].[GetMentionIDAliasKeys] 
-- Gets all the mention keys (and their primary) that belong to a specific object (there could be aliases).
 CREATE TABLE #MentionKeys
		(MentionID nvarchar(128),
		MentionKey int,
		Primary__Mention_Key int, 
		Primary__MentionID nvarchar(128),
		TableCode varchar(1),
		TableKey int)

INSERT INTO  #MentionKeys
SELECT     M_1.MentionID
          ,M_1.Mention_Key
          ,M_2.Mention_Key              AS Primary__Mention_Key
          ,M_2.MentionID                AS Primary__MentionID
          ,M.[ReferencedTable__Code]    AS TableCode
          ,M.[ReferencedTable__Key]     AS TableKey     --select *
      FROM dESx.Mention M
      JOIN dESx.Mention M_1
        ON M_1.ReferencedTable__Code = M.ReferencedTable__Code
       AND M_1.ReferencedTable__Key  = M.ReferencedTable__Key
      JOIN dESx.Mention M_2
        ON M_2.ReferencedTable__Code = M.ReferencedTable__Code
       AND M_2.ReferencedTable__Key  = M.ReferencedTable__Key
       AND M_2.IsPrimary = 1
     WHERE M.MentionID = @pMentionID;


-- get comments  
		SELECT 
			c.Comment_Key, 
			CreateTime, 
			c.UpdateTime, 
			c.CommentText,
			m.Primary__MentionID AS PrimaryMentionID,
			m.TableCode,
			u.LoginName AS CreatorName, 
			mn.MentionID AS CreatorMentionID
		FROM dESx.Comment c
		JOIN #MentionKeys m 
		ON  c.Implicit__Mention_Key = m.Primary__Mention_Key
		JOIN [dESx].[User] u
		ON c.Create__User_Key = u.User_Key
		JOIN dESx.Contact ct
		ON u.Contact_Key = ct.Contact_Key
		JOIN dESx.Mention mn
		ON u.Contact_Key = mn.Contact_Key
		WHERE (@pFilterText IS NULL OR c.CommentText = @pFilterText) 
		ORDER BY c.CreateTime DESC


DROP TABLE #MentionKeys

/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@SomeEmployee|E','this','CreateTime',0,10,0;
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@SomeEmployee|E',''    ,'CreateTime',0,10,0;
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@SomeEmployee|E',''    ,'CreateTime',1,10,0;
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@SomeEmployee|E',''    ,'CreateTime',0,2,0;
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@SomeEmployee|E',''    ,'CreateTime',1,2,0;
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@SomeEmployee|E',''    ,'CreateTime',0,2,2;
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@SomeEmployee|E',''    ,'CreateTime',1,2,2;

EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@SomeEmployeeAlias|E';
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@BigCompany|A';
EXEC xESxApp.Login 'User2',@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@SomeEmployee|E';
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@SomeEmployeeAlias|E';
EXEC xESxApp.GetMentionReferenceComments @AccessToken, '@BigCompany|A';
--*/
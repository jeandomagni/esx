﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/12/23
-- Description: Drops all tables in the dESx schema
-- =============================================
CREATE PROCEDURE xESxApp.AppDev_DropAllTables
    @pTest bit = 0
AS
/*
    DECLARE @pTest bit = 0;
--*/
    SET NOCOUNT ON;
    -- Build structure to determine delete table content order to avoid constraint issues
    SELECT DISTINCT 
           RefFrom.object_id                    AS FromID
          ,RefFrom.name                         AS FromTable
          ,RefTo.object_id                      AS ToID
          ,RefTo.name                           AS ToName
          ,CASE WHEN RefTo.object_id IS NULL 
                THEN 10 
                ELSE NULL 
           END                                  AS GroupNumber
      INTO #t
      FROM sys.tables RefFrom
      LEFT JOIN sys.foreign_keys FK
           JOIN sys.objects RefTo
             ON RefTo.object_id = FK.referenced_object_id
        ON FK.parent_object_id = RefFrom.object_id
       AND RefFrom.object_id <> RefTo.object_id
     WHERE RefFrom.schema_id = SCHEMA_ID('dESx');
    
    -- Calculate delete order
    SET ANSI_WARNINGS OFF;
    WHILE EXISTS(SELECT NULL FROM #t WHERE GroupNumber IS NULL)
    UPDATE t SET GroupNumber =  
                CASE WHEN EXISTS(SELECT X.FromID ,*
                                   FROM #t AS X
                                   JOIN #t AS Y
                                     ON Y.FromID = X.ToID 
                                  WHERE Y.GroupNumber IS NULL
                                    AND x.FromID =  t.FromID) 
                     THEN NULL
                     ELSE (SELECT MIN(GroupNumber)-1 FROM #t) 
                END
      FROM #t t
    WHERE t.GroupNumber IS NULL;
    SET ANSI_WARNINGS ON;
      
    -- Build the delete commands in the correct order
    DECLARE @Cmds nvarchar(MAX) = '--'+ CHAR(13)+CHAR(10);
    WITH x AS (
        SELECT DISTINCT 
               MIN(GroupNumber) GroupNumber
              ,FromTable 
          FROM #t 
         GROUP BY FromTable 
    )
    SELECT @Cmds = @Cmds+'DROP TABLE dESx.'+QUOTENAME(x.FromTable)+';'+CHAR(13)+CHAR(10)
      FROM x
     ORDER BY x.GroupNumber,x.FromTable;
    DROP TABLE #t;
    
    -- Execute or display as requested
    IF @pTest = 1
        SELECT CAST('<?q '+(@cmds)+' ?>' AS XML);
    ELSE
        EXEC (@cmds);
    
/*Test
GO
exec xESxApp.AppDev_DropAllTables 1;    -- This is for testing (displays reseed commands only)
exec xESxApp.AppDev_DropAllTables;      -- Normally you want this
--*/
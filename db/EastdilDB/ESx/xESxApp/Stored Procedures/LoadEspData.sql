﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/03
-- Description: Populates ESX tables with ESP data.  
--              ****** In this version there must be no conflicting data in the ESX tables *******
--                      That is: MEREGE is not supported in most cases.
-- =============================================
CREATE PROCEDURE [xESxApp].[LoadEspData]
AS

    SET NOCOUNT ON;
    EXEC Global.DeleteAllSchemaData @pSchemaName='pESxETL';

    /*  -- Use this to reset all data populated by this routine so it can be tested again
        -- Please keep this up-to-date!!!
    SET NOCOUNT OFF;
    DELETE dESx.Mention where Deal_Key >= 0;            -- \  These are needed because [xESxApp].[AppDev_RebuildAllMentions] is
    DELETE dESx.Mention where Office_Key >= 0;          --  \ typically called next and testing after mentions are generated requires
    DELETE dESx.Mention where Contact_Key >= 0;         --  / dependent rows to be deleted.  NOTE: RebuildAllMentions doesn't support 
    DELETE dESx.Mention where Alias_Key >= 0;           -- /  some mentions remaining but 'Seed' mentions have other dependencies.
    DELETE dESx.DealAsset WHERE DealAsset_Key >= 0;
    DBCC CHECKIDENT ('dESx.DealAsset', RESEED, 0);
    DELETE dESx.DealCompany WHERE DealCompany_Key >= 0;
    DBCC CHECKIDENT ('dESx.DealCompany', RESEED, 0);
    DELETE dESx.Deal WHERE Deal_Key >= 0;
    DBCC CHECKIDENT ('dESx.Deal', RESEED, 0);
    DELETE dESx.Asset WHERE Asset_Key >= 0;
    DBCC CHECKIDENT ('dESx.Asset', RESEED, 0);
    DELETE dESx.ContactOffice where ContactOffice_Key >= 0;
    DBCC CHECKIDENT ('dESx.ContactOffice', RESEED, 0);
    DELETE dESx.Office where Office_Key >= 0;
    DBCC CHECKIDENT ('dESx.Office', RESEED, 0);
    DELETE dESx.Company_ElectronicAddress where Company_ElectronicAddress_Key >= 0;
    DBCC CHECKIDENT ('dESx.Company_ElectronicAddress', RESEED, 0);
    DELETE dESx.Contact_ElectronicAddress where Contact_ElectronicAddress_Key >= 0;
    DBCC CHECKIDENT ('dESx.Contact_ElectronicAddress', RESEED, 0);
    DELETE dESx.Contact where Contact_Key >= 0;
    DBCC CHECKIDENT ('dESx.Contact', RESEED, 0);
    DELETE dESx.EastdilOffice WHERE EastdilOffice_Key >= 0;
    DBCC CHECKIDENT ('dESx.EastdilOffice', RESEED, 0);
    DELETE dESx.PhysicalAddress where PhysicalAddress_Key >= 0;
    DBCC CHECKIDENT ('dESx.PhysicalAddress', RESEED, 0);
    DELETE dESx.ElectronicAddress where ElectronicAddress_Key >= 0;
    DBCC CHECKIDENT ('dESx.ElectronicAddress', RESEED, 0);
    DELETE dESx.CompanyAlias where Company_Key >= 0;
    DBCC CHECKIDENT ('dESx.CompanyAlias', RESEED, 0);
    DELETE dESx.CompanyHierarchy where CompanyHierarchy_Key >= 0;
    DBCC CHECKIDENT ('dESx.CompanyHierarchy', RESEED, 0);
    DELETE dESx.Company where Company_Key >= 0;
    DBCC CHECKIDENT ('dESx.Company', RESEED, 0);
    --*/ 

    -- The order of operations is important.  Tables have dependencies.    
    -- Later procedures may depend on pESxETL tables being populated by earlier procedures.
    EXEC pESxETL.LoadCompany;
    EXEC pESxETL.LoadContact;
    EXEC pESxETL.LoadOffice;
    EXEC pESxETL.LoadProperty;
    EXEC pESxETL.LoadDeal;
    EXEC pESxETL.LoadEmployee;
/*Test
GO
exec xESxApp.LoadEspData;       -- takes 47 seconds locally
--*/
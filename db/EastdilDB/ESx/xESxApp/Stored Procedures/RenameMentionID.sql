﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/15
-- Description: Changes a specific MentionID value to a new value - updating references to the old value in the process.
--              If @pNewMentionID is null the object's MentionID is calculated using the same algorithm as CreateNewMention.
-- =============================================
CREATE PROCEDURE xESxApp.RenameMentionID
     @pAccessToken          iESx.TYP_AccessToken  -- This token represents the user on whose behalf the Put is being made.
    ,@pOriginalMentionID    iESx.TYP_Reference    -- An existing MentionID.
    ,@pNewMentionID         iESx.TYP_Reference    -- A new unique MentionID.
AS 
    SET NOCOUNT ON;
/*
DECLARE @pAccessToken  iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
DECLARE @pMentionID    iESx.TYP_Reference   = '@BigCompany|A';
DECLARE @pMentionID    iESx.TYP_Reference   = '@BigNewNameCompany|A';
--*/
    DECLARE @UserKey            int;
    DECLARE @ErrorText          nvarchar(100) = '';
    DECLARE @RawMentionID       iESx.TYP_Reference;
    DECLARE @Uniquifier         char(10) = '';
    DECLARE @NewMentionType     char(1);
    DECLARE @ExistingType       char(1);
    DECLARE @MentionKey         int;
    DECLARE @ObjectKey          int;
	DECLARE @DefaultMentionValue nvarchar(40) = (SELECT NEWID()) 

    SELECT @UserKey     = GAcTo.User_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcTo;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

    SELECT @MentionKey      = M.Mention_Key
          ,@ExistingType    = M.ReferencedTable__Code
          ,@ObjectKey       = M.ReferencedTable__Key
      FROM dESx.Mention M
     WHERE M.MentionID = @pOriginalMentionID;

    IF @MentionKey IS NULL
    BEGIN
        SET @ErrorText = '@pOriginalMentionID must contain an existing ID. "'+@pOriginalMentionID+'" does not exist.';
        RAISERROR (@ErrorText,16,0);
        RETURN;
    END;

    IF @pNewMentionID IS NULL   -- they want us to change it only if it needs changing
    BEGIN
        SET @RawMentionID = iESx.CalculateRawMentionName(@ExistingType, @ObjectKey, @DefaultMentionValue);
        SET @pNewMentionID = iESx.FormatMentionName(@ExistingType,@RawMentionID,@Uniquifier);
        IF @pOriginalMentionID <> @pNewMentionID
        BEGIN
            SET @Uniquifier = iESx.GetMentionNameUniquifier(@ExistingType,@RawMentionID);
            SET @pNewMentionID = iESx.FormatMentionName(@ExistingType,@RawMentionID,@Uniquifier);
        END
    END
    BEGIN
        IF EXISTS (SELECT M.Mention_Key FROM dESx.Mention M WHERE M.MentionID = @pNewMentionID)
        BEGIN
            SET @ErrorText = '@pNewMentionID must not already exist: "'+@pOriginalMentionID+'"';
            RAISERROR (@ErrorText,16,0);
            RETURN;
        END;
    
        SELECT @RawMentionID    = RawName
              ,@NewMentionType  = ObjectType
              ,@ErrorText       = ErrorText 
          FROM iESx.ParseMentionName(@pNewMentionID);
        IF @ErrorText <> ''
        BEGIN
            SET @ErrorText = @ErrorText + ' in @pNewMentionID'
            RAISERROR (@ErrorText,16,0);
            RETURN;
        END;
        SELECT @ExistingType    = ObjectType
              ,@ErrorText       = ErrorText 
          FROM iESx.ParseMentionName(@pOriginalMentionID);
        IF @NewMentionType <> @ExistingType
        BEGIN
            SET @ErrorText = '@pNewMentionID "'+@NewMentionType+'" must have the same object type as @pOriginalMentionID "'+@ExistingType+'"';
            RAISERROR (@ErrorText,16,0);
            RETURN;
        END;
    END

    IF @pNewMentionID <> @pOriginalMentionID
    BEGIN
    BEGIN TRANSACTION
        UPDATE M
           SET MentionID = @pNewMentionID
          FROM dESx.Mention M
         WHERE M.Mention_Key = @MentionKey;

        UPDATE Cm
           SET Cm.CommentText = REPLACE(Cm.CommentText,@pOriginalMentionID,@pNewMentionID)
          FROM dESx.Comment Cm
          JOIN dESx.Comment_Mention CmM
            ON CmM.Comment_Key = Cm.Comment_Key
         WHERE CmM.Mention_Key = @MentionKey;

    COMMIT TRANSACTION;
    END;
/*Test
GO
DECLARE @pAccessToken     iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.CreateNewMention @pAccessToken, 'A', 1, 0, @MentionID OUTPUT;
SELECT @MentionID;
SELECT * FROM dESx.Mention WHERE ReferencedTable__Key = 1;
--*/
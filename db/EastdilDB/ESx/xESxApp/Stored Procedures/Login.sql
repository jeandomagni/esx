﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/11/16
-- Description: Verifies and caches a users permissions for quick access.
--              Returns a token to be included in all other operations and the user's MentionID.
--
--      Ideally this function should reside in a different schema that only an ID other than the one
--      used for xESxApp access can reach.  That way it would require the compromise of the other ID
--      before any data would be accessable via xESxApp but since we've moved to EF access via views
--      we can no longer control schema access with tokens so making tokens harder to get is pointless.
-- =============================================
CREATE PROCEDURE xESxApp.Login
     @UserName      sysname
    ,@pAccessToken  iESx.TYP_AccessToken    OUTPUT
    ,@pMentionID    iESx.TYP_Reference      OUTPUT
	,@pUserKey		int						OUTPUT
AS
    SET NOCOUNT ON;

/*
DECLARE @UserName      sysname  = 'User1';
DECLARE @pAccessToken  iESx.TYP_AccessToken;
DECLARE @pMentionID    iESx.TYP_Reference;
--*/
    DECLARE @ContactKey int;
    DECLARE @UserKey    int;
    DECLARE @HasAccess  bit;
    DECLARE @Result     int = 1;    -- If we don't throw the default is 1 (error). Success returns 0;

    -- Insure only null can return unless a valid token is created
    SET @pAccessToken   = NULL;
    SET @pMentionID     = NULL;

    -- Collect basic user information
    --/*
    SELECT @ContactKey  = U.Contact_Key
          ,@UserKey     = U.User_Key
          ,@HasAccess   = U.HasAccess   --*/SELECT *
      FROM dESx.[User] U
     WHERE U.LoginName = @UserName;

    -- Delete any expired tokens before (potentially) creating a new one
    -- You could prevent multiple logins by the same user by simply removing the time constraint
    -- As soon as the token is removed, any interface using the token will stop working.
    DELETE FROM dESx.CacheAccessToken
     WHERE User_Key = @UserKey
       AND CreateTime < DATEADD(DAY,-1,GETUTCDATE()); -- token expires after 24 hrs

    -- If the UserName is active then grant a token otherwise return null
    IF @HasAccess = 1
    BEGIN
        -- The lookup of any existing token before creating a new one has the effect that
        -- any data cached by one session of this user will affect the cache for any other
        -- session by that same user.  This was added because the application was getting
        -- a new access token for every I/O rather than every session.  
        -- To make it work as originally intended see (*) comments
        --/*
        SELECT @pMentionID      = M.MentionID
              ,@pAccessToken    = CAT.AccessToken       -- (*) change 'CAT.AccessToken' to 'NEWID()'  */ select *
          FROM dESx.Mention M
          LEFT JOIN dESx.CacheAccessToken CAT
            ON CAT.User_Key = @UserKey
         WHERE M.Contact_Key = @ContactKey
           AND M.IsPrimary = 1;

        IF @pMentionID IS NOT NULL
        BEGIN
			SET @pUserKey = @UserKey; 
            IF @pAccessToken IS NULL                    -- (*) Remove this line
            BEGIN                                       -- (*) Remove this line
                SET @pAccessToken = NEWID();            -- (*) Remove this line

                INSERT INTO dESx.CacheAccessToken
                        (AccessToken
                        ,CreateTime
                        ,User__MentionID
                        ,User_Key
                        )
                VALUES  (@pAccessToken
                        ,SYSDATETIME()
                        ,@pMentionID
                        ,@UserKey
                        );
            END;                                        -- (*) Remove this line

            SET @Result = 0;
        END
    END;

    RETURN @Result;

/*Test
GO
DECLARE @AccessToken  iESx.TYP_AccessToken;
DECLARE @MentionID    iESx.TYP_Reference;
exec xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
SELECT @AccessToken,@MentionID;
exec xESxApp.Login 'User2',@AccessToken OUTPUT,@MentionID OUTPUT;
SELECT @AccessToken,@MentionID;
exec xESxApp.Login 'User3',@AccessToken OUTPUT,@MentionID OUTPUT;
SELECT @AccessToken,@MentionID;
exec xESxApp.Login 'User4',@AccessToken OUTPUT,@MentionID OUTPUT;
SELECT @AccessToken,@MentionID;
--*/
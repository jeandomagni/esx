﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/10/30
-- Description: Returns comments associated with a specific hash tag.
-- =============================================
CREATE PROCEDURE xESxApp.GetTagComments
     @pAccessToken      iESx.TYP_AccessToken      -- This token represents the user on whose behalf the call is being made.
    ,@pTagID            iESx.TYP_Reference        -- A legal MentionID identifying the object upon which the comment is created.
    ,@pMaxRows          int = 100
    ,@pFirstRow         int = 0

AS
    SET NOCOUNT ON;

/*
DECLARE @pAccessToken   iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
DECLARE @pTagID         iESx.TYP_Reference   ='#aTag01'
DECLARE @pMaxRows       int                  = 10;
--*/
    DECLARE @UserKey        int;
    SELECT @UserKey     = GAcTo.User_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcTo;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

    SELECT TOP (@pMaxRows)
           Cm.Comment_Key           [CommentID]
          ,Cm.CreateTime            [CreateTime]
          ,Cm.UpdateTime            [UpdateTime]
          ,Cm.CommentText           [CommentText]
          ,M.MentionID              [PrimaryMentionID]
          ,M.ReferencedTable__Code  [TableCode]
      FROM dESx.Comment Cm
      JOIN dESx.Comment_Tag CmTg
        ON CmTg.Comment_Key = Cm.Comment_Key
      JOIN dESx.Tag Tg
        ON Tg.Tag_Key = CmTg.Tag_Key
      JOIN dESx.Mention M
        ON M.Mention_Key = Cm.Implicit__Mention_Key
     WHERE Tg.Tag_Key = @pTagID;
/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC xESxApp.GetTagComments @AccessToken, '#Tag01';
--*/
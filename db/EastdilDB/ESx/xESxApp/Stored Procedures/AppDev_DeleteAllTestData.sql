﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/12/23
-- Description: Removes all data from all dEsx tables so structure changes can be made without conflicts.
-- =============================================
CREATE PROCEDURE xESxApp.AppDev_DeleteAllTestData
    @pTest bit = 0
AS
/*
    DECLARE @pTest bit = 0;
--*/
	EXEC [Global].DeleteAllSchemaData @pSchemaName='dESx', @pTest=@pTest;
/*Test
GO
exec xESxApp.AppDev_DeleteAllTestData 1;    -- This is for testing (displays reseed commands only)
exec xESxApp.AppDev_DeleteAllTestData;      -- Normally you want this
--*/
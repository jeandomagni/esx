﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/11/16
-- Description: Performs validations that are required to be met by integrety standards but can't be performed easily by the database itself.
-- =============================================
CREATE PROCEDURE xESxApp.ValidateDatabase
AS
    SET NOCOUNT ON;

    CREATE TABLE #Log(TableName sysname,MessageText nvarchar(1000));

    INSERT #Log(TableName,MessageText)
    SELECT 'Company'
          ,'Key='+CAST(Co.Company_Key AS varchar)+' Has no Legal name.'
      FROM dESx.Company Co
      LEFT JOIN dESx.CompanyAlias Al
        ON Al.Company_Key = Co.Company_Key
       AND Al.IsLegal = 1
     WHERE Co.IsDefunct = 0
       AND Al.Company_Key IS NULL;

    INSERT #Log(TableName,MessageText)
    SELECT 'Company'
          ,'Key='+CAST(Co.Company_Key AS varchar)+' Has no Common name.'  -- select *
      FROM dESx.Company Co
      LEFT JOIN dESx.CompanyAlias Al
        ON Al.Company_Key = Co.Company_Key
       AND Al.IsPrimary = 1
     WHERE Co.IsDefunct = 0
       AND Al.Company_Key IS NULL;

    INSERT #Log(TableName,MessageText)
    SELECT 'Company'
          ,'Key='+CAST(Co.Company_Key AS varchar)+' Has no headquarters. '
      FROM dESx.Company Co
      LEFT JOIN dESx.Office O
        ON O.Company_Key = Co.Company_Key
       AND O.IsHeadquarters = 1
     WHERE Co.IsDefunct = 0
       AND O.Company_Key IS NULL;

    INSERT #Log(TableName,MessageText)
    SELECT 'Alias'
          ,'Key='+CAST(Al.CompanyAlias_Key AS varchar)+' Has no MentionID. '
      FROM dESx.CompanyAlias Al
      LEFT JOIN dESx.Mention M
        ON M.Alias_Key = Al.CompanyAlias_Key
     WHERE M.MentionID IS NULL;

    INSERT #Log(TableName,MessageText)
    SELECT 'Deal'
          ,'Key='+CAST(D.Deal_Key AS varchar)+' Has no MentionID. '
      FROM dESx.Deal D
      LEFT JOIN dESx.Mention M
        ON M.Deal_Key = D.Deal_Key
     WHERE M.MentionID IS NULL;

    INSERT #Log(TableName,MessageText)
    SELECT 'PropertyAsset'
          ,'Key='+CAST(P.Asset_Key AS varchar)+' Has no MentionID. '
      FROM dESx.Asset P
	  LEFT JOIN dESx.AssetName AN ON P.Asset_Key = AN.Asset_Key AND AN.IsCurrent = 1
      LEFT JOIN dESx.Mention M
        ON M.PropertyAssetName_Key = AN.AssetName_Key
     WHERE M.MentionID IS NULL;

    INSERT #Log(TableName,MessageText)
    SELECT 'LoanAsset'
          ,'Key='+CAST(P.Asset_Key AS varchar)+' Has no MentionID. '
      FROM dESx.Asset P
	  LEFT JOIN dESx.AssetName AN ON P.Asset_Key = AN.Asset_Key AND AN.IsCurrent = 1
      LEFT JOIN dESx.Mention M
        ON M.LoanAssetName_Key = AN.AssetName_Key
     WHERE M.MentionID IS NULL;

    INSERT #Log(TableName,MessageText)
    SELECT 'Contact'
          ,'Key='+CAST(Cn.Contact_Key AS varchar)+' Has no MentionID. '
      FROM dESx.Contact Cn
      LEFT JOIN dESx.Mention M
        ON M.Contact_Key = Cn.Contact_Key
     WHERE M.MentionID IS NULL;

    SELECT * FROM #Log;

/*Test
GO
--insert dESx.Company(Division,TickerSymbol,CibosCode,IsCanonical,IsDefunct)
--VALUES  (N'',N'',N'',0,0);
--insert dESx.Employee(EmployeeName,EmpInitials,EmpType,Department,EastdilOffice_Key,Active,Title)
--VALUES  (N'Some Emp Name',N'',N'',N'',1,1,N'');
exec xESxApp.ValidateDatabase;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/01/14
-- Description: Generates a new Mention
-- =============================================
CREATE PROCEDURE xESxApp.CreateNewMention
     @pAccessToken  iESx.TYP_AccessToken      -- This token represents the user on whose behalf the Put is being made.
    ,@pObjectType   char(1)                   -- One of A,C,D,E,L,M,O,P indicating the type of object @pObjectKey belongs to.
    ,@pObjectKey    int                       -- The primary key to the object requiring a new Mention ID created.
    ,@pIsPrimary    bit                       -- Indicates if the new ID should be flagged as primary for the @pObjectKey object.
    ,@pMentionID    iESx.TYP_Reference OUTPUT -- Returns the MentionID created.  When a non-null value is supplied it is used directly and will cause a throw if illegal or exists.
    ,@pMentionKey   int OUTPUT                -- Returns the Mention_Key of the row created and null if no row is created.
AS 
    SET NOCOUNT ON;
    SET @pMentionKey = NULL;
/*
DECLARE @pAccessToken  iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
DECLARE @pObjectType   char(1)              = 'A'
DECLARE @pObjectKey    int                  = 1
DECLARE @pIsPrimary    bit                  = 1;
DECLARE @pMentionID    iESx.TYP_Reference;
DECLARE @pMentionKey   int;
--*/
    DECLARE @UserKey            int;
	DECLARE @LoginName			nvarchar(50);
    DECLARE @ErrorText          nvarchar(100) = '';
    DECLARE @RawMentionID       iESx.TYP_Reference;
    DECLARE @NewMentionType     char(1);
    DECLARE @Uniquifier         varchar(3) = '';
    DECLARE @ExistingMentionKey int = NULL;
    DECLARE @Alias_Key          int = NULL;
    DECLARE @Contact_Key        int = NULL;
    DECLARE @Deal_Key           int = NULL;
    DECLARE @Employee_Key       int = NULL;
    DECLARE @Market_Key         int = NULL;
    DECLARE @Office_Key         int = NULL;
    DECLARE @PropertyAssetName_Key int = NULL;
	DECLARE @LoanAssetName_Key  int = NULL;
	DECLARE @DefaultMentionValue nvarchar(40) = (SELECT NEWID()) 

    SELECT @UserKey     = GAcTo.User_Key,
		   @LoginName   = GAcTo.LoginName
      FROM iESx.GetAccessContext(@pAccessToken) GAcTo;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

    IF CHARINDEX(@pObjectType,'ACDELMOP') = 0
    BEGIN
        SET @ErrorText = '@pObjectType value "'+@pObjectType+'" must be one of A,C,D,E,L,M,O,P,T';
        RAISERROR (@ErrorText,16,0);
        RETURN;
    END;

    -- If an ID is not supplied generate one and insure it's unique.  Otherwise validate the supplied one
    IF @pMentionID IS NULL
    BEGIN
        SET @RawMentionID = iESx.CalculateRawMentionName(@pObjectType, @pObjectKey,@DefaultMentionValue);
        IF @RawMentionID IS NULL
        BEGIN
            SET @ErrorText = 'Key '+CAST(@pObjectKey AS varchar)+' of type "'+@pObjectType+'" must exist but was not found';
            RAISERROR (@ErrorText,16,0);
            RETURN;
        END;
        
        SET @Uniquifier = iESx.GetMentionNameUniquifier(@pObjectType,@RawMentionID);
    END
    ELSE
    BEGIN
        SELECT @RawMentionID    = RawName
              ,@NewMentionType  = ObjectType
              ,@ErrorText       = ErrorText 
              ,@Uniquifier      = ''        -- If there is one we can't tell and don't care anyway since it's pre-determined by the caller
          FROM iESx.ParseMentionName(@pMentionID);
        IF @ErrorText <> ''
        BEGIN
            SET @ErrorText = @ErrorText + ' in @pMentionID'
            RAISERROR (@ErrorText,16,0);
            RETURN;
        END;
        IF @NewMentionType <> @pObjectType
        BEGIN
            SET @ErrorText = '@pMentionID "'+@pMentionID+'" must have the same object type as @pObjectType "'+@pObjectType+'"';
            RAISERROR (@ErrorText,16,0);
            RETURN;
        END;
    END;
    
    SET @pMentionID = iESx.FormatMentionName(@pObjectType,@RawMentionID,@Uniquifier);

    IF @pIsPrimary = 1
         SELECT @ExistingMentionKey = M.Mention_Key 
           FROM dESx.Mention M 
          WHERE M.IsPrimary = 1
            AND CASE WHEN @pObjectType = 'A'
                     THEN M.Alias_Key
                     WHEN @pObjectType = 'C'
                     THEN M.Contact_Key
                     WHEN @pObjectType = 'D'
                     THEN M.Deal_Key
                     WHEN @pObjectType = 'M'
                     THEN M.Market_Key
                     WHEN @pObjectType = 'O'
                     THEN M.Office_Key
                     WHEN @pObjectType = 'P'
                     THEN M.PropertyAssetName_Key
					 WHEN @pObjectType = 'L'
                     THEN M.LoanAssetName_Key
					
                END = @pObjectKey;

    IF @pObjectType = 'A'
        SET @Alias_Key      = @pObjectKey;
    ELSE 
    IF @pObjectType = 'C'
        SET @Contact_Key    = @pObjectKey;
    ELSE 
    IF @pObjectType = 'D'
        SET @Deal_Key       = @pObjectKey;
    ELSE 
	IF @pObjectType = 'M'
        SET @Market_Key     = @pObjectKey;
    ELSE 
    IF @pObjectType = 'O'
        SET @Office_Key     = @pObjectKey;
	ELSE
	IF @pObjectType = 'P'
        SET @PropertyAssetName_Key  = @pObjectKey;
	ELSE
	IF @pObjectType = 'L'
        SET @LoanAssetName_Key  = @pObjectKey;

    BEGIN TRANSACTION;
        -- If this mention is primary and some other primary already exists, make the existing one not-primary so the insert can succeed.
        IF @ExistingMentionKey IS NOT NULL 
            UPDATE dESx.Mention
               SET IsPrimary = 0
             WHERE Mention_Key = @ExistingMentionKey;
    
        -- Actually create the mention
        DECLARE @t table (Mention_Key int);
        INSERT dESx.Mention(MentionID,IsPrimary,Alias_Key,Deal_Key,Contact_Key,Market_Key,Office_Key,PropertyAssetName_Key,LoanAssetName_Key,LastUseDate,TotalUseCount,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy)
        OUTPUT Inserted.Mention_Key
          INTO @t
        VALUES  (@pMentionID,@pIsPrimary,@Alias_Key,@Deal_Key,@Contact_Key,@Market_Key,@Office_Key,@PropertyAssetName_Key,@LoanAssetName_Key,GETUTCDATE(),0,GETUTCDATE(),@LoginName,NULL,NULL);
    COMMIT TRANSACTION;

    SET @pMentionKey = (SELECT Mention_Key FROM @t);

/*Test
GO
DECLARE @pAccessToken     iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
DECLARE @MentionID iESx.TYP_Reference;
DECLARE @pMentionKey   int;

EXEC xESxApp.CreateNewMention @pAccessToken, 'A', 1, 0, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT @MentionID, @pMentionKey; SET @MentionID = NULL;
EXEC xESxApp.CreateNewMention @pAccessToken, 'A', 1, 1, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT @MentionID, @pMentionKey; SET @MentionID = NULL;
EXEC xESxApp.CreateNewMention @pAccessToken, 'C', 1, 0, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT @MentionID, @pMentionKey; SET @MentionID = NULL;
EXEC xESxApp.CreateNewMention @pAccessToken, 'E', 1, 0, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT @MentionID, @pMentionKey; SET @MentionID = NULL;
EXEC xESxApp.CreateNewMention @pAccessToken, 'L', 1, 0, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT @MentionID, @pMentionKey; SET @MentionID = NULL;
EXEC xESxApp.CreateNewMention @pAccessToken, 'P', 1, 0, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT @MentionID, @pMentionKey; SET @MentionID = NULL;
EXEC xESxApp.CreateNewMention @pAccessToken, 'M', 1, 0, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT @MentionID, @pMentionKey; SET @MentionID = NULL;
EXEC xESxApp.CreateNewMention @pAccessToken, 'O', 1, 0, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT @MentionID, @pMentionKey;
go
DECLARE @pAccessToken     iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
DECLARE @MentionID iESx.TYP_Reference = '@dISTINCTcALCULATEDnAME|A';
DECLARE @pMentionKey   int;
EXEC xESxApp.CreateNewMention @pAccessToken, 'A', 1, 0, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT @MentionID, @pMentionKey;
EXEC xESxApp.CreateNewMention @pAccessToken, 'A', 1, 0, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT * FROM dESx.Mention WHERE ReferencedTable__Key = 1;
GO
DECLARE @pAccessToken     iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
DECLARE @MentionID iESx.TYP_Reference;
DECLARE @pMentionKey   int;
SET @MentionID = 'dISTINCTcALCULATEDnAME';
EXEC xESxApp.CreateNewMention @pAccessToken, 'A', 1, 0, @MentionID OUTPUT, @pMentionKey OUTPUT;
SELECT @MentionID, @pMentionKey; SET @MentionID = NULL;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/10/15
-- Description: Writes a message to the application log.
-- =============================================
CREATE PROCEDURE xESxApp.LogApplicationMessage
     @pAccessToken iESx.TYP_AccessToken      -- This token represents the user behind the interface
    ,@pSeverity             int              -- A value 0 thru 5 representing Usage,Trace,Info,Warning,Error and Critical respectively
    ,@pInstanceID           uniqueidentifier -- A GUID, distinct to the thread or instance
    ,@pApplicationName      nvarchar(128)    -- The name of the application logging this event
    ,@pApplicationLocation  nvarchar(128)    -- The location within the application where the event is being originated from
    ,@pEventNumber          int              -- Identifies the location or event for programatic purposes
    ,@pEventText            nvarchar(max)    -- A descriptive text of the event
    ,@pAdditionalData       nvarchar(max)    -- Typically a stack trace or null but can also contain some kind of supporting text
AS
    SET NOCOUNT ON;

/*
DECLARE @pSeverity             int              = 1;
DECLARE @pInstanceID           uniqueidentifier = 'ADABE0B5-BBC0-45CA-9141-89FF0494B239';
DECLARE @pApplicationName      nvarchar(128)    = 'tEST aPP';
DECLARE @pApplicationLocation  nvarchar(128)    = 'sOMEWHERE';
DECLARE @pEventNumber          int              = 112;
DECLARE @pEventText            nvarchar(max)    = 'aNYTHING uSEFUL';
DECLARE @pAdditionalData       nvarchar(max)    = NULL;
--*/
    INSERT INTO dESx.ApplicationLog(
            LogSeverityType_Key
           ,InstanceID
           ,ApplicationName
           ,ApplicationLocation
           ,EventNumber
           ,EventText
           ,AdditionalData)
    VALUES ( @pSeverity
            ,@pInstanceID
            ,@pApplicationName
            ,@pApplicationLocation
            ,@pEventNumber
            ,@pEventText
            ,@pAdditionalData     );

/*Test
GO
TRUNCATE TABLE dESx.ApplicationLog;
EXEC xESxApp.LogApplicationMessage 0x01,1,'ADABE0B5-BBC0-45CA-9141-89FF0494B239','tEST aPP','sOMEWHERE',112,'aNYTHING uSEFUL',NULL;
EXEC xESxApp.LogApplicationMessage 0x01,5,'B5E0ABAD-C0BB-CA45-9141-89FF0494B239','tEST aPP','sOMEWHERE',113,'nOTHING uSEFUL','sTACK tRACE';
SELECT * FROM dESx.ApplicationLog
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/10/19
-- Description: Returns the top x tags that match the prefix passed as sorted by relevance.
-- =============================================
CREATE PROCEDURE xESxApp.IntellisenseTags
     @pAccessToken  iESx.TYP_AccessToken
    ,@pTagPrefix    iESx.TYP_Reference
    ,@pMaxRows      int                  = 10
AS
BEGIN
    SET NOCOUNT ON;
    /*
    DECLARE @pAccessToken iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
    DECLARE @pTagPrefix iESx.TYP_Reference    = '#A';
    DECLARE @pMaxRows int                      = 4;
    --*/
    DECLARE @UserKey    int;
    DECLARE @TagPattern iESx.TYP_Reference    = SUBSTRING(@pTagPrefix,1,127)+'%';
    DECLARE @Potentials TABLE(TagName iESx.TYP_Reference,LastUseDate date,UseCount int,GroupType int);

    SELECT @UserKey     = GAcTo.User_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcTo;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END

    INSERT INTO @Potentials
    SELECT TOP (@pMaxRows)                -- Users personal tags
           T.Tag_Key
          ,TUs.LastUseDate
          ,TUs.UseCount
          ,1 GroupType                    -- Users tags are 'Most Relevant'
      FROM dESx.Tag T
      JOIN dESx.TagUsage TUs
        ON TUs.Tag_Key = T.Tag_Key
     WHERE T.Tag_Key LIKE @TagPattern
       AND TUs.User_Key = @UserKey
     ORDER BY 2 DESC,3 DESC;

    INSERT INTO @Potentials
    SELECT TOP (@pMaxRows)                -- Recently created tags
           T.Tag_Key
          ,T.CreateDate
          ,T.TotalUseCount
          ,2 GroupType                    -- listed 2nd
      FROM dESx.Tag T
     WHERE T.Tag_Key LIKE @TagPattern
     ORDER BY 2 DESC,3 DESC;

    INSERT INTO @Potentials
    SELECT TOP (@pMaxRows)                -- Tags recently used by others
           T.Tag_Key
          ,T.LastUseDate
          ,T.TotalUseCount
          ,3 GroupType                    -- Listed last
      FROM dESx.Tag T
     WHERE T.Tag_Key LIKE @TagPattern
     ORDER BY 2 DESC,3 DESC;

--  DECLARE @pMaxRows int                 = 5;
    WITH X AS (   -- Order each GroupType by LastUsedDate and UseCount
        SELECT pT.TagName
              ,pT.LastUseDate
              ,pT.UseCount
              ,pT.GroupType
              ,ROW_NUMBER() OVER(PARTITION BY pT.GroupType ORDER BY pT.LastUseDate DESC,pT.UseCount DESC) GroupRow
          FROM @Potentials pT
    ), Y AS (     -- Mark duplicates with NameRow > 1 where kept row is the one in the highest order group type
        SELECT X.TagName
              ,X.LastUseDate
              ,X.UseCount
              ,X.GroupType
              ,X.GroupRow
              ,ROW_NUMBER() OVER(PARTITION BY X.TagName ORDER BY X.GroupRow,X.GroupType) NameRow
          FROM X
    ), Z AS (     -- Pick the first occurance of each relevant name
        SELECT TOP (@pMaxRows)
               Y.TagName
              ,Y.LastUseDate
              ,Y.UseCount
              ,Y.GroupType
          FROM Y
         WHERE Y.NameRow = 1
         ORDER BY Y.GroupRow,Y.GroupType
    )
    -- Return the names with their corresponding Relevance
    SELECT Z.TagName
          ,ROW_NUMBER() OVER(ORDER BY Z.GroupType ASC, Z.LastUseDate DESC, Z.UseCount DESC) Relevance
      FROM Z
     ORDER BY Relevance;

END;
/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC xESxApp.IntellisenseTags @pAccessToken = @AccessToken,@pTagPrefix = '#t',@pMaxRows = 3;
EXEC xESxApp.IntellisenseTags @pAccessToken = @AccessToken,@pTagPrefix = '#t',@pMaxRows = 4;
EXEC xESxApp.IntellisenseTags @pAccessToken = @AccessToken,@pTagPrefix = '#t',@pMaxRows = 5;
EXEC xESxApp.IntellisenseTags @pAccessToken = @AccessToken,@pTagPrefix = '#t',@pMaxRows = 6;
EXEC xESxApp.IntellisenseTags @pAccessToken = @AccessToken,@pTagPrefix = '#t',@pMaxRows = 7;
--*/
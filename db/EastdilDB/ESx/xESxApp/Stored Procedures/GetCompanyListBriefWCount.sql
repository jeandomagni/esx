﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/11/16
-- Description: Returns 2 datasets
--      The first contains one row with total number of rows meeting the criteria and a varchar with the applied criteria.
--      The Second contains the rows requested from the total possible mentioned above.
-- =============================================
CREATE PROCEDURE xESxApp.GetCompanyListBriefWCount
     @pAccessToken      iESx.TYP_AccessToken    -- This token represents the user on whose behalf the Put is being made.
    ,@pFilterText       nvarchar(1000)          -- Text to be searched for within company aliases
    ,@pSortColumn       sysname = 'IsWatched'   -- The column to use as primary sort order (secondary is alphabetically by AliasName). 0=IsWatched,1=AliasName
    ,@pSortDescending   bit = 0                 -- The default (0) sorts ascending.  1 causes sort to be descending
    ,@pMaxRows          int = 10                -- The number of rows to return
    ,@pFirstRow         int = 0                 -- The row number to start with when returning @pMaxRows results
AS
    SET NOCOUNT ON;

/*
    DECLARE @pAccessToken   iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
    DECLARE @pFilterText    nvarchar(1000)       = null;
    DECLARE @pSortColumn       sysname = 'IsWatched'   -- The column to use as primary sort order (secondary is alphabetically by AliasName). 0=IsWatched,1=AliasName
    DECLARE @pSortDescending   bit = 0                 -- The default (0) sorts ascending.  1 causes sort to be descending
    DECLARE @pMaxRows       int                  = 10;
    DECLARE @pFirstRow      int                  = 0;
--*/
    DECLARE @UserKey        int;
    DECLARE @TotalFilter    nvarchar(1000);
    DECLARE @QueryKey       int;
    DECLARE @ForceRebuild   bit     = 0;
    DECLARE @NoRebuild      bit     = 0;
    DECLARE @TotalRows      int;

    SELECT @UserKey     = GAcTo.User_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcTo;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

    SELECT @pFilterText = ISNULL(@pFilterText,'')
          ,@pMaxRows    = ISNULL(@pMaxRows,10)-1    -- Using BETWEEN below grabs an extra row so subtract it in advance
          ,@pFirstRow   = ISNULL(@pFirstRow,0)+1;   -- Add 1 because Row_Number() starts at 1

    -- Cache (re)population is only needed on page 1 otherwise just get the cache key
    IF @pFirstRow = 1 AND @pFilterText = ''
        SET @ForceRebuild = 1;
    IF @pFirstRow > 1
        SET @NoRebuild = 1;
    EXEC iESx.CacheCompanyBrief @pAccessToken,@pFilterText,@ForceRebuild,@NoRebuild,@QueryKey OUTPUT,@TotalFilter OUTPUT;

    -- In "...WCount" procedures we return 2 result sets.  The first is the total row count available
    SELECT @TotalRows = COUNT(DISTINCT CaCoB.[Canonical__Alias_Key])
      FROM dESx.CacheCompanyBrief CaCoB
      JOIN dESx.CacheSessionQuery CaSeQ
        ON CaSeQ.CacheSessionQuery_Key = CaCoB.CacheSessionQuery_Key
     WHERE CaCoB.CacheSessionQuery_Key = @QueryKey;

    SELECT @TotalRows                           AS TotalRows
          ,REPLACE(@TotalFilter,NCHAR(1),'\')   AS TotalFilter;

    SET @TotalRows = CASE WHEN @pSortDescending = 1 THEN @TotalRows+2 ELSE 0 END;

    -- The 2nd result set is the subset of available rows asked for
    WITH x AS (
        SELECT M.MentionID                                        AS MentionID
              ,ISNULL(UMDa.IsWatched,0)                             AS IsWatched
              ,Al1C.AliasName                                       AS CanonicalCompany
              ,MAX(Al2M.AliasName)                                  AS MatchingCompanyName
              ,ROW_NUMBER() OVER(ORDER BY 
                    CASE WHEN @pSortColumn = 'IsWatched' AND @pSortDescending = 0
                         THEN ISNULL(UMDa.IsWatched,0)
                    END DESC
                   ,CASE WHEN @pSortDescending = 0
                         THEN Al1C.AliasName
                    END ASC
                   ,CASE WHEN @pSortColumn = 'IsWatched' AND @pSortDescending = 1
                         THEN ISNULL(UMDa.IsWatched,0)
                    END ASC
                   ,CASE WHEN @pSortDescending = 1
                         THEN Al1C.AliasName
                    END DESC)                                       AS RowNumber 
          FROM dESx.CacheCompanyBrief CaCoB
          JOIN dESx.CacheSessionQuery CaSeQ
            ON CaSeQ.CacheSessionQuery_Key = CaCoB.CacheSessionQuery_Key
          JOIN dESx.Mention M
            ON M.Alias_Key = CaCoB.[Canonical__Alias_Key]
           AND M.IsPrimary = 1
          LEFT JOIN dESx.UserMentionData UMDa
            ON UMDa.Mention_Key = M.Mention_Key
           AND UMDa.User_Key = @UserKey
          JOIN dESx.CompanyAlias Al1C
            ON Al1C.CompanyAlias_Key = CaCoB.[Canonical__Alias_Key]
          JOIN dESx.CompanyAlias Al2M
            ON Al2M.CompanyAlias_Key = CaCoB.[Matching__Alias_Key]
         WHERE CaCoB.CacheSessionQuery_Key = @QueryKey
         GROUP BY ISNULL(UMDa.IsWatched,0),Al1C.AliasName,M.MentionID
    )
    SELECT x.MentionID
          ,x.IsWatched
          ,x.CanonicalCompany
          ,x.MatchingCompanyName
      FROM x
     WHERE x.RowNumber BETWEEN @pFirstRow AND @pFirstRow + @pMaxRows
     ORDER BY x.RowNumber;
/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
SELECT @AccessToken,@MentionID;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,null   ,'IsWatched'       ,0,60 ,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,null   ,'IsWatched'       ,0,60 ,59  ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,null   ,'IsWatched'       ,1,60 ,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,null   ,'IsWatched'       ,1,60 ,59  ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,null   ,'IsWatched'       ,1,66 ,1351; -- ask for extra to insure we're reading to the end
go
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
SELECT @AccessToken,@MentionID;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'','IsWatched'       ,0,200,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'','CanonicalCompany',0,200,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'','IsWatched'       ,1,200,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'','CanonicalCompany',1,200,0   ;
go
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
SELECT @AccessToken,@MentionID;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,' bi'  ,'IsWatched'       ,0,10 ,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,' bi'  ,'IsWatched'       ,1,10 ,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'x'    ,'IsWatched'       ,0,10 ,1   ;  -- the x is ignored because first row = 1
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'x'    ,'IsWatched'       ,1,10 ,1   ;  -- Again, x is ignored.  Reverse of 2nd bi list
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,null   ,'IsWatched'       ,0,10 ,0   ;  -- reset to all
go
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
SELECT @AccessToken,@MentionID;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'group','IsWatched'       ,0,200,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'group','CanonicalCompany',0,200,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'h'    ,'IsWatched'       ,0,10 ,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'-'    ,'IsWatched'       ,0,10 ,0   ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'-'    ,'IsWatched'       ,0,10 ,10  ;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'%'    ,'IsWatched'       ,0,10 ,0   ;
go
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC xESxApp.GetCompanyListBriefWCount @AccessToken,'walt' ,'CanonicalCompany',1,200,0   ;
--*/
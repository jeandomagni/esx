﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2015/12/08
-- Description: Returns the top x Companies that match the prefix passed, as sorted by relevance.
-- =============================================
CREATE PROCEDURE xESxApp.IntellisenseCompanies
     @pAccessToken      iESx.TYP_AccessToken      -- This token represents the user on whose behalf the call is being made.
    ,@pNamePattern      iESx.TYP_Reference
    ,@pMaxRows          int                  = 10
AS
BEGIN
    SET NOCOUNT ON;
    /*
    DECLARE @pAccessToken iESx.TYP_AccessToken = (SELECT top 1 CaAcTo.AccessToken FROM dESx.CacheAccessToken CaAcTo WHERE User_Key = 1 order by createtime desc);
    DECLARE @pNamePattern iESx.TYP_Reference   = '';
    DECLARE @pMaxRows int                      = 8;
    --*/

    DECLARE @UserKey        int;
    DECLARE @SearchPattern  iESx.TYP_Reference  = SUBSTRING(@pNamePattern,1,127)+'%';
    DECLARE @MentionPattern iESx.TYP_Reference  = '@' + @SearchPattern;
    DECLARE @Potentials     table(MentionID iESx.TYP_Reference,AliasName nvarchar(128),Company_Key INT,LastUseDate date,UseCount int,GroupType int);

    SELECT @UserKey     = GAcTo.User_Key
      FROM iESx.GetAccessContext(@pAccessToken) GAcTo;

    IF @UserKey IS NULL
    BEGIN
        RAISERROR ('Access token is invalid or expired',16,0);
        RETURN;
    END;

    INSERT INTO @Potentials
    SELECT TOP (@pMaxRows)                -- Users personal Mentions
           M.MentionID
          ,AL.AliasName
          ,AL.Company_Key
          ,UMDa.LastUseDate
          ,UMDa.UseCount
          ,1 GroupType                    -- Users Mentions are 'Most Relevant'
      FROM dESx.Mention M
      JOIN dESx.UserMentionData UMDa
        ON UMDa.Mention_Key = M.Mention_Key
      JOIN dESx.CompanyAlias AL
        ON AL.CompanyAlias_Key = M.Alias_Key
     WHERE (M.MentionID LIKE @MentionPattern
           OR AL.AliasName LIKE @SearchPattern)
       AND UMDa.User_Key = @UserKey
     ORDER BY 3 DESC,4 DESC,1;

    INSERT INTO @Potentials
    SELECT TOP (@pMaxRows)                -- Recently created Mentions
           M.MentionID
          ,AL.AliasName
          ,AL.Company_Key
          ,M.CreatedDate AS CreateDate
          ,M.TotalUseCount
          ,2 GroupType                    -- listed 2nd
      FROM dESx.Mention M
      JOIN dESx.CompanyAlias AL
        ON AL.CompanyAlias_Key = M.Alias_Key
     WHERE (M.MentionID LIKE @MentionPattern
           OR AL.AliasName LIKE @SearchPattern)
     ORDER BY 3 DESC,4 DESC,1;

    INSERT INTO @Potentials
    SELECT TOP (@pMaxRows)                -- Mentions recently used by others
           M.MentionID
          ,AL.AliasName
          ,AL.Company_Key
          ,M.LastUseDate
          ,M.TotalUseCount
          ,3 GroupType                    -- Listed last
      FROM dESx.Mention M
      JOIN dESx.CompanyAlias AL
        ON AL.CompanyAlias_Key = M.Alias_Key
     WHERE (M.MentionID LIKE @MentionPattern
           OR AL.AliasName LIKE @SearchPattern)
     ORDER BY 3 DESC,4 DESC,1;

--  DECLARE @pMaxRows int                 = 5;
    WITH W AS (   -- Order each GroupType by LastUsedDate and UseCount
        SELECT pT.MentionID
              ,pT.AliasName
              ,pT.Company_Key
              ,pT.LastUseDate
              ,pT.UseCount
              ,pT.GroupType
              ,ROW_NUMBER() OVER(PARTITION BY pT.GroupType ORDER BY pT.LastUseDate DESC,pT.UseCount DESC) GroupRow
          FROM @Potentials pT
    ), X AS (     -- Mark duplicates with NameRow > 1 where kept row is the one in the highest order group type
        SELECT W.MentionID
              ,W.AliasName
              ,W.Company_Key
              ,W.LastUseDate
              ,W.UseCount
              ,W.GroupType
              ,W.GroupRow
              ,ROW_NUMBER() OVER(PARTITION BY W.MentionID ORDER BY W.GroupRow,W.GroupType) NameRow
          FROM W
    ), Y AS (     -- Pick the first occurance of each relevant name
        SELECT X.MentionID
              ,X.AliasName
              ,X.Company_Key
              ,X.LastUseDate
              ,X.UseCount
              ,X.GroupType
              ,ROW_NUMBER() OVER(PARTITION BY X.GroupType ORDER BY X.GroupRow) NameRow
          FROM X
         WHERE X.NameRow = 1
    ), Z AS (     -- Renumber the group type rows so groups don't fall off the list due to duplicates in another list
        SELECT TOP (@pMaxRows)
               Y.MentionID
              ,Y.AliasName
              ,Y.Company_Key
              ,ROW_NUMBER() OVER(ORDER BY Y.GroupType ASC, Y.LastUseDate DESC, Y.UseCount DESC,Y.MentionID) Relevance
          FROM Y
         ORDER BY Y.NameRow,Y.GroupType
    )
    -- Return the names with their corresponding Relevance
    SELECT Z.MentionID
          ,Z.AliasName
          ,z.Company_Key
          ,ROW_NUMBER() OVER(ORDER BY Z.Relevance) Relevance
      FROM Z
     ORDER BY Relevance;

END;
/*Test
GO
DECLARE @AccessToken iESx.TYP_AccessToken;
DECLARE @MentionID iESx.TYP_Reference;
EXEC xESxApp.Login 'User1',@AccessToken OUTPUT,@MentionID OUTPUT;
EXEC xESxApp.IntellisenseCompanies @pAccessToken = @AccessToken,@pNamePattern = '' ,@pMaxRows = 8;
EXEC xESxApp.IntellisenseCompanies @pAccessToken = @AccessToken,@pNamePattern = 's',@pMaxRows = 4;
EXEC xESxApp.IntellisenseCompanies @pAccessToken = @AccessToken,@pNamePattern = 'a',@pMaxRows = 5;
EXEC xESxApp.IntellisenseCompanies @pAccessToken = @AccessToken,@pNamePattern = 'a',@pMaxRows = 6;
EXEC xESxApp.IntellisenseCompanies @pAccessToken = @AccessToken,@pNamePattern = 'a',@pMaxRows = 7;
--*/
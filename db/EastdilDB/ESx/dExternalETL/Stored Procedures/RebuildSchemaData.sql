﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/09
-- Description: Cleanses and organizes WEB based ETL data into tables and views presented in rESxStage.
-- =============================================
CREATE PROCEDURE [dExternalETL].[RebuildSchemaData]
AS 
BEGIN
	-- Currently the tables loaded don't need any additional transformation or cleaning.
	PRINT 'Nothing to do in dExternalETL.RebuildSchemaData'
END;
/*Test
go
EXEC dExternalETL.RebuildSchemaData;
*/
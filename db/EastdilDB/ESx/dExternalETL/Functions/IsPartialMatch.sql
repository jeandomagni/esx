﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/11
-- Description: Return a flag indicating if this was a partial match or not.
--              Note only the 1st result is examined.  If there are more this function is useless since
--              it can't tell you which result contains the partial match.
-- =============================================
CREATE FUNCTION dExternalETL.IsPartialMatch(@Data xml) 
	RETURNS bit AS 
BEGIN 
	RETURN @Data.exist('GeocodeResponse[1]/result[1][partial_match="true"]'); 
END
/*Test
go
select dExternalETL.ExtractPartialMatch('<GeocodeResponse><status>OK</status><result><partial_match>Partial</partial_match></result></GeocodeResponse>');
*/
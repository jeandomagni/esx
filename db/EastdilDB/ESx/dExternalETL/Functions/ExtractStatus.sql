﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/04
-- Description: Returns the status value from the geocoding response.
-- =============================================
CREATE FUNCTION [dExternalETL].[ExtractStatus](@Data xml) 
	RETURNS varchar(20) AS 
BEGIN 
	RETURN @Data.value('GeocodeResponse[1]/status[1]','varchar(20)'); 
END
/*Test
go
select dExternalETL.ExtractStatus('<GeocodeResponse><status>OK</status></GeocodeResponse>') 
*/
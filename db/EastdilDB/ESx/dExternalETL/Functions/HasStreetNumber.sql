﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/11
-- Description: Determines if geo data resolves the address down to the street number.
--              Note only the 1st result is examined.  If there are more this function is useless since
--              it can't tell you which result contains the partial match.
-- =============================================
CREATE FUNCTION [dExternalETL].[HasStreetNumber](@Data xml)
	RETURNS bit AS
BEGIN
	RETURN @Data.exist('GeocodeResponse[1]/result[1]/address_component[type="street_number"]');
END
/*Test
go
select dExternalETL.HasStreetNumber('<GeocodeResponse><result><address_component><type>street_number</type></address_component></result></GeocodeResponse>') 
*/
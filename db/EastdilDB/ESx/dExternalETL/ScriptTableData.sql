-- This script (re)creates SQLCMD scripts *in this project* that contain insert statements to recreate table content.
-- That is, it scripts out the data currently in local database tables for (re)loading a table's content.
-- Due to limitations in SQLCMD functionality the path to the scripts must be hard coded.

-- THIS SCRIPT ONLY WORKS *CORRECTLY* when run directly from sqlcmd.exe.
-- Execution succeeds, but results are incorrect when run from SSMS or VS in SQLCMD MODE.
-- Either tool can be used for basic development and testing but output can only be verified by using the commands below.
/*
-- These commands run THIS (you're looking at it now) script in a way that executes properly in SSMS/VS.
-- Do not uncomment them (recursion!!!).  Select them and execute with [Shift][Control]E
-- Don't forget to save the file content.  It executes what's on disk not what's on your screen!
:SETVAR SolutionPath C:\ESX\db\EastdilDB\
:SETVAR ScriptPath .\ESx\dExternalETL\
:!! SQLCMD -d ESxDEV -u -W -E -i "$(SolutionPath)$(ScriptPath)ScriptTableData.sql"
--*/

------------------------------------------------------------------------------------------------
-- When adding or deleting scripts be sure to update Script.PostDeployment1.sql so it knows 
-- what scripts are available and what order they must be executed in.
------------------------------------------------------------------------------------------------
GO
--/*
PRINT 'Script generation starting';		-- TOTO:  On next MAJOR REBUILD, add an INT key so we don't need to code dates for @pWhere...
:SETVAR SQLCMDHEADERS -1
:SETVAR SQLCMDCOLWIDTH 65535
:SETVAR SQLCMDMAXFIXEDTYPEWIDTH 0
:SETVAR SQLCMDMAXVARTYPEWIDTH 0
:SETVAR SolutionPath C:\ESX\db\EastdilDB\
:SETVAR ScriptPath ".\ESx\xESxApp\EtlData\"
:SETVAR Schema dExternalETL
PRINT 'Solution Script Path = $(SolutionPath)$(ScriptPath)';
SET nocount ON;
GO
:SETVAR Table PhysicalAddressGeoData
:OUT $(SolutionPath)$(ScriptPath)$(Table)1.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pMaxRows=10000,@pOrderBy='ValidationDatetime',
    @pSelect='LookupValue,ValidationData,ValidationDatetime,ValidatedAddress,Coordinates',
    @pWhere='ValidationDatetime < ''2016-03-29'' or ValidationDatetime is null',@pAddPrint=1;
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)2.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pMaxRows=10000,@pOrderBy='ValidationDatetime',
    @pSelect='LookupValue,ValidationData,ValidationDatetime,ValidatedAddress,Coordinates',
    @pWhere='ValidationDatetime between ''2016-03-29'' and ''2016-04-05''',@pAddPrint=1;
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)3.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pMaxRows=10000,@pOrderBy='ValidationDatetime',
    @pSelect='LookupValue,ValidationData,ValidationDatetime,ValidatedAddress,Coordinates',
    @pWhere='ValidationDatetime between ''2016-04-05'' and ''2016-04-07 14:00''',@pAddPrint=1;
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)4.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pMaxRows=10000,@pOrderBy='ValidationDatetime',
    @pSelect='LookupValue,ValidationData,ValidationDatetime,ValidatedAddress,Coordinates',
    @pWhere='ValidationDatetime between ''2016-04-07 14:00'' and ''2016-04-09 20:00''',@pAddPrint=1;
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)5.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pMaxRows=10000,@pOrderBy='ValidationDatetime',
    @pSelect='LookupValue,ValidationData,ValidationDatetime,ValidatedAddress,Coordinates',
    @pWhere='ValidationDatetime between ''2016-04-09 20:00'' and ''2016-05-07''',@pAddPrint=1;
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)6.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pMaxRows=10000,@pOrderBy='ValidationDatetime',
    @pSelect='LookupValue,ValidationData,ValidationDatetime,ValidatedAddress,Coordinates',
    @pWhere='ValidationDatetime between ''2016-05-07'' and ''2016-05-10''',@pAddPrint=1;
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)7.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pMaxRows=10000,@pOrderBy='ValidationDatetime',
    @pSelect='LookupValue,ValidationData,ValidationDatetime,ValidatedAddress,Coordinates',
    @pWhere='ValidationDatetime between ''2016-05-10'' and ''2016-05-20''',@pAddPrint=1;
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)8.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pMaxRows=10000,@pOrderBy='ValidationDatetime',
    @pSelect='LookupValue,ValidationData,ValidationDatetime,ValidatedAddress,Coordinates',
    @pWhere='ValidationDatetime > ''2016-05-20''',@pAddPrint=1;
GO
:OUT STDOUT
PRINT 'Scripting Complete';
GO
--*/
﻿CREATE TABLE [dExternalETL].[PhysicalAddressGeoData] (
    [LookupValue]        NVARCHAR (303)    NOT NULL,
    [ValidationData]     XML               NULL,
    [ValidationDatetime] DATETIME          NULL,
    [ValidatedAddress]   NVARCHAR (200)    NULL,
    [Coordinates]        [sys].[geography] NULL,
    [AddressStatus]      AS                (case when [ValidationData] IS NULL then NULL when [dExternalETL].[ExtractStatus]([ValidationData])<>'OK' then [dExternalETL].[ExtractStatus]([ValidationData]) when [Coordinates] IS NULL then 'Ambiguous' when [dExternalETL].[HasStreetNumber]([ValidationData])=(1) then 'OK' when [dExternalETL].[IsPartialMatch]([ValidationData])=(1) then 'Partial' else 'OK' end),
    CONSTRAINT [PK_PhysicalAddressGeoData] PRIMARY KEY CLUSTERED ([LookupValue] ASC)
);










﻿CREATE VIEW [rESxStage].[TransactionTypes]
AS
    SELECT TransactionTypeID
          ,CASE WHEN TransactionType IN ('Agency Debt Placement','Financing','Secured Debt Placement')      THEN 'Debt Placement'
                WHEN TransactionType IN ('Equity Raise','JV/Recap','JV/Recap (WFS)')                        THEN 'Equity Sale <100%'
                WHEN TransactionType IN ('Equity Sale','Private Placement','Sale Leaseback')                THEN 'Equity Sale 100%'
                WHEN TransactionType IN ('Common Equity','Derivatives - Fixed Income','Investment Banking') THEN 'Investment Banking'
                WHEN TransactionType IN ('Loan Sale')                                                       THEN 'Loan Sale'
                                                                                                            ELSE 'Non-transactional Advisory'
            END AS TransactionType
          ,DisplayOrder
          ,msrepl_tran_version
          ,RowVersion
          ,DateAdded
          ,LastUpdate
          ,AddedBy
          ,UpdatedBy
      FROM rESP.TransactionTypes;
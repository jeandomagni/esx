﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/22
-- Description: Presents a view of Property data that includes Geocoding data.
-- =============================================
CREATE VIEW rESxStage.Properties
AS
WITH x AS (  
    SELECT CAST(pr.PropertyID AS int) PropertyID
          ,pr.PropertyName
          ,pr.PropertyDescription
          ,pr.PropertyTypeID
          ,pr.LocationQuality
          ,pr.Address1
          ,pr.City
          ,pr.State
          ,pr.Country
          ,pr.PropertyType
          ,pr.PropertySize
          ,pr.ProjectSizeTypeID
          ,pr.LastSoldDate
          ,pr.Buyer
          ,pr.BuyerDBA
          ,pr.BuyerCompanyID
          ,pr.DateAcquired
          ,pr.Seller
          ,pr.SellerDBA
          ,pr.SellerCompanyID
          ,pr.DateSold
          ,pr.LenderCompanyID
          ,pr.LenderDBA
          ,pr.LastUpdate
          ,pr.LookupValue       -- select *
      FROM dEspStage.Properties pr
    UNION ALL
    SELECT -slp.PropertyID   AS PropertyID      -- insure the keys join to each other later
          ,slp.PropertyName
          ,NULL AS PropertyDescription
          ,NULL AS PropertyTypeID
          ,NULL AS LocationQuality
          ,slp.Address1
          ,slp.City
          ,slp.State
          ,slp.Country
          ,NULL AS PropertyType
          ,NULL AS PropertySize
          ,NULL AS ProjectSizeTypeID
          ,NULL AS LastSoldDate
          ,NULL AS Buyer
          ,NULL AS BuyerDBA
          ,NULL AS BuyerCompanyID
          ,NULL AS DateAcquired
          ,NULL AS Seller
          ,NULL AS SellerDBA
          ,NULL AS SellerCompanyID
          ,NULL AS DateSold
          ,NULL AS LenderCompanyID
          ,NULL AS LenderDBA
          ,slp.UpdateDatetime
          ,slp.LookupValue
      FROM dExcelETL.SLProperties slp
)
    SELECT pr.PropertyID
          ,pr.PropertyName
          ,pr.PropertyDescription
          ,pr.PropertyTypeID
          ,pr.LocationQuality
          ,pr.Address1
          ,pr.City
          ,pr.State
          ,pr.Country
          ,pr.PropertyType
          ,pr.PropertySize
          ,pr.ProjectSizeTypeID
          ,pr.LastSoldDate
          ,pr.Buyer
          ,pr.BuyerDBA
          ,pr.BuyerCompanyID
          ,pr.DateAcquired
          ,pr.Seller
          ,pr.SellerDBA
          ,pr.SellerCompanyID
          ,pr.DateSold
          ,pr.LenderCompanyID
          ,pr.LenderDBA
          ,pr.LastUpdate
          ,pr.LookupValue
          ,prgd.ValidationData
          ,prgd.ValidationDatetime
          ,prgd.ValidatedAddress
          ,prgd.Coordinates
          ,prgd.AddressStatus   --*/ select *
      FROM x pr
      LEFT JOIN dExternalETL.PhysicalAddressGeoData prgd
        ON prgd.LookupValue = pr.LookupValue;
/*Test
GO
SELECT * FROM rESxStage.Properties;
--*/
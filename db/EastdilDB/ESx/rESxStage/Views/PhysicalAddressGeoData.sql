﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/11
-- Description: Exposes Geo data collected by a PowerShell script from Google
-- =============================================
CREATE VIEW [rESxStage].[PhysicalAddressGeoData] AS
    SELECT *
      FROM dExternalETL.PhysicalAddressGeoData;
/*Test
GO
select * from rESxStage.PhysicalAddressGeoData;
--*/
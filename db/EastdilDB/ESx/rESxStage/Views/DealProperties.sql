﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/22
-- Description: Relates Deals to Properties for ESx.
-- =============================================
CREATE VIEW [rESxStage].[DealProperties] AS
    WITH x AS (
        SELECT sldp.DealID
              ,pr.PropertyID
              ,sldp.UpdateDatetime       --select *
          FROM dExcelETL.SLDealProperties sldp
          JOIN rESxStage.Properties pr
            ON pr.PropertyID = -sldp.PropertyID
    ), y AS (
        SELECT dpr.DealID
              ,pr.PropertyID
              ,ISNULL(dpr.LastUpdate,dpr.DateAdded)LastUpdate
          FROM rESP.DealProperties dpr
          JOIN rESxStage.Properties pr
            ON pr.PropertyID = dpr.PropertyID
    )
    SELECT * 
      FROM x
     UNION ALL
    SELECT *
      FROM y
     WHERE  CAST(y.DealID AS varchar)+'|'+CAST(y.PropertyID AS varchar) NOT IN 
    (SELECT CAST(x.DealID AS varchar)+'|'+CAST(x.PropertyID AS varchar) FROM x);
/*Test
GO
SELECT * FROM rESxStage.DealProperties;
--*/
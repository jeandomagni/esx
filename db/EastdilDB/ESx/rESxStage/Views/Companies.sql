﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/04
-- Description: Exposes the ESP.dbo.companies table to ESx with metadata
-- =============================================
CREATE VIEW [rESxStage].[Companies]
AS
    SELECT CoDL.CompanyName                                               AS CorrectedName
          ,slc.CanonicalName                                              AS ParentName
          ,COALESCE(CoDLp.CompanyID,CoDLp2.CompanyID,CoDLp3.CompanyID)    AS ParentID         -- Take match on algorithm 1 before 2 or 3
          ,CoDL.DBAName                                                   AS ParentParentName
          ,COALESCE(CoDLpp.CompanyID,CoDLpp2.CompanyID,CoDLpp3.CompanyID) AS ParentParentID   -- Take match on algorithm 1 before 2 or 3
          ,CASE WHEN CoDL.DuplicateOfID = c.CompanyID THEN NULL ELSE CoDL.DuplicateOfID END DuplicateOfID
          ,c.CompanyID
          ,c.CompanyName
          ,c.CompanyLegalName
          ,c.Division
          ,c.AddressID
          ,c.ShippingID
          ,c.TickerSymbol
          ,c.CIBOSCode
          ,c.PrimaryCompanyRecord
          ,c.PrimaryCompanyID
          ,c.PrimaryCompanyCode
          ,c.Region
          ,NULLIF(c.MainPhone,'')               AS MainPhone
          ,NULLIF(c.Fax,'')                     AS Fax
          ,NULLIF(c.TollFreeNumber,'')          AS TollFreeNumber
          ,NULLIF(c.AlternateCompanyPhone,'')   AS AlternateCompanyPhone
          ,NULLIF(c.CompanyEmail,'')            AS CompanyEmail
          ,NULLIF(c.Website,'')                 AS Website
          ,c.MiscInfo
          ,c.Client
          ,c.ESContactEmployeeID
          ,c.REIBCompany
          ,c.PECompany
          ,c.PECompanyTypeID
          ,c.DoNotSolicitAsOfDate
          ,c.DoNotSolicit
          ,c.NoInterestInDeals
          ,c.NoInterestNote
          ,c.CompanyType
          ,c.ESClassification
          ,c.PrimaryInstType
          ,c.InstitutionType
          ,c.LeadQualRelCompanyRec
          ,c.LeadQualRelCompanyID
          ,c.QualifiedRelationship
          ,c.QualRelNote
          ,c.DMSLeadQualRelCompanyID
          ,c.WFCrossSell
          ,c.RestrictedVisibility
          ,c.TrackedByAsiaTeam
          ,c.TypicalInvestmentSize
          ,c.AssetsUnderManagement
          ,c.RealEstateAllocationPct
          ,c.Office
          ,c.Industrial
          ,c.Mixed
          ,c.Residential
          ,c.Retail
          ,c.Hotel
          ,c.USA
          ,c.Canada
          ,c.Europe
          ,c.Asia
          ,c.MiddleEast
          ,c.CompanyRegionID
          ,c.ConsultantID
          ,c.WFSCorrelation
          ,c.FCPAListName
          ,c.NameChange
          ,c.AddressChange
          ,c.OFACCheck
          ,c.OFACCheckDate
          ,c.FGOV
          ,c.Active
          ,c.ReasonInactive
          ,c.Status
          ,c.FGOVT
          ,c.DBAName
          ,c.Development
          ,c.OfficeMarketNote
          ,c.IndustrialMarketNote
          ,c.MixedMarketNote
          ,c.ResidentialMarketNote
          ,c.RetailMarketNote
          ,c.HotelMarketNote
          ,c.USAMarketNote
          ,c.CanadaMarketNote
          ,c.EuropeMarketNote
          ,c.AsiaMarketNote
          ,c.MiddleEastMarketNote
          ,ISNULL(c.LastUpdate,c.DateAdded) LastUpdate    -- select *
      FROM rESP.Companies c
      JOIN dEspStage.CompanyDupList CoDL            -- Get Corrected name
           LEFT JOIN dEspStage.SLCanonicals slc     -- Get Parent Name (Canonical name)
             ON slc.CanonicalID = CoDL.Canonical
        ON CoDL.CompanyID = c.CompanyID
      LEFT JOIN (                                   -- Minimum 'Parent' row algorithm #1 - match on name
            SELECT CoDLi.CompanyName
                  ,ISNULL(MIN(CoDLi.DuplicateOfID),MIN(CoDLi.CompanyID)) AS CompanyID
              FROM dEspStage.CompanyDupList CoDLi
             GROUP BY CoDLi.CompanyName) CoDLp
        ON CoDLp.CompanyName = slc.CanonicalName
      OUTER APPLY (                                 -- Minimum 'Parent' row Algorithm #2 - first match with same starting characters
            SELECT ISNULL(MIN(CoDLi.DuplicateOfID),MIN(CoDLi.CompanyID)) AS CompanyID
              FROM dEspStage.CompanyDupList CoDLi
             WHERE CoDLi.Canonical = slc.CanonicalID
               AND SUBSTRING(CoDLi.CompanyName,1,3) = SUBSTRING(slc.CanonicalName,1,3)) CoDLp2
      OUTER APPLY (                                 -- Minimum 'Parent' row Algorithm #3 - first match of any kind
            SELECT ISNULL(MIN(CoDLi.DuplicateOfID),MIN(CoDLi.CompanyID)) AS CompanyID
              FROM dEspStage.CompanyDupList CoDLi
             WHERE CoDLi.Canonical = slc.CanonicalID) CoDLp3
      LEFT JOIN (                                   -- Minimum DBA/ParentParent row algorithm #1 - match on name
            SELECT REPLACE(CoDLi.SquashedName,'&','')                    AS CompanyName
                  ,ISNULL(MIN(CoDLi.DuplicateOfID),MIN(CoDLi.CompanyID)) AS CompanyID
              FROM dEspStage.CompanyDupList CoDLi
             GROUP BY REPLACE(CoDLi.SquashedName,'&','')) CoDLpp
        ON CoDLpp.CompanyName = CoDL.DBAName
      OUTER APPLY (                                   -- Minimum DBA/ParentParent row algorithm #2 - first match with same starting characters
            SELECT ISNULL(MIN(CoDLi.DuplicateOfID),MIN(CoDLi.CompanyID)) AS CompanyID,COUNT(*) choices
              FROM dEspStage.CompanyDupList CoDLi
             WHERE CoDLi.DBAName = CoDL.DBAName
               AND SUBSTRING(CoDLi.CompanyName,1,3) = SUBSTRING(CoDL.DBAName,1,3)) CoDLpp2
      OUTER APPLY (                                   -- Minimum DBA/ParentParent row algorithm #3 - first match of any kind
            SELECT ISNULL(MIN(CoDLi.DuplicateOfID),MIN(CoDLi.CompanyID)) AS CompanyID
              FROM dEspStage.CompanyDupList CoDLi
             WHERE CoDLi.DBAName = CoDL.DBAName) CoDLpp3;
/*Test
GO
select * from rESxStage.Companies;
-- 2169 parents have no id | 4567 have them | 6735 total 6736
-- 4422 parent parents have no id | 4828 have them | 9250 total 9250
--*/
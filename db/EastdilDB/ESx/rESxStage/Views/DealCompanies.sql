﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/22
-- Description: Relates Deals to companies for ESx.
-- =============================================
CREATE VIEW [rESxStage].[DealCompanies] AS
    SELECT DealID
          ,UpdateDatetime
          ,CompanyID
          ,CompanyRole
      FROM dExcelETL.SLDealCompanies
/*Test
GO
SELECT * FROM rESxStage.DealCompanies;
--*/
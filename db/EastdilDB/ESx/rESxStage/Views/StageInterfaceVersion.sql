﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/09
-- Description: Allows stage data to be rebuilt only when needed because it's very time consuming.
--      Increment this value when stage source data changes.
--      This is needed because source file timestamps are not reliable.
-- =============================================
CREATE VIEW [rESxStage].[StageInterfaceVersion]
AS SELECT 2 AS DataVersion;
/*
select * from rESxStage.StageInterfaceVersion;
--*/
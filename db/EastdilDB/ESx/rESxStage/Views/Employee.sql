﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/15
-- Description: Exposes certain ESP.dbo.Employee data to ESx.
-- =============================================
CREATE VIEW [rESxStage].[Employee]
AS
    SELECT E.EmployeeID
          ,E.EmployeeName
          ,E.LastName
          ,E.FirstName
          ,E.MiddleName
          ,E.NickName
          ,E.EmpInitials
          ,E.Department
          ,E.SeniorManagement
          ,E.RegisteredRep
          ,E.SupervisoryPrincipal
          ,E.ManagingDirectorAndUp
          ,E.OnDeals
          ,E.ConfidentialDealAccess
          ,CASE E.Office WHEN 'DC' THEN 'Washington, DC' ELSE E.Office END AS Office
          ,E.Title
          ,E.Extension
          ,E.EMailAddress EmailAddress
          ,E.AssistedBy
          ,E.Phone
          ,E.MobilePhone
          ,E.WorkFax
          ,E.HomeFax
          ,E.OtherPhone
          ,E.WorkPhone
          ,E.BusinessPhone
          ,E.Spouse
          ,E.UsernameWF AS AdEntID
          ,E.LastUpdate
          ,E.Active         -- select *
      FROM rESP.Employees E;

/*Test
GO
SELECT * FROM rESxStage.Employee;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/11
-- Description: Exposes the ESP.dbo.addresses data to ESx with metadata
-- =============================================
CREATE VIEW [rESxStage].[Addresses] AS
    SELECT slp.AddressID
          ,slp.Description
          ,slp.TypeCode
          ,slp.Address1
          ,slp.Address2
          ,slp.City
          ,slp.State
          ,slp.PostalCode
          ,ISNULL(NULLIF(slp.Country,'USA'),'United States') AS Country
          ,slp.IsPrimary
          ,slp.IsMailing
          ,slp.Salutation
          ,slp.Routing
          ,slp.Address3
          ,slp.Address4
          ,slp.Timezone
          ,slp.Source
          ,slp.Confidence
          ,ISNULL(slp.LastUpdate,slp.DateAdded) UpdateDatetime
          ,slp.AddedBy
          ,slp.UpdatedBy
          ,slp.LookupValue
          ,slprgd.ValidationData
          ,slprgd.ValidationDatetime
          ,slprgd.ValidatedAddress
          ,slprgd.Coordinates
          ,slprgd.AddressStatus --*/ select *
      FROM dEspStage.Addresses slp
      JOIN dExternalETL.PhysicalAddressGeoData slprgd
        ON slprgd.LookupValue = slp.LookupValue;
/*Test
GO
SELECT * FROM rESxStage.Addresses;
--*/
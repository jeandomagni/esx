﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/11
-- Description: Exposes properties of deals from SLTables spreadsheet
-- =============================================
CREATE VIEW [rESxStage].[SLProperties] AS
    SELECT *
      FROM dExcelETL.SLProperties;
/*Test
GO
select * from rESxStage.SLProperties;
--*/
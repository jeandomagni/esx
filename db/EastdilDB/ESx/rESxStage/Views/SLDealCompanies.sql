﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/11
-- Description: Exposes deal companies from SLTables spreadsheet
-- =============================================
CREATE VIEW [rESxStage].[SLDealCompanies] AS
    SELECT *
      FROM dExcelETL.SLDealCompanies;
/*Test
GO
select * from rESxStage.SLDealCompanies;
--*/
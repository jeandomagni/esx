﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/11
-- Description: Exposes the ESP.dbo.Contacts data to ESx with (possible) duplicates indicated.
-- =============================================
CREATE VIEW [rESxStage].[Contacts] AS
    WITH x AS (
        SELECT MIN(ContactID                   )   AS ContactID
              ,EmailAddress     -- select *
          FROM rESP.Contacts
         WHERE Active = 1 
           AND NULLIF(EmailAddress,'') IS NOT NULL
         GROUP BY EmailAddress
        HAVING COUNT(*)>1
    ) --/*
    SELECT CASE WHEN x.ContactID <> Cn.ContactID THEN x.ContactID END AS DuplicateOfID
          ,ISNULL(Cn.LastUpdate,Cn.DateAdded) LastUpdate
          ,Cn.ContactID
          ,Cn.CompanyID
          ,NULLIF(Cn.FullName,'')           AS FullName         
          ,NULLIF(Cn.FirstName,'')          AS FirstName        
          ,NULLIF(Cn.MiddleName,'')         AS MiddleName       
          ,NULLIF(Cn.LastName,'')           AS LastName         
          ,NULLIF(Cn.Nickname,'')           AS Nickname         
          ,NULLIF(Cn.FirstnameLastname,'')  AS FirstnameLastname
          ,NULLIF(Cn.LastnameFirstname,'')  AS LastnameFirstname
          ,NULLIF(Cn.Prefix,'')             AS Prefix           
          ,NULLIF(Cn.Suffix,'')             AS Suffix           
          ,NULLIF(Cn.Salutation,'')         AS Salutation       
          ,NULLIF(Cn.Title,'')              AS Title            
          ,NULLIF(Cn.Department,'')         AS Department       
          ,NULLIF(Cn.Other,'')              AS Other            
          ,Cn.AddressID
          ,Cn.Address2ID
          ,NULLIF(Cn.Address2For,'')        AS Address2For      
          ,NULLIF(Cn.Address2TypeID,'')     AS Address2TypeID   
          ,NULLIF(Cn.Phone,'')              AS Phone
          ,NULLIF(Cn.DirectPhone,'')        AS DirectPhone
          ,NULLIF(Cn.MainPhoneExt,'')       AS MainPhoneExt
          ,NULLIF(Cn.Fax,'')                AS Fax
          ,NULLIF(Cn.HomePhone,'')          AS HomePhone
          ,NULLIF(Cn.MobilePhone,'')        AS MobilePhone
          ,NULLIF(Cn.BlackberryPhone,'')    AS BlackberryPhone
          ,NULLIF(Cn.OtherPhone,'')         AS OtherPhone
          ,NULLIF(Cn.OtherPhoneType,'')     AS OtherPhoneType
          ,NULLIF(Cn.Pager,'')              AS Pager
          ,NULLIF(Cn.EmailAddress,'')       AS EmailAddress
          ,NULLIF(Cn.SecondaryEmail,'')     AS SecondaryEmail
          ,NULLIF(Cn.WebAddress,'')         AS WebAddress
          ,NULLIF(Cn.Assistant,'')          AS Assistant
          ,NULLIF(Cn.AssistantPhone,'')     AS AssistantPhone
          ,NULLIF(Cn.AssistantEmail,'')     AS AssistantEmail
          ,Cn.Active
          ,NULLIF(Cn.ReasonInactive,'')     AS ReasonInactive
          ,Cn.InactivatedByEmployeeID
          ,Cn.PreviousEmployers
          ,Cn.SpecialConditions
          ,Cn.ETeasersBlocked
          ,Cn.ApprovalRequiredForMarketing
          ,Cn.MarketingApprovalEmployeeID
          ,Cn.MarketingApprovalCCTeam
          ,Cn.Notes
          ,Cn.DRENumber
          ,Cn.ESContact
          ,Cn.ESContactEmployeeID
          ,Cn.KeyContact
          ,Cn.WellsFargoContact
          ,Cn.BestRelationship
          ,Cn.RetailRelationship
          ,Cn.EntityType
          ,Cn.InvestorType
          ,Cn.ContactType
          ,Cn.CategoryID
          ,Cn.Qualified
          ,Cn.DoNotSolicit
          ,Cn.DoNotSolicitAsOfDate
          ,Cn.DoNotSolicitRequestID
          ,Cn.ExpressedConsent
          ,Cn.DoNotPhone
          ,Cn.DoNotEmail
          ,Cn.DoNotMail
          ,Cn.DoNotFax
          ,Cn.PrefersDigitalOM
          ,Cn.IsPrimary
          ,Cn.Principal
          ,Cn.Broker
          ,Cn.Investor
          ,Cn.Client
          ,Cn.Vendor
          ,NULLIF(Cn.Spouse,'')             AS Spouse
          ,Cn.Referral
          ,Cn.Status
          ,Cn.REIBContact
          ,Cn.REIBCorporateOfficerTitle
          ,Cn.REIBBoardMemberTitle
          ,Cn.NameChange
          ,Cn.AddressChange
          ,Cn.SentToCDC
          ,Cn.SentToCDCDate
          ,Cn.CanadaOptIn
          ,Cn.AutoInExpirationDate
          ,Cn.CriteriaFlag
          ,Cn.CriteriaDealID
          ,Cn.NumDealsOn
          ,Cn.CALogged
          ,Cn.DealConvPct
          ,Cn.EmailUpdateFlag
          ,NULLIF(Cn.UserEmail,'')          AS UserEmail
          ,Cn.DoNotAddToContactLists         --*/ select *
      FROM rESP.Contacts Cn
      LEFT JOIN x
        ON x.EmailAddress = Cn.EmailAddress;
/*Test
GO
SELECT * FROM rESxStage.Contacts where nullif(PreviousEmployers,'') is not null;
SELECT * FROM rESxStage.Contacts where nullif(otherphone,'') is not null;
SELECT * FROM rESxStage.Contacts where nullif(pager,'') is not null;
SELECT * FROM rESxStage.Contacts where DuplicateOfID is not null;
--*/
﻿CREATE VIEW rESxStage.EastdilOffices
AS
    SELECT EaO.RowNumber
          ,EaO.UpdateDatetime
          ,EaO.OfficeName
          ,EaO.Address1
          ,EaO.Address2
          ,EaO.Address3
          ,EaO.Phone
          ,EaO.Fax
          ,EaO.LookupValue
          ,PhAdGD.ValidationData
          ,PhAdGD.ValidationDatetime
          ,PhAdGD.ValidatedAddress
          ,PhAdGD.Coordinates
          ,PhAdGD.AddressStatus
      FROM dExcelETL.EastdilOffices EaO
      JOIN dExternalETL.PhysicalAddressGeoData PhAdGD
        ON PhAdGD.LookupValue = EaO.LookupValue;
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/27
-- Description: Exposes the only bid data I could find in ESP to ESx.
-- =============================================
CREATE VIEW [rESxStage].[Bid]
AS
SELECT DCn.DealID
      ,DCn.ContactID
      ,DCn.CompanyID
      ,DCn.EmployeeID
      ,DCn.CoverageContactID
      ,NULLIF(DCn.CoverageTeam,'') AS CoverageTeam
      ,CAST(DCnB.BidOffer AS date) AS BidOffer
      ,DCnB.BidOfferAmt
      ,DCnB.BidRound
      ,DCnB.BidRank
      ,NULLIF(DCnB.BidDepositNote,'') AS BidDepositNote
      ,ISNULL(DCnB.BidDebtFinancing,0)  AS BidDebtFinancing
      ,DCnB.BidRate
      ,DCnB.BidSpread
      ,DCnB.BidLoanTermYears
      ,NULLIF(DCnB.BidPrepaymentPenaltyNote,'') AS BidPrepaymentPenaltyNote
      ,DCnB.BidCapRate
      ,DCnB.BidPricePSF
      ,NULLIF(DCnB.BidGeneralNote,'') AS BidGeneralNote
      ,DCnB.BidOriginationFee
      ,NULLIF(DCnB.BidReferenceIndex,'') AS BidReferenceIndex
      ,NULLIF(DCnB.BidLoanType,'') AS BidLoanType
      ,(SELECT MAX(i.LastUpdate) FROM (VALUES (DCnB.LastUpdate),(DCnB.DateAdded),(DCn.LastUpdate),(DCn.DateAdded))i(LastUpdate)) AS LastUpdate
      ,DCn.Status
      ,DCn.MarketingStatusID
      ,NULLIF(NULLIF(DCn.Tier,'---'),'') AS Tier
      ,CAST(DCn.IMSent AS date) AS IMSent
      ,CAST(DCn.CASent AS date) AS CASent
      ,CAST(DCn.CALogged AS date) AS CALogged
      ,DCn.CAUnApproved
      ,CAST(DCn.OMSent AS date) AS OMSent
      ,CAST(DCn.OMDigital AS date) AS OMDigital
      ,CAST(DCn.DIP AS date) AS DIP
      ,DCn.AdditionalInfo
      ,DCn.Tour
      ,DCn.StatusNote
      ,DCn.CategoryID
      ,DCn.WarroomAccess
      ,DCn.Tags
      ,DCn.InsertFlag
      ,DCn.UpdateFlag
      ,DCn.DeleteFlag
      ,DCn.NumComments
  FROM rESP.DealContactBids DCnB
  JOIN rESP.DealContacts DCn
    ON DCn.DealContactID = DCnB.DealContactID;
;
/*Test
GO
SELECT * FROM rESxStage.Bid; --(13K+ rows of dealcontacts have companyID that doesn't match deal company or contact company)
--*/
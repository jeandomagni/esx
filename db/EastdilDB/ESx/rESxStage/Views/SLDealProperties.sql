﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/11
-- Description: Exposes DealProperties from SLTables spreadsheet
-- =============================================
CREATE VIEW [rESxStage].[SLDealProperties] AS
    SELECT *
      FROM dExcelETL.SLDealProperties;
/*Test
GO
select * from rESxStage.SLDealProperties;
--*/
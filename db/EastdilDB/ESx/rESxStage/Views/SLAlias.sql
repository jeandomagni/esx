﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/11
-- Description: Exposes Aliases to canonical company names
-- =============================================
CREATE VIEW [rESxStage].[SLAlias] AS
    SELECT *
      FROM dExcelETL.SLAlias;
/*Test
GO
select * from rESxStage.SLAlias;
--*/
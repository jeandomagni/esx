﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/04
-- Description: Exposes the Excel table content SLCanonicalAliases to ESx
-- =============================================
CREATE VIEW rESxStage.SLCanonicalAliases AS 
    SELECT slc.CanonicalName
          ,slca.AliasName
      FROM dExcelETL.SLCanonicalAliases slca
      JOIN dExcelETL.SLCanonicals slc
        ON slc.CanonicalID = slca.CanonicalID;
/*Test
GO
SELECT * FROM rESxStage.SLCanonicalAliases ORDER BY CanonicalID;
--*/
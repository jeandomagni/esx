﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/11
-- Description: Exposes Tickers for canonical companies from SLTables spreadsheet
-- =============================================
CREATE VIEW [rESxStage].[SLTickers] AS
    SELECT *
      FROM dExcelETL.SLTickers;
/*Test
GO
select * from rESxStage.SLTickers;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/15
-- Description: Exposes certain ESP.dbo.Deal data to ESx.
-- =============================================
CREATE VIEW [rESxStage].[Deal]
AS
    SELECT D.DealID
          ,D.DealCode
          ,D.PreCode
          ,ISNULL(SLD.DealName,D.DealName)      AS DealName
          ,D.ClientCompanyID
          ,D.TransactionTypeID
          ,D.PropertyTypeID
          ,PT.PropertyType
          ,0 AS Active
          ,D.Confidential
          -- Replaced by DealOffice view to support multiple offices per deal.
          --,NULLIF(REPLACE(REPLACE(REPLACE(D.DealOffice,'Washington DC','Washington, DC'),'Washington, D.C.','Washington, DC'),'---',''),'') DealOffice
          ,D.DealLeader
          ,NULLIF(D.DealManager,'')             AS DealManager
          ,D.Closed
          ,D.DateClosed
          ,ISNULL(D.DealYear,YEAR(D.DateClosed))AS DealYear
          ,D.ESTeam
          ,D.TransactionType
          ,CAST(CASE WHEN SLD.DealID IS NULL THEN 0 ELSE 1 END AS bit) IsFromSLDeals
          ,D.DateAdded
          ,D.LastUpdate   -- select *
      FROM rESP.Deals D
      LEFT JOIN dExcelETL.SLDeals SLD
        ON SLD.DealID = D.DealID
      JOIN rESP.PropertyTypes PT
        ON PT.PropertyTypeID = D.PropertyTypeID;
;
/*Test
GO
SELECT * FROM rESxStage.Deal;
--*/
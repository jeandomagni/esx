﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/11
-- Description: Exposes deals from SLTables spreadsheet
-- =============================================
CREATE VIEW [rESxStage].[SLDeals] AS
    SELECT *
      FROM dExcelETL.SLDeals;
/*Test
GO
select * from rESxStage.SLDeals;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/15
-- Description: Exposes certain ESP.dbo.Deal data to ESx.
-- =============================================
CREATE VIEW [rESxStage].[DealEmployee]
AS
    SELECT D.DealID,WPI.WorkingPartyRole,WPI.EmployeeID        -- Get leaders based on initials
          ,CASE WHEN WPI.LastUpdate>D.LastUpdate THEN WPI.LastUpdate ELSE D.LastUpdate END LastUpdate
      FROM rESxStage.Deal D
      JOIN rESP.DealWPInternal WPI
        ON WPI.DealID = D.DealID;
/*
GO
select * from rESxStage.DealMember;
*/
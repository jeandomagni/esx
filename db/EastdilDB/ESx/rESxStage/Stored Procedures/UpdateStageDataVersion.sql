﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/09
-- Description: Allows stage data to be rebuilt only when needed because it's very time consuming.
-- =============================================
CREATE PROCEDURE [rESxStage].[UpdateStageDataVersion] (@Version AS int) AS 
     TRUNCATE TABLE rESxStage.DataVersion;
     INSERT INTO rESxStage.DataVersion
            (DataVersion)
     VALUES (@Version);
/*
exec rESxStage.UpdateStageDataVersion 3;
select * from rESxStage.DataVersion;
--*/
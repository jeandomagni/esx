﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/09
-- Description: Rebuilds the schemas that make up the ESX staged data.
-- =============================================
CREATE PROCEDURE [rESxStage].[RebuildStageData]
AS 
BEGIN
	-- These prepare imported data for import into ESX
	EXEC dExcelETL.RebuildSchemaData;
	EXEC dExternalETL.RebuildSchemaData;
	EXEC dEspStage.RebuildSchemaData;
	--EXEC pESxETL.RebuildSchemaData;

END;
/*Test
go
EXEC rESxStage.RebuildStageData;
*/
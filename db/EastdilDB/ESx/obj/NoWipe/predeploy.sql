﻿ /*
     DO NOT MANUALLY MODIFY THIS SCRIPT.                                                     
                                                                                             
     It is overwritten during build.                                                         
     Content IS based on the Build configuration (NoWipe, WipeSeed, WipeETL, WipeEtlSeed...) 
                                                                                             
     Modify BUILD.DeploymentScripts.sql to effect changes in executable content.             
 */
PRINT 'PreDeployment script starting at'+CAST(GETDATE() AS nvarchar)+' with Configuration = NoWipe';
GO
PRINT 'PreDeployment data table wipe executing'
IF (SELECT OBJECT_ID('Global.DeleteAllSchemaData')) IS NOT NULL
BEGIN
    EXEC Global.DeleteAllSchemaData 'dEspStage';
    EXEC Global.DeleteAllSchemaData 'pEsxETL';
    EXEC Global.DeleteAllSchemaData 'dExternalETL';
    EXEC Global.DeleteAllSchemaData 'dExcelETL';
END
ELSE
BEGIN
    EXEC iESxETL.DeleteAllSchemaData 'rESxStage';
    EXEC iESxETL.DeleteAllSchemaData 'dEspStage';
    EXEC iESxETL.DeleteAllSchemaData 'pExternalETL';
    EXEC iESxETL.DeleteAllSchemaData 'pExcelETL';
    EXEC iESxETL.DeleteAllSchemaData 'pEsxETL';
END
GO
PRINT 'No PreDeployment ESX actions required';
GO
PRINT 'PreDeployment script finished at'+CAST(GETDATE() AS nvarchar);
GO

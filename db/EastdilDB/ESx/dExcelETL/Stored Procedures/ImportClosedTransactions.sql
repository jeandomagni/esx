﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/02/22
-- Description: Moves unprocessed ClosedTransaction in StagedExcelData to AdjustedExcelData for adjustment.
--              Performs needed adjustments then loads the data into ClosedTransaction.
-- =============================================
CREATE PROCEDURE dExcelETL.ImportClosedTransactions AS
BEGIN
    SET NOCOUNT ON;
    /*      -- Use this to retry importing all data.
    --TRUNCATE TABLE dExcelETL.AdjustedExcelData;
    DELETE dExcelETL.AdjustedExcelData            -- SELECT * FROM dExcelETL.AdjustedExcelData
     WHERE StagedExcelData_Key in 
        (SELECT StagedExcelData_Key 
           FROM dExcelETL.StagedExcelData sed
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
          WHERE ess.SheetFormat = 'ClosedTransactions');
    --*/ 

    INSERT INTO dExcelETL.AdjustedExcelData(StagedExcelData_Key,RowFormat,RowData)
    SELECT sed.StagedExcelData_Key,'',sed.RowData       -- SELECT *
      FROM dExcelETL.StagedExcelData sed
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
      LEFT JOIN dExcelETL.AdjustedExcelData aed
        ON aed.StagedExcelData_Key = sed.StagedExcelData_Key
      WHERE ess.SheetFormat = 'ClosedTransactions'
        AND aed.StagedExcelData_Key IS NULL;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to AdjustedExcelData';

    WITH x AS (
        SELECT sed.ExcelSourceSheet_Key
              ,sed.RowNumber
              ,CASE WHEN aed.RowData.value('Row[1]/F1[1]','nvarchar(5)') = '# of'
                    THEN aed.RowData.value('Row[1]/F1[1]','nvarchar(50)')
                    WHEN aed.RowData.value('Row[1]/F2[1]','nvarchar(5)') = '# of'
                    THEN aed.RowData.value('Row[1]/F2[1]','nvarchar(50)')
                    WHEN aed.RowData.value('Row[1]/F3[1]','nvarchar(5)') = '# of'
                    THEN aed.RowData.value('Row[1]/F3[1]','nvarchar(50)')
               END CLASS
          FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         WHERE ess.SheetFormat = 'ClosedTransactions'
           AND aed.RowFormat.exist('/Class[1]') = 0
           AND (   aed.RowData.value('Row[1]/F1[1]','nvarchar(5)') = '# of'
                OR aed.RowData.value('Row[1]/F2[1]','nvarchar(5)') = '# of'
                OR aed.RowData.value('Row[1]/F3[1]','nvarchar(5)') = '# of')
    ), Y AS (
        SELECT x.ExcelSourceSheet_Key,x.RowNumber UpperBound,SUBSTRING(x.CLASS,6,50)CLASS,x2.RowNumber LowerBound
          FROM x
          LEFT JOIN x x2 
            ON x2.ExcelSourceSheet_Key = x.ExcelSourceSheet_Key
           AND x2.RowNumber < x.RowNumber
    ), Z AS (
        SELECT Y.ExcelSourceSheet_Key,Y.UpperBound,SUBSTRING(CLASS,1,LEN(CLASS)-1) CLASS,ISNULL(MAX(LowerBound),1) LowerBound
          FROM Y
         GROUP BY Y.ExcelSourceSheet_Key,Y.UpperBound,CLASS
    ) --/*
    UPDATE aed
       SET RowFormat.modify('insert <Class>{sql:column("Z.CLASS")}</Class> into /')  -- */ select *
      FROM dExcelETL.AdjustedExcelData aed 
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN Z
        ON Z.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
       AND Z.UpperBound >= sed.RowNumber
       AND Z.LowerBound < sed.RowNumber;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' AdjustedExcelData Rows clasified';
    
    UPDATE dExcelETL.AdjustedExcelData			-- Standardize category names.  Some sheets said one thing and some were slightly different
       SET RowFormat.modify('replace value of (Class/text())[1] with "Loan Sales"')
     WHERE RowFormat.value('Class[1]','nvarchar(50)')='Loan Sale Transactions';
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' AdjustedExcelData Rows Standardized';
    
    UPDATE dExcelETL.AdjustedExcelData			-- Standardize category names.  Some sheets said one thing and some were slightly different
       SET RowFormat.modify('replace value of (Class/text())[1] with "Property Sales"')
     WHERE RowFormat.value('Class[1]','nvarchar(50)')='Property Sale Transactions';
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' AdjustedExcelData Rows Standardized';
    
    UPDATE dExcelETL.AdjustedExcelData			-- Standardize category names.  Some sheets said one thing and some were slightly different
       SET RowFormat.modify('replace value of (Class/text())[1] with "Financing Transactions"')
     WHERE RowFormat.value('Class[1]','nvarchar(50)')='Securitization';
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' AdjustedExcelData Rows Standardized';
    
    UPDATE dExcelETL.AdjustedExcelData			-- Standardize category names.  Some sheets said one thing and some were slightly different
       SET RowFormat.modify('replace value of (Class/text())[1] with "Private Equity/JV-Recap"')
     WHERE RowFormat.value('Class[1]','nvarchar(50)')='Debt/Equity Placements';
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' AdjustedExcelData Rows Standardized';
    
    --  							SOME TABS START DATA ON A DIFFERENT COLUMN THAN OTHERS.  
    -- 2001 NEEDS SHIFTING RIGHT 2 COLUMNS
    UPDATE aed
       SET RowFormat.modify('insert <Shift2 /> into /')
          ,RowData = CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
                          REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CAST(aed.RowData as nvarchar(4000))
                    ,'F20>','F22>')
                    ,'F19>','F21>')
                    ,'F18>','F20>')
                    ,'F17>','F19>')
                    ,'F16>','F18>')
                    ,'F15>','F17>')
                    ,'F14>','F16>')
                    ,'F13>','F15>')
                    ,'F12>','F14>')
                    ,'F11>','F13>')
                    ,'F10>','F12>')
                    ,'F9>','F11>')
                    ,'F8>','F10>')
                    ,'F7>','F9>')
                    ,'F6>','F8>')
                    ,'F5>','F7>')
                    ,'F4>','F6>')
                    ,'F3>','F5>')
                    ,'F2>','F4>')
                    ,'F1>','F3>') AS XML)  -- select *
      FROM dExcelETL.AdjustedExcelData aed
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'ClosedTransactions'
       AND RowFormat.exist('/Class[1]') = 1
       AND RowFormat.exist('/Shift2[1]') = 0
       AND SheetName = '''2001$'''
       AND RowNumber > 2;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' AdjustedExcelData Rows Shifted';
    
    --	2003 NEEDS SHIFTING RIGHT 1 COLUMN
    UPDATE aed
       SET RowFormat.modify('insert <Shift1 /> into /')
          ,RowData = CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
                          REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CAST(aed.RowData as nvarchar(4000))
                    ,'F20>','F21>')
                    ,'F19>','F20>')
                    ,'F18>','F19>')
                    ,'F17>','F18>')
                    ,'F16>','F17>')
                    ,'F15>','F16>')
                    ,'F14>','F15>')
                    ,'F13>','F14>')
                    ,'F12>','F13>')
                    ,'F11>','F12>')
                    ,'F10>','F11>')
                    ,'F9>','F10>')
                    ,'F8>','F9>')
                    ,'F7>','F8>')
                    ,'F6>','F7>')
                    ,'F5>','F6>')
                    ,'F4>','F5>')
                    ,'F3>','F4>')
                    ,'F2>','F3>')
                    ,'F1>','F2>') AS XML)
      FROM dExcelETL.AdjustedExcelData aed
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'ClosedTransactions'
       AND RowFormat.exist('/Class[1]') = 1
       AND RowFormat.exist('/Shift1[1]') = 0
       AND SheetName = '''2003$'''
       AND RowNumber > 2;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' AdjustedExcelData Rows Shifted';

    --                                      Stage ClosedTransactions data 

    --TRUNCATE TABLE dExcelETL.ClosedTransaction;       
    -- SELECT * FROM dExcelETL.ClosedTransaction;
    
    WITH X AS (
       SELECT esf.SourceFileName                                AS SourceFileName
             ,ess.SheetName                                     AS SheetName
             ,sed.RowNumber                                     AS RowNumber
             ,aed.RowFormat.value('Class[1]' ,'nvarchar(50)')   AS RowType
             ,aed.RowData.value('Row[1]/F1[1]' ,'nvarchar(50)') AS OfficeName
             ,aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(50)') AS DealCode
             ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(50)') AS ClientNames
             ,aed.RowData.value('Row[1]/F4[1]' ,'nvarchar(50)') AS BuyerLenderNames
             ,aed.RowData.value('Row[1]/F5[1]' ,'nvarchar(50)') AS TransactionType
             ,aed.RowData.value('Row[1]/F6[1]' ,'nvarchar(50)') AS Description
             ,aed.RowData.value('Row[1]/F7[1]' ,'nvarchar(50)') AS AssetType
             ,aed.RowData.value('Row[1]/F8[1]' ,'nvarchar(50)') AS Location
             ,aed.RowData.value('Row[1]/F9[1]' ,'nvarchar(50)') AS SfUnits
             ,aed.RowData.value('Row[1]/F10[1]','nvarchar(50)') AS CloseDate
             ,aed.RowData.value('Row[1]/F11[1]','nvarchar(50)') AS DealAmount
             ,aed.RowData.value('Row[1]/F12[1]','nvarchar(50)') AS ZipCode             -- select *
         FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
        WHERE ess.SheetFormat = 'ClosedTransactions'
          AND aed.RowFormat.exist('/Class[1]')=1
    ), Y AS (
        SELECT X.SourceFileName
              ,X.SheetName
              ,X.RowNumber
              ,X.RowType
              ,X.OfficeName
              ,X.DealCode
              ,X.ClientNames
              ,X.BuyerLenderNames
              ,X.TransactionType
              ,X.Description
              ,X.AssetType
              ,X.Location
              ,X.SfUnits
              ,'01-'+X.CloseDate                                             AS CloseDate
              ,[dExcelETL].FindNumberMaybe(X.DealAmount)                         AS DealAmount
              ,CASE WHEN ISNUMERIC(X.ZipCode)=1 THEN X.ZipCode ELSE NULL END AS ZipCode
          FROM X
         WHERE TransactionType IS NOT NULL
           AND TransactionType NOT IN ('Transaction','Type')
           AND ISNUMERIC(TransactionType)=0
    ), Z as (
        SELECT Y.SourceFileName
              ,Y.SheetName
              ,Y.RowNumber
              ,Y.RowType
              ,Y.OfficeName
              ,Y.DealCode
              ,Y.ClientNames
              ,Y.BuyerLenderNames
              ,Y.TransactionType
              ,Y.Description
              ,Y.AssetType
              ,Y.Location
              ,Y.SfUnits
              ,Y.CloseDate
              ,CASE WHEN ISNUMERIC(Y.DealAmount)=1 THEN Y.DealAmount ELSE NULL END AS DealAmount
              ,Y.ZipCode
          FROM Y
    )
    MERGE INTO dExcelETL.ClosedTransaction AS tgt
    USING Z AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.RowNumber       = tgt.RowNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.RowType         ,'0') != isnull(tgt.RowType          ,'0')
        OR isnull(src.OfficeName      ,'0') != isnull(tgt.OfficeName       ,'0')
        OR isnull(src.DealCode        ,'0') != isnull(tgt.DealCode         ,'0')
        OR isnull(src.ClientNames     ,'0') != isnull(tgt.ClientNames      ,'0')
        OR isnull(src.BuyerLenderNames,'0') != isnull(tgt.BuyerLenderNames ,'0')
        OR isnull(src.TransactionType ,'0') != isnull(tgt.TransactionType  ,'0')
        OR isnull(src.Description     ,'0') != isnull(tgt.Description      ,'0')
        OR isnull(src.AssetType       ,'0') != isnull(tgt.AssetType        ,'0')
        OR isnull(src.Location        ,'0') != isnull(tgt.Location         ,'0')
        OR isnull(src.SfUnits         ,'0') != isnull(tgt.SfUnits          ,'0')
        OR isnull(src.CloseDate       ,'')  != isnull(tgt.CloseDate        ,'')
        OR isnull(src.DealAmount      ,'0') != isnull(tgt.DealAmount       ,'0')
        OR isnull(src.ZipCode         ,'0') != isnull(tgt.ZipCode          ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime   = GETUTCDATE()
         ,tgt.RowType          = src.RowType
         ,tgt.OfficeName       = src.OfficeName
         ,tgt.DealCode         = src.DealCode
         ,tgt.ClientNames      = src.ClientNames
         ,tgt.BuyerLenderNames = src.BuyerLenderNames
         ,tgt.TransactionType  = src.TransactionType
         ,tgt.Description      = src.Description
         ,tgt.AssetType        = src.AssetType
         ,tgt.Location         = src.Location
         ,tgt.SfUnits          = src.SfUnits
         ,tgt.CloseDate        = src.CloseDate
         ,tgt.DealAmount       = src.DealAmount
         ,tgt.ZipCode          = src.ZipCode
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,RowType
                 ,UpdateDatetime
                 ,OfficeName
                 ,DealCode
                 ,ClientNames
                 ,BuyerLenderNames
                 ,TransactionType
                 ,Description
                 ,AssetType
                 ,Location
                 ,SfUnits
                 ,CloseDate
                 ,DealAmount
                 ,ZipCode)
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,src.RowType
                 ,GETUTCDATE()
                 ,src.OfficeName
                 ,src.DealCode
                 ,src.ClientNames
                 ,src.BuyerLenderNames
                 ,src.TransactionType
                 ,src.Description
                 ,src.AssetType
                 ,src.Location
                 ,src.SfUnits
                 ,src.CloseDate
                 ,src.DealAmount
                 ,src.ZipCode);
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into ClosedTransaction';

    --update dExcelETL.ClosedTransaction set DealCode = 'abc' where OfficeName = 'LA';
END
/*Test
GO
EXEC dExcelETL.ImportClosedTransactions;
--*/
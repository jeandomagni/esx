﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/09
-- Description: Cleanses and organizes (mostly Excel) ETL data into tables and views presented in rESxStage.
-- =============================================
CREATE PROCEDURE [dExcelETL].[RebuildSchemaData]
AS 
BEGIN
	EXEC dExcelETL.CategorizeImportedData;
	EXEC dExcelETL.ImportClosedProperties;
	EXEC dExcelETL.ImportClosedTransactions;
	EXEC dExcelETL.ImportFinancingTransactions;
	EXEC dExcelETL.ImportSLProperties;
	EXEC dExcelETL.ImportSLCanonicals;
    EXEC dExcelETL.ImportSLDeals;
    EXEC dExcelETL.ImportSLDealCompanies;
    EXEC dExcelETL.ImportSLDealProperties;
    EXEC dExcelETL.ImportEastdilOffices;
END;
/*Test
go
EXEC dExcelETL.RebuildSchemaData;
*/
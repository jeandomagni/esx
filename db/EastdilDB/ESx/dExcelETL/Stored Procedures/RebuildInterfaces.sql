﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/03/30
-- Description: Deletes all ESXDEV.rESP views and re-creates them based on current ESNetDBTransition.dbo tables.
--              Eventually it may also rebuild the rESxETL interface schema too.
-- =============================================
CREATE PROCEDURE dExcelETL.RebuildInterfaces AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @InterfaceSchema sysname = 'rESP';
    DECLARE @InterfaceSchemaID int = (SELECT schema_id FROM sys.schemas WHERE name = @InterfaceSchema);
    DECLARE @DataSchema sysname = 'dbo';
    DECLARE @DataSchemaID int = (SELECT schema_id FROM ESNetDBTransition.sys.schemas WHERE name = @DataSchema);
    DECLARE @Cmds nvarchar(MAX) = '';
    
    -- build/execute Drop commands
    SELECT @Cmds += 'EXEC ('''+'DROP VIEW '+QUOTENAME(@InterfaceSchema)+'.'+QUOTENAME(name)+';'');'+CHAR(13)+CHAR(10)
      FROM sys.views
     WHERE schema_id = @InterfaceSchemaID
     ORDER BY name;
    
    EXEC (@Cmds);
    SET @Cmds = '';

    SELECT @Cmds += 'EXEC ('''+'CREATE VIEW '+QUOTENAME(@InterfaceSchema)+'.'+QUOTENAME(name)
            +' AS SELECT * FROM [ESNetDBTransition].'+QUOTENAME(@DataSchema)+'.'+QUOTENAME(name)+';'');'+CHAR(13)+CHAR(10)
      FROM ESNetDBTransition.sys.tables 
     WHERE schema_id = @DataSchemaID
     ORDER BY name;

    EXEC (@Cmds);
END
/*Test
GO
EXEC dExcelETL.RebuildInterfaces;
*/
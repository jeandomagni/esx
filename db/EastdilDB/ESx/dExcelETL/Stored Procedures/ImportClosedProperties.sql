﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/02/22
-- Description: Moves unprocessed ClosedTransaction in StagedExcelData to AdjustedExcelData for adjustment.
--              Performs needed adjustments then loads the data into ClosedProperty.
-- =============================================
CREATE PROCEDURE dExcelETL.ImportClosedProperties AS
BEGIN
    SET NOCOUNT ON;
    /*      -- Use this to retry importing all data.
    TRUNCATE TABLE dExcelETL.AdjustedExcelData;
    DELETE dExcelETL.AdjustedExcelData            -- SELECT * FROM dExcelETL.AdjustedExcelData
     WHERE StagedExcelData_Key in 
        (SELECT StagedExcelData_Key 
           FROM dExcelETL.StagedExcelData sed
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
          WHERE ess.SheetFormat = 'ClosedProperties');
    --*/ 

    INSERT INTO dExcelETL.AdjustedExcelData(StagedExcelData_Key,RowFormat,RowData)
    SELECT sed.StagedExcelData_Key,'',sed.RowData       -- SELECT *
      FROM dExcelETL.StagedExcelData sed
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
      LEFT JOIN dExcelETL.AdjustedExcelData aed
        ON aed.StagedExcelData_Key = sed.StagedExcelData_Key
      WHERE ess.SheetFormat = 'ClosedProperties'
        AND aed.StagedExcelData_Key IS NULL;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to AdjustedExcelData';

    WITH X AS (
        SELECT sed.ExcelSourceSheet_Key,ess.SheetName,MIN(sed.RowNumber) UpperBound
          FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
        WHERE ess.SheetFormat = 'ClosedProperties'
          AND RowNumber > 2
          AND(    aed.RowData.exist('Row[1]/F3[1]') = 0
           OR(    aed.RowData.exist('Row[1]/F4[1]') = 0
              AND aed.RowData.exist('Row[1]/F5[1]') = 0
              AND aed.RowData.exist('Row[1]/F6[1]') = 0))
        GROUP BY sed.ExcelSourceSheet_Key,ess.SheetName
    ) --/*
    UPDATE aed
       SET RowFormat.modify('insert if (sql:column("X.UpperBound") <= sql:column("sed.RowNumber")) then <Detail /> else <Base /> into /')
           -- */ select *,CASE WHEN X.UpperBound <= sed.RowNumber THEN 'UPPER' ELSE 'LOWER' END,DATALENGTH(RowFormat)
      FROM dExcelETL.AdjustedExcelData aed 
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN X
        ON X.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE aed.RowFormat.exist('/Detail')=0
       AND aed.RowFormat.exist('/Base')=0;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows in AdjustedExcelData given a RowFormat';
    
    
    UPDATE aed				--	The Deal-Code column is missing from these sheets so the data needs shifting.
       SET RowFormat.modify('insert <DealCodeShift /> into /')
          ,RowData = CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
                          REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CAST(aed.RowData as nvarchar(4000))
                    ,'F21>','F22>')
                    ,'F20>','F21>')
                    ,'F19>','F20>')
                    ,'F18>','F19>')
                    ,'F17>','F18>')
                    ,'F16>','F17>')
                    ,'F15>','F16>')
                    ,'F14>','F15>')
                    ,'F13>','F14>')
                    ,'F12>','F13>')
                    ,'F11>','F12>')
                    ,'F10>','F11>')
                    ,'F9>' ,'F10>')
                    ,'F8>' ,'F9>' )
                    ,'F7>' ,'F8>' )
                    ,'F6>' ,'F7>' )
                    ,'F5>' ,'F6>' )
                    ,'F4>' ,'F5>' ) AS XML) -- select * 
      FROM dExcelETL.AdjustedExcelData aed
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'ClosedProperties'
       AND RowFormat.exist('/DealCodeShift[1]') = 0
       AND SheetName in ('''SCC 2002$''','''SCC 2003$''','''SCC 2004$''')
       AND RowNumber > 2;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows in AdjustedExcelData shifted for missing DealCode';
    
    UPDATE aed				--	The ES-Team column is missing from these sheets so the data needs shifting.
       SET RowFormat.modify('insert <EsTeamShift /> into /')
          ,RowData = CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
                          REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CAST(aed.RowData as nvarchar(4000))
                    ,'F22>','F23>')
                    ,'F21>','F22>')
                    ,'F20>','F21>')
                    ,'F19>','F20>')
                    ,'F18>','F19>')
                    ,'F17>','F18>')
                    ,'F16>','F17>')
                    ,'F15>','F16>')
                    ,'F14>','F15>')
                    ,'F13>','F14>')
                    ,'F12>','F13>')
                    ,'F11>','F12>')
                    ,'F10>','F11>')
                    ,'F9>' ,'F10>')
                    ,'F8>' ,'F9>' )
                    ,'F7>' ,'F8>' )
                    ,'F6>' ,'F7>' )
                    ,'F5>' ,'F6>' ) AS XML) -- select * 
      FROM dExcelETL.AdjustedExcelData aed
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'ClosedProperties'
       AND RowFormat.exist('/EsTeamShift[1]') = 0
       AND SheetName in ('''SCC 2002$''','''SCC 2003$''','''SCC 2004$''','''SCC 2005$''','''ES 2006$''')
       AND RowNumber > 2;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows in AdjustedExcelData shifted for missing ES Team column';
    
    UPDATE aed				--	There is an extra/hidden column that needs removing
       SET RowFormat.modify('insert <R17Delete /> into /')
          ,RowData.modify('delete /Row/F17') -- select *
      FROM dExcelETL.AdjustedExcelData aed
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'ClosedProperties'
       AND RowFormat.exist('/R17Delete[1]') = 0
       AND SheetName in ('''SCC 2002$''','''SCC 2003$''','''SCC 2004$''')
       AND RowNumber > 2;
    
    UPDATE aed				-- Now the removed column needs data shifted into it
       SET RowFormat.modify('insert <R17Shift /> into /')
          ,RowData = CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CAST(aed.RowData as nvarchar(4000))
                    ,'F18>','F17>') 
                    ,'F19>','F18>')
                    ,'F20>','F19>')
                    ,'F21>','F20>')
                    ,'F22>','F21>')
                    ,'F23>','F22>') AS XML) -- select * 
      FROM dExcelETL.AdjustedExcelData aed
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'ClosedProperties'
       AND RowFormat.exist('/R17Shift[1]') = 0
       AND SheetName in ('''SCC 2002$''','''SCC 2003$''','''SCC 2004$''')
       AND RowNumber > 2;
    
    UPDATE aed				--	The Comments and Address columns are (almost) reversed in these sheets.  Fix it.
       SET RowFormat.modify('insert <CommentShift /> into /')
          ,RowData = CAST(REPLACE(REPLACE(REPLACE(REPLACE(CAST(aed.RowData as nvarchar(4000))
                    ,'F22>','F23>')
                    ,'F21>','F22>')
                    ,'F20>','F21>')
                    ,'F23>','F20>') AS XML) -- select * 
      FROM dExcelETL.AdjustedExcelData aed
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'ClosedProperties'
       AND RowFormat.exist('/CommentShift[1]') = 0
       AND SheetName in ('''SCC 2002$''','''SCC 2003$''','''SCC 2004$''','''SCC 2005$''','''ES 2006$''')
       AND RowNumber > 2;

    --                                      Stage ClosedProperties data 

    --TRUNCATE TABLE dExcelETL.ClosedProperty;       
    -- SELECT * FROM dExcelETL.ClosedProperty;
    
    WITH X AS (
       SELECT esf.SourceFileName
             ,ess.SheetName
             ,sed.RowNumber
             ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(50)') ClosingDate
             ,aed.RowData.value('Row[1]/F4[1]' ,'nvarchar(50)') DealCode
             ,aed.RowData.value('Row[1]/F5[1]' ,'nvarchar(50)') EsTeam
             ,aed.RowData.value('Row[1]/F6[1]' ,'nvarchar(50)') Seller
             ,aed.RowData.value('Row[1]/F7[1]' ,'nvarchar(50)') Buyer
             ,aed.RowData.value('Row[1]/F8[1]' ,'nvarchar(100)')AssetName
             ,aed.RowData.value('Row[1]/F9[1]' ,'nvarchar(50)') AssetType
             ,aed.RowData.value('Row[1]/F10[1]','nvarchar(50)') Location
             ,aed.RowData.value('Row[1]/F11[1]','nvarchar(50)') PctLeased
             ,aed.RowData.value('Row[1]/F12[1]','nvarchar(50)') SfUnits
             ,aed.RowData.value('Row[1]/F13[1]','nvarchar(50)') SalePrice
             ,aed.RowData.value('Row[1]/F14[1]','nvarchar(50)') IgnorePricePerUnit
             ,aed.RowData.value('Row[1]/F15[1]','nvarchar(50)') InPlaceCap
             ,aed.RowData.value('Row[1]/F16[1]','nvarchar(50)') Yr1Cap
             ,aed.RowData.value('Row[1]/F17[1]','nvarchar(50)') Irr10Yr
             ,aed.RowData.value('Row[1]/F18[1]','nvarchar(50)') Coc5Yr
             ,aed.RowData.value('Row[1]/F19[1]','nvarchar(50)') NumOffers
             ,aed.RowData.value('Row[1]/F20[1]','nvarchar(500)')Comment
             ,aed.RowData.value('Row[1]/F21[1]','nvarchar(200)')Address  -- Select *
         FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
        WHERE ess.SheetFormat = 'ClosedProperties'
          AND RowFormat.exist('Base')=1 
    ),Y AS (
        SELECT X.SourceFileName
              ,X.SheetName
              ,X.RowNumber
              ,'01-'+X.ClosingDate                                                  AS ClosingDate
              ,X.DealCode
              ,X.EsTeam
              ,X.Seller
              ,X.Buyer
              ,X.AssetName
              ,X.AssetType
              ,X.Location
              ,[dExcelETL].FindNumberMaybe(X.PctLeased)                                 AS PctLeased
              ,[dExcelETL].FindNumberMaybe(X.SfUnits)                                   AS SfUnits
              ,REPLACE(REPLACE(SUBSTRING(X.SalePrice,1,10),'$',''),',','')          AS SalePrice
              ,REPLACE(REPLACE(SUBSTRING(X.IgnorePricePerUnit,1,50),'$',''),',','') AS IgnorePricePerUnit
              ,[dExcelETL].FindNumberMaybe(X.InPlaceCap)                                AS InPlaceCap
              ,[dExcelETL].FindNumberMaybe(X.Yr1Cap)                                    AS Yr1Cap
              ,[dExcelETL].FindNumberMaybe(X.Irr10Yr)                                   AS Irr10Yr
              ,[dExcelETL].FindNumberMaybe(X.Coc5Yr)                                    AS Coc5Yr
              ,X.NumOffers
              ,X.Comment
              ,X.Address
          FROM X
    ), Z AS (
        SELECT Y.SourceFileName
              ,Y.SheetName
              ,Y.RowNumber
              ,CAST(Y.ClosingDate AS DATE)                                          AS ClosingDate 
              ,Y.DealCode
              ,Y.EsTeam
              ,Y.Seller
              ,Y.Buyer
              ,Y.AssetName
              ,Y.AssetType
              ,Y.Location
              ,CASE WHEN ISNUMERIC(Y.PctLeased)=1  THEN Y.PctLeased  ELSE NULL END  AS PctLeased
              ,CASE WHEN ISNUMERIC(Y.SfUnits)=1    THEN Y.SfUnits    ELSE NULL END  AS SfUnits
              ,Y.SalePrice
              ,CASE WHEN ISNUMERIC(Y.InPlaceCap)=1 THEN Y.InPlaceCap ELSE NULL END  AS InPlaceCap
              ,CASE WHEN ISNUMERIC(Y.Yr1Cap)=1     THEN Y.Yr1Cap     ELSE NULL END  AS Yr1Cap
              ,CASE WHEN ISNUMERIC(Y.Irr10Yr)=1    THEN Y.Irr10Yr    ELSE NULL END  AS Irr10Yr
              ,CASE WHEN ISNUMERIC(Y.Coc5Yr)=1     THEN Y.Coc5Yr     ELSE NULL END  AS Coc5Yr
              ,CASE WHEN Y.NumOffers NOT LIKE 'NOT%' AND Y.NumOffers != 'N/A' 
                                                   THEN Y.NumOffers  ELSE NULL END  AS NumOffers
              ,Y.Comment
              ,Y.Address
          FROM Y
         WHERE ISDATE(ClosingDate) = 1
    ) --/*
    MERGE INTO dExcelETL.ClosedProperty AS tgt
    USING Z AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.RowNumber       = tgt.RowNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.ClosingDate,'')  != isnull(tgt.ClosingDate   ,'') 
        OR isnull(src.DealCode   ,'0') != isnull(tgt.DealCode      ,'0')
        OR isnull(src.EsTeam     ,'0') != isnull(tgt.EsTeam        ,'0')
        OR isnull(src.Seller     ,'0') != isnull(tgt.Seller        ,'0')
        OR isnull(src.Buyer      ,'0') != isnull(tgt.Buyer         ,'0')
        OR isnull(src.AssetName  ,'0') != isnull(tgt.AssetName     ,'0')
        OR isnull(src.AssetType  ,'0') != isnull(tgt.AssetType     ,'0')
        OR isnull(src.Location   ,'0') != isnull(tgt.Location      ,'0')
        OR isnull(src.PctLeased  ,'0') != isnull(tgt.Occupancy     ,'0')
        OR isnull(src.SfUnits    ,'0') != isnull(tgt.SfUnits       ,'0')
        OR isnull(src.SalePrice  ,'0') != isnull(tgt.SalePrice     ,'0')
        OR isnull(src.InPlaceCap ,'0') != isnull(tgt.CapRateInPlace,'0')
        OR isnull(src.Yr1Cap     ,'0') != isnull(tgt.CapRate1Year  ,'0')
        OR isnull(src.Irr10Yr    ,'0') != isnull(tgt.Irr10Year     ,'0')
        OR isnull(src.Coc5Yr     ,'0') != isnull(tgt.Coc5Year      ,'0')
        OR isnull(src.NumOffers  ,'0') != isnull(tgt.NumOffers     ,'0')
        OR isnull(src.Comment    ,'0') != isnull(tgt.Comments      ,'0')
        OR isnull(src.Address    ,'0') != isnull(tgt.Address       ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime = GETUTCDATE()
         ,tgt.ClosingDate    = src.ClosingDate  
         ,tgt.DealCode       = src.DealCode     
         ,tgt.EsTeam         = src.EsTeam       
         ,tgt.Seller         = src.Seller       
         ,tgt.Buyer          = src.Buyer        
         ,tgt.AssetName      = src.AssetName 
         ,tgt.AssetType      = src.AssetType 
         ,tgt.Location       = src.Location     
         ,tgt.Occupancy      = src.PctLeased    
         ,tgt.SfUnits        = src.SfUnits      
         ,tgt.SalePrice      = src.SalePrice    
         ,tgt.CapRateInPlace = src.InPlaceCap   
         ,tgt.CapRate1Year   = src.Yr1Cap       
         ,tgt.Irr10Year      = src.Irr10Yr      
         ,tgt.Coc5Year       = src.Coc5Yr       
         ,tgt.NumOffers      = src.NumOffers    
         ,tgt.Comments       = src.Comment      
         ,tgt.Address        = src.Address      
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime        
                 ,ClosingDate   
                 ,DealCode      
                 ,EsTeam        
                 ,Seller        
                 ,Buyer         
                 ,AssetName  
                 ,AssetType  
                 ,Location      
                 ,Occupancy     
                 ,SfUnits       
                 ,SalePrice     
                 ,CapRateInPlace
                 ,CapRate1Year  
                 ,Irr10Year     
                 ,Coc5Year      
                 ,NumOffers     
                 ,Comments      
                 ,Address)
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,GETUTCDATE()
                 ,src.ClosingDate 
                 ,src.DealCode    
                 ,src.EsTeam      
                 ,src.Seller      
                 ,src.Buyer       
                 ,src.AssetName
                 ,src.AssetType
                 ,src.Location    
                 ,src.PctLeased   
                 ,src.SfUnits     
                 ,src.SalePrice   
                 ,src.InPlaceCap  
                 ,src.Yr1Cap      
                 ,src.Irr10Yr     
                 ,src.Coc5Yr      
                 ,src.NumOffers   
                 ,src.Comment     
                 ,src.Address);
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into ClosedProperty';

    --update dExcelETL.ClosedProperty set DealCode = 'abc' where DealCode like 'E%';

    SELECT ess.ExcelSourceFile_Key,ess.SheetName,sed.RowNumber,0 UpdateDatetime
		 ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(100)') PortfolioName
    INTO #ClosedPropertyPortfolios
     FROM dExcelETL.AdjustedExcelData aed
     JOIN dExcelETL.StagedExcelData sed
       ON sed.StagedExcelData_Key = aed.StagedExcelData_Key
     JOIN dExcelETL.ExcelSourceSheet ess
       ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
    WHERE ess.SheetFormat = 'ClosedProperties'
      AND (aed.RowData.exist('Row[1]/F3[1]') =1 AND aed.RowData.exist('Row[1]/F4[1]') =0 AND aed.RowData.exist('Row[1]/F6[1]') =0)
      AND RowFormat.exist('/Base')=0;           --SELECT * FROM #ClosedPropertyPortfolios;
       
    -- truncate table dExcelETL.ClosedPropertyPartition;
    WITH X AS (
        SELECT esf.SourceFileName
              ,cpp.SheetName
              ,cpp.RowNumber
              ,cpp.PortfolioName
              ,ROW_NUMBER() OVER(ORDER BY cpp.ExcelSourceFile_Key,cpp.SheetName,cpp.RowNumber) SeqNum
          FROM #ClosedPropertyPortfolios cpp
          JOIN dExcelETL.ExcelSourceFile esf
            ON esf.ExcelSourceFile_Key = cpp.ExcelSourceFile_Key
    ),Y AS (
        SELECT X.SourceFileName
              ,X.SheetName
              ,X.RowNumber
              ,CASE WHEN X.RowNumber+1=Next1.RowNumber THEN NULL 
                    WHEN X.RowNumber-1=Prev1.RowNumber THEN Prev1.PortfolioName+' '+X.PortfolioName 
                    ELSE X.PortfolioName END             AS PortfolioName
              ,X.RowNumber+1                             AS DataStartRow
              ,ISNULL(Next1.RowNumber-1,100000)          AS DataEndRow
          FROM X X
          LEFT JOIN X Prev1
            ON Prev1.SourceFileName = X.SourceFileName
           AND Prev1.SheetName      = X.SheetName
           AND Prev1.SeqNum         = X.SeqNum-1
          LEFT JOIN X Next1
            ON Next1.SourceFileName = X.SourceFileName
           AND Next1.SheetName      = X.SheetName
           AND Next1.SeqNum         = X.SeqNum+1
    ),Z AS (
        SELECT *
          FROM Y
         WHERE Y.PortfolioName IS NOT null
    ) --SELECT * FROM z
    MERGE INTO dExcelETL.ClosedPropertyPartition AS tgt
    USING Z AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.RowNumber       = tgt.RowNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.PortfolioName,'0') != isnull(tgt.PortfolioName,'0')
       OR  isnull(src.DataStartRow ,'0') != isnull(tgt.DataStartRow ,'0')
       OR  isnull(src.DataEndRow   ,'0') != isnull(tgt.DataEndRow   ,'0'))
     THEN 
    UPDATE SET
           tgt.UpdateDatetime  = GETUTCDATE()
          ,tgt.PortfolioName   = src.PortfolioName
          ,tgt.DataStartRow    = src.DataStartRow             
          ,tgt.DataEndRow      = src.DataEndRow
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime
                 ,PortfolioName
                 ,DataStartRow
                 ,DataEndRow)
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,GETUTCDATE()
                 ,src.PortfolioName
                 ,src.DataStartRow
                 ,src.DataEndRow);             -- select * from dExcelETL.ClosedPropertyPartition;
    
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into ClosedPropertyPartition';
    DROP TABLE #ClosedPropertyPortfolios;

    -- truncate table dExcelETL.ClosedPropertyDetail
    WITH X AS (
       SELECT Pa.ClosedPropertyPartition_Key
             ,sed.RowNumber - Pa.RowNumber                      AS EntryNumber      
             ,aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(50)') AS PreTable
             ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(50)') AS ClosingDate
             ,aed.RowData.value('Row[1]/F4[1]' ,'nvarchar(50)') AS DealCode
             ,aed.RowData.value('Row[1]/F5[1]' ,'nvarchar(50)') AS EsTeam
             ,aed.RowData.value('Row[1]/F6[1]' ,'nvarchar(50)') AS Seller
             ,aed.RowData.value('Row[1]/F7[1]' ,'nvarchar(50)') AS Buyer
             ,aed.RowData.value('Row[1]/F8[1]' ,'nvarchar(100)')AS AssetName
             ,aed.RowData.value('Row[1]/F9[1]' ,'nvarchar(50)') AS AssetType
             ,aed.RowData.value('Row[1]/F10[1]','nvarchar(100)')AS Location
             ,aed.RowData.value('Row[1]/F11[1]','nvarchar(50)') AS PctLeased
             ,aed.RowData.value('Row[1]/F12[1]','nvarchar(50)') AS SfUnits
             ,aed.RowData.value('Row[1]/F13[1]','nvarchar(50)') AS SalePrice
             ,aed.RowData.value('Row[1]/F14[1]','nvarchar(50)') AS IgnorePricePerUnit
             ,aed.RowData.value('Row[1]/F15[1]','nvarchar(50)') AS InPlaceCap
             ,aed.RowData.value('Row[1]/F16[1]','nvarchar(50)') AS Yr1Cap
             ,aed.RowData.value('Row[1]/F17[1]','nvarchar(50)') AS Irr10Yr
             ,aed.RowData.value('Row[1]/F18[1]','nvarchar(50)') AS Coc5Yr
             ,aed.RowData.value('Row[1]/F19[1]','nvarchar(50)') AS NumOffers
             ,aed.RowData.value('Row[1]/F20[1]','nvarchar(500)')AS Comment
             ,aed.RowData.value('Row[1]/F21[1]','nvarchar(200)')AS Address  -- Select *
         FROM dExcelETL.ClosedPropertyPartition Pa
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.SourceFileName = Pa.SourceFileName
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceFile_Key = esf.ExcelSourceFile_Key
          AND ess.SheetName = Pa.SheetName
         JOIN dExcelETL.StagedExcelData sed
           ON sed.ExcelSourceSheet_Key = ess.ExcelSourceSheet_Key
          AND sed.RowNumber BETWEEN Pa.DataStartRow AND Pa.DataEndRow
         JOIN dExcelETL.AdjustedExcelData aed
           ON aed.StagedExcelData_Key = sed.StagedExcelData_Key
    ),Y AS (
        SELECT X.ClosedPropertyPartition_Key
              ,X.EntryNumber
              ,X.PreTable
              ,'01-'+X.ClosingDate                                                  AS ClosingDate
              ,X.DealCode
              ,X.EsTeam
              ,X.Seller
              ,X.Buyer
              ,X.AssetName
              ,X.AssetType
              ,X.Location
              ,X.PctLeased                                                          AS Location2
              ,[dExcelETL].FindNumberMaybe(X.PctLeased)                                 AS PctLeased
              ,[dExcelETL].FindNumberMaybe(X.SfUnits)                                   AS SfUnits
              ,REPLACE(REPLACE(SUBSTRING(X.SalePrice,1,10),'$',''),',','')          AS SalePrice
              ,REPLACE(REPLACE(SUBSTRING(X.IgnorePricePerUnit,1,50),'$',''),',','') AS IgnorePricePerUnit
              ,[dExcelETL].FindNumberMaybe(X.InPlaceCap)                                AS InPlaceCap
              ,[dExcelETL].FindNumberMaybe(X.Yr1Cap)                                    AS Yr1Cap
              ,[dExcelETL].FindNumberMaybe(X.Irr10Yr)                                   AS Irr10Yr
              ,[dExcelETL].FindNumberMaybe(X.Coc5Yr)                                    AS Coc5Yr
              ,X.NumOffers
              ,X.Comment
              ,X.Address
          FROM X
    ), Z AS (
        SELECT Y.ClosedPropertyPartition_Key
              ,Y.EntryNumber
              ,Y.PreTable
              ,CASE WHEN ISDATE(Y.ClosingDate)=1  THEN Y.ClosingDate ELSE NULL END  AS ClosingDate 
              ,Y.DealCode
              ,Y.EsTeam
              ,Y.Seller
              ,Y.Buyer
              ,Y.AssetName
              ,Y.AssetType
              ,Y.Location
              ,CASE WHEN ISNUMERIC(Y.PctLeased)=0  THEN Y.Location2  ELSE NULL END  AS Location2
              ,CASE WHEN ISNUMERIC(Y.PctLeased)=1  THEN Y.PctLeased  ELSE NULL END  AS PctLeased
              ,CASE WHEN ISNUMERIC(Y.SfUnits)=1    THEN Y.SfUnits    ELSE NULL END  AS SfUnits
              ,CASE WHEN ISNUMERIC(Y.SalePrice)=1  THEN Y.SalePrice  ELSE NULL END  AS SalePrice
              ,CASE WHEN ISNUMERIC(Y.InPlaceCap)=1 THEN Y.InPlaceCap ELSE NULL END  AS InPlaceCap
              ,CASE WHEN ISNUMERIC(Y.Yr1Cap)=1     THEN Y.Yr1Cap     ELSE NULL END  AS Yr1Cap
              ,CASE WHEN ISNUMERIC(Y.Irr10Yr)=1    THEN Y.Irr10Yr    ELSE NULL END  AS Irr10Yr
              ,CASE WHEN ISNUMERIC(Y.Coc5Yr)=1     THEN Y.Coc5Yr     ELSE NULL END  AS Coc5Yr
              ,CASE WHEN Y.NumOffers NOT LIKE 'NOT%' AND Y.NumOffers != 'N/A' 
                                                   THEN Y.NumOffers  ELSE NULL END  AS NumOffers
              ,Y.Comment
              ,Y.Address
          FROM Y
    ) -- SELECT Z.InPlaceCap,Z.Irr10Yr FROM Z WHERE Z.Irr10Yr > CAST(100 AS real)
    MERGE INTO dExcelETL.ClosedPropertyDetail AS tgt
    USING Z AS src
       ON src.ClosedPropertyPartition_Key = tgt.ClosedPropertyPartition_Key
      AND src.EntryNumber                 = tgt.EntryNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED
      AND (isnull(src.PreTable   ,'0')  != isnull(tgt.PreTable      ,'0')
        OR isnull(src.ClosingDate,'')   != isnull(tgt.ClosingDate   ,'' )
        OR isnull(src.DealCode   ,'0')  != isnull(tgt.DealCode      ,'0')
        OR isnull(src.EsTeam     ,'0')  != isnull(tgt.EsTeam        ,'0')
        OR isnull(src.Seller     ,'0')  != isnull(tgt.Seller        ,'0')
        OR isnull(src.Buyer      ,'0')  != isnull(tgt.Buyer         ,'0')
        OR isnull(src.AssetName  ,'0')  != isnull(tgt.AssetName     ,'0')
        OR isnull(src.AssetType  ,'0')  != isnull(tgt.AssetType     ,'0')
        OR isnull(src.Location   ,'0')  != isnull(tgt.Location      ,'0')
        OR isnull(src.Location2  ,'0')  != isnull(tgt.Location2     ,'0')
        OR isnull(src.PctLeased  ,'0')  != isnull(tgt.Occupancy     ,'0')
        OR isnull(src.SfUnits    ,'0')  != isnull(tgt.SfUnits       ,'0')
        OR isnull(src.SalePrice  ,'0')  != isnull(tgt.SalePrice     ,'0')
        OR isnull(src.InPlaceCap ,'0')  != isnull(tgt.CapRateInPlace,'0')
        OR isnull(src.Yr1Cap     ,'0')  != isnull(tgt.CapRate1Year  ,'0')
        OR isnull(src.Irr10Yr    ,'0')  != isnull(tgt.Irr10Year     ,'0')
        OR isnull(src.Coc5Yr     ,'0')  != isnull(tgt.Coc5Year      ,'0')
        OR isnull(src.NumOffers  ,'0')  != isnull(tgt.NumOffers     ,'0')
        OR isnull(src.Comment    ,'0')  != isnull(tgt.Comment       ,'0')
        OR isnull(src.Address    ,'0')  != isnull(tgt.Address       ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime = GETUTCDATE()
         ,tgt.PreTable       = src.PreTable       
         ,tgt.ClosingDate    = src.ClosingDate
         ,tgt.DealCode       = src.DealCode 
         ,tgt.EsTeam         = src.EsTeam 
         ,tgt.Seller         = src.Seller 
         ,tgt.Buyer          = src.Buyer 
         ,tgt.AssetName      = src.AssetName 
         ,tgt.AssetType      = src.AssetType 
         ,tgt.Location       = src.Location 
         ,tgt.Location2      = src.Location2
         ,tgt.Occupancy      = src.PctLeased 
         ,tgt.SfUnits        = src.SfUnits 
         ,tgt.SalePrice      = src.SalePrice 
         ,tgt.CapRateInPlace = src.InPlaceCap 
         ,tgt.CapRate1Year   = src.Yr1Cap 
         ,tgt.Irr10Year      = src.Irr10Yr 
         ,tgt.Coc5Year       = src.Coc5Yr 
         ,tgt.NumOffers      = src.NumOffers 
         ,tgt.Comment        = src.Comment 
         ,tgt.Address        = src.Address
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (ClosedPropertyPartition_Key
                 ,EntryNumber
                 ,UpdateDatetime        
                 ,PreTable 
                 ,ClosingDate 
                 ,DealCode 
                 ,EsTeam 
                 ,Seller 
                 ,Buyer 
                 ,AssetName 
                 ,AssetType 
                 ,Location 
                 ,Location2
                 ,Occupancy
                 ,SfUnits 
                 ,SalePrice 
                 ,CapRateInPlace
                 ,CapRate1Year 
                 ,Irr10Year 
                 ,Coc5Year
                 ,NumOffers 
                 ,Comment
                 ,Address )
          VALUES (src.ClosedPropertyPartition_Key
                 ,src.EntryNumber
                 ,GETUTCDATE()
                 ,src.PreTable   
                 ,src.ClosingDate
                 ,src.DealCode 
                 ,src.EsTeam 
                 ,src.Seller 
                 ,src.Buyer 
                 ,src.AssetName 
                 ,src.AssetType 
                 ,src.Location 
                 ,src.Location2
                 ,src.PctLeased 
                 ,src.SfUnits 
                 ,src.SalePrice 
                 ,src.InPlaceCap 
                 ,src.Yr1Cap 
                 ,src.Irr10Yr 
                 ,src.Coc5Yr 
                 ,src.NumOffers 
                 ,src.Comment 
                 ,src.Address );
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into ClosedPropertyDetail';
    --SELECT * FROM dExcelETL.ClosedPropertyDetail where Coc5Year < 1;
END
/*Test
GO
EXEC dExcelETL.ImportClosedProperties;
--*/
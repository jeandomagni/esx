﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/18
-- Description: Moves SLDealProps rows in StagedExcelData to AdjustedExcelData for adjustment.
--              Performs needed adjustments then loads the results into SLDealProperties.
-- =============================================
CREATE PROCEDURE [dExcelETL].[ImportSLDealProperties] AS
BEGIN
    SET NOCOUNT ON;
    /*      -- Use this to retry importing all data.
    --TRUNCATE TABLE dExcelETL.AdjustedExcelData;
    DELETE dExcelETL.AdjustedExcelData            -- SELECT * FROM dExcelETL.AdjustedExcelData
     WHERE StagedExcelData_Key in 
        (SELECT StagedExcelData_Key             -- select distinct ess.SheetFormat
           FROM dExcelETL.StagedExcelData sed
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
          WHERE ess.SheetFormat = 'SLDealProps');
    --*/ 

    -- Populate the adjusted data table
    INSERT INTO dExcelETL.AdjustedExcelData(StagedExcelData_Key,RowFormat,RowData)
    SELECT sed.StagedExcelData_Key,'',sed.RowData       -- SELECT *
      FROM dExcelETL.StagedExcelData sed
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
      LEFT JOIN dExcelETL.AdjustedExcelData aed
        ON aed.StagedExcelData_Key = sed.StagedExcelData_Key
      WHERE ess.SheetFormat = 'SLDealProps'
        AND aed.StagedExcelData_Key IS NULL;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' SLDealProps rows inserted to AdjustedExcelData';

    -- Mark duplicates to be ignored
    WITH x AS (
        SELECT ess.ExcelSourceSheet_Key
              ,aed.AdjustedExcelData_Key
              ,aed.RowData.value('Row[1]/F1[1]' ,'nvarchar(10)') AS DealID
              ,aed.RowData.value('Row[1]/F4[1]' ,'nvarchar(10)') AS PropertyID
          FROM dExcelETL.AdjustedExcelData aed 
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         WHERE ess.SheetFormat = 'SLDealProps'
           AND aed.RowFormat.exist('/*')=0
    ), y AS (
        SELECT MIN(x.AdjustedExcelData_Key) AdjustedExcelData_Key
              ,x.ExcelSourceSheet_Key
              ,x.DealID
              ,x.PropertyID
          FROM x
         GROUP BY x.ExcelSourceSheet_Key
                 ,x.DealID
                 ,x.PropertyID
        HAVING COUNT(*)>1
    ), z AS (
        SELECT x.AdjustedExcelData_Key
          FROM y
          JOIN x
            ON x.ExcelSourceSheet_Key = y.ExcelSourceSheet_Key
           AND x.DealID = y.DealID
           AND x.PropertyID = y.PropertyID
           AND x.AdjustedExcelData_Key <> y.AdjustedExcelData_Key
    )--/* SELECT * FROM z;  
    UPDATE aed
       SET RowFormat.modify('insert <Duplicate /> into /')    --*/ select *
      FROM dExcelETL.AdjustedExcelData aed
     WHERE aed.AdjustedExcelData_Key IN (SELECT * FROM z);
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' SLDealProps rows flagged as Duplicate';


    -- Set the row format for valid rows without one.
    UPDATE aed
       SET RowFormat.modify('insert <Deal /> into /')    -- select *
      FROM dExcelETL.AdjustedExcelData aed 
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'SLDealProps'
       AND aed.RowFormat.exist('/*')=0
       AND ISNUMERIC(aed.RowData.value('Row[1]/F1[1]','nvarchar(10)')) = 1
       AND aed.RowData.exist('Row[1]/F2[1]')=1
       AND aed.RowData.exist('Row[1]/F3[1]')=1
       AND ISNUMERIC(aed.RowData.value('Row[1]/F4[1]','nvarchar(10)')) = 1
       AND aed.RowData.exist('Row[1]/F5[1]')=1;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' SLDealProps rows given RowFormat = SLDealProperties';
        
    --                                      Stage SLDealProperties data 

    -- TRUNCATE TABLE dExcelETL.SLDealProperties;
    -- SELECT * FROM dExcelETL.SLDealProperties where UpdateDatetime < (getutcdate()-0.1);
    -- SELECT * INTO #T FROM dExcelETL.SLDealProperties;      DROP TABLE #T;
    -- UPDATE dExcelETL.SLDealProperties SET DealName = 'abc' WHERE DealName LIKE '1%'; -- Changing a value forces another change to set it back during merge
    
    WITH X AS (
       SELECT esf.SourceFileName
             ,esf.ModificationTimestamp
             ,ess.SheetName
             ,sed.RowNumber
             ,aed.RowData.value('Row[1]/F1[1]' ,'int'          ) AS DealID
             ,aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(25)' ) AS DealCode
             ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(100)') AS DealName
             ,aed.RowData.value('Row[1]/F4[1]' ,'int'          ) AS PropertyID
             ,aed.RowData.value('Row[1]/F5[1]' ,'nvarchar(500)') AS Address
         FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
        WHERE ess.SheetFormat = 'SLDealProps'
          AND RowFormat.exist('/Deal')=1
    ) --SELECT * FROM X
    MERGE INTO dExcelETL.SLDealProperties AS tgt
    USING X AS src
       ON src.DealID          = tgt.DealID
      AND src.PropertyID       = tgt.PropertyID
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.SourceFileName,'0')  != isnull(tgt.SourceFileName,'0')
        OR isnull(src.SheetName     ,'0')  != isnull(tgt.SheetName     ,'0')
        OR isnull(src.RowNumber     ,'0')  != isnull(tgt.RowNumber     ,'0')
        OR isnull(src.DealCode      ,'0')  != isnull(tgt.DealCode      ,'0')
        OR isnull(src.DealName      ,'0')  != isnull(tgt.DealName      ,'0')
        OR isnull(src.Address       ,'0')  != isnull(tgt.Address       ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime  = src.ModificationTimestamp
         ,tgt.SourceFileName  = src.SourceFileName
         ,tgt.SheetName       = src.SheetName     
         ,tgt.RowNumber       = src.RowNumber     
         ,tgt.DealCode        = src.DealCode         
         ,tgt.DealName        = src.DealName       
         ,tgt.Address         = src.Address   
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime
                 ,DealID
                 ,DealCode
                 ,DealName
                 ,Address
                 ,PropertyID )
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,src.ModificationTimestamp
                 ,src.DealID
                 ,src.DealCode
                 ,src.DealName
                 ,src.Address
                 ,src.PropertyID  );
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into SLDealProperties';

    -- SELECT * FROM dExcelETL.SLDealProperties;
    -- SELECT * FROM dExcelETL.SLDealProperties WHERE UpdateDatetime > (getutcdate()-0.1);
END
/*Test
GO
EXEC dExcelETL.ImportSLDealProperties;
--*/
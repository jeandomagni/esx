﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/03/29
-- Description: Moves unprocessed SLProperties rows in StagedExcelData to AdjustedExcelData for adjustment.
--              Performs needed adjustments then loads the results into SLProperties.
-- =============================================
CREATE PROCEDURE [dExcelETL].[ImportSLProperties] AS
BEGIN
    SET NOCOUNT ON;
    /*      -- Use this to retry importing all data.
    --TRUNCATE TABLE dExcelETL.AdjustedExcelData;
    DELETE dExcelETL.AdjustedExcelData            -- SELECT * FROM dExcelETL.AdjustedExcelData
     WHERE StagedExcelData_Key in 
        (SELECT StagedExcelData_Key 
           FROM dExcelETL.StagedExcelData sed
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
          WHERE ess.SheetFormat = 'SLProperties');
    --*/ 

    -- Populate the adjusted data table
    INSERT INTO dExcelETL.AdjustedExcelData(StagedExcelData_Key,RowFormat,RowData)
    SELECT sed.StagedExcelData_Key,'',sed.RowData       -- SELECT *
      FROM dExcelETL.StagedExcelData sed
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
      LEFT JOIN dExcelETL.AdjustedExcelData aed
        ON aed.StagedExcelData_Key = sed.StagedExcelData_Key
      WHERE ess.SheetFormat = 'SLProperties'
        AND aed.StagedExcelData_Key IS NULL;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to AdjustedExcelData';

    -- Set the row format for rows without one.
    UPDATE aed
       SET RowFormat.modify('insert <Address /> into /')    -- select *
      FROM dExcelETL.AdjustedExcelData aed 
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'SLProperties'
       AND aed.RowFormat.exist('/Address')=0
       AND ISNUMERIC(aed.RowData.value('Row[1]/F1[1]','nvarchar(10)')) = 1
       AND aed.RowData.exist('Row[1]/F5[1]')=1;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows in AdjustedExcelData have RowFormat = Address';
    
    --                                      Fix known incorrect addresses
    UPDATE aed
       SET RowData.modify('replace value of (Row/F3/text())[1] with "610 Exterior Street"')
      FROM dExcelETL.AdjustedExcelData aed
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'SLProperties'
       AND aed.RowFormat.exist('/Address')=1
       AND aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(50)' ) = 'Bronx Terminal Market'
       AND aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(50)' ) = '610 Gateway Center Blvd';
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' "Bronx Terminal Market" address fixed';

    --                                      Stage SLProperties data 

    -- TRUNCATE TABLE dExcelETL.SLProperties;       
    -- SELECT * FROM dExcelETL.SLProperties where UpdateDatetime < (getutcdate()-0.1);
    -- SELECT * INTO #T FROM dExcelETL.SLProperties;      DROP TABLE #T;
    -- UPDATE dExcelETL.SLProperties SET Address1 = 'abc' WHERE Address1 LIKE '1%'; -- Changing an address forces another change to set it back during merge
    
    WITH X AS (
       SELECT esf.SourceFileName
             ,esf.ModificationTimestamp
             ,ess.SheetName
             ,sed.RowNumber
             ,aed.RowData.value('Row[1]/F1[1]' ,'nvarchar(50)' ) PropertyID
             ,aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(50)' ) PropertyName
             ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(50)' ) Address1
             ,aed.RowData.value('Row[1]/F4[1]' ,'nvarchar(50)' ) City
             ,aed.RowData.value('Row[1]/F5[1]' ,'nvarchar(20)' ) StateCountry
         FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
        WHERE ess.SheetFormat = 'SLProperties'
          AND RowFormat.exist('Address')=1 
    ),Y AS (
        SELECT X.SourceFileName
              ,X.ModificationTimestamp
              ,X.SheetName
              ,X.RowNumber
              ,X.PropertyID
              ,X.PropertyName
              ,X.Address1
              ,X.City
              ,S.State
              ,ISNULL(S.Country,X.StateCountry) AS Country
          FROM X
          LEFT JOIN rESP.States S
            ON S.State = LTRIM(X.StateCountry)
    ) --SELECT * FROM Y
    MERGE INTO dExcelETL.SLProperties AS tgt
    USING Y AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.PropertyID      = tgt.PropertyID
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.RowNumber      ,'0')  != isnull(tgt.RowNumber      ,'0')
        OR isnull(src.PropertyName   ,'0')  != isnull(tgt.PropertyName   ,'0')
        OR isnull(src.Address1       ,'0')  != isnull(tgt.Address1       ,'0')
        OR isnull(src.City           ,'0')  != isnull(tgt.City           ,'0')
        OR isnull(src.[State]        ,'0')  != isnull(tgt.[State]        ,'0')
        OR isnull(src.Country        ,'0')  != isnull(tgt.Country        ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime  = src.ModificationTimestamp
         ,tgt.RowNumber       = src.RowNumber   
         ,tgt.PropertyName    = src.PropertyName
         ,tgt.Address1        = src.Address1    
         ,tgt.City            = src.City        
         ,tgt.[State]         = src.[State]     
         ,tgt.Country         = src.Country     
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime
                 ,PropertyID
                 ,PropertyName
                 ,Address1
                 ,City
                 ,[State]
                 ,Country      )
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,src.ModificationTimestamp
                 ,src.PropertyID
                 ,src.PropertyName
                 ,src.Address1
                 ,src.City
                 ,src.[State]
                 ,src.Country      );
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into SLProperties';

    -- SELECT * FROM dExcelETL.SLProperties;
    -- SELECT * FROM dExcelETL.SLProperties WHERE UpdateDatetime > (getutcdate()-0.1);
END
/*Test
GO
EXEC dExcelETL.ImportSLProperties;
--*/
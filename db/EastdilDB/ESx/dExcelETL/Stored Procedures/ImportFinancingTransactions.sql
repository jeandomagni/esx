﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/02/22
-- Description: Moves unprocessed ClosedTransaction in StagedExcelData to AdjustedExcelData for adjustment.
--              Performs needed adjustments then loads the data into FinancingTransactions.
-- =============================================
CREATE PROCEDURE dExcelETL.ImportFinancingTransactions AS
BEGIN
    SET NOCOUNT ON;
    /*      -- Use this to retry importing all data.
    --TRUNCATE TABLE dExcelETL.AdjustedExcelData;
    DELETE dExcelETL.AdjustedExcelData            -- SELECT * FROM dExcelETL.AdjustedExcelData
     WHERE StagedExcelData_Key in 
        (SELECT StagedExcelData_Key 
           FROM dExcelETL.StagedExcelData sed
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
          WHERE ess.SheetFormat = 'FinancingTransactions');
    --*/ 

    INSERT INTO dExcelETL.AdjustedExcelData(StagedExcelData_Key,RowFormat,RowData)
    SELECT sed.StagedExcelData_Key,'',sed.RowData       -- SELECT *
      FROM dExcelETL.StagedExcelData sed
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
      LEFT JOIN dExcelETL.AdjustedExcelData aed
        ON aed.StagedExcelData_Key = sed.StagedExcelData_Key
      WHERE ess.SheetFormat = 'FinancingTransactions'
        AND aed.StagedExcelData_Key IS NULL;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to AdjustedExcelData';

    WITH X AS (
        SELECT sed.ExcelSourceSheet_Key,ess.SheetName,MIN(sed.RowNumber) UpperBound
          FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
        WHERE ess.SheetFormat = 'FinancingTransactions'
          AND RowNumber > 2
          AND aed.RowData.exist('Row[1]/F2[1]') = 1
        GROUP BY sed.ExcelSourceSheet_Key,ess.SheetName
    ) --/*
    UPDATE aed
       SET RowFormat.modify('insert if (sql:column("X.UpperBound") <= sql:column("sed.RowNumber")) then <Detail /> else <Base /> into /')
           -- */ select CASE WHEN X.UpperBound <= sed.RowNumber THEN 'UPPER' ELSE 'LOWER' END,DATALENGTH(RowFormat),*
      FROM dExcelETL.AdjustedExcelData aed 
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN X
        ON X.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE aed.RowFormat.exist('/Detail')=0
       AND aed.RowFormat.exist('/Base')=0;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows in AdjustedExcelData have RowFormat';
    
    
    --                                      Stage FinancingTransaction data 

    --TRUNCATE TABLE dExcelETL.FinancingTransaction;       
    -- SELECT * FROM dExcelETL.FinancingTransaction where UpdateDatetime > (getutcdate()-0.1);
    
    WITH X AS (
       SELECT esf.SourceFileName
             ,ess.SheetName
             ,sed.RowNumber
             ,aed.RowData.value('Row[1]/F1[1]' ,'nvarchar(50)' ) ES
             ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(50)' ) EsTeam
             ,aed.RowData.value('Row[1]/F4[1]' ,'nvarchar(50)' ) DealCode
             ,aed.RowData.value('Row[1]/F5[1]' ,'nvarchar(100)') Client
             ,aed.RowData.value('Row[1]/F6[1]' ,'nvarchar(200)') Lender
             ,aed.RowData.value('Row[1]/F7[1]' ,'nvarchar(50)' ) LenderType
             ,aed.RowData.value('Row[1]/F8[1]' ,'nvarchar(50)' ) LenderTypeII
             ,aed.RowData.value('Row[1]/F9[1]' ,'nvarchar(100)') AssetName
             ,aed.RowData.value('Row[1]/F10[1]','nvarchar(100)') Address
             ,aed.RowData.value('Row[1]/F11[1]','nvarchar(50)' ) AssetType
             ,aed.RowData.value('Row[1]/F12[1]','nvarchar(50)' ) AssetTypeII
             ,aed.RowData.value('Row[1]/F13[1]','nvarchar(50)' ) NumProperties
             ,aed.RowData.value('Row[1]/F14[1]','nvarchar(50)' ) Occupancy
             ,aed.RowData.value('Row[1]/F15[1]','nvarchar(50)' ) SfUnits
             ,aed.RowData.value('Row[1]/F16[1]','nvarchar(100)') Location
             ,aed.RowData.value('Row[1]/F17[1]','nvarchar(50)' ) CloseDate
             ,aed.RowData.value('Row[1]/F18[1]','nvarchar(50)' ) Status
             ,aed.RowData.value('Row[1]/F19[1]','nvarchar(50)' ) LoanAmount
             ,aed.RowData.value('Row[1]/F21[1]','nvarchar(50)' ) InPlaceYield
             ,aed.RowData.value('Row[1]/F22[1]','nvarchar(50)' ) FixedRate
             ,aed.RowData.value('Row[1]/F23[1]','nvarchar(50)' ) TransactionType
             ,aed.RowData.value('Row[1]/F24[1]','nvarchar(100)') Spread
             ,aed.RowData.value('Row[1]/F25[1]','nvarchar(50)' ) Leverage
             ,aed.RowData.value('Row[1]/F27[1]','nvarchar(100)') Structure
         FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
        WHERE ess.SheetFormat = 'FinancingTransactions'
          AND RowFormat.exist('Base')=1 
    ),Y AS (
        SELECT X.SourceFileName
              ,X.SheetName
              ,X.RowNumber
              ,X.ES
              ,X.EsTeam
              ,X.DealCode
              ,X.Client
              ,X.Lender
              ,X.LenderType
              ,X.LenderTypeII
              ,X.AssetName
              ,X.Address
              ,X.AssetType
              ,X.AssetTypeII
              ,CASE WHEN ISNUMERIC(X.NumProperties)=1 THEN CAST(X.NumProperties AS int) ELSE NULL END AS NumProperties
              ,[dExcelETL].FindNumberMaybe(X.Occupancy)     AS Occupancy
              ,[dExcelETL].FindNumberMaybe(X.SfUnits)       AS SfUnits
              ,X.Location
              ,'01-'+X.CloseDate                        AS CloseDate
              ,X.Status
              ,[dExcelETL].FindNumberMaybe(X.LoanAmount)    AS LoanAmount
              ,[dExcelETL].FindNumberMaybe(X.InPlaceYield)  AS InPlaceYield
              ,[dExcelETL].FindNumberMaybe(X.FixedRate)     AS FixedRate
              ,X.TransactionType
              ,X.Spread
              ,[dExcelETL].FindNumberMaybe(X.Leverage)      AS Leverage
              ,X.Structure
          FROM X
         WHERE X.AssetName IS NOT NULL
           AND X.Lender <> 'Lender'
    ), Z AS (
        SELECT Y.SourceFileName
              ,Y.SheetName
              ,Y.RowNumber
              ,Y.ES
              ,Y.EsTeam
              ,Y.DealCode
              ,Y.Client
              ,Y.Lender
              ,Y.LenderType
              ,Y.LenderTypeII
              ,Y.AssetName
              ,Y.Address
              ,Y.AssetType
              ,Y.AssetTypeII
              ,Y.NumProperties
              ,CASE WHEN ISNUMERIC(Y.Occupancy)=1    THEN CAST(Y.Occupancy AS numeric(5,2))    ELSE NULL END AS Occupancy
              ,CASE WHEN ISNUMERIC(Y.SfUnits)=1      THEN CAST(Y.SfUnits AS numeric(18,2))     ELSE NULL END AS SfUnits
              ,Y.Location
              ,CASE WHEN ISDATE(Y.CloseDate)=1       THEN CAST(Y.CloseDate AS DATE)            ELSE NULL END AS CloseDate
              ,Y.Status
              ,CASE WHEN ISNUMERIC(Y.LoanAmount)=1   THEN CAST(Y.LoanAmount as numeric(18,2))  ELSE NULL END AS LoanAmount
              ,CASE WHEN ISNUMERIC(Y.InPlaceYield)=1 THEN CAST(Y.InPlaceYield as numeric(5,2)) ELSE NULL END AS InPlaceYield
              ,CASE WHEN ISNUMERIC(Y.FixedRate)=1    THEN CAST(Y.FixedRate as numeric(5,2))    ELSE NULL END AS FixedRate
              ,Y.TransactionType
              ,Y.Spread
              ,CASE WHEN ISNUMERIC(Y.Leverage)=1     THEN CAST(Y.Leverage as numeric(5,2))     ELSE NULL END AS Leverage
              ,Y.Structure
          FROM Y
    )
    MERGE INTO dExcelETL.FinancingTransaction AS tgt
    USING Z AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.RowNumber       = tgt.RowNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.ES             ,'0')  != isnull(tgt.ES             ,'0')
        OR isnull(src.EsTeam         ,'0')  != isnull(tgt.EsTeam         ,'0')
        OR isnull(src.DealCode       ,'0')  != isnull(tgt.DealCode       ,'0')
        OR isnull(src.Client         ,'0')  != isnull(tgt.Client         ,'0')
        OR isnull(src.Lender         ,'0')  != isnull(tgt.Lender         ,'0')
        OR isnull(src.LenderType     ,'0')  != isnull(tgt.LenderType     ,'0')
        OR isnull(src.LenderTypeII   ,'0')  != isnull(tgt.LenderTypeII   ,'0')
        OR isnull(src.AssetName      ,'0')  != isnull(tgt.AssetName      ,'0')
        OR isnull(src.Address        ,'0')  != isnull(tgt.Address        ,'0')
        OR isnull(src.AssetType      ,'0')  != isnull(tgt.AssetType      ,'0')
        OR isnull(src.AssetTypeII    ,'0')  != isnull(tgt.AssetTypeII    ,'0')
        OR isnull(src.NumProperties  ,'0')  != isnull(tgt.NumProperties  ,'0')
        OR isnull(src.Occupancy      ,'0')  != isnull(tgt.Occupancy      ,'0')
        OR isnull(src.SfUnits        ,'0')  != isnull(tgt.SfUnits        ,'0')
        OR isnull(src.Location       ,'0')  != isnull(tgt.Location       ,'0')
        OR isnull(src.CloseDate      ,'' )  != isnull(tgt.CloseDate      ,'' )
        OR isnull(src.Status         ,'0')  != isnull(tgt.Status         ,'0')
        OR isnull(src.LoanAmount     ,'0')  != isnull(tgt.LoanAmount     ,'0')
        OR isnull(src.InPlaceYield   ,'0')  != isnull(tgt.InPlaceYield   ,'0')
        OR isnull(src.FixedRate      ,'0')  != isnull(tgt.FixedRate      ,'0')
        OR isnull(src.TransactionType,'0')  != isnull(tgt.TransactionType,'0')
        OR isnull(src.Spread         ,'0')  != isnull(tgt.Spread         ,'0')
        OR isnull(src.Leverage       ,'0')  != isnull(tgt.Leverage       ,'0')
        OR isnull(src.Structure      ,'0')  != isnull(tgt.Structure      ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime  = GETUTCDATE()
         ,tgt.ES              = src.ES             
         ,tgt.EsTeam          = src.EsTeam         
         ,tgt.DealCode        = src.DealCode       
         ,tgt.Client          = src.Client         
         ,tgt.Lender          = src.Lender         
         ,tgt.LenderType      = src.LenderType     
         ,tgt.LenderTypeII    = src.LenderTypeII   
         ,tgt.AssetName       = src.AssetName   
         ,tgt.Address         = src.Address        
         ,tgt.AssetType       = src.AssetType   
         ,tgt.AssetTypeII     = src.AssetTypeII 
         ,tgt.NumProperties   = src.NumProperties  
         ,tgt.Occupancy       = src.Occupancy      
         ,tgt.SfUnits         = src.SfUnits        
         ,tgt.Location        = src.Location       
         ,tgt.CloseDate       = src.CloseDate
         ,tgt.Status          = src.Status         
         ,tgt.LoanAmount      = src.LoanAmount     
         ,tgt.InPlaceYield    = src.InPlaceYield   
         ,tgt.FixedRate       = src.FixedRate      
         ,tgt.TransactionType = src.TransactionType
         ,tgt.Spread          = src.Spread         
         ,tgt.Leverage        = src.Leverage       
         ,tgt.Structure       = src.Structure      
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime        
                 ,ES             
                 ,EsTeam         
                 ,DealCode       
                 ,Client         
                 ,Lender         
                 ,LenderType     
                 ,LenderTypeII   
                 ,AssetName   
                 ,Address        
                 ,AssetType   
                 ,AssetTypeII 
                 ,NumProperties  
                 ,Occupancy      
                 ,SfUnits        
                 ,Location       
                 ,CloseDate
                 ,Status         
                 ,LoanAmount     
                 ,InPlaceYield   
                 ,FixedRate      
                 ,TransactionType
                 ,Spread         
                 ,Leverage       
                 ,Structure )
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,GETUTCDATE()
                 ,src.ES             
                 ,src.EsTeam         
                 ,src.DealCode       
                 ,src.Client         
                 ,src.Lender         
                 ,src.LenderType     
                 ,src.LenderTypeII   
                 ,src.AssetName   
                 ,src.Address        
                 ,src.AssetType   
                 ,src.AssetTypeII 
                 ,src.NumProperties  
                 ,src.Occupancy      
                 ,src.SfUnits        
                 ,src.Location       
                 ,src.CloseDate
                 ,src.Status         
                 ,src.LoanAmount     
                 ,src.InPlaceYield   
                 ,src.FixedRate      
                 ,src.TransactionType
                 ,src.Spread         
                 ,src.Leverage       
                 ,src.Structure      );
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into ClosedProperty';
 
    -- The query literally takes 100 times longer without this intermediate table.
    SELECT ess.ExcelSourceFile_Key,sed.ExcelSourceSheet_Key,ess.SheetName,sed.RowNumber
          ,aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(100)') PortfolioName
      INTO #FinancingPartition
      FROM dExcelETL.AdjustedExcelData aed
      JOIN dExcelETL.StagedExcelData sed
        ON sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'FinancingTransactions'
       AND RowFormat.exist('/Base')=0
       AND aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(100)') is not NULL;
    
    -- truncate table dExcelETL.FinancingPartition;
    WITH X AS (
        SELECT esf.SourceFileName
              ,fp.SheetName
              ,fp.RowNumber
              ,fp.PortfolioName
              ,ROW_NUMBER() OVER(ORDER BY fp.ExcelSourceFile_Key, fp.SheetName, fp.RowNumber) SeqNum
          FROM #FinancingPartition fp
          JOIN dExcelETL.ExcelSourceFile esf
            ON esf.ExcelSourceFile_Key = fp.ExcelSourceFile_Key
    ),Y AS (
        SELECT X1.SourceFileName,X1.SheetName,X1.RowNumber,X1.PortfolioName,X1.RowNumber+1 DataStartRow
              ,ISNULL(X2.RowNumber-1,100000) DataEndRow
          FROM X X1
          LEFT JOIN X X2
            ON X2.SeqNum = X1.SeqNum+1
    )        --select * from y
    MERGE INTO dExcelETL.FinancingPartition AS tgt
    USING Y AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.RowNumber       = tgt.RowNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.PortfolioName,'0') != isnull(tgt.PortfolioName,'0')
       OR  isnull(src.DataStartRow ,'0') != isnull(tgt.DataStartRow ,'0')
       OR  isnull(src.DataEndRow   ,'0') != isnull(tgt.DataEndRow   ,'0'))
     THEN 
   UPDATE SET
          tgt.UpdateDatetime  = GETUTCDATE()
         ,tgt.PortfolioName   = src.PortfolioName
         ,tgt.DataStartRow    = src.DataStartRow             
         ,tgt.DataEndRow      = src.DataEndRow
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime
                 ,PortfolioName
                 ,DataStartRow
                 ,DataEndRow)
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,GETUTCDATE()
                 ,src.PortfolioName
                 ,src.DataStartRow
                 ,src.DataEndRow);             -- select * from dExcelETL.FinancingPartition;
    
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into FinancingPartition';
    DROP TABLE #FinancingPartition;

    -- truncate table dExcelETL.FinancingDetail
    WITH X AS (
       SELECT Pa.FinancingPartition_Key
             ,sed.RowNumber - Pa.RowNumber                       AS EntryNumber
             ,aed.RowData.value('Row[1]/F1[1]' ,'nvarchar(50)' ) AS PreTable
             ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(50)' ) AS EsTeam
             ,aed.RowData.value('Row[1]/F4[1]' ,'nvarchar(50)' ) AS DealCode
             ,aed.RowData.value('Row[1]/F5[1]' ,'nvarchar(100)') AS Client
             ,aed.RowData.value('Row[1]/F6[1]' ,'nvarchar(200)') AS Lender
             ,aed.RowData.value('Row[1]/F9[1]' ,'nvarchar(100)') AS AssetName
             ,aed.RowData.value('Row[1]/F10[1]','nvarchar(100)') AS Address
             ,aed.RowData.value('Row[1]/F11[1]','nvarchar(50)' ) AS AssetType
             ,aed.RowData.value('Row[1]/F12[1]','nvarchar(50)' ) AS AssetTypeII
             ,aed.RowData.value('Row[1]/F13[1]','nvarchar(50)' ) AS NumProperties
             ,aed.RowData.value('Row[1]/F14[1]','nvarchar(50)' ) AS Occupancy
             ,aed.RowData.value('Row[1]/F15[1]','nvarchar(50)' ) AS SfUnits
             ,aed.RowData.value('Row[1]/F16[1]','nvarchar(100)') AS Location
             ,aed.RowData.value('Row[1]/F17[1]','nvarchar(50)' ) AS CloseDate
             ,aed.RowData.value('Row[1]/F18[1]','nvarchar(50)' ) AS Status
             ,aed.RowData.value('Row[1]/F19[1]','nvarchar(50)' ) AS LoanAmount
             ,aed.RowData.value('Row[1]/F21[1]','nvarchar(50)' ) AS InPlaceYield
             ,aed.RowData.value('Row[1]/F22[1]','nvarchar(50)' ) AS FixedRate
             ,aed.RowData.value('Row[1]/F23[1]','nvarchar(50)' ) AS TransactionType
             ,aed.RowData.value('Row[1]/F24[1]','nvarchar(100)') AS Spread
             ,aed.RowData.value('Row[1]/F25[1]','nvarchar(50)' ) AS Leverage
             ,aed.RowData.value('Row[1]/F27[1]','nvarchar(100)') AS Structure
    		 ,aed.RowData.value('Row[1]/F28[1]','nvarchar(100)') AS PostTable -- select *
         FROM dExcelETL.FinancingPartition Pa
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.SourceFileName = Pa.SourceFileName
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceFile_Key = esf.ExcelSourceFile_Key
          AND ess.SheetName = Pa.SheetName
         JOIN dExcelETL.StagedExcelData sed
           ON sed.ExcelSourceSheet_Key = ess.ExcelSourceSheet_Key
          AND sed.RowNumber BETWEEN Pa.DataStartRow AND Pa.DataEndRow
         JOIN dExcelETL.AdjustedExcelData aed
           ON aed.StagedExcelData_Key = sed.StagedExcelData_Key
    ),Y AS (
        SELECT X.FinancingPartition_Key
              ,X.EntryNumber
              ,X.PreTable
              ,X.EsTeam
              ,X.DealCode
              ,X.Client
              ,X.Lender
              ,X.AssetName
              ,X.Address
              ,X.AssetType
              ,X.AssetTypeII
              ,X.NumProperties
              ,[dExcelETL].FindNumberMaybe(X.Occupancy)     AS Occupancy
              ,[dExcelETL].FindNumberMaybe(X.SfUnits)       AS SfUnits
              ,X.Location
              ,'01-'+X.CloseDate                        AS CloseDate
              ,X.Status
              ,[dExcelETL].FindNumberMaybe(X.LoanAmount)    AS LoanAmount
              ,[dExcelETL].FindNumberMaybe(X.InPlaceYield)  AS InPlaceYield
              ,[dExcelETL].FindNumberMaybe(X.FixedRate)     AS FixedRate
              ,X.TransactionType
              ,X.Spread
              ,[dExcelETL].FindNumberMaybe(X.Leverage)      AS Leverage
              ,X.Structure
              ,X.PostTable
          FROM X
         WHERE X.AssetName IS NOT NULL
           AND X.Lender <> 'Lender'
    ), Z AS (
        SELECT Y.FinancingPartition_Key
              ,Y.EntryNumber
              ,Y.PreTable
              ,Y.EsTeam
              ,Y.DealCode
              ,Y.Client
              ,Y.Lender
              ,Y.AssetName
              ,Y.Address
              ,Y.AssetType
              ,Y.AssetTypeII
              ,CASE WHEN ISNUMERIC(Y.NumProperties)=1THEN CAST(Y.NumProperties AS int)         ELSE NULL END AS NumProperties
              ,CASE WHEN ISNUMERIC(Y.Occupancy)=1    THEN CAST(Y.Occupancy AS numeric(4,1))    ELSE NULL END AS Occupancy
              ,CASE WHEN ISNUMERIC(Y.SfUnits)=1      THEN CAST(Y.SfUnits AS numeric(18,2))     ELSE NULL END AS SfUnits
              ,Y.Location
              ,CASE WHEN ISDATE(Y.CloseDate)=1       THEN CAST(Y.CloseDate AS DATE)            ELSE NULL END AS CloseDate
              ,Y.Status
              ,CASE WHEN ISNUMERIC(Y.LoanAmount)=1   THEN CAST(Y.LoanAmount as money)  ELSE NULL END AS LoanAmount
              ,CASE WHEN ISNUMERIC(Y.InPlaceYield)=1 THEN CAST(Y.InPlaceYield as numeric(18,2)) ELSE NULL END AS InPlaceYield
              ,CASE WHEN ISNUMERIC(Y.FixedRate)=1    THEN CAST(Y.FixedRate as numeric(7,4))    ELSE NULL END AS FixedRate
              ,Y.TransactionType
              ,Y.Spread
              ,CASE WHEN ISNUMERIC(Y.Leverage)=1     THEN CAST(Y.Leverage as numeric(4,1))     ELSE NULL END AS Leverage
              ,Y.Structure
              ,Y.PostTable
          FROM Y
    ) --SELECT * FROM Z;
    MERGE INTO dExcelETL.FinancingDetail AS tgt
    USING Z AS src
       ON src.FinancingPartition_Key = tgt.FinancingPartition_Key
      AND src.EntryNumber            = tgt.EntryNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.PreTable       ,'0') != isnull(tgt.PreTable       ,'0')
        OR isnull(src.EsTeam         ,'0') != isnull(tgt.EsTeam         ,'0')
        OR isnull(src.DealCode       ,'0') != isnull(tgt.DealCode       ,'0')
        OR isnull(src.Client         ,'0') != isnull(tgt.Client         ,'0')
        OR isnull(src.Lender         ,'0') != isnull(tgt.Lender         ,'0')
        OR isnull(src.AssetName      ,'0') != isnull(tgt.AssetName      ,'0')
        OR isnull(src.Address        ,'0') != isnull(tgt.Address        ,'0')
        OR isnull(src.AssetType      ,'0') != isnull(tgt.AssetType      ,'0')
        OR isnull(src.AssetTypeII    ,'0') != isnull(tgt.AssetTypeII    ,'0')
        OR isnull(src.NumProperties  ,'0') != isnull(tgt.NumProperties  ,'0')
        OR isnull(src.Occupancy      ,'0') != isnull(tgt.Occupancy      ,'0')
        OR isnull(src.SfUnits        ,'0') != isnull(tgt.SfUnits        ,'0')
        OR isnull(src.Location       ,'0') != isnull(tgt.Location       ,'0')
        OR isnull(src.CloseDate      ,'' ) != isnull(tgt.CloseDate      ,'' )
        OR isnull(src.Status         ,'0') != isnull(tgt.Status         ,'0')
        OR isnull(src.LoanAmount     ,'0') != isnull(tgt.LoanAmount     ,'0')
        OR isnull(src.InPlaceYield   ,'0') != isnull(tgt.InPlaceYield   ,'0')
        OR isnull(src.FixedRate      ,'0') != isnull(tgt.FixedRate      ,'0')
        OR isnull(src.TransactionType,'0') != isnull(tgt.TransactionType,'0')
        OR isnull(src.Spread         ,'0') != isnull(tgt.Spread         ,'0')
        OR isnull(src.Leverage       ,'0') != isnull(tgt.Leverage       ,'0')
        OR isnull(src.Structure      ,'0') != isnull(tgt.Structure      ,'0')
        OR isnull(src.PostTable      ,'0') != isnull(tgt.PostTable      ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime  = GETUTCDATE()
         ,tgt.PreTable        = src.PreTable             
         ,tgt.EsTeam          = src.EsTeam         
         ,tgt.DealCode        = src.DealCode       
         ,tgt.Client          = src.Client         
         ,tgt.Lender          = src.Lender         
         ,tgt.AssetName       = src.AssetName   
         ,tgt.Address         = src.Address        
         ,tgt.AssetType       = src.AssetType   
         ,tgt.AssetTypeII     = src.AssetTypeII 
         ,tgt.NumProperties   = src.NumProperties  
         ,tgt.Occupancy       = src.Occupancy      
         ,tgt.SfUnits         = src.SfUnits        
         ,tgt.Location        = src.Location       
         ,tgt.CloseDate       = src.CloseDate
         ,tgt.Status          = src.Status         
         ,tgt.LoanAmount      = src.LoanAmount     
         ,tgt.InPlaceYield    = src.InPlaceYield   
         ,tgt.FixedRate       = src.FixedRate      
         ,tgt.TransactionType = src.TransactionType
         ,tgt.Spread          = src.Spread         
         ,tgt.Leverage        = src.Leverage       
         ,tgt.Structure       = src.Structure      
         ,tgt.PostTable       = src.PostTable
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (FinancingPartition_Key
                 ,EntryNumber
                 ,UpdateDatetime        
                 ,PreTable
                 ,EsTeam         
                 ,DealCode       
                 ,Client         
                 ,Lender         
                 ,AssetName   
                 ,Address        
                 ,AssetType   
                 ,AssetTypeII 
                 ,NumProperties  
                 ,Occupancy      
                 ,SfUnits        
                 ,Location       
                 ,CloseDate
                 ,Status         
                 ,LoanAmount     
                 ,InPlaceYield   
                 ,FixedRate      
                 ,TransactionType
                 ,Spread         
                 ,Leverage       
                 ,Structure
                 ,PostTable )
          VALUES (src.FinancingPartition_Key
                 ,src.EntryNumber
                 ,GETUTCDATE()
                 ,src.PreTable             
                 ,src.EsTeam         
                 ,src.DealCode       
                 ,src.Client         
                 ,src.Lender         
                 ,src.AssetName   
                 ,src.Address        
                 ,src.AssetType   
                 ,src.AssetTypeII 
                 ,src.NumProperties  
                 ,src.Occupancy      
                 ,src.SfUnits        
                 ,src.Location       
                 ,src.CloseDate
                 ,src.Status         
                 ,src.LoanAmount     
                 ,src.InPlaceYield   
                 ,src.FixedRate      
                 ,src.TransactionType
                 ,src.Spread         
                 ,src.Leverage       
                 ,src.Structure      
                 ,src.PostTable);
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into FinancingDetail';
--    SELECT * FROM dExcelETL.FinancingDetail

    --update dExcelETL.FinancingTransaction set DealCode = 'abc' where DealCode like 'E%';
END
/*Test
GO
EXEC dExcelETL.ImportFinancingTransactions;
--*/
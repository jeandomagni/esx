﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/03/30
-- Description: Moves unprocessed SL Company related data to AdjustedExcelData for adjustment.
--              Performs needed adjustments then loads the data into SLBranchCanonicals,SLCanonicalAliases,SLCanonicals and ESPBranchEntities
-- =============================================
CREATE PROCEDURE dExcelETL.ImportSLCanonicals AS
BEGIN
    SET NOCOUNT ON;
    /*      -- Use this to retry importing all data.
    TRUNCATE TABLE dExcelETL.AdjustedExcelData;
    DELETE dExcelETL.AdjustedExcelData            -- SELECT * FROM dExcelETL.AdjustedExcelData
     WHERE StagedExcelData_Key in 
        (SELECT StagedExcelData_Key
           FROM dExcelETL.StagedExcelData sed
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
          WHERE ess.SheetFormat in ('SLBranchCanonicals','SLCanonicalAliases','SLCanonicals'));
    --*/ 

    -- Populate 
    INSERT INTO dExcelETL.AdjustedExcelData(StagedExcelData_Key,RowFormat,RowData)
    SELECT sed.StagedExcelData_Key,'',sed.RowData       -- SELECT *
      FROM dExcelETL.StagedExcelData sed
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
      LEFT JOIN dExcelETL.AdjustedExcelData aed
        ON aed.StagedExcelData_Key = sed.StagedExcelData_Key
      WHERE ess.SheetFormat in ('SLBranchCanonicals','SLCanonicalAliases','SLCanonicals','ESPBranchEntities')
        AND aed.StagedExcelData_Key IS NULL;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows inserted to AdjustedExcelData';


    UPDATE aed
       SET RowFormat.modify('insert <Data /> into /')
           -- */ select *
      FROM dExcelETL.AdjustedExcelData aed 
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE aed.RowFormat.exist('/Data')=0
       AND ess.SheetFormat in ('SLBranchCanonicals','SLCanonicalAliases','SLCanonicals','ESPBranchEntities')
       AND sed.RowNumber > 1
       AND (ess.SheetFormat <> 'SLCanonicals' OR ISNUMERIC(aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(200)')) = 0);
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows in AdjustedExcelData given a RowFormat';

    --                                      Stage SLCanonicals data 

    -- TRUNCATE TABLE dExcelETL.SLCanonicals;       
    -- SELECT * FROM dExcelETL.SLCanonicals;
    
    WITH X AS (
       SELECT esf.SourceFileName
             ,ess.SheetName
             ,sed.RowNumber
             ,aed.RowData.value('Row[1]/F1[1]' ,'int') CanonicalID
             ,aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(200)')CanonicalName  -- Select *
         FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
        WHERE ess.SheetFormat = 'SLCanonicals'
          AND RowFormat.exist('Data')=1 
    )
    MERGE INTO dExcelETL.SLCanonicals AS tgt
    USING X AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.RowNumber       = tgt.RowNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.CanonicalID  ,'')  != isnull(tgt.CanonicalID  ,'' ) 
        OR isnull(src.CanonicalName,'0') != isnull(tgt.CanonicalName,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime = GETUTCDATE()
         ,tgt.CanonicalID    = src.CanonicalID  
         ,tgt.CanonicalName  = src.CanonicalName
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime        
                 ,CanonicalID   
                 ,CanonicalName      )
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,GETUTCDATE()
                 ,src.CanonicalID 
                 ,src.CanonicalName   );
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into SLCanonicals';

    --UPDATE dExcelETL.SLCanonicals SET CanonicalName = 'abc' WHERE CanonicalName like 'E%';
    --SELECT * FROM dExcelETL.SLCanonicals WHERE UpdateDatetime > (getutcdate()-0.1);

    --                                      Stage SLCanonicalAliases data 

    -- TRUNCATE TABLE dExcelETL.SLCanonicalAliases;       
    -- SELECT * FROM dExcelETL.SLCanonicalAliases;
    
    WITH X AS (
        SELECT esf.SourceFileName
              ,ess.SheetName
              ,sed.RowNumber
              ,aed.RowData.value('Row[1]/F1[1]' ,'int') CanonicalID
              ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(200)')AliasName     -- select * 
         FROM dExcelETL.AdjustedExcelData aed
         JOIN dExcelETL.StagedExcelData sed
           on sed.StagedExcelData_Key = aed.StagedExcelData_Key
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
        WHERE ess.SheetFormat = 'SLCanonicalAliases'
          AND RowFormat.exist('Data')=1 
    )
    MERGE INTO dExcelETL.SLCanonicalAliases AS tgt
    USING X AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.RowNumber       = tgt.RowNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.CanonicalID,'')  != isnull(tgt.CanonicalID,'' ) 
        OR isnull(src.AliasName  ,'0') != isnull(tgt.AliasName  ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime = GETUTCDATE()
         ,tgt.CanonicalID    = src.CanonicalID  
         ,tgt.AliasName      = src.AliasName
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime        
                 ,CanonicalID   
                 ,AliasName      )
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,GETUTCDATE()
                 ,src.CanonicalID 
                 ,src.AliasName   );
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into SLCanonicalAliases';

    --UPDATE dExcelETL.SLCanonicalAliases SET AliasName = 'abc' WHERE AliasName like 'H%';
    --SELECT * FROM dExcelETL.SLCanonicalAliases WHERE UpdateDatetime > (getutcdate()-0.1);

    --                                      Stage ESPBranchEntities data 

    -- TRUNCATE TABLE dExcelETL.ESPBranchEntities;       
    -- SELECT * FROM dExcelETL.ESPBranchEntities;
    
    WITH X AS (
        SELECT esf.SourceFileName
              ,ess.SheetName
              ,sed.RowNumber
              ,aed.RowData.value('Row[1]/F1[1]' ,'nvarchar(50)') CompanyID
              ,aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(200)')CompanyName
              ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(200)')DBAName
              ,aed.RowData.value('Row[1]/F4[1]' ,'nvarchar(50)') Active
              ,aed.RowData.value('Row[1]/F5[1]' ,'nvarchar(50)') LastUpdate     -- select * 
          FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            ON sed.StagedExcelData_Key = aed.StagedExcelData_Key
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
          JOIN dExcelETL.ExcelSourceFile esf
            ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
         WHERE ess.SheetFormat = 'ESPBranchEntities'
           AND RowFormat.exist('Data')=1
    ), Y AS (
        SELECT X.SourceFileName
              ,X.SheetName
              ,X.RowNumber
              ,X.CompanyID
              ,X.CompanyName
              ,X.DBAName
              ,CAST(CASE WHEN X.Active = 'TRUE' THEN 1 ELSE 0 END AS bit) Active
              ,CASE WHEN ISNUMERIC(X.LastUpdate)=1
                    THEN CAST(CAST(CAST(X.LastUpdate AS numeric(18,6)) - 2 as DateTime) AS date)
                    ELSE CAST(X.LastUpdate AS date)
               END LastUpdate
          FROM X
    )    --select * from y;
    MERGE INTO dExcelETL.ESPBranchEntities AS tgt
    USING Y AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.RowNumber       = tgt.RowNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.CompanyID  ,'0') != isnull(tgt.CompanyID  ,'0') 
        OR isnull(src.CompanyName,'0') != isnull(tgt.CompanyName,'0') 
        OR isnull(src.DBAName    ,'0') != isnull(tgt.DBAName    ,'0') 
        OR isnull(src.Active     ,'0') != isnull(tgt.Active     ,'0') 
        OR isnull(src.LastUpdate ,'0') != isnull(tgt.LastUpdateDate ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime = GETUTCDATE()
         ,tgt.CompanyID      = src.CompanyID  
         ,tgt.CompanyName    = src.CompanyName
         ,tgt.DBAName        = src.DBAName    
         ,tgt.Active         = src.Active
         ,tgt.LastUpdateDate = src.LastUpdate
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime        
                 ,CompanyID     
                 ,CompanyName   
                 ,DBAName       
                 ,Active        
                 ,LastUpdateDate    )
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,GETUTCDATE()
                 ,src.CompanyID     
                 ,src.CompanyName   
                 ,src.DBAName       
                 ,src.Active
                 ,src.LastUpdate    );
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into ESPBranchEntities';

    --UPDATE dExcelETL.ESPBranchEntities SET CompanyName = 'abc' WHERE CompanyName like 'H%';
    --SELECT * FROM dExcelETL.ESPBranchEntities WHERE UpdateDatetime > (getutcdate()-0.1);

    --                                      Stage SLBranchCanonicals data 

    -- TRUNCATE TABLE dExcelETL.SLBranchCanonicals;       
    -- SELECT * FROM dExcelETL.SLBranchCanonicals;
    
    WITH X AS (
       SELECT esf.SourceFileName
             ,ess.SheetName
             ,sed.RowNumber
             ,aed.RowData.value('Row[1]/F1[1]' ,'nvarchar(200)')CompanyName
             ,aed.RowData.value('Row[1]/F2[1]' ,'int') CompanyID
             ,aed.RowData.value('Row[1]/F4[1]' ,'int') CanonicalID     -- select * 
         FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
        WHERE ess.SheetFormat = 'SLBranchCanonicals'
          AND RowFormat.exist('Data')=1 
    )
    MERGE INTO dExcelETL.SLBranchCanonicals AS tgt
    USING X AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.RowNumber       = tgt.RowNumber
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.CompanyID  ,'0') != isnull(tgt.CompanyID  ,'0') 
        OR isnull(src.CompanyName,'0') != isnull(tgt.CompanyName,'0') 
        OR isnull(src.CanonicalID,'0') != isnull(tgt.CanonicalID,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime = GETUTCDATE()
         ,tgt.CompanyID      = src.CompanyID  
         ,tgt.CompanyName    = src.CompanyName
         ,tgt.CanonicalID    = src.CanonicalID
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime        
                 ,CompanyID     
                 ,CompanyName   
                 ,CanonicalID      )
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,GETUTCDATE()
                 ,src.CompanyID     
                 ,src.CompanyName   
                 ,src.CanonicalID    );
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into SLBranchCanonicals';

    --UPDATE dExcelETL.SLBranchCanonicals SET CompanyName = 'abc' WHERE CompanyName like 'E%';
    --SELECT * FROM dExcelETL.SLBranchCanonicals WHERE UpdateDatetime > (getutcdate()-0.1);

END
/*Test
GO
EXEC dExcelETL.ImportSLCanonicals;
--*/
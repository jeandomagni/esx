﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/02/22
-- Description: Removes characters typically found in Eastdil Excel 'numeric' cells that prevent recognition as numbers
-- =============================================
CREATE PROCEDURE [dExcelETL].[CategorizeImportedData] AS
BEGIN
    SET NOCOUNT ON;

    SELECT ess.ExcelSourceSheet_Key
      INTO #CategorizeImportedData
      FROM dExcelETL.ExcelSourceFile esf
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceFile_Key = esf.ExcelSourceFile_Key
     WHERE esf.IsComplete = 1
       AND ess.SheetFormat = 'Unknown';             -- SELECT * FROM #CategorizeImportedData;

    UPDATE dExcelETL.ExcelSourceSheet
       SET SheetFormat = 'EastdilOffices'          -- SELECT * FROM dExcelETL.ExcelSourceSheet
     WHERE ExcelSourceSheet_Key IN (
            SELECT ess.ExcelSourceSheet_Key
              FROM dExcelETL.ExcelSourceFile esf
              JOIN dExcelETL.ExcelSourceSheet ess
                ON ess.ExcelSourceFile_Key = esf.ExcelSourceFile_Key
             WHERE ess.SheetName = 'Sheet1$'
               AND esf.SourceFileName LIKE '%\EastdilOffices.xlsx'
               AND ExcelSourceSheet_Key IN (SELECT ExcelSourceSheet_Key FROM #CategorizeImportedData));
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' EastdilOffices rows marked';

    UPDATE dExcelETL.ExcelSourceSheet
       SET SheetFormat = 'ClosedTransactions'          -- SELECT * FROM dExcelETL.ExcelSourceSheet
     WHERE ExcelSourceSheet_Key IN (
            SELECT ExcelSourceSheet_Key 
              FROM dExcelETL.StagedExcelData 
             WHERE CAST(RowData AS nvarchar(4000)) like '%Closed Transactions%' and RowNumber < 4
               AND ExcelSourceSheet_Key IN (SELECT ExcelSourceSheet_Key FROM #CategorizeImportedData));
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' ClosedTransactions rows marked';
    
    UPDATE dExcelETL.ExcelSourceSheet
       SET SheetFormat = 'ClosedProperties'          -- SELECT * FROM dExcelETL.ExcelSourceSheet
     WHERE ExcelSourceSheet_Key IN (
            SELECT ExcelSourceSheet_Key 
              FROM dExcelETL.StagedExcelData 
             WHERE CAST(RowData AS nvarchar(4000)) like '%Closed Property%' and RowNumber < 4
               AND ExcelSourceSheet_Key IN (SELECT ExcelSourceSheet_Key FROM #CategorizeImportedData));
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' ClosedProperties rows marked';
    
    UPDATE dExcelETL.ExcelSourceSheet
       SET SheetFormat = 'FinancingTransactions'          -- SELECT * FROM dExcelETL.ExcelSourceSheet
     WHERE ExcelSourceSheet_Key IN (
            SELECT ExcelSourceSheet_Key 
              FROM dExcelETL.StagedExcelData 
             WHERE CAST(RowData AS nvarchar(4000)) like '%Financing Transactions%' and RowNumber < 4
               AND ExcelSourceSheet_Key IN (SELECT ExcelSourceSheet_Key FROM #CategorizeImportedData));
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' FinancingTransactions rows marked';

    UPDATE dExcelETL.ExcelSourceSheet
       SET SheetFormat = SUBSTRING(SheetName,1,LEN(SheetName)-1)          -- SELECT *,SUBSTRING(SheetName,1,LEN(SheetName)-1) FROM dExcelETL.ExcelSourceSheet
     WHERE SheetName LIKE 'SL%'
       AND ExcelSourceSheet_Key IN (SELECT ExcelSourceSheet_Key FROM #CategorizeImportedData);
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' SL-Table rows marked';

    UPDATE dExcelETL.ExcelSourceSheet
       SET SheetFormat = SUBSTRING(SheetName,1,LEN(SheetName)-1)          -- SELECT *,SUBSTRING(SheetName,1,LEN(SheetName)-1) FROM dExcelETL.ExcelSourceSheet
     WHERE SheetName = 'ESPBranchEntities$'
       AND ExcelSourceSheet_Key IN (SELECT ExcelSourceSheet_Key FROM #CategorizeImportedData);
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' ESPBranchEntities rows marked';

    UPDATE dExcelETL.ExcelSourceSheet
       SET SheetFormat = SUBSTRING(SheetName,1,LEN(SheetName)-1)          -- SELECT *,SUBSTRING(SheetName,1,LEN(SheetName)-1) FROM dExcelETL.ExcelSourceSheet
     WHERE SheetName = 'ESPDealid$'
       AND ExcelSourceSheet_Key IN (SELECT ExcelSourceSheet_Key FROM #CategorizeImportedData);
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' ESPDealid rows marked';

    DROP TABLE #CategorizeImportedData;
END
/*Test
GO
EXEC dExcelETL.CategorizeImportedData;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/20
-- Description: Moves Eastdil Office data to EastdilOffices
-- =============================================
CREATE PROCEDURE [dExcelETL].[ImportEastdilOffices] AS
BEGIN
    SET NOCOUNT ON;
    /*      -- Use this to retry importing all data.
    --TRUNCATE TABLE dExcelETL.AdjustedExcelData;
    DELETE dExcelETL.AdjustedExcelData            -- SELECT * FROM dExcelETL.AdjustedExcelData
     WHERE StagedExcelData_Key in 
        (SELECT StagedExcelData_Key             -- select distinct ess.SheetFormat
           FROM dExcelETL.StagedExcelData sed
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
          WHERE ess.SheetFormat = 'EastdilOffices');
    --*/ 

    -- Populate the adjusted data table
    INSERT INTO dExcelETL.AdjustedExcelData(StagedExcelData_Key,RowFormat,RowData)
    SELECT sed.StagedExcelData_Key,'',sed.RowData       -- SELECT *
      FROM dExcelETL.StagedExcelData sed
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
      LEFT JOIN dExcelETL.AdjustedExcelData aed
        ON aed.StagedExcelData_Key = sed.StagedExcelData_Key
      WHERE ess.SheetFormat = 'EastdilOffices'
        AND aed.StagedExcelData_Key IS NULL;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' EastdilOffices rows inserted to AdjustedExcelData';

    -- Set the row format for rows without one.
    UPDATE aed
       SET RowFormat.modify('insert <EastdilOffices /> into /')    -- select *
      FROM dExcelETL.AdjustedExcelData aed 
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'EastdilOffices'
       AND aed.RowFormat.exist('/EastdilOffices')=0
       AND aed.RowData.exist('Row[1]/F1[1]')=1
       AND aed.RowData.exist('Row[1]/F2[1]')=1
       AND aed.RowData.exist('Row[1]/F3[1]')=1
       AND aed.RowData.exist('Row[1]/F4[1]')=1;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows in AdjustedExcelData given RowFormat = EastdilOffices';
    
    -- Fix office names.  Website names differ from Employee mapping list
    DECLARE @cnt int = 0;
    UPDATE dExcelETL.AdjustedExcelData
       SET RowData.modify('replace value of (Row[1]/F1[1]/text())[1] with "San Jose"')        -- select * from dExcelETL.AdjustedExcelData
     WHERE RowFormat.exist('EastdilOffices')=1
       AND RowData.value('Row[1]/F1[1]','nvarchar(50)')='Silicon Valley';
    SET @cnt += @@ROWCOUNT;
    UPDATE dExcelETL.AdjustedExcelData
       SET RowData.modify('replace value of (Row[1]/F1[1]/text())[1] with "Washington, DC"')
     WHERE RowFormat.exist('EastdilOffices')=1
       AND RowData.value('Row[1]/F1[1]','nvarchar(50)')='Washington, D.C.';
    SET @cnt += @@ROWCOUNT;
    UPDATE dExcelETL.AdjustedExcelData
       SET RowData.modify('replace value of (Row[1]/F1[1]/text())[1] with "Irvine"')
     WHERE RowFormat.exist('EastdilOffices')=1
       AND RowData.value('Row[1]/F1[1]','nvarchar(50)')='Orange County';
    SET @cnt += @@ROWCOUNT;
    UPDATE dExcelETL.AdjustedExcelData
       SET RowData.modify('replace value of (Row[1]/F1[1]/text())[1] with "Other"')
     WHERE RowFormat.exist('EastdilOffices')=1
       AND RowData.value('Row[1]/F1[1]','nvarchar(50)')='Las Vegas';
    SET @cnt += @@ROWCOUNT;
    PRINT CAST(@cnt AS nvarchar)+' AdjustedExcelData Rows have had their OfficeName values adjusted';

    --                                      Stage EastdilOffices data 

    -- TRUNCATE TABLE dExcelETL.EastdilOffices;       
    -- SELECT * FROM dExcelETL.EastdilOffices where UpdateDatetime < (getutcdate()-0.1);
    -- SELECT * INTO #T FROM dExcelETL.EastdilOffices;      DROP TABLE #T;
    -- Changing a address forces another change to set it back during merge
    -- UPDATE dExcelETL.EastdilOffices SET address1 = 'abc' WHERE OfficeName LIKE 'L%';
    -- Changing the name forces deletes of the changed name and inserts of the original name
    -- UPDATE dExcelETL.EastdilOffices SET OfficeName = 'abc' WHERE OfficeName LIKE 'L%';
    
    WITH X AS (
       SELECT esf.SourceFileName
             ,esf.ModificationTimestamp
             ,ess.SheetName
             ,sed.RowNumber
             ,aed.RowData.value('Row[1]/F1[1]' ,'nvarchar(50)' ) OfficeName
             ,aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(100)') Address1
             ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(100)') Address2
             ,aed.RowData.value('Row[1]/F4[1]' ,'nvarchar(100)') Address3
             ,aed.RowData.value('Row[1]/F5[1]' ,'nvarchar(20)' ) Phone
             ,aed.RowData.value('Row[1]/F6[1]' ,'nvarchar(20)' ) Fax
         FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
        WHERE ess.SheetFormat = 'EastdilOffices'
          AND RowFormat.exist('EastdilOffices')=1
    ) --SELECT * FROM X
    MERGE INTO dExcelETL.EastdilOffices AS tgt
    USING X AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.OfficeName      = tgt.OfficeName
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.RowNumber     ,'0')  != isnull(tgt.RowNumber      ,'0')
        OR isnull(src.Address1      ,'0')  != isnull(tgt.Address1       ,'0')
        OR isnull(src.Address2      ,'0')  != isnull(tgt.Address2       ,'0')
        OR isnull(src.Address3      ,'0')  != isnull(tgt.Address3       ,'0')
        OR isnull(src.Phone         ,'0')  != isnull(tgt.Phone          ,'0')
        OR isnull(src.Fax           ,'0')  != isnull(tgt.Fax            ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime  = src.ModificationTimestamp
         ,tgt.RowNumber       = src.RowNumber   
         ,tgt.Address1        = src.Address1
         ,tgt.Address2        = src.Address2
         ,tgt.Address3        = src.Address3
         ,tgt.Phone           = src.Phone
         ,tgt.Fax             = src.Fax
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime
                 ,OfficeName
                 ,Address1
                 ,Address2
                 ,Address3
                 ,Phone   
                 ,Fax     )
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,src.ModificationTimestamp
                 ,src.OfficeName
                 ,src.Address1
                 ,src.Address2
                 ,src.Address3
                 ,src.Phone   
                 ,src.Fax        );
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into EastdilOffices';

    -- SELECT * FROM dExcelETL.EastdilOffices;
    -- SELECT * FROM dExcelETL.EastdilOffices WHERE UpdateDatetime > (getutcdate()-0.1);
END
/*Test
GO
EXEC dExcelETL.ImportEastdilOffices;
--*/
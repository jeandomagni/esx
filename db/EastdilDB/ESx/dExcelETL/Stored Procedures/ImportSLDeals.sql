﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/04/18
-- Description: Moves Deal rows in StagedExcelData to AdjustedExcelData for adjustment.
--              Performs needed adjustments then loads the results into SLDeals.
-- =============================================
CREATE PROCEDURE [dExcelETL].[ImportSLDeals] AS
BEGIN
    SET NOCOUNT ON;
    /*      -- Use this to retry importing all data.
    --TRUNCATE TABLE dExcelETL.AdjustedExcelData;
    DELETE dExcelETL.AdjustedExcelData            -- SELECT * FROM dExcelETL.AdjustedExcelData
     WHERE StagedExcelData_Key in 
        (SELECT StagedExcelData_Key             -- select distinct ess.SheetFormat
           FROM dExcelETL.StagedExcelData sed
          JOIN dExcelETL.ExcelSourceSheet ess
            ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
          WHERE ess.SheetFormat = 'ESPDealid');
    --*/ 

    -- Populate the adjusted data table
    INSERT INTO dExcelETL.AdjustedExcelData(StagedExcelData_Key,RowFormat,RowData)
    SELECT sed.StagedExcelData_Key,'',sed.RowData       -- SELECT *
      FROM dExcelETL.StagedExcelData sed
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
      LEFT JOIN dExcelETL.AdjustedExcelData aed
        ON aed.StagedExcelData_Key = sed.StagedExcelData_Key
      WHERE ess.SheetFormat = 'ESPDealid'
        AND aed.StagedExcelData_Key IS NULL;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' ESPDealid rows inserted to AdjustedExcelData';

    -- Set the row format for rows without one.
    UPDATE aed
       SET RowFormat.modify('insert <Deal /> into /')    -- select *
      FROM dExcelETL.AdjustedExcelData aed 
      JOIN dExcelETL.StagedExcelData sed
        on sed.StagedExcelData_Key = aed.StagedExcelData_Key
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
     WHERE ess.SheetFormat = 'ESPDealid'
       AND aed.RowFormat.exist('/Deal')=0
       AND ISNUMERIC(aed.RowData.value('Row[1]/F1[1]','nvarchar(10)')) = 1
       AND aed.RowData.exist('Row[1]/F2[1]')=1
       AND aed.RowData.exist('Row[1]/F3[1]')=1;
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows in AdjustedExcelData given RowFormat = SLDeals';
    
    --                                      Stage SLDeals data 

    -- TRUNCATE TABLE dExcelETL.SLDeals;       
    -- SELECT * FROM dExcelETL.SLDeals where UpdateDatetime < (getutcdate()-0.1);
    -- SELECT * INTO #T FROM dExcelETL.SLDeals;      DROP TABLE #T;
    -- UPDATE dExcelETL.SLDeals SET DealName = 'abc' WHERE DealName LIKE '1%'; -- Changing a name forces another change to set it back during merge
    
    WITH X AS (
       SELECT esf.SourceFileName
             ,esf.ModificationTimestamp
             ,ess.SheetName
             ,sed.RowNumber
             ,aed.RowData.value('Row[1]/F1[1]' ,'int'          ) DealID
             ,aed.RowData.value('Row[1]/F2[1]' ,'nvarchar(25)' ) DealCode
             ,aed.RowData.value('Row[1]/F3[1]' ,'nvarchar(100)') DealName
         FROM dExcelETL.AdjustedExcelData aed
          JOIN dExcelETL.StagedExcelData sed
            on sed.StagedExcelData_Key = aed.StagedExcelData_Key
         JOIN dExcelETL.ExcelSourceSheet ess
           ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
         JOIN dExcelETL.ExcelSourceFile esf
           ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
        WHERE ess.SheetFormat = 'ESPDealid'
          AND RowFormat.exist('Deal')=1
    ) --SELECT * FROM X
    MERGE INTO dExcelETL.SLDeals AS tgt
    USING X AS src
       ON src.SourceFileName  = tgt.SourceFileName
      AND src.SheetName       = tgt.SheetName
      AND src.DealID          = tgt.DealID
     WHEN NOT MATCHED BY SOURCE
     THEN DELETE
     WHEN MATCHED 
      AND (isnull(src.RowNumber     ,'0')  != isnull(tgt.RowNumber      ,'0')
        OR isnull(src.DealCode      ,'0')  != isnull(tgt.DealCode       ,'0')
        OR isnull(src.DealName      ,'0')  != isnull(tgt.DealName       ,'0'))
     THEN UPDATE SET
          tgt.UpdateDatetime  = src.ModificationTimestamp
         ,tgt.RowNumber       = src.RowNumber   
         ,tgt.DealCode        = src.DealCode
         ,tgt.DealName        = src.DealName    
     WHEN NOT MATCHED BY TARGET
     THEN INSERT (SourceFileName
                 ,SheetName
                 ,RowNumber
                 ,UpdateDatetime
                 ,DealID
                 ,DealCode
                 ,DealName )
          VALUES (src.SourceFileName
                 ,src.SheetName
                 ,src.RowNumber
                 ,src.ModificationTimestamp
                 ,src.DealID
                 ,src.DealCode
                 ,src.DealName     );
    PRINT CAST(@@ROWCOUNT AS nvarchar)+' Rows merged into SLDeals';

    -- SELECT * FROM dExcelETL.SLDeals;
    -- SELECT * FROM dExcelETL.SLDeals WHERE UpdateDatetime > (getutcdate()-0.1);
END
/*Test
GO
EXEC dExcelETL.ImportSLDeals;
--*/
﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/11
-- Description: Exposes the Tickers data from SLTables spreadsheet import
-- =============================================
CREATE VIEW [dExcelETL].[SLTickers] AS
    SELECT esf.SourceFileName
          ,ess.SheetName
          ,sed.RowNumber
          ,sed.RowData.value('Row[1]/F1[1]' ,'int') CompanyID
          ,sed.RowData.value('Row[1]/F2[1]' ,'nvarchar(128)')CompanyName
          ,sed.RowData.value('Row[1]/F3[1]' ,'nvarchar(20)')Ticker
          ,sed.RowData.value('Row[1]/F4[1]' ,'int') TickerID     -- select top 10 * 
      FROM dExcelETL.StagedExcelData sed
      JOIN dExcelETL.ExcelSourceSheet ess
        ON ess.ExcelSourceSheet_Key = sed.ExcelSourceSheet_Key
      JOIN dExcelETL.ExcelSourceFile esf
        ON esf.ExcelSourceFile_Key = ess.ExcelSourceFile_Key
     WHERE ess.SheetFormat = 'SLCanonicalTickers'
       AND ISNUMERIC(sed.RowData.value('Row[1]/F1[1]' ,'nvarchar(50)'))=1;
/*Test
GO
select * from dExcelETL.SLTickers;
--*/
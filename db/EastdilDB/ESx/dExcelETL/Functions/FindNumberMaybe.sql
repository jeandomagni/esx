﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/02/19
-- Description: Removes characters typically found in Eastdil Excel 'numeric' cells that prevent recognition as numbers
-- =============================================
CREATE FUNCTION [dExcelETL].FindNumberMaybe(@SourceString nvarchar(100)) RETURNS nvarchar(100) AS BEGIN
    DECLARE @T nvarchar(100) = @SourceString;

    SET @T = CASE WHEN PATINDEX('(%)',@T) = 1 THEN '-'+SUBSTRING(@T,2,CHARINDEX(')',@T)-2) ELSE @T END;
    SET @T = CASE WHEN CHARINDEX('-',@T) = 4 AND LEN(@T) = 6 THEN '01-'+STUFF(@T,5,0,'20') ELSE CASE WHEN CHARINDEX('-',@T) > 1 THEN SUBSTRING(@T,1,CHARINDEX('-',@T)-1) ELSE @T END END;
    SET @T = CASE WHEN CHARINDEX(' ',@T) > 0 THEN SUBSTRING(@T,1,CHARINDEX(' ',@T)-1) ELSE @T END;
    SET @T = CASE WHEN CHARINDEX(CHAR(10),@T) > 0 THEN SUBSTRING(@T,1,CHARINDEX(CHAR(10),@T)-1) ELSE @T END;
    SET @T = CASE WHEN CHARINDEX(CHAR(13),@T) > 0 THEN SUBSTRING(@T,1,CHARINDEX(CHAR(13),@T)-1) ELSE @T END;
    SET @T = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@T,'*',''),'%',''),',',''),'$',''),'~',''),'+',''),'..','.');
    SET @T = CASE WHEN @T = '-' THEN NULL ELSE @T END;
    RETURN @T;
END
/*Test
GO
SELECT dExcelETL.FindNumberMaybe('25.5% OCCUPANCY')       ;
SELECT dExcelETL.FindNumberMaybe('$250,000,000')          ;
SELECT dExcelETL.FindNumberMaybe('($250,000,000)')        ;
SELECT dExcelETL.FindNumberMaybe('~17 ACRES')             ;
SELECT dExcelETL.FindNumberMaybe('25%-35%')               ;
SELECT dExcelETL.FindNumberMaybe('10+%')                  ;
SELECT dExcelETL.FindNumberMaybe('n/a')                   ;
SELECT cast( dExcelETL.FindNumberMaybe('Sep-15') as date) ;
SELECT dExcelETL.FindNumberMaybe('-');
--*/
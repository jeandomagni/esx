﻿CREATE TABLE [dExcelETL].[ExcelSourceFile] (
    [ExcelSourceFile_Key]   INT             IDENTITY (1, 1) NOT NULL,
    [SourceFileName]        NVARCHAR (250)  NOT NULL,
    [ModificationTimestamp] DATETIME        NULL,
    [ImportTime]            DATETIME2 (0)   CONSTRAINT [DF_ExcelSourceFile_ImportTime] DEFAULT (getutcdate()) NOT NULL,
    [IsComplete]            BIT             CONSTRAINT [DF_ExcelSourceFile_IsComplete] DEFAULT ((0)) NOT NULL,
    [ContentType]           NVARCHAR (50)   CONSTRAINT [DF_ExcelSourceFile_ContentFormat] DEFAULT ('') NOT NULL,
    [ImportResult]          NVARCHAR (4000) CONSTRAINT [DF_ExcelSourceFile_ImportResult] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_ExcelSourceFile] PRIMARY KEY CLUSTERED ([ExcelSourceFile_Key] ASC)
);


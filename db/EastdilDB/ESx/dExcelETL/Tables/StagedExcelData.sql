﻿CREATE TABLE [dExcelETL].[StagedExcelData] (
    [StagedExcelData_Key]  INT IDENTITY (1, 1) NOT NULL,
    [ExcelSourceSheet_Key] INT NOT NULL,
    [RowNumber]            INT NOT NULL,
    [RowData]              XML NOT NULL,
    CONSTRAINT [PK_StagedExcelData] PRIMARY KEY CLUSTERED ([StagedExcelData_Key] ASC),
    CONSTRAINT [FK_StagedExcelData_ExcelSourceSheet] FOREIGN KEY ([ExcelSourceSheet_Key]) REFERENCES [dExcelETL].[ExcelSourceSheet] ([ExcelSourceSheet_Key])
);


GO
CREATE NONCLUSTERED INDEX [IX_StagedExcelData_ExcelSourceSheet]
    ON [dExcelETL].[StagedExcelData]([ExcelSourceSheet_Key] ASC);


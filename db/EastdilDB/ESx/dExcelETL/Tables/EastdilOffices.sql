﻿CREATE TABLE [dExcelETL].[EastdilOffices] (
    [EastdilOffices_Key] INT            IDENTITY (1, 1) NOT NULL,
    [SourceFileName]     NVARCHAR (250) NOT NULL,
    [SheetName]          NVARCHAR (50)  NOT NULL,
    [RowNumber]          INT            NOT NULL,
    [UpdateDatetime]     DATETIME       NOT NULL,
    [OfficeName]         NVARCHAR (50)  NOT NULL,
    [Address1]           NVARCHAR (100) NOT NULL,
    [Address2]           NVARCHAR (100) NOT NULL,
    [Address3]           NVARCHAR (100) NOT NULL,
    [Phone]              NVARCHAR (20)  NOT NULL,
    [Fax]                NVARCHAR (20)  NULL,
    [LookupValue]        AS             (((([Address1]+',')+[Address2])+',')+[Address3]),
    CONSTRAINT [PK_EastdilOffices] PRIMARY KEY CLUSTERED ([EastdilOffices_Key] ASC)
);


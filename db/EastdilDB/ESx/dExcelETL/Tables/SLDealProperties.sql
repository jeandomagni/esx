﻿CREATE TABLE [dExcelETL].[SLDealProperties] (
    [SLDealProperties_Key] INT            IDENTITY (1, 1) NOT NULL,
    [SourceFileName]       NVARCHAR (250) NOT NULL,
    [SheetName]            NVARCHAR (50)  NOT NULL,
    [RowNumber]            INT            NOT NULL,
    [UpdateDatetime]       DATETIME       NOT NULL,
    [DealID]               INT            NOT NULL,
    [DealCode]             NVARCHAR (25)  NOT NULL,
    [DealName]             NVARCHAR (100) NOT NULL,
    [PropertyID]           INT            NOT NULL,
    [Address]              NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_SLDealProperties] PRIMARY KEY CLUSTERED ([SLDealProperties_Key] ASC)
);


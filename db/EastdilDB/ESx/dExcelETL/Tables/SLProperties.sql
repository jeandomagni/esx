﻿CREATE TABLE [dExcelETL].[SLProperties] (
    [SLProperties_Key] INT            IDENTITY (1, 1) NOT NULL,
    [SourceFileName]   NVARCHAR (250) NOT NULL,
    [SheetName]        NVARCHAR (50)  NOT NULL,
    [RowNumber]        INT            NOT NULL,
    [UpdateDatetime]   DATETIME       NOT NULL,
    [PropertyID]       BIGINT         NOT NULL,
    [PropertyName]     NVARCHAR (100) NULL,
    [Address1]         NVARCHAR (100) NULL,
    [City]             NVARCHAR (100) NULL,
    [State]            NCHAR (2)      NULL,
    [Country]          NVARCHAR (50)  NOT NULL,
    [LookupValue]      AS             ((((((isnull([Address1],[PropertyName])+',')+isnull([City],''))+',')+ltrim(isnull([State],'')))+',')+isnull([Country],'')),
    CONSTRAINT [PK_SLProperties] PRIMARY KEY CLUSTERED ([SLProperties_Key] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=53 @ 19.94:1', @level0type = N'SCHEMA', @level0name = N'dExcelETL', @level1type = N'TABLE', @level1name = N'SLProperties', @level2type = N'COLUMN', @level2name = N'State';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=431 @ 2.45:1', @level0type = N'SCHEMA', @level0name = N'dExcelETL', @level1type = N'TABLE', @level1name = N'SLProperties', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1041 @ 1.02:1', @level0type = N'SCHEMA', @level0name = N'dExcelETL', @level1type = N'TABLE', @level1name = N'SLProperties', @level2type = N'COLUMN', @level2name = N'Address1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=493 @ 2.14:1', @level0type = N'SCHEMA', @level0name = N'dExcelETL', @level1type = N'TABLE', @level1name = N'SLProperties', @level2type = N'COLUMN', @level2name = N'PropertyName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dExcelETL', @level1type = N'TABLE', @level1name = N'SLProperties', @level2type = N'COLUMN', @level2name = N'PropertyID';


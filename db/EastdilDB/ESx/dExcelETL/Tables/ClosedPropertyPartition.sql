﻿CREATE TABLE [dExcelETL].[ClosedPropertyPartition] (
    [ClosedPropertyPartition_Key] INT            IDENTITY (1, 1) NOT NULL,
    [SourceFileName]              NVARCHAR (250) NOT NULL,
    [SheetName]                   NVARCHAR (50)  NOT NULL,
    [RowNumber]                   INT            NOT NULL,
    [UpdateDatetime]              DATETIME       NOT NULL,
    [PortfolioName]               NVARCHAR (200) NOT NULL,
    [DataStartRow]                INT            NOT NULL,
    [DataEndRow]                  INT            NOT NULL,
    CONSTRAINT [PK_ClosedPropertyPartition] PRIMARY KEY CLUSTERED ([ClosedPropertyPartition_Key] ASC)
);




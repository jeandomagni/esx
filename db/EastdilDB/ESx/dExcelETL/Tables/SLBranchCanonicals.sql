﻿CREATE TABLE [dExcelETL].[SLBranchCanonicals] (
    [SLBranchCanonicals_Key] INT            IDENTITY (1, 1) NOT NULL,
    [SourceFileName]         NVARCHAR (250) NOT NULL,
    [SheetName]              NVARCHAR (50)  NOT NULL,
    [RowNumber]              INT            NOT NULL,
    [UpdateDatetime]         DATETIME       NOT NULL,
    [CompanyID]              INT            NOT NULL,
    [CanonicalID]            INT            NOT NULL,
    [CompanyName]            NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_SLBranchCanonicals] PRIMARY KEY CLUSTERED ([SLBranchCanonicals_Key] ASC),
    CONSTRAINT [FK_SLBranchCanonicals_ESPBranchEntities1] FOREIGN KEY ([CompanyID]) REFERENCES [dExcelETL].[ESPBranchEntities] ([CompanyID]),
    CONSTRAINT [FK_SLBranchCanonicals_SLCanonicals1] FOREIGN KEY ([CanonicalID]) REFERENCES [dExcelETL].[SLCanonicals] ([CanonicalID])
);




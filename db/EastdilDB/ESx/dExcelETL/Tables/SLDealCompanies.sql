﻿CREATE TABLE [dExcelETL].[SLDealCompanies] (
    [DealID]         INT            NOT NULL,
    [SourceFileName] NVARCHAR (250) NOT NULL,
    [SheetName]      NVARCHAR (50)  NOT NULL,
    [RowNumber]      INT            NOT NULL,
    [UpdateDatetime] DATETIME       NOT NULL,
    [DealCode]       NVARCHAR (25)  NOT NULL,
    [DealName]       NVARCHAR (100) NOT NULL,
    [CompanyName]    NVARCHAR (100) NOT NULL,
    [CompanyID]      INT            NOT NULL,
    [CompanyRole]    NVARCHAR (10)  NOT NULL,
    [RoleID]         INT            NOT NULL,
    CONSTRAINT [PK_SLDealCompanies] PRIMARY KEY CLUSTERED ([DealID] ASC, [CompanyID] ASC, [RoleID] ASC)
);


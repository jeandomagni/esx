﻿CREATE TABLE [dExcelETL].[SLCanonicals] (
    [CanonicalID]    INT            NOT NULL,
    [SourceFileName] NVARCHAR (250) NOT NULL,
    [SheetName]      NVARCHAR (50)  NOT NULL,
    [RowNumber]      INT            NOT NULL,
    [UpdateDatetime] DATETIME       NOT NULL,
    [CanonicalName]  NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_SLCanonicals] PRIMARY KEY CLUSTERED ([CanonicalID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dExcelETL', @level1type = N'TABLE', @level1name = N'SLCanonicals', @level2type = N'COLUMN', @level2name = N'CanonicalName';


﻿CREATE TABLE [dExcelETL].[FinancingDetail] (
    [FinancingDetail_Key]    INT             IDENTITY (1, 1) NOT NULL,
    [FinancingPartition_Key] INT             NOT NULL,
    [EntryNumber]            INT             NOT NULL,
    [UpdateDatetime]         DATETIME        NOT NULL,
    [PreTable]               NVARCHAR (50)   NULL,
    [EsTeam]                 NVARCHAR (50)   NULL,
    [DealCode]               NVARCHAR (50)   NULL,
    [Client]                 NVARCHAR (100)  NULL,
    [Lender]                 NVARCHAR (200)  NULL,
    [AssetName]              NVARCHAR (100)  NULL,
    [Address]                NVARCHAR (100)  NULL,
    [AssetType]              NVARCHAR (50)   NULL,
    [AssetTypeII]            NVARCHAR (50)   NULL,
    [NumProperties]          INT             NULL,
    [Occupancy]              NUMERIC (4, 1)  NULL,
    [SfUnits]                NUMERIC (18, 2) NULL,
    [Location]               NVARCHAR (100)  NULL,
    [CloseDate]              DATE            NULL,
    [Status]                 NVARCHAR (50)   NULL,
    [LoanAmount]             MONEY           NULL,
    [InPlaceYield]           NUMERIC (18, 4) NULL,
    [FixedRate]              NUMERIC (7, 4)  NULL,
    [TransactionType]        NVARCHAR (50)   NULL,
    [Spread]                 NVARCHAR (100)  NULL,
    [Leverage]               NUMERIC (4, 1)  NULL,
    [Structure]              NVARCHAR (500)  NULL,
    [PostTable]              NVARCHAR (50)   NULL,
    CONSTRAINT [PK_FinancingDetail] PRIMARY KEY CLUSTERED ([FinancingDetail_Key] ASC),
    CONSTRAINT [FK_FinancingDetail_FinancingPartition] FOREIGN KEY ([FinancingPartition_Key]) REFERENCES [dExcelETL].[FinancingPartition] ([FinancingPartition_Key])
);




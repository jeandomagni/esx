﻿CREATE TABLE [dExcelETL].[ClosedPropertyDetail] (
    [ClosedProperty_Key]          INT             IDENTITY (1, 1) NOT NULL,
    [ClosedPropertyPartition_Key] INT             NOT NULL,
    [EntryNumber]                 INT             NOT NULL,
    [UpdateDatetime]              DATETIME        NOT NULL,
    [PreTable]                    NVARCHAR (50)   NULL,
    [ClosingDate]                 DATE            NULL,
    [DealCode]                    NVARCHAR (50)   NULL,
    [EsTeam]                      NVARCHAR (50)   NULL,
    [Seller]                      NVARCHAR (50)   NULL,
    [Buyer]                       NVARCHAR (50)   NULL,
    [AssetName]                   NVARCHAR (100)  NULL,
    [AssetType]                   NVARCHAR (50)   NULL,
    [Location]                    NVARCHAR (50)   NULL,
    [Location2]                   NVARCHAR (50)   NULL,
    [Occupancy]                   NUMERIC (4, 1)  NULL,
    [SfUnits]                     NUMERIC (18, 2) NULL,
    [SalePrice]                   MONEY           NULL,
    [CapRateInPlace]              NUMERIC (7, 1)  NULL,
    [CapRate1Year]                NUMERIC (4, 1)  NULL,
    [Irr10Year]                   NUMERIC (7, 1)  NULL,
    [Coc5Year]                    NUMERIC (4, 1)  NULL,
    [NumOffers]                   NVARCHAR (50)   NULL,
    [Comment]                     NVARCHAR (500)  NULL,
    [Address]                     NVARCHAR (200)  NULL,
    CONSTRAINT [PK_ClosedPropertyDetail] PRIMARY KEY CLUSTERED ([ClosedProperty_Key] ASC),
    CONSTRAINT [FK_ClosedPropertyDetail_ClosedPropertyPartition] FOREIGN KEY ([ClosedPropertyPartition_Key]) REFERENCES [dExcelETL].[ClosedPropertyPartition] ([ClosedPropertyPartition_Key])
);




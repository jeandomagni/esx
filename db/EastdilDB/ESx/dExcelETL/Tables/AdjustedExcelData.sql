﻿CREATE TABLE [dExcelETL].[AdjustedExcelData] (
    [AdjustedExcelData_Key] INT IDENTITY (1, 1) NOT NULL,
    [StagedExcelData_Key]   INT NOT NULL,
    [RowFormat]             XML NOT NULL,
    [RowData]               XML NOT NULL,
    CONSTRAINT [PK_AdjustedExcelData] PRIMARY KEY CLUSTERED ([AdjustedExcelData_Key] ASC),
    CONSTRAINT [FK_AdjustedExcelData_StagedExcelData] FOREIGN KEY ([StagedExcelData_Key]) REFERENCES [dExcelETL].[StagedExcelData] ([StagedExcelData_Key])
);


GO
CREATE NONCLUSTERED INDEX [IX_AdjustedExcelData_StagedExcelData]
    ON [dExcelETL].[AdjustedExcelData]([StagedExcelData_Key] ASC);


﻿CREATE TABLE [dExcelETL].[ClosedProperty] (
    [ClosedProperty_Key] INT             IDENTITY (1, 1) NOT NULL,
    [SourceFileName]     NVARCHAR (250)  NOT NULL,
    [SheetName]          NVARCHAR (50)   NOT NULL,
    [RowNumber]          INT             NOT NULL,
    [UpdateDatetime]     DATETIME        NOT NULL,
    [ClosingDate]        DATE            NOT NULL,
    [DealCode]           NVARCHAR (50)   NULL,
    [EsTeam]             NVARCHAR (50)   NULL,
    [Seller]             NVARCHAR (50)   NULL,
    [Buyer]              NVARCHAR (50)   NULL,
    [AssetName]          NVARCHAR (100)  NULL,
    [AssetType]          NVARCHAR (50)   NULL,
    [Location]           NVARCHAR (50)   NULL,
    [Occupancy]          NUMERIC (4, 1)  NULL,
    [SfUnits]            NUMERIC (18, 2) NULL,
    [SalePrice]          MONEY           NULL,
    [CapRateInPlace]     NUMERIC (4, 1)  NULL,
    [CapRate1Year]       NUMERIC (4, 1)  NULL,
    [Irr10Year]          NUMERIC (4, 1)  NULL,
    [Coc5Year]           NUMERIC (4, 1)  NULL,
    [NumOffers]          NVARCHAR (50)   NULL,
    [Comments]           NVARCHAR (500)  NULL,
    [Address]            NVARCHAR (200)  NULL,
    CONSTRAINT [PK_ClosedProperty] PRIMARY KEY CLUSTERED ([ClosedProperty_Key] ASC)
);






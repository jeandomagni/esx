﻿CREATE TABLE [dExcelETL].[ClosedTransaction] (
    [ClosedTransaction_Key] INT             IDENTITY (1, 1) NOT NULL,
    [SourceFileName]        NVARCHAR (250)  NOT NULL,
    [SheetName]             NVARCHAR (50)   NOT NULL,
    [RowType]               NVARCHAR (50)   NULL,
    [RowNumber]             INT             NOT NULL,
    [UpdateDatetime]        DATETIME        NOT NULL,
    [OfficeName]            NVARCHAR (50)   NULL,
    [DealCode]              NVARCHAR (50)   NULL,
    [ClientNames]           NVARCHAR (200)  NOT NULL,
    [BuyerLenderNames]      NVARCHAR (200)  NULL,
    [TransactionType]       NVARCHAR (50)   NOT NULL,
    [Description]           NVARCHAR (200)  NULL,
    [AssetType]             NVARCHAR (50)   NULL,
    [Location]              NVARCHAR (50)   NULL,
    [SfUnits]               NVARCHAR (50)   NULL,
    [CloseDate]             DATE            NULL,
    [DealAmount]            NUMERIC (18, 2) NULL,
    [ZipCode]               NVARCHAR (50)   NULL,
    CONSTRAINT [PK_ClosedTransaction] PRIMARY KEY CLUSTERED ([ClosedTransaction_Key] ASC)
);






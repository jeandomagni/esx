﻿CREATE TABLE [dExcelETL].[SLCanonicalAliases] (
    [SLCanonicalAliases_Key] INT            IDENTITY (1, 1) NOT NULL,
    [SourceFileName]         NVARCHAR (250) NOT NULL,
    [SheetName]              NVARCHAR (50)  NOT NULL,
    [RowNumber]              INT            NOT NULL,
    [UpdateDatetime]         DATETIME       NOT NULL,
    [CanonicalID]            INT            NOT NULL,
    [AliasName]              NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_SLCanonicalAliases] PRIMARY KEY CLUSTERED ([SLCanonicalAliases_Key] ASC),
    CONSTRAINT [FK_SLCanonicalAliases_SLCanonicals1] FOREIGN KEY ([CanonicalID]) REFERENCES [dExcelETL].[SLCanonicals] ([CanonicalID])
);




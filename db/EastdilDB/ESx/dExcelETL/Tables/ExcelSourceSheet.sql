﻿CREATE TABLE [dExcelETL].[ExcelSourceSheet] (
    [ExcelSourceSheet_Key] INT           IDENTITY (1, 1) NOT NULL,
    [ExcelSourceFile_Key]  INT           NOT NULL,
    [SheetName]            NVARCHAR (50) NOT NULL,
    [ColumnCount]          TINYINT       CONSTRAINT [DF_ExcelSourceSheet_ColumnCount] DEFAULT ((0)) NOT NULL,
    [IsComplete]           BIT           CONSTRAINT [DF_ExcelSourceSheet_IsComplete] DEFAULT ((0)) NOT NULL,
    [SheetFormat]          NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ExcelSourceSheet] PRIMARY KEY CLUSTERED ([ExcelSourceSheet_Key] ASC),
    CONSTRAINT [FK_ExcelSourceSheet_ExcelSourceFile] FOREIGN KEY ([ExcelSourceFile_Key]) REFERENCES [dExcelETL].[ExcelSourceFile] ([ExcelSourceFile_Key])
);


GO
CREATE NONCLUSTERED INDEX [IX_ExcelSourceSheet_ExcelSourceFile]
    ON [dExcelETL].[ExcelSourceSheet]([ExcelSourceFile_Key] ASC);


﻿CREATE TABLE [dExcelETL].[FinancingTransaction] (
    [FinancingTransaction_Key] INT             IDENTITY (1, 1) NOT NULL,
    [SourceFileName]           NVARCHAR (250)  NOT NULL,
    [SheetName]                NVARCHAR (50)   NOT NULL,
    [RowNumber]                INT             NOT NULL,
    [UpdateDatetime]           DATETIME        NOT NULL,
    [ES]                       NVARCHAR (50)   NULL,
    [EsTeam]                   NVARCHAR (50)   NULL,
    [DealCode]                 NVARCHAR (50)   NULL,
    [Client]                   NVARCHAR (100)  NULL,
    [Lender]                   NVARCHAR (250)  NULL,
    [LenderType]               NVARCHAR (50)   NULL,
    [LenderTypeII]             NVARCHAR (50)   NULL,
    [AssetName]                NVARCHAR (100)  NULL,
    [Address]                  NVARCHAR (100)  NULL,
    [AssetType]                NVARCHAR (50)   NULL,
    [AssetTypeII]              NVARCHAR (50)   NULL,
    [NumProperties]            SMALLINT        NULL,
    [Occupancy]                NUMERIC (4, 1)  NULL,
    [SfUnits]                  NUMERIC (18, 2) NULL,
    [Location]                 NVARCHAR (100)  NULL,
    [CloseDate]                DATE            NULL,
    [Status]                   NVARCHAR (50)   NULL,
    [LoanAmount]               MONEY           NULL,
    [InPlaceYield]             NUMERIC (7, 4)  NULL,
    [FixedRate]                NUMERIC (7, 4)  NULL,
    [TransactionType]          NVARCHAR (50)   NULL,
    [Spread]                   NVARCHAR (100)  NULL,
    [Leverage]                 NUMERIC (4, 1)  NULL,
    [Structure]                NVARCHAR (100)  NULL,
    CONSTRAINT [PK_FinancingTransaction] PRIMARY KEY CLUSTERED ([FinancingTransaction_Key] ASC)
);






﻿CREATE TABLE [dExcelETL].[SLDeals] (
    [DealID]         INT            NOT NULL,
    [SourceFileName] NVARCHAR (250) NOT NULL,
    [SheetName]      NVARCHAR (50)  NOT NULL,
    [RowNumber]      INT            NOT NULL,
    [UpdateDatetime] DATETIME       NOT NULL,
    [DealCode]       NVARCHAR (25)  NOT NULL,
    [DealName]       NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_SLDeals] PRIMARY KEY CLUSTERED ([DealID] ASC)
);


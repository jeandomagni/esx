﻿CREATE TABLE [dExcelETL].[ESPBranchEntities] (
    [CompanyID]      INT            NOT NULL,
    [SourceFileName] NVARCHAR (250) NOT NULL,
    [SheetName]      NVARCHAR (50)  NOT NULL,
    [RowNumber]      INT            NOT NULL,
    [UpdateDatetime] DATETIME       NOT NULL,
    [CompanyName]    NVARCHAR (200) NOT NULL,
    [DBAName]        NVARCHAR (200) NULL,
    [Active]         BIT            NOT NULL,
    [LastUpdateDate] DATE           NOT NULL,
    CONSTRAINT [PK_ESPBranchEntities] PRIMARY KEY CLUSTERED ([CompanyID] ASC)
);




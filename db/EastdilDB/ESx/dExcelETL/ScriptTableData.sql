-- This script (re)creates SQLCMD scripts *in this project* that contain insert statements to recreate table content.
-- That is, it scripts out the data currently in local database tables for (re)loading a table's content.
-- Due to limitations in SQLCMD functionality the path to the scripts must be hard coded.

-- THIS SCRIPT ONLY WORKS *CORRECTLY* when run directly from sqlcmd.exe.
-- Execution succeeds, but results are incorrect when run from SSMS or VS in SQLCMD MODE.
-- Either tool can be used for basic development and testing but output can only be verified by using the commands below.
/*
-- These commands run THIS (you're looking at it now) script in a way that executes properly in SSMS/VS.
-- Do not uncomment them (recursion!!!).  Select them and execute with [Shift][Control]E
-- Don't forget to save the file content.  It executes what's on disk not what's on your screen!
:SETVAR SolutionPath C:\ESX\db\EastdilDB\
:SETVAR ScriptPath .\ESx\dExcelETL\
:!! SQLCMD -d ESxDEV -u -W -E -i "$(SolutionPath)$(ScriptPath)ScriptTableData.sql"
--*/

------------------------------------------------------------------------------------------------
-- When adding or deleting scripts be sure to update Script.PostDeployment1.sql so it knows 
-- what scripts are available and what order they must be executed in.
------------------------------------------------------------------------------------------------
GO
--/*
PRINT 'Script generation starting'
:SETVAR SQLCMDHEADERS -1
:SETVAR SQLCMDCOLWIDTH 65535
:SETVAR SQLCMDMAXFIXEDTYPEWIDTH 0
:SETVAR SQLCMDMAXVARTYPEWIDTH 0
:SETVAR SolutionPath C:\ESX\db\EastdilDB\
:SETVAR ScriptPath ".\ESx\xESxApp\EtlData\"
:SETVAR Schema dExcelETL
PRINT 'Solution Script Path = $(SolutionPath)$(ScriptPath)'
SET nocount ON;
GO
:SETVAR Table ExcelSourceFile
:OUT $(SolutionPath)$(ScriptPath)$(Table).sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1;
GO
:SETVAR Table ExcelSourceSheet
:OUT $(SolutionPath)$(ScriptPath)$(Table).sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1;
GO
:SETVAR Table StagedExcelData
:OUT $(SolutionPath)$(ScriptPath)$(Table)1.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 0 AND 5000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)2.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 5001 AND 10000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)3.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 10001 AND 15000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)4.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 15001 AND 20000';
GO	
:OUT $(SolutionPath)$(ScriptPath)$(Table)5.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 20001 AND 25000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)6.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 25001 AND 30000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)7.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 30001 AND 35000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)8.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 35001 AND 40000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)9.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 40001 AND 45000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)10.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 45001 AND 50000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)11.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 50001 AND 55000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)12.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 55001 AND 60000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)13.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 60001 AND 65000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)14.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 65001 AND 70000';
GO	
:OUT $(SolutionPath)$(ScriptPath)$(Table)15.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 70001 AND 75000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)16.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 75001 AND 80000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)17.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 80001 AND 85000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)18.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key BETWEEN 85001 AND 90000';
GO
:OUT $(SolutionPath)$(ScriptPath)$(Table)19.sql
PRINT '-- Script Generated ON '+@@SERVERNAME;
EXEC [Global].InsertGenerator @pTable='$(Schema).$(Table)',@pOrderBy='$(Table)_Key',@pAddPrint=1,@pMaxRows=10000,@pWhere='$(Table)_Key >= 90001';
GO
:OUT STDOUT
PRINT 'Scripting Complete'
GO
--*/
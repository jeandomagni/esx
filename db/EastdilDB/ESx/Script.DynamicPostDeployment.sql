 /*
     DO NOT MANUALLY MODIFY THIS SCRIPT.                                               
                                                                                       
     It is overwritten during build.                                                   
     Content IS based on the Build configuration (NoWipe, WipeSeed, WipeETL, WipeEtlSeed...) 
                                                                                       
     Modify BUILD.DeploymentScripts.sql to effect changes in executable content.         
 */
PRINT 'PostDeployment script starting at'+CAST(GETDATE() AS nvarchar)+' with Configuration = WipeSeed';
GO
:r .\xESxApp\LookupData\AssetType.sql                  
:r .\xESxApp\LookupData\DealValuationRiskProfile.sql   
:r .\xESxApp\LookupData\AssetUsageType.sql             
:r .\xESxApp\LookupData\BidStatus.sql                  
:r .\xESxApp\LookupData\AssetContactType.sql           
:r .\xESxApp\LookupData\DealContactType.sql            
:r .\xESxApp\LookupData\Country.sql                    
:r .\xESxApp\LookupData\DealValuationType.sql          
:r .\xESxApp\LookupData\DealStatus.sql                 
:r .\xESxApp\LookupData\DealTeamRole.sql               
GO
:r .\xESxApp\LookupData\DealType.sql                   
:r .\xESxApp\LookupData\ElectronicAddressUsageType.sql 
:r .\xESxApp\LookupData\LegalEntity.sql                
:r .\xESxApp\LookupData\LoanBenchmarkType.sql          
:r .\xESxApp\LookupData\LoanCashMgmtType.sql           
:r .\xESxApp\LookupData\LoanLenderType.sql             
:r .\xESxApp\LookupData\LoanPaymentType.sql            
:r .\xESxApp\LookupData\LoanRateType.sql               
:r .\xESxApp\LookupData\LoanSourceType.sql             
:r .\xESxApp\LookupData\LoanTermDateType.sql           
GO
:r .\xESxApp\LookupData\LogSeverityType.sql            
:r .\xESxApp\LookupData\MarketType.sql                 
:r .\xESxApp\LookupData\NoticeSeverityType.sql         
:r .\xESxApp\LookupData\PhysicalAddressType.sql        
:r .\xESxApp\LookupData\PreferenceType.sql             
:r .\xESxApp\LookupData\StateProvince.sql              
:r .\xESxApp\LookupData\DealParticipationType.sql      
:r .\xESxApp\LookupData\MarketingListStatusType.sql    
:r .\xESxApp\LookupData\MarketingListActionType.sql    
:r .\xESxApp\LookupData\Feature.sql                    
GO
:r .\xESxApp\LookupData\Role.sql                       
GO
PRINT 'Lookup data finished at'+CAST(GETDATE() AS nvarchar);
GO
:r .\xESxApp\SeedData\Company.sql                        
:r .\xESxApp\SeedData\Contact.sql                        
:r .\xESxApp\SeedData\Park.sql                           
:r .\xESxApp\SeedData\CompanyAlias.sql                   
:r .\xESxApp\SeedData\CompanyDealTypeInterest.sql        
:r .\xESxApp\SeedData\CompanyHierarchy.sql               
:r .\xESxApp\SeedData\PhysicalAddress.sql                
:r .\xESxApp\SeedData\Office.sql                         
GO
:r .\xESxApp\SeedData\Deal.sql                           
:r .\xESxApp\SeedData\DealStatusLog.sql                  
:r .\xESxApp\SeedData\ElectronicAddress.sql              
:r .\xESxApp\SeedData\Market.sql                         
:r .\xESxApp\SeedData\MarketStates.sql                   
:r .\xESxApp\SeedData\MarketMSAs.sql                     
:r .\xESxApp\SeedData\User.sql                           
:r .\xESxApp\SeedData\Asset.sql                          
:r .\xESxApp\SeedData\DealValuation.sql                  
:r .\xESxApp\SeedData\Company_ElectronicAddress.sql      
GO
:r .\xESxApp\SeedData\CompanyMarket.sql                  
:r .\xESxApp\SeedData\Contact_ElectronicAddress.sql      
:r .\xESxApp\SeedData\ContactMarket.sql                  
:r .\xESxApp\SeedData\DealCompany.sql                    
:r .\xESxApp\SeedData\DealContact.sql                    
:r .\xESxApp\SeedData\DealValuation.sql                  
:r .\xESxApp\SeedData\DealTeam.sql                       
:r .\xESxApp\SeedData\EastdilOffice.sql                  
:r .\xESxApp\SeedData\MarketHierarchy.sql                
:r .\xESxApp\SeedData\TagUsage.sql                       
GO
:r .\xESxApp\SeedData\UserPreference.sql                 
:r .\xESxApp\SeedData\AssetContact.sql                   
:r .\xESxApp\SeedData\AssetLoanBorrower.sql              
:r .\xESxApp\SeedData\AssetLoanTerms.sql                 
:r .\xESxApp\SeedData\AssetName.sql                      
:r .\xESxApp\SeedData\AssetUsage.sql                     
:r .\xESxApp\SeedData\AssetValue.sql                     
:r .\xESxApp\SeedData\CompanyAsset.sql                   
:r .\xESxApp\SeedData\ContactOffice.sql                  
:r .\xESxApp\SeedData\DealAsset.sql                      
GO
:r .\xESxApp\SeedData\DealValuationAsset.sql             
:r .\xESxApp\SeedData\DealValuationCompany.sql           
:r .\xESxApp\SeedData\DealValuationDocument.sql          
:r .\xESxApp\SeedData\DealContactStatus.sql              
:r .\xESxApp\SeedData\Office_ElectronicAddress.sql       
:r .\xESxApp\SeedData\OfficeMarket.sql                   
:r .\xESxApp\SeedData\AssetLoanProperty.sql              
:r .\xESxApp\SeedData\AssetLoanTermDates.sql             
:r .\xESxApp\SeedData\DealValuationAsset.sql             
GO
:r .\xESxApp\SeedData\Mention.sql                        
--:r .\xESxApp\SeedData\CacheAccessToken.sql             
:r .\xESxApp\SeedData\Comment.sql                        
:r .\xESxApp\SeedData\EastdilOfficeMentionUsage.sql      
:r .\xESxApp\SeedData\Notice.sql                         
--:r .\xESxApp\SeedData\CacheSessionQuery.sql            
:r .\xESxApp\SeedData\Comment_Mention.sql                
:r .\xESxApp\SeedData\Comment_Tag.sql                    
:r .\xESxApp\SeedData\Notice_Mention.sql                 
:r .\xESxApp\SeedData\NoticeRecipient.sql                
GO
:r .\xESxApp\SeedData\MarketingList.sql                  
:r .\xESxApp\SeedData\MarketingListContact.sql           
:r .\xESxApp\SeedData\MarketingListContactStatusLog.sql  
:r .\xESxApp\SeedData\FeatureRole.sql                    
GO
PRINT 'Seed data finished at'+CAST(GETDATE() AS nvarchar);
GO
EXEC xESxApp.AppDev_RebuildAllMentions;
GO
PRINT 'PostDeployment script finished at'+CAST(GETDATE() AS nvarchar);
GO

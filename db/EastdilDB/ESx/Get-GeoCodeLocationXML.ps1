﻿<#
  .SYNOPSIS
    Updates tables requiring address validation whose addresses have not been validated.
  .Description
    This script runs xESxETL.UpdateAddressGeoData on all rows returned by view xESxETL.AddressesNeedingGeoData
    where GeocodeResponse data returns from http://maps.googleapis.com/maps/api/geocode/xml
  .Notes
    Author: Rick Bielawski
#>

$ErrorActionPreference = 'Stop'
if (!(Get-Module SqlPs)){Import-Module SqlPs}    #This generates a warning that Microsoft caused and won't fix.


function Invoke-DbCommand ([string]$Command) {
    Invoke-Sqlcmd -ServerInstance . -Database EsxDEV -Query $Command
}
#Invoke-DbCommand 'Select 1,2,3'


function Get-LocationXML([string]$Address) 
{ 
    $Address = $Address.Replace('	',',')
    $Key =

    if ($Key)
        {$result = Invoke-WebRequest "https://maps.googleapis.com/maps/api/geocode/xml?address=$Address&key=$Key"}
    else 
        {$result = Invoke-WebRequest "http://maps.googleapis.com/maps/api/geocode/xml?address=$Address"}
    Start-Sleep -Milliseconds 100
    if ($result.StatusCode -eq 200)
        {([xml]$result.Content).GeocodeResponse}
    else {if ($result.StatusCode -eq $null)
              {[xml]"<Error>$($Error)</Error>"}
          else
              {[xml]"<Error>$($result.StatusDescription)</Error>"}}
}
#Get-LocationXML '1 Hampton Ct	Carlisle	PA	United States'
#Get-LocationXML '1 Finsbury Ave	London		UK'


function Update-GeoLocationFromView {
    $UpdateList = Invoke-DbCommand "SELECT top 2500 LookupValue FROM xESxETL.AddressesNeedingGeoData;"

    if ($UpdateList -eq $null) {
        Write-Output "NO ROWS NEED UPDATING"
    }
    else {
        trap{if ($t) {$t|fl}
             $Error[0]
             continue
            }
        $UpdateList `
            |%{ $t= Get-LocationXML $_.LookupValue.Replace('#',' ')
                if ($t.status -in 'OK','ZERO_RESULTS') {
                    Invoke-DbCommand ("EXEC xESxETL.UpdateAddressGeoData"+
                                      " @LookupValue='$($_.LookupValue.Replace('''',''''''))'"+
                                      ",@GeoData='$($t.outerxml.Replace('''',''''''))'")
                    Write-Output $t.result
                }
                else {
                    throw 
                }
              }
    }
}

#Invoke-DbCommand 'EXEC xESxETL.LoadAddressesToValidate;'         #<- Run commented out statements from SSMS to see counts and avoid timeout.
Update-GeoLocationFromView
#Invoke-DbCommand 'EXEC xESxETL.ExtractValidatedAddressData;'
"Extract Complete"
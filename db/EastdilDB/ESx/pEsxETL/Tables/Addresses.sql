﻿CREATE TABLE [pESxETL].[Addresses] (
    [AddressID]          BIGINT            NOT NULL,
    [Description]        NVARCHAR (100)    NULL,
    [TypeCode]           NVARCHAR (4000)   NOT NULL,
    [Address1]           NVARCHAR (100)    NULL,
    [Address2]           NVARCHAR (100)    NULL,
    [City]               NVARCHAR (50)     NULL,
    [State]              NVARCHAR (50)     NULL,
    [PostalCode]         NVARCHAR (50)     NULL,
    [Country]            NVARCHAR (50)     NOT NULL,
    [IsPrimary]          BIT               NULL,
    [IsMailing]          BIT               NULL,
    [Salutation]         NVARCHAR (100)    NULL,
    [Routing]            NVARCHAR (100)    NULL,
    [Address3]           NVARCHAR (100)    NULL,
    [Address4]           NVARCHAR (100)    NULL,
    [Timezone]           NVARCHAR (100)    NULL,
    [Source]             NVARCHAR (50)     NULL,
    [Confidence]         SMALLINT          NULL,
    [UpdateDatetime]     DATETIME          NULL,
    [AddedBy]            NVARCHAR (100)    NULL,
    [UpdatedBy]          NVARCHAR (100)    NULL,
    [LookupValue]        NVARCHAR (303)    NULL,
    [ValidationData]     XML               NULL,
    [ValidationDatetime] DATETIME          NULL,
    [ValidatedAddress]   NVARCHAR (200)    NULL,
    [Coordinates]        [sys].[geography] NULL,
    [AddressStatus]      VARCHAR (20)      NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_Address_LookupValue]
    ON [pESxETL].[Addresses]([LookupValue] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_Address_AddressID]
    ON [pESxETL].[Addresses]([AddressID] ASC);


﻿CREATE TABLE [pESxETL].[Companies] (
    [CorrectedName]         NVARCHAR (200)  NULL,
    [ParentName]            NVARCHAR (200)  NULL,
    [ParentID]              BIGINT          NULL,
    [ParentParentName]      NVARCHAR (200)  NULL,
    [ParentParentID]        BIGINT          NULL,
    [DuplicateOfID]         BIGINT          NULL,
    [CompanyID]             BIGINT          NOT NULL,
    [CompanyName]           NVARCHAR (128)  NULL,
    [CompanyLegalName]      NVARCHAR (200)  NULL,
    [Division]              NVARCHAR (100)  NULL,
    [AddressID]             BIGINT          NULL,
    [ShippingID]            BIGINT          NULL,
    [TickerSymbol]          NVARCHAR (50)   NULL,
    [CIBOSCode]             NVARCHAR (50)   NULL,
    [MainPhone]             NVARCHAR (50)   NULL,
    [Fax]                   NVARCHAR (50)   NULL,
    [TollFreeNumber]        NVARCHAR (50)   NULL,
    [AlternateCompanyPhone] NVARCHAR (50)   NULL,
    [CompanyEmail]          NVARCHAR (128)  NULL,
    [Website]               NVARCHAR (128)  NULL,
    [Active]                BIT             NULL,
    [ReasonInactive]        NVARCHAR (1000) NULL,
    [LastUpdate]            DATETIME        NULL,
    CONSTRAINT [pk_pESxETL_Companies_CompanyID] PRIMARY KEY CLUSTERED ([CompanyID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ixf_pESxETL_Companies_ParentParentID]
    ON [pESxETL].[Companies]([ParentParentID] ASC);


GO
CREATE NONCLUSTERED INDEX [ixf_pESxETL_Companies_DuplicateOfID]
    ON [pESxETL].[Companies]([DuplicateOfID] ASC) WHERE ([DuplicateOfID] IS NOT NULL);


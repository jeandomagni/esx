﻿CREATE TABLE [pESxETL].[CompanyMap] (
    [CompanyID]   BIGINT NULL,
    [Company_Key] INT    NULL
);


GO
CREATE CLUSTERED INDEX [ix_pESxETL_CompanyMap_CompanyID]
    ON [pESxETL].[CompanyMap]([CompanyID] ASC);


﻿CREATE TABLE [pESxETL].[Office] (
    [LookupValue] NVARCHAR (303) NULL,
    [OfficeName]  NVARCHAR (200) NULL,
    [Company_Key] INT            NULL,
    [CompanyID]   BIGINT         NULL,
    [CreatedDate] DATETIME       NULL,
    [CreatedBy]   NVARCHAR (128) NULL,
    [UpdatedDate] DATETIME       NULL,
    [UpdatedBy]   NVARCHAR (128) NULL
);


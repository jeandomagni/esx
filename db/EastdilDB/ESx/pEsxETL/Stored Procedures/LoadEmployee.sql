﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/24
-- Description: Populates dESx.Contact and immediately related tables with Employee data
-- =============================================
CREATE PROCEDURE [pESxETL].[LoadEmployee]
AS
    -- Employees require an EastdilOffice and offices require an address
    WITH x AS (
        SELECT UpdateDatetime
              ,OfficeName
              ,Address1
              ,Address2
              ,Address3     
              -- I can't parse the country from the freeform text but a key is required so put one that is correct most of the time
              ,(SELECT Country_Key FROM dESx.Country WHERE CountryName = 'United States') Country_Key
              ,ValidationDatetime
              ,Coordinates
          FROM rESxStage.EastdilOffices
    )
     MERGE dESx.PhysicalAddress tgt
     USING x src
        ON 1=0
      WHEN NOT MATCHED BY TARGET THEN
    INSERT(PhysicalAddressType_Code
          ,Address1
          ,Address2
          ,Address3
          ,Address4
          ,City
          ,StateProvidence_Code
          ,Postal_Code
          ,Country_Key
          ,AddressValidationDate
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy
          ,Coordinates
          ,Latitude
          ,Longitude    )
    VALUES('Primary'
          ,ISNULL(src.Address1      ,'')
          ,NULLIF(src.Address2      ,'')
          ,NULLIF(src.Address3      ,'')
          ,''
          ,''
          ,NULL
          ,''
          ,src.Country_Key
          ,src.ValidationDatetime
          ,GETUTCDATE()
          ,'ETL'
          ,ISNULL(src.UpdateDatetime,'19000101')
          ,'ETL'
          ,src.Coordinates
          ,src.Coordinates.Lat
          ,src.Coordinates.Long )
    OUTPUT Inserted.PhysicalAddress_Key,src.OfficeName
      INTO pESxETL.OfficePhysicalAddressMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' Eastdil offices added to PhysicalAddress';  -- select * from pESxETL.OfficePhysicalAddressMap;

    -- Insert the offices for employees 
    WITH x AS (
        SELECT DISTINCT E.Office
          FROM rESxStage.Employee E
    )
    INSERT INTO dESx.EastdilOffice (
           EastdilOfficeName
          ,PhysicalAddress_Key
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy    )
    SELECT x.Office
          ,OPhAMa.PhysicalAddress_Key
          ,GETUTCDATE()
          ,'ETL'
          ,GETUTCDATE()
          ,'ETL'
      FROM x
      LEFT JOIN pESxETL.OfficePhysicalAddressMap OPhAMa
        ON OPhAMa.OfficeName = x.Office
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to EastdilOffice';    -- select * from dESx.EastdilOffice;

    -- TODO: Once Eastdil Offices are just Offices, add their phone numbers.  They are in rESxStage.EastdilOffices.

    -- Employee data is similar but not identical to contacts.  It has a different source and usage but goes into the same table.
    WITH x AS (
        SELECT E.EmployeeID
              ,E.EmployeeName
              ,E.LastName
              ,E.FirstName
              ,E.MiddleName
              ,E.NickName
              ,E.Title
              ,E.Spouse
              ,E.AdEntID
              ,E.LastUpdate
              ,E.Active
              ,EaO.EastdilOffice_Key        -- select *
          FROM rESxStage.Employee E
          JOIN dESx.EastdilOffice EaO
            ON EaO.EastdilOfficeName = E.Office
         WHERE E.Department <> 'lighthouse'         -- SEED/Lookup adds these and we don't want duplicates
    )
    MERGE dESx.Contact tgt
    USING x src
       ON 1=0
    WHEN NOT MATCHED BY TARGET THEN
    INSERT(FullName
          ,LastName
          ,FirstName
          ,MiddleName
          ,Nickname
          ,Title
          ,SpouseName
          ,IsDefunct
          ,DefunctReason
          ,EastdilOffice_Key
          ,AdEntID
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy)
    VALUES(src.EmployeeName
          ,src.LastName
          ,src.FirstName
          ,src.MiddleName
          ,src.NickName
          ,src.Title
          ,src.Spouse
          ,CASE WHEN src.Active=1 THEN 0 ELSE 1 END
          ,CAST(CASE WHEN src.Active = 0 THEN 'Unknown' ELSE NULL END AS nvarchar(500))
          ,src.EastdilOffice_Key
          ,CASE WHEN src.AdEntID LIKE '[AXU][0-9]%' THEN UPPER(src.AdEntID) END
          ,GETUTCDATE()
          ,'ETL'
          ,ISNULL(src.LastUpdate,'19000101')
          ,'ETL')
    OUTPUT src.EmployeeID,Inserted.Contact_Key
      INTO pESxETL.EmployeeMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' Employees added to Contact';        -- select * from pESxETL.EmployeeMap

    -- Assign phone numbers etc. to employees
    -- Phone number types must exist
     MERGE dESx.ElectronicAddressUsageType tgt
     USING (VALUES ('Extension' ,'PhoneNumber')
                  ,('Main'      ,'PhoneNumber')
                  ,('Mobile'    ,'PhoneNumber')
                  ,('WorkFax'   ,'PhoneNumber')
                  ,('HomeFax'   ,'PhoneNumber')
                  ,('Other'     ,'PhoneNumber')
                  ,('Direct'    ,'PhoneNumber')
                  ,('Blackberry','PhoneNumber')
                  ,('Main'      ,'EmailAddress')
           ) src(typ,usage)
        ON src.typ = tgt.AddressUsageType_Code
       AND src.usage = tgt.ElectronicAddressType_Code
      WHEN NOT MATCHED BY TARGET THEN
    INSERT (AddressUsageType_Code,ElectronicAddressType_Code)
    VALUES (src.typ,src.usage);
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to ElectronicAddressUsageType';

    -- Add the Electronic Address rows for the employees
    CREATE TABLE #ElectronicAddressMap(Company_Key int,ElectronicAddress_Key int);
    WITH x AS (
        SELECT un.Contact_Key
              ,CASE un.AddressType 
                    WHEN 'EmailAddress'         THEN 'EmailAddress'
                                                ELSE 'PhoneNumber' 
               END AS ElectronicAddressType_Code
              ,CASE un.AddressType 
                    WHEN 'EmailAddress'         THEN 'Main'
                    ELSE un.AddressType
               END AS AddressUsageType_Code
              ,un.Value AS [Address]
              ,GETUTCDATE() AS CreatedDate
              ,'ETL' AS CreatedBy
              ,ISNULL(un.LastUpdate,'19000101') AS UpdatedDate
              ,'ETL' AS UpdatedBy           -- select *
          FROM (
            SELECT CAST(rE.Extension        AS nvarchar(255)) AS Extension                
                  ,CAST(rE.EmailAddress     AS nvarchar(255)) AS EmailAddress
                  ,CAST(rE.Phone            AS nvarchar(255)) AS Main
                  ,CAST(rE.MobilePhone      AS nvarchar(255)) AS Mobile
                  ,CAST(rE.WorkFax          AS nvarchar(255)) AS WorkFax
                  ,CAST(rE.HomeFax          AS nvarchar(255)) AS HomeFax
                  ,CAST(rE.OtherPhone       AS nvarchar(255)) AS Other
                  ,CAST(rE.WorkPhone        AS nvarchar(255)) AS Direct
                  ,CAST(rE.BusinessPhone    AS nvarchar(255)) AS Blackberry
                  ,rE.LastUpdate
                  ,EMp.Contact_Key          -- select *
              FROM rESxStage.Employee rE
              JOIN pESxETL.EmployeeMap EMp
                ON EMp.EmployeeID = rE.EmployeeID) AS p
        UNPIVOT (Value FOR AddressType IN (Extension   
                                          ,EmailAddress
                                          ,Main
                                          ,Mobile
                                          ,WorkFax
                                          ,HomeFax
                                          ,Other
                                          ,Direct
                                          ,Blackberry)) AS un
    )
     MERGE dESx.ElectronicAddress tgt
     USING x src
        ON 1 = 0
      WHEN NOT MATCHED BY TARGET THEN
    INSERT(ElectronicAddressType_Code
          ,AddressUsageType_Code
          ,Address
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy)
    VALUES(src.ElectronicAddressType_Code
          ,src.AddressUsageType_Code
          ,src.Address
          ,src.CreatedDate
          ,src.CreatedBy
          ,src.UpdatedDate
          ,src.UpdatedBy)
    OUTPUT src.Contact_Key,inserted.ElectronicAddress_Key
      INTO #ElectronicAddressMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' employee rows added to ElectronicAddress';

    -- Join electronic addresses to their respective employees(contacts)
    INSERT INTO dESx.Contact_ElectronicAddress
            (Contact_Key
            ,ElectronicAddress_Key
            ,CreatedDate
            ,CreatedBy
            ,UpdatedDate
            ,UpdatedBy
            )
    SELECT Company_Key
          ,ElectronicAddress_Key
          ,GETUTCDATE()
          ,'ETL'
          ,GETUTCDATE()
          ,'ETL'
      FROM #ElectronicAddressMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' employee rows added to Contact_ElectronicAddress';

    DROP TABLE #ElectronicAddressMap;
/*Test
GO
exec pESxETL.LoadEmployee;       -- takes 1 second locally
--*/
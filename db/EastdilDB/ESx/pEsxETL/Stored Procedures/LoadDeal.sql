﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/24
-- Description: Populates dESx.Deal data and immediately related tables
-- =============================================
CREATE PROCEDURE [pESxETL].[LoadDeal]
AS
    -- Deals only have names and a client company identified by office
    INSERT INTO dESx.Deal
            (DealName
            ,DealCode
            ,DealType_Code
            ,CreatedDate
            ,CreatedBy
            ,UpdatedDate
            ,UpdatedBy)
    SELECT D.DealName
          ,D.DealCode
          ,TrT.TransactionType
          ,GETUTCDATE()
          ,'ETL'
          ,ISNULL(D.LastUpdate,'19000101')
          ,'ETL'        -- select *
      FROM rESxStage.Deal D
      LEFT JOIN (SELECT rD.DealCode FROM rESxStage.Deal rD GROUP BY rD.DealCode HAVING COUNT(*)>1) Dups
        ON Dups.DealCode = D.DealCode
      JOIN rESxStage.TransactionTypes TrT
        ON TrT.TransactionTypeID = D.TransactionTypeID
     WHERE Dups.DealCode IS NULL    -- Skip duplicated codes
       AND D.DealCode IS NOT NULL
       AND D.DealName IS NOT NULL;  -- Deal with no name is no deal
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to Deal';

    -- Deal company is very much shorter than deal because SL tables didn't populate companies for all deals
    INSERT INTO dESx.DealCompany
            (Deal_Key
            ,Company_Key
            ,DealParticipationType_Code
            ,IsClient
            ,CreatedDate
            ,CreatedBy
            ,UpdatedDate
            ,UpdatedBy)
    SELECT D.Deal_Key
          ,CoAl.Company_Key
          ,SLDCo.CompanyRole
          ,CASE WHEN SLDCo.CompanyRole IN ('Borrower','Seller') THEN 1 ELSE 0 END
          ,GETUTCDATE()
          ,'ETL'
          ,SLDCo.UpdateDatetime
          ,'ETL'            -- select *
      FROM rESxStage.SLDealCompanies SLDCo
      JOIN dESx.Deal D
        ON D.DealCode = SLDCo.DealCode
      JOIN dESx.CompanyAlias CoAl
        ON CoAl.AliasName = SLDCo.CompanyName;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to DealCompany';

    -- Deal Property is Deal Asset.  Again, only SL data is applied.
    INSERT INTO dESx.DealAsset
            (Deal_Key
            ,Asset_Key
            ,CreatedDate
            ,CreatedBy
            ,UpdatedDate
            ,UpdatedBy)
    SELECT D.Deal_Key
          ,Ast.Asset_Key
          ,GETUTCDATE()
          ,'ETL'
          ,SLDP.UpdateDatetime
          ,'ETL'            -- select *
      FROM rESxStage.SLDealProperties SLDP
      JOIN dESx.Deal D
        ON D.DealCode = SLDP.DealCode
      JOIN pESxETL.PropertyPhysicalAddressMap PPhAMp
        ON PPhAMp.PropertyID = SLDP.PropertyID
      JOIN dESx.Asset Ast
        ON Ast.PhysicalAddress_Key = PPhAMp.PhysicalAddress_Key;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to DealAsset';

    -- SL defined some ticker symbols for certain companies.  Overwrite anything that exists.
    UPDATE Co
       SET TickerSymbol = SLTi.Ticker     -- select *
      FROM dESx.Company Co
      JOIN dESx.CompanyAlias CoAl
      JOIN rESxStage.SLTickers SLTi
            ON SLTi.CompanyName = CoAl.AliasName
        ON CoAl.Company_Key = Co.Company_Key;
    PRINT CAST(@@ROWCOUNT AS varchar)+' Company Tickers updated';

/*Test
GO
exec pESxETL.LoadDeal;       -- takes 1 second locally
--*/
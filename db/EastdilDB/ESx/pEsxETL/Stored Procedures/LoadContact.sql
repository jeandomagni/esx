﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/24
-- Description: Populates dESx.Contact and immediately related tables
-- =============================================
CREATE PROCEDURE [pESxETL].[LoadContact]
AS
    -- Create Contacts
    MERGE dESx.Contact tgt
    USING rESxStage.Contacts src
       ON 1=0
    WHEN NOT MATCHED BY TARGET THEN
    INSERT(FullName
          ,LastName
          ,FirstName
          ,MiddleName
          ,LastnameFirstname
          ,Nickname
          ,SpouseName
          ,Title
          ,IsDefunct
          ,DefunctReason
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy)
    VALUES(src.FullName
          ,src.LastName
          ,src.FirstName
          ,src.MiddleName
          ,src.LastnameFirstname
          ,src.Nickname
          ,src.Spouse
          ,src.Title
          ,CASE WHEN src.Active=1 THEN 0 ELSE 1 END
          ,CAST(CASE WHEN src.Active = 0 AND LEN(src.ReasonInactive)<4 THEN 'Unknown'
                     WHEN src.Active = 0 THEN ISNULL(src.ReasonInactive,'Unknown')
                     ELSE NULL
                END AS nvarchar(500))
          ,GETUTCDATE()
          ,'ETL'
          ,ISNULL(src.LastUpdate,'19000101')
          ,'ETL')
    OUTPUT src.ContactID,Inserted.Contact_Key
      INTO pESxETL.ContactMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to Contact';

    -- Assign phone numbers etc. to contacts
    -- Phone number types must exist
     MERGE dESx.ElectronicAddressUsageType tgt
     USING (VALUES ('Main'      ,'PhoneNumber')
                  ,('Direct'    ,'PhoneNumber')
                  ,('Fax'       ,'PhoneNumber')
                  ,('Home'      ,'PhoneNumber')
                  ,('Mobile'    ,'PhoneNumber')
                  ,('Other'     ,'PhoneNumber')
                  ,('Blackberry','PhoneNumber')
                  ,('Pager'     ,'PhoneNumber')
                  ,('Assistant' ,'PhoneNumber')
                  ,('Main'      ,'EmailAddress')
                  ,('Secondary' ,'EmailAddress')
                  ,('Assistant' ,'EmailAddress')
                  ,('Main'      ,'Website')
           ) src(typ,usage)
        ON src.typ = tgt.AddressUsageType_Code
       AND src.usage = tgt.ElectronicAddressType_Code
      WHEN NOT MATCHED BY TARGET THEN
    INSERT (AddressUsageType_Code,ElectronicAddressType_Code)
    VALUES (src.typ,src.usage);
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to ElectronicAddressUsageType';

    CREATE TABLE #ElectronicAddressMap(Company_Key int,ElectronicAddress_Key int);
    WITH x AS (
        SELECT un.Contact_Key
              ,CASE un.AddressType 
                    WHEN 'WebAddress'           THEN 'Website' 
                    WHEN 'EmailAddress'         THEN 'EmailAddress'
                    WHEN 'SecondaryEmail'       THEN 'EmailAddress'
                    WHEN 'AssistantEmail'       THEN 'EmailAddress'
                                                ELSE 'PhoneNumber' 
               END AS ElectronicAddressType_Code
              ,CASE un.AddressType 
                    WHEN 'WebAddress'           THEN 'Main' 
                    WHEN 'EmailAddress'         THEN 'Main'
                    WHEN 'SecondaryEmail'       THEN 'Secondary'
                    WHEN 'AssistantEmail'       THEN 'Assistant'
                    WHEN 'Main'                 THEN 'Main'
                    WHEN 'Direct'               THEN 'Direct'    
                    WHEN 'Fax'                  THEN 'Fax'
                    WHEN 'Home'                 THEN 'Home'
                    WHEN 'Mobile'               THEN 'Mobile'
                    WHEN 'Other'                THEN 'Other'
                    WHEN 'Blackberry'           THEN 'Blackberry'
                    WHEN 'Pager'                THEN 'Pager'
                    WHEN 'AssistantPhone'       THEN 'Assistant'
                    WHEN 'TollFreeNumber'       THEN 'TollFree'
                    WHEN 'AlternateCompanyPhone'THEN 'Alternate' 
                    WHEN 'Fax'                  THEN 'Fax'
               END AS AddressUsageType_Code
              ,un.Value AS [Address]
              ,GETUTCDATE() AS CreatedDate
              ,'ETL' AS CreatedBy
              ,ISNULL(un.LastUpdate,'19000101') AS UpdatedDate
              ,'ETL' AS UpdatedBy           -- select *
          FROM (
            SELECT CAST(rCn.DirectPhone           AS nvarchar(255)) AS Direct                
                  ,CAST(rCn.MainPhoneExt          AS nvarchar(255)) AS Main
                  ,CAST(rCn.Fax                   AS nvarchar(255)) AS Fax
                  ,CAST(rCn.HomePhone             AS nvarchar(255)) AS Home
                  ,CAST(rCn.MobilePhone           AS nvarchar(255)) AS Mobile
                  ,CAST(rCn.BlackberryPhone       AS nvarchar(255)) AS Blackberry
                  ,CAST(rCn.OtherPhone            AS nvarchar(255)) AS Other
                  ,CAST(rCn.Pager                 AS nvarchar(255)) AS Pager
                  ,CAST(rCn.AssistantPhone        AS nvarchar(255)) AS AssistantPhone
                  ,CAST(rCn.EmailAddress          AS nvarchar(255)) AS EmailAddress
                  ,CAST(rCn.SecondaryEmail        AS nvarchar(255)) AS SecondaryEmail
                  ,CAST(rCn.AssistantEmail        AS nvarchar(255)) AS AssistantEmail
                  ,rCn.WebAddress
                  ,rCn.LastUpdate
                  ,CnMp.Contact_Key
              FROM rESxStage.Contacts rCn
              JOIN pESxETL.ContactMap CnMp
                ON CnMp.ContactID = rCn.ContactID) AS p
        UNPIVOT (Value FOR AddressType IN (Direct        
                                          ,Main
                                          ,Fax
                                          ,Home
                                          ,Mobile
                                          ,Blackberry
                                          ,Other
                                          ,Pager
                                          ,AssistantPhone
                                          ,EmailAddress
                                          ,SecondaryEmail
                                          ,AssistantEmail
                                          ,WebAddress )) AS un
    )
     MERGE dESx.ElectronicAddress tgt
     USING x src
        ON 1 = 0
      WHEN NOT MATCHED BY TARGET THEN
    INSERT(ElectronicAddressType_Code
          ,AddressUsageType_Code
          ,Address
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy)
    VALUES(src.ElectronicAddressType_Code
          ,src.AddressUsageType_Code
          ,src.Address
          ,src.CreatedDate
          ,src.CreatedBy
          ,src.UpdatedDate
          ,src.UpdatedBy)
    OUTPUT src.Contact_Key,inserted.ElectronicAddress_Key
      INTO #ElectronicAddressMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to ElectronicAddress';

    -- Join electronic addresses to their respective contacts
    INSERT INTO dESx.Contact_ElectronicAddress
            (Contact_Key
            ,ElectronicAddress_Key
            ,CreatedDate
            ,CreatedBy
            ,UpdatedDate
            ,UpdatedBy
            )
    SELECT Company_Key
          ,ElectronicAddress_Key
          ,GETUTCDATE()
          ,'ETL'
          ,GETUTCDATE()
          ,'ETL'
      FROM #ElectronicAddressMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to Contact_ElectronicAddress';

    DROP TABLE #ElectronicAddressMap;
/*Test
GO
exec pESxETL.LoadContact;       -- takes 5 seconds locally
--*/
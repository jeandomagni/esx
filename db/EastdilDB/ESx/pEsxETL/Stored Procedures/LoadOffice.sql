﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/24
-- Description: Populates dESx.Office data and immediately related tables
-- =============================================
CREATE PROCEDURE [pESxETL].[LoadOffice]
AS
    -- For performance
    TRUNCATE TABLE pESxETL.Addresses;
    INSERT INTO pESxETL.Addresses SELECT * FROM rESxStage.Addresses;

    -- Offices require an address so save these until addresses are added
    INSERT INTO pESxETL.Office
    SELECT A.LookupValue
          ,MAX(Co.CorrectedName)                                                                        AS OfficeName
          ,CoMp.Company_Key
          ,CoMp.CompanyID
          ,GETUTCDATE()                                                                                 AS CreatedDate
          ,'ETL'                                                                                        AS CreatedBy
          ,MAX(CASE WHEN Co.LastUpdate < A.UpdateDatetime THEN Co.LastUpdate ELSE A.UpdateDatetime END) AS UpdatedDate
          ,'ETL'                                                                                        AS UpdatedBy        -- select * 
      FROM pESxETL.CompanyMap CoMp
      JOIN pESxETL.Companies Co
        ON Co.ParentID  = CoMp.CompanyID
        OR Co.CompanyID = CoMp.CompanyID
      JOIN pESxETL.Addresses A 
        ON A.AddressID = Co.AddressID 
       AND A.LookupValue IS NOT NULL
     GROUP BY A.LookupValue
          ,CoMp.Company_Key
          ,CoMp.CompanyID;        -- select * from pESxETL.Office;

    -- Address types must exist
     MERGE dESx.PhysicalAddressType tgt
     USING (SELECT DISTINCT a.TypeCode
              FROM pESxETL.Addresses A) src(typ)
        ON src.typ = tgt.PhysicalAddressType_Code
      WHEN NOT MATCHED BY TARGET THEN
    INSERT (PhysicalAddressType_Code)
    VALUES (src.typ);
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to PhysicalAddressType';
    
    -- Add the addresses for offices
    WITH x AS (
        SELECT A.LookupValue
              ,A.AddressID
              ,O.Company_Key
              ,ROW_NUMBER() OVER(PARTITION BY O.Company_Key,A.LookupValue 
                        ORDER BY LEN(NULLIF(A.Address1,''))+LEN(NULLIF(A.Address2,''))+LEN(NULLIF(A.Address3,''))
              +LEN(NULLIF(A.City,''))+LEN(NULLIF(A.State,''))+LEN(NULLIF(A.PostalCode,''))+LEN(NULLIF(A.Country,''))DESC) Selector  -- select *
          FROM pESxETL.Office O
          JOIN pESxETL.Addresses A
            ON A.LookupValue = O.LookupValue --/*
    ), y AS (
        SELECT x.AddressID
              ,x.LookupValue
              ,x.Company_Key
              ,A.TypeCode                                                                                           AS PhysicalAddressType_Code
              ,A.Address1
              ,A.Address2
              ,A.Address3
              ,A.Address4
              ,A.City
              ,CASE WHEN LEN(A.State)>2 THEN NULL ELSE SUBSTRING(A.State,1,2) END                                   AS [State]
              ,CAST(A.PostalCode AS nvarchar(15))                                                                   AS PostalCode
              ,ISNULL(Cu.Country_Key,(SELECT Country_Key FROM dESx.Country WHERE CountryName = 'United States'))    AS Country_Key
              ,A.UpdateDatetime
              ,A.Coordinates
              ,CASE WHEN A.Coordinates IS NULL THEN NULL ELSE A.ValidationDatetime END                      AS ValidationDatetime
              ,A.AddressStatus      --*/) select *
          FROM x
          JOIN pESxETL.Addresses A
            ON A.AddressID = x.AddressID
          LEFT JOIN dESx.Country Cu
            ON Cu.CountryName = A.Country
         WHERE x.Selector = 1
    )   --SELECT * FROM y
     MERGE dESx.PhysicalAddress tgt
     USING y src
        ON 1=0
      WHEN NOT MATCHED BY TARGET THEN
    INSERT(PhysicalAddressType_Code
          ,Address1
          ,Address2
          ,Address3
          ,Address4
          ,City
          ,StateProvidence_Code
          ,Postal_Code
          ,Country_Key
          ,Coordinates
          ,Latitude
          ,Longitude
          ,AddressValidationDate
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy)
    VALUES(PhysicalAddressType_Code
          ,ISNULL(src.Address1      ,'')
          ,NULLIF(src.Address2      ,'')
          ,NULLIF(src.Address3      ,'')
          ,NULLIF(src.Address4      ,'')
          ,ISNULL(src.City          ,'')
          ,NULLIF(src.State         ,'')
          ,ISNULL(src.PostalCode    ,'')
          ,src.Country_Key
          ,src.Coordinates
          ,src.Coordinates.Lat
          ,src.Coordinates.Long
          ,src.ValidationDatetime
          ,GETUTCDATE()
          ,'ETL'
          ,ISNULL(src.UpdateDatetime,'19000101')
          ,'ETL')
    OUTPUT Inserted.PhysicalAddress_Key,src.LookupValue,src.Company_Key
      INTO pESxETL.CompanyPhysicalAddressMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to PhysicalAddress';

    -- Now that we have the physical address keys the office rows can be inserted
    INSERT INTO dESx.Office
          (OfficeName
          ,Company_Key
          ,IsHeadquarters
          ,PhysicalAddress_Key
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy)
    SELECT O.OfficeName
          ,O.Company_Key
          ,0                            AS IsHeadquarters
          ,CoPhAMp.PhysicalAddress_Key
          ,O.CreatedDate
          ,O.CreatedBy
          ,ISNULL(O.UpdatedDate,'19000101') AS UpdatedDate
          ,O.UpdatedBy      -- select * 
      FROM pESxETL.Office O
      LEFT JOIN pESxETL.CompanyPhysicalAddressMap CoPhAMp
        ON CoPhAMp.Company_Key = O.Company_Key
       AND CoPhAMp.LookupValue = O.LookupValue;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to Office';       -- select * from dESx.Office order by company_Key,isheadquarters desc

    -- Make the first office with an address headquarters
    UPDATE dESx.Office
       SET IsHeadquarters = 1   -- select * from desx.office
     WHERE Office_Key IN (
            SELECT MIN(Office_Key)
              FROM dESx.Office
             WHERE PhysicalAddress_Key > 0
             GROUP BY Company_Key
            HAVING MAX(CAST(IsHeadquarters AS int)) = 0);
    PRINT CAST(@@ROWCOUNT AS varchar)+' Office rows updated to be headquarters';

    -- All too often the AddressID of a contact belongs to a different company than CompanyID.
    -- So for now, give all the contacts to the company primary office.  
    INSERT INTO dESx.ContactOffice
            (Contact_Key
            ,Office_Key
            ,ContactOfficeRole_Code
            ,EndDate
            ,CreatedDate
            ,CreatedBy
            ,UpdatedDate
            ,UpdatedBy
            )
    SELECT CnMp.Contact_Key
          ,O.Office_Key
          ,'Worker'                         AS ContactOfficeRole
          ,CASE WHEN Cn.Active = 0 THEN DATEADD(YEAR,-1,GETUTCDATE()) ELSE NULL END AS EndDate
          ,GETUTCDATE()                     AS CreateDate
          ,'ETL'                            AS CreatedBy
          ,ISNULL(Cn.LastUpdate,'19000101') AS UpdatedDate
          ,'ETL'                            AS UpdatedBy       -- select *
      FROM rESxStage.Contacts Cn
      JOIN pESxETL.ContactMap CnMp
        ON CnMp.ContactID = Cn.ContactID
      JOIN pESxETL.Companies Co
      JOIN pESxETL.CompanyMap CoMp
      JOIN dESx.Office O
        ON O.Company_Key = CoMp.Company_Key
               AND O.IsHeadquarters = 1
            ON CoMp.CompanyID = Co.ParentID
            OR CoMp.CompanyID = Co.CompanyID
        ON Co.CompanyID = Cn.CompanyID;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to ContactOffice';        -- select * from dESx.ContactOffice
/*Test
GO
exec pESxETL.LoadOffice;       -- takes 25 seconds locally
--*/
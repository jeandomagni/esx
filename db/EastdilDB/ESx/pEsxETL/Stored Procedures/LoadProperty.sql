﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/24
-- Description: Populates Properties into dESx.Asset data and immediately related tables
-- =============================================
CREATE PROCEDURE [pESxETL].[LoadProperty]
AS
    -- Create physical address rows for properties
    WITH x AS (
        SELECT SLP.Address1
              ,SLP.City
              ,SLP.State
              ,ISNULL(Cu.Country_Key,(SELECT Country_Key FROM dESx.Country WHERE CountryName = 'United States'))AS Country_Key
              ,PhAGD.Coordinates
              ,CASE WHEN PhAGD.Coordinates IS NULL THEN NULL ELSE PhAGD.ValidationDatetime END  AS ValidationDatetime
              ,SLP.UpdateDatetime      
              ,SLP.LookupValue
              ,SLP.PropertyID        -- select *
          FROM rESxStage.SLProperties SLP
          JOIN rESxStage.PhysicalAddressGeoData PhAGD
            ON PhAGD.LookupValue = SLP.LookupValue
          LEFT JOIN dESx.Country Cu
            ON Cu.CountryName = ISNULL(NULLIF(SLP.Country,'UK'),'United Kingdom') 
    )--   SELECT * FROM x
     MERGE dESx.PhysicalAddress tgt
     USING x src
        ON 1=0
      WHEN NOT MATCHED BY TARGET THEN
    INSERT(PhysicalAddressType_Code
          ,Address1
          ,City
          ,StateProvidence_Code
          ,Postal_Code
          ,Country_Key
          ,Coordinates
          ,Latitude
          ,Longitude
          ,AddressValidationDate
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy)
    VALUES('Physical'
          ,ISNULL(src.Address1      ,'')
          ,ISNULL(src.City          ,'')
          ,NULLIF(src.State         ,'')
          ,''
          ,src.Country_Key
          ,src.Coordinates
          ,src.Coordinates.Lat
          ,src.Coordinates.Long
          ,src.ValidationDatetime
          ,GETUTCDATE()
          ,'ETL'
          ,src.UpdateDatetime
          ,'ETL')
    OUTPUT Inserted.PhysicalAddress_Key,src.LookupValue,src.PropertyID
      INTO pESxETL.PropertyPhysicalAddressMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to PhysicalAddress';

    -- Properties are stored as Assets    
    INSERT INTO dESx.Asset
            (AssetType_Code
            ,PhysicalAddress_Key
            ,CreatedDate
            ,CreatedBy
            ,UpdatedDate
            ,UpdatedBy)
    SELECT 'Property'
          ,PhA.PhysicalAddress_Key
          ,GETUTCDATE()
          ,'ETL'
          ,PhA.UpdatedDate
          ,'ETL'
      FROM pESxETL.PropertyPhysicalAddressMap PPhAMa
      JOIN dESx.PhysicalAddress PhA
        ON PhA.PhysicalAddress_Key = PPhAMa.PhysicalAddress_Key;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to Asset';
/*Test
GO
exec pESxETL.LoadProperty;       -- takes 1 second locally
--*/
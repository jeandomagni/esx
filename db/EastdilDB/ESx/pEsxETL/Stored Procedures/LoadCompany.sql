﻿-- =============================================
-- Author:      Rick Bielawski
-- Create date: 2016/05/24
-- Description: Populates dESx.Company and immediately related tables (CompanyHierarchy, CompanyAlias, Company ElectronicAddress rows...)
-- =============================================
CREATE PROCEDURE [pESxETL].[LoadCompany]
AS
    -- This is for performance.
    ALTER TABLE pESxETL.Companies DROP pk_pESxETL_Companies_CompanyID;
    DROP INDEX ixf_pESxETL_Companies_DuplicateOfID ON pESxETL.Companies;
    DROP INDEX ixf_pESxETL_Companies_ParentParentID ON pESxETL.Companies;
    INSERT INTO pESxETL.Companies 
    SELECT CorrectedName          
          ,ParentName             
          ,ParentID               
          ,ParentParentName       
          ,ParentParentID         
          ,DuplicateOfID          
          ,CompanyID              
          ,CompanyName            
          ,CompanyLegalName       
          ,Division               
          ,AddressID              
          ,ShippingID             
          ,TickerSymbol           
          ,CIBOSCode              
          ,MainPhone              
          ,Fax                    
          ,TollFreeNumber         
          ,AlternateCompanyPhone  
          ,CompanyEmail           
          ,Website                
          ,Active                 
          ,ReasonInactive         
          ,LastUpdate             
      FROM rESxStage.Companies;
    ALTER TABLE [pESxETL].[Companies] ADD CONSTRAINT [pk_pESxETL_Companies_CompanyID] PRIMARY KEY (CompanyID);
    CREATE NONCLUSTERED INDEX [ixf_pESxETL_Companies_DuplicateOfID] ON [pESxETL].[Companies](DuplicateOfID ASC) WHERE (DuplicateOfID IS NOT NULL);
    CREATE NONCLUSTERED INDEX [ixf_pESxETL_Companies_ParentParentID] ON [pESxETL].[Companies](ParentParentID ASC);


    -- Insert all the distinct Canonicals and save their translation keys.
     MERGE dESx.Company tgt
     USING(SELECT Co.ParentID
                 ,CAST(MAX(CAST(Co.Active AS int)) AS bit) AS Active
                 ,MAX(Co.LastUpdate)                       AS LastUpdate
             FROM pESxETL.Companies Co 
            WHERE Co.DuplicateOfID IS NULL
              AND Co.ParentID IS NOT NULL
            GROUP BY Co.ParentID) src
        ON 1 = 0
      WHEN NOT MATCHED BY TARGET THEN
    INSERT (IsDefunct
           ,CreatedDate
           ,CreatedBy
           ,UpdatedDate
           ,UpdatedBy
           )
    VALUES(CASE WHEN src.Active = 1 THEN 0 ELSE 1 END
          ,GETUTCDATE()
          ,'ETL'
          ,src.LastUpdate
          ,'ETL')
    OUTPUT src.ParentID,Inserted.Company_Key 
      INTO pESxETL.CompanyMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to Company';

    -- Build the 1st (only) level hierarchy - DBA sits on top of Canonical
    WITH x AS (
        SELECT Co.ParentID          AS ChildID
              ,Co.ParentParentID    AS ParentID
              ,GETUTCDATE()         AS CreateDate
              ,'ETL'                AS CreateBy
              ,MAX(Co.LastUpdate)   AS UpdateDate
              ,'ETL'                AS UpdateBy
          FROM pESxETL.Companies Co 
          JOIN pESxETL.Companies CoPP
            ON CoPP.CompanyID = Co.ParentParentID
           AND CoPP.ParentID IS NOT NULL
         WHERE Co.DuplicateOfID IS NULL
           AND Co.ParentID <> Co.CompanyID
         GROUP BY Co.ParentID,Co.ParentParentID
    )   --/*
    INSERT INTO dESx.CompanyHierarchy
          (Child__Company_Key
          ,Parent__Company_Key
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy
          ,TopParent__Company_Key
          )
    SELECT CoMC.Company_Key
          ,CoMP.Company_Key
          ,x.CreateDate
          ,x.CreateBy
          ,x.UpdateDate
          ,x.UpdateBy
          ,CoMP.Company_Key     --*/  SELECT *
      FROM x
      JOIN pESxETL.CompanyMap CoMC
        ON CoMC.CompanyID = x.ChildID
      JOIN pESxETL.CompanyMap CoMP
        ON CoMP.CompanyID = x.ParentID;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to CompanyHierarchy';

    -- Name the companies just inserted
    INSERT INTO dESx.CompanyAlias
            (Company_Key
            ,AliasName
            ,IsLegal
            ,IsPrimary
            ,IsDefunct
            ,CreatedDate
            ,CreatedBy
            ,UpdatedDate
            ,UpdatedBy
            )
    SELECT CoMa.Company_Key
          ,ISNULL(Co.ParentName,Co.CorrectedName)
          ,0
          ,1
          ,0
          ,GETUTCDATE()
          ,'ETL'
          ,Co.LastUpdate
          ,'ETL'            -- select *
      FROM pESxETL.Companies Co
      JOIN pESxETL.CompanyMap CoMa
        ON CoMa.CompanyID = Co.CompanyID
       ORDER BY 2;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to CompanyAlias';

    -- Assign phone numbers etc. to companies
    -- Phone number types must exist
     MERGE dESx.ElectronicAddressUsageType tgt
     USING (VALUES ('Main'      ,'PhoneNumber')
                  ,('Fax'       ,'PhoneNumber')
                  ,('TollFree'  ,'PhoneNumber')
                  ,('Alternate' ,'PhoneNumber')
                  ,('Main'      ,'EmailAddress')
                  ,('Main'      ,'Website')
           ) src(typ,usage)
        ON src.typ = tgt.AddressUsageType_Code
       AND src.usage = tgt.ElectronicAddressType_Code
      WHEN NOT MATCHED BY TARGET THEN
    INSERT (AddressUsageType_Code,ElectronicAddressType_Code)
    VALUES (src.typ,src.usage);
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to ElectronicAddressUsageType';

    CREATE TABLE #ElectronicAddressMap(Company_Key int,ElectronicAddress_Key int);
    WITH x AS (
        SELECT un.Company_Key
              ,CASE un.AddressType 
                    WHEN 'Website'      THEN 'Website' 
                    WHEN 'CompanyEmail' THEN 'EmailAddress'
                                        ELSE 'PhoneNumber' 
               END AS ElectronicAddressType_Code
              ,CASE un.AddressType 
                    WHEN 'Website'              THEN 'Main' 
                    WHEN 'CompanyEmail'         THEN 'Main'
                    WHEN 'MainPhone'            THEN 'Main'
                    WHEN 'TollFreeNumber'       THEN 'TollFree'
                    WHEN 'AlternateCompanyPhone'THEN 'Alternate' 
                    WHEN 'Fax'                  THEN 'Fax'
               END AS AddressUsageType_Code
              ,un.Value AS [Address]
              ,GETUTCDATE() AS CreatedDate
              ,'ETL' AS CreatedBy
              ,un.LastUpdate AS UpdatedDate
              ,'ETL' AS UpdatedBy
          FROM (
            SELECT CAST(rCo.MainPhone             AS nvarchar(128)) AS MainPhone                
                  ,CAST(rCo.Fax                   AS nvarchar(128)) AS Fax
                  ,CAST(rCo.TollFreeNumber        AS nvarchar(128)) AS TollFreeNumber           
                  ,CAST(rCo.AlternateCompanyPhone AS nvarchar(128)) AS AlternateCompanyPhone    
                  ,rCo.CompanyEmail
                  ,rCo.Website
                  ,rCo.LastUpdate
                  ,CoMp.Company_Key
              FROM pESxETL.Companies rCo
              JOIN pESxETL.CompanyMap CoMp
                ON CoMp.CompanyID = rCo.CompanyID
            WHERE COALESCE(rCo.MainPhone,rCo.Fax,rCo.TollFreeNumber,rCo.AlternateCompanyPhone,rCo.CompanyEmail,rCo.Website) IS NOT NULL) AS p
        UNPIVOT (Value FOR AddressType IN (MainPhone,Fax,TollFreeNumber,AlternateCompanyPhone,CompanyEmail,Website)) AS un
    )
     MERGE dESx.ElectronicAddress tgt
     USING x src
        ON 1 = 0
      WHEN NOT MATCHED BY TARGET THEN
    INSERT(ElectronicAddressType_Code
          ,AddressUsageType_Code
          ,Address
          ,CreatedDate
          ,CreatedBy
          ,UpdatedDate
          ,UpdatedBy)
    VALUES(src.ElectronicAddressType_Code
          ,src.AddressUsageType_Code
          ,src.Address
          ,src.CreatedDate
          ,src.CreatedBy
          ,src.UpdatedDate
          ,src.UpdatedBy)
    OUTPUT src.Company_Key,inserted.ElectronicAddress_Key
      INTO #ElectronicAddressMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to ElectronicAddress';

    -- Join electronic addresses to their respective companies
    INSERT INTO dESx.Company_ElectronicAddress
            (Company_Key
            ,ElectronicAddress_Key
            ,CreatedDate
            ,CreatedBy
            ,UpdatedDate
            ,UpdatedBy
            )
    SELECT Company_Key
          ,ElectronicAddress_Key
          ,GETUTCDATE()
          ,'ETL'
          ,GETUTCDATE()
          ,'ETL'
      FROM #ElectronicAddressMap;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to Company_ElectronicAddress';

    DROP TABLE #ElectronicAddressMap;

    -- SL defined aliases for certain companies.  Add them only if they don't already exist.
    INSERT INTO dESx.CompanyAlias
            (Company_Key
            ,AliasName
            ,IsLegal
            ,IsPrimary
            ,IsDefunct
            ,CreatedDate
            ,CreatedBy
            ,UpdatedDate
            ,UpdatedBy)
    SELECT CoAl.Company_Key AS Company_Key
          ,SLAl.Alias       AS AliasName
          ,0                AS IsLegal
          ,0                AS IsPrimary
          ,0                AS IsDefunct
          ,GETUTCDATE()     AS CreatedDate
          ,'ETL'            AS CreatedBy
          ,GETUTCDATE()     AS UpdatedDate
          ,'ETL'            AS UpdatedBy    -- select *
      FROM rESxStage.SLAlias SLAl
      JOIN dESx.CompanyAlias CoAl
            ON CoAl.AliasName = SLAl.CompanyName 
      LEFT JOIN dESx.CompanyAlias CoAl2
        ON CoAl2.AliasName = SLAl.Alias
     WHERE CoAl2.CompanyAlias_Key is NULL;
    PRINT CAST(@@ROWCOUNT AS varchar)+' added to CompanyAlias';
/*Test
GO
exec pESxETL.LoadCompany;       -- takes 3 seconds locally
--*/
﻿CREATE TABLE [dESx].[TagUsage] (
    [TagUsage_Key] INT                    IDENTITY (1, 1) NOT NULL,
    [User_Key]     INT                    NOT NULL,
    [Tag_Key]      [iESx].[TYP_Reference] NOT NULL,
    [UseCount]     INT                    NOT NULL,
    [LastUseDate]  DATE                   NOT NULL,
    CONSTRAINT [PK_TagUsage] PRIMARY KEY CLUSTERED ([TagUsage_Key] ASC),
    CONSTRAINT [FK_TagUsage_Tag] FOREIGN KEY ([Tag_Key]) REFERENCES [dESx].[Tag] ([Tag_Key]),
    CONSTRAINT [FK_TagUsage_User] FOREIGN KEY ([User_Key]) REFERENCES [dESx].[User] ([User_Key])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TagUsage_UserKey_TagKey]
    ON [dESx].[TagUsage]([User_Key] ASC, [Tag_Key] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'User & Tag must be unique', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'TagUsage', @level2type = N'INDEX', @level2name = N'UIX_TagUsage_UserKey_TagKey';


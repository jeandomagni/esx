﻿CREATE TABLE [dESx].[Company_ElectronicAddress] (
    [Company_ElectronicAddress_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Company_Key]                   INT          NOT NULL,
    [ElectronicAddress_Key]         INT          NOT NULL,
    [CreatedDate]                   DATETIME     CONSTRAINT [DF_Company_ElectronicAddress_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]                     VARCHAR (20) NOT NULL,
    [UpdatedDate]                   DATETIME     CONSTRAINT [DF_Company_ElectronicAddress_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                     VARCHAR (20) NULL,
    CONSTRAINT [PK_Company_ElectronicAddress] PRIMARY KEY CLUSTERED ([Company_ElectronicAddress_Key] ASC),
    CONSTRAINT [FK_Company_ElectronicAddress_Company] FOREIGN KEY ([Company_Key]) REFERENCES [dESx].[Company] ([Company_Key]),
    CONSTRAINT [FK_Company_ElectronicAddress_ElectronicAddress] FOREIGN KEY ([ElectronicAddress_Key]) REFERENCES [dESx].[ElectronicAddress] ([ElectronicAddress_Key])
);




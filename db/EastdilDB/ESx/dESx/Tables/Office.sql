﻿CREATE TABLE [dESx].[Office] (
    [Office_Key]          INT            IDENTITY (1, 1) NOT NULL,
    [OfficeName]          NVARCHAR (200) NULL,
    [Company_Key]         INT            NOT NULL,
    [IsHeadquarters]      BIT            CONSTRAINT [DF_Office_IsHeadquarters] DEFAULT ((0)) NOT NULL,
    [PhysicalAddress_Key] INT            NULL,
    [CreatedDate]         DATETIME       CONSTRAINT [DF_Office_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]           VARCHAR (20)   NOT NULL,
    [UpdatedDate]         DATETIME       CONSTRAINT [DF_Office_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]           VARCHAR (20)   NULL,
    CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED ([Office_Key] ASC),
    CONSTRAINT [CK_Office_HeadquartersRequiresAnAddress] CHECK (NOT ([IsHeadquarters]=(1) AND [PhysicalAddress_Key] IS NULL)),
    CONSTRAINT [FK_Office_Company] FOREIGN KEY ([Company_Key]) REFERENCES [dESx].[Company] ([Company_Key]),
    CONSTRAINT [FK_Office_PhysicalAddress] FOREIGN KEY ([PhysicalAddress_Key]) REFERENCES [dESx].[PhysicalAddress] ([PhysicalAddress_Key])
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Office_CompanyHas1Headquarters]
    ON [dESx].[Office]([Company_Key] ASC, [IsHeadquarters] ASC) WHERE ([IsHeadquarters]=(1));


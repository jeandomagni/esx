﻿CREATE TABLE [dESx].[DealValuationType]
(
	[DealValuationType_Code] NVARCHAR(20) NOT NULL PRIMARY KEY,
	[Description] NVARCHAR (200) NOT NULL, 
    [Sort] INT NULL
)

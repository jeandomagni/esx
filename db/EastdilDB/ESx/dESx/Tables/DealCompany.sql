﻿CREATE TABLE [dESx].[DealCompany] (
    [DealCompany_Key]            INT           IDENTITY (1, 1) NOT NULL,
    [Deal_Key]                   INT           NOT NULL,
    [Company_Key]                INT           NOT NULL,
    [DealParticipationType_Code] NVARCHAR (50) NOT NULL,
    [IsClient]                   BIT           DEFAULT ((0)) NOT NULL,
    [CreatedDate]                DATETIME      CONSTRAINT [DF_DealCompany_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]                  VARCHAR (20)  NOT NULL,
    [UpdatedDate]                DATETIME      CONSTRAINT [DF_DealCompany_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                  VARCHAR (20)  NULL,
    CONSTRAINT [PK_DealCompany] PRIMARY KEY CLUSTERED ([DealCompany_Key] ASC),
    CONSTRAINT [FK_DealCompany_Company] FOREIGN KEY ([Company_Key]) REFERENCES [dESx].[Company] ([Company_Key]),
    CONSTRAINT [FK_DealCompany_Deal] FOREIGN KEY ([Deal_Key]) REFERENCES [dESx].[Deal] ([Deal_Key]),
    CONSTRAINT [FK_DealCompany_DealParticipationType] FOREIGN KEY ([DealParticipationType_Code]) REFERENCES [dESx].[DealParticipationType] ([DealParticipationType_Code])
);




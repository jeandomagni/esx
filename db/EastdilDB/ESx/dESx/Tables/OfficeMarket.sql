﻿CREATE TABLE [dESx].[OfficeMarket] (
    [OfficeMarket_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Office_Key]       INT          NOT NULL,
    [Market_Key]       INT          NOT NULL,
    [EndDate]          DATETIME     NULL,
    [IsCurrent]        AS           (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]      DATETIME     CONSTRAINT [DF_OfficeMarket_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]        VARCHAR (20) NOT NULL,
    [UpdatedDate]      DATETIME     CONSTRAINT [DF_OfficeMarket_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]        VARCHAR (20) NULL,
    CONSTRAINT [PK_OfficeMarket] PRIMARY KEY CLUSTERED ([OfficeMarket_Key] ASC),
    CONSTRAINT [FK_OfficeMarket_Market] FOREIGN KEY ([Market_Key]) REFERENCES [dESx].[Market] ([Market_Key]),
    CONSTRAINT [FK_OfficeMarket_Office] FOREIGN KEY ([Office_Key]) REFERENCES [dESx].[Office] ([Office_Key])
);








GO
CREATE NONCLUSTERED INDEX [UIX_OfficeMarket_Office_Market]
    ON [dESx].[OfficeMarket]([Office_Key] ASC, [Market_Key] ASC);


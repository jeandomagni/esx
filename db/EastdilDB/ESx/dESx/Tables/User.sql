﻿CREATE TABLE [dESx].[User] (
    [User_Key]      INT           IDENTITY (1, 1) NOT NULL,
    [LoginName]     NVARCHAR (50) NOT NULL,
    [LastLoginDate] DATE          CONSTRAINT [DF_User_LastLoginDate] DEFAULT ('19000101') NOT NULL,
    [HasAccess]     BIT           CONSTRAINT [DF_User_IsAllowedAccess] DEFAULT ((0)) NOT NULL,
	[Contact_Key]   INT           NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([User_Key] ASC), 
    CONSTRAINT [FK_User_ToContact] FOREIGN KEY ([Contact_Key]) REFERENCES [dESx].[Contact]([Contact_Key])
);




GO
CREATE NONCLUSTERED INDEX [UIX_User_LoginName]
    ON [dESx].[User]([User_Key] ASC);


GO



GO


GO




﻿CREATE TABLE [dESx].[BidStatus] (
    [BidStatus_Code] NVARCHAR (10) NOT NULL,
    [Description]    NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_BidStatus] PRIMARY KEY CLUSTERED ([BidStatus_Code] ASC)
);


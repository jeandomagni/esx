﻿CREATE TABLE [dESx].[LegalEntity] (
    [LegalEntity_Code] NVARCHAR (50) NOT NULL,
	[Description]       NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_LegalEntity] PRIMARY KEY CLUSTERED ([LegalEntity_Code] ASC)
);


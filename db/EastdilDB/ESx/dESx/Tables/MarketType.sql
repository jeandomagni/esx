﻿CREATE TABLE [dESx].[MarketType] (
    [MarketType_Code] NVARCHAR (25)  NOT NULL,
    [Description]     NVARCHAR (100) NOT NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_JurisdictionType] PRIMARY KEY CLUSTERED ([MarketType_Code] ASC)
);


﻿CREATE TABLE [dESx].[ElectronicAddress] (
    [ElectronicAddress_Key]      INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ElectronicAddressType_Code] NVARCHAR (15)  NOT NULL,
    [AddressUsageType_Code]      NVARCHAR (15)  NOT NULL,
    [Description]                NVARCHAR (200) NULL,
    [Address]                    NVARCHAR (512) NOT NULL,
    [CreatedDate]                DATETIME       CONSTRAINT [DF_ElectronicAddress_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]                  VARCHAR (20)   NOT NULL,
    [UpdatedDate]                DATETIME       CONSTRAINT [DF_ElectronicAddress_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                  VARCHAR (20)   NULL,
    CONSTRAINT [PK_ElectronicAddress] PRIMARY KEY CLUSTERED ([ElectronicAddress_Key] ASC),
    CONSTRAINT [FK_ElectronicAddress_ElectronicAddressUsageType] FOREIGN KEY ([ElectronicAddressType_Code], [AddressUsageType_Code]) REFERENCES [dESx].[ElectronicAddressUsageType] ([ElectronicAddressType_Code], [AddressUsageType_Code])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IsActive=yes, Type=Utility,Notes=', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'ElectronicAddress';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = N'yes', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'ElectronicAddress';


GO



GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Describes how/when the address is used.  For example if ElectronicAddressType_Code is PhoneNumber than this might be Home, Work or Cell.', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'ElectronicAddress', @level2type = N'COLUMN', @level2name = N'AddressUsageType_Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=37565 @ 5.33:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'ElectronicAddress', @level2type = N'COLUMN', @level2name = N'Address';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Any ancilary data useful in making the best use of the address.', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'ElectronicAddress', @level2type = N'COLUMN', @level2name = N'Description';



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A value indicating the type of electronic address.  For example PhoneNumber, WebPage, EmailAddress', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'ElectronicAddress', @level2type = N'COLUMN', @level2name = N'ElectronicAddressType_Code';



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'ElectronicAddress', @level2type = N'COLUMN', @level2name = N'ElectronicAddress_Key';


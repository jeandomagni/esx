﻿CREATE TABLE [dESx].[CompanyDealTypeInterest] (
    [CompanyDealTypeInterest_Key] INT           IDENTITY (1, 1) NOT NULL,
    [Company_Key]                 INT           NOT NULL,
    [DealType_Code]               NVARCHAR (50) NOT NULL,
    [CreatedDate]                 DATETIME      CONSTRAINT [DF_CompanyDealTypeInterest_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]                   VARCHAR (20)  NOT NULL,
    [UpdatedDate]                 DATETIME      CONSTRAINT [DF_CompanyDealTypeInterest_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                   VARCHAR (20)  NULL,
    CONSTRAINT [PK_CompanyDealTypeInterest] PRIMARY KEY CLUSTERED ([CompanyDealTypeInterest_Key] ASC),
    CONSTRAINT [FK_CompanyDealTypeInterest_Company] FOREIGN KEY ([Company_Key]) REFERENCES [dESx].[Company] ([Company_Key]),
    CONSTRAINT [FK_CompanyDealTypeInterest_DealType] FOREIGN KEY ([DealType_Code]) REFERENCES [dESx].[DealType] ([DealType_Code])
);


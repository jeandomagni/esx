﻿CREATE TABLE [dESx].[DealTeamRole] (
    [DealTeamRole_Code] NVARCHAR (10) NOT NULL,
	[Description]       NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_DealTeamRole] PRIMARY KEY CLUSTERED ([DealTeamRole_Code] ASC)
);


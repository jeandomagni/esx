﻿CREATE TABLE [dESx].[DealContactStatus] (
    [DealContactStatus_Key] INT           IDENTITY (1, 1) NOT NULL,
    [DealContact_Key]       INT           NOT NULL,
    [ContactStatus_Code]    NVARCHAR (10) NOT NULL,
    [StatusDate]            DATETIME      NOT NULL,
    [CreatedDate]           DATETIME      CONSTRAINT [DF_DealContactStatus_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]             VARCHAR (20)  NOT NULL,
    [UpdatedDate]           DATETIME      CONSTRAINT [DF_DealContactStatus_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]             VARCHAR (20)  NULL,
    CONSTRAINT [PK_DealContactStatus] PRIMARY KEY CLUSTERED ([DealContactStatus_Key] ASC),
    CONSTRAINT [FK_DealContactStatus_DealContact] FOREIGN KEY ([DealContact_Key]) REFERENCES [dESx].[DealContact] ([DealContact_Key])
);


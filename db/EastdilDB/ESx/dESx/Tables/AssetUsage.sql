﻿CREATE TABLE [dESx].[AssetUsage] (
    [AssetUsage_Key]     INT          IDENTITY (1, 1) NOT NULL,
    [Asset_Key]          INT          NOT NULL,
    [AssetUsageType_Key] INT          NOT NULL,
    [EndDate]            DATETIME     NULL,
    [IsPrimary]          BIT          DEFAULT ((0)) NULL,
    [IsCurrent]          AS           (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]        DATETIME     CONSTRAINT [DF_AssetUsage_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]          VARCHAR (20) NOT NULL,
    [UpdatedDate]        DATETIME     CONSTRAINT [DF_AssetUsage_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]          VARCHAR (20) NULL,
    CONSTRAINT [PK_AssetUsage] PRIMARY KEY CLUSTERED ([AssetUsage_Key] ASC),
    CONSTRAINT [FK_AssetUsage_Asset] FOREIGN KEY ([Asset_Key]) REFERENCES [dESx].[Asset] ([Asset_Key]),
    CONSTRAINT [FK_AssetUsage_AssetUsageType] FOREIGN KEY ([AssetUsageType_Key]) REFERENCES [dESx].[AssetUsageType] ([AssetUsageType_Key])
);




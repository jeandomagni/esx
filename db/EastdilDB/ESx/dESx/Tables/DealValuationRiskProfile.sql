﻿CREATE TABLE [dESx].[DealValuationRiskProfile]
(
	[DealValuationRiskProfile_Code] NVARCHAR(10) NOT NULL PRIMARY KEY, 
    [Description] VARCHAR(50) NOT NULL, 
    [Sort] INT NULL
)

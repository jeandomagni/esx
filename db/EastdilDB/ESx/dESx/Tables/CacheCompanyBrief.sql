﻿CREATE TABLE [dESx].[CacheCompanyBrief] (
    [CacheSessionQuery_Key] INT NOT NULL,
    [Matching__Alias_Key]   INT NOT NULL,
    [Canonical__Alias_Key]  INT NOT NULL,
    CONSTRAINT [PK_CacheCompanyBrief] PRIMARY KEY CLUSTERED ([CacheSessionQuery_Key] ASC, [Matching__Alias_Key] ASC),
    CONSTRAINT [FK_CacheCompanyBrief_CacheSessionQuery] FOREIGN KEY ([CacheSessionQuery_Key]) REFERENCES [dESx].[CacheSessionQuery] ([CacheSessionQuery_Key]) ON DELETE CASCADE
);




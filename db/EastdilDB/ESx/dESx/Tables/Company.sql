﻿CREATE TABLE [dESx].[Company] (
    [Company_Key]             INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Division]                NVARCHAR (100) NULL,
    [TickerSymbol]            NVARCHAR (50)  NULL,
    [CibosCode]               NVARCHAR (50)  NULL,
    [FcpaName]                NVARCHAR (50)  NULL,
    [CompanyType]             NVARCHAR (100) NULL,
    [EsClassification]        NVARCHAR (100) NULL,
    [InstitutionType]         NVARCHAR (100) NULL,
    [OfacCheckDate]           DATE           NULL,
    [IsSolicitableDate]       DATE           NULL,
    [IsCanonical]             BIT            CONSTRAINT [DF_Company_IsCanonical] DEFAULT ((1)) NOT NULL,
    [IsDefunct]               BIT            CONSTRAINT [DF_Company_IsDefunct] DEFAULT ((0)) NOT NULL,
    [IsSolicitable]           BIT            CONSTRAINT [DF_Company_IsSolicitable] DEFAULT ((0)) NOT NULL,
    [IsForeignGovernment]     BIT            CONSTRAINT [DF_Company_IsForeignGovernment] DEFAULT ((0)) NOT NULL,
    [IsTrackedByAsiaTeam]     BIT            CONSTRAINT [DF_Company_IsTrackedByAsiaTeam] DEFAULT ((0)) NOT NULL,
    [HasInterestedInDeals]    BIT            CONSTRAINT [DF_Company_IsInterestedInDeals] DEFAULT ((0)) NOT NULL,
    [HasWfsCorrelation]       BIT            CONSTRAINT [DF_Company_HasWfsCorrelation] DEFAULT ((0)) NOT NULL,
    [HasRestrictedVisibility] BIT            CONSTRAINT [DF_Company_HasRestrictedVisibility] DEFAULT ((0)) NOT NULL,
	[IsEastdil]				  BIT			 CONSTRAINT [DF_Company_IsEastdil] DEFAULT ((0)) NOT NULL,
    [RealEstateAllocation]    NUMERIC (5, 2) NULL,
    [CreatedDate]             DATETIME       CONSTRAINT [DF_Company_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]               VARCHAR (20)   NOT NULL,
    [UpdatedDate]             DATETIME       CONSTRAINT [DF_Company_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]               VARCHAR (20)   NOT NULL,
    CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED ([Company_Key] ASC)
);






















﻿CREATE TABLE [dESx].[NoticeSeverityType] (
    [NoticeSeverityType_Key] INT           IDENTITY (1, 1) NOT NULL,
    [SeverityName]           NVARCHAR (10) NOT NULL,
    CONSTRAINT [PK_NoticeSeverityType] PRIMARY KEY CLUSTERED ([NoticeSeverityType_Key] ASC)
);


﻿CREATE TABLE [dESx].[PhysicalAddressType] (
    [PhysicalAddressType_Code] NVARCHAR (10)  NOT NULL,
    [Description]              NVARCHAR (200) CONSTRAINT [DF_PhysicalAddressType_Description] DEFAULT ('') NOT NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_PhysicalAddressType] PRIMARY KEY CLUSTERED ([PhysicalAddressType_Code] ASC)
);


﻿CREATE TABLE [dESx].[UserMentionData] (
    [UserMentionData_Key] INT  IDENTITY (1, 1) NOT NULL,
    [User_Key]            INT  NOT NULL,
    [Mention_Key]         INT  NOT NULL,
    [IsWatched]           BIT  CONSTRAINT [DF_UserMentionData_IsWatched] DEFAULT ((0)) NOT NULL,
    [UseCount]            INT  CONSTRAINT [DF_UserMentionData_UseCount] DEFAULT ((0)) NOT NULL,
    [LastUseDate]         DATE CONSTRAINT [DF_UserMentionData_LastUseDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MentionUsage] PRIMARY KEY CLUSTERED ([UserMentionData_Key] ASC),
    CONSTRAINT [FK_UserMentionData_Mention] FOREIGN KEY ([Mention_Key]) REFERENCES [dESx].[Mention] ([Mention_Key]),
    CONSTRAINT [FK_UserMentionData_User] FOREIGN KEY ([User_Key]) REFERENCES [dESx].[User] ([User_Key])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MentionUsage_User_Mention]
    ON [dESx].[UserMentionData]([User_Key] ASC, [Mention_Key] ASC);


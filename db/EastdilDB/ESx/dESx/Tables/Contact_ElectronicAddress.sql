﻿CREATE TABLE [dESx].[Contact_ElectronicAddress] (
    [Contact_ElectronicAddress_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Contact_Key]                   INT          NOT NULL,
    [ElectronicAddress_Key]         INT          NOT NULL,
    [CreatedDate]                   DATETIME     CONSTRAINT [DF_Contact_ElectronicAddress_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]                     VARCHAR (20) NOT NULL,
    [UpdatedDate]                   DATETIME     CONSTRAINT [DF_Contact_ElectronicAddress_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                     VARCHAR (20) NULL,
    CONSTRAINT [PK_Contact_ElectronicAddress] PRIMARY KEY CLUSTERED ([Contact_ElectronicAddress_Key] ASC),
    CONSTRAINT [FK_Contact_ElectronicAddress_Contact] FOREIGN KEY ([Contact_Key]) REFERENCES [dESx].[Contact] ([Contact_Key]),
    CONSTRAINT [FK_Contact_ElectronicAddress_ElectronicAddress] FOREIGN KEY ([ElectronicAddress_Key]) REFERENCES [dESx].[ElectronicAddress] ([ElectronicAddress_Key])
);




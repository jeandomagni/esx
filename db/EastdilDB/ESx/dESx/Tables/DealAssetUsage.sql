﻿CREATE TABLE [dESx].[DealAssetUsage] (
    [DealAssetUsage_Key] INT          IDENTITY (1, 1) NOT NULL,
    [DealAsset_Key]      INT          NOT NULL,
    [AssetUsageType_Key] INT          NOT NULL,
    [EndDate]            DATETIME     NULL,
    [IsPrimary]          BIT          DEFAULT ((0)) NULL,
    [IsCurrent]          AS           (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]        DATETIME     CONSTRAINT [DF_DealAssetUsage_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]          VARCHAR (20) NOT NULL,
    [UpdatedDate]        DATETIME     CONSTRAINT [DF_DealAssetUsage_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]          VARCHAR (20) NULL,
    PRIMARY KEY CLUSTERED ([DealAssetUsage_Key] ASC),
    CONSTRAINT [FK_DealAssetUsage_AssetUsageType] FOREIGN KEY ([AssetUsageType_Key]) REFERENCES [dESx].[AssetUsageType] ([AssetUsageType_Key]),
    CONSTRAINT [FK_DealAssetUsage_DealAsset] FOREIGN KEY ([DealAsset_Key]) REFERENCES [dESx].[DealAsset] ([DealAsset_Key])
);



﻿CREATE TABLE [dESx].[MarketingListActionType]
(
	[MarketingListAction_Code] NVARCHAR(50) NOT NULL,
    [Description]     NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_MarketingListActionType_Code] PRIMARY KEY CLUSTERED ([MarketingListAction_Code] ASC)
)

﻿CREATE TABLE [dESx].[Tag] (
    [Tag_Key]       [iESx].[TYP_Reference] NOT NULL,
    [CreateDate]    DATE                   CONSTRAINT [DF_Tag_CreateDate] DEFAULT (getdate()) NOT NULL,
    [LastUseDate]   DATE                   CONSTRAINT [DF_Tag_LastUsedDate] DEFAULT (getdate()) NOT NULL,
    [TotalUseCount] INT                    CONSTRAINT [DF_Tag_TotalUseCount] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED ([Tag_Key] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Holds the names of tags associated with comments in (pretty much) any table.', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Tag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Uniquely identifies rows in the table', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Tag', @level2type = N'COLUMN', @level2name = N'Tag_Key';


﻿CREATE TABLE [dESx].[UserPreference] (
    [UserPreference_Key]  INT            IDENTITY (1, 1) NOT NULL,
    [User_Key]            INT            NOT NULL,
    [PreferenceType_Code] NVARCHAR (15)  NOT NULL,
    [PreferenceValue]     NVARCHAR (100) NULL,
    CONSTRAINT [PK_UserPreference] PRIMARY KEY CLUSTERED ([UserPreference_Key] ASC),
    CONSTRAINT [FK_UserPreference_PreferenceType] FOREIGN KEY ([PreferenceType_Code]) REFERENCES [dESx].[PreferenceType] ([PreferenceType_Code]),
    CONSTRAINT [FK_UserPreference_User] FOREIGN KEY ([User_Key]) REFERENCES [dESx].[User] ([User_Key])
);


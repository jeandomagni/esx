﻿CREATE TABLE [dESx].[AssetType] (
    [AssetType_Code] NVARCHAR (10)  NOT NULL,
    [Description]     NVARCHAR (200) NOT NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_AssetClass] PRIMARY KEY CLUSTERED ([AssetType_Code] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Property,Park,Loan', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'AssetType';


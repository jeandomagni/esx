﻿CREATE TABLE [dESx].[Reminder_Assignee]
(
	[Reminder_Assignee_Key] INT IDENTITY (1, 1) NOT NULL,
	[Reminder_Key] INT NOT NULL,
	[Assignee_User_Key] INT NOT NULL,
	[Assigner_User_Key] INT NOT NULL,
	[CreateTime]            DATETIME       DEFAULT (getdate()) NOT NULL,
	[UpdateTime]            DATETIME       DEFAULT (getdate()) NOT NULL,
	CONSTRAINT [PK_Reminder_Assignee_Key] PRIMARY KEY CLUSTERED ([Reminder_Assignee_Key] ASC),
	CONSTRAINT [FK_Reminder_Assignee_Reminder] FOREIGN KEY ([Reminder_Key]) REFERENCES [dESx].[Reminder] ([Reminder_Key]),
	CONSTRAINT [FK_Reminder_Assignee_Employee] FOREIGN KEY ([Assignee_User_Key]) REFERENCES [dESx].[User] ([User_Key]),
	CONSTRAINT [FK_Reminder_Assigner_Employee] FOREIGN KEY ([Assigner_User_Key]) REFERENCES [dESx].[User] ([User_Key])
)

﻿CREATE TABLE [dESx].[Office_ElectronicAddress] (
    [Office_ElectronicAddress_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Office_Key]                   INT          NOT NULL,
    [ElectronicAddress_Key]        INT          NOT NULL,
    [CreatedDate]                  DATETIME     CONSTRAINT [DF_Office_ElectronicAddress_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]                    VARCHAR (20) NOT NULL,
    [UpdatedDate]                  DATETIME     CONSTRAINT [DF_Office_ElectronicAddress_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                    VARCHAR (20) NULL,
    CONSTRAINT [PK_Office_ElectronicAddress] PRIMARY KEY CLUSTERED ([Office_ElectronicAddress_Key] ASC),
    CONSTRAINT [FK_Office_ElectronicAddress_ElectronicAddress] FOREIGN KEY ([ElectronicAddress_Key]) REFERENCES [dESx].[ElectronicAddress] ([ElectronicAddress_Key]),
    CONSTRAINT [FK_Office_ElectronicAddress_Office] FOREIGN KEY ([Office_Key]) REFERENCES [dESx].[Office] ([Office_Key])
);




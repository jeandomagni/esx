﻿CREATE TABLE [dESx].[LoanBenchmarkType] (
    [LoanBenchmarkType_Code] NVARCHAR (10) NOT NULL,
    [Description]            NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_LoanBenchmarkType] PRIMARY KEY CLUSTERED ([LoanBenchmarkType_Code] ASC)
);


﻿CREATE TABLE [dESx].[MarketingListStatusType]
(
	[MarketingListStatus_Code] NVARCHAR(50) NOT NULL,
    [Description]     NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_MarketingListStatusType_Code] PRIMARY KEY CLUSTERED ([MarketingListStatus_Code] ASC)
)

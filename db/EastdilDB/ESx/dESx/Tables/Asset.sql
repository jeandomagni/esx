﻿CREATE TABLE [dESx].[Asset] (
    [Asset_Key]             INT           IDENTITY (1, 1) NOT NULL,
    [AssetType_Code]       NVARCHAR (10) NULL,
    [CreatedDate]           DATETIME      CONSTRAINT [DF_Asset_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]             VARCHAR (20)  NOT NULL,
    [UpdatedDate]           DATETIME      CONSTRAINT [DF_Asset_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]             VARCHAR (20)  NULL,
    [PhysicalAddress_Key]   INT           NULL,
    [Park_Key] INT NULL, 
    CONSTRAINT [PK_Property] PRIMARY KEY CLUSTERED ([Asset_Key] ASC),
    CONSTRAINT [FK_Asset_AssetType] FOREIGN KEY ([AssetType_Code]) REFERENCES [dESx].[AssetType] ([AssetType_Code]),
    CONSTRAINT [FK_Asset_PhysicalAddress] FOREIGN KEY ([PhysicalAddress_Key]) REFERENCES [dESx].[PhysicalAddress] ([PhysicalAddress_Key]), 
    CONSTRAINT [FK_Asset_Park] FOREIGN KEY ([Park_Key]) REFERENCES [dESx].[Park]([Park_Key])
);




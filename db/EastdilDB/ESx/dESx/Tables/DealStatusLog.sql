﻿CREATE TABLE [dESx].[DealStatusLog] (
    [DealStatusLog_Key] INT           IDENTITY (1, 1) NOT NULL,
    [Deal_Key]          INT           NOT NULL,
    [DealStatus_Code]   NVARCHAR (50) NOT NULL,
    [EndDate]           DATETIME      NULL,
    [IsCurrent]         AS            (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]       DATETIME      DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]         VARCHAR (20)  NOT NULL,
    [UpdatedDate]       DATETIME      CONSTRAINT [DF_DealStatusLog_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]         VARCHAR (20)  NULL,
    PRIMARY KEY CLUSTERED ([DealStatusLog_Key] ASC),
    CONSTRAINT [FK_DealStatusLog_Deal] FOREIGN KEY ([Deal_Key]) REFERENCES [dESx].[Deal] ([Deal_Key]),
    CONSTRAINT [FK_DealStatusLog_DealStatus] FOREIGN KEY ([DealStatus_Code]) REFERENCES [dESx].[DealStatus] ([DealStatus_Code])
);



GO

CREATE INDEX [IX_DealStatusLog_CreatedDate] ON [dESx].[DealStatusLog] ([CreatedDate])

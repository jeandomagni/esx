﻿CREATE TABLE [dESx].[MarketingListContact]
(
	[MarketingListContact_Key] INT IDENTITY (1, 1) NOT NULL, 
    [MarketingList_Key] INT				NOT NULL,
	[Contact_Key]		INT             NOT NULL,
    [IsPrimaryContact]         BIT             CONSTRAINT [DF_MarketingListContact_IsPrimaryContact] DEFAULT ((0)) NOT NULL,
    [IsSolicitable]     BIT             CONSTRAINT [DF_MarketingListContact_Solicitable] DEFAULT ((0)) NOT NULL,
    [IsRequestingTeaser]                BIT     CONSTRAINT [DF_MarketingListContact_IsRequestingTeaser] DEFAULT ((0)) NOT NULL,
    [IsRequestingWarRoomAccess]         BIT     CONSTRAINT [DF_MarketingListContact_IsRequestingWarRoomAccess] DEFAULT ((0)) NOT NULL,
    [IsRequestingAdditionalMaterials]   BIT     CONSTRAINT [DF_MarketingListContact_IsRequestingAdditionalMaterials] DEFAULT ((0)) NOT NULL,
    [IsRequestingPrintedOm]             BIT             CONSTRAINT [DF_MarketingListContact_IsRequestingPrintedOm] DEFAULT ((0)) NOT NULL,
    [IsRequestingCallForOffers]         BIT             CONSTRAINT [DF_MarketingListContact_RequestingCallForOffers] DEFAULT ((0)) NOT NULL,
	[CreatedDate]		DATETIME        CONSTRAINT [DF_MarketingListContact_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]			VARCHAR (20)    NOT NULL,
    [UpdatedDate]		DATETIME        CONSTRAINT [DF_MarketingListContact_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]			VARCHAR (20)    NULL,
	CONSTRAINT [PK_MarketingListContact] PRIMARY KEY CLUSTERED ([MarketingListContact_Key] ASC),
    CONSTRAINT [FK_MarketingListContact_MarketingList] FOREIGN KEY ([MarketingList_Key]) REFERENCES [dESx].[MarketingList] ([MarketingList_Key]),
    CONSTRAINT [FK_MarketingListContact_Contact] FOREIGN KEY ([Contact_Key]) REFERENCES [dESx].[Contact] ([Contact_Key])
)

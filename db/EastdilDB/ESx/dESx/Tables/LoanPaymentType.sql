﻿CREATE TABLE [dESx].[LoanPaymentType] (
    [LoanPaymentType_Code] NVARCHAR (10) NOT NULL,
    [Description]          NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_LoanPaymentType] PRIMARY KEY CLUSTERED ([LoanPaymentType_Code] ASC)
);


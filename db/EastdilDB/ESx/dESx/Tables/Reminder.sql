﻿CREATE TABLE [dESx].[Reminder] (
    [Reminder_Key]     INT             IDENTITY (1, 1) NOT NULL,
    [Comment_Key]      INT             NULL,
    [Mention_Key]      INT             NULL,
    [ReminderDate]     DATETIME        NULL,
    [ReminderText]     NVARCHAR (1000) NULL,
    [IsDone]           BIT             DEFAULT ((0)) NOT NULL,
    [IsSnoozed]        BIT             DEFAULT ((0)) NOT NULL,
    [IsDeleted]        BIT             DEFAULT ((0)) NOT NULL,
    [Create__User_Key] INT             NOT NULL,
    [CreateTime]       DATETIME        DEFAULT (getdate()) NOT NULL,
    [Update__User_Key] INT             NOT NULL,
    [UpdateTime]       DATETIME        DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Reminder_Key] PRIMARY KEY CLUSTERED ([Reminder_Key] ASC),
    CONSTRAINT [FK_Reminder_Comment] FOREIGN KEY ([Comment_Key]) REFERENCES [dESx].[Comment] ([Comment_Key]),
    CONSTRAINT [FK_Reminder_Mention] FOREIGN KEY ([Mention_Key]) REFERENCES [dESx].[Mention] ([Mention_Key]),
    CONSTRAINT [FK_Reminder_User] FOREIGN KEY ([Create__User_Key]) REFERENCES [dESx].[User] ([User_Key]),
    CONSTRAINT [FK_Reminder_User1] FOREIGN KEY ([Update__User_Key]) REFERENCES [dESx].[User] ([User_Key])
);




﻿CREATE TABLE [dESx].[AssetUsageType] (
    [AssetUsageType_Key] INT            IDENTITY (1, 1) NOT NULL,
    [UsageType]          NVARCHAR (50)  NOT NULL,
    [SpaceType]          NVARCHAR (10)  NOT NULL,
    [Description]        NVARCHAR (200) NOT NULL,
    [CreatedDate]        DATETIME       CONSTRAINT [DF_AssetUsageType_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]          VARCHAR (20)   NOT NULL,
    [UpdatedDate]        DATETIME       CONSTRAINT [DF_AssetUsageType_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]          VARCHAR (20)   NULL,
    CONSTRAINT [PK_PropertyType] PRIMARY KEY CLUSTERED ([AssetUsageType_Key] ASC)
);


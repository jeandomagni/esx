﻿CREATE TABLE [dESx].[CacheSessionQuery] (
    [CacheSessionQuery_Key] INT             IDENTITY (1, 1) NOT NULL,
    [CacheAccessToken_Key]  INT             NOT NULL,
    [QueryType]             NVARCHAR (128)  NOT NULL,
    [FilterText]            NVARCHAR (1000) NOT NULL,
    [TotalFilter]           NVARCHAR (1000) NOT NULL,
    [CreateTime]            DATETIME2 (0)   CONSTRAINT [DF_CacheSessionQuery_CreationTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SessionQueryCache] PRIMARY KEY CLUSTERED ([CacheSessionQuery_Key] ASC),
    CONSTRAINT [FK_CacheSessionQuery_CacheAccessToken] FOREIGN KEY ([CacheAccessToken_Key]) REFERENCES [dESx].[CacheAccessToken] ([CacheAccessToken_Key]) ON DELETE CASCADE
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_SessionQueryCountCache_AccessToken_QueryType]
    ON [dESx].[CacheSessionQuery]([CacheAccessToken_Key] ASC, [QueryType] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Insures that only one value per query type is cached per session (access token)', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'CacheSessionQuery', @level2type = N'INDEX', @level2name = N'UIX_SessionQueryCountCache_AccessToken_QueryType';


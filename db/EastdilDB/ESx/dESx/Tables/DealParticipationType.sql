﻿CREATE TABLE [dESx].[DealParticipationType](
	[DealParticipationType_Code]  NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (200) NOT NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_DealParticipationType] PRIMARY KEY CLUSTERED ([DealParticipationType_Code] ASC)
);

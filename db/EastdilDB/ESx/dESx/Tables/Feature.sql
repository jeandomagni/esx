﻿CREATE TABLE [dESx].[Feature]
(
	[Feature_Code] NVARCHAR(50) NOT NULL,
	[Description] NVARCHAR(200) NOT NULL
	CONSTRAINT [PK_Feature] PRIMARY KEY CLUSTERED ([Feature_Code] ASC), 
    [Sort] INT NULL 
);

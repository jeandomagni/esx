﻿CREATE TABLE [dESx].[LoanSourceType] (
    [LoanSourceType_Code] NVARCHAR (10) NOT NULL,
    [Description]         NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_LoanSourceType] PRIMARY KEY CLUSTERED ([LoanSourceType_Code] ASC)
);


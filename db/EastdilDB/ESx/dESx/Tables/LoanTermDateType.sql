﻿CREATE TABLE [dESx].[LoanTermDateType] (
    [LoanTermDateType_Code] NVARCHAR(20) NOT NULL,
    [Description]          NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_LoanTermDateType] PRIMARY KEY CLUSTERED ([LoanTermDateType_Code] ASC)
);


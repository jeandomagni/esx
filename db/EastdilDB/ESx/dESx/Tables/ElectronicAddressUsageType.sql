﻿CREATE TABLE [dESx].[ElectronicAddressUsageType] (
    [AddressUsageType_Code]      NVARCHAR (15) NOT NULL,
	[ElectronicAddressType_Code] NVARCHAR (15) NOT NULL,
	[Description]       NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_ElectronicAddressTypeUsage] PRIMARY KEY CLUSTERED ([ElectronicAddressType_Code] ASC, [AddressUsageType_Code] ASC)
);


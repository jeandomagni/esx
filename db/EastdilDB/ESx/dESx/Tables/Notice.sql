﻿CREATE TABLE [dESx].[Notice] (
    [Notice_Key]             INT            IDENTITY (1, 1) NOT NULL,
    [NoticeSeverityType_Key] INT            NOT NULL,
    [Generating__User_Key]   INT            NOT NULL,
    [Subject__Mention_Key]   INT            NOT NULL,
    [NoticeText]             NVARCHAR (MAX) NOT NULL,
    [CreatedDate]            DATETIME       CONSTRAINT [DF_Notice_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]              VARCHAR (20)   NOT NULL,
    [UpdatedDate]            DATETIME       CONSTRAINT [DF_Notice_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]              VARCHAR (20)   NULL,
    CONSTRAINT [PK_Notice] PRIMARY KEY CLUSTERED ([Notice_Key] ASC),
    CONSTRAINT [FK_Notice_Mention] FOREIGN KEY ([Subject__Mention_Key]) REFERENCES [dESx].[Mention] ([Mention_Key]),
    CONSTRAINT [FK_Notice_NoticeSeverityType] FOREIGN KEY ([NoticeSeverityType_Key]) REFERENCES [dESx].[NoticeSeverityType] ([NoticeSeverityType_Key]),
    CONSTRAINT [FK_Notice_User] FOREIGN KEY ([Generating__User_Key]) REFERENCES [dESx].[User] ([User_Key])
);














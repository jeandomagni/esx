﻿CREATE TABLE [dESx].[UserDelegate] (
    [UserDelegate_Key]				INT           IDENTITY (1, 1) NOT NULL,
    [User_Key]						INT NOT NULL,
    [Delegate_User_Key]				INT NOT NULL,
    [CreatedDate]					DATETIME      CONSTRAINT [DF_UserDelegate_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]						VARCHAR (20)  NOT NULL,
    [UpdatedDate]					DATETIME      CONSTRAINT [DF_UserDelegate_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]						VARCHAR (20)  NULL,
    CONSTRAINT [PK_UserDelegate] PRIMARY KEY CLUSTERED ([UserDelegate_Key] ASC),
    CONSTRAINT [FK_UserDelegate_User] FOREIGN KEY ([User_Key]) REFERENCES [dESx].[User] ([User_Key]),
    CONSTRAINT [FK_UserDelegate_User2] FOREIGN KEY ([Delegate_User_Key]) REFERENCES [dESx].[User] ([User_Key])
);




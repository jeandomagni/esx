﻿CREATE TABLE [dESx].[DealContactType] (
    [DealContactType_Code] NVARCHAR(25) NOT NULL,
    [Description]     NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_DealContactType_Code] PRIMARY KEY CLUSTERED ([DealContactType_Code] ASC)
);


﻿CREATE TABLE [dESx].[Deal] (
    [Deal_Key]               INT            IDENTITY (1, 1) NOT NULL,
    [DealName]               NVARCHAR (100) NOT NULL,
    [DealCode]               NVARCHAR (25)  NOT NULL,
    [DealType_Code]          NVARCHAR (50)  NOT NULL,
    [LegalEntity_Code]       NVARCHAR (50)  NULL,
    [CreatedDate]            DATETIME       CONSTRAINT [DF_Deal_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]              VARCHAR (20)   NOT NULL,
    [UpdatedDate]            DATETIME       CONSTRAINT [DF_Deal_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]              VARCHAR (20)   NULL,
    [DealFee]                MONEY          NULL,
    [FinalTransactionAmount] MONEY          NULL,
    [Office_Key]             INT            NULL,
    [EastdilOffice_Key]      INT            NULL,
    [LaunchDate]             DATETIME       NULL,
    CONSTRAINT [PK_Deal] PRIMARY KEY CLUSTERED ([Deal_Key] ASC),
    CONSTRAINT [FK_Deal_DealType] FOREIGN KEY ([DealType_Code]) REFERENCES [dESx].[DealType] ([DealType_Code]),
    CONSTRAINT [FK_Deal_EastdilOffice] FOREIGN KEY ([EastdilOffice_Key]) REFERENCES [dESx].[EastdilOffice] ([EastdilOffice_Key]),
    CONSTRAINT [FK_Deal_LegalEntity] FOREIGN KEY ([LegalEntity_Code]) REFERENCES [dESx].[LegalEntity] ([LegalEntity_Code]),
    CONSTRAINT [FK_Deal_Office] FOREIGN KEY ([Office_Key]) REFERENCES [dESx].[Office] ([Office_Key])
);




















GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Deal_DealCode]
    ON [dESx].[Deal]([DealCode] ASC);


﻿CREATE TABLE [dESx].[MarketHierarchy] (
    [MarketHierarchy_Key] INT NOT NULL,
    [Parent__Market_Key]  INT NOT NULL,
    [Child__Market_Key]   INT NOT NULL,
    CONSTRAINT [PK_MarketHierarchy] PRIMARY KEY CLUSTERED ([MarketHierarchy_Key] ASC),
    CONSTRAINT [FK_MarketHierarchy_Market] FOREIGN KEY ([Parent__Market_Key]) REFERENCES [dESx].[Market] ([Market_Key]),
    CONSTRAINT [FK_MarketHierarchy_Market1] FOREIGN KEY ([Child__Market_Key]) REFERENCES [dESx].[Market] ([Market_Key])
);






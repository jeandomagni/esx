﻿CREATE TABLE [dESx].[LogSeverityType] (
    [LogSeverityType_Key] INT           NOT NULL,
    [SeverityName]        NVARCHAR (10) NOT NULL,
    CONSTRAINT [PK_LogSeverityType] PRIMARY KEY CLUSTERED ([LogSeverityType_Key] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Constrains the severity in AppLog entries to specific values.', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'LogSeverityType';


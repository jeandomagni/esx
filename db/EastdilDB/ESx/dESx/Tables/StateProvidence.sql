﻿CREATE TABLE [dESx].[StateProvidence] (
    [StateProvidence_Code] NCHAR (2)     NOT NULL,
	[Description]       NVARCHAR (50) NULL,
    [StateProvidenceName]  NVARCHAR (50) NOT NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_StateProvidence] PRIMARY KEY CLUSTERED ([StateProvidence_Code] ASC)
);


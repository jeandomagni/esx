﻿CREATE TABLE [dESx].[MarketingList]
(
	[MarketingList_Key] INT IDENTITY (1, 1) NOT NULL, 
    [Deal_Key] INT NOT NULL,
    [Name] NVARCHAR(50) NULL,
	[CreatedDate]      DATETIME       CONSTRAINT [DF_MarketingList_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]        VARCHAR (20)   NOT NULL,
    [UpdatedDate]      DATETIME       CONSTRAINT [DF_MarketingList_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]        VARCHAR (20)   NULL,
	CONSTRAINT [PK_MarketingList] PRIMARY KEY CLUSTERED ([MarketingList_Key] ASC),
    CONSTRAINT [FK_MarketingList_Deal] FOREIGN KEY ([Deal_Key]) REFERENCES [dESx].[Deal] ([Deal_Key]),
)

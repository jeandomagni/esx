﻿CREATE TABLE [dESx].[ContactOffice] (
    [ContactOffice_Key]      INT           IDENTITY (1, 1) NOT NULL,
    [Contact_Key]            INT           NOT NULL,
    [Office_Key]             INT           NOT NULL,
    [ContactOfficeRole_Code] NVARCHAR (50) NOT NULL,
    [EndDate]                DATETIME      NULL,
    [IsCurrent]              AS            (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]            DATETIME      CONSTRAINT [DF_ContactOffice_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]              VARCHAR (20)  NOT NULL,
    [UpdatedDate]            DATETIME      CONSTRAINT [DF_ContactOffice_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]              VARCHAR (20)  NULL,
	[IsPrimaryCompanyContact] BIT NULL,
    CONSTRAINT [PK_OfficeContact] PRIMARY KEY CLUSTERED ([ContactOffice_Key] ASC),
    CONSTRAINT [FK_ContactOffice_Contact] FOREIGN KEY ([Contact_Key]) REFERENCES [dESx].[Contact] ([Contact_Key]),
    CONSTRAINT [FK_ContactOffice_Office] FOREIGN KEY ([Office_Key]) REFERENCES [dESx].[Office] ([Office_Key])
);










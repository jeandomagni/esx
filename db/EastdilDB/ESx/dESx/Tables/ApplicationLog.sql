CREATE TABLE [dESx].[ApplicationLog] (
    [ApplicationLog_Key]  INT              IDENTITY (1, 1) NOT NULL,
    [LogSeverityType_Key] INT              NOT NULL,
    [InstanceID]          UNIQUEIDENTIFIER NOT NULL,
    [ApplicationName]     NVARCHAR (64)    NOT NULL,
    [ApplicationLocation] NVARCHAR (256)   NOT NULL,
    [EventNumber]         INT              NOT NULL,
    [EventText]           NVARCHAR (MAX)   NOT NULL,
    [AdditionalData]      NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_ApplicationLog] PRIMARY KEY CLUSTERED ([ApplicationLog_Key] ASC),
    CONSTRAINT [FK_ApplicationLog_LogSeverityType] FOREIGN KEY ([LogSeverityType_Key]) REFERENCES [dESx].[LogSeverityType] ([LogSeverityType_Key])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A number associated with the event location or text for programatic use.', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'ApplicationLog', @level2type = N'COLUMN', @level2name = N'EventNumber';


﻿CREATE TABLE [dESx].[CompanyHierarchy] (
    [CompanyHierarchy_Key]   INT          IDENTITY (1, 1) NOT NULL,
    [Child__Company_Key]     INT          NOT NULL,
    [Parent__Company_Key]    INT          NOT NULL,
    [CreatedDate]            DATETIME     CONSTRAINT [DF_CompanyHierarchy_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]              VARCHAR (20) NOT NULL,
    [UpdatedDate]            DATETIME     CONSTRAINT [DF_CompanyHierarchy_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]              VARCHAR (20) NULL,
    [TopParent__Company_Key] INT          NULL,
    CONSTRAINT [PK_JointVenture] PRIMARY KEY CLUSTERED ([CompanyHierarchy_Key] ASC),
    CONSTRAINT [FK_CompanyHierarchy_Comany2] FOREIGN KEY ([TopParent__Company_Key]) REFERENCES [dESx].[Company] ([Company_Key]),
    CONSTRAINT [FK_CompanyHierarchy_Company] FOREIGN KEY ([Child__Company_Key]) REFERENCES [dESx].[Company] ([Company_Key]),
    CONSTRAINT [FK_CompanyHierarchy_Company1] FOREIGN KEY ([Parent__Company_Key]) REFERENCES [dESx].[Company] ([Company_Key])
);








﻿CREATE TABLE [dESx].[Notice_Mention] (
    [Notice_Mention_Key] INT IDENTITY (1, 1) NOT NULL,
    [Notice_Key]         INT NOT NULL,
    [Mention_Key]        INT NOT NULL,
    CONSTRAINT [PK_NoticeLog_Mention] PRIMARY KEY CLUSTERED ([Notice_Mention_Key] ASC),
    CONSTRAINT [FK_Notice_Mention_Mention] FOREIGN KEY ([Mention_Key]) REFERENCES [dESx].[Mention] ([Mention_Key]),
    CONSTRAINT [FK_Notice_Mention_Notice] FOREIGN KEY ([Notice_Key]) REFERENCES [dESx].[Notice] ([Notice_Key])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Records objects of a notice.  None are required (for example a contact is inserted)', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Notice_Mention';


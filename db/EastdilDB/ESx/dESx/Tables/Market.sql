﻿CREATE TABLE [dESx].[Market] (
    [Market_Key]       INT               IDENTITY (1, 1) NOT NULL,
    [MarketName]       NVARCHAR (100)    NOT NULL,
    [MarketType_Code]  NVARCHAR (25)     NOT NULL,
    [MarketBoundaries] [sys].[geography] NULL,
    [CreatedDate]      DATETIME          CONSTRAINT [DF_Market_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]        VARCHAR (20)      NOT NULL,
    [UpdatedDate]      DATETIME          CONSTRAINT [DF_Market_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]        VARCHAR (20)      NULL,
    CONSTRAINT [PK_Market] PRIMARY KEY CLUSTERED ([Market_Key] ASC),
    CONSTRAINT [FK_Market_MarketType] FOREIGN KEY ([MarketType_Code]) REFERENCES [dESx].[MarketType] ([MarketType_Code])
);








GO
CREATE NONCLUSTERED INDEX [UIX_Market_Name]
    ON [dESx].[Market]([MarketName] ASC);


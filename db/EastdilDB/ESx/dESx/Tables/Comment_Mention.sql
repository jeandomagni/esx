﻿CREATE TABLE [dESx].[Comment_Mention] (
    [Comment_Mention_Key] INT IDENTITY (1, 1) NOT NULL,
    [Comment_Key]         INT NOT NULL,
    [Mention_Key]         INT NOT NULL,
    CONSTRAINT [PK_Note_Mention] PRIMARY KEY CLUSTERED ([Comment_Mention_Key] ASC),
    CONSTRAINT [FK_Comment_Mention_Comment] FOREIGN KEY ([Comment_Key]) REFERENCES [dESx].[Comment] ([Comment_Key]),
    CONSTRAINT [FK_Comment_Mention_Mention] FOREIGN KEY ([Mention_Key]) REFERENCES [dESx].[Mention] ([Mention_Key])
);




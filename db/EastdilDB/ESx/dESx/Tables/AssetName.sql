﻿CREATE TABLE [dESx].[AssetName] (
    [AssetName_Key] INT            IDENTITY (1, 1)NOT NULL,
    [Asset_Key]     INT            NOT NULL,
    [AssetName]     NVARCHAR (150) NOT NULL,
    [EndDate]       DATETIME       NULL,
    [IsCurrent]     AS             (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]   DATETIME       CONSTRAINT [DF_AssetName_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]     VARCHAR (20)   NOT NULL,
    [UpdatedDate]   DATETIME       CONSTRAINT [DF_AssetName_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]     VARCHAR (20)   NULL,
    CONSTRAINT [PK_AssetName] PRIMARY KEY CLUSTERED ([AssetName_Key] ASC),
    CONSTRAINT [FK_AssetName_Asset] FOREIGN KEY ([Asset_Key]) REFERENCES [dESx].[Asset] ([Asset_Key])
);


﻿CREATE TABLE [dESx].[Comment] (
    [Comment_Key]           INT            IDENTITY (1, 1) NOT NULL,
    [Implicit__Mention_Key] INT            NOT NULL,
    [CommentText]           NVARCHAR (MAX) NOT NULL,
    [Create__User_Key]      INT            NOT NULL,
    [CreateTime]            DATETIME       CONSTRAINT [DF_Note_CreateTime] DEFAULT (getdate()) NOT NULL,
    [Update__User_Key]      INT            NOT NULL,
    [UpdateTime]            DATETIME       CONSTRAINT [DF_Note_UpdateTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Note] PRIMARY KEY CLUSTERED ([Comment_Key] ASC),
    CONSTRAINT [CK_Note_NoteText] CHECK (datalength([CommentText])>(4)),
    CONSTRAINT [FK_Comment_User] FOREIGN KEY ([Create__User_Key]) REFERENCES [dESx].[User] ([User_Key]),
    CONSTRAINT [FK_Comment_User1] FOREIGN KEY ([Update__User_Key]) REFERENCES [dESx].[User] ([User_Key]),
    CONSTRAINT [FK_Note_Mention] FOREIGN KEY ([Implicit__Mention_Key]) REFERENCES [dESx].[Mention] ([Mention_Key])
);




GO
CREATE NONCLUSTERED INDEX [IX_Note_Primary$Mention]
    ON [dESx].[Comment]([Implicit__Mention_Key] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'All entities have implicit @tags', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Comment', @level2type = N'COLUMN', @level2name = N'Implicit__Mention_Key';


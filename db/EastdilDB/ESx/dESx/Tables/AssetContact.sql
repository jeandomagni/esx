﻿CREATE TABLE [dESx].[AssetContact] (
    [AssetContact_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Asset_Key]        INT          NULL,
    [Contact_Key]      INT          NOT NULL,
    [EndDate]          DATETIME     NULL,
    [IsCurrent]        AS           (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [AssetContactType_Code]  NVARCHAR(20)          NOT NULL,
    [CreatedDate]      DATETIME     CONSTRAINT [DF_AssetContact_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]        VARCHAR (20) NOT NULL,
    [UpdatedDate]      DATETIME     CONSTRAINT [DF_AssetContact_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]        VARCHAR (20) NULL,
    CONSTRAINT [PK_PropertyContact] PRIMARY KEY CLUSTERED ([AssetContact_Key] ASC),
    CONSTRAINT [FK_AssetContact_Asset] FOREIGN KEY ([Asset_Key]) REFERENCES [dESx].[Asset] ([Asset_Key]),
    CONSTRAINT [FK_AssetContact_ContactRole] FOREIGN KEY ([AssetContactType_Code]) REFERENCES [dESx].[AssetContactType] ([AssetContactType_Code]),
    CONSTRAINT [FK_PropertyContact_Contact] FOREIGN KEY ([Contact_Key]) REFERENCES [dESx].[Contact] ([Contact_Key])
);




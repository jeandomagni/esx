﻿CREATE TABLE [dESx].[AssetLoanProperty]
(
	[AssetLoanProperty_Key] INT NOT NULL PRIMARY KEY IDENTITY, 
    [PropertyAsset_Key] INT NOT NULL, 
    [LoanAsset_Key] INT NOT NULL, 
    CONSTRAINT [FK_AssetLoanProperty_Asset] FOREIGN KEY ([PropertyAsset_Key]) REFERENCES [dESx].[Asset]([Asset_Key]), 
    CONSTRAINT [FK_AssetLoanProperty_LoanAsset] FOREIGN KEY ([LoanAsset_Key]) REFERENCES [dESx].[Asset]([Asset_Key])
)

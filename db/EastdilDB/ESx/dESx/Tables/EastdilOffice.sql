﻿CREATE TABLE [dESx].[EastdilOffice] (
    [EastdilOffice_Key]   INT            IDENTITY (1, 1) NOT NULL,
    [EastdilOfficeName]   NVARCHAR (200) NOT NULL,
    [PhysicalAddress_Key] INT            NULL,
    [CreatedDate]         DATETIME       CONSTRAINT [DF_EastdilOffice_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]           VARCHAR (20)   NOT NULL,
    [UpdatedDate]         DATETIME       CONSTRAINT [DF_EastdilOffice_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]           VARCHAR (20)   NULL,
    CONSTRAINT [PK_EastdilOffice] PRIMARY KEY CLUSTERED ([EastdilOffice_Key] ASC),
    CONSTRAINT [FK_EastdilOffice_PhysicalAddress] FOREIGN KEY ([PhysicalAddress_Key]) REFERENCES [dESx].[PhysicalAddress] ([PhysicalAddress_Key])
);






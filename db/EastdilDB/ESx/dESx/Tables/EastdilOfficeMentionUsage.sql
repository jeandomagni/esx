﻿CREATE TABLE [dESx].[EastdilOfficeMentionUsage] (
    [EastdilOfficeMentionUsage_Key] INT          IDENTITY (1, 1) NOT NULL,
    [EastdilOffice_Key]             INT          NOT NULL,
    [Mention_key]                   INT          NOT NULL,
    [UseCount]                      INT          NOT NULL,
    [LastUseDate]                   DATE         NOT NULL,
    [CreatedDate]                   DATETIME     CONSTRAINT [DF_EastdilOfficeMentionUsage_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]                     VARCHAR (20) NOT NULL,
    [UpdatedDate]                   DATETIME     CONSTRAINT [DF_EastdilOfficeMentionUsage_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                     VARCHAR (20) NULL,
    CONSTRAINT [PK_EastdilOfficeMentionUsage] PRIMARY KEY CLUSTERED ([EastdilOfficeMentionUsage_Key] ASC),
    CONSTRAINT [FK_EastdilOfficeMentionUsage_EastdilOffice] FOREIGN KEY ([EastdilOffice_Key]) REFERENCES [dESx].[EastdilOffice] ([EastdilOffice_Key]),
    CONSTRAINT [FK_EastdilOfficeMentionUsage_Mention] FOREIGN KEY ([Mention_key]) REFERENCES [dESx].[Mention] ([Mention_Key])
);






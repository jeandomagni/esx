﻿CREATE TABLE [dESx].[DealType] (
    [DealType_Code] NVARCHAR (50)  NOT NULL,
    [Description]   NVARCHAR (200) NOT NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_DealType] PRIMARY KEY CLUSTERED ([DealType_Code] ASC)
);




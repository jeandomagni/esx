﻿CREATE TABLE [dESx].[DealStatus] (
    [DealStatus_Code] NVARCHAR (50)  NOT NULL,
    [Description]     NVARCHAR (200) NOT NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_DealStatus_1] PRIMARY KEY CLUSTERED ([DealStatus_Code] ASC)
);




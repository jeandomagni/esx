﻿CREATE TABLE [dESx].[CompanyAlias] (
    [CompanyAlias_Key] INT            IDENTITY (1, 1) NOT NULL,
    [Company_Key]      INT            NOT NULL,
    [AliasName]        NVARCHAR (128) NOT NULL,
    [IsLegal]          BIT            CONSTRAINT [DF_Alias_IsLegal] DEFAULT ((1)) NOT NULL,
    [IsPrimary]        BIT            CONSTRAINT [DF_Alias_IsCannonical] DEFAULT ((1)) NOT NULL,
    [IsDefunct]        BIT            CONSTRAINT [DF_Alias_IsDefunct] DEFAULT ((0)) NOT NULL,
    [CreatedDate]      DATETIME       CONSTRAINT [DF_CompanyAlias_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]        VARCHAR (20)   NOT NULL,
    [UpdatedDate]      DATETIME       CONSTRAINT [DF_CompanyAlias_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]        VARCHAR (20)   NULL,
    CONSTRAINT [PK_Alias] PRIMARY KEY CLUSTERED ([CompanyAlias_Key] ASC),
    CONSTRAINT [CK_Alias_NotLegalOrCanonicalIfDefunct] CHECK (case [IsDefunct] when (0) then (0) else CONVERT([int],[IsLegal],(0))+CONVERT([int],[IsPrimary],(0)) end=(0)),
    CONSTRAINT [FK_Alias_Company] FOREIGN KEY ([Company_Key]) REFERENCES [dESx].[Company] ([Company_Key])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Alias_Company_IsLegal]
    ON [dESx].[CompanyAlias]([Company_Key] ASC, [IsLegal] ASC) WHERE ([IsLegal]=(1));


GO
CREATE NONCLUSTERED INDEX [UIX_Alias_Company_IsPrimary]
    ON [dESx].[CompanyAlias]([Company_Key] ASC, [IsPrimary] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Alias_AliasName]
    ON [dESx].[CompanyAlias]([AliasName] ASC);


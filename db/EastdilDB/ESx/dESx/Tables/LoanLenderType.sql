﻿CREATE TABLE [dESx].[LoanLenderType] (
    [LoanLenderType_Code] NVARCHAR (10) NOT NULL,
    [Description]         NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_LoanLenderType] PRIMARY KEY CLUSTERED ([LoanLenderType_Code] ASC)
);


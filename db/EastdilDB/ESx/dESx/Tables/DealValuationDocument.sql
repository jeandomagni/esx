﻿CREATE TABLE [dESx].[DealValuationDocument] (
    [DealValuationDocument_Key] INT          IDENTITY (1, 1) NOT NULL,
    [DealValuation_Key]         INT          NOT NULL,
    [Document_Key]    INT          NOT NULL,
    [CreatedDate]     DATETIME     CONSTRAINT [DF_BidDocument_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]       VARCHAR (20) NOT NULL,
    [UpdatedDate]     DATETIME     CONSTRAINT [DF_BidDocument_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]       VARCHAR (20) NULL,
    CONSTRAINT [PK_DealValuationDocument] PRIMARY KEY CLUSTERED ([DealValuationDocument_Key] ASC),
    CONSTRAINT [FK_DealValuationDocument_DealValuation] FOREIGN KEY ([DealValuation_Key]) REFERENCES [dESx].[DealValuation] ([DealValuation_Key]), 
    CONSTRAINT [FK_DealValuationDocument_Document] FOREIGN KEY ([Document_Key]) REFERENCES [dESx].[Document]([Document_Key])
);


﻿CREATE TABLE [dESx].[DealValuationAssetUsageValue]
(
	[DealValuationAssetUsageValue_Key] INT NOT NULL PRIMARY KEY IDENTITY, 
	[DealValuation_Key] INT NOT NULL,
    [AssetUsageType_Key] INT NULL, 
    [Spaces] INT NULL, 
    [SpaceType] NVARCHAR(10) NULL, 
    [Value] MONEY NULL, 
    CONSTRAINT [FK_DealValuationAssetUsageValue_DealValuation] FOREIGN KEY ([DealValuation_Key]) REFERENCES [dESx].[DealValuation]([DealValuation_Key]), 
    CONSTRAINT [FK_DealValuationAssetUsageValue_AssetUsageType] FOREIGN KEY ([AssetUsageType_Key]) REFERENCES [dESx].[AssetUsageType]([AssetUsageType_Key])
)

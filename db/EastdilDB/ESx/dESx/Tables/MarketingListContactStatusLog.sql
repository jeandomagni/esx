﻿CREATE TABLE [dESx].[MarketingListContactStatusLog] (
    [MarketingListContactStatusLog_Key] INT           IDENTITY (1, 1) NOT NULL,
    [MarketingListContact_Key]          INT           NOT NULL,
    [MarketingListStatus_Code]   NVARCHAR (50) NOT NULL,
    [StatusDate]        DATETIME      NOT NULL,
    [IsCurrent]         AS            (isnull(CONVERT([bit],case when [StatusDate] IS NULL OR [StatusDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]       DATETIME      DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]         VARCHAR (20)  NOT NULL,
    [UpdatedDate]       DATETIME      CONSTRAINT [DF_MarketingListContactStatusLog_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]         VARCHAR (20)  NULL,
    PRIMARY KEY CLUSTERED ([MarketingListContactStatusLog_Key] ASC),
    CONSTRAINT [FK_MarketingListStatusLog_MarketingList] FOREIGN KEY ([MarketingListContact_Key]) REFERENCES [dESx].[MarketingListContact] ([MarketingListContact_Key]),
    CONSTRAINT [FK_MarketingListStatusLog_MarketingListContactStatus] FOREIGN KEY ([MarketingListStatus_Code]) REFERENCES [dESx].[MarketingListStatusType] ([MarketingListStatus_Code])
);

GO

CREATE INDEX [IX_MarketingListStatusLog_CreatedDate] ON [dESx].[MarketingListContactStatusLog] ([CreatedDate])

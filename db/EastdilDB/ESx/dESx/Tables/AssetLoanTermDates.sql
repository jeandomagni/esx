﻿CREATE TABLE [dESx].[AssetLoanTermDates] (
	[AssetLoanTermDates_Key] INT NOT NULL IDENTITY,
    [Asset_Key]     INT  NOT NULL,
    [LoanTermDateType_Code]   NVARCHAR(20)  NOT NULL,
    [LoanTermDate]           DATE NOT NULL,
    CONSTRAINT [FK_AssetLoanTermDates_AssetLoanTerms] FOREIGN KEY ([Asset_Key]) REFERENCES [dESx].[AssetLoanTerms] ([Asset_Key]),
    CONSTRAINT [FK_AssetLoanTermDates_LoanTermDateType] FOREIGN KEY ([LoanTermDateType_Code]) REFERENCES [dESx].[LoanTermDateType] ([LoanTermDateType_Code]), 
    CONSTRAINT [PK_AssetLoanTermDates] PRIMARY KEY ([AssetLoanTermDates_Key]), 
);


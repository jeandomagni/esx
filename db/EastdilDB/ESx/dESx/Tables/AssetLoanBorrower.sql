﻿CREATE TABLE [dESx].[AssetLoanBorrower] (
    [LoanBorrower_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Company_Key]      INT          NOT NULL,
    [Asset_Key]        INT          NULL,
    [CreatedDate]      DATETIME     CONSTRAINT [DF_AssetLoanBorrower_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]        VARCHAR (20) NOT NULL,
    [UpdatedDate]      DATETIME     CONSTRAINT [DF_AssetLoanBorrower_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]        VARCHAR (20) NULL,
    CONSTRAINT [PK_LoanBorrower] PRIMARY KEY CLUSTERED ([LoanBorrower_Key] ASC),
    CONSTRAINT [FK_AssetLoanBorrower_Asset] FOREIGN KEY ([Asset_Key]) REFERENCES [dESx].[Asset] ([Asset_Key]),
    CONSTRAINT [FK_AssetLoanBorrower_Company] FOREIGN KEY ([Company_Key]) REFERENCES [dESx].[Company] ([Company_Key])
);


﻿CREATE TABLE [dESx].[PhysicalAddress] (
    [PhysicalAddress_Key]      INT               IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PhysicalAddressType_Code] NVARCHAR (10)     NOT NULL,
    [Description]              NVARCHAR (100)    NULL,
    [Address1]                 NVARCHAR (100)    NOT NULL,
    [Address2]                 NVARCHAR (100)    NULL,
    [Address3]                 NVARCHAR (100)    NULL,
    [Address4]                 NVARCHAR (100)    NULL,
    [City]                     NVARCHAR (50)     NOT NULL,
    [StateProvidence_Code]     NCHAR (2)         NULL,
    [Postal_Code]              NVARCHAR (15)     NOT NULL,
    [County]                   NVARCHAR (50)     NULL,
    [Country_Key]              INT               NOT NULL,
    [CrossStreet]              NVARCHAR (150)    NULL,
    [Coordinates]              [sys].[geography] NULL,
	[Latitude]				   FLOAT			 Null,
	[Longitude]				   FLOAT			 Null,
    [AddressValidationDate]    DATE              NULL,
    [CreatedDate]              DATETIME          CONSTRAINT [DF_PhysicalAddress_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]                VARCHAR (20)      NOT NULL,
    [UpdatedDate]              DATETIME          CONSTRAINT [DF_PhysicalAddress_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]                VARCHAR (20)      NULL,
    CONSTRAINT [PK_PhysicalAddress] PRIMARY KEY CLUSTERED ([PhysicalAddress_Key] ASC),
    CONSTRAINT [FK_PhysicalAddress_Country] FOREIGN KEY ([Country_Key]) REFERENCES [dESx].[Country] ([Country_Key]),
    CONSTRAINT [FK_PhysicalAddress_PhysicalAddressType] FOREIGN KEY ([PhysicalAddressType_Code]) REFERENCES [dESx].[PhysicalAddressType] ([PhysicalAddressType_Code]),
    CONSTRAINT [FK_PhysicalAddress_StateProvidence] FOREIGN KEY ([StateProvidence_Code]) REFERENCES [dESx].[StateProvidence] ([StateProvidence_Code])
);




GO
CREATE NONCLUSTERED INDEX [IX_PhysicalAddress_Country]
    ON [dESx].[PhysicalAddress]([Country_Key] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IsActive=yes, Type=Utility,Notes=', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = N'yes', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Purely for performance of typeahead to select existing country.', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'INDEX', @level2name = N'IX_PhysicalAddress_Country';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=120 @ 1669.31:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'Country_Key';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=78 @ 2568.17:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'CrossStreet';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=3 @ 66772.34:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'County';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=12320 @ 16.26:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'Postal_Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=74 @ 2706.99:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'StateProvidence_Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=4164 @ 48.11:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=57 @ 3514.33:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'Address4';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=2187 @ 91.59:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'Address3';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=9172 @ 21.84:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'Address2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=37565 @ 5.33:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'Address1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non-Null=45 @ 4451.49:1', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'Description';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'PhysicalAddress', @level2type = N'COLUMN', @level2name = N'PhysicalAddress_Key';


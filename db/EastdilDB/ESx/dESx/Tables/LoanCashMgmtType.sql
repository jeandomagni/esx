﻿CREATE TABLE [dESx].[LoanCashMgmtType] (
    [LoanCashMgmtType_Code] NVARCHAR (10) NOT NULL,
    [Description]           NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_LoanCashMgmtType] PRIMARY KEY CLUSTERED ([LoanCashMgmtType_Code] ASC)
);


﻿CREATE TABLE [dESx].[Contact] (
    [Contact_Key]             INT            IDENTITY (1, 1) NOT NULL,
    [ParentContact_Key]       INT            NULL,
    [FullName]                NVARCHAR (100) NULL,
    [LastName]                NVARCHAR (50)  NOT NULL,
    [FirstName]               NVARCHAR (50)  NULL,
    [MiddleName]              NVARCHAR (50)  NULL,
    [LastnameFirstname]       NVARCHAR (100) NULL,
    [Nickname]                NVARCHAR (50)  NULL,
    [SpouseName]              NVARCHAR (100) NULL,
    [Title]                   NVARCHAR (100) NULL,
    [IsSolicitable]                     BIT     CONSTRAINT [DF_Contact_Solicitable] DEFAULT ((0)) NOT NULL,
    [IsRequestingTeaser]                BIT     CONSTRAINT [DF_Contact_IsRequestingTeaser] DEFAULT ((0)) NOT NULL,
    [IsRequestingWarRoomAccess]         BIT     CONSTRAINT [DF_Contact_IsRequestingWarRoomAccess] DEFAULT ((0)) NOT NULL,
    [IsRequestingAdditionalMaterials]   BIT     CONSTRAINT [DF_Contact_IsRequestingAdditionalMaterials] DEFAULT ((0)) NOT NULL,
    [IsRequestingPrintedOm]             BIT     CONSTRAINT [DF_Contact_IsRequestingPrintedOm] DEFAULT ((0)) NOT NULL,
    [IsRequestingCallForOffers]         BIT     CONSTRAINT [DF_tContact_RequestingCallForOffers] DEFAULT ((0)) NOT NULL,
    [IsDefunct]               BIT            CONSTRAINT [DF_Contact_IsActive] DEFAULT ((0)) NOT NULL,
    [DefunctReason]           NVARCHAR (500) NULL,
    [CreatedDate]             DATETIME       CONSTRAINT [DF_Contact_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]               VARCHAR (20)   NOT NULL,
    [UpdatedDate]             DATETIME       CONSTRAINT [DF_Contact_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]               VARCHAR (20)   NULL,
	[EastdilOffice_Key]       INT            NULL,
	[AdEntID]				  NVARCHAR (50)  NULL
    CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED ([Contact_Key] ASC),
    CONSTRAINT [CK_Contact_DefunctReason] CHECK (len([DefunctReason])>(3) OR [IsDefunct]=(0)),
    CONSTRAINT [CK_Contact_LastNameNotBlank] CHECK (len([LastName])>(0)),
    CONSTRAINT [FK_Contact_Contact] FOREIGN KEY ([ParentContact_Key]) REFERENCES [dESx].[Contact] ([Contact_Key]), 
    CONSTRAINT [FK_Contact_EastdilOffice] FOREIGN KEY ([EastdilOffice_Key]) REFERENCES [dESx].[EastdilOffice]([EastdilOffice_Key])
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cannot be blank if IsDefunct=true', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'CONSTRAINT', @level2name = N'CK_Contact_DefunctReason';


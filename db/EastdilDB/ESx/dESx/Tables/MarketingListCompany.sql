﻿CREATE TABLE [dESx].[MarketingListCompany]
(
    [MarketingListCompany_Key] INT IDENTITY (1, 1) NOT NULL, 
    [MarketingList_Key] INT				NOT NULL,
	[Company_Key]		INT             NOT NULL,
	[CreatedDate]		DATETIME        CONSTRAINT [DF_MarketingListCompany_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]			VARCHAR (20)    NOT NULL,
    [UpdatedDate]		DATETIME        CONSTRAINT [DF_MarketingListCompany_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]			VARCHAR (20)    NULL,
	CONSTRAINT [PK_MarketingListCompany] PRIMARY KEY CLUSTERED ([MarketingListCompany_Key] ASC),
    CONSTRAINT [FK_MarketingListCompany_MarketingList] FOREIGN KEY ([MarketingList_Key]) REFERENCES [dESx].[MarketingList] ([MarketingList_Key]),
    CONSTRAINT [FK_MarketingListCompany_Company] FOREIGN KEY ([Company_Key]) REFERENCES [dESx].[Company] ([Company_Key])
)

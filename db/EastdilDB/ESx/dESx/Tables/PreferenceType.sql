﻿CREATE TABLE [dESx].[PreferenceType] (
    [PreferenceType_Code] NVARCHAR (15)  NOT NULL,
    [Description]         NVARCHAR (100) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_PreferenceType] PRIMARY KEY CLUSTERED ([PreferenceType_Code] ASC)
);


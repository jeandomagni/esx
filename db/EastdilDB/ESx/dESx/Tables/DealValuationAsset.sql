﻿CREATE TABLE [dESx].[DealValuationAsset] (
    [DealValuationAsset_Key] INT          IDENTITY (1, 1) NOT NULL,
    [DealValuation_Key]      INT          NOT NULL,
    [DealAsset_Key]         INT          NOT NULL,
    [CreatedDate]           DATETIME     CONSTRAINT [DF_DealValuationAsset_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]             VARCHAR (20) NOT NULL,
    [UpdatedDate]           DATETIME     CONSTRAINT [DF_DealValuationAsset_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]             VARCHAR (20) NULL,
    CONSTRAINT [PK_DealValuationAsset] PRIMARY KEY CLUSTERED ([DealValuationAsset_Key] ASC),
    CONSTRAINT [FK_DealValuationAsset_DealAsset] FOREIGN KEY ([DealAsset_Key]) REFERENCES [dESx].[DealAsset] ([DealAsset_Key]),
    CONSTRAINT [FK_DealValuationAsset_DealValuation] FOREIGN KEY ([DealValuation_Key]) REFERENCES [dESx].[DealValuation] ([DealValuation_Key])
);


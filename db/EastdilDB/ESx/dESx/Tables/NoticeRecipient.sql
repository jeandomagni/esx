﻿CREATE TABLE [dESx].[NoticeRecipient] (
    [NoticeRecipient_Key] INT           IDENTITY (1, 1) NOT NULL,
    [Notice_Key]          INT           NOT NULL,
    [User_Key]            INT           NOT NULL,
    [IsAcknowledged]      BIT           NOT NULL,
    [SnoozeUntilTime]     DATETIME2 (7) NULL,
    CONSTRAINT [PK_NoticeRecipient] PRIMARY KEY CLUSTERED ([NoticeRecipient_Key] ASC),
    CONSTRAINT [FK_NoticeRecipient_Notice] FOREIGN KEY ([Notice_Key]) REFERENCES [dESx].[Notice] ([Notice_Key]),
    CONSTRAINT [FK_NoticeRecipient_User] FOREIGN KEY ([User_Key]) REFERENCES [dESx].[User] ([User_Key])
);






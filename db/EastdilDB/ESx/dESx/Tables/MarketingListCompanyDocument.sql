﻿CREATE TABLE [dESx].[MarketingListCompanyDocument]
(
    [MarketingListCompanyDocument_Key] INT IDENTITY (1, 1) NOT NULL,
	[MarketingListCompany_Key]			 INT NOT NULL,
	[Document_Key]			 INT NOT NULL,
	[CreatedDate]		DATETIME        CONSTRAINT [DF_MarketingListCompanyDocument_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]			VARCHAR (20)    NOT NULL,
    [UpdatedDate]		DATETIME        CONSTRAINT [DF_MarketingListCompanyDocument_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]			VARCHAR (20)    NULL,
	CONSTRAINT [PK_MarketingListCompanyDocument_Key] PRIMARY KEY CLUSTERED ([MarketingListCompanyDocument_Key] ASC),
	CONSTRAINT [FK_MarketingListCompanyDocument_MarketingListCompany] FOREIGN KEY ([MarketingListCompany_Key]) REFERENCES [dESx].[MarketingListCompany] ([MarketingListCompany_Key]),
	CONSTRAINT [FK_MarketingListCompanyDocument_Document] FOREIGN KEY ([Document_Key]) REFERENCES [dESx].[Document] (Document_Key)
)

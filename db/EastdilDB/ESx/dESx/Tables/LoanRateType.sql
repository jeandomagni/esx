﻿CREATE TABLE [dESx].[LoanRateType] (
    [LoanRateType_Code] NVARCHAR (10) NOT NULL,
    [Description]       NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_LoanRateType] PRIMARY KEY CLUSTERED ([LoanRateType_Code] ASC)
);


﻿CREATE TABLE [dESx].[AssetContactType] (
    [AssetContactType_Code] NVARCHAR(20) NOT NULL,
    [Description]     NVARCHAR (50) NULL,
    [Sort] INT NULL, 
    CONSTRAINT [PK_AssetContactType_Code] PRIMARY KEY CLUSTERED ([AssetContactType_Code] ASC)
);


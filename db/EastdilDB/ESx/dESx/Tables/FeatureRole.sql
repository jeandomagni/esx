﻿CREATE TABLE [dESx].[FeatureRole] (
    [FeatureRole_Key] INT           IDENTITY (1, 1) NOT NULL,
    [Feature_Code]    NVARCHAR (50) NOT NULL,
    [Role_Code]       NVARCHAR (50) NOT NULL,
    [HasAccess]       BIT           CONSTRAINT [DF_FeatureRole_HasAccess] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FeatureRole] PRIMARY KEY CLUSTERED ([FeatureRole_Key] ASC),
    CONSTRAINT [FK_FeatureRole_Feature] FOREIGN KEY ([Feature_Code]) REFERENCES [dESx].[Feature] ([Feature_Code]),
    CONSTRAINT [FK_FeatureRole_Role] FOREIGN KEY ([Role_Code]) REFERENCES [dESx].[Role] ([Role_Code]),
    CONSTRAINT [PK_Feature_Code_Role_Key] UNIQUE NONCLUSTERED ([Feature_Code] ASC, [Role_Code] ASC)
);



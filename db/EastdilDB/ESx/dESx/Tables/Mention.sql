﻿CREATE TABLE [dESx].[Mention] (
    [Mention_Key]           INT                    IDENTITY (1, 1) NOT NULL,
    [MentionID]             [iESx].[TYP_Reference] NOT NULL,
    [ReferencedTable__Code] AS                     (case when [Alias_Key] IS NOT NULL then 'A' when [Contact_Key] IS NOT NULL then 'C' when [Deal_Key] IS NOT NULL then 'D' when [PropertyAssetName_Key] IS NOT NULL then 'P' when [LoanAssetName_Key] IS NOT NULL then 'L' when [Market_Key] IS NOT NULL then 'M' when [Office_Key] IS NOT NULL then 'O'  end),
    [ReferencedTable__Key]  AS                     (coalesce([Alias_Key],[Deal_Key],[PropertyAssetName_Key],[LoanAssetName_Key],[Contact_Key],[Market_Key],[Office_Key])),
    [IsPrimary]             BIT                    CONSTRAINT [DF_Mention_IsPrimary] DEFAULT ((1)) NOT NULL,
    [Alias_Key]             INT                    NULL,
    [Deal_Key]              INT                    NULL,
    [PropertyAssetName_Key] INT                    NULL,
    [LoanAssetName_Key]     INT                    NULL,
    [Contact_Key]           INT                    NULL,
    [Market_Key]            INT                    NULL,
    [Office_Key]            INT                    NULL,
    [LastUseDate]           DATETIME               CONSTRAINT [DF_Mention_LastUseDate] DEFAULT (getutcdate()) NOT NULL,
    [TotalUseCount]         INT                    CONSTRAINT [DF_Mention_TotalUseCount] DEFAULT ((0)) NOT NULL,
    [CreatedDate]           DATETIME               CONSTRAINT [DF_Mention_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]             VARCHAR (20)           NOT NULL,
    [UpdatedDate]           DATETIME               CONSTRAINT [DF_Mention_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]             VARCHAR (20)           NULL,
    CONSTRAINT [PK_Mention] PRIMARY KEY CLUSTERED ([Mention_Key] ASC),
    CONSTRAINT [CK_Mention_ExactlyOneKey] CHECK (((((((isnull(([Alias_Key]-[Alias_Key])+(1),(0))+isnull(([Deal_Key]-[Deal_Key])+(1),(0)))+isnull(([PropertyAssetName_Key]-[PropertyAssetName_Key])+(1),(0)))+isnull(([LoanAssetName_Key]-[LoanAssetName_Key])+(1),(0)))+isnull(([Contact_Key]-[Contact_Key])+(1),(0)))+isnull(([Market_Key]-[Market_Key])+(1),(0)))+isnull(([Office_Key]-[Office_Key])+(1),(0)))=(1)),
    CONSTRAINT [CK_Mention_KeyEndsWithVBarTableCode] CHECK ([MentionID] like '%|'+case when [Alias_Key] IS NOT NULL then 'A' when [Contact_Key] IS NOT NULL then 'C' when [Deal_Key] IS NOT NULL then 'D' when [PropertyAssetName_Key] IS NOT NULL then 'P' when [LoanAssetName_Key] IS NOT NULL then 'L' when [Market_Key] IS NOT NULL then 'M' when [Office_Key] IS NOT NULL then 'O'  end),
    CONSTRAINT [FK_Mention_Alias] FOREIGN KEY ([Alias_Key]) REFERENCES [dESx].[CompanyAlias] ([CompanyAlias_Key]),
    CONSTRAINT [FK_Mention_Contact] FOREIGN KEY ([Contact_Key]) REFERENCES [dESx].[Contact] ([Contact_Key]),
    CONSTRAINT [FK_Mention_Deal] FOREIGN KEY ([Deal_Key]) REFERENCES [dESx].[Deal] ([Deal_Key]),
    CONSTRAINT [FK_Mention_LoanAssetName] FOREIGN KEY ([LoanAssetName_Key]) REFERENCES [dESx].[AssetName] ([AssetName_Key]),
    CONSTRAINT [FK_Mention_Market] FOREIGN KEY ([Market_Key]) REFERENCES [dESx].[Market] ([Market_Key]),
    CONSTRAINT [FK_Mention_Office] FOREIGN KEY ([Office_Key]) REFERENCES [dESx].[Office] ([Office_Key]),
    CONSTRAINT [FK_Mention_PropertyAssetName] FOREIGN KEY ([PropertyAssetName_Key]) REFERENCES [dESx].[AssetName] ([AssetName_Key])
);









GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Mention_MentionID]
    ON [dESx].[Mention]([MentionID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIXF_Mention_OnePrimary]
    ON [dESx].[Mention]([ReferencedTable__Code] ASC, [ReferencedTable__Key] ASC, [IsPrimary] ASC) WHERE ([IsPrimary]=(1));

	
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Only one MentionID per Table/Key can be designated IsPrimary', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Mention', @level2type = N'INDEX', @level2name = N'UIXF_Mention_OnePrimary';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Insure that exactly 1 reference key is stored.  All other keys are null.', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Mention', @level2type = N'CONSTRAINT', @level2name = N'CK_Mention_ExactlyOneKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A Mention_Key should end with |x where x is the table reference code (one of A,C,D,L,P', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Mention', @level2type = N'CONSTRAINT', @level2name = N'CK_Mention_KeyEndsWithVBarTableCode';


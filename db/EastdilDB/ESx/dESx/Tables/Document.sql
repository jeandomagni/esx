﻿CREATE TABLE [dESx].[Document] (
    [Document_Key] INT              IDENTITY (1, 1) NOT NULL,
    [External_Key] UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name]         NVARCHAR (256)   NOT NULL,
    [Size]         BIGINT           NULL,
    [CreateTime]   DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Document] PRIMARY KEY CLUSTERED ([Document_Key] ASC)
);



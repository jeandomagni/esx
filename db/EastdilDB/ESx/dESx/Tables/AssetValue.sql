﻿CREATE TABLE [dESx].[AssetValue] (
    [AssetValue_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Asset_Key]      INT          NOT NULL,
    [AssetValue]     MONEY        NOT NULL,
    [Spaces]         INT          NULL,
    [SpaceType]      NVARCHAR(10) NULL,
    [EndDate]        DATETIME     NULL,
    [IsCurrent]      AS           (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]    DATETIME     CONSTRAINT [DF_AssetValue_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]      VARCHAR (20) NOT NULL,
    [UpdatedDate]    DATETIME     CONSTRAINT [DF_AssetValue_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]      VARCHAR (20) NULL,
    CONSTRAINT [PK_ValuationData] PRIMARY KEY CLUSTERED ([AssetValue_Key] ASC),
    CONSTRAINT [FK_AssetValue_Asset] FOREIGN KEY ([Asset_Key]) REFERENCES [dESx].[Asset] ([Asset_Key])
);


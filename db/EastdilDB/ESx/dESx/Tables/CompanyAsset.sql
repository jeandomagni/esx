﻿CREATE TABLE [dESx].[CompanyAsset] (
    [CompanyAsset_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Company_Key]      INT          NOT NULL,
    [Asset_Key]        INT          NOT NULL,
    [OwnershipPercent] INT          NULL,
    [EndDate]          DATETIME     NULL,
    [IsCurrent]        AS           (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]      DATETIME     CONSTRAINT [DF_CompanyAsset_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]        VARCHAR (20) NOT NULL,
    [UpdatedDate]      DATETIME     CONSTRAINT [DF_CompanyAsset_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]        VARCHAR (20) NULL,
    CONSTRAINT [PK_Company_Loan] PRIMARY KEY CLUSTERED ([CompanyAsset_Key] ASC),
    CONSTRAINT [FK_CompanyAsset_Asset] FOREIGN KEY ([Asset_Key]) REFERENCES [dESx].[Asset] ([Asset_Key]),
    CONSTRAINT [FK_LoanLender_Company] FOREIGN KEY ([Company_Key]) REFERENCES [dESx].[Company] ([Company_Key])
);


﻿CREATE TABLE [dESx].[DealContact] (
    [DealContact_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Deal_Key]        INT          NOT NULL,
    [Contact_Key]     INT          NOT NULL,
    [DealContactType_Code] NVARCHAR(25)          NOT NULL,
    [CreatedDate]     DATETIME     CONSTRAINT [DF_DealContact_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]       VARCHAR (20) NOT NULL,
    [UpdatedDate]     DATETIME     CONSTRAINT [DF_DealContact_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]       VARCHAR (20) NULL,
    CONSTRAINT [PK_DealContact] PRIMARY KEY CLUSTERED ([DealContact_Key] ASC),
    CONSTRAINT [FK_DealContact_Contact] FOREIGN KEY ([Contact_Key]) REFERENCES [dESx].[Contact] ([Contact_Key]),
    CONSTRAINT [FK_DealContact_ContactCode] FOREIGN KEY ([DealContactType_Code]) REFERENCES [dESx].[DealContactType] ([DealContactType_Code]),
    CONSTRAINT [FK_DealContact_Deal] FOREIGN KEY ([Deal_Key]) REFERENCES [dESx].[Deal] ([Deal_Key])
);






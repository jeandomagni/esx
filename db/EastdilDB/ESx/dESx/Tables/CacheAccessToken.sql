﻿CREATE TABLE [dESx].[CacheAccessToken] (
    [CacheAccessToken_Key] INT                      IDENTITY (1, 1) NOT NULL,
    [AccessToken]          [iESx].[TYP_AccessToken] NOT NULL,
    [CreateTime]           DATETIME2 (0)            NOT NULL,
    [User__MentionID]      [iESx].[TYP_Reference]   NOT NULL,
    [User_Key]             INT                      NOT NULL,
    CONSTRAINT [PK_AccessTokenCache] PRIMARY KEY CLUSTERED ([CacheAccessToken_Key] ASC),
    CONSTRAINT [FK_CacheAccessToken_Mention] FOREIGN KEY ([User__MentionID]) REFERENCES [dESx].[Mention] ([MentionID]),
    CONSTRAINT [FK_CacheAccessToken_User] FOREIGN KEY ([User_Key]) REFERENCES [dESx].[User] ([User_Key])
);






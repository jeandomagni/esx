﻿CREATE TABLE [dESx].[DealTeam] (
    [DealTeam_Key]      INT           IDENTITY (1, 1) NOT NULL,
    [Deal_Key]          INT           NOT NULL,
    [Contact_Key]       INT           NOT NULL,
    [DealTeamRole_Code] NVARCHAR (10) NOT NULL,
    [CreatedDate]       DATETIME      CONSTRAINT [DF_DealTeam_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]         VARCHAR (20)  NOT NULL,
    [UpdatedDate]       DATETIME     CONSTRAINT [DF_DealTeam_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]         VARCHAR (20)  NULL,
    CONSTRAINT [PK_DealTeam] PRIMARY KEY CLUSTERED ([DealTeam_Key] ASC),
    CONSTRAINT [FK_DealTeam_Contact] FOREIGN KEY ([Contact_Key]) REFERENCES [dESx].[Contact] ([Contact_Key]),
    CONSTRAINT [FK_DealTeam_Deal] FOREIGN KEY ([Deal_Key]) REFERENCES [dESx].[Deal] ([Deal_Key]),
    CONSTRAINT [FK_DealTeam_DealTeamRole] FOREIGN KEY ([DealTeamRole_Code]) REFERENCES [dESx].[DealTeamRole] ([DealTeamRole_Code])
);


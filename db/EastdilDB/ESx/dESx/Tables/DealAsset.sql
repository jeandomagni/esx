﻿CREATE TABLE [dESx].[DealAsset] (
    [DealAsset_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Deal_Key]      INT          NOT NULL,
    [Asset_Key]     INT          NOT NULL,
    [CreatedDate]   DATETIME     CONSTRAINT [DF_DealAsset_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]     VARCHAR (20) NOT NULL,
    [UpdatedDate]   DATETIME     CONSTRAINT [DF_DealAsset_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]     VARCHAR (20) NULL,
    CONSTRAINT [PK_DealAsset] PRIMARY KEY CLUSTERED ([DealAsset_Key] ASC),
    CONSTRAINT [FK_DealAsset_Asset] FOREIGN KEY ([Asset_Key]) REFERENCES [dESx].[Asset] ([Asset_Key]),
    CONSTRAINT [FK_DealAsset_Deal] FOREIGN KEY ([Deal_Key]) REFERENCES [dESx].[Deal] ([Deal_Key])
);


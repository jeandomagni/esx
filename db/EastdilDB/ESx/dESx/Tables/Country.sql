﻿CREATE TABLE [dESx].[Country] (
    [Country_Key]       INT            IDENTITY (1, 1) NOT NULL,
    [CountryName]       NVARCHAR (50)  NOT NULL,
    [NativeCountryName] NVARCHAR (50)  NULL,
    [CallingCode]       VARCHAR (10)   NOT NULL,
    [ZipCodeFormat]     NVARCHAR (100) NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([Country_Key] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Country_CountryName]
    ON [dESx].[Country]([CountryName] ASC);


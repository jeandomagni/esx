CREATE TABLE [dESx].[Comment_Tag] (
    [Comment_Tag_Key] INT                    IDENTITY (1, 1) NOT NULL,
    [Comment_Key]     INT                    NOT NULL,
    [Tag_Key]         [iESx].[TYP_Reference] NOT NULL,
    CONSTRAINT [PK_Note_Tag] PRIMARY KEY CLUSTERED ([Comment_Tag_Key] ASC),
    CONSTRAINT [FK_Comment_Tag_Comment] FOREIGN KEY ([Comment_Key]) REFERENCES [dESx].[Comment] ([Comment_Key]),
    CONSTRAINT [FK_Comment_Tag_Tag] FOREIGN KEY ([Tag_Key]) REFERENCES [dESx].[Tag] ([Tag_Key])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ties tags to notes.', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Comment_Tag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies the tag referred to in a note', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Comment_Tag', @level2type = N'COLUMN', @level2name = N'Tag_Key';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies a Note containing a Tag', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Comment_Tag', @level2type = N'COLUMN', @level2name = N'Comment_Key';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Uniquely identifies a table row', @level0type = N'SCHEMA', @level0name = N'dESx', @level1type = N'TABLE', @level1name = N'Comment_Tag', @level2type = N'COLUMN', @level2name = N'Comment_Tag_Key';


﻿CREATE TABLE [dESx].[ContactMarket] (
    [ContactMarket_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Contact_Key]       INT          NOT NULL,
    [Market_Key]        INT          NOT NULL,
    [EndDate]           DATETIME     NULL,
    [IsCurrent]         AS           (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]       DATETIME     CONSTRAINT [DF_ContactMarket_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]         VARCHAR (20) NOT NULL,
    [UpdatedDate]       DATETIME     CONSTRAINT [DF_ContactMarket_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]         VARCHAR (20) NULL,
    CONSTRAINT [FK_ContactMarket_Contact] FOREIGN KEY ([Contact_Key]) REFERENCES [dESx].[Contact] ([Contact_Key]),
    CONSTRAINT [FK_ContactMarket_Market] FOREIGN KEY ([Market_Key]) REFERENCES [dESx].[Market] ([Market_Key]), 
    CONSTRAINT [PK_ContactMarket] PRIMARY KEY ([ContactMarket_Key])
);








﻿CREATE TABLE [dESx].[DealValuationCompany] (
    [DealValuationCompany_Key]   INT          IDENTITY (1, 1) NOT NULL,
    [DealValuation_Key] INT          NOT NULL,
    [Company_Key]      INT          NOT NULL,
    [OwnershipPercent] INT          NULL,
    [CreatedDate]      DATETIME     CONSTRAINT [DF_DealValuationCompany_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]        VARCHAR (20) NOT NULL,
    [UpdatedDate]      DATETIME     CONSTRAINT [DF_DealValuationCompany_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]        VARCHAR (20) NULL,
    CONSTRAINT [PK_DealValuationCompany] PRIMARY KEY CLUSTERED ([DealValuationCompany_Key] ASC),
    CONSTRAINT [FK_DealValuationCompany_DealValuation] FOREIGN KEY ([DealValuation_Key]) REFERENCES [dESx].[DealValuation] ([DealValuation_Key]),
    CONSTRAINT [FK_DealValuationCompany_Company] FOREIGN KEY ([Company_Key]) REFERENCES [dESx].[Company] ([Company_Key])
);


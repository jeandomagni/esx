﻿CREATE TABLE [dESx].[CompanyMarket] (
    [CompanyMarket_Key] INT          IDENTITY (1, 1) NOT NULL,
    [Company_Key]       INT          NOT NULL,
    [Market_Key]        INT          NOT NULL,
    [EndDate]           DATETIME     NULL,
    [IsCurrent]         AS           (isnull(CONVERT([bit],case when [EndDate] IS NULL OR [EndDate]>getutcdate() then (1) else (0) end,(0)),(0))),
    [CreatedDate]       DATETIME     CONSTRAINT [DF_CompanyMarket_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]         VARCHAR (20) NOT NULL,
    [UpdatedDate]       DATETIME     CONSTRAINT [DF_CompanyMarket_UpdatedDate] DEFAULT (getutcdate()) NOT NULL,
    [UpdatedBy]         VARCHAR (20) NULL,
    CONSTRAINT [PK_CompanyMarket] PRIMARY KEY CLUSTERED ([CompanyMarket_Key] ASC),
    CONSTRAINT [FK_CompanyMarket_Company] FOREIGN KEY ([Company_Key]) REFERENCES [dESx].[Company] ([Company_Key]),
    CONSTRAINT [FK_CompanyMarket_Market] FOREIGN KEY ([Market_Key]) REFERENCES [dESx].[Market] ([Market_Key])
);








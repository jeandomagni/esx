﻿CREATE TABLE [dESx].[Comment_Attachment]
(
	[Comment_Attachment_Key] INT IDENTITY (1, 1) NOT NULL,
	[Comment_Key]			 INT NOT NULL,
	[Document_Key]			 INT NOT NULL,
	[Create__User_Key]       INT            NOT NULL,	
    [CreateTime]             DATETIME       DEFAULT (getdate()) NOT NULL,
    [Update__User_Key]       INT            NOT NULL,
    [UpdateTime]             DATETIME       DEFAULT (getdate()) NOT NULL,
	CONSTRAINT [PK_Comment_Attachment_Key] PRIMARY KEY CLUSTERED ([Comment_Attachment_Key] ASC),
	CONSTRAINT [FK_Comment_Attachment_Comment] FOREIGN KEY ([Comment_Key]) REFERENCES [dESx].[Comment] ([Comment_Key]),
	CONSTRAINT [FK_Comment_Attachment_Document] FOREIGN KEY ([Document_Key]) REFERENCES [dESx].[Document] (Document_Key)
)

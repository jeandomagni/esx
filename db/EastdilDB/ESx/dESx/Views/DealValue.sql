﻿CREATE VIEW [dESx].[DealValue]	AS

SELECT
	[Deal_Key] = d.[Deal_Key]
	, [DealValuation_Key] = Valuations.[DealValuation_Key]
	, [Value] = CASE WHEN dsl.DealStatus_Code = 'Closed' AND d.FinalTransactionAmount > 0
					 THEN d.FinalTransactionAmount
					 ELSE Valuations.Value
					 END
FROM [dESx].[Deal] d
JOIN [dESx].[DealStatusLog] dsl ON d.Deal_Key = dsl.Deal_Key AND dsl.IsCurrent = 1
OUTER APPLY (
	SELECT TOP 1
		  dv.DealValuation_Key
		, dv.Value
	FROM [dESx].[DealValuation] dv
	WHERE dv.Deal_Key = d.Deal_Key
	ORDER BY 
		-- Sort bids before scenarios starting with the winning bid if it exists.
		CASE WHEN dv.DealValuationType_Code = 'Bid' AND dv.BidStatusCode = 'W' THEN 0 ELSE 1 END, 

		-- Then by the latest round of bidding.
		CASE WHEN dv.DealValuationType_Code = 'Bid' THEN dv.BidRound ELSE NULL END DESC,

		-- Then by the highest ranked bid.
		CASE WHEN dv.DealValuationType_Code = 'Bid' THEN dv.BidRank ELSE NULL END DESC,

		-- Then by the highest bid.
		CASE WHEN dv.DealValuationType_Code = 'Bid' THEN dv.Value ELSE NULL END DESC,

		-- Then if using scenarios, by Base Case first.
		CASE WHEN dv.DealValuationType_Code = 'BaseCase' THEN 0 ELSE 1 END,

		-- They by the last valuation updated.
        dv.UpdatedDate DESC,

		-- Then by the last valuation created.
		dv.CreatedDate DESC
) Valuations

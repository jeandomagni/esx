﻿CREATE PROCEDURE dbo.updBO_EmployeePaidTimeOff
(
@EmployeeID int,
@VacationHoursAllowed int,
@VacationHoursPriorYr int,
 @LastUpdate datetime
  )
As
IF (SELECT CONVERT(nvarchar, LastUpdate, 120) as charLastUpdate FROM Employees WHERE EmployeeID=@EmployeeID)=CONVERT(nvarchar, @LastUpdate, 120)
 UPDATE Employees SET 
VacationHoursAllowed=@VacationHoursAllowed,
VacationHoursPriorYr=@VacationHoursPriorYr,
LastUpdate=GetDate() 
 WHERE EmployeeID=@EmployeeID
Else
 return -1
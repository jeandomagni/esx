﻿CREATE PROCEDURE [dbo].[GetDealList]

(

@p_PageIndex int,
@p_PageSize int,
@p_InnerWhere varchar(200),
@p_OuterWhere varchar(200),
@p_InnerSort varchar(200),
@p_OuterSort varchar(200)
)

AS

BEGIN
SET NOCOUNT ON;

DECLARE @Sql varchar(5000);
DECLARE @SqlCount varchar(5000);

SELECT @Sql = 'SELECT
	dl.RowNum,
	dl.DealID,
	dl.DealCode,
	dl.PreCode,
	dl.DealName,
	dl.CompanyName,
	dl.TransactionType,
	dl.TransactionTypeID,
	dl.PropertyType,
	dl.PropertyTypeID,
	dl.LocationAddress,
	dl.LocationCity,
	dl.LocationState,
	dl.Active,
	dl.DealStatus,
	dl.DealStatusID,
	dl.DealOffice,
	dl.Confidential,
	dl.ESTeam,
	dl.LastUpdate
FROM
	(
	SELECT DISTINCT
	DealProfiles.[DealID],
	[DealCode],
	[PreCode],
	[DealName],
	Companies.CompanyName,
	TransactionTypes.TransactionType,
	DealProfiles.TransactionTypeID,
	PropertyTypes.PropertyType,
	DealProfiles.PropertyTypeID,
	[LocationAddress],
	[LocationCity],
	[LocationState],
	DealProfiles.Active,
	DealStatuses.DealStatus,
	DealProfiles.DealStatusID,
	[DealOffice],
	[Confidential],
	[ESTeam],
	DealProfiles.[LastUpdate],
	ROW_NUMBER() OVER (ORDER BY ' + @p_InnerSort + ') AS RowNum
	FROM
	[dbo].[DealProfiles] 
INNER JOIN 
	Companies ON DealProfiles.ClientCompanyID=Companies.CompanyID
INNER JOIN 
	TransactionTypes ON DealProfiles.TransactionTypeID=TransactionTypes.TransactionTypeID
INNER JOIN 
	PropertyTypes ON DealProfiles.PropertyTypeID=PropertyTypes.PropertyTypeID
INNER JOIN 
	DealStatuses ON DealProfiles.DealStatusID=DealStatuses.DealStatusID
WHERE (' + @p_InnerWhere + ')' 
	+ ') dl '
+ ' WHERE dl.RowNum BETWEEN
( ' + CAST(@p_PageIndex AS varchar(10)) + ' * ' + CAST(@p_PageSize AS varchar(10)) + ' + 1 ) AND (( '
+ CAST(@p_PageIndex AS varchar(10)) + ' + 1 ) * ' + CAST(@p_PageSize AS varchar(10)) + ' )  AND (' + @p_OuterWhere + ')' 
+ ' ORDER BY ' + @p_OuterSort;

EXECUTE(@Sql);


--Return total number of records for paging
/*
SELECT @SqlCount = 'SELECT
	Count(dl.DealID) TotalRowCount
FROM
	(
	SELECT
	DealProfiles.[DealID]
	FROM
	[dbo].[DealProfiles] 
INNER JOIN 
	Companies ON DealProfiles.ClientCompanyID=Companies.CompanyID
INNER JOIN 
	TransactionTypes ON DealProfiles.TransactionTypeID=TransactionTypes.TransactionTypeID
INNER JOIN 
	PropertyTypes ON DealProfiles.PropertyTypeID=PropertyTypes.PropertyTypeID
INNER JOIN 
	DealStatuses ON DealProfiles.DealStatusID=DealStatuses.DealStatusID
WHERE (' + @p_InnerWhere + ')' 
	+ ') dl '

EXECUTE(@SqlCount);
*/
END
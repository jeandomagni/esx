﻿CREATE PROCEDURE dbo.updBO_EmployeeHistory
(
 @EmployeeHistoryID int,
 @EmployeeID int,
 @EventDate datetime,
 @EventType varchar(50),
 @Comment varchar(1000)
 )
As

 UPDATE EmployeeHistory SET 
 EmployeeID = @EmployeeID,
 EventDate=@EventDate, 
 EventType= @EventType, 
 Comment=@Comment 
 WHERE EmployeeHistoryID=@EmployeeHistoryID
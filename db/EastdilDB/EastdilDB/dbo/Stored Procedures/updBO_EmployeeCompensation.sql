﻿CREATE PROCEDURE dbo.updBO_EmployeeCompensation
(
@EmployeeID int,
@W4Status nvarchar(50),
@SocialSecurity nvarchar(50),
@BaseSalary nvarchar(50),
@NonExempt bit,
@JobClass nvarchar(50),
@VacationDaysAllowed int,
@SickDaysAllowed int,
 @LastUpdate datetime
  )
As
IF (SELECT CONVERT(nvarchar, LastUpdate, 120) as charLastUpdate FROM Employees WHERE EmployeeID=@EmployeeID)=CONVERT(nvarchar, @LastUpdate, 120)
 UPDATE Employees SET 
W4Status=@W4Status,
SocialSecurity=@SocialSecurity,
BaseSalary=@BaseSalary,
NonExempt=@NonExempt,
JobClass=@JobClass, 
VacationDaysAllowed=@VacationDaysAllowed,
SickDaysAllowed=@SickDaysAllowed,
LastUpdate=GetDate() 
 WHERE EmployeeID=@EmployeeID
Else
 return -1
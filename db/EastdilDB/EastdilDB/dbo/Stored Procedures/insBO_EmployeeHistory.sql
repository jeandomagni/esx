﻿CREATE PROCEDURE dbo.insBO_EmployeeHistory
(  
  @EmployeeID int,
  @EventDate datetime,
  @EventType nvarchar(50),
  @Comment nvarchar(1000)  
 )
As
INSERT INTO EmployeeHistory (EmployeeID, EventDate, EventType, Comment) 
VALUES (@EmployeeID, @EventDate, @EventType, @Comment)
 
 /* set nocount on */
 return @@IDENTITY
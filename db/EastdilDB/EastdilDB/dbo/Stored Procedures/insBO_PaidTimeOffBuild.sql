﻿CREATE PROCEDURE dbo.insBO_PaidTimeOffBuild
(  
  @EmployeeID int
  
 )
As
INSERT INTO EmployeePaidTimeOff (EmployeeID, PTOHrsUsed, WorkDate, LastUpdate) 
VALUES (@EmployeeID, '0', GETDATE(), GETDATE())
 
 /* set nocount on */
 return @@IDENTITY
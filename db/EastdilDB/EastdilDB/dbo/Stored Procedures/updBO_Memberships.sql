﻿CREATE PROCEDURE dbo.updBO_Memberships
(
 @MembershipID int,
 @MembershipDate datetime,
 @Organization varchar(50),
 @Comment varchar(1000),
 @EmployeeID int
 )
As

 UPDATE EmployeeMemberships SET 
 EmployeeID = @EmployeeID,
 Organization= @Organization, 
 Comment=@Comment,
 MembershipDate=@MembershipDate
 WHERE MembershipID=@MembershipID
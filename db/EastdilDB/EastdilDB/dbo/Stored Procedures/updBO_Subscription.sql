﻿CREATE PROCEDURE dbo.updBO_Subscription
(  
  @SubscriptionID int,
  @Subscription nvarchar(1000),
  @Description nvarchar(1000),
  @Vendor nvarchar(1000),
  @RenewalDate datetime,
  @Cost nvarchar(100),
  @InstallInstructions nvarchar(1000),
  @ApprovedBy nvarchar(100)  
 )
 As

 UPDATE Subscriptions SET 
 Subscription = @Subscription,
 Description=@Description, 
 Vendor= @Vendor, 
 RenewalDate=@RenewalDate,
 Cost=@Cost,
 InstallInstructions=@InstallInstructions,
 ApprovedBy=@ApprovedBy
 WHERE SubscriptionID=@SubscriptionID
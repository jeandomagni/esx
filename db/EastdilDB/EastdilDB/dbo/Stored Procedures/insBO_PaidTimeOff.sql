﻿CREATE PROCEDURE dbo.insBO_PaidTimeOff
(  
  @EmployeeID int,
  @WorkDate datetime,
  @PTOHrsUsed decimal (19,3),
  @Comment varchar(1000)  
 )
As
INSERT INTO EmployeePaidTimeOff (EmployeeID, WorkDate, PTOHrsUsed, Comment, LastUpdate) 
VALUES (@EmployeeID, @WorkDate, @PTOHrsUsed, @Comment, GETDATE())
 
 /* set nocount on */
 return @@IDENTITY
﻿CREATE PROCEDURE dbo.insBO_PitchLibrary
(  
  @CompanyID int,
  @ProductLine varchar(100),
  @ProductType varchar(100),
  @PresentedTo varchar(100),
  @ProjectName varchar(100), 
  @AssetLocation varchar(100),
  @Office varchar(100),
  @MD varchar(100),
  @Portfolio bit,
  @FileName varchar(100),
  @LeadPresenter varchar(100),
  @AdditionalPresenter varchar(100),
  @MaterialsCoordinator varchar(100),
  @MaterialsContributor varchar(100),
  @PitchDate datetime
    
 )
As
INSERT INTO PitchLibrary (CompanyID, ProductLine, ProductType, PresentedTo, ProjectName, AssetLocation, Office, MD, Portfolio, FileName, LeadPresenter, AdditionalPresenter, MaterialsCoordinator, MaterialsContributor, PitchDate) 
VALUES (@CompanyID, @ProductLine, @ProductType, @PresentedTo, @ProjectName, @AssetLocation, @Office, @MD, 0, @FileName, @LeadPresenter, @AdditionalPresenter, @MaterialsCoordinator, @MaterialsContributor, @PitchDate)
 
 /* set nocount on */
 return @@IDENTITY
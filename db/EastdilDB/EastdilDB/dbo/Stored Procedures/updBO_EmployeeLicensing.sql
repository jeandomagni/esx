﻿CREATE PROCEDURE [dbo].[updBO_EmployeeLicensing]
 (
 @EmployeeLicensingID int,
 @EmployeeID int,
 @LicensingType varchar(50),
 @LicensingStatus varchar(50),
 @State varchar(50),
 @ProcessStartDate datetime,
 @ExpirationDate datetime,
 @LicenseNumber varchar(50),
 @Comments varchar(1000)
 )
As

 UPDATE EmployeeLicensing SET 
 EmployeeID = @EmployeeID,
 LicensingType= @LicensingType, 
 LicensingStatus= @LicensingStatus,
 State = @State,
 ProcessStartDate=@ProcessStartDate, 
 ExpirationDate=@ExpirationDate,
 LicenseNumber=@LicenseNumber,
 Comments=@Comments 
 WHERE EmployeeLicensingID=@EmployeeLicensingID
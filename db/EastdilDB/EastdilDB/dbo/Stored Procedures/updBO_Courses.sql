﻿CREATE PROCEDURE dbo.updBO_Courses
(
 @CourseID int,
 @CourseName  nvarchar(500),
 @CourseDescription  nvarchar(500),
 @CourseAvailable datetime,
 @CourseDueDate datetime,
 @CourseFileName nvarchar(500)
 )
As

 UPDATE EmployeeCourses SET 
 CourseName = @CourseName,
 CourseDescription=@CourseDescription, 
 CourseAvailable= @CourseAvailable, 
 CourseDueDate=@CourseDueDate,
 CourseFileName=@CourseFileName
 WHERE CourseID=@CourseID
﻿CREATE PROCEDURE [dbo].[updBO_EmployeePoll]
(
@EmployeeID int,
@PollQ1 nvarchar(200),
@PollQ2 nvarchar(200),
 @LastUpdate datetime
  )
As
IF (SELECT CONVERT(nvarchar, LastUpdate, 120) as charLastUpdate FROM Employees WHERE EmployeeID=@EmployeeID)=CONVERT(nvarchar, @LastUpdate, 120)
 UPDATE Employees SET 
PollQ1=@PollQ1,
PollQ2=@PollQ2,
LastUpdate=GetDate() 
 WHERE EmployeeID=@EmployeeID
Else
 return -1
﻿CREATE PROCEDURE dbo.insBO_Courses
(  
  @CourseName nvarchar(1000),
  @CourseDescription nvarchar(1000),
  @CourseAvailable datetime,
  @CourseDueDate datetime,
  @CourseFileName nvarchar(1000)  
 )
As
INSERT INTO EmployeeCourses (CourseName, CourseDescription, CourseAvailable, CourseDueDate, CourseFileName) 
VALUES (@CourseName, @CourseDescription, @CourseAvailable, @CourseDueDate, @CourseFileName)
 
 /* set nocount on */
 return @@IDENTITY
﻿CREATE PROCEDURE dbo.insBO_SubscriptionMembers
(  
  @SubscriptionID int,
  @EmployeeID int,
  @InstallDate datetime,
  @ApprovedBy nvarchar(1000),
  @ReviewDate datetime,
  @RemoveDate datetime  
 )
As
INSERT INTO SubscriptionMembers (SubscriptionID, EmployeeID, InstallDate, ApprovedBy, ReviewDate, RemoveDate) 
VALUES (@SubscriptionID, @EmployeeID, @InstallDate, @ApprovedBy, @ReviewDate, @RemoveDate)
 
 /* set nocount on */
 return @@IDENTITY
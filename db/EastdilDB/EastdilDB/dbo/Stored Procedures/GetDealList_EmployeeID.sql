﻿CREATE PROCEDURE [dbo].[GetDealList_EmployeeID]

(

@PageIndex int,
@NumberOfRecords int,
@InnerWhere varchar(200),
@OuterWhere varchar(200),
@InnerSort varchar(200),
@OuterSort varchar(200)
)

AS

BEGIN
SET NOCOUNT ON;

DECLARE @Sql varchar(5000);
DECLARE @SqlCount varchar(5000);

SELECT @Sql = 'SELECT
	dl.RowNum,
	dl.DealID,
	dl.DealCode,
	dl.PreCode,
	dl.DealName,
	dl.CompanyName,
	dl.TransactionType,
	dl.TransactionTypeID,
	dl.PropertyType,
	dl.PropertyTypeID,
	dl.LocationAddress,
	dl.LocationCity,
	dl.LocationState,
	dl.Active,
	dl.DealStatus,
	dl.DealStatusID,
	dl.DealOffice,
	dl.Confidential,
	dl.ESTeam,
	dl.EmployeeID,
	dl.LastUpdate
FROM
	(
	SELECT DISTINCT
	DealProfiles.[DealID],
	[DealCode],
	[PreCode],
	[DealName],
	Companies.CompanyName,
	TransactionTypes.TransactionType,
	DealProfiles.TransactionTypeID,
	PropertyTypes.PropertyType,
	DealProfiles.PropertyTypeID,
	[LocationAddress],
	[LocationCity],
	[LocationState],
	DealProfiles.Active,
	DealStatuses.DealStatus,
	DealProfiles.DealStatusID,
	[DealOffice],
	[Confidential],
	[ESTeam],
	DealProfiles.[LastUpdate],
	DealWPInternal.EmployeeID,
	ROW_NUMBER() OVER (ORDER BY ' + @InnerSort + ') AS RowNum
	FROM
	[dbo].[DealProfiles] 
INNER JOIN 
	Companies ON DealProfiles.ClientCompanyID=Companies.CompanyID
INNER JOIN 
	TransactionTypes ON DealProfiles.TransactionTypeID=TransactionTypes.TransactionTypeID
INNER JOIN 
	PropertyTypes ON DealProfiles.PropertyTypeID=PropertyTypes.PropertyTypeID
INNER JOIN 
	DealStatuses ON DealProfiles.DealStatusID=DealStatuses.DealStatusID
INNER JOIN 
	DealWPInternal ON DealProfiles.DealID=DealWPInternal.DealID
WHERE (' + @InnerWhere + ')' 
	+ ') dl '
+ ' WHERE dl.RowNum BETWEEN
( ' + CAST(@PageIndex AS varchar(10)) + ' * ' + CAST(@NumberOfRecords AS varchar(10)) + ' + 1 ) AND (( '
+ CAST(@PageIndex AS varchar(10)) + ' + 1 ) * ' + CAST(@NumberOfRecords AS varchar(10)) + ' )  AND (' + @OuterWhere + ')' 
+ ' ORDER BY ' + @OuterSort;

EXECUTE(@Sql);


--Return total number of records for paging

SELECT @SqlCount = 'SELECT
	Count(dl.DealID) TotalRowCount
FROM
	(
	SELECT
	DealProfiles.[DealID]
	FROM
	[dbo].[DealProfiles] 
INNER JOIN 
	Companies ON DealProfiles.ClientCompanyID=Companies.CompanyID
INNER JOIN 
	TransactionTypes ON DealProfiles.TransactionTypeID=TransactionTypes.TransactionTypeID
INNER JOIN 
	PropertyTypes ON DealProfiles.PropertyTypeID=PropertyTypes.PropertyTypeID
INNER JOIN 
	DealStatuses ON DealProfiles.DealStatusID=DealStatuses.DealStatusID
INNER JOIN 
	DealWPInternal ON DealProfiles.DealID=DealWPInternal.DealID
WHERE (' + @InnerWhere + ')' 
	+ ') dl '

EXECUTE(@SqlCount);

END
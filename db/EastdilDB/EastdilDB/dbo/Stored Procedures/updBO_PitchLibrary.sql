﻿CREATE PROCEDURE dbo.updBO_PitchLibrary
(
@PitchID int,
@CompanyID int,
@ProductLine nvarchar(100),
@ProductType nvarchar(100),
@PresentedTo nvarchar(100),
@ProjectName nvarchar(100),
@AssetLocation nvarchar(100),
@Office nvarchar(50),
@MD nvarchar(100),
@Portfolio bit,
@FileName nvarchar(100),
@LeadPresenter varchar(100),
@AdditionalPresenter varchar(100),
@MaterialsCoordinator varchar(100),
@MaterialsContributor varchar(100),
@PitchDate datetime
)
As

 UPDATE PitchLibrary SET 
CompanyID=@CompanyID,
ProductLine=@ProductLine,
ProductType=@ProductType,
PresentedTo=@PresentedTo, 
ProjectName=@ProjectName, 
AssetLocation=AssetLocation, 
Office=@Office, 
MD=@MD,
Portfolio=@Portfolio,
FileName=@FileName, 
LeadPresenter=@LeadPresenter,
AdditionalPresenter=@AdditionalPresenter,
MaterialsCoordinator=@MaterialsCoordinator,
MaterialsContributor=@MaterialsContributor,
PitchDate=@PitchDate
 WHERE PitchID=@PitchID
﻿CREATE PROCEDURE dbo.updBO_EmpTraining
(
 @EmployeeTrainingID int,
 @Complete bit,
 @CompleteDate datetime
 )
As

 UPDATE EmployeeTraining SET 
 Complete = @Complete,
 CompleteDate=@CompleteDate
 WHERE EmployeeTrainingID=@EmployeeTrainingID
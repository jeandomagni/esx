﻿CREATE PROCEDURE dbo.insBO_Memberships
(  
  @EmployeeID int,
  @MembershipDate datetime,
  @Organization nvarchar(50),
  @Comment nvarchar(1000)  
 )
As
INSERT INTO EmployeeMemberships (EmployeeID, MembershipDate, Organization, Comment) 
VALUES (@EmployeeID, @MembershipDate, @Organization, @Comment)
 
 /* set nocount on */
 return @@IDENTITY
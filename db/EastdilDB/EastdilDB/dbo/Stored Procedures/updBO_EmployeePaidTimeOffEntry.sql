﻿CREATE PROCEDURE dbo.updBO_EmployeePaidTimeOffEntry
(
 @PaidTimeOffID int,
 @EmployeeID int,
 @WorkDate datetime,
 @PTOHrsUsed decimal(19,3),
 @Comment varchar(1000)
 )
As

 UPDATE EmployeePaidTimeOff SET 
 EmployeeID = @EmployeeID,
 WorkDate=@WorkDate, 
 PTOHrsUsed= @PTOHrsUsed, 
 Comment=@Comment, 
 LastUpdate=GETDATE()
 WHERE PaidTimeOffID=@PaidTimeOffID
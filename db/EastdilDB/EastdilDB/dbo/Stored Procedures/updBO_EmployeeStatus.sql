﻿CREATE Procedure dbo.updBO_EmployeeStatus
(
@EmployeeID int,
@EmployeeIDNum int,
@Active bit,
@EmploymentDate datetime,
@SeparationDate datetime,
@Birthday datetime,
@VoluntarySeparation nvarchar(50),
@HRAdminNote nvarchar(1000),
 @LastUpdate datetime
  )
As
IF (SELECT CONVERT(nvarchar, LastUpdate, 120) as charLastUpdate FROM Employees WHERE EmployeeID=@EmployeeID)=CONVERT(nvarchar, @LastUpdate, 120)
 UPDATE Employees SET 
EmployeeIDNum=@EmployeeIDNum,
Active=@Active,
EmploymentDate=@EmploymentDate,
SeparationDate=@SeparationDate,
Birthday=@Birthday,
VoluntarySeparation=@VoluntarySeparation,
HRAdminNote=@HRAdminNote,
LastUpdate=GetDate() 
 WHERE EmployeeID=@EmployeeID
Else
 return -1
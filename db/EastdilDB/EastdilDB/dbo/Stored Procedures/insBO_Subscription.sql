﻿CREATE PROCEDURE dbo.insBO_Subscription
(  
  @Subscription nvarchar(1000),
  @Description nvarchar(1000),
  @Vendor nvarchar(1000),
  @RenewalDate datetime,
  @Cost nvarchar(100),
  @InstallInstructions nvarchar(1000),
  @ApprovedBy nvarchar(100)  
 )
As
INSERT INTO Subscriptions (Subscription, Description, Vendor, RenewalDate, Cost, InstallInstructions, ApprovedBy) 
VALUES (@Subscription, @Description, @Vendor, @RenewalDate, @Cost, @InstallInstructions, @ApprovedBy)
 
 /* set nocount on */
 return @@IDENTITY
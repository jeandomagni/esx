﻿CREATE PROCEDURE dbo.updBO_SubscriptionMember
(  
  @SubscriptionMemberID int,
  @InstallDate datetime,
  @ApprovedBy nvarchar(1000),
  @ReviewDate datetime,
  @RemoveDate datetime  
 )
 As

 UPDATE SubscriptionMembers SET 
 InstallDate=@InstallDate,
 ApprovedBy=@ApprovedBy,
 ReviewDate=@ReviewDate,
 RemoveDate=@RemoveDate
 WHERE SubscriptionMemberID=@SubscriptionMemberID
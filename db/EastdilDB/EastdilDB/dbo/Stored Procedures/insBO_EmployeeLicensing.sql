﻿CREATE PROCEDURE [dbo].[insBO_EmployeeLicensing]
(  
 @EmployeeID int,
 @LicensingType varchar(50),
 @LicensingStatus varchar(50),
 @State varchar(50),
 @ProcessStartDate datetime,
 @ExpirationDate datetime,
 @LicenseNumber varchar(50),
 @Comments varchar(1000) 
 )
As
INSERT INTO EmployeeLicensing (EmployeeID, LicensingType, LicensingStatus, State, ProcessStartDate, ExpirationDate, LicenseNumber, Comments) 
VALUES (@EmployeeID, @LicensingType, @LicensingStatus, @State, @ProcessStartDate, @ExpirationDate, @LicenseNumber, @Comments)
 
 /* set nocount on */
 return @@IDENTITY
﻿CREATE Procedure dbo.insBO_EmployeeBase
(  
@EmpInitials nvarchar(50),
@Title nvarchar(50),
@Office nvarchar(50),
@Department nvarchar(50),
@Active bit,
@Extension nvarchar(50),
@EmployeeName nvarchar(50),
@Salutation nvarchar(50),
@LastName nvarchar(50),
@MiddleName nvarchar(50),
@FirstName nvarchar(50),
@NickName nvarchar(50),
@LastnameFirstname nvarchar(50),
@EMailAddress nvarchar(50),
@AssistedBy nvarchar(50),
@IMAddress nvarchar(50),
@StreetAddress nvarchar(50),
@Address2 nvarchar(50),
@City nvarchar(50),
@State nvarchar(50),
@Zip nvarchar(50),
@LegacyCompany nvarchar(50),
@Phone nvarchar(50),
@WorkFax nvarchar(50),
@HomeFax nvarchar(50),
@WorkPhone nvarchar(50),
@MobilePhone nvarchar(50),
@OtherPhone nvarchar(50),
@CallingCardNo nvarchar(50),
@CallingCardIssued datetime,
@Spouse nvarchar(100),
@Notes nvarchar(50),
@EditBy nvarchar(50)
 )
As
INSERT INTO Employees (EmpInitials, Title, Office, Department, Active, EmpType, Extension, EmployeeName, Salutation, LastName, MiddleName, FirstName, NickName, LastnameFirstName, EMailAddress, AssistedBy, IMAddress, StreetAddress, Address2, City, State, Zip, Phone, WorkFax, HomeFax, WorkPhone, MobilePhone, OtherPhone, CallingCardNo, CallingCardIssued, Spouse, Notes, UserType, NonExempt, OnDeals, REIBAdministrator, LSPAdministrator, RegisteredRep, Licensed, Series24, DealFeeAdministrator, SeniorManagement, CompCalc, ITAdministrator, HRAdministrator, ListAdministrator, DealCoordinator, ResearchAdministrator, LegalDept, RetailTeam, ManagingDirectorAndUp, WachoviaCoverage, IronMtAdmin, TimecardSupervisor, BetaTester, DealExpenseAdministrator, PrintRequestAdministrator, EditBy, LastUpdate) 
VALUES (@EmpInitials, @Title, @Office, @Department, 1, 'Employee', @Extension, @EmployeeName, @Salutation, @LastName, @MiddleName, @FirstName, @NickName, @LastnameFirstname, @EMailAddress, @AssistedBy, @IMAddress, @StreetAddress, @Address2, @City, @State, @Zip, @Phone, @WorkFax, @HomeFax, @WorkPhone, @MobilePhone, @OtherPhone, @CallingCardNo, @CallingCardIssued, @Spouse, @Notes, 'User', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, @EditBy, GETDATE())
 
 /* set nocount on */
 return @@IDENTITY
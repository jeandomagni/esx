﻿CREATE Procedure dbo.insBO_EmergContacts
(  
@EmployeeID int

 )
As
INSERT INTO EmployeeEmergContacts (EmployeeID, LastUpdate)
VALUES (@EmployeeID, GETDATE())
 
 /* set nocount on */
 return @@IDENTITY
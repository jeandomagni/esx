﻿CREATE PROCEDURE [dbo].[GenerateCoverageOfficerActivityRptData]

AS

BEGIN
SET NOCOUNT ON;

DECLARE @Sql varchar(5000);

SELECT @Sql = 'DELETE FROM CoverageOfficerActivityRpt

INSERT INTO CoverageOfficerActivityRpt ( DealContactID, ContactID, DealID, ESContactEmployeeID, ActivityType )
SELECT dc.DealContactID, dc.ContactID, dc.DealID, c.ESContactEmployeeID, "Added to Marketing List" from DealContacts dc
INNER JOIN Contacts c ON dc.ContactID=c.ContactID
where c.ESContactEmployeeid <> 1338791542 AND dc.DateAdded=DATEADD(dd, -1, DATEDIFF(dd, 0, GETDATE()))

INSERT INTO CoverageOfficerActivityRpt ( DealContactID, ContactID, DealID, ESContactEmployeeID, ActivityType )
SELECT dc.DealContactID, dc.ContactID, dc.DealID, c.ESContactEmployeeID, "Signed CA" from DealContacts dc
INNER JOIN Contacts c ON dc.ContactID=c.ContactID
where c.ESContactEmployeeid <> 1338791542 AND dc.CALogged=DATEADD(dd, -1, DATEDIFF(dd, 0, GETDATE()))

INSERT INTO CoverageOfficerActivityRpt ( DealContactID, ContactID, DealID, ESContactEmployeeID, ActivityType )
SELECT dc.DealContactID, dc.ContactID, dc.DealID, c.ESContactEmployeeID, "Sent OM" from DealContacts dc
INNER JOIN Contacts c ON dc.ContactID=c.ContactID
where c.ESContactEmployeeid <> 1338791542 AND dc.OMSent=DATEADD(dd, -1, DATEDIFF(dd, 0, GETDATE()))

INSERT INTO CoverageOfficerActivityRpt ( DealContactID, ContactID, DealID, ESContactEmployeeID, ActivityType )
SELECT dc.DealContactID, dc.ContactID, dc.DealID, c.ESContactEmployeeID, "Toured Property" from DealContacts dc
INNER JOIN Contacts c ON dc.ContactID=c.ContactID
where c.ESContactEmployeeid <> 1338791542 AND dc.Tour=DATEADD(dd, -1, DATEDIFF(dd, 0, GETDATE()))

INSERT INTO CoverageOfficerActivityRpt ( DealContactID, ContactID, DealID, ESContactEmployeeID, ActivityType )
SELECT dc.DealContactID, dc.ContactID, dc.DealID, c.ESContactEmployeeID, "Bid Submitted" from DealContacts dc
INNER JOIN Contacts c ON dc.ContactID=c.ContactID
where c.ESContactEmployeeid <> 1338791542 AND dc.BidOffer=DATEADD(dd, -1, DATEDIFF(dd, 0, GETDATE()))
'



EXECUTE(@Sql);


END
﻿CREATE VIEW dbo.ContactListContactExport
AS
SELECT     TOP (100) PERCENT dbo.ContactListContacts.ContactListContactID, dbo.ContactLists.ContactListID, dbo.ContactListContacts.ContactID, dbo.Contacts.CompanyID, 
                      dbo.Contacts.FullName, dbo.Contacts.FirstnameLastname, dbo.Companies.CompanyName, dbo.ContactLists.ListName, dbo.Employees.EmployeeName, 
                      dbo.ContactLists.Comments, dbo.ContactLists.ListManagerEmployeeID, dbo.ContactLists.ListTypeID, dbo.Contacts.Active, dbo.RSVP.Rsvp, 
                      dbo.ContactListContacts.NumAttending, dbo.ContactListContacts.Field1, dbo.ContactListContacts.Field2, dbo.ContactListContacts.Field3, 
                      dbo.ContactListContacts.Field4, dbo.ContactListContacts.Field5, dbo.ContactListContacts.Field6, dbo.Contacts.DirectPhone, dbo.Contacts.EmailAddress, 
                      dbo.ContactListContacts.InsertFlag, dbo.ContactListContacts.UpdateFlag, dbo.ContactListContacts.DeleteFlag, dbo.ContactListContacts.Comment, 
                      dbo.Addresses.Address1, dbo.Addresses.Address2, dbo.Addresses.Address3, dbo.Addresses.CrossStreet, dbo.Addresses.City, dbo.Addresses.State, 
                      dbo.Addresses.PostalCode, dbo.Addresses.Country, dbo.Contacts.LastName, dbo.Contacts.FirstName, dbo.Contacts.Salutation, dbo.Contacts.Prefix, 
                      dbo.Contacts.DoNotSolicit, dbo.Contacts.DoNotMail, dbo.Contacts.Title
FROM         dbo.RSVP INNER JOIN
                      dbo.ContactLists INNER JOIN
                      dbo.ContactListContacts ON dbo.ContactLists.ContactListID = dbo.ContactListContacts.ContactListID ON 
                      dbo.RSVP.RsvpID = dbo.ContactListContacts.RsvpID LEFT OUTER JOIN
                      dbo.Companies INNER JOIN
                      dbo.Contacts ON dbo.Companies.CompanyID = dbo.Contacts.CompanyID ON dbo.ContactListContacts.ContactID = dbo.Contacts.ContactID LEFT OUTER JOIN
                      dbo.Addresses ON dbo.Contacts.AddressID = dbo.Addresses.AddressID LEFT OUTER JOIN
                      dbo.Employees ON dbo.ContactListContacts.RespEmployeeID = dbo.Employees.EmployeeID
ORDER BY dbo.Companies.CompanyName
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ContactListContactExport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'   DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 42
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ContactListContactExport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "RSVP"
            Begin Extent = 
               Top = 6
               Left = 1050
               Bottom = 110
               Right = 1210
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ContactLists"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ContactListContacts"
            Begin Extent = 
               Top = 6
               Left = 280
               Bottom = 210
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 786
               Bottom = 125
               Right = 1012
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Contacts"
            Begin Extent = 
               Top = 6
               Left = 510
               Bottom = 327
               Right = 748
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Addresses"
            Begin Extent = 
               Top = 174
               Left = 1029
               Bottom = 293
               Right = 1216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employees"
            Begin Extent = 
               Top = 291
               Left = 241
               Bottom = 350
               Right = 454
            End
         ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ContactListContactExport';


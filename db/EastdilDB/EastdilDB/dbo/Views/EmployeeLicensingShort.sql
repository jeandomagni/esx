﻿CREATE VIEW dbo.EmployeeLicensingShort
AS
SELECT     EmployeeLicensingID, EmployeeID, LicensingType, LicenseNumber, State, ProcessStartDate, ExpirationDate, Comments, LastUpdate, DateAdded, AddedBy, 
                      UpdatedBy, LicensingStatus
FROM         dbo.EmployeeLicensing
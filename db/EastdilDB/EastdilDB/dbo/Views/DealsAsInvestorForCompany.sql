﻿CREATE VIEW dbo.DealsAsInvestorForCompany
AS
SELECT DISTINCT 
                      TOP (100) PERCENT MAX(dbo.Deals.DealID) AS DealID, MAX(dbo.Companies.CompanyName) AS CompanyName, MAX(dbo.Companies.DBAName) AS DBAName, 
                      MAX(dbo.TransactionTypes.TransactionType) AS DealType, MAX(dbo.Deals.DealCode) AS DealCode, MAX(dbo.Deals.DateAdded) AS DateAdded, 
                      MAX(dbo.Deals.DateClosed) AS DateClosed, COUNT(dbo.DealContacts.DealContactID) AS Contacts, MAX(dbo.DealContacts.CASent) AS CASent, 
                      MAX(dbo.DealContacts.CALogged) AS CALogged, MAX(dbo.DealContacts.OMSent) AS OMSent, MAX(dbo.DealContacts.WarroomAccess) AS WarroomAccess, 
                      MAX(dbo.DealContacts.Tour) AS Tour, MAX(CASE WHEN BidSubmitted = 1 THEN 1 ELSE 0 END) AS BidSubmitted, MAX(dbo.DealContacts.BidRank) AS BidRank, 
                      MAX(dbo.DealContacts.BidOffer) AS BidOffer, MAX(dbo.DealContacts.BidOfferAmt) AS BidOfferAmt, MAX(dbo.DealContacts.Won) AS Won, dbo.Companies.CompanyID, 
                      dbo.Deals.DealID AS Expr1, dbo.DealEquitySales.ProjectSize AS EquitySaleProjectSize, dbo.DealFinancings.ProjectSize AS FinancingProjectSize
FROM         dbo.Companies INNER JOIN
                      dbo.DealContacts ON dbo.Companies.CompanyID = dbo.DealContacts.CompanyID INNER JOIN
                      dbo.Deals ON dbo.DealContacts.DealID = dbo.Deals.DealID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID INNER JOIN
                      dbo.DealEquitySales ON dbo.Deals.DealID = dbo.DealEquitySales.DealID INNER JOIN
                      dbo.DealFinancings ON dbo.Deals.DealID = dbo.DealFinancings.DealID
GROUP BY dbo.Deals.DealCode, dbo.Companies.CompanyID, dbo.Deals.DealID, dbo.DealEquitySales.ProjectSize, dbo.DealFinancings.ProjectSize
ORDER BY DateClosed, DateAdded
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 2205
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealsAsInvestorForCompany';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 280
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealContacts"
            Begin Extent = 
               Top = 6
               Left = 318
               Bottom = 125
               Right = 526
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 564
               Bottom = 364
               Right = 833
            End
            DisplayFlags = 280
            TopColumn = 39
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 16
               Left = 1187
               Bottom = 135
               Right = 1390
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealEquitySales"
            Begin Extent = 
               Top = 150
               Left = 919
               Bottom = 334
               Right = 1153
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealFinancings"
            Begin Extent = 
               Top = 203
               Left = 1354
               Bottom = 311
               Right = 1568
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealsAsInvestorForCompany';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealsAsInvestorForCompany';


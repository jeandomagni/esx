﻿CREATE VIEW dbo.CommentsForEmpDealContacts
AS
SELECT     dbo.Comments.CommentID, dbo.Comments.CompanyID, dbo.Comments.ContactID, dbo.Comments.DealID, dbo.Comments.DealContactID, 
                      dbo.Comments.CommentDate, dbo.Employees.EmployeeName, ConBy.FirstnameLastname, dbo.CommentTypes.CommentType, 
                      dbo.CommentVisibility.CommentVisibility, dbo.Deals.DealCode, dbo.Deals.DealName, ConFor.FirstnameLastname AS Expr1, dbo.Comments.CommentText, 
                      dbo.Comments.PropertyID, dbo.Comments.LoanID, dbo.Comments.DocumentID, dbo.Comments.EmployeeID, dbo.Comments.CommentByContactID, 
                      dbo.DealContacts.EmployeeID AS DealContactsEmployeeID, dbo.Comments.CommentTypeID, dbo.Deals.ESTeam
FROM         dbo.Comments INNER JOIN
                      dbo.CommentTypes ON dbo.Comments.CommentTypeID = dbo.CommentTypes.CommentTypeID INNER JOIN
                      dbo.CommentVisibility ON dbo.Comments.CommentVisibilityID = dbo.CommentVisibility.CommentVisibilityID INNER JOIN
                      dbo.DealContacts ON dbo.Comments.DealContactID = dbo.DealContacts.DealContactID LEFT OUTER JOIN
                      dbo.Deals ON dbo.Comments.DealID = dbo.Deals.DealID LEFT OUTER JOIN
                      dbo.Employees ON dbo.Comments.EmployeeID = dbo.Employees.EmployeeID LEFT OUTER JOIN
                      dbo.Contacts AS ConBy ON dbo.Comments.CommentByContactID = ConBy.ContactID LEFT OUTER JOIN
                      dbo.Contacts AS ConFor ON dbo.Comments.ContactID = ConFor.ContactID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CommentsForEmpDealContacts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N' DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ConFor"
            Begin Extent = 
               Top = 138
               Left = 678
               Bottom = 246
               Right = 907
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CommentsForEmpDealContacts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -192
      End
      Begin Tables = 
         Begin Table = "Comments"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 269
            End
            DisplayFlags = 280
            TopColumn = 16
         End
         Begin Table = "CommentTypes"
            Begin Extent = 
               Top = 6
               Left = 307
               Bottom = 114
               Right = 485
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CommentVisibility"
            Begin Extent = 
               Top = 6
               Left = 523
               Bottom = 114
               Right = 701
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealContacts"
            Begin Extent = 
               Top = 6
               Left = 739
               Bottom = 114
               Right = 922
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 960
               Bottom = 324
               Right = 1157
            End
            DisplayFlags = 280
            TopColumn = 34
         End
         Begin Table = "Employees"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ConBy"
            Begin Extent = 
               Top = 168
               Left = 447
               Bottom = 276
               Right = 676
            End
           ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CommentsForEmpDealContacts';


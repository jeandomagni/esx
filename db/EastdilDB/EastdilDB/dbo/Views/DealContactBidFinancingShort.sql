﻿CREATE VIEW dbo.DealContactBidFinancingShort
AS
SELECT DISTINCT 
                      dc.DealContactID, d.DealID, con.FirstnameLastname, c.CompanyName, dc.Tour, ms.MarketingStatus, dcbf.BidDate, dcbf.BidRank, dcbf.BidComments, 
                      dcbf.OfferNumber, dcbf.Lender, dcbf.LenderType, dcbf.InitialLoan, dcbf.ReferenceIndex, dcbf.Spread, dcbf.RateFloor, dcbf.AllInRate, dcbf.IncrementalCostOfDebt, 
                      dcbf.LTC_LTV, dcbf.ClosingTests, dcbf.ExtensionTests, dcbf.LoanTerm, dcbf.EarnoutHoldback, dcbf.TI_LC_Holdback, dcbf.DueDiligencePeriodDays, 
                      dcbf.ClosingPeriodDays, dcbf.LoanType, dcbf.OriginationFee, dcbf.InterestOnlyPeriodMonths, dcbf.AmortizationMonths, dcbf.Prepayment, dcbf.LockOutYears, 
                      dcbf.OpenPeriodYears, dcbf.Recourse, dcbf.UpfrontReserves, dcbf.OngoingReserves, dcbf.CashManagementTest, dcbf.TotalValueOrPurchasePrice, 
                      dcbf.TotalLoanCALC, dcbf.LoanPerSFUnitCALC, dcbf.InPlaceDebtYieldCALC, dcbf.Year1DebtYieldCALC, dcbf.StabilizedDebtYieldCALC, d.DealCode, d.DealName, 
                      df.ProjectSize, dbo.ProjectSizeTypes.ProjectSizeType, d.LocationCity, d.LocationState, dbo.PropertyTypes.PropertyType, df.PctLeased, 
                      dbo.FinancingTypes.FinancingType, df.UrbanOrSuburban, df.StablizedOrNonstabilized, df.NOI, df.NOIFirstYear, df.NOIStabilized, con.Active, 
                      con.LastnameFirstname
FROM         dbo.DealContactBidFinancing AS dcbf RIGHT OUTER JOIN
                      dbo.DealContacts AS dc ON dcbf.DealContactID = dc.DealContactID INNER JOIN
                      dbo.MarketingStatuses AS ms ON ms.MarketingStatusID = dc.MarketingStatusID INNER JOIN
                      dbo.Deals AS d ON d.DealID = dc.DealID INNER JOIN
                      dbo.DealFinancings AS df ON df.DealID = d.DealID INNER JOIN
                      dbo.Contacts AS con ON con.ContactID = dc.ContactID INNER JOIN
                      dbo.Companies AS c ON c.CompanyID = dc.CompanyID INNER JOIN
                      dbo.FinancingTypes ON df.FinancingTypeID = dbo.FinancingTypes.FinancingTypeID INNER JOIN
                      dbo.ProjectSizeTypes ON df.ProjectSizeTypeID = dbo.ProjectSizeTypes.ProjectSizeTypeID INNER JOIN
                      dbo.PropertyTypes ON d.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID
WHERE     (dc.CALogged IS NOT NULL) AND (con.Active = 1)
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[56] 4[18] 2[9] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -576
      End
      Begin Tables = 
         Begin Table = "dcbf"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 475
               Right = 272
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "dc"
            Begin Extent = 
               Top = 6
               Left = 326
               Bottom = 136
               Right = 561
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ms"
            Begin Extent = 
               Top = 6
               Left = 599
               Bottom = 136
               Right = 793
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 30
               Left = 854
               Bottom = 346
               Right = 1121
            End
            DisplayFlags = 280
            TopColumn = 15
         End
         Begin Table = "df"
            Begin Extent = 
               Top = 165
               Left = 1173
               Bottom = 395
               Right = 1391
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "con"
            Begin Extent = 
               Top = 6
               Left = 1415
               Bottom = 136
               Right = 1670
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 217
               Left = 598
               Bottom = 347
               Right = 841
            End
            DisplayFlags = 280
            TopColumn = 0
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactBidFinancingShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactBidFinancingShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'         End
         Begin Table = "FinancingTypes"
            Begin Extent = 
               Top = 6
               Left = 1136
               Bottom = 136
               Right = 1314
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProjectSizeTypes"
            Begin Extent = 
               Top = 138
               Left = 1429
               Bottom = 268
               Right = 1612
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 138
               Left = 1650
               Bottom = 268
               Right = 1844
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactBidFinancingShort';


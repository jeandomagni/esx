﻿
CREATE VIEW [dbo].[MktgListBasic]
AS
SELECT     dbo.DealContacts.DealContactID, dbo.Companies.CompanyName, dbo.Contacts.FullName, dbo.Contacts.LastName, dbo.Contacts.FirstName, 
                      dbo.Contacts.FirstnameLastname, dbo.Categories.Category, dbo.MarketingStatuses.MarketingStatus, dbo.Classes.Class, dbo.DealContacts.CoverageTeam, 
                      dbo.DealContacts.CAUnApproved, dbo.DealContacts.IMSent, dbo.DealContacts.CASent, dbo.DealContacts.CALogged, dbo.DealContacts.OMSent, 
                      dbo.DealContacts.OMDigital, dbo.DealContacts.DoNotSendOM, dbo.DealContacts.WarroomAccess, dbo.DealContacts.Tour, dbo.DealContacts.DIP, 
                      dbo.DealContacts.AdditionalInfo, dbo.Contacts.DealConvPct, dbo.DealContacts.LastUpdate, dbo.DealContacts.DealID, dbo.DealContacts.ContactID, 
                      dbo.Companies.CompanyID, dbo.DealContacts.DDSent, dbo.Contacts.Active, dbo.Contacts.DoNotSolicit, dbo.DealContacts.DoNotSendWarroom, 
                      dbo.Contacts.SpecialConditions, dbo.Contacts.Notes, dbo.Contacts.ETeasersBlocked, dbo.Contacts.EmailAddress, dbo.DealContacts.BidOffer, 
                      dbo.DealContacts.InsertFlag, dbo.DealContacts.UpdateFlag, dbo.DealContacts.DeleteFlag, dbo.Employees.EmpInitials, dbo.Employees.EmployeeName, 
                      CC.LastnameFirstname, dbo.Contacts.DirectPhone, dbo.DealContacts.EmployeeID, dbo.DealContacts.DoNotCopy, dbo.DealContacts.NumComments, 
                      dbo.Deals.ESTeam, dbo.Contacts.CategoryID AS TypicalRoleCategoryID, dbo.DealContacts.CategoryID, dbo.DealContacts.BidOfferAmt, dbo.DealContacts.Won, 
                      dbo.DealContacts.WinningBidder, dbo.DealContacts.StatusNote, dbo.DealContacts.Tier, dbo.DealContacts.BidRank, dbo.DealContacts.BidSubmitted, 
                      dbo.DealContacts.BidDepositNote, dbo.DealContacts.BidDDPeriodNote, dbo.DealContacts.BidDebtFinancing, dbo.DealContacts.BidRate, dbo.DealContacts.BidSpread, 
                      dbo.DealContacts.BidLoanTermYears, dbo.DealContacts.BidPrepaymentPenaltyNote, dbo.DealContacts.BidCapRate, dbo.DealContacts.BidPricePSF, 
                      dbo.DealContacts.BidEquityCapital, dbo.DealContacts.BidGeneralNote, dbo.DealContacts.BidOriginationFee, dbo.DealContacts.BidLoanType, 
                      dbo.DealContacts.BidReferenceIndex, dbo.DealContacts.BidRound, dbo.Contacts.PrefersDigitalOM, dbo.DealContacts.CAPortal, dbo.DealContacts.CAMethod,
                      dbo.DealContacts.ESI_TARGET_2ND,dbo.DealContacts.ESI_TARGET_CFO,dbo.DealContacts.ESI_TARGET_ETEASER,dbo.DealContacts.ESI_TARGET_FINAL,
                      dbo.DealContacts.ESI_TARGET_IM_OM,dbo.DealContacts.ESI_TARGET_OTHER,dbo.DealContacts.ESI_TARGET_WARROOM
FROM         dbo.Deals INNER JOIN
                      dbo.DealContacts ON dbo.Deals.DealID = dbo.DealContacts.DealID INNER JOIN
                      dbo.MarketingStatuses ON dbo.DealContacts.MarketingStatusID = dbo.MarketingStatuses.MarketingStatusID INNER JOIN
                      dbo.Companies ON dbo.DealContacts.CompanyID = dbo.Companies.CompanyID INNER JOIN
                      dbo.Contacts ON dbo.DealContacts.ContactID = dbo.Contacts.ContactID INNER JOIN
                      dbo.Categories ON dbo.DealContacts.CategoryID = dbo.Categories.CategoryID INNER JOIN
                      dbo.Classes ON dbo.DealContacts.ClassID = dbo.Classes.ClassID LEFT OUTER JOIN
                      dbo.Contacts AS CC ON dbo.DealContacts.CoverageContactID = CC.ContactID LEFT OUTER JOIN
                      dbo.Employees ON dbo.DealContacts.EmployeeID = dbo.Employees.EmployeeID AND dbo.DealContacts.EmployeeID = dbo.Employees.EmployeeID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListBasic';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'       DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CC"
            Begin Extent = 
               Top = 6
               Left = 494
               Bottom = 125
               Right = 732
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employees"
            Begin Extent = 
               Top = 6
               Left = 1232
               Bottom = 114
               Right = 1436
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1965
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListBasic';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[50] 4[12] 2[21] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -586
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 208
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 77
         End
         Begin Table = "DealContacts"
            Begin Extent = 
               Top = 6
               Left = 273
               Bottom = 637
               Right = 563
            End
            DisplayFlags = 280
            TopColumn = 43
         End
         Begin Table = "MarketingStatuses"
            Begin Extent = 
               Top = 187
               Left = 519
               Bottom = 295
               Right = 697
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Companies"
            Begin Extent = 
               Top = 228
               Left = 742
               Bottom = 336
               Right = 959
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Contacts"
            Begin Extent = 
               Top = 91
               Left = 982
               Bottom = 395
               Right = 1211
            End
            DisplayFlags = 280
            TopColumn = 62
         End
         Begin Table = "Categories"
            Begin Extent = 
               Top = 225
               Left = 35
               Bottom = 318
               Right = 186
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Classes"
            Begin Extent = 
               Top = 313
               Left = 186
               Bottom = 421
               Right = 364
            End
     ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListBasic';


﻿CREATE VIEW dbo.ContactListContactShort
AS
SELECT        TOP (100) PERCENT dbo.ContactListContacts.ContactListContactID, dbo.ContactLists.ContactListID, dbo.ContactListContacts.ContactID, dbo.Contacts.CompanyID, dbo.Contacts.FullName, 
                         dbo.Contacts.FirstnameLastname, dbo.Companies.CompanyName, dbo.ContactLists.ListName, dbo.Employees.EmployeeName, dbo.ContactLists.Comments, dbo.ContactLists.ListManagerEmployeeID, 
                         dbo.ContactLists.ListTypeID, dbo.Contacts.Title, dbo.Contacts.Active, dbo.RSVP.Rsvp, dbo.ContactListContacts.NumAttending, dbo.ContactListContacts.Field1, dbo.ContactListContacts.Field2, 
                         dbo.ContactListContacts.Field3, dbo.ContactListContacts.Field4, dbo.ContactListContacts.Field5, dbo.ContactListContacts.Field6, dbo.Contacts.DirectPhone, dbo.Contacts.EmailAddress, 
                         dbo.ContactListContacts.InsertFlag, dbo.ContactListContacts.UpdateFlag, dbo.ContactListContacts.DeleteFlag, dbo.ContactListContacts.Comment, dbo.Contacts.LastName, dbo.Addresses.Address1, 
                         dbo.Addresses.City, dbo.Addresses.State, dbo.Employees.EmpInitials, dbo.ContactListContacts.RespEmployeeID, dbo.Contacts.DoNotSolicit, dbo.ContactListContacts.RsvpID, dbo.Contacts.LastnameFirstname, 
                         dbo.ContactListContacts.AddedByEmployeeName, dbo.ContactListContacts.EditedByEmployeeName, dbo.ContactListContacts.LastUpdate, dbo.Companies.DBAName
FROM            dbo.ContactLists INNER JOIN
                         dbo.ContactListContacts ON dbo.ContactLists.ContactListID = dbo.ContactListContacts.ContactListID INNER JOIN
                         dbo.Contacts ON dbo.ContactListContacts.ContactID = dbo.Contacts.ContactID INNER JOIN
                         dbo.Companies ON dbo.Companies.CompanyID = dbo.Contacts.CompanyID INNER JOIN
                         dbo.RSVP ON dbo.ContactListContacts.RsvpID = dbo.RSVP.RsvpID LEFT OUTER JOIN
                         dbo.Employees ON dbo.ContactListContacts.RespEmployeeID = dbo.Employees.EmployeeID LEFT OUTER JOIN
                         dbo.Addresses ON dbo.Contacts.AddressID = dbo.Addresses.AddressID
ORDER BY dbo.Companies.CompanyName
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ContactListContactShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'  DisplayFlags = 280
            TopColumn = 6
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ContactListContactShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[43] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ContactLists"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ContactListContacts"
            Begin Extent = 
               Top = 6
               Left = 702
               Bottom = 182
               Right = 885
            End
            DisplayFlags = 280
            TopColumn = 17
         End
         Begin Table = "Contacts"
            Begin Extent = 
               Top = 6
               Left = 923
               Bottom = 197
               Right = 1152
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "Companies"
            Begin Extent = 
               Top = 200
               Left = 29
               Bottom = 367
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 72
         End
         Begin Table = "RSVP"
            Begin Extent = 
               Top = 78
               Left = 254
               Bottom = 171
               Right = 405
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employees"
            Begin Extent = 
               Top = 205
               Left = 324
               Bottom = 385
               Right = 528
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Addresses"
            Begin Extent = 
               Top = 228
               Left = 747
               Bottom = 347
               Right = 934
            End
          ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'ContactListContactShort';


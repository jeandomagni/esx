﻿CREATE VIEW dbo.BidSubmittedShort
AS
SELECT     dbo.Deals.DealCode, dbo.Deals.DealID, dbo.DealContactBids.BidOffer, dbo.DealContactBids.BidOfferAmt, dbo.DealContactBids.BidRound, 
                      dbo.DealContactBids.BidRank, dbo.DealContactBids.BidSubmitted, dbo.DealContactBids.BidRate, dbo.DealContactBids.BidSpread, 
                      dbo.DealContactBids.BidLoanTermYears, dbo.DealContactBids.BidPrepaymentPenaltyNote, dbo.DealContactBids.BidCapRate, dbo.DealContactBids.BidPricePSF, 
                      dbo.DealContactBids.BidGeneralNote, dbo.DealContactBids.BidOriginationFee, dbo.DealContactBids.BidReferenceIndex, dbo.DealContactBids.BidLoanType
FROM         dbo.DealContactBids INNER JOIN
                      dbo.DealContacts ON dbo.DealContactBids.DealContactID = dbo.DealContacts.DealContactID INNER JOIN
                      dbo.Deals ON dbo.DealContacts.DealID = dbo.Deals.DealID
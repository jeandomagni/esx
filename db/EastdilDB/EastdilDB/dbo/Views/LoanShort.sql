﻿CREATE VIEW dbo.LoanShort
AS
SELECT     dbo.Loans.LoanID, dbo.Loans.LoanName, dbo.Loans.LoanDescription, dbo.Properties.PropertyName, dbo.LoanTypes.LoanType, dbo.Loans.LoanBalance, 
                      dbo.Loans.OriginationDate, dbo.Loans.OriginalMaturityDate, dbo.Loans.AllInInterestRate, dbo.LoanRateTypes.LoanRateType, dbo.Loans.Amortization, 
                      dbo.Loans.First$LTV, dbo.Loans.Last$LTV, dbo.Loans.First$DebtYield, dbo.Loans.Last$DebtYield, dbo.Loans.First$PerSFOrUnit, dbo.Loans.IsOutstanding, 
                      dbo.Loans.IsEquityLike, dbo.Loans.PropertyID, dbo.Loans.Lender, dbo.Loans.LenderCompanyID, dbo.Loans.Borrowers, dbo.LoanBorrowers.BorrowerCompanyID, 
                      BorrowerCompanies.CompanyName AS BorrowerCompanyName, dbo.Loans.SpecialServicerCompanyID, 
                      BorrowerCompanies.CompanyName AS SpecialServicerCompany, dbo.Loans.NextMaturityDate, dbo.Loans.FinalMaturityDate, dbo.Loans.Last$PerSFOrUnit, 
                      dbo.Loans.LenderDBA, dbo.LoanBorrowers.BorrowerDBA, LenderCompanies.CompanyName AS LenderCompanyName
FROM         dbo.Companies AS LenderCompanies RIGHT OUTER JOIN
                      dbo.Loans ON LenderCompanies.CompanyID = dbo.Loans.LenderCompanyID LEFT OUTER JOIN
                      dbo.Companies AS BorrowerCompanies INNER JOIN
                      dbo.LoanBorrowers ON BorrowerCompanies.CompanyID = dbo.LoanBorrowers.BorrowerCompanyID ON 
                      dbo.Loans.LoanID = dbo.LoanBorrowers.LoanID LEFT OUTER JOIN
                      dbo.Companies AS SpecialServicerCompanies ON dbo.Loans.SpecialServicerCompanyID = SpecialServicerCompanies.CompanyID LEFT OUTER JOIN
                      dbo.LoanRateTypes ON dbo.Loans.LoanRateTypeID = dbo.LoanRateTypes.LoanRateTypeID LEFT OUTER JOIN
                      dbo.Properties ON dbo.Loans.PropertyID = dbo.Properties.PropertyID LEFT OUTER JOIN
                      dbo.LoanTypes ON dbo.Loans.LoanTypeID = dbo.LoanTypes.LoanTypeID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'LoanShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'          End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LenderCompanies"
            Begin Extent = 
               Top = 364
               Left = 0
               Bottom = 483
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 33
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3165
         Alias = 2145
         Table = 2400
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'LoanShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[48] 4[14] 2[21] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Loans"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 305
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoanBorrowers"
            Begin Extent = 
               Top = 446
               Left = 1185
               Bottom = 633
               Right = 1374
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BorrowerCompanies"
            Begin Extent = 
               Top = 485
               Left = 762
               Bottom = 686
               Right = 988
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SpecialServicerCompanies"
            Begin Extent = 
               Top = 0
               Left = 1154
               Bottom = 216
               Right = 1371
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoanRateTypes"
            Begin Extent = 
               Top = 274
               Left = 506
               Bottom = 378
               Right = 676
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Properties"
            Begin Extent = 
               Top = 34
               Left = 854
               Bottom = 284
               Right = 1032
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "LoanTypes"
            Begin Extent = 
               Top = 6
               Left = 302
               Bottom = 216
               Right = 453
  ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'LoanShort';


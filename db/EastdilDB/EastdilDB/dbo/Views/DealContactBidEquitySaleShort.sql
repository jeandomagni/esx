﻿CREATE VIEW dbo.DealContactBidEquitySaleShort
AS
SELECT     dc.DealContactID, d.DealID, con.FirstnameLastname, c.CompanyName, ms.MarketingStatus, dc.Tour, dcbes.BidDate, dcbes.BidRank, dcbes.BidComments, 
                      dcbes.OfferNumber, dcbes.Purchaser, dcbes.InvestorType, dcbes.InitialBidAmount, dcbes.EquityCapitalProvider, dcbes.RevisedBidAmount, dcbes.NetProceeds, 
                      dcbes.DepositInitial, dcbes.DepositTotal, dcbes.PropertyTour, dcbes.UnderwrittenCapital, dcbes.DebtFinancingNeededToClose, dcbes.DueDiligencePeriodDays, 
                      dcbes.ClosingPeriodDays, dcbes.PricePSFCALC, dcbes.InPlaceCapRateCALC, dcbes.Year1CapRateCALC, dcbes.StabilizedCapRateCALC, des.ProjectSize, 
                      dbo.ProjectSizeTypes.ProjectSizeType, d.DealCode, d.DealName, d.LocationCity, d.LocationState, dbo.PropertyTypes.PropertyType, des.PctLeased, 
                      con.LastnameFirstname, con.Active
FROM         dbo.DealContactBidEquitySale AS dcbes RIGHT OUTER JOIN
                      dbo.DealContacts AS dc ON dcbes.DealContactID = dc.DealContactID INNER JOIN
                      dbo.MarketingStatuses AS ms ON ms.MarketingStatusID = dc.MarketingStatusID INNER JOIN
                      dbo.Deals AS d ON d.DealID = dc.DealID INNER JOIN
                      dbo.DealEquitySales AS des ON des.DealID = d.DealID INNER JOIN
                      dbo.Contacts AS con ON con.ContactID = dc.ContactID INNER JOIN
                      dbo.Companies AS c ON c.CompanyID = dc.CompanyID INNER JOIN
                      dbo.ProjectSizeTypes ON des.ProjectSizeTypeID = dbo.ProjectSizeTypes.ProjectSizeTypeID INNER JOIN
                      dbo.PropertyTypes ON d.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID
WHERE     (dc.CALogged IS NOT NULL) AND (con.Active = 1)
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[56] 4[15] 2[26] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "dcbes"
            Begin Extent = 
               Top = 6
               Left = 612
               Bottom = 410
               Right = 862
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dc"
            Begin Extent = 
               Top = 6
               Left = 900
               Bottom = 322
               Right = 1135
            End
            DisplayFlags = 280
            TopColumn = 18
         End
         Begin Table = "ms"
            Begin Extent = 
               Top = 352
               Left = 975
               Bottom = 482
               Right = 1169
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 26
               Left = 1692
               Bottom = 426
               Right = 1959
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "des"
            Begin Extent = 
               Top = 67
               Left = 1187
               Bottom = 427
               Right = 1428
            End
            DisplayFlags = 280
            TopColumn = 16
         End
         Begin Table = "con"
            Begin Extent = 
               Top = 6
               Left = 319
               Bottom = 216
               Right = 574
            End
            DisplayFlags = 280
            TopColumn = 30
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 380
               Right = 281
            End
            DisplayFlags = 280
            TopColumn = 0', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactBidEquitySaleShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactBidEquitySaleShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'
         End
         Begin Table = "ProjectSizeTypes"
            Begin Extent = 
               Top = 264
               Left = 1465
               Bottom = 442
               Right = 1648
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 223
               Left = 2065
               Bottom = 353
               Right = 2259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4800
         Alias = 2430
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactBidEquitySaleShort';


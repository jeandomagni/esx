﻿CREATE VIEW dbo.MktgListExport
AS
SELECT     dbo.DealContacts.DealContactID, dbo.DealContacts.DealID, dbo.DealContacts.ContactID, dbo.Companies.CompanyName, dbo.Contacts.FirstnameLastname, 
                      dbo.Categories.Category, dbo.MarketingStatuses.MarketingStatus, dbo.DealContacts.Status, dbo.Classes.Class, dbo.DealContacts.Tier, 
                      dbo.Employees.EmployeeName AS CoverageInt, CoverageContacts.FullName AS CoverageExt, dbo.DealContacts.CoverageTeam, dbo.DealContacts.IMSent, 
                      dbo.DealContacts.CASent, dbo.DealContacts.CAUnApproved, dbo.DealContacts.CALogged, dbo.DealContacts.OMSent, dbo.DealContacts.WarroomAccess, 
                      dbo.DealContacts.OMDigital, dbo.Contacts.ETeasersBlocked, dbo.DealContacts.DoNotSendOM, dbo.DealContacts.DoNotSendWarroom, dbo.DealContacts.Tour, 
                      dbo.DealContacts.DIP AS AddlInfo, dbo.DealContacts.AdditionalInfo AS AddlInfoNote, dbo.DealContacts.DDSent, dbo.DealContacts.BidOffer, dbo.DealContacts.Won, 
                      dbo.DealContacts.LastUpdate, dbo.Contacts.Active, dbo.Contacts.DoNotSolicit, dbo.Contacts.SpecialConditions, dbo.Contacts.Salutation, dbo.Contacts.Title, 
                      dbo.Contacts.FirstName, dbo.Contacts.MiddleName, dbo.Contacts.LastName, dbo.Contacts.Prefix, dbo.Contacts.Suffix, dbo.Contacts.FullName, 
                      dbo.Contacts.EmailAddress, dbo.Contacts.SecondaryEmail, dbo.Contacts.DirectPhone, dbo.Contacts.Fax, dbo.Addresses.Address1, dbo.Addresses.Address2, 
                      dbo.Addresses.Address3, dbo.Addresses.City, dbo.Addresses.State, dbo.Addresses.PostalCode, dbo.Addresses.Country, 
                      Employees_1.EmployeeName AS MktgApprvlEmployee, dbo.Employees.EmpInitials, dbo.DealContacts.BidSubmitted, dbo.DealContacts.BidRank, 
                      dbo.DealContacts.BidOfferAmt, dbo.DealContacts.BidDepositNote, dbo.DealContacts.BidDDPeriodNote, dbo.DealContacts.BidDebtFinancing, 
                      dbo.DealContacts.BidRate, dbo.DealContacts.BidSpread, dbo.DealContacts.BidLoanTermYears, dbo.DealContacts.BidPrepaymentPenaltyNote, 
                      dbo.DealContacts.BidPricePSF, dbo.DealContacts.BidEquityCapital, dbo.DealContacts.BidCapRate, dbo.DealContacts.BidGeneralNote, 
                      dbo.DealContacts.BidOriginationFee, dbo.DealContacts.BidLoanType, dbo.DealContacts.BidReferenceIndex, dbo.Contacts.PrefersDigitalOM, 
                      dbo.DealContacts.BidRound, dbo.DealContacts.CAPortal, dbo.DealContacts.CAMethod
FROM         dbo.Deals INNER JOIN
                      dbo.DealContacts ON dbo.Deals.DealID = dbo.DealContacts.DealID INNER JOIN
                      dbo.MarketingStatuses ON dbo.DealContacts.MarketingStatusID = dbo.MarketingStatuses.MarketingStatusID INNER JOIN
                      dbo.Companies ON dbo.DealContacts.CompanyID = dbo.Companies.CompanyID INNER JOIN
                      dbo.Contacts ON dbo.DealContacts.ContactID = dbo.Contacts.ContactID INNER JOIN
                      dbo.Categories ON dbo.DealContacts.CategoryID = dbo.Categories.CategoryID INNER JOIN
                      dbo.Classes ON dbo.DealContacts.ClassID = dbo.Classes.ClassID LEFT OUTER JOIN
                      dbo.Employees ON dbo.DealContacts.EmployeeID = dbo.Employees.EmployeeID LEFT OUTER JOIN
                      dbo.Contacts AS CoverageContacts ON dbo.DealContacts.CoverageContactID = CoverageContacts.ContactID LEFT OUTER JOIN
                      dbo.Addresses ON dbo.Contacts.AddressID = dbo.Addresses.AddressID LEFT OUTER JOIN
                      dbo.Employees AS Employees_1 ON dbo.Contacts.MarketingApprovalEmployeeID = Employees_1.EmployeeID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListExport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'       DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employees"
            Begin Extent = 
               Top = 230
               Left = 1084
               Bottom = 381
               Right = 1288
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CoverageContacts"
            Begin Extent = 
               Top = 122
               Left = 24
               Bottom = 325
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Addresses"
            Begin Extent = 
               Top = 335
               Left = 86
               Bottom = 443
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employees_1"
            Begin Extent = 
               Top = 310
               Left = 686
               Bottom = 429
               Right = 899
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 55
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 2355
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListExport';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[48] 4[13] 2[33] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -285
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealContacts"
            Begin Extent = 
               Top = 1
               Left = 445
               Bottom = 359
               Right = 793
            End
            DisplayFlags = 280
            TopColumn = 75
         End
         Begin Table = "MarketingStatuses"
            Begin Extent = 
               Top = 3
               Left = 740
               Bottom = 111
               Right = 918
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Companies"
            Begin Extent = 
               Top = 46
               Left = 1095
               Bottom = 154
               Right = 1312
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Contacts"
            Begin Extent = 
               Top = 176
               Left = 326
               Bottom = 336
               Right = 555
            End
            DisplayFlags = 280
            TopColumn = 63
         End
         Begin Table = "Categories"
            Begin Extent = 
               Top = 133
               Left = 962
               Bottom = 226
               Right = 1113
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Classes"
            Begin Extent = 
               Top = 166
               Left = 633
               Bottom = 274
               Right = 811
            End
     ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListExport';


﻿CREATE VIEW dbo.DealFinancingLong
AS
SELECT     dbo.Deals.DealID, dbo.Deals.DealCode, dbo.Deals.DealName, dbo.Companies.CompanyName, dbo.TransactionTypes.TransactionType, 
                      dbo.Deals.TransactionTypeID, dbo.PropertyTypes.PropertyType, dbo.DealStatuses.DealStatus, dbo.Deals.Active, dbo.Deals.Confidential, dbo.Deals.DealOffice, 
                      dbo.Deals.DealLeader, dbo.Deals.DealManager, dbo.Deals.ESTeam, dbo.Deals.LocationAddress, dbo.Deals.LocationCity, dbo.Deals.LocationState, 
                      dbo.Deals.MktgListSize, dbo.DealTimelines.DateToMarket, dbo.DealTimelines.DateCloseAct, dbo.DealFinancings.ProjectSize, dbo.ProjectSizeTypes.ProjectSizeType, 
                      dbo.DealFinancings.ProjectSizeTypeID, dbo.DealFinancings.PctLeased, dbo.DealFinancings.CMBSTransaction, dbo.FinancingTypes.FinancingType, 
                      dbo.DealFinancings.FinancingTypeID, dbo.DealFinancings.PurchasePrice_Valuation, dbo.DealFinancings.PricePerSqFt, dbo.DealFinancings.Occupancy, 
                      dbo.DealFinancings.CapRate, dbo.DealFinancings.CapRateNote, dbo.DealFinancings.NOI, dbo.DealFinancings.NOIDescription, dbo.DealFinancings.LenderName, 
                      LenderCompanies.CompanyName AS LenderCompany, dbo.DealFinancings.LenderCompanyID, dbo.LenderTypes.LenderType, dbo.DealFinancings.LenderTypeID, 
                      dbo.DealFinancings.LoanAmount, dbo.DealFinancings.LoanPerSFOrUnit, dbo.DealFinancings.LTV_LTC, dbo.DealFinancings.DebtYield, dbo.LoanTypes.LoanType, 
                      dbo.DealFinancings.LoanTypeID, dbo.DealFinancings.LoanTerm, dbo.LoanRateTypes.LoanRateType, dbo.DealFinancings.LoanRateTypeID, 
                      dbo.DealFinancings.ApproxDateQuoted, dbo.ReferenceIndex.ReferenceIndex, dbo.DealFinancings.ReferenceIndexID, dbo.DealFinancings.Spread, 
                      dbo.DealFinancings.TreasuryWhenLocked, dbo.DealFinancings.InitialRate, dbo.DealFinancings.DateLocked, dbo.DealFinancings.RateFloor, 
                      dbo.DealFinancings.OriginationFee, dbo.DealFinancings.Amortization, dbo.DealFinancings.Prepayment, dbo.DealFinancings.LockOut, 
                      dbo.DealFinancings.OpenPeriod, dbo.DealFinancings.MaxLTV, dbo.DealFinancings.Recourse, dbo.DealFinancings.Reserves, dbo.DealFinancings.SponsorName, 
                      SponsorCompanies.CompanyName AS SponsorCompany, dbo.DealFinancings.SponsorCompanyID, dbo.SponsorTypes.SponsorType, 
                      dbo.DealFinancings.SponsorTypeID, dbo.DealFinancings.UrbanOrSuburban, dbo.DealFinancings.StablizedOrNonstabilized, dbo.DealFinancings.NOIFirstYear, 
                      dbo.DealFinancings.NOIStabilized
FROM         dbo.Companies INNER JOIN
                      dbo.Deals ON dbo.Companies.CompanyID = dbo.Deals.ClientCompanyID INNER JOIN
                      dbo.DealTimelines ON dbo.Deals.DealID = dbo.DealTimelines.DealID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID INNER JOIN
                      dbo.DealStatuses ON dbo.Deals.DealStatusID = dbo.DealStatuses.DealStatusID INNER JOIN
                      dbo.PropertyTypes ON dbo.Deals.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID INNER JOIN
                      dbo.DealFinancings ON dbo.Deals.DealID = dbo.DealFinancings.DealID LEFT OUTER JOIN
                      dbo.ProjectSizeTypes ON dbo.DealFinancings.ProjectSizeTypeID = dbo.ProjectSizeTypes.ProjectSizeTypeID LEFT OUTER JOIN
                      dbo.SponsorTypes ON dbo.DealFinancings.SponsorTypeID = dbo.SponsorTypes.SponsorTypeID LEFT OUTER JOIN
                      dbo.LenderTypes ON dbo.DealFinancings.LenderTypeID = dbo.LenderTypes.LenderTypeID LEFT OUTER JOIN
                      dbo.Companies AS LenderCompanies ON dbo.DealFinancings.LenderCompanyID = LenderCompanies.CompanyID LEFT OUTER JOIN
                      dbo.Companies AS SponsorCompanies ON dbo.DealFinancings.SponsorCompanyID = SponsorCompanies.CompanyID LEFT OUTER JOIN
                      dbo.FinancingTypes ON dbo.DealFinancings.FinancingTypeID = dbo.FinancingTypes.FinancingTypeID LEFT OUTER JOIN
                      dbo.LoanRateTypes ON dbo.DealFinancings.LoanRateTypeID = dbo.LoanRateTypes.LoanRateTypeID LEFT OUTER JOIN
                      dbo.ReferenceIndex ON dbo.DealFinancings.ReferenceIndexID = dbo.ReferenceIndex.ReferenceIndexID LEFT OUTER JOIN
                      dbo.LoanTypes ON dbo.DealFinancings.LoanTypeID = dbo.LoanTypes.LoanTypeID
WHERE     (dbo.Deals.TransactionTypeID = 12)
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane3', @value = N'dth = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4935
         Alias = 1785
         Table = 2835
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealFinancingLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 3, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealFinancingLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'nd
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProjectSizeTypes"
            Begin Extent = 
               Top = 497
               Left = 1563
               Bottom = 616
               Right = 1740
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SponsorTypes"
            Begin Extent = 
               Top = 45
               Left = 1095
               Bottom = 161
               Right = 1249
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LenderTypes"
            Begin Extent = 
               Top = 15
               Left = 835
               Bottom = 123
               Right = 986
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LenderCompanies"
            Begin Extent = 
               Top = 162
               Left = 1014
               Bottom = 281
               Right = 1240
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SponsorCompanies"
            Begin Extent = 
               Top = 282
               Left = 1101
               Bottom = 401
               Right = 1327
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FinancingTypes"
            Begin Extent = 
               Top = 214
               Left = 848
               Bottom = 410
               Right = 1017
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoanRateTypes"
            Begin Extent = 
               Top = 480
               Left = 986
               Bottom = 584
               Right = 1156
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ReferenceIndex"
            Begin Extent = 
               Top = 646
               Left = 294
               Bottom = 750
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoanTypes"
            Begin Extent = 
               Top = 657
               Left = 877
               Bottom = 761
               Right = 1037
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 69
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Wi', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealFinancingLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[43] 4[30] 2[12] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 17
               Left = 286
               Bottom = 250
               Right = 483
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealTimelines"
            Begin Extent = 
               Top = 6
               Left = 528
               Bottom = 114
               Right = 745
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 315
               Left = 278
               Bottom = 423
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealStatuses"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 339
               Left = 45
               Bottom = 447
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealFinancings"
            Begin Extent = 
               Top = 134
               Left = 516
               Bottom = 710
               Right = 727
            E', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealFinancingLong';


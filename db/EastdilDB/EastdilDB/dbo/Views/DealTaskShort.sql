﻿CREATE VIEW dbo.DealTaskShort
AS
SELECT     dbo.Deals.DealCode, dbo.Deals.DealName, WPInternalEmployees.EmpInitials AS EmpInitialsAssignedTo, 
                      WPInternalEmployees.EmployeeName AS EmployeeNameAssignedTo, dbo.Employees.EmpInitials AS EmpInitialsAssignedBy, 
                      dbo.Employees.EmployeeName AS EmployeeNameAssignedBy, dbo.WorkingPartyRoles.WorkingPartyRole, dbo.Deliverables.Deliverable, 
                      dbo.WorkActivities.WorkActivity, dbo.DealTasks.TaskDescription, dbo.DealTasks.StatusUpdate, dbo.DealTasks.Active, dbo.DealTasks.Complete, 
                      dbo.DealTasks.DateAdded, dbo.DealTasks.LastUpdate, dbo.DealTasks.DealTaskID, dbo.DealTasks.DealID, dbo.DealTasks.DealWPInternalID, 
                      dbo.DealTasks.DeliverableID, dbo.DealTasks.WorkActivityID, dbo.DealTasks.AssignedByEmployeeID, dbo.DealTasks.Priority, dbo.DealTasks.TimeRequiredID, 
                      dbo.DealTasks.DueDate, dbo.TimeRequired.TimeRequired, dbo.Deals.DealOffice
FROM         dbo.Deals INNER JOIN
                      dbo.DealTasks ON dbo.Deals.DealID = dbo.DealTasks.DealID INNER JOIN
                      dbo.DealWPInternal ON dbo.DealTasks.DealWPInternalID = dbo.DealWPInternal.DealWPInternalID INNER JOIN
                      dbo.Deliverables ON dbo.DealTasks.DeliverableID = dbo.Deliverables.DeliverableID INNER JOIN
                      dbo.Employees AS WPInternalEmployees ON dbo.DealWPInternal.EmployeeID = WPInternalEmployees.EmployeeID INNER JOIN
                      dbo.WorkActivities ON dbo.DealTasks.WorkActivityID = dbo.WorkActivities.WorkActivityID INNER JOIN
                      dbo.WorkingPartyRoles ON dbo.DealWPInternal.WorkingPartyRoleID = dbo.WorkingPartyRoles.WorkingPartyRoleID INNER JOIN
                      dbo.Employees ON dbo.DealTasks.AssignedByEmployeeID = dbo.Employees.EmployeeID INNER JOIN
                      dbo.TimeRequired ON dbo.DealTasks.TimeRequiredID = dbo.TimeRequired.TimeRequiredID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealTaskShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'     End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employees"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 365
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TimeRequired"
            Begin Extent = 
               Top = 234
               Left = 489
               Bottom = 338
               Right = 654
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealTaskShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 291
            End
            DisplayFlags = 280
            TopColumn = 12
         End
         Begin Table = "DealTasks"
            Begin Extent = 
               Top = 6
               Left = 329
               Bottom = 125
               Right = 530
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealWPInternal"
            Begin Extent = 
               Top = 6
               Left = 568
               Bottom = 125
               Right = 755
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deliverables"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 230
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WPInternalEmployees"
            Begin Extent = 
               Top = 126
               Left = 236
               Bottom = 245
               Right = 451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WorkActivities"
            Begin Extent = 
               Top = 126
               Left = 489
               Bottom = 230
               Right = 650
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WorkingPartyRoles"
            Begin Extent = 
               Top = 126
               Left = 688
               Bottom = 245
               Right = 875
       ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealTaskShort';


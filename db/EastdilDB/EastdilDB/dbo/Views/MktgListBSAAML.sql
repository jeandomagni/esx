﻿CREATE VIEW dbo.MktgListBSAAML
AS
SELECT     dbo.Contacts.ContactID, dbo.Contacts.FullName, dbo.Contacts.Active, dbo.Contacts.CompanyID, dbo.Companies.CompanyName, dbo.Contacts.Title, 
                      dbo.Contacts.EmailAddress, dbo.Contacts.DirectPhone, dbo.Contacts.MobilePhone, dbo.Addresses.Address1, dbo.Addresses.City, dbo.Addresses.State, 
                      dbo.Contacts.NumDealsOn, dbo.Contacts.DealConvPct, dbo.Addresses.Country, dbo.Contacts.LastName, dbo.Contacts.FirstName, dbo.Contacts.CriteriaFlag, 
                      dbo.Contacts.EmailUpdateFlag, dbo.Contacts.AddressID, dbo.Companies.MainPhone, dbo.Contacts.MainPhoneExt, dbo.Companies.PrimaryCompanyRecord, 
                      dbo.Companies.DBAName, dbo.Companies.TickerSymbol, dbo.Contacts.CALogged, dbo.Categories.Category, dbo.Contacts.ESContactEmployeeID, 
                      dbo.Contacts.UserEmail, dbo.Contacts.FirstnameLastname, dbo.Contacts.CategoryID, dbo.Addresses.PostalCode, dbo.Contacts.CanadaOptIn, 
                      dbo.Contacts.SentToCDC, dbo.Contacts.SentToCDCDate, dbo.Contacts.NameChange, dbo.Contacts.AddressChange, dbo.Contacts.DoNotSolicit, 
                      dbo.Contacts.DoNotSolicitAsOfDate, dbo.Deals.Active AS ActiveDeal, dbo.Deals.DealCode, dbo.MarketingStatuses.MarketingStatus, 
                      dbo.DealContacts.MarketingStatusID
FROM         dbo.Contacts INNER JOIN
                      dbo.Addresses ON dbo.Contacts.AddressID = dbo.Addresses.AddressID INNER JOIN
                      dbo.Companies ON dbo.Contacts.CompanyID = dbo.Companies.CompanyID INNER JOIN
                      dbo.DealContacts ON dbo.Contacts.ContactID = dbo.DealContacts.ContactID AND dbo.Companies.CompanyID = dbo.DealContacts.CompanyID INNER JOIN
                      dbo.Categories ON dbo.DealContacts.CategoryID = dbo.Categories.CategoryID INNER JOIN
                      dbo.Deals ON dbo.DealContacts.DealID = dbo.Deals.DealID INNER JOIN
                      dbo.MarketingStatuses ON dbo.DealContacts.MarketingStatusID = dbo.MarketingStatuses.MarketingStatusID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[34] 4[40] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -264
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Contacts"
            Begin Extent = 
               Top = 0
               Left = 15
               Bottom = 169
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 77
         End
         Begin Table = "Addresses"
            Begin Extent = 
               Top = 87
               Left = 442
               Bottom = 206
               Right = 631
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Companies"
            Begin Extent = 
               Top = 37
               Left = 772
               Bottom = 156
               Right = 998
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealContacts"
            Begin Extent = 
               Top = 347
               Left = 470
               Bottom = 466
               Right = 662
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "Categories"
            Begin Extent = 
               Top = 172
               Left = 71
               Bottom = 276
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 276
               Left = 1011
               Bottom = 470
               Right = 1264
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "MarketingStatuses"
            Begin Extent = 
               Top = 353
               Left = 85
               Bottom = 472
               Right = 272
            End
    ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListBSAAML';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListBSAAML';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'        DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 42
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListBSAAML';


﻿CREATE VIEW dbo.PropertyValuationLong
AS
SELECT     dbo.Properties.PropertyName, dbo.PropertyValuations.PropertyValuationID, dbo.PropertyValuations.PropertyID, dbo.PropertyValuations.DealID, 
                      dbo.PropertyValuations.PostedByEmployeeID, dbo.PropertyValuations.ValuationDate, dbo.PropertyValuations.Value, dbo.PropertyValuations.ValuePerUnit, 
                      dbo.PropertyValuations.ProjectSizeTypeID, dbo.PropertyValuations.CurrentOwner, dbo.PropertyValuations.MostRecentSeller, dbo.PropertyValuations.DateSold, 
                      dbo.PropertyValuations.JVOwnership, dbo.PropertyValuations.JVOwnershipList, dbo.PropertyValuations.InPlaceNOI, dbo.PropertyValuations.InPlaceOccupany, 
                      dbo.PropertyValuations.Year1NOI, dbo.PropertyValuations.Year5NOI, dbo.PropertyValuations.Year10NOI, dbo.PropertyValuations.FiveYrMarketRentCAGR, 
                      dbo.PropertyValuations.FiveYrNOICAGR, dbo.PropertyValuations.TenYrMarketRentCAGR, dbo.PropertyValuations.TenYrNOICAGR, 
                      dbo.PropertyValuations.IsOfficeIndustrial, dbo.PropertyValuations.MarketRent, dbo.PropertyValuations.GeneralVacancy, dbo.PropertyValuations.ExpenseRatio, 
                      dbo.PropertyValuations.ReimbursementMethod, dbo.PropertyValuations.UnderwrittenLeaseTerm, dbo.PropertyValuations.MarketTenantImprovements, 
                      dbo.PropertyValuations.ExpiringSFInNextFiveYears, dbo.PropertyValuations.ExpiringSFPercentOfTotalInNextFiveYears, 
                      dbo.PropertyValuations.ExpiringRentInNextFiveYears, dbo.PropertyValuations.ExpiringRentPercentOfTotalInNextFiveYears, 
                      dbo.PropertyValuations.OfficeIndustrialNote, dbo.PropertyValuations.IsMultifamily, dbo.PropertyValuations.StabilizedOccupancy, 
                      dbo.PropertyValuations.OtherIncomePerUnit, dbo.PropertyValuations.OperatingExpensesPerUnit, dbo.PropertyValuations.OperatingMargin, 
                      dbo.PropertyValuations.ReplacementReserves, dbo.PropertyValuations.MultifamilyNote, dbo.PropertyValuations.IsRetail, 
                      dbo.PropertyValuations.OperatingExpensesPerSF, dbo.PropertyValuations.InlineTenantImprovements, dbo.PropertyValuations.AnchorTenantImprovements, 
                      dbo.PropertyValuations.InlineMarketRent, dbo.PropertyValuations.AnchorMarketRent, dbo.PropertyValuations.InlineSalesPerSF, 
                      dbo.PropertyValuations.InlineOccupancyCost, dbo.PropertyValuations.InlineOccupancyCostPct, dbo.PropertyValuations.IsOneAnchorAGrocery, 
                      dbo.PropertyValuations.AnchorTenants, dbo.PropertyValuations.AnchorSalesPerSqFt, dbo.PropertyValuations.RetailNote, dbo.PropertyValuations.IsHotel, 
                      dbo.PropertyValuations.FiveYrRevPARCAGR, dbo.PropertyValuations.TenYrRevPARCAGR, dbo.PropertyValuations.CurrentYear, 
                      dbo.PropertyValuations.CurrentOccupancy, dbo.PropertyValuations.CurrentADR, dbo.PropertyValuations.CurrentRevPAR, 
                      dbo.PropertyValuations.GrossOperatingMarginPct, dbo.PropertyValuations.CurrentNOI, dbo.PropertyValuations.CurrentCapRate, 
                      dbo.PropertyValuations.CurrentEBITDA, dbo.PropertyValuations.FFEReserve, dbo.PropertyValuations.Flag, dbo.PropertyValuations.ManagementOrFranchise, 
                      dbo.PropertyValuations.Management, dbo.PropertyValuations.ManagementUnencumbered, dbo.PropertyValuations.Brand, 
                      dbo.PropertyValuations.BrandUnencumbered, dbo.PropertyValuations.PIPAmount, dbo.PropertyValuations.HotelNote, dbo.PropertyValuations.IsCondo, 
                      dbo.PropertyValuations.TotalUnitsForSale, dbo.PropertyValuations.ProjectedUnitsSold, dbo.PropertyValuations.ProjectedSFSold, 
                      dbo.PropertyValuations.UnitSalesStartDate, dbo.PropertyValuations.SalesPerMonth, dbo.PropertyValuations.UnitSalesEndDate, 
                      dbo.PropertyValuations.TotalSelloutPeriod, dbo.PropertyValuations.AvgSalesPricePerSF, dbo.PropertyValuations.AvgSalesPricePerUnit, 
                      dbo.PropertyValuations.GrossSalesPRoceeds, dbo.PropertyValuations.SalesCost, dbo.PropertyValuations.CondoNote, dbo.PropertyValuations.IsDevelopment, 
                      dbo.PropertyValuations.BuildablePropertySize, dbo.PropertyValuations.BuildablePropertySFOrUnits, dbo.PropertyValuations.BuildablePropertyNote, 
                      dbo.PropertyValuations.DevelopmentCost, dbo.PropertyValuations.DevelopmentCostPerBuildableSFOrUnit, dbo.PropertyValuations.DevelopmentStartDate, 
                      dbo.PropertyValuations.DevelopmentPeriod, dbo.PropertyValuations.DevelopmentEndDate, dbo.PropertyValuations.StabilizedNOI, 
                      dbo.PropertyValuations.StabilizationYear, dbo.PropertyValuations.StabilizedPropertyValue, dbo.PropertyValuations.StabilizedValuePerSFOrUnit, 
                      dbo.PropertyValuations.StabilizedCapRate, dbo.PropertyValuations.ReturnOnCost, dbo.PropertyValuations.ReturnOnCostNote, 
                      dbo.PropertyValuations.DevelopmentNote, dbo.PropertyValuations.DateAdded, dbo.PropertyValuations.LastUpdate
FROM         dbo.PropertyValuations INNER JOIN
                      dbo.Properties ON dbo.PropertyValuations.PropertyID = dbo.Properties.PropertyID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PropertyValuationLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PropertyValuations"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 341
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Properties"
            Begin Extent = 
               Top = 6
               Left = 379
               Bottom = 125
               Right = 568
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PropertyValuationLong';


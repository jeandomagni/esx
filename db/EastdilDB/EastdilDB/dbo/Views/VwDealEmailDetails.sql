﻿CREATE VIEW dbo.VwDealEmailDetails
AS
SELECT     dbo.Deals.DealID, dbo.Deals.DealName, dbo.Deals.DealCode, dbo.Deals.DealOffice, dbo.Deals.DealFolderPath, dbo.TransactionTypes.TransactionType, 
                      dbo.Companies.CompanyName, dbo.Deals.ESGSDealFolderPath
FROM         dbo.Companies INNER JOIN
                      dbo.Deals ON dbo.Companies.CompanyID = dbo.Deals.ClientCompanyID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID
﻿CREATE VIEW dbo.MktgListCompaniesOnly
AS
SELECT DISTINCT 
                      TOP (100) PERCENT MAX(dbo.Companies.CompanyID) AS CompanyID, MAX(dbo.Companies.CompanyName) AS CompanyName, MAX(dbo.Companies.DBAName) 
                      AS DBAName, MAX(dbo.TransactionTypes.TransactionType) AS DealType, MAX(dbo.Deals.DealCode) AS DealCode, MAX(dbo.Deals.DateAdded) AS DateAdded, 
                      MAX(dbo.Deals.DateClosed) AS DateClosed, COUNT(dbo.DealContacts.DealContactID) AS Contacts, MAX(dbo.DealContacts.CASent) AS CASent, 
                      MAX(dbo.DealContacts.OMSent) AS OMSent, MAX(dbo.DealContacts.Tour) AS Tour, MAX(CASE WHEN BidSubmitted = 1 THEN 1 ELSE 0 END) AS BidSubmitted, 
                      MIN(dbo.DealContacts.BidRank) AS BidRank, MAX(dbo.DealContacts.BidOffer) AS BidOffer, MAX(dbo.DealContacts.BidOfferAmt) AS BidOfferAmt, 
                      MAX(dbo.DealContacts.Won) AS Won, MAX(dbo.DealContacts.MarketingStatusID) AS MarketingStatusID, dbo.Companies.CompanyID AS Expr1, dbo.Deals.DealID, 
                      dbo.Deals.DealName, dbo.DealEquitySales.Valuation AS EquitySaleValuation, dbo.DealFinancings.PurchasePrice_Valuation AS FinancingValuation
FROM         dbo.Companies INNER JOIN
                      dbo.DealContacts ON dbo.Companies.CompanyID = dbo.DealContacts.CompanyID INNER JOIN
                      dbo.Deals ON dbo.DealContacts.DealID = dbo.Deals.DealID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID INNER JOIN
                      dbo.DealEquitySales ON dbo.Deals.DealID = dbo.DealEquitySales.DealID INNER JOIN
                      dbo.DealFinancings ON dbo.Deals.DealID = dbo.DealFinancings.DealID
WHERE     (dbo.DealContacts.MarketingStatusID <> 12)
GROUP BY dbo.Companies.CompanyName, dbo.Companies.CompanyID, dbo.Deals.DealID, dbo.Deals.DealName, dbo.DealEquitySales.Valuation, 
                      dbo.DealFinancings.PurchasePrice_Valuation
ORDER BY CompanyName
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListCompaniesOnly';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'00
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 2340
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListCompaniesOnly';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 280
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealContacts"
            Begin Extent = 
               Top = 6
               Left = 318
               Bottom = 125
               Right = 526
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 564
               Bottom = 314
               Right = 833
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 0
               Left = 1144
               Bottom = 119
               Right = 1347
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealEquitySales"
            Begin Extent = 
               Top = 152
               Left = 1002
               Bottom = 307
               Right = 1236
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealFinancings"
            Begin Extent = 
               Top = 154
               Left = 1411
               Bottom = 324
               Right = 1625
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 15', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MktgListCompaniesOnly';


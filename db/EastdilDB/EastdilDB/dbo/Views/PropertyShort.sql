﻿CREATE VIEW dbo.PropertyShort
AS
SELECT     dbo.Properties.PropertyID, dbo.Properties.PropertyName, dbo.Properties.PropertyDescription, dbo.PropertyTypes.PropertyType, dbo.AssetClasses.AssetClass, 
                      dbo.Properties.Address1, dbo.Properties.City, dbo.Properties.State, dbo.Properties.Country, dbo.Properties.PropertySize, dbo.Properties.DateAcquired, 
                      dbo.Properties.Buyer, dbo.Properties.BuyerDBA, dbo.Properties.DateSold, dbo.Properties.Seller, dbo.Properties.SellerDBA, dbo.Properties.BuyerCompanyID, 
                      dbo.Properties.SellerCompanyID, dbo.ProjectSizeTypes.ProjectSizeType, dbo.Properties.MostRecentValue, dbo.Properties.ValuePerPropertySize, dbo.Properties.NOI, 
                      dbo.Properties.LenderDBA, dbo.Properties.LoanBalance, dbo.Properties.LoanMaturityDate, dbo.Properties.LoanInterestRate, dbo.Properties.LenderCompanyID
FROM         dbo.ProjectSizeTypes INNER JOIN
                      dbo.Properties ON dbo.ProjectSizeTypes.ProjectSizeTypeID = dbo.Properties.ProjectSizeTypeID LEFT OUTER JOIN
                      dbo.PropertyTypes ON dbo.Properties.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID LEFT OUTER JOIN
                      dbo.AssetClasses ON dbo.Properties.AssetClassID = dbo.AssetClasses.AssetClassID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -29
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProjectSizeTypes"
            Begin Extent = 
               Top = 6
               Left = 571
               Bottom = 125
               Right = 748
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Properties"
            Begin Extent = 
               Top = 17
               Left = 67
               Bottom = 345
               Right = 245
            End
            DisplayFlags = 280
            TopColumn = 22
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 17
               Left = 355
               Bottom = 125
               Right = 533
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AssetClasses"
            Begin Extent = 
               Top = 160
               Left = 444
               Bottom = 253
               Right = 595
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 27
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 150', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PropertyShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PropertyShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'0
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2055
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PropertyShort';


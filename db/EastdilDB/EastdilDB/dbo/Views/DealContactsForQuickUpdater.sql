﻿CREATE VIEW dbo.DealContactsForQuickUpdater
AS
SELECT        dbo.Deals.DealID, dbo.DealContacts.DealContactID, dbo.DealContacts.EmployeeID, dbo.DealContacts.ContactID, dbo.DealContacts.MarketingStatusID, dbo.Deals.DealCode, dbo.Companies.CompanyName, 
                         dbo.Contacts.FullName, dbo.MarketingStatuses.MarketingStatus, dbo.Contacts.LastName, dbo.Contacts.FirstName, dbo.Contacts.EmailAddress, dbo.Contacts.DirectPhone, dbo.Contacts.FirstnameLastname, 
                         dbo.Deals.Active, dbo.Contacts.Notes, dbo.DealContacts.IMSent, dbo.DealContacts.CASent, dbo.DealContacts.CAUnApproved, dbo.DealContacts.CALogged, dbo.DealContacts.OMSent, dbo.DealContacts.OMDigital, 
                         dbo.DealContacts.WarroomAccess, dbo.DealContacts.Tour, dbo.DealContacts.CompanyID, dbo.DealContacts.DealID AS Expr1
FROM            dbo.Deals INNER JOIN
                         dbo.DealContacts ON dbo.Deals.DealID = dbo.DealContacts.DealID INNER JOIN
                         dbo.Contacts ON dbo.DealContacts.ContactID = dbo.Contacts.ContactID INNER JOIN
                         dbo.Companies ON dbo.Contacts.CompanyID = dbo.Companies.CompanyID INNER JOIN
                         dbo.MarketingStatuses ON dbo.DealContacts.MarketingStatusID = dbo.MarketingStatuses.MarketingStatusID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactsForQuickUpdater';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'      Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactsForQuickUpdater';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 305
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealContacts"
            Begin Extent = 
               Top = 6
               Left = 343
               Bottom = 263
               Right = 578
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Contacts"
            Begin Extent = 
               Top = 6
               Left = 616
               Bottom = 244
               Right = 871
            End
            DisplayFlags = 280
            TopColumn = 38
         End
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 909
               Bottom = 136
               Right = 1152
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MarketingStatuses"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
   ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactsForQuickUpdater';


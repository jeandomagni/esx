﻿CREATE VIEW dbo.DocumentLoanSaleLong
AS
SELECT     dbo.Deals.DealID, dbo.Deals.DealCode, dbo.Deals.DealName, dbo.Companies.CompanyName, dbo.TransactionTypes.TransactionType, 
                      dbo.PropertyTypes.PropertyType, dbo.Deals.Active, dbo.Deals.Confidential, dbo.Deals.DealOffice, dbo.Deals.DealLeader, dbo.Deals.DealManager, 
                      dbo.Deals.ESTeam, dbo.Deals.LocationAddress, dbo.Deals.LocationCity, dbo.Deals.LocationState, dbo.Deals.MktgListSize, dbo.DealTimelines.DateToMarket, 
                      dbo.DealTimelines.DateCloseAct, dbo.DealLoanSales.Seller, dbo.DealLoanSales.Buyers, dbo.DealLoanSales.NumberOfLoans, dbo.DealLoanSales.PctLeased, 
                      dbo.DealLoanSales.LoanTypeID, dbo.DealLoanSales.UnpaidLoanBalance, dbo.DealLoanSales.UnpaidLoanBalancePerSF, dbo.DealLoanSales.PurchasePrice, 
                      dbo.DealLoanSales.PurchasePricePerSF, dbo.DealLoanSales.PurchasePricePctOfUPB, dbo.DealLoanSales.LoanRateTypeID, dbo.DealLoanSales.EquityLike, 
                      dbo.DealLoanSales.OriginationDate, dbo.DealLoanSales.MaturityDate, dbo.DealLoanSales.Coupon, dbo.DealLoanSales.CashOnCash, 
                      dbo.DealLoanSales.MostRecentDSCR, dbo.DealLoanSales.InPlaceNOI, dbo.DealLoanSales.EstimatedLTV, dbo.DealLoanSales.Amortization, 
                      dbo.DealLoanSales.YieldToMaturity, dbo.DealLoanSales.NumberOfOffers, dbo.Documents.FileName, dbo.DocumentTypes.DocumentType, 
                      dbo.Documents.DocumentTitle, dbo.Documents.StorageLocation, dbo.DocumentFor.DocumentFor, dbo.Documents.BestPracticeDoc, dbo.Documents.DateCreated, 
                      dbo.Documents.DocumentID, dbo.DealStatuses.DealStatus, dbo.DealLoanSales.SellerTypeID, dbo.DealLoanSales.BuyerTypeID, dbo.DealLoanSales.PropertyNOI, 
                      dbo.DealLoanSales.PropertySite, dbo.DealLoanSales.PropertyValue, dbo.DealLoanSales.PropertyValuePerSF, dbo.DealLoanSales.PropertyCapRate, 
                      dbo.DealLoanSales.DebtYield, dbo.Companies.TickerSymbol
FROM         dbo.Companies INNER JOIN
                      dbo.Deals ON dbo.Companies.CompanyID = dbo.Deals.ClientCompanyID INNER JOIN
                      dbo.DealLoanSales ON dbo.Deals.DealID = dbo.DealLoanSales.DealID INNER JOIN
                      dbo.DealTimelines ON dbo.Deals.DealID = dbo.DealTimelines.DealID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID INNER JOIN
                      dbo.PropertyTypes ON dbo.Deals.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID INNER JOIN
                      dbo.Documents ON dbo.Deals.DealID = dbo.Documents.DealID INNER JOIN
                      dbo.DocumentTypes ON dbo.Documents.DocumentTypeID = dbo.DocumentTypes.DocumentTypeID INNER JOIN
                      dbo.DocumentFor ON dbo.Documents.DocumentForID = dbo.DocumentFor.DocumentForID INNER JOIN
                      dbo.DealStatuses ON dbo.Deals.DealStatusID = dbo.DealStatuses.DealStatusID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DocumentLoanSaleLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'       DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DocumentTypes"
            Begin Extent = 
               Top = 210
               Left = 714
               Bottom = 303
               Right = 877
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DocumentFor"
            Begin Extent = 
               Top = 222
               Left = 254
               Bottom = 326
               Right = 418
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealStatuses"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3315
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DocumentLoanSaleLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[7] 2[34] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 293
               Bottom = 114
               Right = 490
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealLoanSales"
            Begin Extent = 
               Top = 6
               Left = 528
               Bottom = 200
               Right = 721
            End
            DisplayFlags = 280
            TopColumn = 23
         End
         Begin Table = "DealTimelines"
            Begin Extent = 
               Top = 6
               Left = 759
               Bottom = 114
               Right = 976
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 114
               Left = 254
               Bottom = 222
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Documents"
            Begin Extent = 
               Top = 216
               Left = 427
               Bottom = 324
               Right = 633
            End
     ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DocumentLoanSaleLong';


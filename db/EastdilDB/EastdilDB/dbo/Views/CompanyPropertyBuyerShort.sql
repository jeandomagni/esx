﻿CREATE VIEW dbo.CompanyPropertyBuyerShort
AS
SELECT     dbo.CompanyPropertyBuyers.CompanyPropertyBuyerID, dbo.CompanyPropertyBuyers.CompanyID, dbo.Properties.PropertyID, dbo.Properties.PropertyName, 
                      dbo.Properties.PropertyDescription, dbo.PropertyTypes.PropertyType, dbo.AssetClasses.AssetClass, dbo.Properties.Address1, dbo.Properties.City, 
                      dbo.Properties.State, dbo.CompanyPropertyBuyers.DateAcquired, dbo.CompanyPropertyBuyers.DateDisposed, dbo.Companies.CompanyName, 
                      dbo.Addresses.Address1 AS CompanyAddress1, dbo.Addresses.City AS CompanyCity, dbo.Addresses.State AS CompanyState, dbo.Companies.DBAName, 
                      dbo.Companies.TickerSymbol, dbo.Properties.PropertySize, dbo.ProjectSizeTypes.ProjectSizeType
FROM         dbo.Properties INNER JOIN
                      dbo.CompanyPropertyBuyers ON dbo.Properties.PropertyID = dbo.CompanyPropertyBuyers.PropertyID INNER JOIN
                      dbo.Companies ON dbo.CompanyPropertyBuyers.CompanyID = dbo.Companies.CompanyID INNER JOIN
                      dbo.ProjectSizeTypes ON dbo.Properties.ProjectSizeTypeID = dbo.ProjectSizeTypes.ProjectSizeTypeID LEFT OUTER JOIN
                      dbo.PropertyTypes ON dbo.Properties.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID LEFT OUTER JOIN
                      dbo.AssetClasses ON dbo.Properties.AssetClassID = dbo.AssetClasses.AssetClassID LEFT OUTER JOIN
                      dbo.Addresses ON dbo.Companies.AddressID = dbo.Addresses.AddressID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CompanyPropertyBuyerShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'     End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CompanyPropertyBuyerShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Properties"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 16
         End
         Begin Table = "CompanyPropertyBuyers"
            Begin Extent = 
               Top = 6
               Left = 265
               Bottom = 125
               Right = 480
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 518
               Bottom = 125
               Right = 744
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 225
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AssetClasses"
            Begin Extent = 
               Top = 126
               Left = 263
               Bottom = 230
               Right = 423
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Addresses"
            Begin Extent = 
               Top = 126
               Left = 461
               Bottom = 245
               Right = 650
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProjectSizeTypes"
            Begin Extent = 
               Top = 126
               Left = 688
               Bottom = 245
               Right = 865
       ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CompanyPropertyBuyerShort';


﻿

CREATE VIEW [dbo].[VwWorkingPartyRole]
AS


SELECT     
row_number() over(order by dbo.Employees.EMailAddress,dbo.WorkingPartyRoles.WorkingPartyRole,dbo.DealWPInternal.DealID) as RN,
dbo.Employees.EmpInitials as Emp_Initials, 
dbo.Employees.EmployeeName as Employee_Name, 
dbo.Employees.EMailAddress as EMail_Address, 
dbo.WorkingPartyRoles.WorkingPartyRole as WorkingParty_Role, 
dbo.DealWPInternal.DealID as Deal_ID,
dbo.WorkingPartyRoles.DisplayOrder
FROM         
dbo.DealWPInternal INNER JOIN dbo.Employees 
ON dbo.DealWPInternal.EmployeeID = dbo.Employees.EmployeeID 
INNER JOIN dbo.WorkingPartyRoles 
ON dbo.DealWPInternal.WorkingPartyRoleID = dbo.WorkingPartyRoles.WorkingPartyRoleID
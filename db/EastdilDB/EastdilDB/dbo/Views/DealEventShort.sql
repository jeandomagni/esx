﻿CREATE VIEW dbo.DealEventShort
AS
SELECT     dbo.DealEvents.DealEventID, dbo.DealEvents.DealID, dbo.DealEvents.EmployeeID, dbo.DealEvents.EventTypeID, dbo.DealEvents.LastUpdate, dbo.DealEvents.Title, 
                      dbo.DealEvents.Description, dbo.DealEvents.Start, dbo.DealEvents.[End], dbo.DealEvents.IsAllDay, dbo.DealEvents.RecurrenceRule, dbo.DealEvents.RecurrenceID, 
                      dbo.DealEvents.RecurrenceException, dbo.Deals.DealCode, dbo.Deals.DealName, dbo.Employees.EmpInitials, dbo.Employees.EmployeeName, 
                      dbo.EventTypes.EventType, dbo.TransactionTypes.TransactionType, dbo.Deals.PreCode, dbo.Deals.DealOffice, dbo.Deals.Confidential, dbo.Deals.ESTeam
FROM         dbo.DealEvents INNER JOIN
                      dbo.Deals ON dbo.DealEvents.DealID = dbo.Deals.DealID INNER JOIN
                      dbo.Employees ON dbo.DealEvents.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                      dbo.EventTypes ON dbo.DealEvents.EventTypeID = dbo.EventTypes.EventTypeID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealEventShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealEventShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DealEvents"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 319
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 290
               Right = 489
            End
            DisplayFlags = 280
            TopColumn = 30
         End
         Begin Table = "Employees"
            Begin Extent = 
               Top = 6
               Left = 527
               Bottom = 276
               Right = 740
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "EventTypes"
            Begin Extent = 
               Top = 6
               Left = 778
               Bottom = 275
               Right = 938
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 276
               Left = 527
               Bottom = 395
               Right = 714
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealEventShort';


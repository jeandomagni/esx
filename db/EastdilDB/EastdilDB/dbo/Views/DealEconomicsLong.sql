﻿CREATE VIEW dbo.DealEconomicsLong
AS
SELECT     dbo.Deals.DealID, dbo.Deals.DealCode, dbo.Deals.DealName, dbo.Companies.CompanyName, dbo.TransactionTypes.TransactionType, 
                      dbo.PropertyTypes.PropertyType, dbo.DealStatuses.DealStatus, dbo.Deals.Active, dbo.Deals.Confidential, dbo.Deals.DealOffice, dbo.Deals.DealLeader, 
                      dbo.Deals.DealManager, dbo.Deals.ESTeam, dbo.Deals.LocationAddress, dbo.Deals.LocationCity, dbo.Deals.LocationState, dbo.Deals.MktgListSize, 
                      dbo.DealTimelines.DateToMarket, dbo.DealTimelines.DateCloseAct, dbo.Deals.DateClosed, dbo.Deals.LastUpdate, dbo.DealEconomics.DealID AS Expr1, 
                      dbo.DealEconomics.EstSalePrice, dbo.DealEconomics.ProjectSize, dbo.DealEconomics.ProjectSizeTypeID, dbo.DealEconomics.PricePerUnitOfMeasure, 
                      dbo.DealEconomics.Occupancy, dbo.DealEconomics.InPlaceCapRatePct, dbo.DealEconomics.Year1CapRatePct, dbo.DealEconomics.TenYearIRRPct, 
                      dbo.DealEconomics.IRRPct, dbo.DealEconomics.IRRTypeID, dbo.DealEconomics.ExitCapRatePct, dbo.DealEconomics.BuyerName, dbo.DealEconomics.JVSale, 
                      dbo.DealEconomics.TopBidders, dbo.DealEconomics.OffShoreBidders, dbo.DealEconomics.WasDebtAssumed, dbo.DealEconomics.DebtTerms, 
                      dbo.DealEconomics.AvgSalesPerSqFt, dbo.DealEconomics.AvgOccCost, dbo.DealEconomics.FiveYearCashOnCashPct, dbo.DealEconomics.LoanTypeID, 
                      dbo.DealEconomics.NumberOfLoans, dbo.DealEconomics.TotalDebtStack, dbo.DealEconomics.LoanPerfTypeID, dbo.DealEconomics.LoanRateTypeID, 
                      dbo.DealEconomics.OriginationDate, dbo.DealEconomics.MaturityDate, dbo.DealEconomics.YTMPct, dbo.DealEconomics.NumberOfOffers, 
                      dbo.DealEconomics.PurchasePrice, dbo.Companies.CompanyName AS BuyerCompany
FROM         dbo.Companies INNER JOIN
                      dbo.Deals ON dbo.Companies.CompanyID = dbo.Deals.ClientCompanyID INNER JOIN
                      dbo.DealTimelines ON dbo.Deals.DealID = dbo.DealTimelines.DealID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID INNER JOIN
                      dbo.DealStatuses ON dbo.Deals.DealStatusID = dbo.DealStatuses.DealStatusID INNER JOIN
                      dbo.PropertyTypes ON dbo.Deals.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID INNER JOIN
                      dbo.DealEconomics ON dbo.Deals.DealID = dbo.DealEconomics.DealID INNER JOIN
                      dbo.Companies AS BuyerCompanies ON dbo.DealEconomics.BuyerCompanyID = BuyerCompanies.CompanyID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealEconomicsLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'
            DisplayFlags = 280
            TopColumn = 87
         End
         Begin Table = "BuyerCompanies"
            Begin Extent = 
               Top = 148
               Left = 1381
               Bottom = 256
               Right = 1598
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 2865
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealEconomicsLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 302
               Bottom = 125
               Right = 555
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealTimelines"
            Begin Extent = 
               Top = 188
               Left = 34
               Bottom = 307
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 189
               Left = 305
               Bottom = 308
               Right = 492
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "DealStatuses"
            Begin Extent = 
               Top = 204
               Left = 531
               Bottom = 323
               Right = 718
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 171
               Left = 832
               Bottom = 290
               Right = 1019
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealEconomics"
            Begin Extent = 
               Top = 4
               Left = 682
               Bottom = 348
               Right = 935
            End', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealEconomicsLong';


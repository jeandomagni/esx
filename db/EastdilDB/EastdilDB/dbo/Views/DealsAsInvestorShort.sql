﻿CREATE VIEW dbo.DealsAsInvestorShort
AS
SELECT     dbo.DealContacts.DealContactID, dbo.Deals.DealID, dbo.DealContacts.ContactID, dbo.Deals.DealCode, dbo.Deals.DealName, dbo.DealStatuses.DealStatus, 
                      dbo.TransactionTypes.TransactionType, dbo.Deals.ESTeam, dbo.Deals.DealOffice, dbo.DealTimelines.DateAdded, dbo.DealTimelines.DateToMarket, 
                      dbo.DealTimelines.DateCloseAct, dbo.Companies.CompanyName, dbo.PropertyTypes.PropertyType, dbo.Deals.Confidential, dbo.MarketingStatuses.MarketingStatus, 
                      dbo.DealContacts.EmployeeID, dbo.Deals.Active, dbo.DealContacts.MarketingStatusID, dbo.Companies.DBAName, dbo.Companies.TickerSymbol, 
                      dbo.DealContacts.BidSubmitted, dbo.DealContacts.BidRank, dbo.DealContacts.BidOffer, dbo.DealContacts.BidOfferAmt, dbo.DealContacts.Won
FROM         dbo.Deals INNER JOIN
                      dbo.DealStatuses ON dbo.Deals.DealStatusID = dbo.DealStatuses.DealStatusID INNER JOIN
                      dbo.DealTimelines ON dbo.Deals.DealID = dbo.DealTimelines.DealID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID INNER JOIN
                      dbo.DealContacts ON dbo.Deals.DealID = dbo.DealContacts.DealID INNER JOIN
                      dbo.Companies ON dbo.Deals.ClientCompanyID = dbo.Companies.CompanyID INNER JOIN
                      dbo.PropertyTypes ON dbo.Deals.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID INNER JOIN
                      dbo.MarketingStatuses ON dbo.DealContacts.MarketingStatusID = dbo.MarketingStatuses.MarketingStatusID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealsAsInvestorShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'         DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MarketingStatuses"
            Begin Extent = 
               Top = 135
               Left = 783
               Bottom = 243
               Right = 961
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealsAsInvestorShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 200
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "DealStatuses"
            Begin Extent = 
               Top = 6
               Left = 273
               Bottom = 114
               Right = 451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealTimelines"
            Begin Extent = 
               Top = 6
               Left = 489
               Bottom = 114
               Right = 706
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 6
               Left = 744
               Bottom = 114
               Right = 922
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealContacts"
            Begin Extent = 
               Top = 6
               Left = 960
               Bottom = 198
               Right = 1143
            End
            DisplayFlags = 280
            TopColumn = 36
         End
         Begin Table = "Companies"
            Begin Extent = 
               Top = 188
               Left = 370
               Bottom = 366
               Right = 587
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 238
               Left = 84
               Bottom = 346
               Right = 262
            End
   ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealsAsInvestorShort';


﻿CREATE VIEW dbo.CompanyLoanBorrowerShort
AS
SELECT     dbo.LoanBorrowers.LoanBorrowerID, dbo.LoanBorrowers.BorrowerCompanyID, dbo.Loans.LoanID, dbo.Loans.LoanName, dbo.Loans.LoanDescription, 
                      dbo.LoanTypes.LoanType, dbo.Loans.OriginationDate, dbo.Loans.OriginalMaturityDate, dbo.Companies.CompanyName AS BorrowerCompanyName, 
                      dbo.Addresses.Address1 AS CompanyAddress1, dbo.Addresses.City AS CompanyCity, dbo.Addresses.State AS CompanyState, dbo.Companies.DBAName, 
                      dbo.Companies.TickerSymbol
FROM         dbo.Loans INNER JOIN
                      dbo.LoanBorrowers ON dbo.Loans.LoanID = dbo.LoanBorrowers.LoanID INNER JOIN
                      dbo.Companies ON dbo.LoanBorrowers.BorrowerCompanyID = dbo.Companies.CompanyID LEFT OUTER JOIN
                      dbo.LoanTypes ON dbo.Loans.LoanTypeID = dbo.LoanTypes.LoanTypeID LEFT OUTER JOIN
                      dbo.Addresses ON dbo.Companies.AddressID = dbo.Addresses.AddressID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CompanyLoanBorrowerShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'       Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CompanyLoanBorrowerShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Loans"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 273
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoanBorrowers"
            Begin Extent = 
               Top = 163
               Left = 285
               Bottom = 282
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Companies"
            Begin Extent = 
               Top = 153
               Left = 638
               Bottom = 272
               Right = 864
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "LoanTypes"
            Begin Extent = 
               Top = 181
               Left = 27
               Bottom = 285
               Right = 187
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Addresses"
            Begin Extent = 
               Top = 107
               Left = 1025
               Bottom = 226
               Right = 1214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3615
         Alias = 3195
         Table = 2475
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
  ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CompanyLoanBorrowerShort';


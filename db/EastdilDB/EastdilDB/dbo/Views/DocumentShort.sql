﻿CREATE VIEW dbo.DocumentShort
AS
SELECT     dbo.Documents.DocumentID, dbo.Documents.CompanyID, dbo.Documents.ContactID, dbo.Documents.DealID, dbo.Deals.TransactionTypeID, 
                      dbo.Documents.PropertyID, dbo.Documents.LoanID, dbo.Documents.DealContactID, dbo.Documents.StorageLocation, dbo.Documents.FileName, 
                      dbo.Documents.BestPracticeDoc, dbo.TransactionTypes.TransactionType, dbo.DocumentTypes.DocumentType, EmpAuthor.EmployeeName AS Author, 
                      dbo.Documents.DateCreated, dbo.Documents.DateAdded, Companies_1.CompanyName, dbo.Contacts.FirstnameLastname, dbo.Deals.DealCode, 
                      dbo.Deals.DealName, dbo.Properties.PropertyName,
                          (SELECT     dbo.Companies.CompanyName
                            FROM          dbo.Companies INNER JOIN
                                                   dbo.Loans ON dbo.Companies.CompanyID = dbo.Loans.LenderCompanyID
                            WHERE      (dbo.Documents.LoanID = dbo.Loans.LoanID)) AS Lender, Loans_1.Borrowers, dbo.DocumentFor.DocumentFor, dbo.DocumentTypes.DocumentTypeID, 
                      dbo.Deals.Confidential, dbo.Deals.ESTeam, dbo.Documents.PostedByEmployeeID, dbo.Documents.PrimaryAuthorEmployeeID, Companies_1.DBAName, 
                      Companies_1.TickerSymbol, Companies_1.PrimaryCompanyRecord, dbo.DocumentTypes.DisplayOrder, dbo.DocumentTypes.PitchBOV, 
                      dbo.DocumentTypes.Marketing, dbo.DocumentTypes.[Post-Closing], dbo.DocumentTypes.Financials
FROM         dbo.TransactionTypes INNER JOIN
                      dbo.Deals ON dbo.TransactionTypes.TransactionTypeID = dbo.Deals.TransactionTypeID RIGHT OUTER JOIN
                      dbo.Documents INNER JOIN
                      dbo.DocumentTypes ON dbo.Documents.DocumentTypeID = dbo.DocumentTypes.DocumentTypeID INNER JOIN
                      dbo.DocumentFor ON dbo.Documents.DocumentForID = dbo.DocumentFor.DocumentForID LEFT OUTER JOIN
                      dbo.Companies AS Companies_1 ON dbo.Documents.CompanyID = Companies_1.CompanyID LEFT OUTER JOIN
                      dbo.Contacts ON dbo.Documents.ContactID = dbo.Contacts.ContactID ON dbo.Deals.DealID = dbo.Documents.DealID LEFT OUTER JOIN
                      dbo.Properties ON dbo.Documents.PropertyID = dbo.Properties.PropertyID LEFT OUTER JOIN
                      dbo.Loans AS Loans_1 ON dbo.Documents.LoanID = Loans_1.LoanID LEFT OUTER JOIN
                      dbo.DealContacts ON dbo.Documents.DealContactID = dbo.DealContacts.DealContactID LEFT OUTER JOIN
                      dbo.Employees AS EmpAuthor ON dbo.Documents.PrimaryAuthorEmployeeID = EmpAuthor.EmployeeID LEFT OUTER JOIN
                      dbo.Companies AS LenderCompanies ON Companies_1.CompanyID = Loans_1.LenderCompanyID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DocumentShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'       DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Properties"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Loans_1"
            Begin Extent = 
               Top = 114
               Left = 517
               Bottom = 222
               Right = 743
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealContacts"
            Begin Extent = 
               Top = 114
               Left = 781
               Bottom = 222
               Right = 964
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "EmpAuthor"
            Begin Extent = 
               Top = 210
               Left = 254
               Bottom = 318
               Right = 458
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LenderCompanies"
            Begin Extent = 
               Top = 222
               Left = 496
               Bottom = 330
               Right = 713
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2415
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DocumentShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 114
               Left = 1002
               Bottom = 222
               Right = 1180
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 102
               Left = 282
               Bottom = 210
               Right = 479
            End
            DisplayFlags = 280
            TopColumn = 35
         End
         Begin Table = "Documents"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "DocumentTypes"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 175
               Right = 445
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DocumentFor"
            Begin Extent = 
               Top = 318
               Left = 38
               Bottom = 484
               Right = 202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Companies_1"
            Begin Extent = 
               Top = 6
               Left = 672
               Bottom = 114
               Right = 889
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "Contacts"
            Begin Extent = 
               Top = 6
               Left = 927
               Bottom = 114
               Right = 1156
            End
     ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DocumentShort';


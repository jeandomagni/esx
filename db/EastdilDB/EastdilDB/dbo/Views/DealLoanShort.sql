﻿CREATE VIEW dbo.DealLoanShort
AS
SELECT     dbo.DealLoans.DealLoanID, dbo.Loans.LoanID, dbo.Loans.LoanName, dbo.Loans.LoanDescription, dbo.Loans.OriginationDate, 
                      LenderCompanies.CompanyName AS LenderCompanyName, dbo.Loans.Borrowers, dbo.LoanTypes.LoanType, dbo.Loans.LoanBalance, dbo.DealLoans.DealID, 
                      dbo.Deals.DealCode, dbo.Deals.DealName, dbo.DealStatuses.DealStatus, dbo.TransactionTypes.TransactionType, dbo.Deals.DealOffice, dbo.Deals.ESTeam, 
                      dbo.Deals.DateAdded, dbo.DealTimelines.DateToMarket, dbo.DealTimelines.DateCloseAct
FROM         dbo.Companies AS LenderCompanies RIGHT OUTER JOIN
                      dbo.Loans INNER JOIN
                      dbo.Deals INNER JOIN
                      dbo.DealLoans ON dbo.Deals.DealID = dbo.DealLoans.DealID INNER JOIN
                      dbo.DealStatuses ON dbo.Deals.DealStatusID = dbo.DealStatuses.DealStatusID INNER JOIN
                      dbo.DealTimelines ON dbo.Deals.DealID = dbo.DealTimelines.DealID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID ON dbo.Loans.LoanID = dbo.DealLoans.LoanID LEFT OUTER JOIN
                      dbo.Companies AS SpecialServicerCompanies ON dbo.Loans.SpecialServicerCompanyID = SpecialServicerCompanies.CompanyID ON 
                      LenderCompanies.CompanyID = dbo.Loans.LenderCompanyID LEFT OUTER JOIN
                      dbo.LoanTypes ON dbo.Loans.LoanTypeID = dbo.LoanTypes.LoanTypeID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealLoanShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SpecialServicerCompanies"
            Begin Extent = 
               Top = 5
               Left = 734
               Bottom = 113
               Right = 951
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoanTypes"
            Begin Extent = 
               Top = 288
               Left = 93
               Bottom = 381
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1800
         Table = 2625
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealLoanShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[54] 4[22] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "LenderCompanies"
            Begin Extent = 
               Top = 99
               Left = 544
               Bottom = 207
               Right = 761
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Loans"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 180
               Left = 811
               Bottom = 288
               Right = 1008
            End
            DisplayFlags = 280
            TopColumn = 65
         End
         Begin Table = "DealLoans"
            Begin Extent = 
               Top = 157
               Left = 324
               Bottom = 265
               Right = 475
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealStatuses"
            Begin Extent = 
               Top = 279
               Left = 338
               Bottom = 387
               Right = 516
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealTimelines"
            Begin Extent = 
               Top = 306
               Left = 1006
               Bottom = 414
               Right = 1223
            End
            DisplayFlags = 280
            TopColumn = 17
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 269
               Left = 604
               Bottom = 377
               Right = 782
            End', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealLoanShort';


﻿CREATE VIEW dbo.CommentShort
AS
SELECT        dbo.Comments.CommentID, dbo.Comments.CompanyID, dbo.Comments.ContactID, dbo.Comments.DealID, dbo.Comments.DealContactID, dbo.Comments.CommentDate, dbo.Employees.EmployeeName, 
                         ConBy.FirstnameLastname, dbo.CommentTypes.CommentType, dbo.CommentVisibility.CommentVisibility, dbo.Deals.DealCode, dbo.Deals.DealName, dbo.Companies.CompanyName, 
                         ConFor.FirstnameLastname AS ContactName, dbo.Comments.CommentText, dbo.Comments.PropertyID, dbo.Comments.LoanID, dbo.Comments.DocumentID, dbo.Comments.EmployeeID, 
                         dbo.Comments.CommentByContactID, dbo.Comments.UpdateFlag, dbo.Comments.DeleteFlag, dbo.Comments.CommentTypeID, dbo.Deals.ESTeam, dbo.Companies.DBAName, dbo.Companies.TickerSymbol, 
                         dbo.Comments.CommentIDForSubComment, dbo.Comments.SubCommentTypeID, dbo.SubCommentTypes.SubCommentType, dbo.Comments.FollowupDate, dbo.Comments.FollowupTask, 
                         dbo.Comments.FollowupEmployeeID, dbo.Comments.FollowupDone, dbo.Comments.NumSubComments, dbo.Comments.HasSubComments
FROM            dbo.Employees RIGHT OUTER JOIN
                         dbo.Companies RIGHT OUTER JOIN
                         dbo.SubCommentTypes RIGHT OUTER JOIN
                         dbo.Comments INNER JOIN
                         dbo.CommentTypes ON dbo.Comments.CommentTypeID = dbo.CommentTypes.CommentTypeID INNER JOIN
                         dbo.CommentVisibility ON dbo.Comments.CommentVisibilityID = dbo.CommentVisibility.CommentVisibilityID ON dbo.SubCommentTypes.SubCommentTypeID = dbo.Comments.SubCommentTypeID ON 
                         dbo.Companies.CompanyID = dbo.Comments.CompanyID LEFT OUTER JOIN
                         dbo.Deals ON dbo.Comments.DealID = dbo.Deals.DealID ON dbo.Employees.EmployeeID = dbo.Comments.EmployeeID LEFT OUTER JOIN
                         dbo.Contacts AS ConBy ON dbo.Comments.CommentByContactID = ConBy.ContactID LEFT OUTER JOIN
                         dbo.Contacts AS ConFor ON dbo.Comments.ContactID = ConFor.ContactID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CommentShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'         DisplayFlags = 280
            TopColumn = 36
         End
         Begin Table = "ConBy"
            Begin Extent = 
               Top = 169
               Left = 790
               Bottom = 277
               Right = 1019
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ConFor"
            Begin Extent = 
               Top = 6
               Left = 739
               Bottom = 114
               Right = 968
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CommentShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[37] 4[14] 2[42] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Employees"
            Begin Extent = 
               Top = 114
               Left = 305
               Bottom = 222
               Right = 509
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Companies"
            Begin Extent = 
               Top = 301
               Left = 395
               Bottom = 409
               Right = 612
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "SubCommentTypes"
            Begin Extent = 
               Top = 418
               Left = 317
               Bottom = 531
               Right = 517
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Comments"
            Begin Extent = 
               Top = 14
               Left = 13
               Bottom = 508
               Right = 282
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "CommentTypes"
            Begin Extent = 
               Top = 6
               Left = 307
               Bottom = 114
               Right = 485
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CommentVisibility"
            Begin Extent = 
               Top = 6
               Left = 523
               Bottom = 114
               Right = 701
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 114
               Left = 547
               Bottom = 309
               Right = 744
            End
   ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CommentShort';


﻿CREATE VIEW dbo.PrintRequestShort
AS
SELECT     dbo.PrintRequests.PrintRequestID, dbo.PrintRequests.DealID, dbo.PrintRequests.RequesterEmployeeID, dbo.PrintRequests.ESGSLeadEmployeeID, 
                      dbo.Deals.DealCode, dbo.Deals.PreCode, dbo.Deals.DealName, dbo.Deals.DealOffice, dbo.TransactionTypes.TransactionType, dbo.PrintRequests.DateRequested, 
                      dbo.PrintRequests.DateNeeded, dbo.PrintRequests.NumberPages, dbo.PrintRequests.CopiesRequested, dbo.PrintRequests.BindingTypeAndVendor, 
                      dbo.PrintRequests.StatusComment, dbo.Employees.EmpInitials AS RequesterEmpInitials, dbo.Employees.EmployeeName AS RequesterEmployeeName, 
                      dbo.Employees.EMailAddress AS RequestorEmailAddress, Employees_1.EmpInitials AS ESGSLeadEmpInitials, 
                      Employees_1.EmployeeName AS ESGSLeadEmployeeName, Employees_1.EmployeeID AS ESGSEmployeeID, dbo.Employees.EmployeeID AS ReqEmployeeID, 
                      dbo.Deals.Active, dbo.Deals.Confidential, dbo.PrintRequests.SourceFileTypeID, dbo.PrintRequests.RequestStatusID, dbo.SourceFileTypes.SourceFileType, 
                      dbo.RequestStatuses.RequestStatus, dbo.PrintRequests.LastUpdate, dbo.Deals.ESTeam
FROM         dbo.Deals INNER JOIN
                      dbo.PrintRequests ON dbo.Deals.DealID = dbo.PrintRequests.DealID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID LEFT OUTER JOIN
                      dbo.RequestStatuses ON dbo.PrintRequests.RequestStatusID = dbo.RequestStatuses.RequestStatusID LEFT OUTER JOIN
                      dbo.SourceFileTypes ON dbo.PrintRequests.SourceFileTypeID = dbo.SourceFileTypes.SourceFileTypeID LEFT OUTER JOIN
                      dbo.Employees AS Employees_1 ON dbo.PrintRequests.ESGSLeadEmployeeID = Employees_1.EmployeeID LEFT OUTER JOIN
                      dbo.Employees ON dbo.PrintRequests.RequesterEmployeeID = dbo.Employees.EmployeeID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PrintRequestShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'      End
            DisplayFlags = 280
            TopColumn = 48
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2160
         Alias = 2415
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PrintRequestShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[21] 4[42] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -288
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Deals"
            Begin Extent = 
               Top = 20
               Left = 861
               Bottom = 304
               Right = 1114
            End
            DisplayFlags = 280
            TopColumn = 34
         End
         Begin Table = "PrintRequests"
            Begin Extent = 
               Top = 2
               Left = 282
               Bottom = 121
               Right = 482
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 177
               Left = 793
               Bottom = 296
               Right = 980
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RequestStatuses"
            Begin Extent = 
               Top = 347
               Left = 38
               Bottom = 451
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SourceFileTypes"
            Begin Extent = 
               Top = 347
               Left = 247
               Bottom = 451
               Right = 420
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employees_1"
            Begin Extent = 
               Top = 120
               Left = 44
               Bottom = 239
               Right = 257
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employees"
            Begin Extent = 
               Top = 221
               Left = 624
               Bottom = 405
               Right = 837
      ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PrintRequestShort';


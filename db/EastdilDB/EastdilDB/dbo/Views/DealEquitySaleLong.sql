﻿CREATE VIEW dbo.DealEquitySaleLong
AS
SELECT     dbo.Deals.DealID, dbo.Deals.DealCode, dbo.Deals.DealName, dbo.Companies.CompanyName, dbo.Deals.ClientCompanyID, dbo.TransactionTypes.TransactionType, 
                      dbo.Deals.TransactionTypeID, dbo.PropertyTypes.PropertyType, dbo.Deals.PropertyTypeID, dbo.DealStatuses.DealStatus, dbo.Deals.Active, dbo.Deals.Confidential, 
                      dbo.Deals.DealOffice, dbo.Deals.DealLeader, dbo.Deals.DealManager, dbo.Deals.ESTeam, dbo.Deals.LocationAddress, dbo.Deals.LocationCity, 
                      dbo.Deals.LocationState, dbo.Deals.MktgListSize, dbo.DealTimelines.DateToMarket, dbo.DealTimelines.DateCloseAct, dbo.DealEquitySales.Valuation, 
                      dbo.DealEquitySales.ProjectSize, dbo.ProjectSizeTypes.ProjectSizeType, dbo.DealEquitySales.ProjectSizeTypeID, dbo.DealEquitySales.ValuationPerUnitOfMeasure, 
                      dbo.DealEquitySales.CapRate1, dbo.DealEquitySales.CapRate1Description, dbo.DealEquitySales.CapRate2, dbo.DealEquitySales.CapRate2Description, 
                      dbo.DealEquitySales.ExitCapRate, dbo.DealEquitySales.UnleveredIRR, dbo.DealEquitySales.UnleveredIRRDescription, dbo.DealEquitySales.LeveredIRR, 
                      dbo.DealEquitySales.LeveredIRRDescription, dbo.DealEquitySales.CashOnCash, dbo.DealEquitySales.CashOnCashDescription, dbo.DealEquitySales.PctLeased, 
                      dbo.DealEquitySales.WasDebtAssumed, dbo.DealEquitySales.WasNewDebtPlaced, dbo.DealEquitySales.Lender, 
                      LenderCompanies.CompanyName AS LenderCompany, dbo.DealEquitySales.LenderCompanyID, dbo.LenderTypes.LenderType, dbo.DealEquitySales.LenderTypeID, 
                      dbo.DealEquitySales.TotalLoanAmt, dbo.DealEquitySales.TotalLoanAmtBySqFt, dbo.LoanRateTypes.LoanRateType, dbo.DealEquitySales.LoanRateTypeID, 
                      dbo.DealEquitySales.MaturityDate, dbo.DealEquitySales.InterestRate, dbo.DealEquitySales.Amortization, dbo.DealEquitySales.LTV, dbo.DealEquitySales.DebtYield, 
                      dbo.DealEquitySales.EstimatedAnnualDebtService, dbo.DealEquitySales.DSCR, dbo.DealEquitySales.PrepaymentString, dbo.DealEquitySales.BuyerName, 
                      BuyerCompanies.CompanyName AS BuyerCompany, dbo.DealEquitySales.BuyerCompanyID, dbo.BuyerTypes.BuyerType, dbo.DealEquitySales.BuyerTypeID, 
                      dbo.DealEquitySales.JVSale, dbo.DealEquitySales.PercentInterestSold, dbo.DealEquitySales.TopBidders, dbo.DealEquitySales.OffShoreBidders, 
                      dbo.DealEquitySales.SellerName, SellerCompanies.CompanyName AS SellerCompany, dbo.DealEquitySales.SellerCompanyID, dbo.SellerTypes.SellerType, 
                      dbo.DealEquitySales.SellerTypeID, dbo.DealEquitySales.Occupancy, dbo.Deals.DateClosed, dbo.Deals.LastUpdate
FROM         dbo.Companies INNER JOIN
                      dbo.Deals ON dbo.Companies.CompanyID = dbo.Deals.ClientCompanyID INNER JOIN
                      dbo.DealEquitySales ON dbo.Deals.DealID = dbo.DealEquitySales.DealID INNER JOIN
                      dbo.DealTimelines ON dbo.Deals.DealID = dbo.DealTimelines.DealID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID INNER JOIN
                      dbo.DealStatuses ON dbo.Deals.DealStatusID = dbo.DealStatuses.DealStatusID INNER JOIN
                      dbo.PropertyTypes ON dbo.Deals.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID INNER JOIN
                      dbo.BuyerTypes ON dbo.DealEquitySales.BuyerTypeID = dbo.BuyerTypes.BuyerTypeID INNER JOIN
                      dbo.LenderTypes ON dbo.DealEquitySales.LenderTypeID = dbo.LenderTypes.LenderTypeID INNER JOIN
                      dbo.SellerTypes ON dbo.DealEquitySales.SellerTypeID = dbo.SellerTypes.SellerTypeID LEFT OUTER JOIN
                      dbo.LoanRateTypes ON dbo.DealEquitySales.LoanRateTypeID = dbo.LoanRateTypes.LoanRateTypeID LEFT OUTER JOIN
                      dbo.ProjectSizeTypes ON dbo.DealEquitySales.ProjectSizeTypeID = dbo.ProjectSizeTypes.ProjectSizeTypeID LEFT OUTER JOIN
                      dbo.Companies AS LenderCompanies ON dbo.DealEquitySales.LenderCompanyID = LenderCompanies.CompanyID LEFT OUTER JOIN
                      dbo.Companies AS BuyerCompanies ON dbo.DealEquitySales.LenderCompanyID = BuyerCompanies.CompanyID LEFT OUTER JOIN
                      dbo.Companies AS SellerCompanies ON dbo.DealEquitySales.SellerCompanyID = SellerCompanies.CompanyID
WHERE     (dbo.Deals.TransactionTypeID = 11)
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane3', @value = N'idth = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2805
         Alias = 2235
         Table = 2700
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealEquitySaleLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[43] 4[30] 2[12] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 293
               Bottom = 170
               Right = 490
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "DealEquitySales"
            Begin Extent = 
               Top = 6
               Left = 528
               Bottom = 114
               Right = 746
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "DealTimelines"
            Begin Extent = 
               Top = 6
               Left = 784
               Bottom = 114
               Right = 1001
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 6
               Left = 1039
               Bottom = 114
               Right = 1217
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealStatuses"
            Begin Extent = 
               Top = 168
               Left = 21
               Bottom = 276
               Right = 199
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 283
               Left = 219
               Bottom = 391
               Right = 397
            En', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealEquitySaleLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 3, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealEquitySaleLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'd
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BuyerTypes"
            Begin Extent = 
               Top = 370
               Left = 1072
               Bottom = 448
               Right = 1223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LenderTypes"
            Begin Extent = 
               Top = 381
               Left = 696
               Bottom = 459
               Right = 847
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SellerTypes"
            Begin Extent = 
               Top = 144
               Left = 1167
               Bottom = 262
               Right = 1318
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProjectSizeTypes"
            Begin Extent = 
               Top = 210
               Left = 464
               Bottom = 329
               Right = 641
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LenderCompanies"
            Begin Extent = 
               Top = 371
               Left = 418
               Bottom = 490
               Right = 644
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BuyerCompanies"
            Begin Extent = 
               Top = 239
               Left = 802
               Bottom = 407
               Right = 1028
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SellerCompanies"
            Begin Extent = 
               Top = 161
               Left = 915
               Bottom = 280
               Right = 1141
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "LoanRateTypes"
            Begin Extent = 
               Top = 163
               Left = 631
               Bottom = 267
               Right = 801
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 76
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         W', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealEquitySaleLong';


﻿CREATE VIEW dbo.DealLoanSaleLong
AS
SELECT     dbo.Deals.DealID, dbo.Deals.DealCode, dbo.Deals.DealName, dbo.Companies.CompanyName, dbo.TransactionTypes.TransactionType, 
                      dbo.Deals.TransactionTypeID, dbo.PropertyTypes.PropertyType, dbo.DealStatuses.DealStatus, dbo.Deals.Active, dbo.Deals.Confidential, dbo.Deals.DealOffice, 
                      dbo.Deals.DealLeader, dbo.Deals.DealManager, dbo.Deals.ESTeam, dbo.Deals.LocationAddress, dbo.Deals.LocationCity, dbo.Deals.LocationState, 
                      dbo.Deals.MktgListSize, dbo.DealTimelines.DateToMarket, dbo.DealTimelines.DateCloseAct, dbo.DealLoanSales.Seller, 
                      SellerCompanies.CompanyName AS SellerCompany, dbo.DealLoanSales.SellerCompanyID, dbo.SellerTypes.SellerType, dbo.DealLoanSales.SellerTypeID, 
                      dbo.DealLoanSales.Buyers, BuyerCompanies.CompanyName AS BuyerCompany, dbo.BuyerTypes.BuyerType, dbo.DealLoanSales.BuyerTypeID, 
                      dbo.DealLoanSales.BuyerCompanyID, dbo.DealLoanSales.NumberOfLoans, dbo.DealLoanSales.Occupancy, dbo.DealLoanSales.PctLeased, 
                      dbo.LoanTypes.LoanType, dbo.DealLoanSales.LoanTypeID, dbo.DealLoanSales.UnpaidLoanBalance, dbo.DealLoanSales.UnpaidLoanBalancePerSF, 
                      dbo.DealLoanSales.PurchasePrice, dbo.DealLoanSales.PurchasePricePerSF, dbo.DealLoanSales.PurchasePricePctOfUPB, dbo.LoanRateTypes.LoanRateType, 
                      dbo.DealLoanSales.LoanRateTypeID, dbo.DealLoanSales.PropertyNOI, dbo.DealLoanSales.PropertySite, dbo.DealLoanSales.PropertyValue, 
                      dbo.DealLoanSales.PropertyValuePerSF, dbo.DealLoanSales.PropertyCapRate, dbo.DealLoanSales.EquityLike, dbo.DealLoanSales.OriginationDate, 
                      dbo.DealLoanSales.MaturityDate, dbo.DealLoanSales.Coupon, dbo.DealLoanSales.CashOnCash, dbo.DealLoanSales.MostRecentDSCR, 
                      dbo.DealLoanSales.InPlaceNOI, dbo.DealLoanSales.EstimatedLTV, dbo.DealLoanSales.Amortization, dbo.DealLoanSales.YieldToMaturity, 
                      dbo.DealLoanSales.NumberOfOffers, dbo.DealLoanSales.DebtYield
FROM         dbo.Companies INNER JOIN
                      dbo.Deals ON dbo.Companies.CompanyID = dbo.Deals.ClientCompanyID INNER JOIN
                      dbo.DealTimelines ON dbo.Deals.DealID = dbo.DealTimelines.DealID INNER JOIN
                      dbo.TransactionTypes ON dbo.Deals.TransactionTypeID = dbo.TransactionTypes.TransactionTypeID INNER JOIN
                      dbo.DealStatuses ON dbo.Deals.DealStatusID = dbo.DealStatuses.DealStatusID INNER JOIN
                      dbo.PropertyTypes ON dbo.Deals.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID INNER JOIN
                      dbo.DealLoanSales ON dbo.Deals.DealID = dbo.DealLoanSales.DealID INNER JOIN
                      dbo.SellerTypes ON dbo.DealLoanSales.SellerTypeID = dbo.SellerTypes.SellerTypeID INNER JOIN
                      dbo.BuyerTypes ON dbo.DealLoanSales.BuyerTypeID = dbo.BuyerTypes.BuyerTypeID INNER JOIN
                      dbo.Companies AS SellerCompanies ON dbo.DealLoanSales.SellerCompanyID = SellerCompanies.CompanyID INNER JOIN
                      dbo.Companies AS BuyerCompanies ON dbo.DealLoanSales.BuyerCompanyID = BuyerCompanies.CompanyID INNER JOIN
                      dbo.LoanTypes ON dbo.DealLoanSales.LoanTypeID = dbo.LoanTypes.LoanTypeID INNER JOIN
                      dbo.LoanRateTypes ON dbo.DealLoanSales.LoanRateTypeID = dbo.LoanRateTypes.LoanRateTypeID
WHERE     (dbo.Deals.TransactionTypeID = 19)
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'          DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "SellerTypes"
            Begin Extent = 
               Top = 215
               Left = 877
               Bottom = 375
               Right = 1028
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BuyerTypes"
            Begin Extent = 
               Top = 122
               Left = 895
               Bottom = 200
               Right = 1046
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SellerCompanies"
            Begin Extent = 
               Top = 282
               Left = 38
               Bottom = 401
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BuyerCompanies"
            Begin Extent = 
               Top = 306
               Left = 302
               Bottom = 425
               Right = 528
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoanTypes"
            Begin Extent = 
               Top = 378
               Left = 844
               Bottom = 482
               Right = 1004
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoanRateTypes"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 506
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3540
         Alias = 1770
         Table = 2835
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealLoanSaleLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealLoanSaleLong';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[46] 4[16] 2[29] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 6
               Left = 293
               Bottom = 200
               Right = 490
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealTimelines"
            Begin Extent = 
               Top = 2
               Left = 559
               Bottom = 110
               Right = 776
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 6
               Left = 783
               Bottom = 114
               Right = 961
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealStatuses"
            Begin Extent = 
               Top = 174
               Left = 66
               Bottom = 282
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 195
               Left = 345
               Bottom = 303
               Right = 523
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealLoanSales"
            Begin Extent = 
               Top = 194
               Left = 613
               Bottom = 488
               Right = 806
            End
  ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealLoanSaleLong';


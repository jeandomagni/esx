﻿CREATE VIEW dbo.DealContactBidShort
AS
SELECT     dcb.DealContactBidID, dcb.DealContactID, d.DealID, con.FirstnameLastname, con.LastName, c.CompanyName, ms.MarketingStatus, d.DealCode, d.DealName, 
                      d.LocationCity, d.LocationState, dbo.PropertyTypes.PropertyType, con.LastnameFirstname, con.Active, dcb.BidSubmitted, dcb.BidRound, dcb.BidRank, dcb.BidOffer, 
                      dcb.BidOfferAmt, dcb.BidDepositNote, dcb.BidDDPeriodNote, dcb.BidDebtFinancing, dcb.BidRate, dcb.BidSpread, dcb.BidLoanTermYears, 
                      dcb.BidPrepaymentPenaltyNote, dcb.BidCapRate, dcb.BidPricePSF, dcb.BidEquityCapital, dcb.BidGeneralNote, dcb.BidOriginationFee, dcb.BidReferenceIndex, 
                      dcb.BidLoanType, dcb.Won, dcb.LastUpdate, dc.MarketingStatusID, dc.CASent, dc.CALogged, dc.CAUnApproved, dc.OMSent, dc.OMDigital, dc.WarroomAccess, 
                      dc.DDSent, dc.Tour, dc.DIP, dc.DoNotSendOM, con.PrefersDigitalOM, con.DoNotSolicit, dc.DoNotSendWarroom, con.ETeasersBlocked, con.SpecialConditions, 
                      con.EmailAddress, dbo.Employees.EmpInitials, dc.Category, dc.Class, dc.Tier, dc.ContactID, dc.CompanyID, con.Notes, dc.StatusNote, dcb.JointVentureBid, 
                      dcb.PercentAllocation, dcb.JointVentureAmount, dcb.BidIDNumber
FROM         dbo.MarketingStatuses AS ms INNER JOIN
                      dbo.DealContacts AS dc ON ms.MarketingStatusID = dc.MarketingStatusID INNER JOIN
                      dbo.Deals AS d ON d.DealID = dc.DealID INNER JOIN
                      dbo.Contacts AS con ON con.ContactID = dc.ContactID INNER JOIN
                      dbo.Companies AS c ON c.CompanyID = dc.CompanyID INNER JOIN
                      dbo.PropertyTypes ON d.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID INNER JOIN
                      dbo.Employees ON dc.EmployeeID = dbo.Employees.EmployeeID RIGHT OUTER JOIN
                      dbo.DealContactBids AS dcb ON dc.DealContactID = dcb.DealContactID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactBidShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'       TopColumn = 0
         End
         Begin Table = "dcb"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 296
               Right = 249
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 38
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2400
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactBidShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ms"
            Begin Extent = 
               Top = 299
               Left = 561
               Bottom = 407
               Right = 739
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dc"
            Begin Extent = 
               Top = 6
               Left = 299
               Bottom = 379
               Right = 510
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 176
               Left = 796
               Bottom = 405
               Right = 1040
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "con"
            Begin Extent = 
               Top = 26
               Left = 1319
               Bottom = 296
               Right = 1548
            End
            DisplayFlags = 280
            TopColumn = 31
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 1569
               Bottom = 114
               Right = 1786
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 180
               Left = 1133
               Bottom = 288
               Right = 1311
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employees"
            Begin Extent = 
               Top = 163
               Left = 1591
               Bottom = 271
               Right = 1797
            End
            DisplayFlags = 280
     ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealContactBidShort';


﻿CREATE VIEW dbo.DealPropertyShort
AS
SELECT     dbo.DealProperties.DealPropertyID, dbo.Properties.PropertyID, dbo.Properties.PropertyName, dbo.Properties.PropertyDescription, dbo.PropertyTypes.PropertyType, 
                      dbo.AssetClasses.AssetClass, dbo.Properties.Address1, dbo.Properties.City, dbo.Properties.State, dbo.Properties.Country, dbo.DealProperties.DealID, 
                      dbo.Deals.DealCode, dbo.Deals.DealName, dbo.TransactionTypes.TransactionType, dbo.DealStatuses.DealStatus, dbo.Deals.DealOffice, dbo.Deals.ESTeam, 
                      dbo.Deals.DateAdded, dbo.DealTimelines.DateToMarket, dbo.DealTimelines.DateCloseAct, dbo.Deals.Confidential, dbo.Properties.Buyer, dbo.Properties.BuyerDBA, 
                      dbo.Properties.Seller, dbo.Properties.SellerDBA
FROM         dbo.PropertyTypes RIGHT OUTER JOIN
                      dbo.DealProperties INNER JOIN
                      dbo.Properties ON dbo.DealProperties.PropertyID = dbo.Properties.PropertyID INNER JOIN
                      dbo.TransactionTypes INNER JOIN
                      dbo.Deals ON dbo.TransactionTypes.TransactionTypeID = dbo.Deals.TransactionTypeID INNER JOIN
                      dbo.DealStatuses ON dbo.Deals.DealStatusID = dbo.DealStatuses.DealStatusID INNER JOIN
                      dbo.DealTimelines ON dbo.Deals.DealID = dbo.DealTimelines.DealID ON dbo.DealProperties.DealID = dbo.Deals.DealID ON 
                      dbo.PropertyTypes.PropertyTypeID = dbo.Properties.PropertyTypeID LEFT OUTER JOIN
                      dbo.AssetClasses ON dbo.Properties.AssetClassID = dbo.AssetClasses.AssetClassID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'     End
            DisplayFlags = 280
            TopColumn = 20
         End
         Begin Table = "AssetClasses"
            Begin Extent = 
               Top = 183
               Left = 435
               Bottom = 276
               Right = 586
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 22
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1980
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealPropertyShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealPropertyShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[51] 4[21] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 121
               Left = 606
               Bottom = 229
               Right = 784
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealProperties"
            Begin Extent = 
               Top = 17
               Left = 20
               Bottom = 231
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Properties"
            Begin Extent = 
               Top = 16
               Left = 266
               Bottom = 261
               Right = 444
            End
            DisplayFlags = 280
            TopColumn = 24
         End
         Begin Table = "TransactionTypes"
            Begin Extent = 
               Top = 336
               Left = 401
               Bottom = 444
               Right = 579
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Deals"
            Begin Extent = 
               Top = 262
               Left = 123
               Bottom = 370
               Right = 320
            End
            DisplayFlags = 280
            TopColumn = 66
         End
         Begin Table = "DealStatuses"
            Begin Extent = 
               Top = 285
               Left = 679
               Bottom = 393
               Right = 857
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealTimelines"
            Begin Extent = 
               Top = 267
               Left = 933
               Bottom = 375
               Right = 1150
       ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'DealPropertyShort';


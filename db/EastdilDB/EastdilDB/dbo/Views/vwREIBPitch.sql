﻿CREATE VIEW dbo.vwREIBPitch
AS
SELECT     dbo.PitchLibrary.PitchID, dbo.PitchLibrary.CompanyID, dbo.PitchLibrary.ProductLine, dbo.PitchLibrary.ProductType, dbo.PitchLibrary.PresentedTo, 
                      dbo.PitchLibrary.ProjectName, dbo.PitchLibrary.AssetLocation, dbo.PitchLibrary.Office, dbo.PitchLibrary.MD, dbo.PitchLibrary.Portfolio, dbo.PitchLibrary.FileName, 
                      dbo.PitchLibrary.LeadPresenter, dbo.PitchLibrary.AdditionalPresenter, dbo.PitchLibrary.MaterialsCoordinator, dbo.PitchLibrary.MaterialsContributor, 
                      dbo.Companies.CompanyName, dbo.PitchLibrary.PitchDate
FROM         dbo.PitchLibrary INNER JOIN
                      dbo.Companies ON dbo.PitchLibrary.CompanyID = dbo.Companies.CompanyID
﻿CREATE VIEW dbo.EmployeeTrainingShort
AS
SELECT     dbo.EmployeeTraining.EmployeeTrainingID, dbo.EmployeeTraining.EmployeeID, dbo.EmployeeTraining.CourseID, dbo.EmployeeCourses.CourseName, 
                      dbo.EmployeeCourses.CourseDescription, dbo.EmployeeCourses.CourseDueDate, dbo.EmployeeTraining.CompleteDate, dbo.EmployeeTraining.Complete
FROM         dbo.EmployeeCourses INNER JOIN
                      dbo.EmployeeTraining ON dbo.EmployeeCourses.CourseID = dbo.EmployeeTraining.CourseID
﻿CREATE VIEW dbo.LoanBorrowerShort
AS
SELECT        dbo.Companies.CompanyName AS BorrowerCompanyName, dbo.Companies.Active, dbo.Companies.Client, dbo.Addresses.Address1, dbo.Addresses.City, dbo.Addresses.State, dbo.LoanTypes.LoanType, 
                         dbo.Loans.LoanDescription, dbo.Loans.LoanBalance, dbo.Loans.NextMaturityDate, dbo.Loans.FinalMaturityDate, dbo.LoanBorrowers.LoanID, dbo.Companies.CompanyID, 
                         dbo.LoanBorrowers.BorrowerCompanyID, dbo.LoanBorrowers.LoanBorrowerID, dbo.LoanBorrowers.BorrowerDBA, dbo.Loans.LenderDBA, dbo.Loans.LenderCompanyID, dbo.Loans.OriginalMaturityDate, 
                         dbo.Loans.OriginationDate, dbo.Companies.DBAName
FROM            dbo.Companies INNER JOIN
                         dbo.Addresses ON dbo.Addresses.AddressID = dbo.Companies.AddressID INNER JOIN
                         dbo.LoanBorrowers ON dbo.Companies.CompanyID = dbo.LoanBorrowers.BorrowerCompanyID INNER JOIN
                         dbo.Loans ON dbo.LoanBorrowers.LoanID = dbo.Loans.LoanID LEFT OUTER JOIN
                         dbo.LoanTypes ON dbo.Loans.LoanTypeID = dbo.LoanTypes.LoanTypeID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[33] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Companies"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 210
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 75
         End
         Begin Table = "Addresses"
            Begin Extent = 
               Top = 246
               Left = 231
               Bottom = 354
               Right = 409
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LoanBorrowers"
            Begin Extent = 
               Top = 26
               Left = 440
               Bottom = 231
               Right = 620
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Loans"
            Begin Extent = 
               Top = 6
               Left = 727
               Bottom = 322
               Right = 953
            End
            DisplayFlags = 280
            TopColumn = 26
         End
         Begin Table = "LoanTypes"
            Begin Extent = 
               Top = 6
               Left = 991
               Bottom = 99
               Right = 1142
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
     ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'LoanBorrowerShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'LoanBorrowerShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'    Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2790
         Alias = 2655
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'LoanBorrowerShort';


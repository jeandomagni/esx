﻿CREATE VIEW dbo.PropertyLoanShort
AS
SELECT     dbo.Loans.LoanID, dbo.Loans.LoanName, dbo.Loans.LoanDescription, dbo.Loans.Lender, dbo.Properties.PropertyID, dbo.Properties.PropertyName, 
                      dbo.Properties.PropertyDescription, dbo.PropertyTypes.PropertyType, dbo.AssetClasses.AssetClass, dbo.Properties.Address1, dbo.Properties.City, 
                      dbo.Properties.State, dbo.Properties.Country, dbo.LoanTypes.LoanType, dbo.Loans.Borrowers, dbo.Loans.LoanBalance, dbo.Loans.OriginalMaturityDate, 
                      dbo.Loans.First$DebtYield, dbo.Loans.Last$DebtYield, dbo.Loans.AllInInterestRate
FROM         dbo.Properties INNER JOIN
                      dbo.PropertyTypes ON dbo.Properties.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID INNER JOIN
                      dbo.AssetClasses ON dbo.Properties.AssetClassID = dbo.AssetClasses.AssetClassID INNER JOIN
                      dbo.Loans ON dbo.Properties.PropertyID = dbo.Loans.PropertyID INNER JOIN
                      dbo.LoanTypes ON dbo.Loans.LoanTypeID = dbo.LoanTypes.LoanTypeID
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PropertyLoanShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'  Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PropertyLoanShort';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Properties"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 6
               Left = 254
               Bottom = 114
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "AssetClasses"
            Begin Extent = 
               Top = 6
               Left = 470
               Bottom = 99
               Right = 621
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Loans"
            Begin Extent = 
               Top = 6
               Left = 659
               Bottom = 264
               Right = 885
            End
            DisplayFlags = 280
            TopColumn = 26
         End
         Begin Table = "LoanTypes"
            Begin Extent = 
               Top = 6
               Left = 923
               Bottom = 110
               Right = 1083
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
       ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PropertyLoanShort';


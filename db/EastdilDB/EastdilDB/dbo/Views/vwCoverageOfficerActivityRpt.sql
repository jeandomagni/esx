﻿CREATE VIEW dbo.vwCoverageOfficerActivityRpt
AS
SELECT     TOP (100) PERCENT e.EmployeeID, e.EmployeeName, e.EMailAddress, cov.ContactID, c.FirstnameLastname + ' (' + co.CompanyName + ')' AS Contact, 
                      d.DealCode + ' - ' + d.DealName + ' (' + ds.DealStatus + ')' AS DealCodeNameStatus, dbo.DealEconomics.ProjectSize, 
                      dbo.PropertyTypes.PropertyType, d.DealOffice, d.DealLeader, cov.ActivityType
FROM         dbo.CoverageOfficerActivityRpt AS cov INNER JOIN
                      dbo.Contacts AS c ON cov.ContactID = c.ContactID INNER JOIN
                      dbo.Companies AS co ON c.CompanyID = co.CompanyID INNER JOIN
                      dbo.Deals AS d ON cov.DealID = d.DealID INNER JOIN
                      dbo.DealStatuses AS ds ON ds.DealStatusID = d.DealStatusID INNER JOIN
                      dbo.Employees AS e ON cov.ESContactEmployeeID = e.EmployeeID INNER JOIN
                      dbo.DealEconomics ON d.DealID = dbo.DealEconomics.DealID RIGHT OUTER JOIN
                      dbo.PropertyTypes ON d.PropertyTypeID = dbo.PropertyTypes.PropertyTypeID
ORDER BY e.LastName, e.FirstName, d.DealCode, co.CompanyName, c.LastName, c.FirstName
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'        End
         Begin Table = "PropertyTypes"
            Begin Extent = 
               Top = 191
               Left = 334
               Bottom = 299
               Right = 512
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 2835
         Width = 4380
         Width = 3315
         Width = 3225
         Width = 1500
         Width = 2325
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 5370
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwCoverageOfficerActivityRpt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwCoverageOfficerActivityRpt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "cov"
            Begin Extent = 
               Top = 6
               Left = 1011
               Bottom = 213
               Right = 1198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 267
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "co"
            Begin Extent = 
               Top = 6
               Left = 305
               Bottom = 114
               Right = 522
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 560
               Bottom = 297
               Right = 757
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "ds"
            Begin Extent = 
               Top = 6
               Left = 795
               Bottom = 114
               Right = 973
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 6
               Left = 1236
               Bottom = 319
               Right = 1440
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DealEconomics"
            Begin Extent = 
               Top = 185
               Left = 37
               Bottom = 370
               Right = 281
            End
            DisplayFlags = 280
            TopColumn = 0
 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwCoverageOfficerActivityRpt';


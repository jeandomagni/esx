﻿CREATE TABLE [dbo].[CompanyLists] (
    [CompanyListID]         INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ListName]              NVARCHAR (64)    NULL,
    [ListManagerEmployeeID] BIGINT           NULL,
    [Author]                VARCHAR (50)     NULL,
    [CapRaiseDealsOnly]     BIT              CONSTRAINT [DF__CompanyLi__CapRa__75A278F5] DEFAULT ((0)) NULL,
    [Comments]              NVARCHAR (1000)  NULL,
    [Active]                BIT              CONSTRAINT [DF__CompanyLi__Activ__76969D2E] DEFAULT ((0)) NULL,
    [LastUpdate]            DATETIME         CONSTRAINT [DF__CompanyLi__LastU__778AC167] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]   UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_459A8471_0F89_4FB1_8AC3_9E16BF739C93_485576768] DEFAULT (newid()) NOT NULL,
    [RowVersion]            ROWVERSION       NOT NULL,
    [DateAdded]             DATETIME         NULL,
    [AddedBy]               NVARCHAR (100)   NULL,
    [UpdatedBy]             NVARCHAR (100)   NULL,
    CONSTRAINT [PK_CompanyLists] PRIMARY KEY CLUSTERED ([CompanyListID] ASC),
    CONSTRAINT [FK_CompanyLists_Employees] FOREIGN KEY ([ListManagerEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLists';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLists';


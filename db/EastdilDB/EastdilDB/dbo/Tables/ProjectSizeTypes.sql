﻿CREATE TABLE [dbo].[ProjectSizeTypes] (
    [ProjectSizeTypeID] INT            NOT NULL,
    [ProjectSizeType]   NVARCHAR (50)  NULL,
    [DisplayOrder]      INT            NULL,
    [RowVersion]        ROWVERSION     NOT NULL,
    [DateAdded]         DATETIME       NULL,
    [LastUpdate]        DATETIME       NULL,
    [AddedBy]           NVARCHAR (100) NULL,
    [UpdatedBy]         NVARCHAR (100) NULL,
    CONSTRAINT [PK_ProjectSizeTypes] PRIMARY KEY CLUSTERED ([ProjectSizeTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProjectSizeTypes';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProjectSizeTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProjectSizeTypes', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProjectSizeTypes', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProjectSizeTypes', @level2type = N'COLUMN', @level2name = N'ProjectSizeTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProjectSizeTypes', @level2type = N'COLUMN', @level2name = N'ProjectSizeType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProjectSizeTypes', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProjectSizeTypes', @level2type = N'COLUMN', @level2name = N'DisplayOrder';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProjectSizeTypes', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProjectSizeTypes', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[ESIEligibility] (
    [ESIEligibilityID] INT            NOT NULL,
    [ESIEligibility]   NVARCHAR (50)  NULL,
    [Definition]       NVARCHAR (500) NULL,
    CONSTRAINT [PK_ESIEligibility] PRIMARY KEY CLUSTERED ([ESIEligibilityID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ESIEligibility';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ESIEligibility';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ESIEligibility', @level2type = N'COLUMN', @level2name = N'ESIEligibilityID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ESIEligibility', @level2type = N'COLUMN', @level2name = N'ESIEligibility';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ESIEligibility', @level2type = N'COLUMN', @level2name = N'Definition';




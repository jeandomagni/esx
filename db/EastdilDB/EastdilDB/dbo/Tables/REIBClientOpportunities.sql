﻿CREATE TABLE [dbo].[REIBClientOpportunities] (
    [REIBClientOpportunityID]           BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CompanyID]                         BIGINT           NULL,
    [MAStrategicOpportunity1]           NVARCHAR (2000)  NULL,
    [MAStrategicOpportunity2]           NVARCHAR (2000)  NULL,
    [MAStrategicOpportunity3]           NVARCHAR (2000)  NULL,
    [MAStrategicOpportunity4]           NVARCHAR (2000)  NULL,
    [MAStratObservations1]              NVARCHAR (2000)  NULL,
    [MAStratObservations2]              NVARCHAR (2000)  NULL,
    [MAStratObservations3]              NVARCHAR (2000)  NULL,
    [MAStratObservations4]              NVARCHAR (2000)  NULL,
    [EquityOpportunity1]                NVARCHAR (2000)  NULL,
    [EquityOpportunity2]                NVARCHAR (2000)  NULL,
    [EquityOpportunity3]                NVARCHAR (2000)  NULL,
    [EquityOpportunity4]                NVARCHAR (2000)  NULL,
    [EquityObservations1]               NVARCHAR (2000)  NULL,
    [EquityObservations2]               NVARCHAR (2000)  NULL,
    [EquityObservations3]               NVARCHAR (2000)  NULL,
    [EquityObservations4]               NVARCHAR (2000)  NULL,
    [FixedIncomeOpportunity1]           NVARCHAR (2000)  NULL,
    [FixedIncomeOpportunity2]           NVARCHAR (2000)  NULL,
    [FixedIncomeOpportunity3]           NVARCHAR (2000)  NULL,
    [FixedIncomeOpportunity4]           NVARCHAR (2000)  NULL,
    [FixedIncomeObservations1]          NVARCHAR (2000)  NULL,
    [FixedIncomeObservations2]          NVARCHAR (2000)  NULL,
    [FixedIncomeObservations3]          NVARCHAR (2000)  NULL,
    [FixedIncomeObservations4]          NVARCHAR (2000)  NULL,
    [DebtPlacementOpportunity1]         NVARCHAR (2000)  NULL,
    [DebtPlacementOpportunity2]         NVARCHAR (2000)  NULL,
    [DebtPlacementOpportunity3]         NVARCHAR (2000)  NULL,
    [DebtPlacementOpportunity4]         NVARCHAR (2000)  NULL,
    [DebtPlacementObservations1]        NVARCHAR (2000)  NULL,
    [DebtPlacementObservations2]        NVARCHAR (2000)  NULL,
    [DebtPlacementObservations3]        NVARCHAR (2000)  NULL,
    [DebtPlacementObservations4]        NVARCHAR (2000)  NULL,
    [LoanSyndBankTermDebtOpportunity1]  NVARCHAR (2000)  NULL,
    [LoanSyndBankTermDebtOpportunity2]  NVARCHAR (2000)  NULL,
    [LoanSyndBankTermDebtOpportunity3]  NVARCHAR (2000)  NULL,
    [LoanSyndBankTermDebtOpportunity4]  NVARCHAR (2000)  NULL,
    [LoanSyndBankTermDebtObservations1] NVARCHAR (2000)  NULL,
    [LoanSyndBankTermDebtObservations2] NVARCHAR (2000)  NULL,
    [LoanSyndBankTermDebtObservations3] NVARCHAR (2000)  NULL,
    [LoanSyndBankTermDebtObservations4] NVARCHAR (2000)  NULL,
    [AssetSalesOpportunity1]            NVARCHAR (2000)  NULL,
    [AssetSalesOpportunity2]            NVARCHAR (2000)  NULL,
    [AssetSalesOpportunity3]            NVARCHAR (2000)  NULL,
    [AssetSalesOpportunity4]            NVARCHAR (2000)  NULL,
    [AssetSalesObservations1]           NVARCHAR (2000)  NULL,
    [AssetSalesObservations2]           NVARCHAR (2000)  NULL,
    [AssetSalesObservations3]           NVARCHAR (2000)  NULL,
    [AssetSalesObservations4]           NVARCHAR (2000)  NULL,
    [PrivateEquityOpportunity1]         NVARCHAR (2000)  NULL,
    [PrivateEquityOpportunity2]         NVARCHAR (2000)  NULL,
    [PrivateEquityOpportunity3]         NVARCHAR (2000)  NULL,
    [PrivateEquityOpportunity4]         NVARCHAR (2000)  NULL,
    [PrivateEquityObservations1]        NVARCHAR (2000)  NULL,
    [PrivateEquityObservations2]        NVARCHAR (2000)  NULL,
    [PrivateEquityObservations3]        NVARCHAR (2000)  NULL,
    [PrivateEquityObservations4]        NVARCHAR (2000)  NULL,
    [DerivativesOtherOpportunity1]      NVARCHAR (2000)  NULL,
    [DerivativesOtherOpportunity2]      NVARCHAR (2000)  NULL,
    [DerivativesOtherOpportunity3]      NVARCHAR (2000)  NULL,
    [DerivativesOtherOpportunity4]      NVARCHAR (2000)  NULL,
    [DerivativesOtherObservations1]     NVARCHAR (2000)  NULL,
    [DerivativesOtherObservations2]     NVARCHAR (2000)  NULL,
    [DerivativesOtherObservations3]     NVARCHAR (2000)  NULL,
    [DerivativesOtherObservations4]     NVARCHAR (2000)  NULL,
    [LastUpdate]                        DATETIME         CONSTRAINT [DF_REIBClientOpportunities_LastUpdate] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]               UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_476CD2A2_1E3A_4856_894C_539CDB01FD55_1301579675] DEFAULT (newid()) NOT NULL,
    [RowVersion]                        ROWVERSION       NOT NULL,
    [DateAdded]                         DATETIME         NULL,
    [AddedBy]                           NVARCHAR (100)   NULL,
    [UpdatedBy]                         NVARCHAR (100)   NULL,
    CONSTRAINT [PK_REIBClientOpportunities] PRIMARY KEY CLUSTERED ([REIBClientOpportunityID] ASC),
    CONSTRAINT [FK_REIBClientOpportunities_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'REIBClientOpportunityID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'PrivateEquityOpportunity4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'PrivateEquityOpportunity3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'PrivateEquityOpportunity2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=32 @ 4.34:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'PrivateEquityOpportunity1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'PrivateEquityObservations4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'PrivateEquityObservations3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'PrivateEquityObservations2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=39 @ 3.56:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'PrivateEquityObservations1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=78 @ 1.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'MAStratObservations4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'MAStratObservations3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=39 @ 3.56:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'MAStratObservations2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=49 @ 2.84:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'MAStratObservations1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'MAStrategicOpportunity4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'MAStrategicOpportunity3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'MAStrategicOpportunity2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=50 @ 2.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'MAStrategicOpportunity1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'LoanSyndBankTermDebtOpportunity4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'LoanSyndBankTermDebtOpportunity3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 19.86:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'LoanSyndBankTermDebtOpportunity2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=30 @ 4.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'LoanSyndBankTermDebtOpportunity1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'LoanSyndBankTermDebtObservations4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=15 @ 9.27:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'LoanSyndBankTermDebtObservations3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 4.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'LoanSyndBankTermDebtObservations2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=52 @ 2.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'LoanSyndBankTermDebtObservations1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'FixedIncomeOpportunity4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 34.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'FixedIncomeOpportunity3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=17 @ 8.18:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'FixedIncomeOpportunity2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=40 @ 3.47:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'FixedIncomeOpportunity1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'FixedIncomeObservations4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'FixedIncomeObservations3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=26 @ 5.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'FixedIncomeObservations2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=46 @ 3.02:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'FixedIncomeObservations1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'EquityOpportunity4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'EquityOpportunity3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 15.44:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'EquityOpportunity2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=45 @ 3.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'EquityOpportunity1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'EquityObservations4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 15.44:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'EquityObservations3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=29 @ 4.79:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'EquityObservations2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=51 @ 2.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'EquityObservations1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DerivativesOtherOpportunity4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DerivativesOtherOpportunity3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 34.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DerivativesOtherOpportunity2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=20 @ 6.95:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DerivativesOtherOpportunity1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DerivativesOtherObservations4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DerivativesOtherObservations3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DerivativesOtherObservations2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=18 @ 7.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DerivativesOtherObservations1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DebtPlacementOpportunity4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DebtPlacementOpportunity3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 19.86:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DebtPlacementOpportunity2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=38 @ 3.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DebtPlacementOpportunity1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DebtPlacementObservations4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 34.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DebtPlacementObservations3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DebtPlacementObservations2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=47 @ 2.96:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DebtPlacementObservations1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=137 @ 1.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'AssetSalesOpportunity4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'AssetSalesOpportunity3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 17.38:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'AssetSalesOpportunity2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=38 @ 3.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'AssetSalesOpportunity1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'AssetSalesObservations4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 27.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'AssetSalesObservations3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=17 @ 8.18:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'AssetSalesObservations2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=41 @ 3.39:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'AssetSalesObservations1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBClientOpportunities', @level2type = N'COLUMN', @level2name = N'AddedBy';




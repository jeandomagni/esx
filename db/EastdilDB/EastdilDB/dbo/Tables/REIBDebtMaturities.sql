﻿CREATE TABLE [dbo].[REIBDebtMaturities] (
    [REIBDebtMaturityID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [CompanyID]          BIGINT         NULL,
    [REIBDebtTypeID]     INT            NULL,
    [DebtMaturity]       NVARCHAR (50)  NULL,
    [DebtAmount]         MONEY          NULL,
    [DateAsOf]           DATETIME       NULL,
    [DateAdded]          DATETIME       NULL,
    [LastUpdate]         DATETIME       NULL,
    [RowVersion]         ROWVERSION     NOT NULL,
    [AddedBy]            NVARCHAR (100) NULL,
    [UpdatedBy]          NVARCHAR (100) NULL,
    CONSTRAINT [PK_REIBDebtMaturities] PRIMARY KEY CLUSTERED ([REIBDebtMaturityID] ASC),
    CONSTRAINT [FK_REIBDebtMaturities_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_REIBDebtMaturities_REIBDebtTypes] FOREIGN KEY ([REIBDebtTypeID]) REFERENCES [dbo].[REIBDebtTypes] ([REIBDebtTypeID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=Lookup,Notes=Unimplemented feature brought over from Secured Capital version', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBDebtMaturities';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBDebtMaturities';


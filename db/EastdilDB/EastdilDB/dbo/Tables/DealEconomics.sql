﻿CREATE TABLE [dbo].[DealEconomics] (
    [DealEconomicID]                  BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealID]                          BIGINT           NULL,
    [ClosedPricePSF]                  MONEY            NULL,
    [ClosedValuePerUnit]              MONEY            NULL,
    [ValuePerUnit]                    MONEY            NULL,
    [EstSalePrice]                    MONEY            NULL,
    [ProjectSize]                     INT              NULL,
    [ProjectSizeTypeID]               INT              NULL,
    [PricePerUnitOfMeasure]           INT              NULL,
    [Occupancy]                       NUMERIC (19, 2)  NULL,
    [InPlaceCapRatePct]               NUMERIC (19, 3)  NULL,
    [InPlaceCapRateNote]              NVARCHAR (100)   NULL,
    [Year1CapRatePct]                 NUMERIC (19, 3)  NULL,
    [Year1CapRateNote]                NVARCHAR (100)   NULL,
    [TenYearIRRPct]                   NUMERIC (19, 3)  NULL,
    [IRRPct]                          NUMERIC (19, 3)  NULL,
    [IRRTypeID]                       INT              NULL,
    [FiveYearCashOnCashPct]           NUMERIC (19, 3)  NULL,
    [ExitCapRatePct]                  NUMERIC (19, 3)  NULL,
    [ExitCapRateNote]                 NVARCHAR (100)   NULL,
    [BuyerName]                       NVARCHAR (500)   NULL,
    [JVSale]                          NVARCHAR (500)   NULL,
    [PercentInterestSold]             NUMERIC (19, 3)  NULL,
    [TopBidders]                      NVARCHAR (500)   NULL,
    [OffShoreBidders]                 NVARCHAR (500)   NULL,
    [WasDebtAssumed]                  NVARCHAR (100)   NULL,
    [WasNewDebtPlaced]                NVARCHAR (100)   NULL,
    [DebtTerms]                       NVARCHAR (100)   NULL,
    [AnchorTenants]                   NVARCHAR (2000)  NULL,
    [AnchorSalesPerSqFt]              MONEY            NULL,
    [IsOneAnchorAGrocery]             NVARCHAR (50)    NULL,
    [AvgSalesPerSqFt]                 MONEY            NULL,
    [AvgOccCost]                      MONEY            NULL,
    [PurchasePrice]                   MONEY            NULL,
    [PurchPriceOrValueBySqFtOrUnit]   MONEY            NULL,
    [LenderName]                      NVARCHAR (500)   NULL,
    [LoanTypeID]                      INT              NULL,
    [NumberOfLoans]                   INT              NULL,
    [TotalLoanAmt]                    MONEY            NULL,
    [TotalLoanAmtBySqFtOrUnits]       MONEY            NULL,
    [InitialLoanAmt]                  MONEY            NULL,
    [EarnoutHoldback]                 MONEY            NULL,
    [TILCHoldback]                    MONEY            NULL,
    [TotalDebtStack]                  MONEY            NULL,
    [UnpaidBalance]                   MONEY            NULL,
    [UnpaidBalancePerSqFtOrUnit]      MONEY            NULL,
    [PurchasePricePctOfUnpaidBalance] NUMERIC (19, 2)  NULL,
    [InterestRate]                    NUMERIC (19, 2)  NULL,
    [LoanConstant]                    NVARCHAR (500)   NULL,
    [LoanPerfTypeID]                  INT              NULL,
    [LoanRateTypeID]                  INT              NULL,
    [MostRecentDSCR]                  NVARCHAR (500)   NULL,
    [EquityLike]                      NVARCHAR (50)    NULL,
    [OriginationDate]                 DATETIME         NULL,
    [MaturityDate]                    DATETIME         NULL,
    [LoanTerm]                        NVARCHAR (100)   NULL,
    [ApproxDateRateQuotedLocked]      DATETIME         NULL,
    [ApproxDateSpreadSet]             DATETIME         NULL,
    [ReferenceIndexID]                INT              NULL,
    [Spread]                          INT              NULL,
    [SpreadNote]                      NVARCHAR (100)   NULL,
    [TreasOrSwapRateWhenLocked]       NUMERIC (19, 2)  NULL,
    [LockedRate]                      NUMERIC (19, 2)  NULL,
    [RateFloor]                       NUMERIC (19, 2)  NULL,
    [OriginationFee]                  NUMERIC (19, 2)  NULL,
    [Amortization]                    NVARCHAR (500)   NULL,
    [Prepayment]                      NVARCHAR (500)   NULL,
    [LockOut]                         NVARCHAR (500)   NULL,
    [OpenPeriod]                      NVARCHAR (500)   NULL,
    [MinimumDSCR]                     NVARCHAR (500)   NULL,
    [Recourse]                        NVARCHAR (500)   NULL,
    [Reserves]                        NVARCHAR (500)   NULL,
    [YTMPct]                          NUMERIC (19, 2)  NULL,
    [Coupon]                          NUMERIC (19, 2)  NULL,
    [FinancingTypeID]                 INT              NULL,
    [PercentLeased]                   NUMERIC (19, 2)  NULL,
    [NumberOfOffers]                  INT              NULL,
    [CommentsOnDeal]                  NVARCHAR (4000)  NULL,
    [LTCtoLTV]                        NUMERIC (19, 2)  NULL,
    [InPlaceDebtYield]                NUMERIC (19, 2)  NULL,
    [EstLoanAmount]                   MONEY            NULL,
    [EstPricePSF]                     MONEY            NULL,
    [CapRate]                         NVARCHAR (100)   NULL,
    [EncumberedID]                    INT              NULL,
    [AnticipYield]                    NUMERIC (18, 1)  NULL,
    [IRR]                             NVARCHAR (100)   NULL,
    [TrailingYield]                   NUMERIC (19, 2)  NULL,
    [PricingDate]                     DATETIME         NULL,
    [InitialAnnouncement]             DATETIME         NULL,
    [BusinessDescription]             NVARCHAR (4000)  NULL,
    [UseOfProceeds]                   NVARCHAR (2000)  NULL,
    [BookRunningManagers]             NVARCHAR (500)   NULL,
    [CoManagers]                      NVARCHAR (500)   NULL,
    [StabilizationAgent]              NVARCHAR (500)   NULL,
    [BuysideAdvisor]                  NVARCHAR (500)   NULL,
    [TransactionSize]                 MONEY            NULL,
    [TransactionOverview]             NVARCHAR (4000)  NULL,
    [NotableAspects]                  NVARCHAR (4000)  NULL,
    [WFDealTeamContacts]              NVARCHAR (2000)  NULL,
    [LastUpdate]                      DATETIME         CONSTRAINT [DF__DealEcono__LastU__22751F6C] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]             UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_0973D5F5_2FDC_4C63_88B7_E35CEA8F2770_853578079] DEFAULT (newid()) NOT NULL,
    [RowVersion]                      ROWVERSION       NOT NULL,
    [DateAdded]                       DATETIME         NULL,
    [AddedBy]                         NVARCHAR (100)   NULL,
    [UpdatedBy]                       NVARCHAR (100)   NULL,
    [BuyerCompanyID]                  BIGINT           NULL,
    CONSTRAINT [PK_DealEconomics] PRIMARY KEY CLUSTERED ([DealEconomicID] ASC),
    CONSTRAINT [FK_DealEconomics_BuyerCompanies] FOREIGN KEY ([BuyerCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealEconomics_DealEconomics2] FOREIGN KEY ([DealEconomicID]) REFERENCES [dbo].[DealEconomics] ([DealEconomicID]),
    CONSTRAINT [FK_DealEconomics_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealEconomics_Encumbered] FOREIGN KEY ([EncumberedID]) REFERENCES [dbo].[Encumbered] ([EncumberedID]),
    CONSTRAINT [FK_DealEconomics_IRRTypes] FOREIGN KEY ([IRRTypeID]) REFERENCES [dbo].[IRRTypes] ([IRRTypeID]),
    CONSTRAINT [FK_DealEconomics_LoanPerfTypes] FOREIGN KEY ([LoanPerfTypeID]) REFERENCES [dbo].[LoanPerfTypes] ([LoanPerfTypeID]),
    CONSTRAINT [FK_DealEconomics_LoanRateTypes] FOREIGN KEY ([LoanRateTypeID]) REFERENCES [dbo].[LoanRateTypes] ([LoanRateTypeID]),
    CONSTRAINT [FK_DealEconomics_LoanTypes] FOREIGN KEY ([LoanTypeID]) REFERENCES [dbo].[LoanTypes] ([LoanTypeID]),
    CONSTRAINT [FK_DealEconomics_ProjectSizeTypes] FOREIGN KEY ([ProjectSizeTypeID]) REFERENCES [dbo].[ProjectSizeTypes] ([ProjectSizeTypeID]),
    CONSTRAINT [FK_DealEconomics_ReferenceIndex] FOREIGN KEY ([ReferenceIndexID]) REFERENCES [dbo].[ReferenceIndex] ([ReferenceIndexID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=Covers Deal financial metrics for deal types other than EquitySales, Financings and LoanSales ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'YTMPct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=60 @ 204.77:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'Year1CapRatePct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'Year1CapRateNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'WFDealTeamContacts';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 6143.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'WasNewDebtPlaced';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 1535.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'WasDebtAssumed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=602 @ 20.41:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'ValuePerUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'UseOfProceeds';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'UnpaidBalancePerSqFtOrUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=392 @ 31.34:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'UnpaidBalance';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'TreasOrSwapRateWhenLocked';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=49 @ 250.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'TransactionSize';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'TransactionOverview';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=60 @ 204.77:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'TrailingYield';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'TotalLoanAmtBySqFtOrUnits';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=752 @ 16.34:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'TotalLoanAmt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'TotalDebtStack';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=67 @ 183.37:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'TopBidders';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'TILCHoldback';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=58 @ 211.83:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'TenYearIRRPct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'StabilizationAgent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 1365.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'SpreadNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=90 @ 136.51:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'Spread';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'Reserves';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4095.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'ReferenceIndexID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'Recourse';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'RateFloor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'PurchPriceOrValueBySqFtOrUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'PurchasePricePctOfUnpaidBalance';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=26 @ 472.54:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'PurchasePrice';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5 @ 2457.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'ProjectSizeTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4978 @ 2.47:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'ProjectSize';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'PricingDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=89 @ 138.04:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'PricePerUnitOfMeasure';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'Prepayment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 1535.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'PercentLeased';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=51 @ 240.90:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'PercentInterestSold';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 2457.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'OriginationFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'OriginationDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'OpenPeriod';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=31 @ 396.32:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'OffShoreBidders';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=156 @ 78.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'Occupancy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=14 @ 877.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'NumberOfOffers';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 2047.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'NumberOfLoans';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'NotableAspects';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4547 @ 2.70:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'MostRecentDSCR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'MinimumDSCR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'MaturityDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=30 @ 409.53:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'LTCtoLTV';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 2047.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'LockOut';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'LockedRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 3071.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'LoanTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 1535.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'LoanTerm';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 3071.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'LoanRateTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 12286.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'LoanPerfTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'LoanConstant';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 1365.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'LenderName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8009 @ 1.53:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 1365.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'JVSale';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'IsOneAnchorAGrocery';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4095.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'IRRTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=16 @ 767.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'IRRPct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=108 @ 113.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'IRR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'InterestRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=16 @ 767.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'InPlaceDebtYield';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=61 @ 201.41:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'InPlaceCapRatePct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'InPlaceCapRateNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'InitialLoanAmt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'InitialAnnouncement';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 361.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'FiveYearCashOnCashPct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 12286.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'FinancingTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=33 @ 372.30:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'ExitCapRatePct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'ExitCapRateNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1625 @ 7.56:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'EstSalePrice';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=449 @ 27.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'EstPricePSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=609 @ 20.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'EstLoanAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'EquityLike';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4095.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'EncumberedID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'EarnoutHoldback';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=75 @ 163.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'DebtTerms';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'DealEconomicID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'Coupon';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=140 @ 87.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'CommentsOnDeal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'CoManagers';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=317 @ 38.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'ClosedValuePerUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1240 @ 9.91:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'ClosedPricePSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=138 @ 89.03:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'CapRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'BuysideAdvisor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=160 @ 76.79:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'BuyerName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=31 @ 396.32:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'BuyerCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'BusinessDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'BookRunningManagers';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'AvgSalesPerSqFt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 2047.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'AvgOccCost';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 1535.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'ApproxDateSpreadSet';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 1755.14:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'ApproxDateRateQuotedLocked';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=61 @ 201.41:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'AnticipYield';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'AnchorTenants';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'AnchorSalesPerSqFt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 1535.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'Amortization';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEconomics', @level2type = N'COLUMN', @level2name = N'AddedBy';




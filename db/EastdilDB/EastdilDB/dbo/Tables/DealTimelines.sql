﻿CREATE TABLE [dbo].[DealTimelines] (
    [DealTimelineID]            BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealID]                    BIGINT           NULL,
    [DateAdded]                 DATETIME         NULL,
    [DatePitchReady]            DATETIME         NULL,
    [DatePitchDue]              DATETIME         NULL,
    [DateKickoff]               DATETIME         NULL,
    [IMINHOUSE]                 DATETIME         NULL,
    [DateFinancingLtrSent]      DATETIME         NULL,
    [DateEngaged]               DATETIME         NULL,
    [DateEngExpires]            DATETIME         NULL,
    [DateIMReady]               DATETIME         NULL,
    [DateOMReady]               DATETIME         NULL,
    [DateToMarket]              DATETIME         NULL,
    [DateWarroomAccessible]     DATETIME         NULL,
    [DateCallForOffersMemoSent] DATETIME         NULL,
    [DateOffersDue]             DATETIME         NULL,
    [DateBestAndFinal]          DATETIME         NULL,
    [DateUnderContract]         DATETIME         NULL,
    [DateNonRefundable]         DATETIME         NULL,
    [DateCloseTgt]              DATETIME         NULL,
    [DateCloseAct]              DATETIME         NULL,
    [DaysInPipeline]            INT              NULL,
    [DateAssigned]              DATETIME         NULL,
    [DateIMDigDraftTgt]         DATETIME         NULL,
    [DateIMDigDraftAct]         DATETIME         NULL,
    [DateIMDigFinalTgt]         DATETIME         NULL,
    [DateIMDigFinalAct]         DATETIME         NULL,
    [DateIMPrtDraftTgt]         DATETIME         NULL,
    [DateIMPrtDraftAct]         DATETIME         NULL,
    [DateIMPrtFinalTgt]         DATETIME         NULL,
    [DateIMPrtFinalAct]         DATETIME         NULL,
    [OMINHOUSE]                 DATETIME         NULL,
    [DateOMDigDraftTgt]         DATETIME         NULL,
    [DateOMDigDraftAct]         DATETIME         NULL,
    [DateOMDigFinalTgt]         DATETIME         NULL,
    [DateOMDigFinalAct]         DATETIME         NULL,
    [DateOMPrtDraftTgt]         DATETIME         NULL,
    [DateOMPrtDraftAct]         DATETIME         NULL,
    [DateOMPrtFinalTgt]         DATETIME         NULL,
    [DateOMPrtFinalAct]         DATETIME         NULL,
    [MktgComment]               NVARCHAR (1000)  NULL,
    [ProductionHoldUntil]       DATETIME         NULL,
    [MatlsComment]              NVARCHAR (1000)  NULL,
    [DateEstReceiptOfDocs]      DATETIME         NULL,
    [DateActReceiptOfDocs]      DATETIME         NULL,
    [DateSizingInfoReceived]    DATETIME         NULL,
    [DateSizingCompleted]       DATETIME         NULL,
    [DateToWeb]                 DATETIME         NULL,
    [LastUpdate]                DATETIME         CONSTRAINT [DF__DealTimel__LastU__114A936A] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]       UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_D43FCD70_1CE2_438A_B8A4_B187D17CBF50_757577737] DEFAULT (newid()) NOT NULL,
    [RowVersion]                ROWVERSION       NOT NULL,
    [AddedBy]                   NVARCHAR (100)   NULL,
    [UpdatedBy]                 NVARCHAR (100)   NULL,
    CONSTRAINT [PK_DealTimelines] PRIMARY KEY CLUSTERED ([DealTimelineID] ASC),
    CONSTRAINT [FK_DealTimelines_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'ProductionHoldUntil';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 12287.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'OMINHOUSE';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4573 @ 2.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'MktgComment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'MatlsComment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8076 @ 1.52:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 12287.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'IMINHOUSE';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DealTimelineID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1969 @ 6.24:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DaysInPipeline';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateWarroomAccessible';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateUnderContract';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=20 @ 614.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateToWeb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1137 @ 10.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateToMarket';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateSizingInfoReceived';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateSizingCompleted';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=13 @ 945.15:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DatePitchReady';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=24 @ 511.96:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DatePitchDue';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=59 @ 208.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateOMReady';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=59 @ 208.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateOMPrtFinalTgt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=275 @ 44.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateOMPrtFinalAct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=48 @ 255.98:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateOMPrtDraftTgt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateOMPrtDraftAct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=41 @ 299.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateOMDigFinalTgt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=59 @ 208.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateOMDigFinalAct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=65 @ 189.03:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateOMDigDraftTgt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateOMDigDraftAct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=106 @ 115.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateOffersDue';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateNonRefundable';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 1365.22:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateKickoff';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=23 @ 534.22:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateIMReady';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=23 @ 534.22:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateIMPrtFinalTgt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=109 @ 112.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateIMPrtFinalAct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=30 @ 409.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateIMPrtDraftTgt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateIMPrtDraftAct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 361.38:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateIMDigFinalTgt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateIMDigFinalAct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=56 @ 219.41:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateIMDigDraftTgt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=16 @ 767.94:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateIMDigDraftAct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateFinancingLtrSent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateEstReceiptOfDocs';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=193 @ 63.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateEngExpires';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=540 @ 22.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateEngaged';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=167 @ 73.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateCloseTgt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=686 @ 17.91:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateCloseAct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateCallForOffersMemoSent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateBestAndFinal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 2457.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateAssigned';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=11596 @ 1.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'DateActReceiptOfDocs';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTimelines', @level2type = N'COLUMN', @level2name = N'AddedBy';




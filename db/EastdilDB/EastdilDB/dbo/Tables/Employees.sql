﻿CREATE TABLE [dbo].[Employees] (
    [EmployeeID]                BIGINT           IDENTITY (1, 1) NOT NULL,
    [EmployeeIDNum]             INT              NULL,
    [EmployeeName]              NVARCHAR (100)   NULL,
    [EmpInitials]               NVARCHAR (4)     NULL,
    [UserName]                  NVARCHAR (100)   NULL,
    [UserType]                  NVARCHAR (50)    NULL,
    [Password]                  NVARCHAR (50)    NULL,
    [ReleaseStampInUse]         DATETIME         NULL,
    [LastLogin]                 DATETIME         NULL,
    [EmpType]                   NVARCHAR (50)    NULL,
    [Department]                NVARCHAR (50)    NULL,
    [SeniorManagement]          BIT              NULL,
    [RegisteredRep]             BIT              NULL,
    [Licensed]                  BIT              NULL,
    [Series24]                  BIT              NULL,
    [FinraAllStates]            BIT              NULL,
    [FSALicensed]               BIT              NULL,
    [FDICClearance]             BIT              NULL,
    [CompCalc]                  BIT              NULL,
    [DRENumber]                 NVARCHAR (50)    NULL,
    [SupervisoryPrincipal]      NVARCHAR (50)    NULL,
    [BetaTester]                BIT              NULL,
    [REIBAdministrator]         BIT              NULL,
    [ITAdministrator]           BIT              NULL,
    [HRAdministrator]           BIT              NULL,
    [ListAdministrator]         BIT              NULL,
    [DealCoordinator]           BIT              NULL,
    [DealFeeAdministrator]      BIT              NULL,
    [DealExpenseAdministrator]  BIT              NULL,
    [LSPAdministrator]          BIT              NULL,
    [ResearchAdministrator]     BIT              NULL,
    [TimecardSupervisor]        BIT              NULL,
    [PrintRequestAdministrator] BIT              NULL,
    [LegalDept]                 BIT              NULL,
    [RetailTeam]                BIT              NULL,
    [ManagingDirectorAndUp]     BIT              NULL,
    [WachoviaCoverage]          BIT              NULL,
    [IronMtAdmin]               BIT              NULL,
    [CompanyID]                 INT              NULL,
    [CompanyName]               NVARCHAR (50)    NULL,
    [LegacyCompany]             NVARCHAR (50)    NULL,
    [ContractingCompanyID]      INT              CONSTRAINT [DF_Employees_ContractingCompanyID] DEFAULT ((4)) NULL,
    [OnDeals]                   BIT              NOT NULL,
    [ConfidentialDealAccess]    BIT              NULL,
    [Office]                    NVARCHAR (50)    NULL,
    [Active]                    BIT              NOT NULL,
    [LastName]                  NVARCHAR (20)    NOT NULL,
    [FirstName]                 NVARCHAR (15)    NOT NULL,
    [MiddleName]                NVARCHAR (10)    NULL,
    [NickName]                  NVARCHAR (50)    NULL,
    [LastnameFirstName]         NVARCHAR (100)   NULL,
    [Title]                     NVARCHAR (50)    NULL,
    [Extension]                 NVARCHAR (10)    NULL,
    [EMailAddress]              NVARCHAR (50)    NULL,
    [Notes]                     NVARCHAR (1000)  NULL,
    [AssistedBy]                NVARCHAR (50)    NULL,
    [Salutation]                NVARCHAR (25)    NULL,
    [StreetAddress]             NVARCHAR (50)    NULL,
    [Address2]                  NVARCHAR (50)    NULL,
    [City]                      NVARCHAR (25)    NULL,
    [State]                     NVARCHAR (50)    NULL,
    [Zip]                       NVARCHAR (10)    NULL,
    [IMAddress]                 NVARCHAR (50)    NULL,
    [Phone]                     NVARCHAR (50)    NULL,
    [MobilePhone]               NVARCHAR (50)    NULL,
    [WorkFax]                   NVARCHAR (50)    NULL,
    [HomeFax]                   NVARCHAR (50)    NULL,
    [OtherPhone]                NVARCHAR (50)    NULL,
    [WorkPhone]                 NVARCHAR (20)    NULL,
    [BusinessPhone]             NVARCHAR (20)    NULL,
    [CallingCardNo]             NVARCHAR (50)    NULL,
    [CallingCardIssued]         DATETIME         NULL,
    [HRAdminNote]               NVARCHAR (1000)  NULL,
    [EmploymentDate]            DATETIME         NULL,
    [SeparationDate]            DATETIME         NULL,
    [VoluntarySeparation]       NVARCHAR (50)    NULL,
    [Spouse]                    NVARCHAR (100)   NULL,
    [Birthday]                  DATETIME         NULL,
    [W4Status]                  NVARCHAR (50)    NULL,
    [SocialSecurity]            NVARCHAR (50)    NULL,
    [BaseSalary]                NVARCHAR (50)    NULL,
    [NonExempt]                 BIT              NULL,
    [JobClass]                  NVARCHAR (50)    NULL,
    [VacationHoursAllowed]      DECIMAL (18)     NULL,
    [VacationDaysAllowed]       DECIMAL (18)     NULL,
    [VacationHoursPriorYr]      NUMERIC (18)     NULL,
    [SickDaysAllowed]           INT              NULL,
    [VacationAgmt]              NVARCHAR (50)    NULL,
    [HealthIns]                 NVARCHAR (50)    NULL,
    [LifeIns]                   NVARCHAR (50)    NULL,
    [DependentCvrge]            NVARCHAR (50)    NULL,
    [EditBy]                    NVARCHAR (50)    NULL,
    [AutoRetrieveMktgList]      BIT              NULL,
    [AutoRetrieveOnBatchOps]    BIT              NULL,
    [LastUpdate]                DATETIME         NULL,
    [msrepl_tran_version]       UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_4C7A2384_9883_4E00_B022_2D8F58DAD09E_373576369] DEFAULT (newid()) NOT NULL,
    [RowVersion]                ROWVERSION       NOT NULL,
    [DateAdded]                 DATETIME         NULL,
    [AddedBy]                   NVARCHAR (100)   NULL,
    [UpdatedBy]                 NVARCHAR (100)   NULL,
    [WFLogonID]                 NVARCHAR (100)   NULL,
    [UsernameWF]                NVARCHAR (50)    NULL,
    [PollQ1]                    NVARCHAR (200)   NULL,
    [PollQ2]                    NVARCHAR (200)   NULL,
    CONSTRAINT [PK_Employees_1] PRIMARY KEY CLUSTERED ([EmployeeID] ASC)
);








GO
CREATE NONCLUSTERED INDEX [IX_EMPLOYEE_CUSTOM]
    ON [dbo].[Employees]([EmployeeID] ASC, [JobClass] ASC, [EmpInitials] ASC, [Active] ASC);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=479 @ 2.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Zip';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=741 @ 1.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'WorkPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=60 @ 22.23:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'WorkFax';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'WFLogonID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'WachoviaCoverage';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'W4Status';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 266.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'VoluntarySeparation';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=27 @ 49.41:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'VacationHoursPriorYr';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=22 @ 60.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'VacationHoursAllowed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=13 @ 102.62:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'VacationDaysAllowed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'VacationAgmt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 333.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'UserType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'UsernameWF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1272 @ 1.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'UserName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=133 @ 10.03:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Title';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'TimecardSupervisor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 222.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'SupervisoryPrincipal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=982 @ 1.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'StreetAddress';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=35 @ 38.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'State';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=196 @ 6.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Spouse';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'SocialSecurity';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 444.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'SickDaysAllowed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Series24';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=528 @ 2.53:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'SeparationDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'SeniorManagement';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 333.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Salutation';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'RetailTeam';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'ResearchAdministrator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 148.22:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'ReleaseStampInUse';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'REIBAdministrator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'RegisteredRep';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 1334.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'PrintRequestAdministrator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 333.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'PollQ2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 266.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'PollQ1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=566 @ 2.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Phone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=135 @ 9.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Password';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'OtherPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'OnDeals';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=22 @ 60.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Office';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=98 @ 13.61:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Notes';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'NonExempt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=183 @ 7.29:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'NickName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1329 @ 1.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=692 @ 1.93:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'MobilePhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=102 @ 13.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'MiddleName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'ManagingDirectorAndUp';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'LSPAdministrator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'ListAdministrator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'LifeIns';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Licensed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'LegalDept';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 333.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'LegacyCompany';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1267 @ 1.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1328 @ 1.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'LastnameFirstName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1133 @ 1.18:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'LastName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=71 @ 18.79:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'LastLogin';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=16 @ 83.38:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'JobClass';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'ITAdministrator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'IronMtAdmin';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'IMAddress';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=28 @ 47.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'HRAdminNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'HRAdministrator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'HomeFax';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'HealthIns';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'FSALicensed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=705 @ 1.89:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'FirstName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'FinraAllStates';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'FDICClearance';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=696 @ 1.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Extension';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5 @ 266.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'EmpType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=768 @ 1.74:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'EmploymentDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1331 @ 1.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'EmployeeName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=908 @ 1.47:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'EmployeeIDNum';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1311 @ 1.02:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'EmpInitials';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1209 @ 1.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'EMailAddress';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12 @ 111.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'EditBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'DRENumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'DependentCvrge';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 39.24:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Department';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'DealFeeAdministrator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'DealExpenseAdministrator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'DealCoordinator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'ContractingCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'ConfidentialDealAccess';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'CompCalc';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 444.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'CompanyName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 148.22:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=301 @ 4.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'City';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'CallingCardNo';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10 @ 133.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'CallingCardIssued';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=125 @ 10.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'BusinessPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=826 @ 1.62:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Birthday';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'BetaTester';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 1334.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'BaseSalary';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'AutoRetrieveOnBatchOps';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'AutoRetrieveMktgList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=97 @ 13.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'AssistedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=47 @ 28.38:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Address2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 667.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Employees', @level2type = N'COLUMN', @level2name = N'Active';




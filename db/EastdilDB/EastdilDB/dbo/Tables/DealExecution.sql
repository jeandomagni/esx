﻿CREATE TABLE [dbo].[DealExecution] (
    [DealExecutionID]                BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealID]                         BIGINT           NULL,
    [ApplyCAOMFilter]                BIT              DEFAULT ((0)) NULL,
    [ApplyUnassignedWithdrawnFilter] BIT              DEFAULT ((0)) NULL,
    [PitchNeeded]                    BIT              DEFAULT ((0)) NULL,
    [PitchMatls]                     BIT              DEFAULT ((0)) NULL,
    [DealWebSite]                    NVARCHAR (250)   NULL,
    [DealWebSiteUsername]            NVARCHAR (50)    NULL,
    [DealWebSitePassword]            NVARCHAR (50)    NULL,
    [OMSite]                         NVARCHAR (250)   NULL,
    [OMSiteUsername]                 NVARCHAR (50)    NULL,
    [OMSitePassword]                 NVARCHAR (50)    NULL,
    [WarRoomSite]                    NVARCHAR (250)   NULL,
    [WarRoomSiteUsername]            NVARCHAR (50)    NULL,
    [WarRoomSitePassword]            NVARCHAR (50)    NULL,
    [Event1]                         NVARCHAR (50)    NULL,
    [Event2]                         NVARCHAR (50)    NULL,
    [Event3]                         NVARCHAR (50)    NULL,
    [Event4]                         NVARCHAR (50)    NULL,
    [Event5]                         NVARCHAR (50)    NULL,
    [Event6]                         NVARCHAR (50)    NULL,
    [Event7]                         NVARCHAR (50)    NULL,
    [Event8]                         NVARCHAR (50)    NULL,
    [Event9]                         NVARCHAR (50)    NULL,
    [Event10]                        NVARCHAR (50)    NULL,
    [Event11]                        NVARCHAR (50)    NULL,
    [Event12]                        NVARCHAR (50)    NULL,
    [Event1Target]                   DATETIME         NULL,
    [Event2Target]                   DATETIME         NULL,
    [Event3Target]                   DATETIME         NULL,
    [Event4Target]                   DATETIME         NULL,
    [Event5Target]                   DATETIME         NULL,
    [Event6Target]                   DATETIME         NULL,
    [Event7Target]                   DATETIME         NULL,
    [Event8Target]                   DATETIME         NULL,
    [Event9Target]                   DATETIME         NULL,
    [Event10Target]                  DATETIME         NULL,
    [Event11Target]                  DATETIME         NULL,
    [Event12Target]                  DATETIME         NULL,
    [LastUpdate]                     DATETIME         DEFAULT (getdate()) NULL,
    [msrepl_tran_version]            UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_169A7ED7_6D41_434A_93C7_54B3007DC7D1_821577965] DEFAULT (newid()) NOT NULL,
    [RowVersion]                     ROWVERSION       NOT NULL,
    [DateAdded]                      DATETIME         NULL,
    [AddedBy]                        NVARCHAR (100)   NULL,
    [UpdatedBy]                      NVARCHAR (100)   NULL,
    CONSTRAINT [PK_DealExecution] PRIMARY KEY CLUSTERED ([DealExecutionID] ASC),
    CONSTRAINT [FK_DealExecution_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=287 @ 42.74:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'WarRoomSiteUsername';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=258 @ 47.54:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'WarRoomSitePassword';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=292 @ 42.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'WarRoomSite';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6133.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'PitchNeeded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12266.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'PitchMatls';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=350 @ 35.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'OMSiteUsername';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=301 @ 40.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'OMSitePassword';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=356 @ 34.46:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'OMSite';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4552 @ 2.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=7858 @ 1.56:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event9Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event9';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event8Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event8';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event7Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event7';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event6Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event6';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event5Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event5';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event4Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event3Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event2Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event1Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event12Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event12';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event11Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event11';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event10Target';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event10';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'Event1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'DealWebSiteUsername';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'DealWebSitePassword';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'DealWebSite';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'DealExecutionID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12266.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'ApplyUnassignedWithdrawnFilter';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12266.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'ApplyCAOMFilter';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExecution', @level2type = N'COLUMN', @level2name = N'AddedBy';




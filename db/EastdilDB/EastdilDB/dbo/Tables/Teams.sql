﻿CREATE TABLE [dbo].[Teams] (
    [TeamID]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [TeamName]          NVARCHAR (100) NULL,
    [TeamPurpose]       NVARCHAR (100) NULL,
    [DateAdded]         DATETIME       NULL,
    [AddedByEmployeeID] BIGINT         NULL,
    [LastUpdate]        DATETIME       NULL,
    [AddedBy]           NVARCHAR (100) NULL,
    [UpdatedBy]         NVARCHAR (100) NULL,
    CONSTRAINT [PK_EmployeeTeams] PRIMARY KEY CLUSTERED ([TeamID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Teams';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Teams';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Teams', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Teams', @level2type = N'COLUMN', @level2name = N'TeamPurpose';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Teams', @level2type = N'COLUMN', @level2name = N'TeamName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Teams', @level2type = N'COLUMN', @level2name = N'TeamID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Teams', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Teams', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Teams', @level2type = N'COLUMN', @level2name = N'AddedByEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Teams', @level2type = N'COLUMN', @level2name = N'AddedBy';




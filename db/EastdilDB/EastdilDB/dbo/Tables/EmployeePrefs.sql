﻿CREATE TABLE [dbo].[EmployeePrefs] (
    [EmployeeID]            BIGINT         NOT NULL,
    [PageSizeDealList]      INT            NULL,
    [PageSizeContactList]   INT            NULL,
    [PageSizeCompanyList]   INT            NULL,
    [PageSizeMarketingList] INT            NULL,
    [LastCalendarID]        BIGINT         NULL,
    [DateAdded]             DATETIME       NULL,
    [RowVersion]            ROWVERSION     NULL,
    [LastUpdate]            DATETIME       NULL,
    [AddedBy]               NVARCHAR (100) NULL,
    [UpdatedBy]             NVARCHAR (100) NULL,
    CONSTRAINT [PK_EmployeePrefs] PRIMARY KEY CLUSTERED ([EmployeeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=Used in ESNet not ESP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=24 @ 16.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'PageSizeMarketingList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=26 @ 14.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'PageSizeDealList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=24 @ 16.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'PageSizeContactList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=14 @ 27.71:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'PageSizeCompanyList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 64.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'LastCalendarID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=380 @ 1.02:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePrefs', @level2type = N'COLUMN', @level2name = N'AddedBy';




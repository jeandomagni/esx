﻿CREATE TABLE [dbo].[CompanyLoanBorrowers] (
    [CompanyLoanBorrowerID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [CompanyID]             BIGINT         NULL,
    [LoanID]                BIGINT         NULL,
    [OriginationDate]       DATETIME       NULL,
    [MaturityDate]          DATETIME       NULL,
    [DateAdded]             DATETIME       NULL,
    [LastUpdate]            DATETIME       NULL,
    [RowVersion]            ROWVERSION     NULL,
    [AddedBy]               NVARCHAR (100) NULL,
    [UpdatedBy]             NVARCHAR (100) NULL,
    CONSTRAINT [PK_CompanyLoanBorrowers] PRIMARY KEY CLUSTERED ([CompanyLoanBorrowerID] ASC),
    CONSTRAINT [FK_CompanyLoanBorrowers_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_CompanyLoanBorrowers_Loans] FOREIGN KEY ([LoanID]) REFERENCES [dbo].[Loans] ([LoanID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=Partially implemented and utilized', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=181 @ 1.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers', @level2type = N'COLUMN', @level2name = N'OriginationDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=199 @ 1.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers', @level2type = N'COLUMN', @level2name = N'MaturityDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers', @level2type = N'COLUMN', @level2name = N'LoanID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 211.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 211.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers', @level2type = N'COLUMN', @level2name = N'CompanyLoanBorrowerID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=150 @ 1.41:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyLoanBorrowers', @level2type = N'COLUMN', @level2name = N'AddedBy';




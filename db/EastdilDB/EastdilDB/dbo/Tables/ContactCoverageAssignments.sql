﻿CREATE TABLE [dbo].[ContactCoverageAssignments] (
    [ContactCoverageAssignmentID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [ContactID]                   BIGINT         NULL,
    [CompanyID]                   BIGINT         NULL,
    [EmployeeID]                  BIGINT         NULL,
    [CoverageRoleID]              INT            NULL,
    [LastUpdate]                  DATETIME       NULL,
    [RowVersion]                  ROWVERSION     NOT NULL,
    [DateAdded]                   DATETIME       NULL,
    [AddedBy]                     NVARCHAR (100) NULL,
    [UpdatedBy]                   NVARCHAR (100) NULL,
    CONSTRAINT [PK_ContactCoverageRoles] PRIMARY KEY CLUSTERED ([ContactCoverageAssignmentID] ASC),
    CONSTRAINT [FK_ContactCoverageRoles_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contacts] ([ContactID]),
    CONSTRAINT [FK_ContactCoverageRoles_CoverageRoles] FOREIGN KEY ([CoverageRoleID]) REFERENCES [dbo].[CoverageRoles] ([CoverageRoleID]),
    CONSTRAINT [FK_ContactCoverageRoles_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCoverageAssignments';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCoverageAssignments';


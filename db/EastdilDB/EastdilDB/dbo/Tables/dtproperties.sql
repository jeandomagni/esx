﻿CREATE TABLE [dbo].[dtproperties] (
    [id]                  INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [objectid]            INT              NULL,
    [property]            VARCHAR (64)     NOT NULL,
    [value]               VARCHAR (255)    NULL,
    [uvalue]              NVARCHAR (255)   NULL,
    [lvalue]              IMAGE            NULL,
    [version]             INT              DEFAULT ((0)) NOT NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_1FDAFBED_CC15_47CB_B1AD_EACB4D90C01E_1778105375] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [DateAdded]           DATETIME         NULL,
    [LastUpdate]          DATETIME         NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [pk_dtproperties] PRIMARY KEY CLUSTERED ([id] ASC, [property] ASC)
);




GO
GRANT UPDATE
    ON OBJECT::[dbo].[dtproperties] TO PUBLIC
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[dtproperties] TO PUBLIC
    AS [dbo];


GO
GRANT REFERENCES
    ON OBJECT::[dbo].[dtproperties] TO PUBLIC
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[dbo].[dtproperties] TO PUBLIC
    AS [dbo];


GO
GRANT DELETE
    ON OBJECT::[dbo].[dtproperties] TO PUBLIC
    AS [dbo];


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This was not on the spreadsheet but Tim Goodman sait it was not used in a phone conversation on 09/30', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'dtproperties';


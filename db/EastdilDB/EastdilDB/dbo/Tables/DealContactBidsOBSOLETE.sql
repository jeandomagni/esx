﻿CREATE TABLE [dbo].[DealContactBidsOBSOLETE] (
    [DealContactBidID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealContactID]              BIGINT         NULL,
    [Submitted]                  BIT            NULL,
    [SubmittedOn]                DATETIME       NULL,
    [GeneralNote]                NVARCHAR (500) NULL,
    [Rank]                       INT            NULL,
    [esBidAmount]                MONEY          NULL,
    [esDepositNote]              NVARCHAR (500) NULL,
    [esDDPeriodNote]             NVARCHAR (500) NULL,
    [esPotentialDealESFinancing] BIT            NULL,
    [finLoanAmount]              NCHAR (10)     NULL,
    [finLoanTypeID]              NCHAR (10)     NULL,
    [finLoanInterestRate]        FLOAT (53)     NULL,
    [finLoanTermYears]           INT            NULL,
    [finReferenceIndexID]        INT            NULL,
    [finSpread]                  FLOAT (53)     NULL,
    [finPrepaymentPenaltyNote]   NVARCHAR (500) NULL,
    [finCapRate]                 FLOAT (53)     NULL,
    [finPricePerSF]              MONEY          NULL,
    [finEquityCapitalNote]       NVARCHAR (500) NULL,
    CONSTRAINT [PK_DealContactBids] PRIMARY KEY CLUSTERED ([DealContactBidID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Added late last year when team was trying to define how to track Bids', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBidsOBSOLETE';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBidsOBSOLETE';


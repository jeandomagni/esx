﻿CREATE TABLE [dbo].[DealContactBidFinancingImport] (
    [DealContactID]             NVARCHAR (50)   NULL,
    [FirstnameLastname]         NVARCHAR (100)  NULL,
    [CompanyName]               NVARCHAR (100)  NULL,
    [MarketingStatus]           NVARCHAR (50)   NULL,
    [Tour]                      NVARCHAR (50)   NULL,
    [BidDate]                   NVARCHAR (50)   NULL,
    [BidRank]                   NVARCHAR (50)   NULL,
    [BidComments]               NVARCHAR (1000) NULL,
    [OfferNumber]               NVARCHAR (50)   NULL,
    [Lender]                    NVARCHAR (200)  NULL,
    [LenderType]                NVARCHAR (50)   NULL,
    [InitialLoan]               NVARCHAR (50)   NULL,
    [TotalLoanCALC]             NVARCHAR (50)   NULL,
    [ReferenceIndex]            NVARCHAR (50)   NULL,
    [Spread]                    NVARCHAR (50)   NULL,
    [RateFloor]                 NVARCHAR (50)   NULL,
    [AllInRate]                 NVARCHAR (50)   NULL,
    [IncrementalCostOfDebt]     NVARCHAR (50)   NULL,
    [LoanPerSFUnitCALC]         NVARCHAR (50)   NULL,
    [LTC_LTV]                   NVARCHAR (50)   NULL,
    [ClosingTests]              NVARCHAR (50)   NULL,
    [ExtensionTests]            NVARCHAR (50)   NULL,
    [LoanTerm]                  NVARCHAR (50)   NULL,
    [EarnoutHoldback]           NVARCHAR (50)   NULL,
    [TI_LC_Holdback]            NVARCHAR (50)   NULL,
    [DueDiligencePeriodDays]    NVARCHAR (50)   NULL,
    [ClosingPeriodDays]         NVARCHAR (50)   NULL,
    [InPlaceDebtYieldCALC]      NVARCHAR (50)   NULL,
    [Year1DebtYieldCALC]        NVARCHAR (50)   NULL,
    [StabilizedDebtYieldCALC]   NVARCHAR (50)   NULL,
    [LoanType]                  NVARCHAR (50)   NULL,
    [OriginationFee]            NVARCHAR (50)   NULL,
    [InterestOnlyPeriodMonths]  NVARCHAR (50)   NULL,
    [AmortizationMonths]        NVARCHAR (50)   NULL,
    [Prepayment]                NVARCHAR (50)   NULL,
    [LockOutYears]              NVARCHAR (50)   NULL,
    [OpenPeriodYears]           NVARCHAR (50)   NULL,
    [Recourse]                  NVARCHAR (50)   NULL,
    [UpfrontReserves]           NVARCHAR (50)   NULL,
    [OngoingReserves]           NVARCHAR (50)   NULL,
    [CashManagementTest]        NVARCHAR (50)   NULL,
    [TotalValueOrPurchasePrice] NVARCHAR (50)   NULL,
    [ProjectSize]               NVARCHAR (50)   NULL,
    [ProjectSizeType]           NVARCHAR (50)   NULL,
    [DealID]                    NVARCHAR (50)   NULL,
    [DealCode]                  NVARCHAR (50)   NULL,
    [DealName]                  NVARCHAR (50)   NULL,
    [LocationCity]              NVARCHAR (50)   NULL,
    [LocationState]             NVARCHAR (50)   NULL,
    [PropertyType]              NVARCHAR (50)   NULL,
    [PctLeased]                 NVARCHAR (50)   NULL,
    [FinancingType]             NVARCHAR (50)   NULL,
    [UrbanOrSuburban]           NVARCHAR (50)   NULL,
    [StabilizedOrNonstabilized] NVARCHAR (50)   NULL,
    [NOI]                       NVARCHAR (50)   NULL,
    [NOIFirstYear]              NVARCHAR (50)   NULL,
    [NOIStabilized]             NVARCHAR (50)   NULL
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Added late last year when team was trying to define how to track Bids', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBidFinancingImport';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBidFinancingImport';


﻿CREATE TABLE [dbo].[PageLog] (
    [PageLogID]      BIGINT          IDENTITY (1, 1) NOT NULL,
    [EmployeeID]     BIGINT          NULL,
    [EmployeeName]   NVARCHAR (100)  NULL,
    [Title]          NVARCHAR (50)   NULL,
    [Office]         NVARCHAR (50)   NULL,
    [PageName]       NVARCHAR (1000) NULL,
    [Controller]     NVARCHAR (50)   NULL,
    [Action]         NVARCHAR (50)   NULL,
    [QueryStrings]   NVARCHAR (1000) NULL,
    [ServerName]     NVARCHAR (50)   NULL,
    [AccessDatetime] DATETIME        NULL,
    [Mode]           NVARCHAR (50)   NULL,
    [DealID]         BIGINT          NULL,
    [ContactID]      BIGINT          NULL,
    [CompanyID]      BIGINT          NULL,
    [DealContactID]  BIGINT          NULL,
    [CommentID]      BIGINT          NULL,
    [DateAdded]      DATETIME        NULL,
    [LastUpdate]     DATETIME        NULL,
    [AddedBy]        NVARCHAR (100)  NULL,
    [UpdatedBy]      NVARCHAR (100)  NULL,
    CONSTRAINT [PK_PageLog] PRIMARY KEY CLUSTERED ([PageLogID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Audit,Notes=Incoming requests, archived as separate DB on ESELADB01 server', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 40764.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'Title';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 61146.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'ServerName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=39125 @ 6.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'QueryStrings';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=18253 @ 13.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'PageName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'PageLogID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=15 @ 16305.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'Office';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'Mode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=470 @ 520.39:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'EmployeeName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=470 @ 520.39:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'DealContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=24 @ 10191.04:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'Controller';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'CommentID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12129 @ 20.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'Action';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=240978 @ 1.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PageLog', @level2type = N'COLUMN', @level2name = N'AccessDatetime';




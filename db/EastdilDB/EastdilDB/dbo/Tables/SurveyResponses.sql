﻿CREATE TABLE [dbo].[SurveyResponses] (
    [ResponseID]   INT             IDENTITY (1, 1) NOT NULL,
    [QuestionID]   INT             NULL,
    [EmployeeID]   INT             NULL,
    [ResponseDate] DATETIME        NULL,
    [Response]     NVARCHAR (1000) NULL
);


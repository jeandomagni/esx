﻿CREATE TABLE [dbo].[REIBProperties] (
    [REIBPropertyID]    BIGINT          IDENTITY (1, 1) NOT NULL,
    [CompanyID]         BIGINT          NULL,
    [PropertyTypeID]    INT             NULL,
    [NumProperties]     INT             NULL,
    [NumUnits]          INT             NULL,
    [Occupancy]         DECIMAL (18, 3) NULL,
    [PropertyValue]     MONEY           NULL,
    [PropertyFinancing] MONEY           NULL,
    [LastUpdate]        DATETIME        NULL,
    [RowVersion]        ROWVERSION      NOT NULL,
    [DateAdded]         DATETIME        NULL,
    [AddedBy]           NVARCHAR (100)  NULL,
    [UpdatedBy]         NVARCHAR (100)  NULL,
    CONSTRAINT [PK_REIBCompanyProperties] PRIMARY KEY CLUSTERED ([REIBPropertyID] ASC),
    CONSTRAINT [FK_REIBCompanyProperties_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_REIBCompanyProperties_PropertyTypes] FOREIGN KEY ([PropertyTypeID]) REFERENCES [dbo].[PropertyTypes] ([PropertyTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'REIBPropertyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'PropertyValue';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'PropertyTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'PropertyFinancing';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'Occupancy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'NumUnits';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'NumProperties';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBProperties', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[EmployeeCourses] (
    [CourseID]          INT            IDENTITY (1, 1) NOT NULL,
    [CourseName]        NVARCHAR (50)  NULL,
    [CourseDescription] NVARCHAR (50)  NULL,
    [CourseDueDate]     DATETIME       NULL,
    [CourseAvailable]   DATETIME       NULL,
    [CourseFileName]    NVARCHAR (50)  NULL,
    [DateAdded]         DATETIME       NULL,
    [LastUpdate]        DATETIME       NULL,
    [AddedBy]           NVARCHAR (100) NULL,
    [UpdatedBy]         NVARCHAR (100) NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=Used in ESNet not ESP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=22 @ 1.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses', @level2type = N'COLUMN', @level2name = N'CourseName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses', @level2type = N'COLUMN', @level2name = N'CourseID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=22 @ 1.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses', @level2type = N'COLUMN', @level2name = N'CourseFileName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=12 @ 1.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses', @level2type = N'COLUMN', @level2name = N'CourseDueDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=22 @ 1.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses', @level2type = N'COLUMN', @level2name = N'CourseDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=11 @ 2.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses', @level2type = N'COLUMN', @level2name = N'CourseAvailable';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeCourses', @level2type = N'COLUMN', @level2name = N'AddedBy';




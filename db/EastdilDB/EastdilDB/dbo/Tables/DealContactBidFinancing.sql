﻿CREATE TABLE [dbo].[DealContactBidFinancing] (
    [DealContactBidFinancingID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [DealContactID]             BIGINT          NULL,
    [DealID]                    BIGINT          NULL,
    [BidDate]                   DATETIME        NULL,
    [BidRank]                   INT             NULL,
    [BidComments]               NVARCHAR (1000) NULL,
    [OfferNumber]               INT             NULL,
    [Lender]                    NVARCHAR (200)  NULL,
    [LenderType]                NVARCHAR (50)   NULL,
    [InitialLoan]               MONEY           NULL,
    [TotalLoanCALC]             MONEY           NULL,
    [ReferenceIndex]            NVARCHAR (50)   NULL,
    [Spread]                    FLOAT (53)      NULL,
    [RateFloor]                 FLOAT (53)      NULL,
    [AllInRate]                 FLOAT (53)      NULL,
    [IncrementalCostOfDebt]     FLOAT (53)      NULL,
    [LoanPerSFUnitCALC]         MONEY           NULL,
    [LTC_LTV]                   FLOAT (53)      NULL,
    [ClosingTests]              NVARCHAR (50)   NULL,
    [ExtensionTests]            NVARCHAR (50)   NULL,
    [LoanTerm]                  NVARCHAR (50)   NULL,
    [EarnoutHoldback]           NVARCHAR (50)   NULL,
    [TI_LC_Holdback]            MONEY           NULL,
    [DueDiligencePeriodDays]    INT             NULL,
    [ClosingPeriodDays]         INT             NULL,
    [InPlaceDebtYieldCALC]      FLOAT (53)      NULL,
    [Year1DebtYieldCALC]        FLOAT (53)      NULL,
    [StabilizedDebtYieldCALC]   FLOAT (53)      NULL,
    [LoanType]                  NVARCHAR (50)   NULL,
    [OriginationFee]            FLOAT (53)      NULL,
    [InterestOnlyPeriodMonths]  INT             NULL,
    [AmortizationMonths]        INT             NULL,
    [Prepayment]                NVARCHAR (50)   NULL,
    [LockOutYears]              INT             NULL,
    [OpenPeriodYears]           INT             NULL,
    [Recourse]                  NVARCHAR (50)   NULL,
    [UpfrontReserves]           NVARCHAR (50)   NULL,
    [OngoingReserves]           NVARCHAR (50)   NULL,
    [CashManagementTest]        NVARCHAR (50)   NULL,
    [TotalValueOrPurchasePrice] MONEY           NULL,
    CONSTRAINT [PK_DealContactBidFinancing] PRIMARY KEY CLUSTERED ([DealContactBidFinancingID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Added late last year when team was trying to define how to track Bids', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBidFinancing';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBidFinancing';


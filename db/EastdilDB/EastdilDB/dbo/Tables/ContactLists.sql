﻿CREATE TABLE [dbo].[ContactLists] (
    [ContactListID]         INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ListName]              NVARCHAR (64)    NULL,
    [ListManagerEmployeeID] BIGINT           NULL,
    [ListTypeID]            INT              NULL,
    [Author]                VARCHAR (50)     NULL,
    [Comments]              NVARCHAR (1000)  NULL,
    [EventDate]             DATETIME         NULL,
    [EventLocation]         NVARCHAR (1000)  NULL,
    [DealID]                BIGINT           NULL,
    [Field1]                NVARCHAR (50)    NULL,
    [Field2]                NVARCHAR (50)    NULL,
    [Field3]                NVARCHAR (50)    NULL,
    [Field4]                NVARCHAR (50)    NULL,
    [Field5]                NVARCHAR (50)    NULL,
    [Field6]                NVARCHAR (50)    NULL,
    [Field7]                NVARCHAR (50)    NULL,
    [Field8]                NVARCHAR (50)    NULL,
    [Active]                BIT              CONSTRAINT [DF__ContactLi__Activ__4BF72343] DEFAULT ((0)) NULL,
    [NumActiveContacts]     INT              NULL,
    [NumContacts]           INT              NULL,
    [LastUpdate]            DATETIME         CONSTRAINT [DF__ContactLi__LastU__4CEB477C] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]   UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_5721A1C3_59F6_4984_9A2D_5A4F5B409ACE_1173579219] DEFAULT (newid()) NOT NULL,
    [RowVersion]            ROWVERSION       NOT NULL,
    [DateAdded]             DATETIME         NULL,
    [AddedBy]               NVARCHAR (100)   NULL,
    [UpdatedBy]             NVARCHAR (100)   NULL,
    CONSTRAINT [PK_ContactLists] PRIMARY KEY CLUSTERED ([ContactListID] ASC),
    CONSTRAINT [FK_ContactLists_Employees] FOREIGN KEY ([ListManagerEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_ContactLists_ListTypes] FOREIGN KEY ([ListTypeID]) REFERENCES [dbo].[ListTypes] ([ListTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=567 @ 2.49:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'NumContacts';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=536 @ 2.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'NumActiveContacts';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=505 @ 2.79:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8 @ 176.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'ListTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1284 @ 1.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'ListName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=212 @ 6.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'ListManagerEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=942 @ 1.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Field8';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Field7';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 156.56:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Field6';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Field5';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=22 @ 64.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Field4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=30 @ 46.97:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Field3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=46 @ 30.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Field2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=61 @ 23.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Field1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=291 @ 4.84:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'EventLocation';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=313 @ 4.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'EventDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'ContactListID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=111 @ 12.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Comments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=162 @ 8.70:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Author';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 704.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactLists', @level2type = N'COLUMN', @level2name = N'Active';




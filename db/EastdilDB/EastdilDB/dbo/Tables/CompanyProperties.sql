﻿CREATE TABLE [dbo].[CompanyProperties] (
    [CompanyPropertyID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [CompanyID]         BIGINT         NULL,
    [PropertyID]        BIGINT         NULL,
    [DateAcquired]      DATETIME       NULL,
    [DateDisposed]      DATETIME       NULL,
    [DateAdded]         DATETIME       NULL,
    [LastUpdate]        DATETIME       NULL,
    [RowVersion]        ROWVERSION     NULL,
    [AddedBy]           NVARCHAR (100) NULL,
    [UpdatedBy]         NVARCHAR (100) NULL,
    CONSTRAINT [PK_CompanyProperties] PRIMARY KEY CLUSTERED ([CompanyPropertyID] ASC),
    CONSTRAINT [FK_CompanyProperties_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_CompanyProperties_Properties] FOREIGN KEY ([PropertyID]) REFERENCES [dbo].[Properties] ([PropertyID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=Partially implemented and utilized', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=353 @ 1.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties', @level2type = N'COLUMN', @level2name = N'PropertyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 88.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties', @level2type = N'COLUMN', @level2name = N'DateDisposed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 88.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=216 @ 1.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties', @level2type = N'COLUMN', @level2name = N'DateAcquired';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties', @level2type = N'COLUMN', @level2name = N'CompanyPropertyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=278 @ 1.28:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyProperties', @level2type = N'COLUMN', @level2name = N'AddedBy';




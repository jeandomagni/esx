﻿CREATE TABLE [dbo].[REIBCoverageAssignments] (
    [REIBCoverageAssignmentID] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CompanyID]                BIGINT           NULL,
    [EmployeeID]               BIGINT           NULL,
    [REIBCoverageAreaID]       INT              NULL,
    [REIBContactLevelID]       INT              NULL,
    [msrepl_tran_version]      UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_DD626A07_0668_4946_B7F7_613B5FB477B4_1077578877] DEFAULT (newid()) NOT NULL,
    [RowVersion]               ROWVERSION       NOT NULL,
    [DateAdded]                DATETIME         NULL,
    [LastUpdate]               DATETIME         NULL,
    [AddedBy]                  NVARCHAR (100)   NULL,
    [UpdatedBy]                NVARCHAR (100)   NULL,
    CONSTRAINT [PK_CoverageAssignments] PRIMARY KEY CLUSTERED ([REIBCoverageAssignmentID] ASC),
    CONSTRAINT [FK_REIBCoverageAssignments_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_REIBCoverageAssignments_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_REIBCoverageAssignments_Employees1] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_REIBCoverageAssignments_REIBContactLevels] FOREIGN KEY ([REIBContactLevelID]) REFERENCES [dbo].[REIBContactLevels] ([REIBContactLevelID]),
    CONSTRAINT [FK_REIBCoverageAssignments_REIBCoverageAreas] FOREIGN KEY ([REIBCoverageAreaID]) REFERENCES [dbo].[REIBCoverageAreas] ([REIBCoverageAreaID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'REIBCoverageAssignmentID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=10 @ 79.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'REIBCoverageAreaID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 264.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'REIBContactLevelID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=361 @ 2.19:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=80 @ 9.90:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=110 @ 7.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAssignments', @level2type = N'COLUMN', @level2name = N'AddedBy';




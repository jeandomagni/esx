﻿CREATE TABLE [dbo].[AuditLog] (
    [AuditLogID]          BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [UpdatedByEmployeeID] BIGINT           NULL,
    [PageUsed]            NVARCHAR (300)   NULL,
    [ContactID]           BIGINT           NULL,
    [CompanyID]           BIGINT           NULL,
    [DealID]              BIGINT           NULL,
    [DealContactID]       BIGINT           NULL,
    [CommentID]           BIGINT           NULL,
    [CriteriaEquityID]    BIGINT           NULL,
    [CriteriaFinancingID] BIGINT           NULL,
    [CriteriaDebtID]      BIGINT           NULL,
    [DealFeeID]           BIGINT           NULL,
    [Content]             VARCHAR (MAX)    NOT NULL,
    [DateAdded]           DATETIME         CONSTRAINT [DF_Audit_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_341A28D2_B53B_49AA_B6C1_A8646D01FA33_181575685] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [LastUpdate]          DATETIME         NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED ([AuditLogID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AuditLog';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AuditLog';


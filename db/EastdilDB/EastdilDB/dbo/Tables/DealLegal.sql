﻿CREATE TABLE [dbo].[DealLegal] (
    [DealLegalID]           BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealID]                BIGINT           NULL,
    [EngLtrSignedWith]      NVARCHAR (50)    NULL,
    [RetentionAgreement]    BIT              DEFAULT ((0)) NULL,
    [DateEngaged]           DATETIME         NULL,
    [DateEngExpired]        DATETIME         NULL,
    [EngagementTerm]        NVARCHAR (100)   NULL,
    [DualAgency]            BIT              DEFAULT ((0)) NULL,
    [ConsentsInFile]        BIT              DEFAULT ((0)) NULL,
    [OutOfState]            BIT              DEFAULT ((0)) NULL,
    [LocalBrokerInvolved]   BIT              DEFAULT ((0)) NULL,
    [LocalBrokerEngaged]    BIT              DEFAULT ((0)) NULL,
    [EngagedEntity]         NVARCHAR (50)    NULL,
    [EngageLtrStatus]       NVARCHAR (50)    NULL,
    [EngLtrReminderFlag]    BIT              DEFAULT ((0)) NULL,
    [FullyExecuted]         BIT              DEFAULT ((0)) NULL,
    [OutForSignature]       BIT              DEFAULT ((0)) NULL,
    [DraftedAndNegotiating] BIT              DEFAULT ((0)) NULL,
    [NotDrafted]            BIT              DEFAULT ((0)) NULL,
    [ProspectListTiming]    NVARCHAR (100)   NULL,
    [NewBusMktRestriction]  BIT              DEFAULT ((0)) NULL,
    [InsuranceRequirement]  BIT              DEFAULT ((0)) NULL,
    [Encumbered]            NVARCHAR (50)    NULL,
    [DateAdded]             DATETIME         NULL,
    [LastUpdate]            DATETIME         DEFAULT (getdate()) NULL,
    [msrepl_tran_version]   UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_A87A9812_44F1_472A_92FC_6E97F731ECD7_1013578649] DEFAULT (newid()) NOT NULL,
    [RowVersion]            ROWVERSION       NOT NULL,
    [AddedBy]               NVARCHAR (100)   NULL,
    [UpdatedBy]             NVARCHAR (100)   NULL,
    CONSTRAINT [PK_DealLegal] PRIMARY KEY CLUSTERED ([DealLegalID] ASC),
    CONSTRAINT [FK_DealLegal_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 6132.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'RetentionAgreement';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'ProspectListTiming';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12264.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'OutOfState';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12264.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'OutForSignature';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6132.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'NotDrafted';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12264.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'NewBusMktRestriction';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4552 @ 2.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12264.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'LocalBrokerInvolved';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12264.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'LocalBrokerEngaged';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=7812 @ 1.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6132.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'InsuranceRequirement';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12264.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'FullyExecuted';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'EngLtrSignedWith';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6132.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'EngLtrReminderFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=37 @ 331.46:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'EngagementTerm';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4088.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'EngageLtrStatus';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 3066.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'EngagedEntity';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4088.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'Encumbered';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6132.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'DualAgency';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12264.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'DraftedAndNegotiating';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'DealLegalID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=192 @ 63.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'DateEngExpired';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=538 @ 22.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'DateEngaged';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6132.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'ConsentsInFile';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLegal', @level2type = N'COLUMN', @level2name = N'AddedBy';




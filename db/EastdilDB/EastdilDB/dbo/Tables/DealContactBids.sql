﻿CREATE TABLE [dbo].[DealContactBids] (
    [DealContactBidID]         BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealContactID]            BIGINT         NULL,
    [BidOffer]                 DATETIME       NULL,
    [BidOfferAmt]              BIGINT         NULL,
    [BidRound]                 NVARCHAR (50)  NULL,
    [BidRank]                  INT            NULL,
    [BidSubmitted]             BIT            NULL,
    [BidDepositNote]           NVARCHAR (100) NULL,
    [BidDDPeriodNote]          NVARCHAR (100) NULL,
    [BidDebtFinancing]         BIT            NULL,
    [BidRate]                  FLOAT (53)     NULL,
    [BidSpread]                FLOAT (53)     NULL,
    [BidLoanTermYears]         INT            NULL,
    [BidPrepaymentPenaltyNote] NVARCHAR (100) NULL,
    [BidCapRate]               FLOAT (53)     NULL,
    [BidPricePSF]              MONEY          NULL,
    [BidEquityCapital]         NVARCHAR (150) NULL,
    [BidGeneralNote]           NVARCHAR (500) NULL,
    [BidOriginationFee]        BIGINT         NULL,
    [BidReferenceIndex]        NVARCHAR (50)  NULL,
    [BidLoanType]              NVARCHAR (50)  NULL,
    [Won]                      DATETIME       NULL,
    [JointVentureBid]          BIT            NULL,
    [BidIDNumber]              NVARCHAR (50)  NULL,
    [PercentAllocation]        FLOAT (53)     NULL,
    [JointVentureAmount]       MONEY          NULL,
    [Selected]                 BIT            NULL,
    [AddedBy]                  NVARCHAR (100) NULL,
    [UpdatedBy]                NVARCHAR (100) NULL,
    [DateAdded]                DATETIME       NULL,
    [LastUpdate]               DATETIME       NULL,
    [RowVersion]               ROWVERSION     NOT NULL,
    CONSTRAINT [PK_DealsContactBids] PRIMARY KEY CLUSTERED ([DealContactBidID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'Won';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'Selected';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=715 @ 3.99:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2718 @ 1.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'DealContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'DealContactBidID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=469 @ 6.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 2850.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidSubmitted';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=114 @ 25.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidSpread';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 950.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidRound';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 712.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidReferenceIndex';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=135 @ 21.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=39 @ 73.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidRank';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=276 @ 10.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidPricePSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=97 @ 29.38:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidPrepaymentPenaltyNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=96 @ 29.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidOriginationFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1038 @ 2.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidOfferAmt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=79 @ 36.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidOffer';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 407.14:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidLoanType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 356.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidLoanTermYears';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=296 @ 9.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidGeneralNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 2850.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidEquityCapital';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=14 @ 203.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidDepositNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1425.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidDebtFinancing';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1425.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidDDPeriodNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=116 @ 24.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'BidCapRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBids', @level2type = N'COLUMN', @level2name = N'AddedBy';




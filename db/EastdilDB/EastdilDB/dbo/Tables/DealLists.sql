﻿CREATE TABLE [dbo].[DealLists] (
    [DealListID]            INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ListName]              NVARCHAR (64)    NULL,
    [ListManagerEmployeeID] BIGINT           NULL,
    [Author]                VARCHAR (50)     NULL,
    [Comments]              NVARCHAR (1000)  NULL,
    [Active]                BIT              CONSTRAINT [DF__DealLists__Activ__73BA3083] DEFAULT ((0)) NULL,
    [LastUpdate]            DATETIME         CONSTRAINT [DF__DealLists__LastU__74AE54BC] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]   UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_AD56448F_BF38_47C3_86D5_C3D55C7948C8_421576540] DEFAULT (newid()) NOT NULL,
    [RowVersion]            ROWVERSION       NOT NULL,
    [DateAdded]             DATETIME         NULL,
    [AddedBy]               NVARCHAR (100)   NULL,
    [UpdatedBy]             NVARCHAR (100)   NULL,
    CONSTRAINT [PK_DealLists] PRIMARY KEY CLUSTERED ([DealListID] ASC),
    CONSTRAINT [FK_DealLists_Employees] FOREIGN KEY ([ListManagerEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLists';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLists';


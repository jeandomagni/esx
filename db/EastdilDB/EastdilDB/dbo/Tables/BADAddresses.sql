﻿CREATE TABLE [dbo].[BADAddresses] (
    [AddressID]           BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ADDRESSID_SLX]       CHAR (12)        NULL,
    [ENTITYID]            CHAR (12)        NULL,
    [Type]                NVARCHAR (100)   NULL,
    [Description]         NVARCHAR (100)   NULL,
    [Address1]            NVARCHAR (100)   NULL,
    [Address2]            NVARCHAR (100)   NULL,
    [City]                NVARCHAR (50)    NULL,
    [State]               NVARCHAR (50)    NULL,
    [PostalCode]          NVARCHAR (50)    NULL,
    [County]              NVARCHAR (50)    NULL,
    [Country]             NVARCHAR (50)    NULL,
    [CrossStreet]         NVARCHAR (150)   NULL,
    [IsPrimary]           BIT              CONSTRAINT [REN1DF__Addresses__IsPri__70DDC3D8] DEFAULT ((0)) NULL,
    [IsMailing]           BIT              CONSTRAINT [REN1DF__Addresses__IsMai__71D1E811] DEFAULT ((0)) NULL,
    [Salutation]          NVARCHAR (100)   NULL,
    [Routing]             NVARCHAR (100)   NULL,
    [Address3]            NVARCHAR (100)   NULL,
    [Address4]            NVARCHAR (100)   NULL,
    [Timezone]            NVARCHAR (100)   NULL,
    [Source]              NVARCHAR (50)    NULL,
    [Confidence]          SMALLINT         NULL,
    [Latitude]            FLOAT (53)       NULL,
    [Longitude]           FLOAT (53)       NULL,
    [BoundaryBaseHeight]  INT              NULL,
    [BoundaryMaxHeight]   INT              NULL,
    [Boundary1]           FLOAT (53)       NULL,
    [Boundary2]           FLOAT (53)       NULL,
    [Boundary3]           FLOAT (53)       NULL,
    [Boundary4]           FLOAT (53)       NULL,
    [DateAdded]           DATETIME         NULL,
    [LastUpdate]          DATETIME         CONSTRAINT [REN1DF__Addresses__LastU__72C60C4A] DEFAULT (getdate()) NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [REN1MSrepl_tran_version_default_0EA1EC0A_C5F0_4D15_A1A0_0F79C56F5D09_341576255] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL
);




GO
CREATE NONCLUSTERED INDEX [IX_ADDRESS_CUSTOM]
    ON [dbo].[BADAddresses]([AddressID] ASC)
    INCLUDE([ADDRESSID_SLX], [ENTITYID], [Type], [Description], [Address1], [Address2], [City], [State], [PostalCode], [County], [Country], [CrossStreet], [IsPrimary], [IsMailing], [Salutation], [Routing], [Address3], [Address4], [Timezone], [DateAdded], [LastUpdate]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BADAddresses';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BADAddresses';


﻿CREATE TABLE [dbo].[PitchLibrary] (
    [PitchID]              INT            IDENTITY (1, 1) NOT NULL,
    [CompanyID]            INT            NULL,
    [PresentedTo]          NVARCHAR (100) NULL,
    [ProductLine]          NVARCHAR (50)  NULL,
    [ProductType]          NVARCHAR (50)  NULL,
    [MD]                   NVARCHAR (50)  NULL,
    [Office]               NVARCHAR (50)  NULL,
    [ProjectName]          NVARCHAR (50)  NULL,
    [AssetLocation]        NVARCHAR (50)  NULL,
    [Portfolio]            BIT            NULL,
    [FileName]             NVARCHAR (100) NULL,
    [PitchDate]            DATETIME       NULL,
    [LeadPresenter]        NVARCHAR (50)  NULL,
    [AdditionalPresenter]  NVARCHAR (50)  NULL,
    [MaterialsCoordinator] NVARCHAR (50)  NULL,
    [MaterialsContributor] NVARCHAR (50)  NULL,
    [DateAdded]            DATETIME       NULL,
    [LastUpdate]           DATETIME       NULL,
    [AddedBy]              NVARCHAR (100) NULL,
    [UpdatedBy]            NVARCHAR (100) NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utility,Notes=Used in ESNet not ESP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=350 @ 1.95:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'ProjectName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 75.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'ProductType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5 @ 136.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'ProductLine';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=216 @ 3.16:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'PresentedTo';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 341.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'Portfolio';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'PitchID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=332 @ 2.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'PitchDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10 @ 68.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'Office';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=115 @ 5.93:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'MD';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=17 @ 40.12:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'MaterialsCoordinator';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=33 @ 20.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'MaterialsContributor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=35 @ 19.49:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'LeadPresenter';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=669 @ 1.02:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'FileName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=167 @ 4.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=123 @ 5.54:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'AssetLocation';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=18 @ 37.89:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'AdditionalPresenter';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PitchLibrary', @level2type = N'COLUMN', @level2name = N'AddedBy';




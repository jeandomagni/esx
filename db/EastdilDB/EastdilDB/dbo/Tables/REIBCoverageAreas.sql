﻿CREATE TABLE [dbo].[REIBCoverageAreas] (
    [REIBCoverageAreaID]  INT              NOT NULL,
    [CoverageArea]        NVARCHAR (50)    NULL,
    [DisplayOrder]        INT              NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_52601645_0EDE_4CF1_8AE7_EFC9C8EDF58A_277576027] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [DateAdded]           DATETIME         NULL,
    [LastUpdate]          DATETIME         NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [PK_CoverageRoles] PRIMARY KEY CLUSTERED ([REIBCoverageAreaID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=Lookup,Notes=Unimplemented feature brought over from Secured Capital version', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAreas';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCoverageAreas';


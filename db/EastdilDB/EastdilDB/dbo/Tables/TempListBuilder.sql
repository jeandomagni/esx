﻿CREATE TABLE [dbo].[TempListBuilder] (
    [TempListBuilderID]    BIGINT         IDENTITY (1, 1) NOT NULL,
    [AddToDealID]          BIGINT         NULL,
    [AddToContactListID]   BIGINT         NULL,
    [AddFromDealID]        BIGINT         NULL,
    [AddFromContactListID] BIGINT         NULL,
    [ContactID]            BIGINT         NULL,
    [CompanyID]            BIGINT         NULL,
    [FirstnameLastname]    NVARCHAR (100) NULL,
    [CompanyName]          NVARCHAR (128) NULL,
    [UserEmail]            NVARCHAR (50)  NULL,
    [DateAdded]            DATETIME       NULL,
    [LastUpdate]           DATETIME       NULL,
    [AddedBy]              NVARCHAR (100) NULL,
    [UpdatedBy]            NVARCHAR (100) NULL,
    CONSTRAINT [PK_TempDealListContacts] PRIMARY KEY CLUSTERED ([TempListBuilderID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempListBuilder';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempListBuilder';


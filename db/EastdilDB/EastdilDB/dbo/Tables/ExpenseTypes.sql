﻿CREATE TABLE [dbo].[ExpenseTypes] (
    [ExpenseTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [ExpenseType]   NVARCHAR (250) NULL,
    [DisplayOrder]  INT            NULL,
    [RowVersion]    ROWVERSION     NOT NULL,
    [DateAdded]     DATETIME       NULL,
    [LastUpdate]    DATETIME       NULL,
    [AddedBy]       NVARCHAR (100) NULL,
    [UpdatedBy]     NVARCHAR (100) NULL,
    CONSTRAINT [PK_ExpenseTypes] PRIMARY KEY CLUSTERED ([ExpenseTypeID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ExpenseTypes';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ExpenseTypes';


﻿CREATE TABLE [dbo].[DealWPExternal] (
    [DealWPExternalID]    BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealID]              BIGINT           NULL,
    [ContactID]           BIGINT           NULL,
    [WorkingPartyRole]    NVARCHAR (50)    NULL,
    [WorkingPartyRoleID]  INT              NULL,
    [ShowOnWPList]        BIT              DEFAULT ((0)) NULL,
    [LastUpdate]          DATETIME         DEFAULT (getdate()) NULL,
    [Migrated]            BIT              NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_5DA1DD28_ACFB_48CC_BCF5_011F3F2B3FEE_917578307] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [DateAdded]           DATETIME         NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [PK_DealWPExternal] PRIMARY KEY CLUSTERED ([DealWPExternalID] ASC),
    CONSTRAINT [FK_DealWPExternal_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contacts] ([ContactID]),
    CONSTRAINT [FK_DealWPExternal_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealWPExternal_WorkingPartyRoles] FOREIGN KEY ([WorkingPartyRoleID]) REFERENCES [dbo].[WorkingPartyRoles] ([WorkingPartyRoleID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=26 @ 24.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'WorkingPartyRoleID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'WorkingPartyRole';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 633.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'ShowOnWPList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=138 @ 4.59:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'Migrated';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=532 @ 1.19:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'DealWPExternalID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=147 @ 4.31:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=577 @ 1.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPExternal', @level2type = N'COLUMN', @level2name = N'AddedBy';




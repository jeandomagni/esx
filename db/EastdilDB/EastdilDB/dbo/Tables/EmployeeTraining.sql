﻿CREATE TABLE [dbo].[EmployeeTraining] (
    [EmployeeTrainingID] INT            IDENTITY (1, 1) NOT NULL,
    [EmployeeID]         INT            NOT NULL,
    [CourseID]           INT            NOT NULL,
    [Complete]           BIT            NULL,
    [CompleteDate]       DATETIME       NULL,
    [DateAdded]          DATETIME       NULL,
    [LastUpdate]         DATETIME       NULL,
    [AddedBy]            NVARCHAR (100) NULL,
    [UpdatedBy]          NVARCHAR (100) NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=Used in ESNet not ESP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining', @level2type = N'COLUMN', @level2name = N'EmployeeTrainingID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=396 @ 8.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=16 @ 214.38:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining', @level2type = N'COLUMN', @level2name = N'CourseID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=81 @ 42.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining', @level2type = N'COLUMN', @level2name = N'CompleteDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 1715.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining', @level2type = N'COLUMN', @level2name = N'Complete';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeTraining', @level2type = N'COLUMN', @level2name = N'AddedBy';




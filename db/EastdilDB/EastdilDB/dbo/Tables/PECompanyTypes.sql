﻿CREATE TABLE [dbo].[PECompanyTypes] (
    [PECompanyTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [CompanyType]     NVARCHAR (100) NULL,
    [DisplayOrder]    INT            NULL,
    [RowVersion]      ROWVERSION     NOT NULL,
    [DateAdded]       DATETIME       NULL,
    [LastUpdate]      DATETIME       NULL,
    [AddedBy]         NVARCHAR (100) NULL,
    [UpdatedBy]       NVARCHAR (100) NULL,
    CONSTRAINT [PK_PECompanyTypes] PRIMARY KEY CLUSTERED ([PECompanyTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Lookup,Notes=PE = Private Equity', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PECompanyTypes';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PECompanyTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PECompanyTypes', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PECompanyTypes', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PECompanyTypes', @level2type = N'COLUMN', @level2name = N'PECompanyTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PECompanyTypes', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PECompanyTypes', @level2type = N'COLUMN', @level2name = N'DisplayOrder';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PECompanyTypes', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PECompanyTypes', @level2type = N'COLUMN', @level2name = N'CompanyType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PECompanyTypes', @level2type = N'COLUMN', @level2name = N'AddedBy';




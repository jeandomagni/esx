﻿CREATE TABLE [dbo].[REIBFinancialSnapshot] (
    [REIBFinancialSnapshotID]   BIGINT          IDENTITY (1, 1) NOT NULL,
    [CompanyID]                 BIGINT          NULL,
    [CompanyName]               NVARCHAR (255)  NULL,
    [Segment]                   NVARCHAR (255)  NULL,
    [InstitutionKey]            NVARCHAR (255)  NULL,
    [InvestmentFocus]           NVARCHAR (255)  NULL,
    [PropertyFocus]             NVARCHAR (255)  NULL,
    [SPRatingsDate]             DATETIME        NULL,
    [MoodysDate]                DATETIME        NULL,
    [Ticker]                    NVARCHAR (255)  NULL,
    [DateOfClosing]             DATETIME        NULL,
    [SPCreditRating]            NVARCHAR (255)  NULL,
    [MoodysCreditRating]        NVARCHAR (255)  NULL,
    [StockPriceCurrent]         NUMERIC (18, 2) NULL,
    [StockPrice52WeekHigh]      NUMERIC (18, 2) NULL,
    [StockPrice52WeekLow]       NUMERIC (18, 2) NULL,
    [TradingRelationToWeekHigh] NUMERIC (18, 5) NULL,
    [ConsensusNAVEstimate]      INT             NULL,
    [Growth0809]                INT             NULL,
    [Growth0910]                INT             NULL,
    [P_FFOMultiples2009E]       INT             NULL,
    [P_FFOMultiples2010E]       INT             NULL,
    [SharesOutstandingCS]       NUMERIC (18, 5) NULL,
    [SharesOutstandingOP]       NUMERIC (18, 5) NULL,
    [SharesOutstandingTotal]    NUMERIC (18, 5) NULL,
    [InstitutionalOwnership]    NUMERIC (18, 5) NULL,
    [UPREITMarketCap]           NUMERIC (18, 5) NULL,
    [TotalPreferredEquity]      NUMERIC (18, 5) NULL,
    [TotalDebt]                 NUMERIC (18, 5) NULL,
    [TotalCap]                  NUMERIC (18, 5) NULL,
    [TotalDebtTotalCap]         NUMERIC (18, 5) NULL,
    [FixedChargeCoverage]       NUMERIC (18, 2) NULL,
    [FFOPerShare2009E]          NUMERIC (18, 2) NULL,
    [FFOPerShare2010E]          NUMERIC (18, 2) NULL,
    [EBITDA2009E]               INT             NULL,
    [EBITDA2010E]               INT             NULL,
    [EV_EBITDA2009E]            NUMERIC (18, 5) NULL,
    [EV_EBITDA2010E]            NUMERIC (18, 5) NULL,
    [TotalReturnLTM]            NUMERIC (18, 5) NULL,
    [TotalReturnYTD]            NUMERIC (18, 5) NULL,
    [DividendYield]             NUMERIC (18, 5) NULL,
    [LastUpdate]                DATETIME        NULL,
    [RowVersion]                ROWVERSION      NOT NULL,
    [DateAdded]                 DATETIME        NULL,
    [AddedBy]                   NVARCHAR (100)  NULL,
    [UpdatedBy]                 NVARCHAR (100)  NULL,
    CONSTRAINT [PK_REIBFinancialSnapshot] PRIMARY KEY CLUSTERED ([REIBFinancialSnapshotID] ASC),
    CONSTRAINT [FK_REIBFinancialSnapshot_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'UPREITMarketCap';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'TradingRelationToWeekHigh';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'TotalReturnYTD';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'TotalReturnLTM';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'TotalPreferredEquity';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'TotalDebtTotalCap';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'TotalDebt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'TotalCap';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'Ticker';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'StockPriceCurrent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'StockPrice52WeekLow';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'StockPrice52WeekHigh';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'SPRatingsDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'SPCreditRating';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'SharesOutstandingTotal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'SharesOutstandingOP';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'SharesOutstandingCS';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'Segment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'REIBFinancialSnapshotID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'PropertyFocus';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'P_FFOMultiples2010E';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'P_FFOMultiples2009E';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'MoodysDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'MoodysCreditRating';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'InvestmentFocus';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'InstitutionKey';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'InstitutionalOwnership';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'Growth0910';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'Growth0809';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'FixedChargeCoverage';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'FFOPerShare2010E';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'FFOPerShare2009E';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'EV_EBITDA2010E';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'EV_EBITDA2009E';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'EBITDA2010E';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'EBITDA2009E';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'DividendYield';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'DateOfClosing';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'ConsensusNAVEstimate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'CompanyName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBFinancialSnapshot', @level2type = N'COLUMN', @level2name = N'AddedBy';




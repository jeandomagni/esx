﻿CREATE TABLE [dbo].[DealPrintJobs] (
    [DealPrintJobID]      BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealID]              BIGINT         NULL,
    [DateProduced]        DATETIME       NULL,
    [QuantityProduced]    INT            NULL,
    [QuantityShipped]     INT            NULL,
    [PageCount]           INT            NULL,
    [RequestorEmployeeID] BIGINT         NULL,
    [PaperType]           NVARCHAR (50)  NULL,
    [PrintSides]          NVARCHAR (50)  NULL,
    [ProjectType]         NVARCHAR (50)  NULL,
    [BindingType]         NVARCHAR (50)  NULL,
    [ShippingMethod]      NVARCHAR (50)  NULL,
    [LastUpdate]          DATETIME       NULL,
    [RowVersion]          ROWVERSION     NOT NULL,
    [DateAdded]           DATETIME       NULL,
    [AddedBy]             NVARCHAR (100) NULL,
    [UpdatedBy]           NVARCHAR (100) NULL,
    CONSTRAINT [PK_DealPrintLog] PRIMARY KEY CLUSTERED ([DealPrintJobID] ASC),
    CONSTRAINT [FK_DealPrintLog_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealPrintLog_Employees] FOREIGN KEY ([RequestorEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 1.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'ShippingMethod';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 1.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'RequestorEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'QuantityShipped';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'QuantityProduced';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'ProjectType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 2.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'PrintSides';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'PaperType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'PageCount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'DealPrintJobID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 4.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'DateProduced';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 2.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'BindingType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealPrintJobs', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[DealFinancings] (
    [DealFinancingID]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealID]                   BIGINT         NULL,
    [ProjectSize]              INT            NULL,
    [ProjectSizeTypeID]        INT            NULL,
    [PctLeased]                FLOAT (53)     NULL,
    [CMBSTransaction]          BIT            NOT NULL,
    [FinancingTypeID]          INT            NULL,
    [PurchasePrice_Valuation]  MONEY          NULL,
    [PricePerSqFt]             MONEY          NULL,
    [Occupancy]                FLOAT (53)     NULL,
    [UrbanOrSuburban]          NVARCHAR (50)  NULL,
    [StablizedOrNonstabilized] NVARCHAR (50)  NULL,
    [CapRate]                  FLOAT (53)     NULL,
    [CapRateNote]              NVARCHAR (50)  NULL,
    [NOI]                      MONEY          NULL,
    [NOIDescription]           NVARCHAR (500) NULL,
    [NOIFirstYear]             MONEY          NULL,
    [NOIStabilized]            MONEY          NULL,
    [LenderCompanyID]          BIGINT         NULL,
    [LenderName]               NVARCHAR (500) NULL,
    [LenderTypeID]             INT            NULL,
    [LoanAmount]               MONEY          NULL,
    [LoanPerSFOrUnit]          MONEY          NULL,
    [LTV_LTC]                  FLOAT (53)     NULL,
    [DebtYield]                FLOAT (53)     NULL,
    [LoanTypeID]               INT            NULL,
    [LoanTerm]                 NVARCHAR (50)  NULL,
    [LoanRateTypeID]           INT            NULL,
    [ApproxDateQuoted]         DATETIME       NULL,
    [ReferenceIndexID]         INT            NULL,
    [Spread]                   FLOAT (53)     NULL,
    [TreasuryWhenLocked]       FLOAT (53)     NULL,
    [InitialRate]              FLOAT (53)     NULL,
    [DateLocked]               DATETIME       NULL,
    [RateFloor]                FLOAT (53)     NULL,
    [OriginationFee]           FLOAT (53)     NULL,
    [Amortization]             NVARCHAR (50)  NULL,
    [Prepayment]               NVARCHAR (50)  NULL,
    [LockOut]                  NVARCHAR (50)  NULL,
    [OpenPeriod]               NVARCHAR (50)  NULL,
    [MaxLTV]                   FLOAT (53)     NULL,
    [Recourse]                 NVARCHAR (250) NULL,
    [Reserves]                 NVARCHAR (250) NULL,
    [SponsorName]              NVARCHAR (250) NULL,
    [SponsorCompanyID]         BIGINT         NULL,
    [SponsorTypeID]            INT            NULL,
    [LastUpdate]               DATETIME       NULL,
    [RowVersion]               ROWVERSION     NULL,
    [DateAdded]                DATETIME       NULL,
    [AddedBy]                  NVARCHAR (100) NULL,
    [UpdatedBy]                NVARCHAR (100) NULL,
    CONSTRAINT [PK_DealFinancings] PRIMARY KEY CLUSTERED ([DealFinancingID] ASC),
    CONSTRAINT [FK_DealFinancings_BuyerOrSponsorTypes] FOREIGN KEY ([SponsorTypeID]) REFERENCES [dbo].[BuyerTypes] ([BuyerTypeID]),
    CONSTRAINT [FK_DealFinancings_Deals] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealFinancings_FinancingTypes] FOREIGN KEY ([FinancingTypeID]) REFERENCES [dbo].[FinancingTypes] ([FinancingTypeID]),
    CONSTRAINT [FK_DealFinancings_LenderCompanies] FOREIGN KEY ([LenderCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealFinancings_LenderTypes] FOREIGN KEY ([LenderTypeID]) REFERENCES [dbo].[LenderTypes] ([LenderTypeID]),
    CONSTRAINT [FK_DealFinancings_LoanRateTypes] FOREIGN KEY ([LoanRateTypeID]) REFERENCES [dbo].[LoanRateTypes] ([LoanRateTypeID]),
    CONSTRAINT [FK_DealFinancings_LoanTypes] FOREIGN KEY ([LoanTypeID]) REFERENCES [dbo].[LoanTypes] ([LoanTypeID]),
    CONSTRAINT [FK_DealFinancings_ProjectSizeTypes] FOREIGN KEY ([ProjectSizeTypeID]) REFERENCES [dbo].[ProjectSizeTypes] ([ProjectSizeTypeID]),
    CONSTRAINT [FK_DealFinancings_ReferenceIndex] FOREIGN KEY ([ReferenceIndexID]) REFERENCES [dbo].[ReferenceIndex] ([ReferenceIndexID]),
    CONSTRAINT [FK_DealFinancings_SponsorCompanies] FOREIGN KEY ([SponsorCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealFinancings_SponsorTypes] FOREIGN KEY ([SponsorTypeID]) REFERENCES [dbo].[SponsorTypes] ([SponsorTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'UrbanOrSuburban';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=137 @ 89.55:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'TreasuryWhenLocked';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'StablizedOrNonstabilized';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=216 @ 56.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'Spread';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 3067.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'SponsorTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=678 @ 18.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'SponsorName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=586 @ 20.94:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'SponsorCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=51 @ 240.55:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'Reserves';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5 @ 2453.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'ReferenceIndexID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=37 @ 331.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'Recourse';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25 @ 490.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'RateFloor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=530 @ 23.15:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'PurchasePrice_Valuation';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5 @ 2453.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'ProjectSizeTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1301 @ 9.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'ProjectSize';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=506 @ 24.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'PricePerSqFt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=272 @ 45.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'Prepayment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=256 @ 47.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'PctLeased';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=51 @ 240.55:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'OriginationFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=178 @ 68.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'OpenPeriod';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=203 @ 60.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'Occupancy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'NOIStabilized';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'NOIFirstYear';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4089.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'NOIDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=37 @ 331.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'NOI';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12 @ 1022.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'MaxLTV';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=86 @ 142.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LTV_LTC';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=79 @ 155.29:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LockOut';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5 @ 2453.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LoanTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=110 @ 111.53:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LoanTerm';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 3067.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LoanRateTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=515 @ 23.82:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LoanPerSFOrUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=579 @ 21.19:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LoanAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 1752.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LenderTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=347 @ 35.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LenderName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=255 @ 48.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LenderCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4672 @ 2.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=125 @ 98.14:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'InitialRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 3067.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'FinancingTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=165 @ 74.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'DebtYield';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'DealFinancingID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=331 @ 37.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'DateLocked';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6134.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'CMBSTransaction';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=17 @ 721.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'CapRateNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=59 @ 207.93:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'CapRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=428 @ 28.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'ApproxDateQuoted';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=205 @ 59.84:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'Amortization';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFinancings', @level2type = N'COLUMN', @level2name = N'AddedBy';




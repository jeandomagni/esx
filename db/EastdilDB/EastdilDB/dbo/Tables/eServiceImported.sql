﻿CREATE TABLE [dbo].[eServiceImported] (
    [VoucherAccount]      NVARCHAR (50) NULL,
    [VENDOR]              NVARCHAR (50) NULL,
    [INVOICE NUMBER]      NVARCHAR (50) NULL,
    [INVOICE DT]          DATETIME      NULL,
    [VOUCHER #]           NVARCHAR (50) NULL,
    [GL EFFEC DT]         DATETIME      NULL,
    [AP CO]               NVARCHAR (50) NULL,
    [GL ENT]              NVARCHAR (50) NULL,
    [ORIG GL ENT]         NVARCHAR (50) NULL,
    [AU]                  NVARCHAR (50) NULL,
    [ACCOUNT]             NVARCHAR (50) NULL,
    [ORIG ACCOUNT]        NVARCHAR (50) NULL,
    [APPROVER]            NVARCHAR (50) NULL,
    [CURRENCY]            NVARCHAR (50) NULL,
    [MONETARY AMOUNT]     MONEY         CONSTRAINT [DF_eServiceImported_MONETARY AMOUNT] DEFAULT ((0)) NOT NULL,
    [USD MONETARY AMOUNT] MONEY         CONSTRAINT [DF_eServiceImported_USD MONETARY AMOUNT] DEFAULT ((0)) NOT NULL,
    [EXCHANGE RATE]       FLOAT (53)    NULL,
    [CHECK DT]            DATETIME      NULL,
    [CHECK #]             NVARCHAR (50) NULL,
    [CHECK AMT]           MONEY         CONSTRAINT [DF_eServiceImported_CHECK AMT] DEFAULT ((0)) NOT NULL,
    [CHECK CLEARED DT]    DATETIME      NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utilty,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1359 @ 5.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'VoucherAccount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=776 @ 9.86:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'VOUCHER #';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=343 @ 22.31:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'VENDOR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4239 @ 1.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'USD MONETARY AMOUNT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 7653.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'ORIG GL ENT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=45 @ 170.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'ORIG ACCOUNT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4239 @ 1.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'MONETARY AMOUNT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=775 @ 9.87:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'INVOICE NUMBER';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=28 @ 273.32:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'INVOICE DT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 7653.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'GL ENT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=25 @ 306.12:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'GL EFFEC DT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 7653.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'EXCHANGE RATE';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 7653.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'CURRENCY';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=26 @ 294.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'CHECK DT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=26 @ 294.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'CHECK CLEARED DT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=484 @ 15.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'CHECK AMT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=514 @ 14.89:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'CHECK #';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 7653.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'AU';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 850.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'APPROVER';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 3826.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'AP CO';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=45 @ 170.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceImported', @level2type = N'COLUMN', @level2name = N'ACCOUNT';




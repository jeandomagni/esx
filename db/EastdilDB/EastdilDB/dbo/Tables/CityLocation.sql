﻿CREATE TABLE [dbo].[CityLocation] (
    [CityLocationID]      INT           IDENTITY (1, 1) NOT NULL,
    [City]                NVARCHAR (50) NULL,
    [State]               NVARCHAR (50) NULL,
    [Country]             NVARCHAR (50) NULL,
    [RegionalMarketID]    INT           NULL,
    [RegionalSubMarketID] INT           NOT NULL,
    CONSTRAINT [PK_CityLocation] PRIMARY KEY CLUSTERED ([CityLocationID] ASC),
    FOREIGN KEY ([RegionalMarketID]) REFERENCES [dbo].[RegionalMarkets] ([RegionalMarketID]),
    FOREIGN KEY ([RegionalSubMarketID]) REFERENCES [dbo].[RegionalSubmarkets] ([RegionalSubmarketID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CityLocation';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CityLocation';


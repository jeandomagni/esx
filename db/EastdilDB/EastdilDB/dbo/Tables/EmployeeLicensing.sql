﻿CREATE TABLE [dbo].[EmployeeLicensing] (
    [EmployeeLicensingID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [EmployeeID]          BIGINT          NULL,
    [LicensingType]       NVARCHAR (50)   NULL,
    [LicensingStatus]     NVARCHAR (50)   NULL,
    [State]               NVARCHAR (50)   NULL,
    [ProcessStartDate]    DATETIME        NULL,
    [ExpirationDate]      DATETIME        NULL,
    [Comments]            NVARCHAR (4000) NULL,
    [LastUpdate]          DATETIME        NULL,
    [RowVersion]          ROWVERSION      NOT NULL,
    [DateAdded]           DATETIME        NULL,
    [AddedBy]             NVARCHAR (100)  NULL,
    [UpdatedBy]           NVARCHAR (100)  NULL,
    [LicenseNumber]       NVARCHAR (50)   NULL,
    CONSTRAINT [PK_EmployeeLicensing] PRIMARY KEY CLUSTERED ([EmployeeLicensingID] ASC),
    CONSTRAINT [FK_EmployeeLicensing_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=Used in ESNet not ESP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=24 @ 24.71:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'State';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 296.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'ProcessStartDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=7 @ 84.71:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'LicensingType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 197.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'LicensingStatus';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=299 @ 1.98:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'LicenseNumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=179 @ 3.31:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'ExpirationDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'EmployeeLicensingID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=369 @ 1.61:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 65.89:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'Comments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeLicensing', @level2type = N'COLUMN', @level2name = N'AddedBy';




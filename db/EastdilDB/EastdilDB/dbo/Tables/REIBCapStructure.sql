﻿CREATE TABLE [dbo].[REIBCapStructure] (
    [REIBCapStructureID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [CompanyID]          BIGINT          NULL,
    [TickerSymbol]       NVARCHAR (50)   NULL,
    [Description]        NVARCHAR (200)  NULL,
    [FacilityBalance]    MONEY           NULL,
    [WFCommitment]       MONEY           NULL,
    [AmountOutstanding]  MONEY           NULL,
    [InterestRate]       DECIMAL (18, 3) NULL,
    [RedemptionDate]     DATETIME        NULL,
    [WFGroup]            NVARCHAR (100)  NULL,
    [LastUpdate]         DATETIME        NULL,
    [RowVersion]         ROWVERSION      NOT NULL,
    [DateAdded]          DATETIME        NULL,
    [AddedBy]            NVARCHAR (100)  NULL,
    [UpdatedBy]          NVARCHAR (100)  NULL,
    CONSTRAINT [PK_REIBCompanyCapStructure] PRIMARY KEY CLUSTERED ([REIBCapStructureID] ASC),
    CONSTRAINT [FK_REIBCompanyCapStructure_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'WFGroup';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'WFCommitment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'TickerSymbol';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'REIBCapStructureID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 1.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'RedemptionDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'InterestRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'FacilityBalance';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'Description';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 3.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'AmountOutstanding';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBCapStructure', @level2type = N'COLUMN', @level2name = N'AddedBy';




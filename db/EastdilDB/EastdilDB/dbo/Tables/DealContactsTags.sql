﻿CREATE TABLE [dbo].[DealContactsTags] (
    [DealContactTagID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealContactID]    BIGINT         NULL,
    [TagID]            BIGINT         NULL,
    [DateAdded]        DATETIME       NULL,
    [LastUpdate]       DATETIME       NULL,
    [AddedBy]          NVARCHAR (100) NULL,
    [UpdatedBy]        NVARCHAR (100) NULL,
    CONSTRAINT [PK_DealContactsTags] PRIMARY KEY CLUSTERED ([DealContactTagID] ASC),
    CONSTRAINT [FK_DealContactsTags_DealsContacts] FOREIGN KEY ([DealContactID]) REFERENCES [dbo].[DealContacts] ([DealContactID]),
    CONSTRAINT [FK_DealContactsTags_TagsForMarketingLists] FOREIGN KEY ([TagID]) REFERENCES [dbo].[TagsForMarketingLists] ([TagID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=This was used primarily to support the Bank of Ireland''s rapid divestiture of loan portfolio a few years ago', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactsTags';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactsTags';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactsTags', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=9 @ 47.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactsTags', @level2type = N'COLUMN', @level2name = N'TagID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactsTags', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactsTags', @level2type = N'COLUMN', @level2name = N'DealContactTagID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactsTags', @level2type = N'COLUMN', @level2name = N'DealContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactsTags', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactsTags', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[CriteriaFinancing] (
    [CriteriaFinancingID]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [ContactID]               BIGINT          NULL,
    [DealID]                  BIGINT          NULL,
    [Comment]                 NVARCHAR (4000) NULL,
    [MustSeeAllDeals]         BIT             NULL,
    [Hotel]                   BIT             NULL,
    [Industrial]              BIT             NULL,
    [MultiFamily]             BIT             NULL,
    [Office]                  BIT             NULL,
    [ResidentialCommunity]    BIT             NULL,
    [Retail]                  BIT             NULL,
    [Land]                    BIT             NULL,
    [OtherProductType]        BIT             NULL,
    [Range10To25]             BIT             NULL,
    [Range25To50]             BIT             NULL,
    [Range50To100]            BIT             NULL,
    [Range100Plus]            BIT             NULL,
    [Nationwide]              BIT             NULL,
    [Southeast]               BIT             NULL,
    [MidAtlantic]             BIT             NULL,
    [Northeast]               BIT             NULL,
    [Midwest]                 BIT             NULL,
    [Southwest]               BIT             NULL,
    [West]                    BIT             NULL,
    [FirstMortgageSeniorLoan] BIT             NULL,
    [PreferredEquityLender]   BIT             NULL,
    [MezzanineLender]         BIT             NULL,
    [DomesticBank]            BIT             NULL,
    [OffshoreBank]            BIT             NULL,
    [CMBS]                    BIT             NULL,
    [InsuranceCompany]        BIT             NULL,
    [MortgageREIT]            BIT             NULL,
    [FundSponsorLender]       BIT             NULL,
    [OtherLenderType]         BIT             NULL,
    [UpdateFromDealID]        BIGINT          NULL,
    [LastUpdatedByEmployeeID] BIGINT          NULL,
    [LastUpdate]              DATETIME        NULL,
    [RowVersion]              ROWVERSION      NOT NULL,
    [DateAdded]               DATETIME        NULL,
    [AddedBy]                 NVARCHAR (100)  NULL,
    [UpdatedBy]               NVARCHAR (100)  NULL,
    CONSTRAINT [PK_CriteriaFinancing] PRIMARY KEY CLUSTERED ([CriteriaFinancingID] ASC),
    CONSTRAINT [FK_CriteriaFinancing_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contacts] ([ContactID]) ON DELETE CASCADE,
    CONSTRAINT [FK_CriteriaFinancing_Employees] FOREIGN KEY ([LastUpdatedByEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utility,Notes=Partially implemented and utilized', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'West';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'UpdateFromDealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Southwest';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Southeast';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Retail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'ResidentialCommunity';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Range50To100';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Range25To50';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Range10To25';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Range100Plus';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'PreferredEquityLender';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'OtherProductType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'OtherLenderType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'OffshoreBank';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Office';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Northeast';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Nationwide';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'MustSeeAllDeals';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'MultiFamily';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'MortgageREIT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Midwest';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'MidAtlantic';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'MezzanineLender';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 23700.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'LastUpdatedByEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=21066 @ 4.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Land';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'InsuranceCompany';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Industrial';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Hotel';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'FundSponsorLender';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'FirstMortgageSeniorLoan';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'DomesticBank';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'CriteriaFinancingID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'Comment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'CMBS';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaFinancing', @level2type = N'COLUMN', @level2name = N'AddedBy';




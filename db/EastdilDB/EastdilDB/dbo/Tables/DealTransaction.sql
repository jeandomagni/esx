﻿CREATE TABLE [dbo].[DealTransaction] (
    [DealTransactionID]      BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealID]                 BIGINT           NULL,
    [DealCode]               NVARCHAR (50)    NULL,
    [OPPORTUNITYID_]         CHAR (12)        NULL,
    [EstGrossFee]            MONEY            NULL,
    [EstNetFeeToES]          MONEY            NULL,
    [FeeToES]                MONEY            NULL,
    [ProposedFeePercentage]  DECIMAL (18, 5)  NULL,
    [FinalFeePercentage]     DECIMAL (18, 5)  NULL,
    [FinalFeeAmount]         MONEY            NULL,
    [FeeComment]             NVARCHAR (1000)  NULL,
    [AmountReimb]            MONEY            NULL,
    [ExpCreditPct]           NUMERIC (19, 2)  NULL,
    [CoBrokerInvolved]       BIT              CONSTRAINT [DF__DealTrans__CoBro__1332DBDC] DEFAULT ((0)) NULL,
    [CoBrokerName]           NVARCHAR (250)   NULL,
    [CoBrokerAgrmtDrafted]   BIT              CONSTRAINT [DF__DealTrans__CoBro__14270015] DEFAULT ((0)) NULL,
    [CoBrokerAgrmtSigned]    BIT              CONSTRAINT [DF__DealTrans__CoBro__151B244E] DEFAULT ((0)) NULL,
    [CoBrokerAgrmtDate]      DATETIME         NULL,
    [CoBrokerFeeAmtPaid]     MONEY            NULL,
    [CoBrokerFeePercentage]  DECIMAL (18)     NULL,
    [EstCoBrokerFee]         MONEY            NULL,
    [TotalExpenses]          MONEY            NULL,
    [ExpensesReimbursed]     BIT              CONSTRAINT [DF__DealTrans__Expen__160F4887] DEFAULT ((0)) NULL,
    [UnReimbExp]             MONEY            NULL,
    [ExpReimbCap]            MONEY            NULL,
    [ReimbPerAsset]          MONEY            NULL,
    [ExpenseBudget]          MONEY            NULL,
    [ExpenseCredit]          BIT              CONSTRAINT [DF__DealTrans__Expen__17036CC0] DEFAULT ((0)) NULL,
    [ExpCreditPercentage]    DECIMAL (18)     NULL,
    [ExpenseComment]         NVARCHAR (1000)  NULL,
    [FinancingSaleCovered]   BIT              CONSTRAINT [DF__DealTrans__Finan__17F790F9] DEFAULT ((0)) NULL,
    [PartialPayments]        BIT              CONSTRAINT [DF__DealTrans__Parti__18EBB532] DEFAULT ((0)) NULL,
    [AlternativeSplitWithWF] BIT              NULL,
    [WellsFargoFee]          BIT              CONSTRAINT [DF__DealTrans__Wells__19DFD96B] DEFAULT ((0)) NULL,
    [GSA]                    MONEY            NULL,
    [NumDealFees]            INT              NULL,
    [LastUpdate]             DATETIME         CONSTRAINT [DF__DealTrans__LastU__1AD3FDA4] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]    UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_BAAE5ECA_0AC8_4C10_8039_C2510BE4E7CD_1598628738] DEFAULT (newid()) NOT NULL,
    [RowVersion]             ROWVERSION       NOT NULL,
    [DateAdded]              DATETIME         NULL,
    [AddedBy]                NVARCHAR (100)   NULL,
    [UpdatedBy]              NVARCHAR (100)   NULL,
    CONSTRAINT [PK_DealTransaction] PRIMARY KEY CLUSTERED ([DealTransactionID] ASC),
    CONSTRAINT [FK_DealTransaction_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6142.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'WellsFargoFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5131 @ 2.39:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'UnReimbExp';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4511 @ 2.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'TotalExpenses';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4094.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'ReimbPerAsset';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=29 @ 423.59:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'ProposedFeePercentage';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6142.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'PartialPayments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'OPPORTUNITYID_';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12 @ 1023.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'NumDealFees';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4571 @ 2.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=7990 @ 1.54:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=31 @ 396.26:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'GSA';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12284.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'FinancingSaleCovered';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=49 @ 250.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'FinalFeePercentage';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1018 @ 12.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'FinalFeeAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 12284.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'FeeToES';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=177 @ 69.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'FeeComment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=68 @ 180.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'ExpReimbCap';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6142.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'ExpensesReimbursed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6142.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'ExpenseCredit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=87 @ 141.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'ExpenseComment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=92 @ 133.52:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'ExpenseBudget';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 6142.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'ExpCreditPercentage';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 6142.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'ExpCreditPct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=136 @ 90.32:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'EstNetFeeToES';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4094.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'EstGrossFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=23 @ 534.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'EstCoBrokerFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'DealTransactionID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=11347 @ 1.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'DealCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 2456.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'CoBrokerName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6142.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'CoBrokerInvolved';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 2456.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'CoBrokerFeePercentage';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=166 @ 74.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'CoBrokerFeeAmtPaid';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6142.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'CoBrokerAgrmtSigned';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6142.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'CoBrokerAgrmtDrafted';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12 @ 1023.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'CoBrokerAgrmtDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2139 @ 5.74:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'AmountReimb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6142.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'AlternativeSplitWithWF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTransaction', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[SLBranchCanonicals] (
    [CompanyID]   BIGINT         NULL,
    [CanonicalID] BIGINT         NULL,
    [DateAdded]   DATETIME       NULL,
    [LastUpdate]  DATETIME       NULL,
    [AddedBy]     NVARCHAR (100) NULL,
    [UpdatedBy]   NVARCHAR (100) NULL,
    CONSTRAINT [FK_SLBranchCanonicals_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_SLBranchCanonicals_SLCanonicals] FOREIGN KEY ([CanonicalID]) REFERENCES [dbo].[SLCanonicals] ([CanonicalID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Skyline Project,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLBranchCanonicals';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLBranchCanonicals';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLBranchCanonicals', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLBranchCanonicals', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLBranchCanonicals', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=6410 @ 1.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLBranchCanonicals', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1410 @ 4.61:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLBranchCanonicals', @level2type = N'COLUMN', @level2name = N'CanonicalID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLBranchCanonicals', @level2type = N'COLUMN', @level2name = N'AddedBy';




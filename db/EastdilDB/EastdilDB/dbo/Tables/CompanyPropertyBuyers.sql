﻿CREATE TABLE [dbo].[CompanyPropertyBuyers] (
    [CompanyPropertyBuyerID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [CompanyID]              BIGINT         NULL,
    [PropertyID]             BIGINT         NULL,
    [DateAcquired]           DATETIME       NULL,
    [DateDisposed]           DATETIME       NULL,
    [DateAdded]              DATETIME       NULL,
    [LastUpdate]             DATETIME       NULL,
    [RowVersion]             ROWVERSION     NULL,
    [AddedBy]                NVARCHAR (100) NULL,
    [UpdatedBy]              NVARCHAR (100) NULL,
    CONSTRAINT [PK_CompanyPropertyBuyers] PRIMARY KEY CLUSTERED ([CompanyPropertyBuyerID] ASC),
    CONSTRAINT [FK_CompanyPropertyBuyers_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_CompanyPropertyBuyers_Properties] FOREIGN KEY ([PropertyID]) REFERENCES [dbo].[Properties] ([PropertyID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=Partially implemented and utilized', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers', @level2type = N'COLUMN', @level2name = N'PropertyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 373.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers', @level2type = N'COLUMN', @level2name = N'DateDisposed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 373.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=224 @ 1.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers', @level2type = N'COLUMN', @level2name = N'DateAcquired';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers', @level2type = N'COLUMN', @level2name = N'CompanyPropertyBuyerID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=292 @ 1.28:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyPropertyBuyers', @level2type = N'COLUMN', @level2name = N'AddedBy';




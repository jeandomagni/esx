﻿CREATE TABLE [dbo].[TeamRoles] (
    [TeamRoleID] INT            IDENTITY (1, 1) NOT NULL,
    [TeamRole]   NVARCHAR (100) NULL,
    [DateAdded]  DATETIME       NULL,
    [LastUpdate] DATETIME       NULL,
    [AddedBy]    NVARCHAR (100) NULL,
    [UpdatedBy]  NVARCHAR (100) NULL,
    CONSTRAINT [PK_TeamRoles] PRIMARY KEY CLUSTERED ([TeamRoleID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamRoles';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamRoles';


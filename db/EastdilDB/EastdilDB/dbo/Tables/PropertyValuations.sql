﻿CREATE TABLE [dbo].[PropertyValuations] (
    [PropertyValuationID]                       BIGINT           IDENTITY (1, 1) NOT NULL,
    [PropertyID]                                BIGINT           NULL,
    [DealID]                                    BIGINT           NULL,
    [PostedByEmployeeID]                        BIGINT           NULL,
    [ValuationDate]                             DATETIME         NULL,
    [Value]                                     MONEY            NULL,
    [ValuePerUnit]                              MONEY            NULL,
    [ProjectSizeTypeID]                         INT              NULL,
    [CurrentOwner]                              NVARCHAR (500)   NULL,
    [MostRecentSeller]                          NVARCHAR (500)   NULL,
    [DateSold]                                  DATETIME         NULL,
    [JVOwnership]                               BIT              NULL,
    [JVOwnershipList]                           NVARCHAR (1000)  NULL,
    [InPlaceNOI]                                MONEY            NULL,
    [InPlaceOccupany]                           INT              NULL,
    [Year1NOI]                                  MONEY            NULL,
    [Year5NOI]                                  MONEY            NULL,
    [Year10NOI]                                 MONEY            NULL,
    [FiveYrMarketRentCAGR]                      FLOAT (53)       NULL,
    [FiveYrNOICAGR]                             FLOAT (53)       NULL,
    [TenYrMarketRentCAGR]                       FLOAT (53)       NULL,
    [TenYrNOICAGR]                              FLOAT (53)       NULL,
    [IsOfficeIndustrial]                        BIT              NULL,
    [MarketRent]                                MONEY            NULL,
    [GeneralVacancy]                            MONEY            NULL,
    [ExpenseRatio]                              FLOAT (53)       NULL,
    [ReimbursementMethod]                       NVARCHAR (500)   NULL,
    [UnderwrittenLeaseTerm]                     NVARCHAR (500)   NULL,
    [MarketTenantImprovements]                  NVARCHAR (1000)  NULL,
    [ExpiringSFInNextFiveYears]                 FLOAT (53)       NULL,
    [ExpiringSFPercentOfTotalInNextFiveYears]   FLOAT (53)       NULL,
    [ExpiringRentInNextFiveYears]               FLOAT (53)       NULL,
    [ExpiringRentPercentOfTotalInNextFiveYears] FLOAT (53)       NULL,
    [OfficeIndustrialNote]                      NVARCHAR (1000)  NULL,
    [IsMultifamily]                             BIT              NULL,
    [StabilizedOccupancy]                       FLOAT (53)       NULL,
    [OtherIncomePerUnit]                        MONEY            NULL,
    [OperatingExpensesPerUnit]                  MONEY            NULL,
    [OperatingMargin]                           FLOAT (53)       NULL,
    [ReplacementReserves]                       MONEY            NULL,
    [MultifamilyNote]                           NVARCHAR (1000)  NULL,
    [IsRetail]                                  BIT              NULL,
    [OperatingExpensesPerSF]                    MONEY            NULL,
    [InlineTenantImprovements]                  MONEY            NULL,
    [AnchorTenantImprovements]                  MONEY            NULL,
    [InlineMarketRent]                          MONEY            NULL,
    [AnchorMarketRent]                          MONEY            NULL,
    [InlineSalesPerSF]                          MONEY            NULL,
    [InlineOccupancyCost]                       MONEY            NULL,
    [InlineOccupancyCostPct]                    FLOAT (53)       NULL,
    [IsOneAnchorAGrocery]                       BIT              CONSTRAINT [DF_PropertyValuations_IsOneAnchorAGrocery] DEFAULT ((0)) NULL,
    [AnchorTenants]                             NVARCHAR (500)   NULL,
    [AnchorSalesPerSqFt]                        MONEY            NULL,
    [RetailNote]                                NVARCHAR (1000)  NULL,
    [IsHotel]                                   BIT              NULL,
    [FiveYrRevPARCAGR]                          FLOAT (53)       NULL,
    [TenYrRevPARCAGR]                           FLOAT (53)       NULL,
    [CurrentYear]                               INT              NULL,
    [CurrentOccupancy]                          MONEY            NULL,
    [CurrentADR]                                MONEY            NULL,
    [CurrentRevPAR]                             MONEY            NULL,
    [GrossOperatingMarginPct]                   FLOAT (53)       NULL,
    [CurrentNOI]                                MONEY            NULL,
    [CurrentCapRate]                            FLOAT (53)       NULL,
    [CurrentEBITDA]                             MONEY            NULL,
    [FFEReserve]                                FLOAT (53)       NULL,
    [Flag]                                      NVARCHAR (50)    NULL,
    [ManagementOrFranchise]                     NVARCHAR (50)    NULL,
    [Management]                                NVARCHAR (250)   NULL,
    [ManagementUnencumbered]                    BIT              CONSTRAINT [DF_PropertyValuations_ManagementUnencumbered] DEFAULT ((0)) NULL,
    [Brand]                                     NVARCHAR (250)   NULL,
    [BrandUnencumbered]                         BIT              CONSTRAINT [DF_PropertyValuations_BrandUnencumbered] DEFAULT ((0)) NULL,
    [PIPAmount]                                 MONEY            NULL,
    [HotelNote]                                 NVARCHAR (1000)  NULL,
    [IsCondo]                                   BIT              NULL,
    [TotalUnitsForSale]                         INT              NULL,
    [ProjectedUnitsSold]                        INT              NULL,
    [ProjectedSFSold]                           INT              NULL,
    [UnitSalesStartDate]                        DATETIME         NULL,
    [SalesPerMonth]                             INT              NULL,
    [UnitSalesEndDate]                          DATETIME         NULL,
    [TotalSelloutPeriod]                        INT              NULL,
    [AvgSalesPricePerSF]                        MONEY            NULL,
    [AvgSalesPricePerUnit]                      MONEY            NULL,
    [GrossSalesPRoceeds]                        MONEY            NULL,
    [SalesCost]                                 MONEY            NULL,
    [CondoNote]                                 NVARCHAR (1000)  NULL,
    [IsDevelopment]                             BIT              NULL,
    [BuildablePropertySize]                     INT              NULL,
    [BuildablePropertySFOrUnits]                INT              NULL,
    [BuildablePropertyNote]                     NVARCHAR (1000)  NULL,
    [DevelopmentCost]                           MONEY            NULL,
    [DevelopmentCostPerBuildableSFOrUnit]       MONEY            NULL,
    [DevelopmentStartDate]                      DATETIME         NULL,
    [DevelopmentPeriod]                         INT              NULL,
    [DevelopmentEndDate]                        DATETIME         NULL,
    [StabilizedNOI]                             MONEY            NULL,
    [StabilizationYear]                         INT              NULL,
    [StabilizedPropertyValue]                   MONEY            NULL,
    [StabilizedValuePerSFOrUnit]                MONEY            NULL,
    [StabilizedCapRate]                         FLOAT (53)       NULL,
    [ReturnOnCost]                              FLOAT (53)       NULL,
    [ReturnOnCostNote]                          VARBINARY (1000) NULL,
    [DevelopmentNote]                           NVARCHAR (1000)  NULL,
    [DateAdded]                                 DATETIME         NULL,
    [LastUpdate]                                DATETIME         NULL,
    [RowVersion]                                ROWVERSION       NULL,
    [AddedBy]                                   NVARCHAR (100)   NULL,
    [UpdatedBy]                                 NVARCHAR (100)   NULL,
    CONSTRAINT [PK_PropertyValuations] PRIMARY KEY CLUSTERED ([PropertyValuationID] ASC),
    CONSTRAINT [FK_PropertyValuations_Employees] FOREIGN KEY ([PostedByEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_PropertyValuations_ProjectSizeTypes] FOREIGN KEY ([ProjectSizeTypeID]) REFERENCES [dbo].[ProjectSizeTypes] ([ProjectSizeTypeID]),
    CONSTRAINT [FK_PropertyValuations_Properties] FOREIGN KEY ([PropertyID]) REFERENCES [dbo].[Properties] ([PropertyID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'Year5NOI';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'Year1NOI';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'Year10NOI';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ValuePerUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'Value';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ValuationDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'UnitSalesStartDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'UnitSalesEndDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'UnderwrittenLeaseTerm';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'TotalUnitsForSale';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'TotalSelloutPeriod';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'TenYrRevPARCAGR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'TenYrNOICAGR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'TenYrMarketRentCAGR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'StabilizedValuePerSFOrUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'StabilizedPropertyValue';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'StabilizedOccupancy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'StabilizedNOI';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'StabilizedCapRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'StabilizationYear';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'SalesPerMonth';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'SalesCost';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ReturnOnCostNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ReturnOnCost';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'RetailNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ReplacementReserves';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ReimbursementMethod';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'PropertyValuationID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'PropertyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ProjectSizeTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ProjectedUnitsSold';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ProjectedSFSold';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'PostedByEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'PIPAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'OtherIncomePerUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'OperatingMargin';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'OperatingExpensesPerUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'OperatingExpensesPerSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'OfficeIndustrialNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'MultifamilyNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'MostRecentSeller';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'MarketTenantImprovements';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'MarketRent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ManagementUnencumbered';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ManagementOrFranchise';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'Management';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'JVOwnershipList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'JVOwnership';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'IsRetail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'IsOneAnchorAGrocery';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'IsOfficeIndustrial';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'IsMultifamily';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'IsHotel';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'IsDevelopment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'IsCondo';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'InPlaceOccupany';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'InPlaceNOI';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'InlineTenantImprovements';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'InlineSalesPerSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'InlineOccupancyCostPct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'InlineOccupancyCost';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'InlineMarketRent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'HotelNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'GrossSalesPRoceeds';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'GrossOperatingMarginPct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'GeneralVacancy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'Flag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'FiveYrRevPARCAGR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'FiveYrNOICAGR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'FiveYrMarketRentCAGR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'FFEReserve';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ExpiringSFPercentOfTotalInNextFiveYears';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ExpiringSFInNextFiveYears';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ExpiringRentPercentOfTotalInNextFiveYears';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ExpiringRentInNextFiveYears';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'ExpenseRatio';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'DevelopmentStartDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'DevelopmentPeriod';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'DevelopmentNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'DevelopmentEndDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'DevelopmentCostPerBuildableSFOrUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'DevelopmentCost';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'DateSold';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'CurrentYear';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'CurrentRevPAR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'CurrentOwner';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'CurrentOccupancy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'CurrentNOI';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'CurrentEBITDA';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'CurrentCapRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'CurrentADR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'CondoNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'BuildablePropertySize';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'BuildablePropertySFOrUnits';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'BuildablePropertyNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'BrandUnencumbered';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'Brand';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'AvgSalesPricePerUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'AvgSalesPricePerSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'AnchorTenants';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'AnchorTenantImprovements';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'AnchorSalesPerSqFt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'AnchorMarketRent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuations', @level2type = N'COLUMN', @level2name = N'AddedBy';




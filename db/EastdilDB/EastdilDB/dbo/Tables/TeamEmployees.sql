﻿CREATE TABLE [dbo].[TeamEmployees] (
    [TeamMemberID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [TeamID]       BIGINT         NULL,
    [EmployeeID]   BIGINT         NULL,
    [TeamRoleID]   INT            NOT NULL,
    [Notes]        NVARCHAR (250) NULL,
    [DateAdded]    DATETIME       NULL,
    [LastUpdate]   DATETIME       NULL,
    [AddedBy]      NVARCHAR (100) NULL,
    [UpdatedBy]    NVARCHAR (100) NULL,
    CONSTRAINT [PK_TeamEmployees] PRIMARY KEY CLUSTERED ([TeamMemberID] ASC),
    CONSTRAINT [FK_TeamEmployees_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_TeamEmployees_TeamRoles] FOREIGN KEY ([TeamRoleID]) REFERENCES [dbo].[TeamRoles] ([TeamRoleID]),
    CONSTRAINT [FK_TeamEmployees_Teams] FOREIGN KEY ([TeamID]) REFERENCES [dbo].[Teams] ([TeamID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees', @level2type = N'COLUMN', @level2name = N'TeamRoleID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees', @level2type = N'COLUMN', @level2name = N'TeamMemberID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees', @level2type = N'COLUMN', @level2name = N'TeamID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees', @level2type = N'COLUMN', @level2name = N'Notes';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TeamEmployees', @level2type = N'COLUMN', @level2name = N'AddedBy';




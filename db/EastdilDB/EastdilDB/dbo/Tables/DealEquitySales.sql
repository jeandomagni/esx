﻿CREATE TABLE [dbo].[DealEquitySales] (
    [DealEquitySaleID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealID]                     BIGINT         NULL,
    [Valuation]                  MONEY          NULL,
    [ProjectSize]                INT            NULL,
    [ProjectSizeTypeID]          INT            NULL,
    [ValuationPerUnitOfMeasure]  MONEY          NULL,
    [Occupancy]                  FLOAT (53)     NULL,
    [CapRate1]                   FLOAT (53)     NULL,
    [CapRate1Description]        NVARCHAR (250) NULL,
    [CapRate2]                   FLOAT (53)     NULL,
    [CapRate2Description]        NVARCHAR (250) NULL,
    [ExitCapRate]                FLOAT (53)     NULL,
    [UnleveredIRR]               FLOAT (53)     NULL,
    [UnleveredIRRDescription]    NVARCHAR (250) NULL,
    [LeveredIRR]                 FLOAT (53)     NULL,
    [LeveredIRRDescription]      NVARCHAR (250) NULL,
    [CashOnCash]                 FLOAT (53)     NULL,
    [CashOnCashDescription]      NVARCHAR (250) NULL,
    [PctLeased]                  FLOAT (53)     NULL,
    [WasDebtAssumed]             BIT            NULL,
    [WasNewDebtPlaced]           BIT            NULL,
    [Lender]                     NVARCHAR (250) NULL,
    [LenderCompanyID]            BIGINT         NULL,
    [LenderTypeID]               INT            NULL,
    [TotalLoanAmt]               MONEY          NULL,
    [TotalLoanAmtBySqFt]         MONEY          NULL,
    [LoanRateTypeID]             INT            NULL,
    [MaturityDate]               DATETIME       NULL,
    [InterestRate]               FLOAT (53)     NULL,
    [Amortization]               FLOAT (53)     NULL,
    [LTV]                        FLOAT (53)     NULL,
    [DebtYield]                  FLOAT (53)     NULL,
    [EstimatedAnnualDebtService] MONEY          NULL,
    [DSCR]                       NVARCHAR (50)  NULL,
    [PrepaymentString]           NVARCHAR (250) NULL,
    [BuyerName]                  NVARCHAR (250) NULL,
    [BuyerCompanyID]             BIGINT         NULL,
    [BuyerTypeID]                INT            NULL,
    [SellerName]                 NVARCHAR (250) NULL,
    [SellerCompanyID]            BIGINT         NULL,
    [SellerTypeID]               INT            NULL,
    [JVSale]                     BIT            NULL,
    [PercentInterestSold]        FLOAT (53)     NULL,
    [TopBidders]                 NVARCHAR (500) NULL,
    [OffShoreBidders]            NVARCHAR (500) NULL,
    [LastUpdate]                 DATETIME       NULL,
    [RowVersion]                 ROWVERSION     NULL,
    [DateAdded]                  DATETIME       NULL,
    [AddedBy]                    NVARCHAR (100) NULL,
    [UpdatedBy]                  NVARCHAR (100) NULL,
    CONSTRAINT [PK_DealEquitySales] PRIMARY KEY CLUSTERED ([DealEquitySaleID] ASC),
    CONSTRAINT [FK_DealEquitySales_BuyerCompanies] FOREIGN KEY ([BuyerCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealEquitySales_BuyerOrSponsorTypes] FOREIGN KEY ([BuyerTypeID]) REFERENCES [dbo].[BuyerTypes] ([BuyerTypeID]),
    CONSTRAINT [FK_DealEquitySales_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealEquitySales_LenderCompanies] FOREIGN KEY ([LenderCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealEquitySales_LenderTypes] FOREIGN KEY ([LenderTypeID]) REFERENCES [dbo].[LenderTypes] ([LenderTypeID]),
    CONSTRAINT [FK_DealEquitySales_LoanRateTypes] FOREIGN KEY ([LoanRateTypeID]) REFERENCES [dbo].[LoanRateTypes] ([LoanRateTypeID]),
    CONSTRAINT [FK_DealEquitySales_ProjectSizeTypes] FOREIGN KEY ([ProjectSizeTypeID]) REFERENCES [dbo].[ProjectSizeTypes] ([ProjectSizeTypeID]),
    CONSTRAINT [FK_DealEquitySales_SellerCompanies] FOREIGN KEY ([SellerCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealEquitySales_SellerTypes] FOREIGN KEY ([SellerTypeID]) REFERENCES [dbo].[SellerTypes] ([SellerTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6138.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'WasNewDebtPlaced';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6138.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'WasDebtAssumed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1955 @ 6.28:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'ValuationPerUnitOfMeasure';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=959 @ 12.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'Valuation';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=41 @ 299.44:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'UnleveredIRRDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=108 @ 113.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'UnleveredIRR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 361.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'TotalLoanAmtBySqFt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=245 @ 50.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'TotalLoanAmt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=707 @ 17.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'TopBidders';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=6 @ 2046.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'SellerTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1014 @ 12.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'SellerName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1188 @ 10.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'SellerCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5 @ 2455.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'ProjectSizeTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2295 @ 5.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'ProjectSize';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25 @ 491.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'PrepaymentString';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 1534.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'PercentInterestSold';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=287 @ 42.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'PctLeased';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=194 @ 63.28:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'OffShoreBidders';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=224 @ 54.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'Occupancy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=142 @ 86.46:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'MaturityDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=57 @ 215.39:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'LTV';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 4092.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'LoanRateTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=19 @ 646.16:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'LeveredIRRDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=48 @ 255.77:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'LeveredIRR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8 @ 1534.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'LenderTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=135 @ 90.94:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'LenderCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=162 @ 75.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'Lender';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5047 @ 2.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6138.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'JVSale';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=101 @ 121.55:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'InterestRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 361.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'ExitCapRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'EstimatedAnnualDebtService';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=11 @ 1116.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'DSCR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=74 @ 165.91:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'DebtYield';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'DealEquitySaleID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25 @ 491.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'CashOnCashDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=110 @ 111.61:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'CashOnCash';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=11 @ 1116.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'CapRate2Description';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=76 @ 161.54:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'CapRate2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=95 @ 129.23:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'CapRate1Description';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=135 @ 90.94:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'CapRate1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=9 @ 1364.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'BuyerTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1091 @ 11.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'BuyerName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=568 @ 21.61:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'BuyerCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 1534.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'Amortization';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEquitySales', @level2type = N'COLUMN', @level2name = N'AddedBy';




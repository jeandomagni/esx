﻿CREATE TABLE [dbo].[ContactListContacts] (
    [ContactListContactID] BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ContactListID]        INT              NULL,
    [ContactID]            BIGINT           NULL,
    [CompanyID]            BIGINT           NULL,
    [RespEmployeeID]       BIGINT           NULL,
    [RsvpID]               INT              NULL,
    [RSVP]                 BIT              NULL,
    [NumAttending]         INT              NULL,
    [Field1]               NVARCHAR (1000)  NULL,
    [Field2]               NVARCHAR (1000)  NULL,
    [Field3]               NVARCHAR (1000)  NULL,
    [Field4]               NVARCHAR (1000)  NULL,
    [Field5]               NVARCHAR (1000)  NULL,
    [Field6]               NVARCHAR (1000)  NULL,
    [Comment]              NVARCHAR (4000)  NULL,
    [MarkDupes]            BIT              CONSTRAINT [DF__ContactLi__MarkD__6442E2C9] DEFAULT ((0)) NULL,
    [InsertFlag]           BIT              NULL,
    [UpdateFlag]           BIT              NULL,
    [DeleteFlag]           BIT              NULL,
    [LastUpdate]           DATETIME         CONSTRAINT [DF__ContactLi__LastU__65370702] DEFAULT (getdate()) NULL,
    [AddedByEmployeeName]  NVARCHAR (50)    NULL,
    [EditedByEmployeeName] NVARCHAR (50)    NULL,
    [msrepl_tran_version]  UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_B48195F6_3060_43C7_9102_9C62FEC033B2_1390627997] DEFAULT (newid()) NOT NULL,
    [RowVersion]           ROWVERSION       NOT NULL,
    [DateAdded]            DATETIME         NULL,
    [AddedBy]              NVARCHAR (100)   NULL,
    [UpdatedBy]            NVARCHAR (100)   NULL,
    CONSTRAINT [PK_ContactListContacts] PRIMARY KEY CLUSTERED ([ContactListContactID] ASC),
    CONSTRAINT [FK_ContactListContacts_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_ContactListContacts_ContactLists] FOREIGN KEY ([ContactListID]) REFERENCES [dbo].[ContactLists] ([ContactListID]),
    CONSTRAINT [FK_ContactListContacts_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contacts] ([ContactID]),
    CONSTRAINT [FK_ContactListContacts_Employees] FOREIGN KEY ([RespEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_ContactListContacts_RSVP] FOREIGN KEY ([RsvpID]) REFERENCES [dbo].[RSVP] ([RsvpID])
);








GO
CREATE NONCLUSTERED INDEX [ContactID]
    ON [dbo].[ContactListContacts]([ContactID] ASC);


GO
CREATE TRIGGER [dbo].[ContactListTotals_DeleteTrigger]
ON dbo.ContactListContacts 
AFTER DELETE
AS

DECLARE @NumRows int
SET @NumRows = (SELECT count(ContactListID) from deleted)

IF (@NumRows=1)
   BEGIN
   DECLARE @contactListID bigint
	SET @contactListID = (SELECT ContactListID from deleted)

	UPDATE ContactLists SET ContactLists.NumContacts=(SELECT COUNT(*) FROM ContactListContacts WHERE ContactListContacts.ContactListID=@contactListID) WHERE ContactLists.ContactListID=@contactListID

	UPDATE ContactLists SET ContactLists.NumActiveContacts=(SELECT COUNT(*) FROM ContactListContacts INNER JOIN Contacts ON Contacts.ContactID=ContactListContacts.ContactID 
	WHERE ContactListContacts.ContactListID=@contactListID AND Contacts.Active=1) 
	WHERE ContactLists.ContactListID=@contactListID
   END
GO
DISABLE TRIGGER [dbo].[ContactListTotals_DeleteTrigger]
    ON [dbo].[ContactListContacts];


GO



GO
CREATE TRIGGER [dbo].[ContactListTotals_InsertUpdateTrigger]
ON dbo.ContactListContacts 
AFTER INSERT, UPDATE
AS

DECLARE @NumRows int
SET @NumRows = (SELECT count(ContactListID) from inserted)

IF (@NumRows=1)
   BEGIN
   DECLARE @contactListID bigint
	SET @contactListID = (SELECT ContactListID from inserted)

	UPDATE ContactLists SET ContactLists.NumContacts=(SELECT COUNT(*) FROM ContactListContacts WHERE ContactListContacts.ContactListID=@contactListID) WHERE ContactLists.ContactListID=@contactListID

	UPDATE ContactLists SET ContactLists.NumActiveContacts=(SELECT COUNT(*) FROM ContactListContacts INNER JOIN Contacts ON Contacts.ContactID=ContactListContacts.ContactID 
	WHERE ContactListContacts.ContactListID=@contactListID AND Contacts.Active=1) 
	WHERE ContactLists.ContactListID=@contactListID
   END
GO
DISABLE TRIGGER [dbo].[ContactListTotals_InsertUpdateTrigger]
    ON [dbo].[ContactListContacts];


GO

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 263060.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'UpdateFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 175373.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'RsvpID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 526121.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'RSVP';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=213 @ 2470.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'RespEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=11 @ 47829.18:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'NumAttending';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=359448 @ 1.46:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 526121.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'MarkDupes';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=281733 @ 1.87:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 526121.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'InsertFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25 @ 21044.84:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'Field6';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 65765.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'Field5';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=195 @ 2698.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'Field4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=480 @ 1096.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'Field3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1165 @ 451.61:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'Field2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1535 @ 342.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'Field1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 15474.15:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'EditedByEmployeeName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 526121.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'DeleteFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1350 @ 389.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'ContactListID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'ContactListContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=65350 @ 8.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25175 @ 20.90:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=214 @ 2458.51:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'Comment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=36 @ 14614.47:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'AddedByEmployeeName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=53 @ 9926.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactListContacts', @level2type = N'COLUMN', @level2name = N'AddedBy';




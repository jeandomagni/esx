﻿CREATE TABLE [dbo].[Contacts] (
    [ContactID]                    BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CompanyID]                    BIGINT           NULL,
    [FullName]                     NVARCHAR (100)   NULL,
    [FirstName]                    NVARCHAR (50)    NULL,
    [MiddleName]                   NVARCHAR (50)    NULL,
    [LastName]                     NVARCHAR (50)    NULL,
    [Nickname]                     NVARCHAR (50)    NULL,
    [FirstnameLastname]            NVARCHAR (100)   NULL,
    [LastnameFirstname]            NVARCHAR (100)   NULL,
    [Prefix]                       NVARCHAR (50)    NULL,
    [Suffix]                       NVARCHAR (50)    NULL,
    [Salutation]                   NVARCHAR (50)    NULL,
    [Title]                        NVARCHAR (100)   NULL,
    [Department]                   NVARCHAR (250)   NULL,
    [Other]                        NVARCHAR (50)    NULL,
    [AddressID]                    BIGINT           NULL,
    [Address2ID]                   BIGINT           NULL,
    [Address2For]                  NVARCHAR (50)    NULL,
    [Address2TypeID]               INT              NULL,
    [Phone]                        NVARCHAR (50)    NULL,
    [DirectPhone]                  NVARCHAR (50)    NULL,
    [MainPhoneExt]                 NVARCHAR (50)    NULL,
    [Fax]                          NVARCHAR (50)    NULL,
    [HomePhone]                    NVARCHAR (50)    NULL,
    [MobilePhone]                  NVARCHAR (50)    NULL,
    [BlackberryPhone]              NVARCHAR (50)    NULL,
    [OtherPhone]                   NVARCHAR (50)    NULL,
    [OtherPhoneType]               NVARCHAR (50)    NULL,
    [Pager]                        NVARCHAR (50)    NULL,
    [EmailAddress]                 NVARCHAR (128)   NULL,
    [SecondaryEmail]               NVARCHAR (128)   NULL,
    [WebAddress]                   NVARCHAR (255)   NULL,
    [Assistant]                    NVARCHAR (64)    NULL,
    [AssistantPhone]               NVARCHAR (64)    NULL,
    [AssistantEmail]               NVARCHAR (128)   NULL,
    [Active]                       BIT              NULL,
    [ReasonInactive]               NVARCHAR (4000)  NULL,
    [InactivatedByEmployeeID]      BIGINT           NULL,
    [PreviousEmployers]            NVARCHAR (1000)  NULL,
    [SpecialConditions]            BIT              NULL,
    [ETeasersBlocked]              BIT              NULL,
    [ApprovalRequiredForMarketing] BIT              NULL,
    [MarketingApprovalEmployeeID]  BIGINT           NULL,
    [MarketingApprovalCCTeam]      NVARCHAR (150)   NULL,
    [Notes]                        NVARCHAR (1000)  NULL,
    [DRENumber]                    NVARCHAR (50)    NULL,
    [ESContact]                    NVARCHAR (255)   NULL,
    [ESContactEmployeeID]          BIGINT           NULL,
    [KeyContact]                   BIT              NULL,
    [WellsFargoContact]            NVARCHAR (100)   NULL,
    [BestRelationship]             NVARCHAR (100)   NULL,
    [RetailRelationship]           NVARCHAR (100)   NULL,
    [EntityType]                   NVARCHAR (100)   NULL,
    [InvestorType]                 NVARCHAR (100)   NULL,
    [ContactType]                  NVARCHAR (64)    NULL,
    [CategoryID]                   INT              NOT NULL,
    [Qualified]                    BIT              NULL,
    [DoNotSolicit]                 BIT              NULL,
    [DoNotSolicitAsOfDate]         DATETIME         NULL,
    [DoNotSolicitRequestID]        INT              NULL,
    [ExpressedConsent]             BIT              NULL,
    [DoNotPhone]                   BIT              NULL,
    [DoNotEmail]                   BIT              NULL,
    [DoNotMail]                    BIT              NULL,
    [DoNotFax]                     BIT              NULL,
    [PrefersDigitalOM]             BIT              NULL,
    [IsPrimary]                    BIT              NULL,
    [Principal]                    BIT              NULL,
    [Broker]                       BIT              NULL,
    [Investor]                     BIT              NULL,
    [Client]                       BIT              NULL,
    [Vendor]                       BIT              NULL,
    [Spouse]                       NVARCHAR (100)   NULL,
    [Referral]                     NVARCHAR (100)   NULL,
    [Status]                       NVARCHAR (64)    NULL,
    [REIBContact]                  BIT              NULL,
    [REIBCorporateOfficerTitle]    NVARCHAR (100)   NULL,
    [REIBBoardMemberTitle]         NVARCHAR (100)   NULL,
    [NameChange]                   BIT              NULL,
    [AddressChange]                BIT              NULL,
    [SentToCDC]                    BIT              NULL,
    [SentToCDCDate]                DATETIME         NULL,
    [CanadaOptIn]                  NVARCHAR (50)    NULL,
    [AutoInExpirationDate]         DATETIME         NULL,
    [CriteriaFlag]                 BIT              NULL,
    [CriteriaDealID]               BIGINT           NULL,
    [NumDealsOn]                   INT              NULL,
    [CALogged]                     INT              NULL,
    [DealConvPct]                  INT              NULL,
    [EmailUpdateFlag]              BIT              NULL,
    [UserEmail]                    NVARCHAR (100)   NULL,
    [DateAdded]                    DATETIME         NULL,
    [LastUpdate]                   DATETIME         NULL,
    [msrepl_tran_version]          UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_FE745DD3_1459_4890_8F12_44FF6B0717C2_430624577] DEFAULT (newid()) NOT NULL,
    [RowVersion]                   ROWVERSION       NOT NULL,
    [DoNotAddToContactLists]       BIT              NULL,
    [AddedBy]                      NVARCHAR (100)   NULL,
    [UpdatedBy]                    NVARCHAR (100)   NULL,
    CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED ([ContactID] ASC),
    CONSTRAINT [FK_Contacts_Addresses] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[Addresses] ([AddressID]),
    CONSTRAINT [FK_Contacts_Addresses1] FOREIGN KEY ([Address2ID]) REFERENCES [dbo].[Addresses] ([AddressID]),
    CONSTRAINT [FK_Contacts_AddressTypes] FOREIGN KEY ([Address2TypeID]) REFERENCES [dbo].[AddressTypes] ([AddressTypeID]),
    CONSTRAINT [FK_Contacts_Categories] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Categories] ([CategoryID]),
    CONSTRAINT [FK_Contacts_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_Contacts_DoNotSolicitRequest] FOREIGN KEY ([DoNotSolicitRequestID]) REFERENCES [dbo].[DoNotSolicitRequest] ([DoNotSolicitRequestID]),
    CONSTRAINT [FK_Contacts_Employees] FOREIGN KEY ([MarketingApprovalEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_Contacts_Employees_ESContactEmployeeID] FOREIGN KEY ([ESContactEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_Contacts_Employees1_InactivatedBYEmployeeID] FOREIGN KEY ([InactivatedByEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);






GO
CREATE NONCLUSTERED INDEX [EmailAddress]
    ON [dbo].[Contacts]([EmailAddress] ASC);


GO
CREATE NONCLUSTERED INDEX [LastName]
    ON [dbo].[Contacts]([LastName] ASC, [FirstName] ASC);


GO
CREATE TRIGGER [dbo].[CompanyTotals_DeleteTrigger]
ON dbo.Contacts 
AFTER DELETE
AS

DECLARE @NumRows int
SET @NumRows = (SELECT count(CompanyID) from deleted)

IF (@NumRows=1)
   BEGIN
   DECLARE @companyID bigint
	SET @companyID = (SELECT CompanyID from deleted)

	UPDATE Companies SET Companies.NumContacts=(SELECT COUNT(*) FROM Contacts Contacts WHERE CompanyID=@companyID) WHERE Companies.CompanyID=@companyID

	UPDATE Companies SET Companies.NumActiveContacts=(SELECT COUNT(*) FROM Contacts WHERE Contacts.CompanyID=@companyID AND Contacts.Active=1)
	WHERE Companies.CompanyID=@companyID
   END
GO
DISABLE TRIGGER [dbo].[CompanyTotals_DeleteTrigger]
    ON [dbo].[Contacts];


GO
CREATE TRIGGER [dbo].[CompanyTotals_InsertUpdateTrigger]
ON dbo.Contacts 
AFTER INSERT, UPDATE
AS
DECLARE @NumRows int
SET @NumRows = (SELECT count(CompanyID) from inserted)

IF (@NumRows=1)
   BEGIN
   DECLARE @companyID bigint
	SET @companyID = (SELECT CompanyID from inserted)

	UPDATE Companies SET Companies.NumContacts=(SELECT COUNT(*) FROM Contacts WHERE CompanyID=@companyID) WHERE Companies.CompanyID=@companyID

	UPDATE Companies SET Companies.NumActiveContacts=(SELECT COUNT(*) FROM Contacts WHERE Contacts.CompanyID=@companyID AND Contacts.Active=1) 
	WHERE Companies.CompanyID=@companyID
   END
GO
DISABLE TRIGGER [dbo].[CompanyTotals_InsertUpdateTrigger]
    ON [dbo].[Contacts];


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=148 @ 757.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'WellsFargoContact';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9192 @ 12.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'WebAddress';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 112155.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Vendor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'UserEmail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12956 @ 8.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Title';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=143 @ 784.30:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Suffix';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 22431.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Status';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=70 @ 1602.21:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Spouse';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'SpecialConditions';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=79 @ 1419.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'SentToCDCDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'SentToCDC';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1256 @ 89.30:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'SecondaryEmail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=17011 @ 6.59:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Salutation';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=24 @ 4673.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'RetailRelationship';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'REIBCorporateOfficerTitle';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'REIBContact';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'REIBBoardMemberTitle';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=26 @ 4313.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Referral';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4599 @ 24.39:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'ReasonInactive';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Qualified';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 112155.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Principal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=64 @ 1752.42:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'PreviousEmployers';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=472 @ 237.62:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Prefix';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'PrefersDigitalOM';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Phone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=115 @ 975.26:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Pager';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=462 @ 242.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'OtherPhoneType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1565 @ 71.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'OtherPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Other';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1110 @ 101.04:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'NumDealsOn';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1722 @ 65.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Notes';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Nickname';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 112155.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'NameChange';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=66352 @ 1.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=29885 @ 3.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'MobilePhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1284 @ 87.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'MiddleName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=42 @ 2670.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'MarketingApprovalEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 12461.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'MarketingApprovalCCTeam';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3459 @ 32.42:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'MainPhoneExt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=43816 @ 2.56:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=92523 @ 1.21:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'LastnameFirstname';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=41529 @ 2.70:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'LastName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 112155.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'KeyContact';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'IsPrimary';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=39 @ 2875.77:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'InvestorType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Investor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=132 @ 849.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'InactivatedByEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1172 @ 95.70:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'HomePhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=103541 @ 1.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'FullName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=92401 @ 1.21:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'FirstnameLastname';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=9843 @ 11.39:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'FirstName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=44700 @ 2.51:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Fax';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'ExpressedConsent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'ETeasersBlocked';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=177 @ 633.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'ESContactEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2313 @ 48.49:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'ESContact';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=123 @ 911.83:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'EntityType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'EmailUpdateFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=86983 @ 1.29:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'EmailAddress';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=18 @ 6230.83:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DRENumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 28038.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DoNotSolicitRequestID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=189 @ 593.41:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DoNotSolicitAsOfDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DoNotSolicit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 112155.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DoNotPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 112155.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DoNotMail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 112155.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DoNotFax';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DoNotEmail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DoNotAddToContactLists';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=81634 @ 1.37:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DirectPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1250 @ 89.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Department';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=92 @ 1219.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DealConvPct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=76707 @ 1.46:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'CriteriaFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 112155.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'CriteriaDealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 16022.14:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'ContactType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=44668 @ 2.51:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Client';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=6 @ 18692.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'CategoryID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'CanadaOptIn';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=225 @ 498.47:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'CALogged';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Broker';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=267 @ 420.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'BlackberryPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=59 @ 1900.93:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'BestRelationship';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'AutoInExpirationDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1557 @ 72.03:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'AssistantPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1434 @ 78.21:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'AssistantEmail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2157 @ 52.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Assistant';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'ApprovalRequiredForMarketing';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=112138 @ 1.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'AddressID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'AddressChange';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 28038.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Address2TypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=28733 @ 3.90:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Address2ID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 28038.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Address2For';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 56077.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contacts', @level2type = N'COLUMN', @level2name = N'Active';




﻿CREATE TABLE [dbo].[RegionalMarketCommentary] (
    [RegionalMarketCommentaryID] INT            IDENTITY (1, 1) NOT NULL,
    [RegionalMarketID]           INT            NULL,
    [RegionalSubmarketID]        INT            NULL,
    [PropertyTypeCategoryID]     INT            NULL,
    [SubmarketComment]           BIT            NULL,
    [Comment]                    NVARCHAR (MAX) NULL,
    [DateUpdated]                DATETIME       NULL,
    [EditBy]                     NVARCHAR (50)  NULL,
    [DateAdded]                  DATETIME       NULL,
    [LastUpdate]                 DATETIME       NULL,
    [AddedBy]                    NVARCHAR (100) NULL,
    [UpdatedBy]                  NVARCHAR (100) NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 8.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'SubmarketComment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=13 @ 1.31:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'RegionalSubmarketID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=11 @ 1.55:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'RegionalMarketID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'RegionalMarketCommentaryID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 17.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'PropertyTypeCategoryID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 17.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'EditBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 17.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'DateUpdated';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'Comment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RegionalMarketCommentary', @level2type = N'COLUMN', @level2name = N'AddedBy';




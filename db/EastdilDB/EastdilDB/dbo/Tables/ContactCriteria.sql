﻿CREATE TABLE [dbo].[ContactCriteria] (
    [ContactCriteriaID]      BIGINT         IDENTITY (1, 1) NOT NULL,
    [ContactID]              BIGINT         NULL,
    [CONTACTID_SLX]          CHAR (12)      NULL,
    [Debt]                   BIT            NULL,
    [Equity]                 BIT            NULL,
    [TranSizeLT25M]          BIT            NULL,
    [TranSize25_50M]         BIT            NULL,
    [TranSize50_100M]        BIT            NULL,
    [TranSizeGT100M]         BIT            NULL,
    [MktFocusPrimary]        BIT            NULL,
    [MktFocusSecondary]      BIT            NULL,
    [MktFocusTertiary]       BIT            NULL,
    [GeoAreaMidwest]         BIT            NULL,
    [GeoAreaNational]        BIT            NULL,
    [GeoAreaNortheast]       BIT            NULL,
    [GeoAreaNorthwest]       BIT            NULL,
    [GeoAreaSoutheast]       BIT            NULL,
    [GeoAreaSouthwest]       BIT            NULL,
    [GeoAreaWest]            BIT            NULL,
    [GeoAreaNonUS]           BIT            NULL,
    [LoanBuyersFinancing]    BIT            NULL,
    [LoanBuyersCDO]          BIT            NULL,
    [LoanBuyersDevelopment]  BIT            NULL,
    [LoanBuyersExisting]     BIT            NULL,
    [LoanBuyersHighYield]    BIT            NULL,
    [LoanBuyersLB_Medical]   BIT            NULL,
    [LoanBuyersMezzanine]    BIT            NULL,
    [LoanBuyersPerforming]   BIT            NULL,
    [ProdTypeOffice]         BIT            NULL,
    [ProdTypeOfficeSuburban] BIT            NULL,
    [ProdTypeOfficeCBD]      BIT            NULL,
    [MultiFamily]            BIT            NULL,
    [MultiFamilyHiRise]      BIT            NULL,
    [MultiFamilyGarden]      BIT            NULL,
    [Retail]                 BIT            NULL,
    [RetailMalls]            BIT            NULL,
    [RetailStrip]            BIT            NULL,
    [Lodging]                BIT            NULL,
    [LodgingLimitedService]  BIT            NULL,
    [LodgingLuxury]          BIT            NULL,
    [EquityDevelopment]      BIT            NULL,
    [Industrial]             BIT            NULL,
    [NetLease]               BIT            NULL,
    [Land]                   BIT            NULL,
    [Medical]                BIT            NULL,
    [SeniorLiving]           BIT            NULL,
    [GolfCourse]             BIT            NULL,
    [LastUpdate]             DATETIME       NULL,
    [RowVersion]             ROWVERSION     NOT NULL,
    [DateAdded]              DATETIME       NULL,
    [AddedBy]                NVARCHAR (100) NULL,
    [UpdatedBy]              NVARCHAR (100) NULL,
    CONSTRAINT [PK_ContactCriteria] PRIMARY KEY CLUSTERED ([ContactCriteriaID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'TranSizeLT25M';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'TranSizeGT100M';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'TranSize50_100M';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'TranSize25_50M';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'SeniorLiving';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'RetailStrip';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'RetailMalls';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'Retail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'ProdTypeOfficeSuburban';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'ProdTypeOfficeCBD';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'ProdTypeOffice';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'NetLease';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'MultiFamilyHiRise';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'MultiFamilyGarden';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'MultiFamily';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'MktFocusTertiary';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'MktFocusSecondary';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'MktFocusPrimary';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'Medical';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LodgingLuxury';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LodgingLimitedService';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 68455.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'Lodging';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LoanBuyersPerforming';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LoanBuyersMezzanine';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LoanBuyersLB_Medical';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LoanBuyersHighYield';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LoanBuyersFinancing';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LoanBuyersExisting';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LoanBuyersDevelopment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LoanBuyersCDO';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'Land';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'Industrial';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'GolfCourse';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'GeoAreaWest';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'GeoAreaSouthwest';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'GeoAreaSoutheast';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'GeoAreaNorthwest';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'GeoAreaNortheast';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'GeoAreaNonUS';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'GeoAreaNational';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'GeoAreaMidwest';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'EquityDevelopment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'Equity';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 34227.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'Debt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'CONTACTID_SLX';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'ContactCriteriaID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContactCriteria', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[Documents] (
    [DocumentID]              BIGINT          IDENTITY (1, 1) NOT NULL,
    [CompanyID]               BIGINT          NULL,
    [ContactID]               BIGINT          NULL,
    [DealID]                  BIGINT          NULL,
    [PropertyID]              BIGINT          NULL,
    [LoanID]                  BIGINT          NULL,
    [DealContactID]           BIGINT          NULL,
    [DocumentForID]           INT             NULL,
    [FileName]                NVARCHAR (500)  NULL,
    [DocumentTypeID]          INT             NULL,
    [BestPracticeDoc]         BIT             NULL,
    [PrimaryAuthorEmployeeID] BIGINT          NULL,
    [PostedByEmployeeID]      BIGINT          NULL,
    [StorageLocation]         NVARCHAR (1000) NULL,
    [DateCreated]             DATETIME        NULL,
    [DateAdded]               DATETIME        NULL,
    [LastUpdate]              DATETIME        NULL,
    [RowVersion]              ROWVERSION      NULL,
    [DocumentTitle]           NVARCHAR (50)   NULL,
    [ContentNotes]            NVARCHAR (50)   NULL,
    [ArchiveFilePath]         NVARCHAR (50)   NULL,
    [SourceFilePath]          NVARCHAR (50)   NULL,
    [AddedBy]                 NVARCHAR (100)  NULL,
    [UpdatedBy]               NVARCHAR (100)  NULL,
    CONSTRAINT [PK_Documents] PRIMARY KEY CLUSTERED ([DocumentID] ASC),
    CONSTRAINT [FK_Documents_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_Documents_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contacts] ([ContactID]),
    CONSTRAINT [FK_Documents_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_Documents_DealsContacts] FOREIGN KEY ([DealContactID]) REFERENCES [dbo].[DealContacts] ([DealContactID]),
    CONSTRAINT [FK_Documents_DocumentFor] FOREIGN KEY ([DocumentForID]) REFERENCES [dbo].[DocumentFor] ([DocumentForID]),
    CONSTRAINT [FK_Documents_DocumentTypes] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentTypes] ([DocumentTypeID]),
    CONSTRAINT [FK_Documents_Employees] FOREIGN KEY ([PostedByEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_Documents_Employees1] FOREIGN KEY ([PrimaryAuthorEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_Documents_Loans] FOREIGN KEY ([LoanID]) REFERENCES [dbo].[Loans] ([LoanID]),
    CONSTRAINT [FK_Documents_Properties] FOREIGN KEY ([PropertyID]) REFERENCES [dbo].[Properties] ([PropertyID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=9274 @ 1.04:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'StorageLocation';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'SourceFilePath';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'PropertyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=288 @ 33.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'PrimaryAuthorEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=46 @ 210.61:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'PostedByEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'LoanID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8948 @ 1.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'FileName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=17 @ 569.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'DocumentTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'DocumentTitle';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'DocumentID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 3229.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'DocumentForID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3596 @ 2.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'DealContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1094 @ 8.86:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'DateCreated';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'ContentNotes';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1472 @ 6.58:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 4844.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'BestPracticeDoc';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'ArchiveFilePath';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Documents', @level2type = N'COLUMN', @level2name = N'AddedBy';




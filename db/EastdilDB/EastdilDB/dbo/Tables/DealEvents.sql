﻿CREATE TABLE [dbo].[DealEvents] (
    [DealEventID]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealID]              BIGINT         NULL,
    [EmployeeID]          BIGINT         NULL,
    [EventTypeID]         INT            NULL,
    [Title]               NVARCHAR (50)  NULL,
    [Description]         NVARCHAR (100) NULL,
    [Start]               DATETIME       NULL,
    [End]                 DATETIME       NULL,
    [IsAllDay]            BIT            NULL,
    [RecurrenceRule]      NVARCHAR (50)  NULL,
    [RecurrenceID]        INT            NULL,
    [RecurrenceException] NVARCHAR (100) NULL,
    [LastUpdate]          DATETIME       NULL,
    [RowVersion]          ROWVERSION     NULL,
    [DateAdded]           DATETIME       NULL,
    [AddedBy]             NVARCHAR (100) NULL,
    [UpdatedBy]           NVARCHAR (100) NULL,
    CONSTRAINT [PK_DealEvents] PRIMARY KEY CLUSTERED ([DealEventID] ASC),
    CONSTRAINT [FK_DealEvents_Deals] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealEvents_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_DealEvents_EventTypes] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventTypes] ([EventTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4420 @ 1.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'Title';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=611 @ 7.24:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'Start';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'RecurrenceRule';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'RecurrenceID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'RecurrenceException';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 4421.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'IsAllDay';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=10 @ 442.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'EventTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=611 @ 7.24:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'End';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=53 @ 83.42:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=11 @ 401.91:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'Description';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3372 @ 1.31:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'DealEventID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealEvents', @level2type = N'COLUMN', @level2name = N'AddedBy';




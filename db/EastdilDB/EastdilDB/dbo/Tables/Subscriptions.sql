﻿CREATE TABLE [dbo].[Subscriptions] (
    [SubscriptionID]      INT            IDENTITY (1, 1) NOT NULL,
    [Subscription]        NVARCHAR (50)  NULL,
    [Description]         NVARCHAR (50)  NULL,
    [Vendor]              NVARCHAR (50)  NULL,
    [RenewalDate]         DATETIME       NULL,
    [Cost]                NVARCHAR (50)  NULL,
    [InstallInstructions] NVARCHAR (50)  NULL,
    [ApprovedBy]          NVARCHAR (50)  NULL,
    [DateAdded]           DATETIME       NULL,
    [LastUpdate]          DATETIME       NULL,
    [AddedBy]             NVARCHAR (100) NULL,
    [UpdatedBy]           NVARCHAR (100) NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=11 @ 1.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'Vendor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'SubscriptionID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'Subscription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 1.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'RenewalDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'InstallInstructions';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'Description';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'Cost';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'ApprovedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscriptions', @level2type = N'COLUMN', @level2name = N'AddedBy';




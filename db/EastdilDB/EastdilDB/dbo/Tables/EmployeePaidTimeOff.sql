﻿CREATE TABLE [dbo].[EmployeePaidTimeOff] (
    [PaidTimeOffID] INT             IDENTITY (1, 1) NOT NULL,
    [EmployeeID]    BIGINT          NULL,
    [WorkDate]      DATETIME        NULL,
    [PTOHrsUsed]    NUMERIC (19, 3) NULL,
    [Comment]       NVARCHAR (1000) NULL,
    [LastUpdate]    DATETIME        NULL,
    [RowVersion]    ROWVERSION      NOT NULL,
    [DateAdded]     DATETIME        NULL,
    [AddedBy]       NVARCHAR (100)  NULL,
    [UpdatedBy]     NVARCHAR (100)  NULL,
    CONSTRAINT [PK_EmployeePaidTimeOff] PRIMARY KEY CLUSTERED ([PaidTimeOffID] ASC),
    CONSTRAINT [FK_EmployeePaidTimeOff_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=Used in ESNet not ESP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1198 @ 6.48:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff', @level2type = N'COLUMN', @level2name = N'WorkDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=258 @ 30.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff', @level2type = N'COLUMN', @level2name = N'PTOHrsUsed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff', @level2type = N'COLUMN', @level2name = N'PaidTimeOffID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5063 @ 1.53:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=165 @ 47.03:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=64 @ 121.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff', @level2type = N'COLUMN', @level2name = N'Comment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeePaidTimeOff', @level2type = N'COLUMN', @level2name = N'AddedBy';




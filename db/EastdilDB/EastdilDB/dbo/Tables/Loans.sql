﻿CREATE TABLE [dbo].[Loans] (
    [LoanID]                      BIGINT          IDENTITY (1, 1) NOT NULL,
    [DealID]                      BIGINT          NULL,
    [DealCode]                    NVARCHAR (50)   NULL,
    [DealType]                    NVARCHAR (50)   NULL,
    [LoanName]                    NVARCHAR (300)  NULL,
    [LoanDescription]             NVARCHAR (500)  NULL,
    [LoanPriorityRank]            INT             NULL,
    [LoanTypeID]                  INT             NULL,
    [PropertyID]                  BIGINT          NULL,
    [Lender]                      NVARCHAR (250)  NULL,
    [LenderCompanyID]             BIGINT          NULL,
    [LenderDBA]                   NVARCHAR (50)   NULL,
    [SpecialServicerCompanyID]    BIGINT          NULL,
    [Borrowers]                   NVARCHAR (1000) NULL,
    [LoanBalance]                 MONEY           NULL,
    [IsOutstanding]               BIT             NOT NULL,
    [IsEquityLike]                BIT             NOT NULL,
    [ReserveBalance]              MONEY           NULL,
    [ReservesPerSFOrUnit]         MONEY           NULL,
    [ReservesNote]                NVARCHAR (1000) NULL,
    [PctOfPosition]               FLOAT (53)      NULL,
    [DebtSeniorToLoan]            MONEY           NULL,
    [DebtPariPassuToLoan]         MONEY           NULL,
    [DebtJuniorToLoan]            MONEY           NULL,
    [LoanLastDollar]              MONEY           NULL,
    [LoanLastDollarNetOFReserves] MONEY           NULL,
    [TotalCapitalStack]           MONEY           NULL,
    [OriginationDate]             DATETIME        NULL,
    [PaidToDate]                  DATETIME        NULL,
    [LoanPerfTypeID]              INT             NULL,
    [OriginalMaturityDate]        DATETIME        NULL,
    [NextMaturityDate]            DATETIME        NULL,
    [FinalMaturityDate]           DATETIME        NULL,
    [ExtensionOptions]            NVARCHAR (500)  NULL,
    [ExtensionFee]                FLOAT (53)      NULL,
    [ReferenceIndexID]            INT             NULL,
    [IndexRate]                   FLOAT (53)      NULL,
    [RateSpread]                  FLOAT (53)      NULL,
    [AllInInterestRate]           FLOAT (53)      NULL,
    [LoanRateTypeID]              INT             NULL,
    [LoanConstant]                FLOAT (53)      NULL,
    [Amortization]                INT             NULL,
    [First$LTV]                   FLOAT (53)      NULL,
    [Last$LTV]                    FLOAT (53)      NULL,
    [First$DebtYield]             FLOAT (53)      NULL,
    [Last$DebtYield]              FLOAT (53)      NULL,
    [First$PerSFOrUnit]           MONEY           NULL,
    [Last$PerSFOrUnit]            MONEY           NULL,
    [FirstDSCR]                   NVARCHAR (50)   NULL,
    [LastDSCR]                    NVARCHAR (50)   NULL,
    [DebtMetricsNote]             NVARCHAR (1000) NULL,
    [PrepaymentString]            NVARCHAR (500)  NULL,
    [CurrentCallProtection]       FLOAT (53)      NULL,
    [LockoutEndDate]              DATETIME        NULL,
    [CashManagement]              NVARCHAR (500)  NULL,
    [DateAdded]                   DATETIME        NULL,
    [SourceOfLoanData]            NVARCHAR (50)   NULL,
    [DealIDFromInsert]            BIGINT          NULL,
    [LastUpdate]                  DATETIME        NULL,
    [RowVersion]                  ROWVERSION      NOT NULL,
    [AddedBy]                     NVARCHAR (100)  NULL,
    [UpdatedBy]                   NVARCHAR (100)  NULL,
    CONSTRAINT [PK_Loans] PRIMARY KEY CLUSTERED ([LoanID] ASC),
    CONSTRAINT [FK_Loans_Companies] FOREIGN KEY ([LenderCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_Loans_Companies1] FOREIGN KEY ([SpecialServicerCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_Loans_Companies2] FOREIGN KEY ([LenderCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_Loans_Companies3] FOREIGN KEY ([SpecialServicerCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_Loans_LoanPerfTypes] FOREIGN KEY ([LoanPerfTypeID]) REFERENCES [dbo].[LoanPerfTypes] ([LoanPerfTypeID]),
    CONSTRAINT [FK_Loans_LoanRateTypes] FOREIGN KEY ([LoanRateTypeID]) REFERENCES [dbo].[LoanRateTypes] ([LoanRateTypeID]),
    CONSTRAINT [FK_Loans_LoanTypes] FOREIGN KEY ([LoanTypeID]) REFERENCES [dbo].[LoanTypes] ([LoanTypeID]),
    CONSTRAINT [FK_Loans_Properties] FOREIGN KEY ([PropertyID]) REFERENCES [dbo].[Properties] ([PropertyID]),
    CONSTRAINT [FK_Loans_ReferenceIndex] FOREIGN KEY ([ReferenceIndexID]) REFERENCES [dbo].[ReferenceIndex] ([ReferenceIndexID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'TotalCapitalStack';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'SpecialServicerCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 2351.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'SourceOfLoanData';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'ReservesPerSFOrUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'ReservesNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'ReserveBalance';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 1175.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'ReferenceIndexID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=99 @ 47.49:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'RateSpread';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'PropertyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=102 @ 46.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'PrepaymentString';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'PctOfPosition';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'PaidToDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=564 @ 8.34:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'OriginationDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=299 @ 15.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'OriginalMaturityDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'NextMaturityDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LockoutEndDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 1567.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanRateTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanPriorityRank';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanPerfTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4281 @ 1.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanLastDollarNetOFReserves';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanLastDollar';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanConstant';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=235 @ 20.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LoanBalance';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=17 @ 276.59:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LenderDBA';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=70 @ 67.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LenderCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=153 @ 30.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'Lender';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'LastDSCR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'Last$PerSFOrUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=61 @ 77.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'Last$LTV';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=58 @ 81.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'Last$DebtYield';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 2351.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'IsOutstanding';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 4702.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'IsEquityLike';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'IndexRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'FirstDSCR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=192 @ 24.49:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'First$PerSFOrUnit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=61 @ 77.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'First$LTV';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=58 @ 81.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'First$DebtYield';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'FinalMaturityDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'ExtensionOptions';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'ExtensionFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'DebtSeniorToLoan';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'DebtPariPassuToLoan';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'DebtMetricsNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'DebtJuniorToLoan';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'DealType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4700 @ 1.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'DealIDFromInsert';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4667 @ 1.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4668 @ 1.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'DealCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'CurrentCallProtection';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'CashManagement';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=481 @ 9.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'Borrowers';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10 @ 470.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'Amortization';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=77 @ 61.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'AllInInterestRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Loans', @level2type = N'COLUMN', @level2name = N'AddedBy';




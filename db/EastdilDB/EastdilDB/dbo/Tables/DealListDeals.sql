﻿CREATE TABLE [dbo].[DealListDeals] (
    [DealListDealID]      BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealListID]          INT              NULL,
    [DealID]              BIGINT           NULL,
    [LastUpdate]          DATETIME         DEFAULT (getdate()) NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_9DF2D83C_7E9B_4F64_9EF5_567F3F12E881_1205579333] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [DateAdded]           DATETIME         NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [PK_DealListDeals] PRIMARY KEY CLUSTERED ([DealListDealID] ASC),
    CONSTRAINT [FK_DealListDeals_DealLists] FOREIGN KEY ([DealListID]) REFERENCES [dbo].[DealLists] ([DealListID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealListDeals';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealListDeals';


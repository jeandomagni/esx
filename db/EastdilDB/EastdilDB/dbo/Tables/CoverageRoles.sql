﻿CREATE TABLE [dbo].[CoverageRoles] (
    [CoverageRoleID] INT            IDENTITY (1, 1) NOT NULL,
    [CoverageRole]   NVARCHAR (50)  NULL,
    [DisplayOrder]   INT            NULL,
    [RowVersion]     ROWVERSION     NOT NULL,
    [DateAdded]      DATETIME       NULL,
    [LastUpdate]     DATETIME       NULL,
    [AddedBy]        NVARCHAR (100) NULL,
    [UpdatedBy]      NVARCHAR (100) NULL,
    CONSTRAINT [PK_CoverageRoles_1] PRIMARY KEY CLUSTERED ([CoverageRoleID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CoverageRoles';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CoverageRoles';


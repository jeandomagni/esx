﻿CREATE TABLE [dbo].[ContractingCompany] (
    [ContractingCompanyID]   INT            IDENTITY (1, 1) NOT NULL,
    [ContractingCompanyName] NVARCHAR (50)  NULL,
    [ServiceType]            NVARCHAR (50)  NULL,
    [CompanyAddress1]        NVARCHAR (50)  NULL,
    [CompanyAddress2]        NVARCHAR (50)  NULL,
    [CompanyCity]            NVARCHAR (50)  NULL,
    [CompanyState]           NVARCHAR (50)  NULL,
    [CompanyZip]             NVARCHAR (50)  NULL,
    [CompanyContact]         NVARCHAR (50)  NULL,
    [CompanyPhone]           NVARCHAR (50)  NULL,
    [CompanyFax]             NVARCHAR (50)  NULL,
    [CompanyComments]        NVARCHAR (50)  NULL,
    [RowVersion]             ROWVERSION     NOT NULL,
    [DateAdded]              DATETIME       NULL,
    [LastUpdate]             DATETIME       NULL,
    [AddedBy]                NVARCHAR (100) NULL,
    [UpdatedBy]              NVARCHAR (100) NULL,
    CONSTRAINT [PK_ContractingCompany] PRIMARY KEY CLUSTERED ([ContractingCompanyID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'ServiceType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'ContractingCompanyName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'ContractingCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'CompanyZip';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'CompanyState';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'CompanyPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'CompanyFax';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'CompanyContact';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'CompanyComments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'CompanyCity';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'CompanyAddress2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'CompanyAddress1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ContractingCompany', @level2type = N'COLUMN', @level2name = N'AddedBy';




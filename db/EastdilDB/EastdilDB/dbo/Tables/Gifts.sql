﻿CREATE TABLE [dbo].[Gifts] (
    [GiftID]               BIGINT          IDENTITY (1, 1) NOT NULL,
    [EmployeeID]           BIGINT          NULL,
    [ContactID]            BIGINT          NULL,
    [CompanyID]            BIGINT          NULL,
    [DateGiven]            DATETIME        NULL,
    [GiftDescription]      NVARCHAR (2000) NULL,
    [GiftAmount]           MONEY           NULL,
    [ApprovedByEmployeeID] BIGINT          NULL,
    [DateAdded]            DATETIME        NULL,
    [LastUpdate]           DATETIME        NULL,
    [RowVersion]           ROWVERSION      NOT NULL,
    [AddedBy]              NVARCHAR (100)  NULL,
    [UpdatedBy]            NVARCHAR (100)  NULL,
    CONSTRAINT [PK_Gifts] PRIMARY KEY CLUSTERED ([GiftID] ASC),
    CONSTRAINT [FK_Gifts_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_Gifts_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contacts] ([ContactID]),
    CONSTRAINT [FK_Gifts_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_Gifts_Employees1] FOREIGN KEY ([ApprovedByEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Gifts';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Gifts';


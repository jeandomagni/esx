﻿CREATE TABLE [dbo].[AnticipatedFeeProbabilities] (
    [AnticipatedFeeProbabilityID] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [msrepl_tran_version]         UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_B02E4F8B_DBE9_41DA_8872_2025F850BCB9_1198627313] DEFAULT (newid()) NOT NULL,
    [RowVersion]                  ROWVERSION       NOT NULL,
    [DateAdded]                   DATETIME         NULL,
    [LastUpdate]                  DATETIME         NULL,
    [AddedBy]                     NVARCHAR (100)   NULL,
    [UpdatedBy]                   NVARCHAR (100)   NULL,
    CONSTRAINT [PK_AnticipatedFeeProbabilities] PRIMARY KEY CLUSTERED ([AnticipatedFeeProbabilityID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AnticipatedFeeProbabilities';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AnticipatedFeeProbabilities';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AnticipatedFeeProbabilities', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AnticipatedFeeProbabilities', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AnticipatedFeeProbabilities', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AnticipatedFeeProbabilities', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AnticipatedFeeProbabilities', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AnticipatedFeeProbabilities', @level2type = N'COLUMN', @level2name = N'AnticipatedFeeProbabilityID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AnticipatedFeeProbabilities', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[DealWPInternal] (
    [DealWPInternalID]    INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealID]              BIGINT           NULL,
    [EmployeeID]          BIGINT           NULL,
    [WorkingPartyRole]    NVARCHAR (50)    NULL,
    [WorkingPartyRoleID]  INT              NULL,
    [WPCategory]          NVARCHAR (50)    NULL,
    [LeadInRole]          BIT              DEFAULT ((0)) NULL,
    [ShowOnWPList]        BIT              DEFAULT ((0)) NULL,
    [SourcingPercent]     INT              DEFAULT ((0)) NULL,
    [ExecutionPercent]    INT              DEFAULT ((0)) NULL,
    [DateAdded]           DATETIME         NULL,
    [LastUpdate]          DATETIME         DEFAULT (getdate()) NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_794DF82B_0FE9_4CF2_ADFF_BFF8B13086FE_725577623] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [PK_DealWPInternal] PRIMARY KEY CLUSTERED ([DealWPInternalID] ASC),
    CONSTRAINT [FK_DealWPInternal_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealWPInternal_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_DealWPInternal_WorkingPartyRoles] FOREIGN KEY ([WorkingPartyRoleID]) REFERENCES [dbo].[WorkingPartyRoles] ([WorkingPartyRoleID])
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 56886.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'WPCategory';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=15 @ 3792.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'WorkingPartyRoleID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 6320.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'WorkingPartyRole';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 56886.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'SourcingPercent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 28443.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'ShowOnWPList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=29317 @ 1.94:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 28443.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'LeadInRole';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=29131 @ 1.95:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 56886.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'ExecutionPercent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=694 @ 81.97:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'DealWPInternalID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8867 @ 6.42:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=28466 @ 2.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealWPInternal', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
CREATE NONCLUSTERED INDEX [IX_DEALWPINTERNALDEALID]
    ON [dbo].[DealWPInternal]([DealID] ASC, [WorkingPartyRoleID] ASC, [DealWPInternalID] ASC, [EmployeeID] ASC);


﻿CREATE TABLE [dbo].[DealContactBidEquitySale] (
    [DealContactBidEquitySaleID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [DealID]                     BIGINT          NULL,
    [DealContactID]              BIGINT          NULL,
    [BidDate]                    DATETIME        NULL,
    [BidRank]                    INT             NULL,
    [BidComments]                NVARCHAR (1000) NULL,
    [OfferNumber]                INT             NULL,
    [Purchaser]                  NVARCHAR (200)  NULL,
    [InvestorType]               NVARCHAR (50)   NULL,
    [InitialBidAmount]           MONEY           NULL,
    [EquityCapitalProvider]      NVARCHAR (200)  NULL,
    [RevisedBidAmount]           MONEY           NULL,
    [NetProceeds]                MONEY           NULL,
    [DepositInitial]             MONEY           NULL,
    [DepositTotal]               MONEY           NULL,
    [PropertyTour]               NVARCHAR (50)   NULL,
    [PricePSFCALC]               MONEY           NULL,
    [UnderwrittenCapital]        MONEY           NULL,
    [DebtFinancingNeededToClose] NVARCHAR (50)   NULL,
    [DueDiligencePeriodDays]     INT             NULL,
    [ClosingPeriodDays]          INT             NULL,
    [InPlaceCapRateCALC]         FLOAT (53)      NULL,
    [Year1CapRateCALC]           FLOAT (53)      NULL,
    [StabilizedCapRateCALC]      FLOAT (53)      NULL,
    CONSTRAINT [PK_DealContactBidEquitySale] PRIMARY KEY CLUSTERED ([DealContactBidEquitySaleID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Added late last year when team was trying to define how to track Bids', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBidEquitySale';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBidEquitySale';


﻿CREATE TABLE [dbo].[Addresses] (
    [AddressID]           BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ADDRESSID_SLX]       CHAR (12)        NULL,
    [ENTITYID]            CHAR (12)        NULL,
    [Type]                NVARCHAR (100)   NULL,
    [Description]         NVARCHAR (100)   NULL,
    [Address1]            NVARCHAR (100)   NULL,
    [Address2]            NVARCHAR (100)   NULL,
    [City]                NVARCHAR (50)    NULL,
    [State]               NVARCHAR (50)    NULL,
    [PostalCode]          NVARCHAR (50)    NULL,
    [County]              NVARCHAR (50)    NULL,
    [Country]             NVARCHAR (50)    NULL,
    [CrossStreet]         NVARCHAR (150)   NULL,
    [IsPrimary]           BIT              CONSTRAINT [DF__Addresses__IsPri__70DDC3D8] DEFAULT ((0)) NULL,
    [IsMailing]           BIT              CONSTRAINT [DF__Addresses__IsMai__71D1E811] DEFAULT ((0)) NULL,
    [Salutation]          NVARCHAR (100)   NULL,
    [Routing]             NVARCHAR (100)   NULL,
    [Address3]            NVARCHAR (100)   NULL,
    [Address4]            NVARCHAR (100)   NULL,
    [Timezone]            NVARCHAR (100)   NULL,
    [Source]              NVARCHAR (50)    NULL,
    [Confidence]          SMALLINT         NULL,
    [Latitude]            FLOAT (53)       NULL,
    [Longitude]           FLOAT (53)       NULL,
    [BoundaryBaseHeight]  INT              NULL,
    [BoundaryMaxHeight]   INT              NULL,
    [Boundary1]           FLOAT (53)       NULL,
    [Boundary2]           FLOAT (53)       NULL,
    [Boundary3]           FLOAT (53)       NULL,
    [Boundary4]           FLOAT (53)       NULL,
    [DateAdded]           DATETIME         NULL,
    [LastUpdate]          DATETIME         CONSTRAINT [DF__Addresses__LastU__72C60C4A] DEFAULT (getdate()) NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_0EA1EC0A_C5F0_4D15_A1A0_0F79C56F5D09_341576255] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED ([AddressID] ASC)
);










GO
CREATE NONCLUSTERED INDEX [IX_ADDRESS_CUSTOM]
    ON [dbo].[Addresses]([AddressID] ASC, [State] ASC)
    INCLUDE([ADDRESSID_SLX], [ENTITYID], [Type], [Description], [Address1], [Address2], [City], [PostalCode], [County], [Country], [CrossStreet], [IsPrimary], [IsMailing], [Salutation], [Routing], [Address3], [Address4], [Timezone], [DateAdded], [LastUpdate]);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utility,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Type';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 5891.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Timezone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=74 @ 2706.99:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'State';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Source';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=16715 @ 11.98:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Salutation';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Routing';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12320 @ 16.26:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'PostalCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=107020 @ 1.87:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Longitude';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Latitude';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=73722 @ 2.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 100158.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'IsPrimary';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 100158.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'IsMailing';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=97285 @ 2.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'ENTITYID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=45 @ 4451.49:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Description';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=23112 @ 8.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=78 @ 2568.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'CrossStreet';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 66772.34:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'County';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=120 @ 1669.31:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Country';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Confidence';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4164 @ 48.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'City';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'BoundaryMaxHeight';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'BoundaryBaseHeight';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Boundary4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Boundary3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Boundary2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Boundary1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'ADDRESSID_SLX';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'AddressID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=57 @ 3514.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Address4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2187 @ 91.59:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Address3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9172 @ 21.84:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Address2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=37565 @ 5.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'Address1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Addresses', @level2type = N'COLUMN', @level2name = N'AddedBy';




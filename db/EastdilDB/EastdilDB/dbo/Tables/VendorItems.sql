﻿CREATE TABLE [dbo].[VendorItems] (
    [VendorItemID]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [VoucherAccount]   NVARCHAR (50)  NULL,
    [Vendor]           NVARCHAR (100) NULL,
    [EserviceNumber]   NVARCHAR (50)  NULL,
    [InvoiceDate]      DATETIME       NULL,
    [Voucher]          NVARCHAR (50)  NULL,
    [TransDate]        DATETIME       NULL,
    [GLDate]           DATETIME       NULL,
    [Account]          NVARCHAR (50)  NULL,
    [AU]               NVARCHAR (50)  NULL,
    [MonetaryAmount]   MONEY          NULL,
    [CheckDate]        DATETIME       NULL,
    [CheckNumber]      NVARCHAR (50)  NULL,
    [CheckAmount]      MONEY          NULL,
    [CheckClearedDate] DATETIME       NULL,
    [Comment]          NVARCHAR (100) NULL,
    [Allocated]        BIT            NULL,
    [TrackedInConcur]  BIT            NULL,
    [RecordSource]     NVARCHAR (50)  NULL,
    [DateAdded]        DATETIME       NULL,
    [LastUpdate]       DATETIME       NULL,
    [AddedBy]          NVARCHAR (100) NULL,
    [UpdatedBy]        NVARCHAR (100) NULL,
    CONSTRAINT [PK_WFVendorItems] PRIMARY KEY CLUSTERED ([VendorItemID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utility,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'VoucherAccount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=29455 @ 2.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'Voucher';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'VendorItemID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1720 @ 34.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'Vendor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=480 @ 123.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'TransDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 29550.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'TrackedInConcur';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 19700.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'RecordSource';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=27775 @ 2.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'MonetaryAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=828 @ 71.38:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'InvoiceDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=451 @ 131.04:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'GLDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=28052 @ 2.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'EserviceNumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2723 @ 21.70:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'Comment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=13474 @ 4.39:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'CheckNumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=447 @ 132.21:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'CheckDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=451 @ 131.04:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'CheckClearedDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10796 @ 5.47:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'CheckAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 29550.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'AU';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 29550.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'Allocated';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=74 @ 798.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'VendorItems', @level2type = N'COLUMN', @level2name = N'Account';




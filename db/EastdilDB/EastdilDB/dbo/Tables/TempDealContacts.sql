﻿CREATE TABLE [dbo].[TempDealContacts] (
    [TempDealContactID]            BIGINT          IDENTITY (1, 1) NOT NULL,
    [AddToDealID]                  BIGINT          NULL,
    [AddToContactListID]           BIGINT          NULL,
    [AddFromDealID]                BIGINT          NULL,
    [AddFromContactListID]         BIGINT          NULL,
    [DealID]                       BIGINT          NULL,
    [ContactID]                    BIGINT          NULL,
    [CompanyID]                    BIGINT          NULL,
    [InsertFlag]                   BIT             NULL,
    [AlreadyOnList]                BIT             NULL,
    [Lastname]                     NVARCHAR (50)   NULL,
    [FirstnameLastname]            NVARCHAR (100)  NULL,
    [CompanyName]                  NVARCHAR (128)  NULL,
    [Address1]                     NVARCHAR (100)  NULL,
    [City]                         NVARCHAR (50)   NULL,
    [EmployeeID]                   BIGINT          NULL,
    [TypicalRole]                  NVARCHAR (50)   NULL,
    [ClassID]                      INT             NULL,
    [CategoryID]                   INT             NULL,
    [MarketingStatusID]            INT             NULL,
    [ApproverEmailFlag]            BIT             NULL,
    [ApprovalRequiredForMarketing] BIT             NULL,
    [MarketingStatus]              NVARCHAR (50)   NULL,
    [Tier]                         NVARCHAR (50)   NULL,
    [CASent]                       DATETIME        NULL,
    [OMDigital]                    DATETIME        NULL,
    [CAUnapproved]                 BIT             NULL,
    [CALogged]                     DATETIME        NULL,
    [OMSent]                       DATETIME        NULL,
    [DoNotSendOM]                  BIT             NULL,
    [WarroomAccess]                DATETIME        NULL,
    [DoNotSendWarroom]             BIT             NULL,
    [StatusNote]                   NVARCHAR (1000) NULL,
    [CoverageContactID]            BIGINT          NULL,
    [OMShipPending]                BIT             NULL,
    [BrokerSendsOM]                BIT             NULL,
    [DoNotCopy]                    BIT             NULL,
    [MarkForDelete]                BIT             NULL,
    [CompanyContactFlag]           BIT             NULL,
    [IsLeadInvestor]               BIT             NULL,
    [WinningBidder]                BIT             NULL,
    [Selected]                     BIT             NULL,
    [UpdateFlag]                   BIT             NULL,
    [DeleteFlag]                   BIT             NULL,
    [CAPortal]                     NVARCHAR (50)   NULL,
    [CAMethod]                     NVARCHAR (50)   NULL,
    [BidRound]                     NVARCHAR (50)   NULL,
    [DealConvPct]                  INT             NULL,
    [UserEmail]                    NVARCHAR (50)   NULL,
    [DateAdded]                    DATETIME        NULL,
    [LastUpdate]                   DATETIME        NULL,
    [AddedBy]                      NVARCHAR (100)  NULL,
    [UpdatedBy]                    NVARCHAR (100)  NULL,
    CONSTRAINT [PK_TempDealContacts] PRIMARY KEY CLUSTERED ([TempDealContactID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utility,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=89 @ 86.44:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'UserEmail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 1923.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'TypicalRole';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'TempDealContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=13 @ 591.77:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'MarketingStatus';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4624 @ 1.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'Lastname';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 3846.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'InsertFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=6514 @ 1.18:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'FirstnameLastname';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=76 @ 101.22:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'DealConvPct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=45 @ 170.96:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=6697 @ 1.15:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2737 @ 2.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'CompanyName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3121 @ 2.46:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=119 @ 64.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'City';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 1923.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'CategoryID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 3846.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'AlreadyOnList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=78 @ 98.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'AddToDealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 7693.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'AddToContactListID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=266 @ 28.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'Address1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8 @ 961.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'AddFromDealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 7693.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'AddFromContactListID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempDealContacts', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[ResearchDocs] (
    [ResearchDocID] INT            IDENTITY (1, 1) NOT NULL,
    [DocTitle]      NVARCHAR (200) NULL,
    [ProductType]   NVARCHAR (100) NOT NULL,
    [CompanyName]   NVARCHAR (100) NULL,
    [CompanyTicker] NVARCHAR (10)  NULL,
    [Keywords]      NVARCHAR (200) NULL,
    [DocDate]       DATETIME       NOT NULL,
    [FilePath]      NVARCHAR (100) NULL,
    [FileName]      NVARCHAR (100) NULL,
    [DateAdded]     DATETIME       NULL
);


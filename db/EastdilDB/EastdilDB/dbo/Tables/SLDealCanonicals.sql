﻿CREATE TABLE [dbo].[SLDealCanonicals] (
    [DealID]      BIGINT         NULL,
    [CanonicalID] BIGINT         NULL,
    [DealRoleID]  INT            NULL,
    [DateAdded]   DATETIME       NULL,
    [LastUpdate]  DATETIME       NULL,
    [AddedBy]     NVARCHAR (100) NULL,
    [UpdatedBy]   NVARCHAR (100) NULL,
    CONSTRAINT [FK_SLDealCanonicals_Deals] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_SLDealCanonicals_SLCanonicals] FOREIGN KEY ([CanonicalID]) REFERENCES [dbo].[SLCanonicals] ([CanonicalID]),
    CONSTRAINT [FK_SLDealCanonicals_SLDealRoles] FOREIGN KEY ([DealRoleID]) REFERENCES [dbo].[SLDealRoles] ([DealRoleID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Skyline Project,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLDealCanonicals';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLDealCanonicals';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLDealCanonicals', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLDealCanonicals', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 282.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLDealCanonicals', @level2type = N'COLUMN', @level2name = N'DealRoleID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=428 @ 2.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLDealCanonicals', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLDealCanonicals', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=503 @ 2.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLDealCanonicals', @level2type = N'COLUMN', @level2name = N'CanonicalID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SLDealCanonicals', @level2type = N'COLUMN', @level2name = N'AddedBy';




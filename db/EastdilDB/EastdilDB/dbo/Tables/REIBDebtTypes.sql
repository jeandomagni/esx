﻿CREATE TABLE [dbo].[REIBDebtTypes] (
    [REIBDebtTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [DebtType]       NVARCHAR (50)  NULL,
    [DisplayOrder]   INT            NULL,
    [RowVersion]     ROWVERSION     NOT NULL,
    [DateAdded]      DATETIME       NULL,
    [LastUpdate]     DATETIME       NULL,
    [AddedBy]        NVARCHAR (100) NULL,
    [UpdatedBy]      NVARCHAR (100) NULL,
    CONSTRAINT [PK_REIBDebtTypes] PRIMARY KEY CLUSTERED ([REIBDebtTypeID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=Lookup,Notes=Unimplemented feature brought over from Secured Capital version', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBDebtTypes';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBDebtTypes';


﻿CREATE TABLE [dbo].[CommentVisibility] (
    [CommentVisibilityID] INT              NOT NULL,
    [CommentVisibility]   NVARCHAR (50)    NULL,
    [DisplayOrder]        INT              NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_D69F6D1A_805C_4605_A73A_63EE2629FBA8_117575457] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [DateAdded]           DATETIME         NULL,
    [LastUpdate]          DATETIME         NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [PK_CommentVisibility] PRIMARY KEY CLUSTERED ([CommentVisibilityID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility', @level2type = N'COLUMN', @level2name = N'DisplayOrder';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility', @level2type = N'COLUMN', @level2name = N'CommentVisibilityID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility', @level2type = N'COLUMN', @level2name = N'CommentVisibility';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CommentVisibility', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[Companies] (
    [CompanyID]                BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CompanyName]              NVARCHAR (128)   NULL,
    [CompanyLegalName]         NVARCHAR (200)   NULL,
    [Division]                 NVARCHAR (100)   NULL,
    [AddressID]                BIGINT           NULL,
    [ShippingID]               BIGINT           NULL,
    [TickerSymbol]             NVARCHAR (50)    NULL,
    [CIBOSCode]                NVARCHAR (50)    NULL,
    [PrimaryCompanyRecord]     BIT              CONSTRAINT [DF_Companies_PrimaryCompanyRecord] DEFAULT ((0)) NOT NULL,
    [PrimaryCompanyID]         BIGINT           NULL,
    [PrimaryCompanyCode]       NVARCHAR (50)    NULL,
    [Region]                   NVARCHAR (100)   NULL,
    [MainPhone]                NVARCHAR (50)    NULL,
    [Fax]                      NVARCHAR (50)    NULL,
    [TollFreeNumber]           NVARCHAR (50)    NULL,
    [AlternateCompanyPhone]    NVARCHAR (50)    NULL,
    [CompanyEmail]             NVARCHAR (128)   NULL,
    [Website]                  NVARCHAR (128)   NULL,
    [MiscInfo]                 NVARCHAR (1000)  NULL,
    [Client]                   BIT              CONSTRAINT [DF__Companies__Clien__7192BC46] DEFAULT ((0)) NULL,
    [ESContactEmployeeID]      BIGINT           NULL,
    [REIBCompany]              BIT              CONSTRAINT [DF__Companies__REIBC__7286E07F] DEFAULT ((0)) NULL,
    [PECompany]                BIT              NULL,
    [PECompanyTypeID]          INT              NULL,
    [DoNotSolicitAsOfDate]     DATETIME         NULL,
    [DoNotSolicit]             BIT              CONSTRAINT [DF__Companies__DoNot__737B04B8] DEFAULT ((0)) NULL,
    [NoInterestInDeals]        BIT              CONSTRAINT [DF__Companies__NoInt__746F28F1] DEFAULT ((0)) NULL,
    [NoInterestNote]           NTEXT            NULL,
    [CompanyType]              NVARCHAR (100)   NULL,
    [ESClassification]         NVARCHAR (100)   NULL,
    [PrimaryInstType]          NVARCHAR (100)   NULL,
    [InstitutionType]          NVARCHAR (100)   NULL,
    [LeadQualRelCompanyRec]    BIT              CONSTRAINT [DF__Companies__LeadQ__75634D2A] DEFAULT ((0)) NULL,
    [LeadQualRelCompanyID]     BIGINT           CONSTRAINT [DF__Companies__LeadQ__76577163] DEFAULT ((0)) NULL,
    [QualifiedRelationship]    BIT              CONSTRAINT [DF__Companies__Quali__774B959C] DEFAULT ((0)) NULL,
    [QualRelNote]              NTEXT            NULL,
    [DMSLeadQualRelCompanyID]  NVARCHAR (50)    NULL,
    [WFCrossSell]              NVARCHAR (50)    NULL,
    [RestrictedVisibility]     BIT              NULL,
    [TrackedByAsiaTeam]        BIT              NULL,
    [TypicalInvestmentSize]    MONEY            NULL,
    [AssetsUnderManagement]    INT              NULL,
    [RealEstateAllocationPct]  NUMERIC (18, 5)  NULL,
    [Office]                   BIT              NULL,
    [Industrial]               BIT              NULL,
    [Mixed]                    BIT              NULL,
    [Residential]              BIT              NULL,
    [Retail]                   BIT              NULL,
    [Hotel]                    BIT              NULL,
    [USA]                      BIT              NULL,
    [Canada]                   BIT              NULL,
    [Europe]                   BIT              NULL,
    [Asia]                     BIT              NULL,
    [MiddleEast]               BIT              NULL,
    [CompanyRegionID]          INT              NULL,
    [ConsultantID]             INT              NULL,
    [WFSCorrelation]           BIT              NULL,
    [FCPAListName]             NVARCHAR (150)   NULL,
    [NameChange]               BIT              NULL,
    [AddressChange]            BIT              NULL,
    [OFACCheck]                BIT              NULL,
    [OFACCheckDate]            DATETIME         NULL,
    [FGOV]                     BIT              NULL,
    [Active]                   BIT              CONSTRAINT [DF__Companies__Activ__783FB9D5] DEFAULT ((0)) NULL,
    [ReasonInactive]           NVARCHAR (1000)  NULL,
    [Status]                   NVARCHAR (100)   NULL,
    [NumActiveContacts]        INT              NULL,
    [NumContacts]              INT              NULL,
    [NumDealsAsInv]            INT              NULL,
    [NumDealsAsClient]         INT              NULL,
    [DateAdded]                DATETIME         NULL,
    [LastUpdate]               DATETIME         CONSTRAINT [DF__Companies__LastU__7933DE0E] DEFAULT (getdate()) NULL,
    [UpdateFlag]               BIT              NULL,
    [msrepl_tran_version]      UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_9462B3F5_414B_49EB_ADC5_90CF1A420CD3_1141579105] DEFAULT (newid()) NOT NULL,
    [RowVersion]               ROWVERSION       NOT NULL,
    [FGOVT]                    NVARCHAR (50)    CONSTRAINT [DF__Companies__FGOVT__0BF99E2D] DEFAULT ('---') NULL,
    [DBAName]                  NVARCHAR (50)    NULL,
    [NumDBACompanies]          INT              CONSTRAINT [DF__Companies__NumDB__4220A4B4] DEFAULT ((0)) NULL,
    [NumDBAContacts]           INT              CONSTRAINT [DF__Companies__NumDB__4314C8ED] DEFAULT ((0)) NULL,
    [NumDBADealsAsClient]      INT              CONSTRAINT [DF__Companies__NumDB__4408ED26] DEFAULT ((0)) NULL,
    [NumDBAPropertiesAsBuyer]  INT              CONSTRAINT [DF__Companies__NumDB__44FD115F] DEFAULT ((0)) NULL,
    [NumDBAPropertiesAsSeller] INT              CONSTRAINT [DF__Companies__NumDB__45F13598] DEFAULT ((0)) NULL,
    [NumDBALoansAsBorrower]    INT              NULL,
    [NumDBALoansAsLender]      INT              NULL,
    [Development]              BIT              NULL,
    [AddedBy]                  NVARCHAR (100)   NULL,
    [UpdatedBy]                NVARCHAR (100)   NULL,
    [OfficeMarketNote]         NVARCHAR (4000)  NULL,
    [IndustrialMarketNote]     NVARCHAR (4000)  NULL,
    [MixedMarketNote]          NVARCHAR (4000)  NULL,
    [ResidentialMarketNote]    NVARCHAR (4000)  NULL,
    [RetailMarketNote]         NVARCHAR (4000)  NULL,
    [HotelMarketNote]          NVARCHAR (4000)  NULL,
    [USAMarketNote]            NVARCHAR (4000)  NULL,
    [CanadaMarketNote]         NVARCHAR (4000)  NULL,
    [EuropeMarketNote]         NVARCHAR (4000)  NULL,
    [AsiaMarketNote]           NVARCHAR (4000)  NULL,
    [MiddleEastMarketNote]     NVARCHAR (4000)  NULL,
    CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED ([CompanyID] ASC),
    CONSTRAINT [FK_Companies_Addresses] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[Addresses] ([AddressID]),
    CONSTRAINT [FK_Companies_CompanyRegions] FOREIGN KEY ([CompanyRegionID]) REFERENCES [dbo].[CompanyRegions] ([CompanyRegionID]),
    CONSTRAINT [FK_Companies_Employees] FOREIGN KEY ([ESContactEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_Companies_PECompanyTypes] FOREIGN KEY ([PECompanyTypeID]) REFERENCES [dbo].[PECompanyTypes] ([PECompanyTypeID])
);








GO
CREATE NONCLUSTERED INDEX [CompanyName]
    ON [dbo].[Companies]([CompanyName] ASC, [CompanyID] ASC);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'WFSCorrelation';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=17 @ 2706.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'WFCrossSell';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8589 @ 5.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Website';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=16 @ 2875.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'USAMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'USA';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'UpdateFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'TypicalInvestmentSize';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'TrackedByAsiaTeam';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=50 @ 920.16:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'TollFreeNumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=133 @ 345.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'TickerSymbol';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Status';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'ShippingID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'RetailMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Retail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'RestrictedVisibility';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'ResidentialMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Residential';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'REIBCompany';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 5751.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Region';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=38 @ 1210.74:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'ReasonInactive';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'RealEstateAllocationPct';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'QualifiedRelationship';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 15336.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'PrimaryInstType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'PrimaryCompanyRecord';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'PrimaryCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'PrimaryCompanyCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 6572.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'PECompanyTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'PECompany';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 9201.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'OfficeMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Office';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=174 @ 264.41:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'OFACCheckDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'OFACCheck';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumDealsAsInv';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=29 @ 1586.48:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumDealsAsClient';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12 @ 3834.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumDBAPropertiesAsSeller';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10 @ 4600.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumDBAPropertiesAsBuyer';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 6572.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumDBALoansAsLender';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 15336.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumDBALoansAsBorrower';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=74 @ 621.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumDBADealsAsClient';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=166 @ 277.16:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumDBAContacts';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=63 @ 730.29:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumDBACompanies';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=101 @ 455.52:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumContacts';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=88 @ 522.82:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NumActiveContacts';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NoInterestInDeals';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'NameChange';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=29393 @ 1.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'MixedMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Mixed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=54 @ 852.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'MiscInfo';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'MiddleEastMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'MiddleEast';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=32090 @ 1.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'MainPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'LeadQualRelCompanyRec';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'LeadQualRelCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=44814 @ 1.03:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=115 @ 400.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'InstitutionType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'IndustrialMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Industrial';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'HotelMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Hotel';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'FGOVT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'FGOV';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'FCPAListName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=26574 @ 1.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Fax';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=11 @ 4182.55:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'EuropeMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Europe';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=140 @ 328.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'ESContactEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=27 @ 1704.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'ESClassification';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10 @ 4600.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'DoNotSolicitAsOfDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'DoNotSolicit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=717 @ 64.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'DMSLeadQualRelCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=502 @ 91.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Division';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Development';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1588 @ 28.97:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'DBAName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34804 @ 1.32:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'ConsultantID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=102 @ 451.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'CompanyType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 9201.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'CompanyRegionID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=38167 @ 1.21:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'CompanyName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=451 @ 102.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'CompanyLegalName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10031 @ 4.59:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'CompanyEmail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Client';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=103 @ 446.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'CIBOSCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'CanadaMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Canada';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'AssetsUnderManagement';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 46008.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'AsiaMarketNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Asia';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'AlternateCompanyPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'AddressID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'AddressChange';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 23004.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Companies', @level2type = N'COLUMN', @level2name = N'Active';




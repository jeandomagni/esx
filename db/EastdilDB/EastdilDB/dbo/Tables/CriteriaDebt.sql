﻿CREATE TABLE [dbo].[CriteriaDebt] (
    [CriteriaDebtID]          BIGINT          IDENTITY (1, 1) NOT NULL,
    [ContactID]               BIGINT          NULL,
    [DealID]                  BIGINT          NULL,
    [Comment]                 NVARCHAR (4000) NULL,
    [MustSeeAllDeals]         BIT             NULL,
    [Hotel]                   BIT             NULL,
    [Industrial]              BIT             NULL,
    [MultiFamily]             BIT             NULL,
    [Office]                  BIT             NULL,
    [ResidentialCommunity]    BIT             NULL,
    [Retail]                  BIT             NULL,
    [Land]                    BIT             NULL,
    [Range1To10]              BIT             NULL,
    [Range10To25]             BIT             NULL,
    [Range25To50]             BIT             NULL,
    [Range50To100]            BIT             NULL,
    [Range100Plus]            BIT             NULL,
    [Nationwide]              BIT             NULL,
    [Southeast]               BIT             NULL,
    [MidAtlantic]             BIT             NULL,
    [Northeast]               BIT             NULL,
    [Midwest]                 BIT             NULL,
    [Southwest]               BIT             NULL,
    [West]                    BIT             NULL,
    [MSA1]                    BIT             NULL,
    [MSA2]                    BIT             NULL,
    [MSA3]                    BIT             NULL,
    [MSA4]                    BIT             NULL,
    [MSA5]                    BIT             NULL,
    [MSA6]                    BIT             NULL,
    [MSA7]                    BIT             NULL,
    [MSA8]                    BIT             NULL,
    [MSA9]                    BIT             NULL,
    [MSA10]                   BIT             NULL,
    [MSA11]                   BIT             NULL,
    [MSA12]                   BIT             NULL,
    [MSA13]                   BIT             NULL,
    [MSA14]                   BIT             NULL,
    [MSA15]                   BIT             NULL,
    [MSA16]                   BIT             NULL,
    [MSA17]                   BIT             NULL,
    [MSA18]                   BIT             NULL,
    [MSA19]                   BIT             NULL,
    [MSA20]                   BIT             NULL,
    [MSA21]                   BIT             NULL,
    [MSA22]                   BIT             NULL,
    [MSA23]                   BIT             NULL,
    [MSA24]                   BIT             NULL,
    [MSA25]                   BIT             NULL,
    [EquityJointVenture]      BIT             NULL,
    [Equity100PctAcquisition] BIT             NULL,
    [Debt]                    BIT             NULL,
    [PreferredEquity]         BIT             NULL,
    [Mezzanine]               BIT             NULL,
    [LoanPurchase]            BIT             NULL,
    [Performing]              BIT             NULL,
    [NonPerforming]           BIT             NULL,
    [SubPerforming]           BIT             NULL,
    [MezzAndBNote]            BIT             NULL,
    [PensionFunds]            BIT             NULL,
    [HighNetWorth]            BIT             NULL,
    [SeparateAccount]         BIT             NULL,
    [Offshore]                BIT             NULL,
    [OffshoreEuropeanGerman]  BIT             NULL,
    [OffshoreEuropeanOther]   BIT             NULL,
    [OffshoreMiddleEastern]   BIT             NULL,
    [OffshoreAsian]           BIT             NULL,
    [PensionFundAdvisor]      BIT             NULL,
    [LifeInsuranceCompany]    BIT             NULL,
    [PrivateInvestor]         BIT             NULL,
    [PensionFund]             BIT             NULL,
    [FundSponsor]             BIT             NULL,
    [SyndicatorOpenEndFund]   BIT             NULL,
    [SyndicatorClosedEndFund] BIT             NULL,
    [PublicREIT]              BIT             NULL,
    [NonTradedREIT]           BIT             NULL,
    [UpdateFromDealID]        BIGINT          NULL,
    [LastUpdatedByEmployeeID] BIGINT          NULL,
    [LastUpdate]              DATETIME        NULL,
    [RowVersion]              ROWVERSION      NOT NULL,
    [DateAdded]               DATETIME        NULL,
    [AddedBy]                 NVARCHAR (100)  NULL,
    [UpdatedBy]               NVARCHAR (100)  NULL,
    CONSTRAINT [PK_CriteriaDebt] PRIMARY KEY CLUSTERED ([CriteriaDebtID] ASC),
    CONSTRAINT [FK_CriteriaDebt_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contacts] ([ContactID]) ON DELETE CASCADE,
    CONSTRAINT [FK_CriteriaDebt_Employees] FOREIGN KEY ([LastUpdatedByEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utility,Notes=Partially implemented and utilized', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'West';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'UpdateFromDealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'SyndicatorOpenEndFund';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'SyndicatorClosedEndFund';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'SubPerforming';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Southwest';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Southeast';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'SeparateAccount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Retail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'ResidentialCommunity';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Range50To100';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Range25To50';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Range1To10';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Range10To25';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Range100Plus';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'PublicREIT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'PrivateInvestor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'PreferredEquity';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Performing';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'PensionFunds';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'PensionFundAdvisor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'PensionFund';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'OffshoreMiddleEastern';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'OffshoreEuropeanOther';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'OffshoreEuropeanGerman';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'OffshoreAsian';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Offshore';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Office';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Northeast';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'NonTradedREIT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'NonPerforming';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Nationwide';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MustSeeAllDeals';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MultiFamily';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA9';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA8';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA7';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA6';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA5';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA25';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA24';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA23';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA22';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA21';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA20';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA19';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA18';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA17';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA16';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA15';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA14';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA13';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA12';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA11';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA10';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MSA1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Midwest';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MidAtlantic';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Mezzanine';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'MezzAndBNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'LoanPurchase';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'LifeInsuranceCompany';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 18960.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'LastUpdatedByEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=21077 @ 4.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Land';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Industrial';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 47401.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Hotel';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'HighNetWorth';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'FundSponsor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'EquityJointVenture';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Equity100PctAcquisition';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 94802.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Debt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'CriteriaDebtID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 31600.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'Comment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CriteriaDebt', @level2type = N'COLUMN', @level2name = N'AddedBy';




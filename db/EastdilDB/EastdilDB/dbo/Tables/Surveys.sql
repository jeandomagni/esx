﻿CREATE TABLE [dbo].[Surveys] (
    [SurveyID]   INT            IDENTITY (1, 1) NOT NULL,
    [SurveyName] NVARCHAR (200) NULL,
    [DateAdded]  DATETIME       NULL
);


﻿CREATE TABLE [dbo].[Restaurants] (
    [RestaurantID]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [RestaurantName]   NVARCHAR (100)  NULL,
    [Address]          NVARCHAR (100)  NULL,
    [City]             NVARCHAR (50)   NULL,
    [State]            NVARCHAR (50)   NULL,
    [PostalCode]       NVARCHAR (50)   NULL,
    [Phone]            NVARCHAR (50)   NULL,
    [Website]          NVARCHAR (50)   NULL,
    [ContactName]      NVARCHAR (100)  NULL,
    [ContactPhone]     NVARCHAR (50)   NULL,
    [RestaurantTypeID] INT             NULL,
    [Notes]            NVARCHAR (4000) NULL,
    [Photo]            IMAGE           NULL,
    [Menu]             IMAGE           NULL,
    [DateAdded]        DATETIME        NULL,
    [LastUpdate]       DATETIME        NULL,
    [RowVersion]       ROWVERSION      NULL,
    [AddedBy]          NVARCHAR (100)  NULL,
    [UpdatedBy]        NVARCHAR (100)  NULL,
    CONSTRAINT [PK_Restaurants] PRIMARY KEY CLUSTERED ([RestaurantID] ASC),
    CONSTRAINT [FK_Restaurants_RestaurantTypes] FOREIGN KEY ([RestaurantTypeID]) REFERENCES [dbo].[RestaurantTypes] ([RestaurantTypeID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Unimplemented feature brought over from Secured Capital version', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Restaurants';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Restaurants';


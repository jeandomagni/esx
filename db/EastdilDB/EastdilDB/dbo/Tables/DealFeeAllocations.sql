﻿CREATE TABLE [dbo].[DealFeeAllocations] (
    [DealFeeAllocationID]  BIGINT          IDENTITY (1, 1) NOT NULL,
    [DealFeeID]            BIGINT          NULL,
    [EmployeeID]           BIGINT          NULL,
    [AllocationPercentage] DECIMAL (18, 3) NULL,
    [LastUpdate]           DATETIME        NULL,
    [RowVersion]           ROWVERSION      NOT NULL,
    [DateAdded]            DATETIME        NULL,
    [AddedBy]              NVARCHAR (100)  NULL,
    [UpdatedBy]            NVARCHAR (100)  NULL,
    CONSTRAINT [PK_DealFeeAllocations] PRIMARY KEY CLUSTERED ([DealFeeAllocationID] ASC),
    CONSTRAINT [FK_DealFeeAllocations_DealFees] FOREIGN KEY ([DealFeeID]) REFERENCES [dbo].[DealFees] ([DealFeeID]),
    CONSTRAINT [FK_DealFeeAllocations_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3579 @ 2.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=118 @ 80.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3729 @ 2.54:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations', @level2type = N'COLUMN', @level2name = N'DealFeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations', @level2type = N'COLUMN', @level2name = N'DealFeeAllocationID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=54 @ 175.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations', @level2type = N'COLUMN', @level2name = N'AllocationPercentage';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAllocations', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[PropertyTypeCategories] (
    [PropertyTypeCategoryID] INT            IDENTITY (1, 1) NOT NULL,
    [PropertyTypeCategory]   NVARCHAR (50)  NULL,
    [DisplayOrder]           INT            NULL,
    [DateAdded]              DATETIME       NULL,
    [LastUpdate]             DATETIME       NULL,
    [AddedBy]                NVARCHAR (100) NULL,
    [UpdatedBy]              NVARCHAR (100) NULL,
    CONSTRAINT [PK_PropertyTypeCategories] PRIMARY KEY CLUSTERED ([PropertyTypeCategoryID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypeCategories';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypeCategories';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypeCategories', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypeCategories', @level2type = N'COLUMN', @level2name = N'PropertyTypeCategoryID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypeCategories', @level2type = N'COLUMN', @level2name = N'PropertyTypeCategory';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypeCategories', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypeCategories', @level2type = N'COLUMN', @level2name = N'DisplayOrder';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypeCategories', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyTypeCategories', @level2type = N'COLUMN', @level2name = N'AddedBy';




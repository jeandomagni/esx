﻿CREATE TABLE [dbo].[REIBInstitutionalOwners] (
    [REIBInstitutionalOwnerID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [CompanyID]                BIGINT          NULL,
    [SharesOwned]              DECIMAL (18, 3) NULL,
    [PercentOwned]             DECIMAL (18, 2) NULL,
    [LastUpdate]               DATETIME        NULL,
    [RowVersion]               ROWVERSION      NOT NULL,
    [DateAdded]                DATETIME        NULL,
    [AddedBy]                  NVARCHAR (100)  NULL,
    [UpdatedBy]                NVARCHAR (100)  NULL,
    CONSTRAINT [PK_REIBInstitutionalOwners] PRIMARY KEY CLUSTERED ([REIBInstitutionalOwnerID] ASC),
    CONSTRAINT [FK_REIBInstitutionalOwners_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners', @level2type = N'COLUMN', @level2name = N'SharesOwned';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners', @level2type = N'COLUMN', @level2name = N'REIBInstitutionalOwnerID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners', @level2type = N'COLUMN', @level2name = N'PercentOwned';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REIBInstitutionalOwners', @level2type = N'COLUMN', @level2name = N'AddedBy';




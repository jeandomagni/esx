﻿CREATE TABLE [dbo].[Streams] (
    [StreamID]    BIGINT         IDENTITY (1, 1) NOT NULL,
    [DataType]    NVARCHAR (50)  NULL,
    [RequestType] NVARCHAR (100) NULL,
    [DateAdded]   DATETIME       NULL,
    [AddedBy]     NVARCHAR (50)  NULL,
    [StreamData]  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Streams] PRIMARY KEY CLUSTERED ([StreamID] ASC)
);


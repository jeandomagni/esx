﻿CREATE TABLE [dbo].[RestaurantTypes] (
    [RestaurantTypeID] INT            IDENTITY (1, 1) NOT NULL,
    [RestaurantType]   NVARCHAR (50)  NULL,
    [DisplayOrder]     INT            NULL,
    [RowVersion]       ROWVERSION     NULL,
    [DateAdded]        DATETIME       NULL,
    [LastUpdate]       DATETIME       NULL,
    [AddedBy]          NVARCHAR (100) NULL,
    [UpdatedBy]        NVARCHAR (100) NULL,
    CONSTRAINT [PK_RestaurantTypes] PRIMARY KEY CLUSTERED ([RestaurantTypeID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=Lookup,Notes=Unimplemented feature brought over from Secured Capital version', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RestaurantTypes';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RestaurantTypes';


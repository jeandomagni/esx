﻿CREATE TABLE [dbo].[DealFees] (
    [DealFeeID]                       BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealID]                          BIGINT           NULL,
    [OPPORTUNITYID_]                  CHAR (12)        NULL,
    [ClientCompanyID]                 BIGINT           NULL,
    [TransactionTypeID]               INT              NULL,
    [PortfolioLevelFee]               BIT              CONSTRAINT [DF_DealFees_PortfolioLevelFee] DEFAULT ((0)) NULL,
    [PortfolioLevelMarketingStatusID] INT              NULL,
    [ProductNameID]                   INT              NULL,
    [FeeStatusID]                     INT              NULL,
    [FeeDate]                         DATETIME         NULL,
    [FeeDescription]                  NVARCHAR (500)   NULL,
    [FeeAmount]                       MONEY            NULL,
    [TransactionAmount]               MONEY            NULL,
    [FeeSplitWithWF]                  BIT              NULL,
    [WFREIBGrossFee]                  MONEY            NULL,
    [ESPercentageOnWFSplit]           DECIMAL (18, 1)  NULL,
    [AnticipatedFeeProbabilityID]     INT              NULL,
    [CoBrokerFee]                     MONEY            NULL,
    [GreenStreetFee]                  MONEY            NULL,
    [ExcludeFromReport]               BIT              NOT NULL,
    [DateAdded]                       DATETIME         NULL,
    [LastUpdate]                      DATETIME         NULL,
    [msrepl_tran_version]             UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_F24F6910_BE5F_4D83_9B56_9A4878BB3AAD_1022626686] DEFAULT (newid()) NOT NULL,
    [RowVersion]                      ROWVERSION       NOT NULL,
    [AddedBy]                         NVARCHAR (100)   NULL,
    [UpdatedBy]                       NVARCHAR (100)   NULL,
    CONSTRAINT [PK_DealFees] PRIMARY KEY CLUSTERED ([DealFeeID] ASC),
    CONSTRAINT [FK_DealFees_AnticipatedFeeProbabilities] FOREIGN KEY ([AnticipatedFeeProbabilityID]) REFERENCES [dbo].[AnticipatedFeeProbabilities] ([AnticipatedFeeProbabilityID]),
    CONSTRAINT [FK_DealFees_Companies] FOREIGN KEY ([ClientCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealFees_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealFees_DealStatuses] FOREIGN KEY ([PortfolioLevelMarketingStatusID]) REFERENCES [dbo].[DealStatuses] ([DealStatusID]),
    CONSTRAINT [FK_DealFees_FeeStatuses] FOREIGN KEY ([FeeStatusID]) REFERENCES [dbo].[FeeStatuses] ([FeeStatusID]),
    CONSTRAINT [FK_DealFees_TransactionTypes] FOREIGN KEY ([TransactionTypeID]) REFERENCES [dbo].[TransactionTypes] ([TransactionTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=88 @ 92.90:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'WFREIBGrossFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=18 @ 454.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'TransactionTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2139 @ 3.82:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'TransactionAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'ProductNameID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=14 @ 583.93:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'PortfolioLevelMarketingStatusID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 4087.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'PortfolioLevelFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2634 @ 3.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'OPPORTUNITYID_';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3491 @ 2.34:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4737 @ 1.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=64 @ 127.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'GreenStreetFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 2725.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'FeeStatusID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 4087.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'FeeSplitWithWF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=381 @ 21.46:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'FeeDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2138 @ 3.82:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'FeeDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3395 @ 2.41:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'FeeAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 8175.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'ExcludeFromReport';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 2725.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'ESPercentageOnWFSplit';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=7395 @ 1.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'DealFeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4486 @ 1.82:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=434 @ 18.84:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'CoBrokerFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2609 @ 3.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'ClientCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 2043.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'AnticipatedFeeProbabilityID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFees', @level2type = N'COLUMN', @level2name = N'AddedBy';




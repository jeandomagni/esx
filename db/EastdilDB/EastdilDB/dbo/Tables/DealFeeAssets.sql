﻿CREATE TABLE [dbo].[DealFeeAssets] (
    [DealFeeAssetID]   BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealFeeID]        BIGINT         NULL,
    [DealID]           BIGINT         NULL,
    [DealAssetID]      BIGINT         NULL,
    [AssetDescription] NVARCHAR (100) NULL,
    [RowVersion]       ROWVERSION     NOT NULL,
    [DateAdded]        DATETIME       NULL,
    [LastUpdate]       DATETIME       NULL,
    [AddedBy]          NVARCHAR (100) NULL,
    [UpdatedBy]        NVARCHAR (100) NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets', @level2type = N'COLUMN', @level2name = N'DealFeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets', @level2type = N'COLUMN', @level2name = N'DealFeeAssetID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets', @level2type = N'COLUMN', @level2name = N'DealAssetID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets', @level2type = N'COLUMN', @level2name = N'AssetDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealFeeAssets', @level2type = N'COLUMN', @level2name = N'AddedBy';




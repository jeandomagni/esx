﻿CREATE TABLE [dbo].[DealContactBidEquitySaleImport] (
    [DealContactID]              NVARCHAR (50)   NULL,
    [DealID]                     NVARCHAR (50)   NULL,
    [FirstnameLastname]          NVARCHAR (50)   NULL,
    [CompanyName]                NVARCHAR (50)   NULL,
    [MarketingStatus]            NVARCHAR (50)   NULL,
    [Tour]                       NVARCHAR (50)   NULL,
    [BidDate]                    NVARCHAR (50)   NULL,
    [BidRank]                    NVARCHAR (50)   NULL,
    [BidComments]                NVARCHAR (1000) NULL,
    [OfferNumber]                NVARCHAR (50)   NULL,
    [Purchaser]                  NVARCHAR (200)  NULL,
    [InvestorType]               NVARCHAR (50)   NULL,
    [InitialBidAmount]           NVARCHAR (50)   NULL,
    [EquityCapitalProvider]      NVARCHAR (200)  NULL,
    [RevisedBidAmount]           NVARCHAR (50)   NULL,
    [NetProceeds]                NVARCHAR (50)   NULL,
    [DepositInitial]             NVARCHAR (50)   NULL,
    [DepositTotal]               NVARCHAR (50)   NULL,
    [PropertyTour]               NVARCHAR (50)   NULL,
    [PricePSFCALC]               NVARCHAR (50)   NULL,
    [UnderwrittenCapital]        NVARCHAR (50)   NULL,
    [DebtFinancingNeededToClose] NVARCHAR (50)   NULL,
    [DueDiligencePeriodDays]     NVARCHAR (50)   NULL,
    [ClosingPeriodDays]          NVARCHAR (50)   NULL,
    [InPlaceCapRateCALC]         NVARCHAR (50)   NULL,
    [Year1CapRateCALC]           NVARCHAR (50)   NULL,
    [StabilizedCapRateCALC]      NVARCHAR (50)   NULL,
    [ProjectSize]                NVARCHAR (50)   NULL,
    [ProjectSizeType]            NVARCHAR (50)   NULL,
    [DealCode]                   NVARCHAR (50)   NULL,
    [DealName]                   NVARCHAR (50)   NULL,
    [LocationCity]               NVARCHAR (100)  NULL,
    [LocationState]              NVARCHAR (50)   NULL,
    [PropertyType]               NVARCHAR (50)   NULL,
    [PctLeased]                  NVARCHAR (50)   NULL
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Added late last year when team was trying to define how to track Bids', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBidEquitySaleImport';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContactBidEquitySaleImport';


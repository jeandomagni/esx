﻿CREATE TABLE [dbo].[WorkingPartyRoles] (
    [WorkingPartyRoleID]  INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [WorkingPartyRole]    NVARCHAR (50)    NOT NULL,
    [IsInternal]          BIT              CONSTRAINT [DF__WorkingPa__IsInt__7C4554E3] DEFAULT ((0)) NOT NULL,
    [WPCategory]          NVARCHAR (50)    NULL,
    [DisplayOrder]        INT              NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_B8167DC8_1118_46D5_87A7_22FF3068D65A_149575571] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [DateAdded]           DATETIME         NULL,
    [LastUpdate]          DATETIME         NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [PK_WorkingPartyRoles_1] PRIMARY KEY CLUSTERED ([WorkingPartyRoleID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 10.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'WPCategory';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'WorkingPartyRoleID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'WorkingPartyRole';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 25.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'IsInternal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'DisplayOrder';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkingPartyRoles', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
CREATE NONCLUSTERED INDEX [IX_WorkingPartyRoles]
    ON [dbo].[WorkingPartyRoles]([DisplayOrder] ASC, [WorkingPartyRoleID] ASC, [WorkingPartyRole] ASC);


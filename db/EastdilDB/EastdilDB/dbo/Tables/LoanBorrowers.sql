﻿CREATE TABLE [dbo].[LoanBorrowers] (
    [LoanBorrowerID]    BIGINT         IDENTITY (1, 1) NOT NULL,
    [LoanID]            BIGINT         NULL,
    [BorrowerCompanyID] BIGINT         NULL,
    [BorrowerDBA]       NVARCHAR (50)  NULL,
    [BorrowerRank]      INT            NULL,
    [DateAdded]         DATETIME       NULL,
    [LastUpdate]        DATETIME       NULL,
    [AddedBy]           NVARCHAR (100) NULL,
    [UpdatedBy]         NVARCHAR (100) NULL,
    CONSTRAINT [PK_LoanBorrowers] PRIMARY KEY CLUSTERED ([LoanBorrowerID] ASC),
    CONSTRAINT [FK_LoanBorrowers_Companies] FOREIGN KEY ([BorrowerCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_LoanBorrowers_Loans] FOREIGN KEY ([LoanID]) REFERENCES [dbo].[Loans] ([LoanID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers', @level2type = N'COLUMN', @level2name = N'LoanID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers', @level2type = N'COLUMN', @level2name = N'LoanBorrowerID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 185.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 185.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 185.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers', @level2type = N'COLUMN', @level2name = N'BorrowerRank';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=40 @ 4.63:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers', @level2type = N'COLUMN', @level2name = N'BorrowerDBA';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=157 @ 1.18:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers', @level2type = N'COLUMN', @level2name = N'BorrowerCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'LoanBorrowers', @level2type = N'COLUMN', @level2name = N'AddedBy';




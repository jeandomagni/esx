﻿CREATE TABLE [dbo].[Offices] (
    [OfficeID]            INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Office]              NVARCHAR (50)    NOT NULL,
    [LazyName]            NVARCHAR (50)    NULL,
    [MAC]                 NVARCHAR (50)    NULL,
    [Address1]            NVARCHAR (100)   NULL,
    [Address2]            NVARCHAR (100)   NULL,
    [City]                NVARCHAR (50)    NULL,
    [State]               NVARCHAR (50)    NULL,
    [Zip]                 NVARCHAR (50)    NULL,
    [Country]             NVARCHAR (50)    NULL,
    [ES_Site_Name]        NVARCHAR (50)    NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_3B7BFB92_9B6C_4AFE_8033_ECB339272DDF_565577053] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [DateAdded]           DATETIME         NULL,
    [LastUpdate]          DATETIME         NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [PK_Offices] PRIMARY KEY CLUSTERED ([OfficeID] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Offices';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Offices';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Offices', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Offices', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Offices', @level2type = N'COLUMN', @level2name = N'OfficeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Offices', @level2type = N'COLUMN', @level2name = N'Office';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Offices', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Offices', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Offices', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Offices', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[TempContactListContacts] (
    [TempContactListContactID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [AddToDealID]              BIGINT         NULL,
    [AddToContactListID]       BIGINT         NULL,
    [AddFromDealID]            BIGINT         NULL,
    [AddFromContactListID]     BIGINT         NULL,
    [ContactID]                BIGINT         NULL,
    [CompanyID]                BIGINT         NULL,
    [InsertFlag]               BIT            NULL,
    [AlreadyOnList]            BIT            NULL,
    [FirstnameLastname]        NVARCHAR (100) NULL,
    [FullName]                 NVARCHAR (100) NULL,
    [Lastname]                 NVARCHAR (100) NULL,
    [CompanyName]              NVARCHAR (128) NULL,
    [EmailAddress]             NVARCHAR (128) NULL,
    [Address1]                 NVARCHAR (100) NULL,
    [City]                     NVARCHAR (50)  NULL,
    [State]                    NVARCHAR (50)  NULL,
    [RespEmpInitials]          NVARCHAR (50)  NULL,
    [RespEmployeeID]           BIGINT         NULL,
    [UserEmail]                NVARCHAR (50)  NULL,
    [RsvpID]                   INT            NULL,
    [DateAdded]                DATETIME       NULL,
    [LastUpdate]               DATETIME       NULL,
    [AddedBy]                  NVARCHAR (100) NULL,
    [UpdatedBy]                NVARCHAR (100) NULL,
    CONSTRAINT [PK_TempContactListContacts] PRIMARY KEY CLUSTERED ([TempContactListContactID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utility,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=54 @ 254.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'UserEmail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'TempContactListContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=55 @ 249.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'State';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 4579.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'RsvpID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=54 @ 254.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'RespEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=54 @ 254.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'RespEmpInitials';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5682 @ 2.42:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'Lastname';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 6869.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'InsertFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=9024 @ 1.52:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'FullName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8455 @ 1.62:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'FirstnameLastname';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8353 @ 1.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'EmailAddress';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=41 @ 335.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8869 @ 1.55:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3696 @ 3.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'CompanyName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4287 @ 3.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=762 @ 18.03:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'City';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6869.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'AlreadyOnList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=9 @ 1526.56:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'AddToDealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=36 @ 381.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'AddToContactListID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3851 @ 3.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'Address1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 13739.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'AddFromDealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=11 @ 1249.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'AddFromContactListID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TempContactListContacts', @level2type = N'COLUMN', @level2name = N'AddedBy';




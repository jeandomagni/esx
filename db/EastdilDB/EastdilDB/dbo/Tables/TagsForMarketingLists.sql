﻿CREATE TABLE [dbo].[TagsForMarketingLists] (
    [TagID]      BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealID]     BIGINT         NULL,
    [Tag]        NVARCHAR (50)  NULL,
    [DateAdded]  DATETIME       NULL,
    [LastUpdate] DATETIME       NULL,
    [AddedBy]    NVARCHAR (100) NULL,
    [UpdatedBy]  NVARCHAR (100) NULL,
    CONSTRAINT [PK_TagsContacts] PRIMARY KEY CLUSTERED ([TagID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TagsForMarketingLists';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TagsForMarketingLists';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TagsForMarketingLists', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TagsForMarketingLists', @level2type = N'COLUMN', @level2name = N'TagID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=17 @ 1.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TagsForMarketingLists', @level2type = N'COLUMN', @level2name = N'Tag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TagsForMarketingLists', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 9.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TagsForMarketingLists', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TagsForMarketingLists', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TagsForMarketingLists', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[DealLoanSales] (
    [DealLoanSaleID]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealID]                 BIGINT         NULL,
    [Seller]                 NVARCHAR (500) NULL,
    [SellerCompanyID]        BIGINT         NULL,
    [SellerTypeID]           INT            NULL,
    [Buyers]                 NVARCHAR (500) NULL,
    [BuyerCompanyID]         BIGINT         NULL,
    [BuyerTypeID]            INT            NULL,
    [NumberOfLoans]          INT            NULL,
    [Occupancy]              FLOAT (53)     NULL,
    [PctLeased]              FLOAT (53)     NULL,
    [LoanTypeID]             INT            NULL,
    [UnpaidLoanBalance]      MONEY          NULL,
    [UnpaidLoanBalancePerSF] MONEY          NULL,
    [PurchasePrice]          MONEY          NULL,
    [PurchasePricePerSF]     MONEY          NULL,
    [PurchasePricePctOfUPB]  FLOAT (53)     NULL,
    [LoanRateTypeID]         INT            NULL,
    [PropertyNOI]            MONEY          NULL,
    [PropertySite]           NVARCHAR (500) NULL,
    [PropertyValue]          MONEY          NULL,
    [PropertyValuePerSF]     MONEY          NULL,
    [PropertyCapRate]        FLOAT (53)     NULL,
    [EquityLike]             BIT            NOT NULL,
    [OriginationDate]        DATETIME       NULL,
    [MaturityDate]           DATETIME       NULL,
    [Coupon]                 FLOAT (53)     NULL,
    [CashOnCash]             FLOAT (53)     NULL,
    [MostRecentDSCR]         FLOAT (53)     NULL,
    [InPlaceNOI]             MONEY          NULL,
    [EstimatedLTV]           FLOAT (53)     NULL,
    [DebtYield]              FLOAT (53)     NULL,
    [Amortization]           NVARCHAR (50)  NULL,
    [YieldToMaturity]        FLOAT (53)     NULL,
    [NumberOfOffers]         INT            NULL,
    [LastUpdate]             DATETIME       NULL,
    [RowVersion]             ROWVERSION     NULL,
    [DateAdded]              DATETIME       NULL,
    [AddedBy]                NVARCHAR (100) NULL,
    [UpdatedBy]              NVARCHAR (100) NULL,
    CONSTRAINT [PK_DealLoanSales] PRIMARY KEY CLUSTERED ([DealLoanSaleID] ASC),
    CONSTRAINT [FK_DealLoanSales_BuyerCompanies] FOREIGN KEY ([BuyerCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealLoanSales_BuyerOrSponsorTypes] FOREIGN KEY ([BuyerTypeID]) REFERENCES [dbo].[BuyerTypes] ([BuyerTypeID]),
    CONSTRAINT [FK_DealLoanSales_Deals] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealLoanSales_LoanRateTypes] FOREIGN KEY ([LoanRateTypeID]) REFERENCES [dbo].[LoanRateTypes] ([LoanRateTypeID]),
    CONSTRAINT [FK_DealLoanSales_LoanTypes] FOREIGN KEY ([LoanTypeID]) REFERENCES [dbo].[LoanTypes] ([LoanTypeID]),
    CONSTRAINT [FK_DealLoanSales_SellerCompanies] FOREIGN KEY ([SellerCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealLoanSales_SellerTypes] FOREIGN KEY ([SellerTypeID]) REFERENCES [dbo].[SellerTypes] ([SellerTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=57 @ 215.21:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'YieldToMaturity';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=150 @ 81.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'UnpaidLoanBalancePerSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=333 @ 36.84:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'UnpaidLoanBalance';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4089.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'SellerTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=140 @ 87.62:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'SellerCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=153 @ 80.18:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'Seller';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=149 @ 82.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'PurchasePricePerSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=165 @ 74.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'PurchasePricePctOfUPB';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=217 @ 56.53:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'PurchasePrice';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'PropertyValuePerSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'PropertyValue';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'PropertySite';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'PropertyNOI';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'PropertyCapRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=101 @ 121.46:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'PctLeased';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=82 @ 149.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'OriginationDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'Occupancy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=24 @ 511.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'NumberOfOffers';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=28 @ 438.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'NumberOfLoans';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=71 @ 172.77:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'MostRecentDSCR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=107 @ 114.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'MaturityDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5 @ 2453.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'LoanTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5 @ 2453.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'LoanRateTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4526 @ 2.71:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 2044.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'InPlaceNOI';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=80 @ 153.34:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'EstimatedLTV';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6133.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'EquityLike';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'DebtYield';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'DealLoanSaleID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=42 @ 292.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'Coupon';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=26 @ 471.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'CashOnCash';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 2453.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'BuyerTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=244 @ 50.27:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'Buyers';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=54 @ 227.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'BuyerCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=16 @ 766.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'Amortization';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoanSales', @level2type = N'COLUMN', @level2name = N'AddedBy';




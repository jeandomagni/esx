﻿CREATE TABLE [dbo].[Entertainment] (
    [EntertainmentID]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [EmployeeID]          BIGINT          NULL,
    [ContactID]           BIGINT          NULL,
    [CompanyID]           BIGINT          NULL,
    [Identifier]          NVARCHAR (50)   NULL,
    [DateEntertained]     DATETIME        NULL,
    [Description]         NVARCHAR (2000) NULL,
    [VendorName]          NVARCHAR (1000) NULL,
    [ExpenseTypeID]       INT             NULL,
    [EntertainmentAmount] MONEY           NULL,
    [OtherAttendees]      NVARCHAR (2000) NULL,
    [DateAdded]           DATETIME        NULL,
    [LastUpdate]          DATETIME        NULL,
    [RowVersion]          ROWVERSION      NOT NULL,
    [AddedBy]             NVARCHAR (100)  NULL,
    [UpdatedBy]           NVARCHAR (100)  NULL,
    CONSTRAINT [PK_Entertainment] PRIMARY KEY CLUSTERED ([EntertainmentID] ASC),
    CONSTRAINT [FK_Entertainment_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_Entertainment_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contacts] ([ContactID]),
    CONSTRAINT [FK_Entertainment_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_Entertainment_ExpenseTypes] FOREIGN KEY ([ExpenseTypeID]) REFERENCES [dbo].[ExpenseTypes] ([ExpenseTypeID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Entertainment';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Entertainment';


﻿CREATE TABLE [dbo].[RCATransactions] (
    [RCATransactionID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Deal_id]                    FLOAT (53)     NULL,
    [Property_id]                FLOAT (53)     NULL,
    [PortFolio]                  NVARCHAR (255) NULL,
    [Property_nb]                FLOAT (53)     NULL,
    [Status_tx]                  NVARCHAR (255) NULL,
    [Status_dt]                  DATETIME       NULL,
    [IntConveyed_nb]             NVARCHAR (255) NULL,
    [IntConvey_tx]               NVARCHAR (255) NULL,
    [TransType_tx]               NVARCHAR (255) NULL,
    [PartialInterestType_tx]     NVARCHAR (255) NULL,
    [PropertyName]               NVARCHAR (255) NULL,
    [Address_tx]                 NVARCHAR (255) NULL,
    [City_tx]                    NVARCHAR (255) NULL,
    [State_cd]                   NVARCHAR (255) NULL,
    [Zip_cd]                     NVARCHAR (255) NULL,
    [Region]                     NVARCHAR (255) NULL,
    [RCA_Metros_tx]              NVARCHAR (255) NULL,
    [RCA_Markets_tx]             NVARCHAR (255) NULL,
    [CBD_fg]                     BIT            NULL,
    [MainType]                   NVARCHAR (255) NULL,
    [SubType]                    NVARCHAR (255) NULL,
    [OutputCategory1]            NVARCHAR (255) NULL,
    [OutputCategory2]            NVARCHAR (255) NULL,
    [Tenancy_tx]                 NVARCHAR (255) NULL,
    [YearBlt]                    FLOAT (53)     NULL,
    [YearRenuExp_nb]             FLOAT (53)     NULL,
    [NumberBldgs_nb]             FLOAT (53)     NULL,
    [NumberFloors_nb]            NVARCHAR (255) NULL,
    [Occupancy_Rate]             FLOAT (53)     NULL,
    [Price]                      MONEY          NULL,
    [StatusPriceAdjustedUSD_amt] MONEY          NULL,
    [PSF_PPU]                    MONEY          NULL,
    [CapRate]                    FLOAT (53)     NULL,
    [CapQualifyer]               NVARCHAR (255) NULL,
    [DealQualifyer]              NVARCHAR (255) NULL,
    [SqFt_nb]                    FLOAT (53)     NULL,
    [NumberUnits_nb]             NVARCHAR (255) NULL,
    [BuyerName1]                 NVARCHAR (255) NULL,
    [BuyerJV]                    NVARCHAR (255) NULL,
    [BuyerName2]                 NVARCHAR (255) NULL,
    [BuyerJV2]                   NVARCHAR (255) NULL,
    [BuyerName3]                 NVARCHAR (255) NULL,
    [SellerName1]                NVARCHAR (255) NULL,
    [SellerJV2]                  NVARCHAR (255) NULL,
    [SellerName3]                NVARCHAR (255) NULL,
    [Tenant1]                    NVARCHAR (255) NULL,
    [Tenant2]                    NVARCHAR (255) NULL,
    [Tenant3]                    NVARCHAR (255) NULL,
    [BuyerObjective]             NVARCHAR (255) NULL,
    [Deal_Update_dt]             DATETIME       NULL,
    [Property_Update_dt]         DATETIME       NULL,
    [Prior_Sale_dt]              DATETIME       NULL,
    [Prior_Sale_Price_at]        MONEY          NULL,
    [Excess_Land_Potential_fg]   BIT            NULL,
    [Leaseback_fg]               BIT            NULL,
    [LeasebackType_tx]           NVARCHAR (255) NULL,
    [County_nm]                  NVARCHAR (255) NULL,
    [land_area_acres_nb]         FLOAT (53)     NULL,
    [BuyerCapGroup1]             NVARCHAR (255) NULL,
    [BuyerCapGroup2]             NVARCHAR (255) NULL,
    [BuyerCapGroup3]             NVARCHAR (255) NULL,
    [BuyerCapType1]              NVARCHAR (255) NULL,
    [BuyerCapType2]              NVARCHAR (255) NULL,
    [BuyerCapType3]              NVARCHAR (255) NULL,
    [BuyerCountry]               NVARCHAR (255) NULL,
    [BuyerCountry2]              NVARCHAR (255) NULL,
    [BuyerCountry3]              NVARCHAR (255) NULL,
    [SellerCapGroup1]            NVARCHAR (255) NULL,
    [SellerCapGroup2]            NVARCHAR (255) NULL,
    [SellerCapGroup3]            NVARCHAR (255) NULL,
    [SellerCapType1]             NVARCHAR (255) NULL,
    [SellerCapType2]             NVARCHAR (255) NULL,
    [SellerCapType3]             NVARCHAR (255) NULL,
    [SellerCountry]              NVARCHAR (255) NULL,
    [SellerCountry2]             NVARCHAR (255) NULL,
    [SellerCountry3]             NVARCHAR (255) NULL,
    [SellerBrokerageGroup]       NVARCHAR (255) NULL,
    [BuyerBrokerageGroup]        NVARCHAR (255) NULL,
    [CBSA_cd]                    NVARCHAR (255) NULL,
    [Lat_nb]                     FLOAT (53)     NULL,
    [Lon_nb]                     FLOAT (53)     NULL,
    [FIPS_cd]                    NVARCHAR (255) NULL,
    [Metro_Div_cd]               NVARCHAR (255) NULL,
    [HotelFranchise_nm]          NVARCHAR (255) NULL,
    [Mtg_Space_nb]               NVARCHAR (255) NULL,
    [DevMainType]                NVARCHAR (255) NULL,
    [DevSubType1]                NVARCHAR (255) NULL,
    [DevSubType2]                NVARCHAR (255) NULL,
    [DevCalcSqFt]                NVARCHAR (255) NULL,
    [DevComments]                NVARCHAR (255) NULL,
    [ImprovedType_tx]            NVARCHAR (255) NULL,
    [ImprovedComments_tx]        NVARCHAR (255) NULL,
    [DevPriorUse_tx]             NVARCHAR (255) NULL,
    [PricePerLandAcre]           MONEY          NULL,
    [PricePerLandSqFt]           MONEY          NULL,
    [PricePerBldSqFt]            NVARCHAR (255) NULL,
    [PercentProjectCost]         NVARCHAR (255) NULL,
    [DevFloors_nb]               NVARCHAR (255) NULL,
    [DevComplete_dt]             NVARCHAR (255) NULL,
    [Website_tx]                 NVARCHAR (255) NULL,
    [Lender]                     NVARCHAR (255) NULL,
    [Loan_amt]                   MONEY          NULL,
    [Lender_Interest_Rate_nb]    NVARCHAR (255) NULL,
    [Lender_Int_Rate_Type_tx]    NVARCHAR (255) NULL,
    [Loan_Term_Mths_tx]          NVARCHAR (255) NULL,
    [Loan_Maturity_dt]           NVARCHAR (255) NULL,
    [Lender_Comments_tx]         NVARCHAR (255) NULL,
    [Loan_Orig_dt]               DATETIME       NULL,
    [Loan_Int_Method_tx]         NVARCHAR (255) NULL,
    [Loan_Cross_Default_fg]      BIT            NULL,
    [Loan_PI_Payment_amt]        MONEY          NULL,
    [Loan_Orig_IO_Terms_nb]      FLOAT (53)     NULL,
    [Loan_Int_Method_IO_tx]      NVARCHAR (255) NULL,
    [Loan_Amort_Orig_nb]         FLOAT (53)     NULL,
    [DSCR_nb]                    FLOAT (53)     NULL,
    [Loan_LTV_Orig_nb]           FLOAT (53)     NULL,
    [Loan_PrePay_tx]             NVARCHAR (255) NULL,
    [Mort_Brokerage_Name]        NVARCHAR (255) NULL,
    [BuyerAssumedDebt]           FLOAT (53)     NULL,
    [Originator_tx]              NVARCHAR (255) NULL,
    [CMBS_Issue_tx]              NVARCHAR (255) NULL,
    [CMBS_fg]                    BIT            NULL,
    [DateAdded]                  DATETIME       NULL,
    [LastUpdate]                 DATETIME       NULL,
    [AddedBy]                    NVARCHAR (100) NULL,
    [UpdatedBy]                  NVARCHAR (100) NULL,
    CONSTRAINT [PK_RCATransactions] PRIMARY KEY CLUSTERED ([RCATransactionID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=Imported from RCA (Real Capital Analytics) Transactions - Subscription data service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10351 @ 15.42:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Zip_cd';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=99 @ 1612.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'YearRenuExp_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=213 @ 749.27:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'YearBlt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2144 @ 74.44:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Website_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 79797.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'TransType_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9363 @ 17.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Tenant3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=13803 @ 11.56:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Tenant2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=22190 @ 7.19:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Tenant1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 39898.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Tenancy_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10 @ 15959.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SubType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=47079 @ 3.39:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'StatusPriceAdjustedUSD_amt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 79797.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Status_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4019 @ 39.71:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Status_dt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=52 @ 3069.12:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'State_cd';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=69992 @ 2.28:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SqFt_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=416 @ 383.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerName3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=70134 @ 2.28:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerName1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 39898.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerJV2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=20 @ 7979.70:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerCountry3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=30 @ 5319.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerCountry2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=55 @ 2901.71:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerCountry';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=21 @ 7599.71:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerCapType3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25 @ 6383.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerCapType2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=26 @ 6138.23:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerCapType1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 19949.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerCapGroup3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 19949.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerCapGroup2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 19949.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerCapGroup1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6002 @ 26.59:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'SellerBrokerageGroup';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 22799.14:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Region';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'RCATransactionID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=100 @ 1595.94:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'RCA_Metros_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=117 @ 1364.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'RCA_Markets_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'PSF_PPU';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=104313 @ 1.53:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'PropertyName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=87767 @ 1.82:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Property_Update_dt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=92 @ 1734.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Property_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Property_id';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=21322 @ 7.48:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Prior_Sale_Price_at';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7223 @ 22.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Prior_Sale_dt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=95794 @ 1.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'PricePerLandSqFt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=100504 @ 1.59:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'PricePerLandAcre';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1148 @ 139.02:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'PricePerBldSqFt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=46537 @ 3.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Price';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 79797.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'PortFolio';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1230 @ 129.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'PercentProjectCost';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 53198.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'PartialInterestType_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=24 @ 6649.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'OutputCategory2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=27 @ 5910.89:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'OutputCategory1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4153 @ 38.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Originator_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=888 @ 179.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Occupancy_Rate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=928 @ 171.98:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'NumberUnits_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=538 @ 296.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'NumberFloors_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=111 @ 1437.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'NumberBldgs_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=737 @ 216.55:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Mtg_Space_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=292 @ 546.55:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Mort_Brokerage_Name';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=30 @ 5319.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Metro_Div_cd';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'MainType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=118396 @ 1.35:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Lon_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=220 @ 725.43:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_Term_Mths_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2287 @ 69.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_PrePay_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2750 @ 58.03:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_PI_Payment_amt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=69 @ 2312.96:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_Orig_IO_Terms_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2864 @ 55.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_Orig_dt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2018 @ 79.09:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_Maturity_dt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12565 @ 12.70:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_LTV_Orig_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=40 @ 3989.85:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_Int_Method_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=47 @ 3395.62:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_Int_Method_IO_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 79797.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_Cross_Default_fg';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=14970 @ 10.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_amt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=95 @ 1679.94:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Loan_Amort_Orig_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1457 @ 109.54:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Lender_Interest_Rate_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 39898.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Lender_Int_Rate_Type_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6178 @ 25.83:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Lender_Comments_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4783 @ 33.37:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Lender';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 31918.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'LeasebackType_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 79797.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Leaseback_fg';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=116305 @ 1.37:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Lat_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=31308 @ 5.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'land_area_acres_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=90 @ 1773.27:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'IntConveyed_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 17732.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'IntConvey_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 26599.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'ImprovedType_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=169 @ 944.34:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'ImprovedComments_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=200 @ 797.97:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'HotelFranchise_nm';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1698 @ 93.99:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'FIPS_cd';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 79797.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Excess_Land_Potential_fg';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=442 @ 361.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DSCR_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=19 @ 8399.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DevSubType2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12 @ 13299.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DevSubType1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=13 @ 12276.46:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DevPriorUse_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 39898.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DevMainType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=72 @ 2216.58:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DevFloors_nb';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=36 @ 4433.17:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DevComplete_dt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2499 @ 63.86:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DevComments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=653 @ 244.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DevCalcSqFt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 22799.14:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DealQualifyer';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=77278 @ 2.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Deal_Update_dt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=130497 @ 1.22:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Deal_id';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1140 @ 139.99:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'County_nm';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'CMBS_Issue_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 79797.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'CMBS_fg';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4500 @ 35.47:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'City_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=883 @ 180.74:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'CBSA_cd';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 79797.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'CBD_fg';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3588 @ 44.48:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'CapRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 22799.14:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'CapQualifyer';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 26599.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerObjective';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=499 @ 319.83:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerName3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3715 @ 42.96:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerName2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=65850 @ 2.42:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerName1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 39898.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerJV2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 39898.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerJV';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=19 @ 8399.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerCountry3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=31 @ 5148.19:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerCountry2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=69 @ 2312.96:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerCountry';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=18 @ 8866.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerCapType3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25 @ 6383.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerCapType2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25 @ 6383.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerCapType1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 22799.14:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerCapGroup3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 19949.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerCapGroup2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 22799.14:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerCapGroup1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2103 @ 75.89:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerBrokerageGroup';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 79797.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'BuyerAssumedDebt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=123536 @ 1.29:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'Address_tx';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RCATransactions', @level2type = N'COLUMN', @level2name = N'AddedBy';




﻿CREATE TABLE [dbo].[DoNotSolicitRequest] (
    [DoNotSolicitRequestID] INT            IDENTITY (1, 1) NOT NULL,
    [RequestedBy]           NVARCHAR (50)  NULL,
    [DateAdded]             DATETIME       NULL,
    [LastUpdate]            DATETIME       NULL,
    [AddedBy]               NVARCHAR (100) NULL,
    [UpdatedBy]             NVARCHAR (100) NULL,
    CONSTRAINT [PK_DoNotSolicitRequest] PRIMARY KEY CLUSTERED ([DoNotSolicitRequestID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DoNotSolicitRequest';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DoNotSolicitRequest';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DoNotSolicitRequest', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DoNotSolicitRequest', @level2type = N'COLUMN', @level2name = N'RequestedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DoNotSolicitRequest', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DoNotSolicitRequest', @level2type = N'COLUMN', @level2name = N'DoNotSolicitRequestID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DoNotSolicitRequest', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DoNotSolicitRequest', @level2type = N'COLUMN', @level2name = N'AddedBy';




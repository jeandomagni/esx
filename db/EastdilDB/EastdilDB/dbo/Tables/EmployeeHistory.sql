﻿CREATE TABLE [dbo].[EmployeeHistory] (
    [EmployeeHistoryID] INT            IDENTITY (1, 1) NOT NULL,
    [EmployeeID]        BIGINT         NOT NULL,
    [EventType]         NVARCHAR (50)  NULL,
    [EventDate]         DATETIME       NULL,
    [Comment]           NVARCHAR (50)  NULL,
    [DateAdded]         DATETIME       NULL,
    [LastUpdate]        DATETIME       NULL,
    [AddedBy]           NVARCHAR (100) NULL,
    [UpdatedBy]         NVARCHAR (100) NULL,
    CONSTRAINT [PK_tblEmployeeHistory] PRIMARY KEY CLUSTERED ([EmployeeHistoryID] ASC),
    CONSTRAINT [FK_EmployeeHistory_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=Used in ESNet not ESP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8 @ 220.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory', @level2type = N'COLUMN', @level2name = N'EventType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=793 @ 2.23:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory', @level2type = N'COLUMN', @level2name = N'EventDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=842 @ 2.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory', @level2type = N'COLUMN', @level2name = N'EmployeeHistoryID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=307 @ 5.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory', @level2type = N'COLUMN', @level2name = N'Comment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeHistory', @level2type = N'COLUMN', @level2name = N'AddedBy';




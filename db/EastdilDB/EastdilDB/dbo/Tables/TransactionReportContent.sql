﻿CREATE TABLE [dbo].[TransactionReportContent] (
    [ReportContentID]         INT            IDENTITY (1, 1) NOT NULL,
    [DealID]                  INT            NULL,
    [ReportName]              NVARCHAR (50)  NULL,
    [ReportRequester]         NVARCHAR (50)  NULL,
    [Market]                  NVARCHAR (50)  NULL,
    [Submarket]               NVARCHAR (50)  NULL,
    [Confidential]            BIT            NULL,
    [ConfidentialDescription] NVARCHAR (50)  NULL,
    [Comments]                NVARCHAR (MAX) NULL,
    [DateAdded]               DATETIME       NULL,
    [LastUpdate]              DATETIME       NULL,
    [AddedBy]                 NVARCHAR (100) NULL,
    [UpdatedBy]               NVARCHAR (100) NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utility,Notes=Used by Reports web application', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=21 @ 3.14:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'Submarket';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'ReportRequester';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 66.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'ReportName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'ReportContentID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=10 @ 6.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'Market';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'ConfidentialDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 33.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'Confidential';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=63 @ 1.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'Comments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TransactionReportContent', @level2type = N'COLUMN', @level2name = N'AddedBy';




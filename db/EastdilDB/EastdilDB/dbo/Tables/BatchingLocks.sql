﻿CREATE TABLE [dbo].[BatchingLocks] (
    [BatchingLockID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealID]         BIGINT         NULL,
    [EmployeeID]     BIGINT         NULL,
    [PageLocked]     NVARCHAR (100) NULL,
    [LockOnOrOff]    NVARCHAR (50)  NULL,
    [DateAdded]      DATETIME       NULL,
    [RowVersion]     ROWVERSION     NOT NULL,
    [LastUpdate]     DATETIME       NULL,
    [AddedBy]        NVARCHAR (100) NULL,
    [UpdatedBy]      NVARCHAR (100) NULL,
    CONSTRAINT [PK_BatchingLocks] PRIMARY KEY CLUSTERED ([BatchingLockID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BatchingLocks';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BatchingLocks';


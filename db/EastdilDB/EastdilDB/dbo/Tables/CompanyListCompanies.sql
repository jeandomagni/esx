﻿CREATE TABLE [dbo].[CompanyListCompanies] (
    [CompanyListCompanyID] BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CompanyListID]        INT              NULL,
    [CompanyID]            BIGINT           NULL,
    [LastUpdate]           DATETIME         DEFAULT (getdate()) NULL,
    [msrepl_tran_version]  UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_58AF91B0_4B01_4CA2_8BE8_C6D07EAB8A84_1237579447] DEFAULT (newid()) NOT NULL,
    [RowVersion]           ROWVERSION       NOT NULL,
    [DateAdded]            DATETIME         NULL,
    [AddedBy]              NVARCHAR (100)   NULL,
    [UpdatedBy]            NVARCHAR (100)   NULL,
    CONSTRAINT [PK_CompanyListCompanies] PRIMARY KEY CLUSTERED ([CompanyListCompanyID] ASC),
    CONSTRAINT [FK_CompanyListCompanies_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_CompanyListCompanies_CompanyLists] FOREIGN KEY ([CompanyListID]) REFERENCES [dbo].[CompanyLists] ([CompanyListID])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyListCompanies';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyListCompanies';


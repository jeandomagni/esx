﻿CREATE TABLE [dbo].[SubCommentTypes] (
    [SubCommentTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [SubCommentType]   NVARCHAR (50) NULL,
    [DisplayOrder]     INT           NULL,
    CONSTRAINT [PK_SubCommentTypes] PRIMARY KEY CLUSTERED ([SubCommentTypeID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Lookup,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubCommentTypes';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubCommentTypes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubCommentTypes', @level2type = N'COLUMN', @level2name = N'SubCommentTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubCommentTypes', @level2type = N'COLUMN', @level2name = N'SubCommentType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubCommentTypes', @level2type = N'COLUMN', @level2name = N'DisplayOrder';




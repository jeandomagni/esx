﻿CREATE TABLE [dbo].[SurveyQuestions] (
    [QuestionID]   INT             IDENTITY (1, 1) NOT NULL,
    [SurveyID]     INT             NULL,
    [Question]     NVARCHAR (1000) NULL,
    [DisplayOrder] INT             NULL
);


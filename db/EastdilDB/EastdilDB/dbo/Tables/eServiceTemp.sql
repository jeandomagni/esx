﻿CREATE TABLE [dbo].[eServiceTemp] (
    [VoucherAccount]   NVARCHAR (50)  NULL,
    [Vendor]           NVARCHAR (100) NULL,
    [eServiceNumber]   NVARCHAR (50)  NULL,
    [InvoiceDate]      DATETIME       NULL,
    [Voucher]          NVARCHAR (50)  NULL,
    [GLDate]           DATETIME       NULL,
    [Account]          NVARCHAR (50)  NULL,
    [AU]               NVARCHAR (50)  NULL,
    [MonetaryAmount]   MONEY          NULL,
    [CheckDate]        DATETIME       NULL,
    [CheckNumber]      NVARCHAR (50)  NULL,
    [CheckAmount]      MONEY          NULL,
    [CheckClearedDate] DATETIME       NULL,
    [Comment]          NVARCHAR (100) NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utilty,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'VoucherAccount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=776 @ 1.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'Voucher';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=343 @ 3.96:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'Vendor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1240 @ 1.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'MonetaryAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=28 @ 48.54:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'InvoiceDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=25 @ 54.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'GLDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=775 @ 1.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'eServiceNumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'Comment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=514 @ 2.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'CheckNumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=26 @ 52.27:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'CheckDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=26 @ 52.27:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'CheckClearedDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=484 @ 2.81:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'CheckAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 1359.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'AU';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=45 @ 30.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'eServiceTemp', @level2type = N'COLUMN', @level2name = N'Account';




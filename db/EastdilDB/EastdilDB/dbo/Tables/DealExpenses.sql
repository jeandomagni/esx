﻿CREATE TABLE [dbo].[DealExpenses] (
    [DealExpenseID]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [VoucherAccount]  NVARCHAR (50)  NULL,
    [DealID]          BIGINT         NULL,
    [DealCode]        NVARCHAR (50)  NULL,
    [GLCode]          NVARCHAR (50)  NULL,
    [Comment]         NVARCHAR (100) NULL,
    [TransactionDate] DATETIME       NULL,
    [APVendor]        NVARCHAR (50)  NULL,
    [OfficeID]        INT            NULL,
    [ExpenseAmount]   MONEY          NULL,
    [WorkCode]        NVARCHAR (50)  NULL,
    [EserviceNumber]  NVARCHAR (50)  NULL,
    [InvoiceDate]     DATETIME       NULL,
    [Voucher]         NVARCHAR (50)  NULL,
    [DateAdded]       DATETIME       NULL,
    [LastUpdate]      DATETIME       NULL,
    [AddedBy]         NVARCHAR (100) NULL,
    [UpdatedBy]       NVARCHAR (100) NULL,
    [RecordSource]    NVARCHAR (50)  NULL,
    CONSTRAINT [PK_DealExpenses] PRIMARY KEY CLUSTERED ([DealExpenseID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-One,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=17 @ 5021.29:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'WorkCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=340 @ 251.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'VoucherAccount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 14227.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'Voucher';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=263 @ 324.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'TransactionDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 85362.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'RecordSource';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=11 @ 7760.18:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'OfficeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 14227.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'InvoiceDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12 @ 7113.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'GLCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=29828 @ 2.86:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'ExpenseAmount';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 14227.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'EserviceNumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3635 @ 23.48:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'DealExpenseID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5395 @ 15.82:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'DealCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2067 @ 41.30:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'Comment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=744 @ 114.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'APVendor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealExpenses', @level2type = N'COLUMN', @level2name = N'AddedBy';




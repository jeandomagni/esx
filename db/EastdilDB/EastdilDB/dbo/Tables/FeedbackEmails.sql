﻿CREATE TABLE [dbo].[FeedbackEmails] (
    [FeedbackEmailID]     INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DateSent]            DATETIME         NULL,
    [Sender]              NVARCHAR (100)   NULL,
    [Office]              NVARCHAR (50)    NULL,
    [Title]               NVARCHAR (50)    NULL,
    [FeedbackComments]    NVARCHAR (3000)  NULL,
    [FeatureComments]     NVARCHAR (3000)  NULL,
    [Reviewed]            BIT              CONSTRAINT [DF_FeedbackEmails_Reviewed] DEFAULT ((0)) NOT NULL,
    [Implement]           BIT              CONSTRAINT [DF_FeedbackEmails_Implement] DEFAULT ((0)) NOT NULL,
    [FollowupTask]        NVARCHAR (1000)  NULL,
    [Completed]           BIT              CONSTRAINT [DF_FeedbackEmails_Completed] DEFAULT ((0)) NOT NULL,
    [LastUpdate]          DATETIME         CONSTRAINT [DF_FeedbackEmails_LastUpdate] DEFAULT (getdate()) NULL,
    [msrepl_tran_version] UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_D9EAF7D5_2528_470C_99E2_FD8888249600_2105058535] DEFAULT (newid()) NOT NULL,
    [RowVersion]          ROWVERSION       NOT NULL,
    [DateAdded]           DATETIME         NULL,
    [AddedBy]             NVARCHAR (100)   NULL,
    [UpdatedBy]           NVARCHAR (100)   NULL,
    CONSTRAINT [PK_FeedbackEmails] PRIMARY KEY CLUSTERED ([FeedbackEmailID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utility,Notes=Used in ESNet not ESP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=25 @ 22.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'Title';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=95 @ 5.79:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'Sender';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 275.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'Reviewed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=12 @ 45.83:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'Office';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=130 @ 4.23:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 275.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'Implement';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=79 @ 6.96:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'FollowupTask';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'FeedbackEmailID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=442 @ 1.24:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'FeedbackComments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=123 @ 4.47:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'FeatureComments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=373 @ 1.47:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'DateSent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 275.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'Completed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeedbackEmails', @level2type = N'COLUMN', @level2name = N'AddedBy';




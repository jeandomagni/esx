﻿CREATE TABLE [dbo].[PropertyValuationTenants] (
    [PropertyTenantID]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [PropertyValuationID]  BIGINT          NULL,
    [TenantName]           NVARCHAR (250)  NULL,
    [TenantSF]             INT             NULL,
    [TenantPercentOfTotal] NUMERIC (18, 2) NULL,
    [TenantExpiration]     DATETIME        NULL,
    [LastUpdate]           DATETIME        NULL,
    [RowVersion]           ROWVERSION      NULL,
    [DateAdded]            DATETIME        NULL,
    [AddedBy]              NVARCHAR (100)  NULL,
    [UpdatedBy]            NVARCHAR (100)  NULL,
    CONSTRAINT [PK_PropertyValuationTenants] PRIMARY KEY CLUSTERED ([PropertyTenantID] ASC),
    CONSTRAINT [FK_PropertyValuationTenants_PropertyValuations] FOREIGN KEY ([PropertyValuationID]) REFERENCES [dbo].[PropertyValuations] ([PropertyValuationID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'TenantSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'TenantPercentOfTotal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'TenantName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'TenantExpiration';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'PropertyValuationID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'PropertyTenantID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PropertyValuationTenants', @level2type = N'COLUMN', @level2name = N'AddedBy';




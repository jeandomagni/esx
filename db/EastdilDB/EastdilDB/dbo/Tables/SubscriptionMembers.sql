﻿CREATE TABLE [dbo].[SubscriptionMembers] (
    [SubscriptionMemberID] INT            IDENTITY (1, 1) NOT NULL,
    [SubscriptionID]       INT            NOT NULL,
    [EmployeeID]           INT            NOT NULL,
    [InstallDate]          DATETIME       NULL,
    [ApprovedBy]           NVARCHAR (50)  NULL,
    [ReviewDate]           DATETIME       NULL,
    [RemoveDate]           DATETIME       NULL,
    [DateAdded]            DATETIME       NULL,
    [LastUpdate]           DATETIME       NULL,
    [AddedBy]              NVARCHAR (100) NULL,
    [UpdatedBy]            NVARCHAR (100) NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'SubscriptionMemberID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=7 @ 37.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'SubscriptionID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10 @ 26.30:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'ReviewDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'RemoveDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12 @ 21.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'InstallDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=158 @ 1.66:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=5 @ 52.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'ApprovedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionMembers', @level2type = N'COLUMN', @level2name = N'AddedBy';




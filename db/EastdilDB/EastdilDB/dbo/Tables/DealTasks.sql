﻿CREATE TABLE [dbo].[DealTasks] (
    [DealTaskID]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealID]               BIGINT         NULL,
    [DealWPInternalID]     INT            NULL,
    [DeliverableID]        INT            NULL,
    [WorkActivityID]       INT            NULL,
    [AssignedByEmployeeID] BIGINT         NULL,
    [TaskDescription]      NVARCHAR (100) NULL,
    [TimeRequiredID]       INT            NULL,
    [Priority]             INT            NULL,
    [StatusUpdate]         NVARCHAR (500) NULL,
    [DueDate]              DATETIME       NULL,
    [Active]               BIT            NULL,
    [Complete]             BIT            NULL,
    [DateAdded]            DATETIME       NULL,
    [LastUpdate]           DATETIME       NULL,
    [RowVersion]           ROWVERSION     NULL,
    [AddedBy]              NVARCHAR (100) NULL,
    [UpdatedBy]            NVARCHAR (100) NULL,
    CONSTRAINT [PK_DealTasks] PRIMARY KEY CLUSTERED ([DealTaskID] ASC),
    CONSTRAINT [FK_DealTasks_Deals] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealTasks_DealWPInternal] FOREIGN KEY ([DealWPInternalID]) REFERENCES [dbo].[DealWPInternal] ([DealWPInternalID]),
    CONSTRAINT [FK_DealTasks_Deliverables] FOREIGN KEY ([DeliverableID]) REFERENCES [dbo].[Deliverables] ([DeliverableID]),
    CONSTRAINT [FK_DealTasks_Employees] FOREIGN KEY ([AssignedByEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_DealTasks_TimeRequired] FOREIGN KEY ([TimeRequiredID]) REFERENCES [dbo].[TimeRequired] ([TimeRequiredID]),
    CONSTRAINT [FK_DealTasks_WorkActivities] FOREIGN KEY ([WorkActivityID]) REFERENCES [dbo].[WorkActivities] ([WorkActivityID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=Underused feature - Deal Tasks', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 1.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'WorkActivityID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 2.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'TimeRequiredID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'TaskDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'StatusUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 2.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'Priority';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 1.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'DueDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 1.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'DeliverableID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 2.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'DealWPInternalID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'DealTaskID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 5.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 1.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 2.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'Complete';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 1.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'AssignedByEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 5.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealTasks', @level2type = N'COLUMN', @level2name = N'Active';




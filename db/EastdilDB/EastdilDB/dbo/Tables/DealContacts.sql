﻿CREATE TABLE [dbo].[DealContacts] (
    [DealContactID]                 BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealID]                        BIGINT           NULL,
    [ContactID]                     BIGINT           NULL,
    [CompanyID]                     BIGINT           NULL,
    [DealIDContactID]               BIGINT           NULL,
    [DealContact]                   NVARCHAR (50)    NULL,
    [ESContact]                     NVARCHAR (50)    NULL,
    [EmployeeID]                    BIGINT           NULL,
    [CoverageContactID]             BIGINT           NULL,
    [CoverageTeam]                  NVARCHAR (250)   NULL,
    [Status]                        NVARCHAR (100)   NULL,
    [MarketingStatusID]             INT              NULL,
    [Class]                         NVARCHAR (50)    NULL,
    [ClassID]                       INT              NULL,
    [Category]                      NVARCHAR (50)    NULL,
    [DealInvRole]                   NVARCHAR (50)    NULL,
    [Broker]                        NVARCHAR (100)   NULL,
    [Pools]                         NVARCHAR (500)   NULL,
    [Tier]                          NVARCHAR (50)    NULL,
    [MinAmt]                        MONEY            NULL,
    [MaxAmt]                        MONEY            NULL,
    [CapitalSource]                 NVARCHAR (50)    NULL,
    [ExpressedInterest]             DATETIME         NULL,
    [IMSent]                        DATETIME         NULL,
    [CASent]                        DATETIME         NULL,
    [CALogged]                      DATETIME         NULL,
    [CAUnApproved]                  BIT              CONSTRAINT [DF__DealsCont__CAUnA__7F2BE32F] DEFAULT ((0)) NULL,
    [OMIDNum]                       NVARCHAR (50)    NULL,
    [OMShipPending]                 BIT              CONSTRAINT [DF__DealsCont__OMShi__00200768] DEFAULT ((0)) NULL,
    [DoNotSendOM]                   BIT              CONSTRAINT [DF__DealsCont__DoNot__01142BA1] DEFAULT ((0)) NULL,
    [BrokerSendsOM]                 BIT              CONSTRAINT [DF__DealsCont__Broke__02084FDA] DEFAULT ((0)) NULL,
    [DoNotSendWarroom]              BIT              NULL,
    [OMSent]                        DATETIME         NULL,
    [OMDigital]                     DATETIME         NULL,
    [DIP]                           DATETIME         NULL,
    [AdditionalInfo]                NVARCHAR (250)   NULL,
    [DDSent]                        DATETIME         NULL,
    [Tour]                          DATETIME         NULL,
    [BidOffer]                      DATETIME         NULL,
    [BidOfferAmt]                   BIGINT           NULL,
    [Won]                           DATETIME         NULL,
    [StatusNote]                    NVARCHAR (1000)  NULL,
    [MarkForDelete]                 BIT              CONSTRAINT [DF__DealsCont__MarkF__02FC7413] DEFAULT ((0)) NULL,
    [MktgRptFlag]                   NVARCHAR (3)     NULL,
    [CompanyContactFlag]            BIT              CONSTRAINT [DF__DealsCont__Compa__03F0984C] DEFAULT ((0)) NULL,
    [IsLeadInvestor]                BIT              NULL,
    [CategoryID]                    INT              NULL,
    [WarroomAccess]                 DATETIME         NULL,
    [WinningBidder]                 BIT              NULL,
    [DoNotCopy]                     BIT              NULL,
    [Tags]                          NVARCHAR (500)   NULL,
    [Selected]                      BIT              NULL,
    [InsertFlag]                    BIT              NULL,
    [UpdateFlag]                    BIT              NULL,
    [DeleteFlag]                    BIT              NULL,
    [ApproverEmailFlag]             BIT              NULL,
    [NumComments]                   INT              NULL,
    [DateAdded]                     DATETIME         NULL,
    [LastUpdate]                    DATETIME         CONSTRAINT [DF__DealsCont__LastU__04E4BC85] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]           UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_C70D0493_B95C_4164_8BE5_610DA3A93B50_661577395] DEFAULT (newid()) NOT NULL,
    [RowVersion]                    ROWVERSION       NOT NULL,
    [BidRank]                       INT              NULL,
    [BidSubmitted]                  BIT              NULL,
    [BidDepositNote]                NVARCHAR (100)   NULL,
    [BidDDPeriodNote]               NVARCHAR (100)   NULL,
    [BidDebtFinancing]              BIT              NULL,
    [BidRate]                       FLOAT (53)       NULL,
    [BidSpread]                     FLOAT (53)       NULL,
    [BidLoanTermYears]              INT              NULL,
    [BidPrepaymentPenaltyNote]      NVARCHAR (100)   NULL,
    [BidCapRate]                    FLOAT (53)       NULL,
    [BidPricePSF]                   MONEY            NULL,
    [BidEquityCapital]              NVARCHAR (150)   NULL,
    [AddedBy]                       NVARCHAR (100)   NULL,
    [UpdatedBy]                     NVARCHAR (100)   NULL,
    [BidGeneralNote]                NVARCHAR (500)   NULL,
    [BidOriginationFee]             BIGINT           NULL,
    [BidLoanType]                   NVARCHAR (50)    NULL,
    [BidReferenceIndex]             NVARCHAR (50)    NULL,
    [BidRound]                      NVARCHAR (50)    NULL,
    [CAPortal]                      NVARCHAR (50)    NULL,
    [CAMethod]                      NVARCHAR (50)    NULL,
    [ESI_TARGET_2ND]                BIT              NULL,
    [ESI_TARGET_2ND_DATE]           DATE             NULL,
    [ESI_TARGET_2ND_FIRED]          BIT              NULL,
    [ESI_TARGET_2ND_FIRED_DATE]     DATE             NULL,
    [ESI_TARGET_CFO]                BIT              NULL,
    [ESI_TARGET_CFO_DATE]           DATE             NULL,
    [ESI_TARGET_CFO_FIRED]          BIT              NULL,
    [ESI_TARGET_CFO_FIRED_DATE]     DATE             NULL,
    [ESI_TARGET_ETEASER]            BIT              NULL,
    [ESI_TARGET_ETEASER_DATE]       DATE             NULL,
    [ESI_TARGET_ETEASER_FIRED]      BIT              NULL,
    [ESI_TARGET_ETEASER_FIRED_DATE] DATE             NULL,
    [ESI_TARGET_FINAL]              BIT              NULL,
    [ESI_TARGET_FINAL_DATE]         DATE             NULL,
    [ESI_TARGET_FINAL_FIRED]        BIT              NULL,
    [ESI_TARGET_FINAL_FIRED_DATE]   DATE             NULL,
    [ESI_TARGET_IM_OM]              BIT              NULL,
    [ESI_TARGET_IM_OM_DATE]         DATE             NULL,
    [ESI_TARGET_IM_OM_FIRED]        BIT              NULL,
    [ESI_TARGET_IM_OM_FIRED_DATE]   DATE             NULL,
    [ESI_TARGET_OTHER]              BIT              NULL,
    [ESI_TARGET_OTHER_DATE]         DATE             NULL,
    [ESI_TARGET_OTHER_FIRED]        BIT              NULL,
    [ESI_TARGET_OTHER_FIRED_DATE]   DATE             NULL,
    [ESI_TARGET_WARROOM]            BIT              NULL,
    [ESI_TARGET_WARROOM_DATE]       DATE             NULL,
    [ESI_TARGET_WARROOM_FIRED]      BIT              NULL,
    [ESI_TARGET_WARROOM_FIRED_DATE] DATE             NULL,
    CONSTRAINT [PK_DealsContacts] PRIMARY KEY CLUSTERED ([DealContactID] ASC),
    CONSTRAINT [FK_DealContacts_Categories] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Categories] ([CategoryID]),
    CONSTRAINT [FK_DealContacts_Classes] FOREIGN KEY ([ClassID]) REFERENCES [dbo].[Classes] ([ClassID]),
    CONSTRAINT [FK_DealContacts_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealContacts_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contacts] ([ContactID]),
    CONSTRAINT [FK_DealContacts_Contacts1] FOREIGN KEY ([CoverageContactID]) REFERENCES [dbo].[Contacts] ([ContactID]),
    CONSTRAINT [FK_DealContacts_Deals] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealContacts_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_DealContacts_MarketingStatuses] FOREIGN KEY ([MarketingStatusID]) REFERENCES [dbo].[MarketingStatuses] ([MarketingStatusID])
);








GO
CREATE NONCLUSTERED INDEX [UpdateFlag]
    ON [dbo].[DealContacts]([UpdateFlag] ASC);


GO
CREATE NONCLUSTERED INDEX [DeleteFlag]
    ON [dbo].[DealContacts]([DeleteFlag] ASC);


GO
CREATE NONCLUSTERED INDEX [DealID]
    ON [dbo].[DealContacts]([DealID] ASC);


GO
CREATE NONCLUSTERED INDEX [ContactID]
    ON [dbo].[DealContacts]([ContactID] ASC);


GO
CREATE TRIGGER [dbo].[DealTotals_DeleteTrigger]
ON dbo.DealContacts 
AFTER DELETE
AS

DECLARE @NumRows int
SET @NumRows = (SELECT count(DealID) from deleted)

IF (@NumRows=1)
   BEGIN
	DECLARE @dealID bigint
	DECLARE @contactID bigint
	
	SET @dealID = (SELECT DealID from deleted)
	SET @contactID = (SELECT ContactID from deleted)
	
	-- Deals table
	UPDATE Deals SET Deals.MktgListSize=(SELECT COUNT(*) FROM DealContacts WHERE Deals.DealID=@dealID) WHERE Deals.DealID=@dealID

	UPDATE Deals SET Deals.ActiveApproved=(SELECT COUNT(*) FROM DealContacts INNER JOIN Contacts ON Contacts.ContactID=DealContacts.ContactID 
	WHERE DealContacts.DealID=@dealID AND Contacts.Active=1 AND DealContacts.MarketingStatusID <> 12) 
	WHERE Deals.DealID=@dealID

	-- Contacts table
	UPDATE Contacts SET Contacts.NumDealsOn=(SELECT COUNT(*) FROM DealContacts WHERE DealContacts.ContactID=@contactID) WHERE Contacts.ContactID=@contactID

	UPDATE Contacts SET Contacts.CALogged=(SELECT COUNT(*) FROM DealContacts WHERE ContactID=@contactID AND DealContacts.CALogged Is Not Null) 
	WHERE Contacts.ContactID=@contactID

   END
GO
DISABLE TRIGGER [dbo].[DealTotals_DeleteTrigger]
    ON [dbo].[DealContacts];


GO
CREATE TRIGGER [dbo].[DealTotals_InsertUpdateTrigger]
ON dbo.DealContacts 
AFTER INSERT, UPDATE
AS

DECLARE @NumRows int
SET @NumRows = (SELECT count(DealID) from inserted)

IF (@NumRows=1)
   BEGIN
	DECLARE @dealID bigint
	DECLARE @contactID bigint

	SET @dealID = (SELECT DealID from inserted)
	SET @contactID = (SELECT ContactID from inserted)

	-- Deals table
	UPDATE Deals SET Deals.MktgListSize=(SELECT COUNT(*) FROM DealContacts WHERE DealContacts.DealID=@dealID) WHERE Deals.DealID=@dealID

	UPDATE Deals SET Deals.ActiveApproved=(SELECT COUNT(*) FROM DealContacts INNER JOIN Contacts ON Contacts.ContactID=DealContacts.ContactID 
	WHERE DealContacts.DealID=@dealID AND Contacts.Active=1 AND DealContacts.MarketingStatusID <> 12) 
	WHERE Deals.DealID=@dealID

	-- Contacts table
	UPDATE Contacts SET Contacts.NumDealsOn=(SELECT COUNT(*) FROM DealContacts WHERE DealContacts.ContactID=@contactID) WHERE Contacts.ContactID=@contactID

	UPDATE Contacts SET Contacts.CALogged=(SELECT COUNT(*) FROM DealContacts WHERE ContactID=@contactID AND DealContacts.CALogged Is Not Null) 
	WHERE Contacts.ContactID=@contactID
   END
GO
DISABLE TRIGGER [dbo].[DealTotals_InsertUpdateTrigger]
    ON [dbo].[DealContacts];


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=Corresponds to "Marketing Lists"', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'Won';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'WinningBidder';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1469 @ 2579.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'WarroomAccess';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'UpdateFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3678 @ 1030.32:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'Tour';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 541362.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'Tier';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=9 @ 421059.44:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'Tags';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=589 @ 6433.85:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'StatusNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=15 @ 252635.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'Status';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 3789535.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'Selected';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'Pools';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'OMShipPending';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5952 @ 636.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'OMSent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=731 @ 5184.04:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'OMIDNum';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3104 @ 1220.86:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'OMDigital';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=49 @ 77337.45:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'NumComments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2634744 @ 1.44:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'MktgRptFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 3789535.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'MinAmt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 3789535.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'MaxAmt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 3789535.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'MarkForDelete';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=14 @ 270681.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'MarketingStatusID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1871588 @ 2.02:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'IsLeadInvestor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'InsertFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1169 @ 3241.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'IMSent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'ExpressedInterest';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=500 @ 7579.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'ESContact';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=206 @ 18395.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DoNotSendWarroom';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DoNotSendOM';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DoNotCopy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3292 @ 1151.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DIP';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DeleteFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DealInvRole';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1027637 @ 3.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DealIDContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6994 @ 541.83:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DealContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DealContact';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=111 @ 34139.95:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DDSent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2370236 @ 1.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=534 @ 7096.51:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CoverageTeam';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=21 @ 180454.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CoverageContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=70486 @ 53.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=29755 @ 127.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 3789535.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CompanyContactFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=15 @ 252635.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'ClassID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=17 @ 222913.83:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'Class';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CAUnApproved';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 473691.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CategoryID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=194 @ 19533.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'Category';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2923 @ 1296.45:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CASent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CAPortal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=196 @ 19334.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CapitalSource';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CAMethod';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5825 @ 650.56:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'CALogged';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BrokerSendsOM';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1071 @ 3538.31:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'Broker';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidSubmitted';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=91 @ 41643.24:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidSpread';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 947383.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidRound';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 3789535.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidReferenceIndex';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=85 @ 44582.77:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25 @ 151581.41:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidRank';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=229 @ 16548.19:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidPricePSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=73 @ 51911.44:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidPrepaymentPenaltyNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=80 @ 47369.19:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidOriginationFee';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1062 @ 3568.30:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidOfferAmt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=199 @ 19042.89:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidOffer';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 631589.19:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidLoanType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 473691.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidLoanTermYears';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=303 @ 12506.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidGeneralNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidEquityCapital';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=16 @ 236845.94:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidDepositNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidDebtFinancing';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidDDPeriodNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=101 @ 37520.15:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'BidCapRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 1894767.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'ApproverEmailFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3790 @ 999.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'AdditionalInfo';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealContacts', @level2type = N'COLUMN', @level2name = N'AddedBy';




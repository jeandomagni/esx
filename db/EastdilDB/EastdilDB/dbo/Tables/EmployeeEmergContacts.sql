﻿CREATE TABLE [dbo].[EmployeeEmergContacts] (
    [EmergContactID]       INT            IDENTITY (1, 1) NOT NULL,
    [EmployeeID]           INT            NOT NULL,
    [Contact1Name]         NVARCHAR (50)  NULL,
    [Contact1Address]      NVARCHAR (50)  NULL,
    [Contact1City]         NVARCHAR (50)  NULL,
    [Contact1State]        NVARCHAR (50)  NULL,
    [Contact1Zip]          NVARCHAR (50)  NULL,
    [Contact1HomePhone]    NVARCHAR (50)  NULL,
    [Contact1CellPhone]    NVARCHAR (50)  NULL,
    [Contact1WorkPhone]    NVARCHAR (50)  NULL,
    [Contact1Relationship] NVARCHAR (50)  NULL,
    [Contact2Name]         NVARCHAR (50)  NULL,
    [Contact2Address]      NVARCHAR (50)  NULL,
    [Contact2City]         NVARCHAR (50)  NULL,
    [Contact2State]        NVARCHAR (50)  NULL,
    [Contact2Zip]          NVARCHAR (50)  NULL,
    [Contact2HomePhone]    NVARCHAR (50)  NULL,
    [Contact2CellPhone]    NVARCHAR (50)  NULL,
    [Contact2WorkPhone]    NVARCHAR (50)  NULL,
    [Contact2Relationship] NVARCHAR (50)  NULL,
    [Contact3Name]         NVARCHAR (50)  NULL,
    [Contact3Address]      NVARCHAR (50)  NULL,
    [Contact3City]         NVARCHAR (50)  NULL,
    [Contact3State]        NVARCHAR (50)  NULL,
    [Contact3Zip]          NVARCHAR (50)  NULL,
    [Contact3HomePhone]    NVARCHAR (50)  NULL,
    [Contact3CellPhone]    NVARCHAR (50)  NULL,
    [Contact3WorkPhone]    NVARCHAR (50)  NULL,
    [Contact3Relationship] NVARCHAR (50)  NULL,
    [LastUpdate]           DATETIME       NULL,
    [EditBy]               NVARCHAR (50)  NULL,
    [DateAdded]            DATETIME       NULL,
    [AddedBy]              NVARCHAR (100) NULL,
    [UpdatedBy]            NVARCHAR (100) NULL
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=Used in ESNet not ESP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=389 @ 1.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=515 @ 1.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'EmergContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'EditBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=64 @ 8.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact3Zip';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25 @ 20.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact3WorkPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=18 @ 28.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact3State';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=25 @ 20.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact3Relationship';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=105 @ 4.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact3Name';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=67 @ 7.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact3HomePhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=53 @ 9.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact3City';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=86 @ 6.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact3CellPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=70 @ 7.39:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact3Address';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=189 @ 2.74:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact2Zip';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=54 @ 9.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact2WorkPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=38 @ 13.61:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact2State';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 15.21:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact2Relationship';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=309 @ 1.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact2Name';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=229 @ 2.26:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact2HomePhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=160 @ 3.23:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact2City';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=243 @ 2.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact2CellPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=245 @ 2.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact2Address';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=338 @ 1.53:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact1Zip';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=126 @ 4.10:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact1WorkPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 15.21:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact1State';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=36 @ 14.36:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact1Relationship';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=515 @ 1.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact1Name';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=425 @ 1.22:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact1HomePhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=275 @ 1.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact1City';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=415 @ 1.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact1CellPhone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=484 @ 1.07:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'Contact1Address';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeEmergContacts', @level2type = N'COLUMN', @level2name = N'AddedBy';




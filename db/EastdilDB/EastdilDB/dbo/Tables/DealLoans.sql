﻿CREATE TABLE [dbo].[DealLoans] (
    [DealLoanID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [DealID]     BIGINT         NULL,
    [LoanID]     BIGINT         NULL,
    [DateAdded]  DATETIME       NULL,
    [LastUpdate] DATETIME       NULL,
    [RowVersion] ROWVERSION     NULL,
    [AddedBy]    NVARCHAR (100) NULL,
    [UpdatedBy]  NVARCHAR (100) NULL,
    CONSTRAINT [PK_DealLoans] PRIMARY KEY CLUSTERED ([DealLoanID] ASC),
    CONSTRAINT [FK_DealLoans_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_DealLoans_Loans] FOREIGN KEY ([LoanID]) REFERENCES [dbo].[Loans] ([LoanID]),
    CONSTRAINT [FK_DealLoans_Loans1] FOREIGN KEY ([LoanID]) REFERENCES [dbo].[Loans] ([LoanID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=One-to-Many,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoans';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoans';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoans', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoans', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoans', @level2type = N'COLUMN', @level2name = N'LoanID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4459 @ 1.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoans', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoans', @level2type = N'COLUMN', @level2name = N'DealLoanID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4667 @ 1.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoans', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4459 @ 1.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoans', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealLoans', @level2type = N'COLUMN', @level2name = N'AddedBy';




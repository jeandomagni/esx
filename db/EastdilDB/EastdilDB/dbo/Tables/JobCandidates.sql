﻿CREATE TABLE [dbo].[JobCandidates] (
    [JobCandidateID]  BIGINT          IDENTITY (1, 1) NOT NULL,
    [Lastname]        NVARCHAR (50)   NULL,
    [Firstname]       NVARCHAR (50)   NULL,
    [CandidateTypeID] INT             NULL,
    [DateApplied]     DATETIME        NULL,
    [ResumeFilePath]  NVARCHAR (500)  NULL,
    [InterviewedBy]   NVARCHAR (500)  NULL,
    [InterviewDate]   DATETIME        NULL,
    [Status]          NVARCHAR (50)   NULL,
    [Comment]         NVARCHAR (1000) NULL,
    [DateAdded]       DATETIME        NULL,
    [LastUpdate]      DATETIME        NULL,
    [AddedBy]         NVARCHAR (100)  NULL,
    [UpdatedBy]       NVARCHAR (100)  NULL,
    CONSTRAINT [PK_JobCandidates] PRIMARY KEY CLUSTERED ([JobCandidateID] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=??, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = '??', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'Status';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'ResumeFilePath';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'Lastname';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'JobCandidateID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'InterviewedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'InterviewDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'Firstname';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'DateApplied';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'Comment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'CandidateTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'JobCandidates', @level2type = N'COLUMN', @level2name = N'AddedBy';




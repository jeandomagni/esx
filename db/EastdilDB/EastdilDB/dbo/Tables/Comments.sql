﻿CREATE TABLE [dbo].[Comments] (
    [CommentID]                   BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DealContactID]               BIGINT           NULL,
    [DealID]                      BIGINT           NULL,
    [CompanyID]                   BIGINT           NULL,
    [ContactID]                   BIGINT           NULL,
    [PropertyID]                  BIGINT           NULL,
    [LoanID]                      BIGINT           NULL,
    [DocumentID]                  BIGINT           NULL,
    [LeadInvestorID]              BIGINT           NULL,
    [ContactCoverageAssignmentID] BIGINT           NULL,
    [CommentByIntOrExt]           NVARCHAR (50)    NULL,
    [EmployeeID]                  BIGINT           NULL,
    [CommentByContactID]          BIGINT           NULL,
    [CommentDate]                 DATETIME         NULL,
    [CommentTime]                 DATETIME         NULL,
    [CommentVisibilityID]         INT              NULL,
    [CommentTypeID]               INT              NULL,
    [CommentText]                 NVARCHAR (MAX)   NULL,
    [CommentLong]                 NTEXT            NULL,
    [FollowupDate]                DATETIME         NULL,
    [FollowupDone]                BIT              CONSTRAINT [DF_Comments_FollowupDone] DEFAULT ((0)) NULL,
    [UpdateFlag]                  BIT              NULL,
    [DeleteFlag]                  BIT              NULL,
    [DateAdded]                   DATETIME         NULL,
    [LastUpdate]                  DATETIME         CONSTRAINT [DF__Comments__LastUp__339FAB6E] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]         UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_14113715_0247_4CCB_975B_0A218B1A33C3_981578535] DEFAULT (newid()) NOT NULL,
    [RowVersion]                  ROWVERSION       NOT NULL,
    [AddedBy]                     NVARCHAR (100)   NULL,
    [UpdatedBy]                   NVARCHAR (100)   NULL,
    [CommentIDForSubComment]      BIGINT           NULL,
    [SubCommentTypeID]            INT              NULL,
    [FollowupTask]                NVARCHAR (100)   NULL,
    [FollowupEmployeeID]          BIGINT           NULL,
    [HasSubComments]              BIT              NULL,
    [NumSubComments]              INT              NULL,
    CONSTRAINT [PK_Comments] PRIMARY KEY CLUSTERED ([CommentID] ASC),
    CONSTRAINT [FK_Comments_CommentTypes] FOREIGN KEY ([CommentTypeID]) REFERENCES [dbo].[CommentTypes] ([CommentTypeID]),
    CONSTRAINT [FK_Comments_CommentVisibility] FOREIGN KEY ([CommentVisibilityID]) REFERENCES [dbo].[CommentVisibility] ([CommentVisibilityID]),
    CONSTRAINT [FK_Comments_Companies] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_Comments_ContactCoverageAssignments] FOREIGN KEY ([ContactCoverageAssignmentID]) REFERENCES [dbo].[ContactCoverageAssignments] ([ContactCoverageAssignmentID]),
    CONSTRAINT [FK_Comments_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contacts] ([ContactID]),
    CONSTRAINT [FK_Comments_Contacts1] FOREIGN KEY ([CommentByContactID]) REFERENCES [dbo].[Contacts] ([ContactID]),
    CONSTRAINT [FK_Comments_DealProfiles] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_Comments_DealsContacts] FOREIGN KEY ([DealContactID]) REFERENCES [dbo].[DealContacts] ([DealContactID]),
    CONSTRAINT [FK_Comments_Documents] FOREIGN KEY ([DocumentID]) REFERENCES [dbo].[Documents] ([DocumentID]),
    CONSTRAINT [FK_Comments_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_Comments_Loans] FOREIGN KEY ([LoanID]) REFERENCES [dbo].[Loans] ([LoanID]),
    CONSTRAINT [FK_Comments_Properties] FOREIGN KEY ([PropertyID]) REFERENCES [dbo].[Properties] ([PropertyID]),
    CONSTRAINT [FK_Comments_SubCommentTypes] FOREIGN KEY ([SubCommentTypeID]) REFERENCES [dbo].[SubCommentTypes] ([SubCommentTypeID])
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=Primary use of Comments is for DealContacts (Marketing Lists)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 336772.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'UpdateFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 168386.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'SubCommentTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 673544.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'PropertyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 168386.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'NumSubComments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=373888 @ 1.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'LoanID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'LeadInvestorID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=184299 @ 3.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 336772.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'HasSubComments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=15 @ 44902.93:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'FollowupTask';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'FollowupEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 336772.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'FollowupDone';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=275 @ 2449.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'FollowupDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=402 @ 1675.48:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'EmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'DocumentID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 673544.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'DeleteFlag';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4519 @ 149.05:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=334397 @ 2.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'DealContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=218898 @ 3.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=36063 @ 18.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'ContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'ContactCoverageAssignmentID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=16622 @ 40.52:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'CompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 336772.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'CommentVisibilityID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 96220.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'CommentTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'CommentTime';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=198230 @ 3.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'CommentText';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=34 @ 19810.12:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'CommentIDForSubComment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'CommentID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=52359 @ 12.86:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'CommentDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 134708.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'CommentByIntOrExt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=47 @ 14330.72:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'CommentByContactID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=156 @ 4317.59:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Comments', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
CREATE NONCLUSTERED INDEX [IX_COMMENTTEXT]
    ON [dbo].[Comments]([DealContactID] ASC, [CommentDate] ASC, [CommentVisibilityID] ASC)
    INCLUDE([CommentText]);


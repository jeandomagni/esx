﻿CREATE TABLE [dbo].[PrintRequests] (
    [PrintRequestID]       BIGINT          IDENTITY (1, 1) NOT NULL,
    [DealID]               BIGINT          NULL,
    [RequesterEmployeeID]  BIGINT          NULL,
    [ESGSLeadEmployeeID]   BIGINT          NULL,
    [DateRequested]        DATETIME        NULL,
    [DateNeeded]           DATETIME        NULL,
    [SourceFileTypeID]     INT             NULL,
    [NumberPages]          INT             NULL,
    [CopiesRequested]      INT             NULL,
    [BindingTypeAndVendor] NVARCHAR (50)   NULL,
    [RequestStatusID]      INT             NULL,
    [StatusComment]        NVARCHAR (4000) NULL,
    [LastUpdate]           DATETIME        NULL,
    [DateAdded]            DATETIME        NULL,
    [AddedBy]              NVARCHAR (100)  NULL,
    [UpdatedBy]            NVARCHAR (100)  NULL,
    CONSTRAINT [PK_PrintRequests] PRIMARY KEY CLUSTERED ([PrintRequestID] ASC),
    CONSTRAINT [FK_PrintRequests_Deals] FOREIGN KEY ([DealID]) REFERENCES [dbo].[Deals] ([DealID]),
    CONSTRAINT [FK_PrintRequests_ESGSLeadEmployees] FOREIGN KEY ([ESGSLeadEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_PrintRequests_RequesterEmployees] FOREIGN KEY ([RequesterEmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID]),
    CONSTRAINT [FK_PrintRequests_RequestStatuses] FOREIGN KEY ([RequestStatusID]) REFERENCES [dbo].[RequestStatuses] ([RequestStatusID]),
    CONSTRAINT [FK_PrintRequests_SourceFileTypes] FOREIGN KEY ([SourceFileTypeID]) REFERENCES [dbo].[SourceFileTypes] ([SourceFileTypeID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'StatusComment';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 2.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'SourceFileTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 1.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'RequestStatusID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 1.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'RequesterEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'PrintRequestID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'NumberPages';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 1.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 1.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'ESGSLeadEmployeeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 1.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 1.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'DateRequested';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'DateNeeded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4 @ 1.25:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'CopiesRequested';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'BindingTypeAndVendor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PrintRequests', @level2type = N'COLUMN', @level2name = N'AddedBy';




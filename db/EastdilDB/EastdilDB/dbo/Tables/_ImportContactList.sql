﻿CREATE TABLE [dbo].[_ImportContactList] (
    [ContactID]     BIGINT         NULL,
    [Email]         NVARCHAR (128) NULL,
    [ContactListID] BIGINT         NULL,
    [AddedBy]       NVARCHAR (100) NULL,
    [UpdatedBy]     NVARCHAR (100) NULL
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'_ImportContactList';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'_ImportContactList';


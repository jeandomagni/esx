﻿CREATE TABLE [dbo].[Deals] (
    [DealID]                         BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Description]                    NVARCHAR (100)   NULL,
    [DealCode]                       NVARCHAR (50)    NULL,
    [PreCode]                        NVARCHAR (50)    NULL,
    [DealName]                       NVARCHAR (250)   NULL,
    [ClientCompanyID]                BIGINT           NULL,
    [TransactionTypeID]              INT              NULL,
    [PropertyTypeID]                 INT              NULL,
    [Active]                         BIT              CONSTRAINT [DF__DealProfi__Activ__46B27FE2] DEFAULT ((0)) NULL,
    [Confidential]                   BIT              NULL,
    [UsePEMarketingList]             BIT              NULL,
    [MarketedOffshore]               BIT              NULL,
    [DealOffice]                     NVARCHAR (255)   NULL,
    [DealLeader]                     NVARCHAR (50)    NULL,
    [DealManager]                    NVARCHAR (50)    NULL,
    [DealTeamAccessOnly]             BIT              CONSTRAINT [DF_DealProfiles_SecuritiesTransaction] DEFAULT ((0)) NULL,
    [DealFolderPath]                 NVARCHAR (500)   NULL,
    [ESGSDealFolderPath]             NVARCHAR (500)   NULL,
    [ClosingSummaryFilePath]         NVARCHAR (500)   NULL,
    [DealPhotoFilePath]              NVARCHAR (500)   NULL,
    [PitchTeamFilePath]              NVARCHAR (500)   NULL,
    [Closed]                         BIT              CONSTRAINT [DF__DealProfi__Close__47A6A41B] DEFAULT ((0)) NULL,
    [DateClosed]                     DATETIME         NULL,
    [VisibleOnRpt]                   BIT              CONSTRAINT [DF__DealProfi__Visib__489AC854] DEFAULT ((0)) NULL,
    [DealCallNote]                   NVARCHAR (MAX)   NULL,
    [LocationAddress]                NVARCHAR (1000)  NULL,
    [LocationCity]                   NVARCHAR (500)   NULL,
    [LocationCounty]                 NVARCHAR (1000)  NULL,
    [LocationState]                  NVARCHAR (250)   NULL,
    [USMarketRegionID]               INT              NULL,
    [RegionalMarketID]               INT              NULL,
    [RegionalSubmarketID]            INT              NULL,
    [ExcludeFromPipelineReport]      BIT              NULL,
    [ApproximateDealTiming]          NVARCHAR (50)    NULL,
    [YearBuilt]                      NVARCHAR (50)    NULL,
    [DealStatusID]                   INT              NULL,
    [Portfolio]                      CHAR (1)         NULL,
    [PortfolioBitField]              BIT              NULL,
    [Buildings]                      INT              NULL,
    [OfficeID]                       INT              NULL,
    [IBProductNameID]                INT              NULL,
    [DealYear]                       NVARCHAR (50)    NULL,
    [ESTeam]                         NVARCHAR (255)   NULL,
    [StatusComments]                 NVARCHAR (1000)  NULL,
    [AcquiredBy]                     NVARCHAR (64)    NULL,
    [SourcedBy]                      NVARCHAR (50)    NULL,
    [DealStatus]                     NVARCHAR (64)    NULL,
    [DebtManager]                    NVARCHAR (50)    NULL,
    [IBEquityDebtRoleID]             INT              NULL,
    [DebtComp]                       CHAR (1)         NULL,
    [DebtSource]                     NVARCHAR (50)    NULL,
    [OffshoreBidder]                 CHAR (1)         NULL,
    [FinancingOpp]                   NVARCHAR (100)   NULL,
    [FinancingOppLevel]              NVARCHAR (50)    NULL,
    [FinancingOppLevelID]            INT              NULL,
    [SellingBrokerCompanyID]         BIGINT           NULL,
    [BuyerType]                      NVARCHAR (50)    NULL,
    [BuyerTypeID]                    INT              NULL,
    [TransactionType]                NVARCHAR (100)   NULL,
    [PropertyType]                   NVARCHAR (100)   NULL,
    [TransLists]                     NVARCHAR (100)   NULL,
    [ExecutedEngagementLetterOnFile] BIT              NULL,
    [MktgListSize]                   INT              NULL,
    [ActiveApproved]                 INT              NULL,
    [IMSent]                         INT              NULL,
    [CASent]                         INT              NULL,
    [CALogged]                       INT              NULL,
    [CAUnapproved]                   INT              NULL,
    [OMSent]                         INT              NULL,
    [OMDigital]                      INT              NULL,
    [DIP]                            INT              NULL,
    [AddlInfo]                       INT              NULL,
    [DDSent]                         INT              NULL,
    [Tour]                           INT              NULL,
    [JurisdictionID]                 INT              NULL,
    [CurrencyUsed]                   NVARCHAR (50)    CONSTRAINT [DF_Deals_CurrencyUsed] DEFAULT (N'USD') NULL,
    [eTeaserText]                    NVARCHAR (4000)  NULL,
    [CompetedForDeal]                BIT              NOT NULL,
    [CompeteForDeal]                 NVARCHAR (50)    NULL,
    [RetentionAgreementStatusID]     INT              NULL,
    [DateAdded]                      DATETIME         NULL,
    [LastUpdate]                     DATETIME         CONSTRAINT [DF__DealProfi__LastU__498EEC8D] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]            UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_7EE67084_0E64_44F5_95EF_7F10F2FF8129_2030630277] DEFAULT (newid()) NOT NULL,
    [RowVersion]                     ROWVERSION       NOT NULL,
    [WarroomCost]                    MONEY            NULL,
    [ClientStrategyNote]             NVARCHAR (2000)  NULL,
    [PipelineReportNote]             NVARCHAR (2000)  NULL,
    [AddedBy]                        NVARCHAR (100)   NULL,
    [UpdatedBy]                      NVARCHAR (100)   NULL,
    [ESIEligibility]                 INT              NULL,
    [ESIPhoto]                       NVARCHAR (250)   NULL,
    CONSTRAINT [PK_DealProfiles] PRIMARY KEY CLUSTERED ([DealID] ASC),
    CONSTRAINT [FK_DealProfiles_Companies] FOREIGN KEY ([ClientCompanyID]) REFERENCES [dbo].[Companies] ([CompanyID]),
    CONSTRAINT [FK_DealProfiles_DealStatuses] FOREIGN KEY ([DealStatusID]) REFERENCES [dbo].[DealStatuses] ([DealStatusID]),
    CONSTRAINT [FK_DealProfiles_IBEquityDebtRoles] FOREIGN KEY ([IBEquityDebtRoleID]) REFERENCES [dbo].[IBEquityDebtRoles] ([IBEquityDebtRoleID]),
    CONSTRAINT [FK_DealProfiles_IBProductNames] FOREIGN KEY ([IBProductNameID]) REFERENCES [dbo].[IBProductNames] ([IBProductNameID]),
    CONSTRAINT [FK_DealProfiles_PropertyTypes] FOREIGN KEY ([PropertyTypeID]) REFERENCES [dbo].[PropertyTypes] ([PropertyTypeID]),
    CONSTRAINT [FK_DealProfiles_USMarketRegions] FOREIGN KEY ([USMarketRegionID]) REFERENCES [dbo].[USMarketRegions] ([USMarketRegionID]),
    CONSTRAINT [FK_Deals_ESIEligibility] FOREIGN KEY ([ESIEligibility]) REFERENCES [dbo].[ESIEligibility] ([ESIEligibilityID]),
    CONSTRAINT [FK_Deals_RegionalMarkets] FOREIGN KEY ([RegionalMarketID]) REFERENCES [dbo].[RegionalMarkets] ([RegionalMarketID]),
    CONSTRAINT [FK_Deals_RegionalSubmarkets] FOREIGN KEY ([RegionalSubmarketID]) REFERENCES [dbo].[RegionalSubmarkets] ([RegionalSubmarketID]),
    CONSTRAINT [FK_Deals_RetentionAgreementStatuses] FOREIGN KEY ([RetentionAgreementStatusID]) REFERENCES [dbo].[RetentionAgreementStatuses] ([RetentionAgreementStatusID]),
    CONSTRAINT [FK_Deals_TransactionTypes] FOREIGN KEY ([TransactionTypeID]) REFERENCES [dbo].[TransactionTypes] ([TransactionTypeID])
);








GO
CREATE NONCLUSTERED INDEX [ESTeam]
    ON [dbo].[Deals]([ESTeam] ASC);


GO
CREATE NONCLUSTERED INDEX [Description]
    ON [dbo].[Deals]([Description] ASC);


GO
CREATE NONCLUSTERED INDEX [DealCode]
    ON [dbo].[Deals]([DealCode] ASC, [DealName] ASC);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 3071.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'YearBuilt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'WarroomCost';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'VisibleOnRpt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 12287.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'USMarketRegionID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'UsePEMarketingList';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=85 @ 144.55:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'TransLists';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=18 @ 682.61:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'TransactionTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=23 @ 534.22:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'TransactionType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'Tour';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2226 @ 5.52:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'StatusComments';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'SourcedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'SellingBrokerCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 2457.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'RetentionAgreementStatusID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=13 @ 945.15:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'RegionalSubmarketID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12 @ 1023.92:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'RegionalMarketID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=49 @ 250.76:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'PropertyTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=37 @ 332.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'PropertyType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10338 @ 1.19:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'PreCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'PortfolioBitField';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'Portfolio';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=28 @ 438.82:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'PitchTeamFilePath';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'PipelineReportNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'OMSent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'OMDigital';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'OffshoreBidder';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'OfficeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=4383 @ 2.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1723 @ 7.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'MktgListSize';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'MarketedOffshore';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=135 @ 91.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'LocationState';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 2457.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'LocationCounty';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1263 @ 9.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'LocationCity';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10 @ 1228.70:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'LocationAddress';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=8374 @ 1.47:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'JurisdictionID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'IMSent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=14 @ 877.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'IBProductNameID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 3071.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'IBEquityDebtRoleID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'FinancingOppLevelID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'FinancingOppLevel';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10 @ 1228.70:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'FinancingOpp';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ExecutedEngagementLetterOnFile';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ExcludeFromPipelineReport';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'eTeaserText';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7460 @ 1.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ESTeam';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ESIPhoto';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ESIEligibility';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3413 @ 3.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ESGSDealFolderPath';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DIP';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4063 @ 3.02:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'Description';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=154 @ 79.79:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DebtSource';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=8 @ 1535.88:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DebtManager';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DebtComp';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=14 @ 877.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealYear';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealTeamAccessOnly';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=14 @ 877.64:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealStatusID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=13 @ 945.15:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealStatus';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=4 @ 3071.75:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealPhotoFilePath';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=55 @ 223.40:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealOffice';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=10842 @ 1.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=60 @ 204.78:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealManager';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=350 @ 35.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealLeader';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5279 @ 2.33:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealFolderPath';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=12190 @ 1.01:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3718 @ 3.30:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DealCallNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DDSent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1123 @ 10.94:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DateClosed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 4095.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'CurrencyUsed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'Confidential';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3 @ 4095.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'CompeteForDeal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=1 @ 12287.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'CompetedForDeal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 1755.29:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ClosingSummaryFilePath';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'Closed';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ClientStrategyNote';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=3732 @ 3.29:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ClientCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'CAUnapproved';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'CASent';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=206 @ 59.65:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'CALogged';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'BuyerTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=37 @ 332.08:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'BuyerType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=46 @ 267.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'Buildings';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=35 @ 351.06:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ApproximateDealTiming';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'AddlInfo';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'AddedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1617 @ 7.60:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'ActiveApproved';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=2 @ 6143.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'Active';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=783 @ 15.69:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Deals', @level2type = N'COLUMN', @level2name = N'AcquiredBy';




﻿CREATE TABLE [dbo].[DealDates] (
    [DealDateID]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [EmployeeID]        BIGINT         NULL,
    [ApptSubject]       NVARCHAR (250) NULL,
    [DateFieldName]     NVARCHAR (50)  NULL,
    [TransactionTypeID] INT            NULL,
    [DealOffice]        NVARCHAR (255) NULL,
    [StartDate]         DATETIME       NULL,
    [EndDate]           DATETIME       NULL,
    [DateAdded]         DATETIME       NULL,
    [LastUpdate]        DATETIME       NULL,
    [AddedBy]           NVARCHAR (100) NULL,
    [UpdatedBy]         NVARCHAR (100) NULL,
    CONSTRAINT [PK_DealDates] PRIMARY KEY CLUSTERED ([DealDateID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=Left over from previous version (Secured Capital)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealDates';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DealDates';


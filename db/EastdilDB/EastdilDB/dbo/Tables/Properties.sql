﻿CREATE TABLE [dbo].[Properties] (
    [PropertyID]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [rcaProperty_id]       BIGINT           NULL,
    [PropertyName]         NVARCHAR (300)   NULL,
    [PropertyDescription]  NVARCHAR (500)   NULL,
    [PropertyTypeID]       INT              NULL,
    [AssetClassID]         INT              NULL,
    [LocationQuality]      NVARCHAR (50)    NULL,
    [USMarketRegionID]     INT              NULL,
    [Address1]             NVARCHAR (100)   NULL,
    [Address2]             NVARCHAR (100)   NULL,
    [Address3]             NVARCHAR (100)   NULL,
    [City]                 NVARCHAR (50)    NULL,
    [State]                NVARCHAR (50)    NULL,
    [PostalCode]           NVARCHAR (50)    NULL,
    [Country]              NVARCHAR (100)   NULL,
    [PropertyType]         NVARCHAR (50)    NULL,
    [PropertySize]         INT              NULL,
    [ProjectSizeTypeID]    INT              NULL,
    [LastSoldDate]         DATETIME         NULL,
    [Buyer]                NVARCHAR (128)   NULL,
    [BuyerDBA]             NVARCHAR (50)    NULL,
    [BuyerCompanyID]       BIGINT           NULL,
    [DateAcquired]         DATETIME         NULL,
    [Seller]               NVARCHAR (128)   NULL,
    [SellerDBA]            NVARCHAR (50)    NULL,
    [SellerCompanyID]      BIGINT           NULL,
    [DateSold]             DATETIME         NULL,
    [LenderCompanyID]      BIGINT           NULL,
    [LenderDBA]            NVARCHAR (50)    NULL,
    [MostRecentValue]      MONEY            NULL,
    [ValuePerPropertySize] MONEY            NULL,
    [NOI]                  MONEY            NULL,
    [LoanBalance]          MONEY            NULL,
    [LoanMaturityDate]     DATETIME         NULL,
    [LoanInterestRate]     FLOAT (53)       NULL,
    [SiteAcreage]          INT              NULL,
    [NumberOfFloors]       INT              NULL,
    [ParkingSpacesTotal]   INT              NULL,
    [ParkingRatio]         NVARCHAR (100)   NULL,
    [YearBuilt]            INT              NULL,
    [YearRenovated]        INT              NULL,
    [IsPropertyAPortfolio] BIT              CONSTRAINT [DF_Properties_IsPropertyAPortfolio] DEFAULT ((0)) NULL,
    [Latitude]             FLOAT (53)       NULL,
    [Longitude]            FLOAT (53)       NULL,
    [BoundaryBaseHeight]   FLOAT (53)       NULL,
    [BoundaryMaxHeight]    FLOAT (53)       NULL,
    [Boundary1]            FLOAT (53)       NULL,
    [Boundary2]            FLOAT (53)       NULL,
    [Boundary3]            FLOAT (53)       NULL,
    [Boundary4]            FLOAT (53)       NULL,
    [DateAdded]            DATETIME         NULL,
    [LastUpdate]           DATETIME         CONSTRAINT [DF__Propertie__LastU__690797E6] DEFAULT (getdate()) NULL,
    [msrepl_tran_version]  UNIQUEIDENTIFIER CONSTRAINT [MSrepl_tran_version_default_BA73BD0D_EE23_4434_B153_FFD41153F259_1333579789] DEFAULT (newid()) NOT NULL,
    [RowVersion]           ROWVERSION       NOT NULL,
    [rcaPortfolio]         NVARCHAR (50)    NULL,
    [rcaRegion]            NVARCHAR (50)    NULL,
    [rcaSF]                INT              NULL,
    [rcaNumberUnits]       INT              NULL,
    [SourceOfPropertyData] NVARCHAR (50)    NULL,
    [DealIDFromInsert]     BIGINT           NULL,
    [AddedBy]              NVARCHAR (100)   NULL,
    [UpdatedBy]            NVARCHAR (100)   NULL,
    CONSTRAINT [PK_Properties] PRIMARY KEY CLUSTERED ([PropertyID] ASC),
    CONSTRAINT [FK_Properties_AssetClasses] FOREIGN KEY ([AssetClassID]) REFERENCES [dbo].[AssetClasses] ([AssetClassID]),
    CONSTRAINT [FK_Properties_ProjectSizeTypes] FOREIGN KEY ([ProjectSizeTypeID]) REFERENCES [dbo].[ProjectSizeTypes] ([ProjectSizeTypeID]),
    CONSTRAINT [FK_Properties_PropertyTypes] FOREIGN KEY ([PropertyTypeID]) REFERENCES [dbo].[PropertyTypes] ([PropertyTypeID]),
    CONSTRAINT [FK_Properties_USMarketRegions] FOREIGN KEY ([USMarketRegionID]) REFERENCES [dbo].[USMarketRegions] ([USMarketRegionID])
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Entity,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'YearRenovated';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'YearBuilt';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'ValuePerPropertySize';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 2251.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'USMarketRegionID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'UpdatedBy';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=77 @ 87.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'State';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'SourceOfPropertyData';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'SiteAcreage';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=231 @ 29.24:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'SellerDBA';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=294 @ 22.98:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'SellerCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=255 @ 26.49:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Seller';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'RowVersion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'rcaSF';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'rcaRegion';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'rcaProperty_id';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'rcaPortfolio';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'rcaNumberUnits';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=48 @ 140.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'PropertyTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=37 @ 182.57:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'PropertyType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1753 @ 3.85:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'PropertySize';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=6109 @ 1.11:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'PropertyName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=All (Key?)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'PropertyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1636 @ 4.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'PropertyDescription';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=5 @ 1351.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'ProjectSizeTypeID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 2251.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'PostalCode';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'ParkingSpacesTotal';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'ParkingRatio';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'NumberOfFloors';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'NOI';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=6749 @ 1.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'msrepl_tran_version';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'MostRecentValue';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Longitude';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'LocationQuality';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'LoanMaturityDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'LoanInterestRate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'LoanBalance';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=77 @ 87.73:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'LenderDBA';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=All distinct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'LenderCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Latitude';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=25 @ 270.20:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'LastUpdate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=224 @ 30.16:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'LastSoldDate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 6755.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'IsPropertyAPortfolio';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'DealIDFromInsert';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=229 @ 29.50:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'DateSold';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=7 @ 965.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'DateAdded';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=224 @ 30.16:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'DateAcquired';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=6 @ 1125.83:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Country';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1126 @ 6.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'City';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=243 @ 27.80:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'BuyerDBA';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=292 @ 23.13:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'BuyerCompanyID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=263 @ 25.68:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Buyer';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'BoundaryMaxHeight';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'BoundaryBaseHeight';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Boundary4';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Boundary3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Boundary2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Boundary1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=3 @ 2251.67:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'AssetClassID';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 6755.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Address3';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1 @ 6755.00:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Address2';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=1603 @ 4.21:1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'Address1';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Non-Null=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Properties', @level2type = N'COLUMN', @level2name = N'AddedBy';




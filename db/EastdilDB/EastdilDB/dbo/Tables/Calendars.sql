﻿CREATE TABLE [dbo].[Calendars] (
    [CalendarID]                          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Title]                               NVARCHAR (100) NULL,
    [Subtitle]                            NVARCHAR (100) NULL,
    [CreatedByOfficeID]                   INT            NULL,
    [Include1AtlantaOffice]               BIT            NULL,
    [Include2BostonOffice]                BIT            NULL,
    [Include3ChicagoOffice]               BIT            NULL,
    [Include4DallasOffice]                BIT            NULL,
    [Include5IrvineOffice]                BIT            NULL,
    [Include6LosAngelesOffice]            BIT            NULL,
    [Include7NewYorkOffice]               BIT            NULL,
    [Include8SanDiegoOffice]              BIT            NULL,
    [Include9SanFranciscoOffice]          BIT            NULL,
    [Include10SanJoseOffice]              BIT            NULL,
    [Include11DCOffice]                   BIT            NULL,
    [Include12LondonOffice]               BIT            NULL,
    [Include2AdvisoryDeals]               BIT            NULL,
    [Include3AgencyDebtPlacementDeals]    BIT            NULL,
    [Include5CommonEquityDeals]           BIT            NULL,
    [Include8DerivativesFixedIncomeDeals] BIT            NULL,
    [Include10EquityRaiseDeals]           BIT            NULL,
    [Include11EquitySaleDeals]            BIT            NULL,
    [Include12FinancingDeals]             BIT            NULL,
    [Include13ForeignExchangeDeals]       BIT            NULL,
    [Include14JVRecapDeals]               BIT            NULL,
    [Include15JVRecapWFSDeals]            BIT            NULL,
    [Include17LiabilityManagementDeals]   BIT            NULL,
    [Include19LoanSaleDeals]              BIT            NULL,
    [Include21PrivatePlacementDeals]      BIT            NULL,
    [Include23SaleLeasebackDeals]         BIT            NULL,
    [Include24SecuredDebtPlacementDeals]  BIT            NULL,
    [Include25InvestmentBankingDeals]     BIT            NULL,
    [CreatedByEmployeeID]                 BIGINT         NULL,
    [DateAdded]                           DATETIME       NULL,
    [LastUpdate]                          DATETIME       NULL,
    [AddedBy]                             NVARCHAR (100) NULL,
    [UpdatedBy]                           NVARCHAR (100) NULL,
    CONSTRAINT [PK_Calendars] PRIMARY KEY CLUSTERED ([CalendarID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=no, Type=,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Calendars';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'no', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Calendars';


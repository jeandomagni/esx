﻿CREATE TABLE [dbo].[ConcurImported] (
    [APPROVED AMT]            MONEY          NULL,
    [EMPLOYEE NAME]           NVARCHAR (100) NULL,
    [ENTRY NO]                NVARCHAR (100) NULL,
    [ENTRY TYPE]              NVARCHAR (100) NULL,
    [GL EFFECTIVE DATE]       DATETIME       NULL,
    [IBCM EXPENSE CODE]       NVARCHAR (50)  NULL,
    [LOCATION CITY]           NVARCHAR (100) NULL,
    [LOCATION COUNTRY]        NVARCHAR (100) NULL,
    [LOCATION STATE/PROVINCE] NVARCHAR (100) NULL,
    [PAYROLL ENTITY NAME]     NVARCHAR (100) NULL,
    [POSTED AMOUNT]           MONEY          NULL,
    [POSTED GL ACCOUNT NBR]   NVARCHAR (100) NULL,
    [RPT NO]                  NVARCHAR (100) NULL,
    [TRANS AMT]               MONEY          NULL,
    [TRANS DATE]              DATETIME       NULL,
    [VENDOR NM]               NVARCHAR (100) NULL
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'IsActive=yes, Type=Utility,Notes=', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported';


GO
EXECUTE sp_addextendedproperty @name = N'IsActive', @value = 'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'VENDOR NM';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'TRANS DATE';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'TRANS AMT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'RPT NO';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'POSTED GL ACCOUNT NBR';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'POSTED AMOUNT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'PAYROLL ENTITY NAME';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'LOCATION STATE/PROVINCE';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'LOCATION COUNTRY';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'LOCATION CITY';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'IBCM EXPENSE CODE';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'GL EFFECTIVE DATE';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'ENTRY TYPE';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'ENTRY NO';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'EMPLOYEE NAME';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Distinct=--Unused--', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ConcurImported', @level2type = N'COLUMN', @level2name = N'APPROVED AMT';




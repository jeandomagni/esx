﻿CREATE FULLTEXT INDEX ON [dbo].[DealContacts]
    KEY INDEX [PK_DealsContacts]
    ON ([ESNetDB], FILEGROUP [ftfg_ESNetDB]);


GO
ALTER FULLTEXT INDEX ON [dbo].[DealContacts] DISABLE;


GO
CREATE FULLTEXT INDEX ON [dbo].[Contacts]
    ([FullName] LANGUAGE 1033, [FirstName] LANGUAGE 1033, [MiddleName] LANGUAGE 1033, [LastName] LANGUAGE 1033, [Nickname] LANGUAGE 1033, [FirstnameLastname] LANGUAGE 1033, [LastnameFirstname] LANGUAGE 1033, [MainPhoneExt] LANGUAGE 1033, [HomePhone] LANGUAGE 1033, [MobilePhone] LANGUAGE 1033, [EmailAddress] LANGUAGE 1033, [Notes] LANGUAGE 1033, [InvestorType] LANGUAGE 1033)
    KEY INDEX [PK_Contacts]
    ON ([ESNetDB], FILEGROUP [ftfg_ESNetDB]);


﻿using System.Data;
using System.Data.SqlClient;
using Esx.Contracts.Models.Comment;
using Intertech.Facade.DapperParameters;

namespace Esx.Data.Dapper
{
    public interface IDapperParameterProvider
    {
        IDapperParameters GetDapperParameters();

        CommentDynamicParameter GetCommentDynamicParameter(Comment comment);
    }

    public class DapperParameterProvider : IDapperParameterProvider
    {
        public IDapperParameters GetDapperParameters()
        {
            return new DapperParameters();
        }

        public CommentDynamicParameter GetCommentDynamicParameter(Comment comment)
        {
            return new CommentDynamicParameter(comment);
        }
    }

}

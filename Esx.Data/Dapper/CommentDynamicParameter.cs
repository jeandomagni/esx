﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Esx.Contracts.Models.Comment;
using Esx.Contracts.Security;
using Microsoft.SqlServer.Server;

namespace Esx.Data.Dapper
{

    public class CommentDynamicParameter : SqlMapper.IDynamicParameters
    {
        private readonly Comment _comment;

        public CommentDynamicParameter(Comment comment)
        {
            _comment = comment;
        }

        public void AddParameters(IDbCommand command, SqlMapper.Identity identity)
        {
            var sqlCommand = (SqlCommand)command;
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.AddWithValue("@pAccessToken", _comment.AccessToken);
            sqlCommand.Parameters.AddWithValue("@pPrimaryMentionID", _comment.PrimaryMentionID);

            sqlCommand.Parameters.AddWithValue("@pCommentID", null);
            sqlCommand.Parameters.AddWithValue("@pCommentText", _comment.CommentText);
            sqlCommand.Parameters.AddWithValue("@pCreateReminder", _comment.CreateReminder);            

            if (_comment.Tags != null && _comment.Tags.Count > 0)
            {
                List<SqlDataRecord> tagList = MapData(_comment.Tags);
                if (tagList.Count > 0)
                {
                    sqlCommand.Parameters.Add(AddTableValueParameter("@pTagTable", "xEsxApp.TVP_Reference", tagList));
                }
            }

            if (_comment.Mentions != null && _comment.Mentions.Count > 0)
            {
                List<SqlDataRecord> mentionList = MapData(_comment.Mentions);
                if (mentionList.Count > 0)
                {
                    sqlCommand.Parameters.Add(AddTableValueParameter("@pmentionTable", "xEsxApp.TVP_Reference", mentionList));
                }
            }

            if (_comment.DocumentIds != null && _comment.DocumentIds.Any())
            {
                List<SqlDataRecord> documentList = MapDocuments(_comment.DocumentIds);
                if (documentList.Any())
                {
                    sqlCommand.Parameters.Add(AddTableValueParameter("@pDocumentTable", "xEsxApp.TVP_Document", documentList));
                }
            }
        }

        private SqlParameter AddTableValueParameter(string paramterName, string parameterTypeName, object values)
        {
            SqlParameter param = new SqlParameter();
            param.ParameterName = paramterName;
            param.SqlDbType = SqlDbType.Structured;
            param.Direction = ParameterDirection.Input;
            param.TypeName = parameterTypeName;
            param.Value = values;

            return param;
        }

        private List<SqlDataRecord> MapData(List<string> values)
        {
            List<SqlDataRecord> lstSqlDatarecord = new List<SqlDataRecord>();
            var rec = new SqlDataRecord();

            foreach (string value in values)
            {
                rec = new SqlDataRecord(new[] { new SqlMetaData("ReferenceName", SqlDbType.NVarChar, 128) });
                rec.SetString(0, value);
                lstSqlDatarecord.Add(rec);
            }
            return lstSqlDatarecord;
        }

        private List<SqlDataRecord> MapDocuments(List<int> values)
        {
            List<SqlDataRecord> lstSqlDatarecord = new List<SqlDataRecord>();
            var rec = new SqlDataRecord();

            foreach (int value in values)
            {
                rec = new SqlDataRecord(new[] { new SqlMetaData("DocumentKey", SqlDbType.Int) });
                rec.SetInt32(0, value);
                lstSqlDatarecord.Add(rec);
            }
            return lstSqlDatarecord;
        }
    }
}

﻿namespace Esx.Data
{
    public partial class FakeDbSet<TEntity>
    {
        /// <summary>
        /// This gets around the issue of unit testing that calls the AddOrUpdate extension method.
        /// </summary>
        /// <param name="entities"></param>
        public void AddOrUpdate(params TEntity[] entities) { }
    }
}

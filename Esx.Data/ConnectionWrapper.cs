﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;

namespace Esx.Data
{
    public interface IConnectionWrapper
    {
        IEnumerable<T> Query<T>(string sql, DynamicParameters p, CommandType type);
        ResultWithCount<T> QueryWithCount<T>(string sql, DynamicParameters p, CommandType type);
        int Execute(string sql, object p, CommandType type);
    }

    public class ConnectionWrapper : IConnectionWrapper, IDisposable
    {
        private readonly IDbConnection _conn;

        public ConnectionWrapper(IDbConnection dbConnection)
        {
            _conn = dbConnection;
        }

        public void Dispose()
        {
            _conn.Dispose();
        }

        public IEnumerable<T> Query<T>(string sql, DynamicParameters p, CommandType type)
        {
            return RunDbMethod(() => _conn.Query<T>(sql, p, commandType: type));
        }

        public ResultWithCount<T> QueryWithCount<T>(string sql, DynamicParameters p, CommandType type)
        {
            return RunDbMethod(() =>
            {
                var multi = _conn.QueryMultiple(sql, p, commandType: type);
                var totalRecords = multi.Read<int>().Single(); // The total count is always in the first resultset.
                var result = multi.Read<T>(); // The records are always in the second resultset.
                return new ResultWithCount<T>
                {
                    TotalRecords = totalRecords,
                    Result = result
                };
            });
        }

        public int Execute(string sql, object p, CommandType type)
        {
            return RunDbMethod(() => _conn.Execute(sql, p, commandType: type));
        }

        private T RunDbMethod<T>(Func<T> method)
        {
            T value;
            try
            {
                value = method();
            }
            finally
            {
                _conn.Close();
            }
            return value;
        }

    }
}

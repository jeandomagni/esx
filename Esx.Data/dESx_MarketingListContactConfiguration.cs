// <auto-generated>
// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier
// TargetFrameworkVersion = 4.52
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using Esx.Contracts.Models;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace Esx.Data
{
    // MarketingListContact
    public partial class dESx_MarketingListContactConfiguration : EntityTypeConfiguration<dESx_MarketingListContact>
    {
        public dESx_MarketingListContactConfiguration()
            : this("dESx")
        {
        }
 
        public dESx_MarketingListContactConfiguration(string schema)
        {
            ToTable(schema + ".MarketingListContact");
            HasKey(x => x.MarketingListContactKey);

            Property(x => x.MarketingListContactKey).HasColumnName("MarketingListContact_Key").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.MarketingListKey).HasColumnName("MarketingList_Key").IsRequired().HasColumnType("int");
            Property(x => x.ContactKey).HasColumnName("Contact_Key").IsRequired().HasColumnType("int");
            Property(x => x.IsPrimaryContact).HasColumnName("IsPrimaryContact").IsRequired().HasColumnType("bit");
            Property(x => x.IsSolicitable).HasColumnName("IsSolicitable").IsRequired().HasColumnType("bit");
            Property(x => x.IsRequestingTeaser).HasColumnName("IsRequestingTeaser").IsRequired().HasColumnType("bit");
            Property(x => x.IsRequestingWarRoomAccess).HasColumnName("IsRequestingWarRoomAccess").IsRequired().HasColumnType("bit");
            Property(x => x.IsRequestingAdditionalMaterials).HasColumnName("IsRequestingAdditionalMaterials").IsRequired().HasColumnType("bit");
            Property(x => x.IsRequestingPrintedOm).HasColumnName("IsRequestingPrintedOm").IsRequired().HasColumnType("bit");
            Property(x => x.IsRequestingCallForOffers).HasColumnName("IsRequestingCallForOffers").IsRequired().HasColumnType("bit");
            Property(x => x.CreatedDate).HasColumnName("CreatedDate").IsRequired().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(20);
            Property(x => x.UpdatedDate).HasColumnName("UpdatedDate").IsRequired().HasColumnType("datetime");
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy").IsOptional().IsUnicode(false).HasColumnType("varchar").HasMaxLength(20);

            // Foreign keys
            HasRequired(a => a.dESx_Contact).WithMany(b => b.dESx_MarketingListContacts).HasForeignKey(c => c.ContactKey); // FK_MarketingListContact_Contact
            HasRequired(a => a.dESx_MarketingList).WithMany(b => b.dESx_MarketingListContacts).HasForeignKey(c => c.MarketingListKey); // FK_MarketingListContact_MarketingList
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>

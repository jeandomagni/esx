// <auto-generated>
// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier
// TargetFrameworkVersion = 4.52
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using Esx.Contracts.Models;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace Esx.Data
{
    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "2.17.1.0")]
    public partial class FakeEsxDbContext : IEsxDbContext
    {
        public DbSet<dESx_ApplicationLog> dESx_ApplicationLogs { get; set; }
        public DbSet<dESx_Asset> dESx_Assets { get; set; }
        public DbSet<dESx_AssetContact> dESx_AssetContacts { get; set; }
        public DbSet<dESx_AssetContactType> dESx_AssetContactTypes { get; set; }
        public DbSet<dESx_AssetLoanBorrower> dESx_AssetLoanBorrowers { get; set; }
        public DbSet<dESx_AssetLoanProperty> dESx_AssetLoanProperties { get; set; }
        public DbSet<dESx_AssetLoanTerm> dESx_AssetLoanTerms { get; set; }
        public DbSet<dESx_AssetLoanTermDate> dESx_AssetLoanTermDates { get; set; }
        public DbSet<dESx_AssetName> dESx_AssetNames { get; set; }
        public DbSet<dESx_AssetType> dESx_AssetTypes { get; set; }
        public DbSet<dESx_AssetUsage> dESx_AssetUsages { get; set; }
        public DbSet<dESx_AssetUsageType> dESx_AssetUsageTypes { get; set; }
        public DbSet<dESx_AssetValue> dESx_AssetValues { get; set; }
        public DbSet<dESx_BidStatu> dESx_BidStatus { get; set; }
        public DbSet<dESx_CacheAccessToken> dESx_CacheAccessTokens { get; set; }
        public DbSet<dESx_CacheCompanyBrief> dESx_CacheCompanyBriefs { get; set; }
        public DbSet<dESx_CacheSessionQuery> dESx_CacheSessionQueries { get; set; }
        public DbSet<dESx_Comment> dESx_Comments { get; set; }
        public DbSet<dESx_CommentAttachment> dESx_CommentAttachments { get; set; }
        public DbSet<dESx_CommentMention> dESx_CommentMentions { get; set; }
        public DbSet<dESx_CommentTag> dESx_CommentTags { get; set; }
        public DbSet<dESx_Company> dESx_Companies { get; set; }
        public DbSet<dESx_CompanyAlias> dESx_CompanyAlias { get; set; }
        public DbSet<dESx_CompanyAsset> dESx_CompanyAssets { get; set; }
        public DbSet<dESx_CompanyDealTypeInterest> dESx_CompanyDealTypeInterests { get; set; }
        public DbSet<dESx_CompanyElectronicAddress> dESx_CompanyElectronicAddresses { get; set; }
        public DbSet<dESx_CompanyHierarchy> dESx_CompanyHierarchies { get; set; }
        public DbSet<dESx_CompanyMarket> dESx_CompanyMarkets { get; set; }
        public DbSet<dESx_Contact> dESx_Contacts { get; set; }
        public DbSet<dESx_ContactElectronicAddress> dESx_ContactElectronicAddresses { get; set; }
        public DbSet<dESx_ContactMarket> dESx_ContactMarkets { get; set; }
        public DbSet<dESx_ContactOffice> dESx_ContactOffices { get; set; }
        public DbSet<dESx_Country> dESx_Countries { get; set; }
        public DbSet<dESx_Deal> dESx_Deals { get; set; }
        public DbSet<dESx_DealAsset> dESx_DealAssets { get; set; }
        public DbSet<dESx_DealCompany> dESx_DealCompanies { get; set; }
        public DbSet<dESx_DealContact> dESx_DealContacts { get; set; }
        public DbSet<dESx_DealContactStatus> dESx_DealContactStatus { get; set; }
        public DbSet<dESx_DealContactType> dESx_DealContactTypes { get; set; }
        public DbSet<dESx_DealParticipationType> dESx_DealParticipationTypes { get; set; }
        public DbSet<dESx_DealStatus> dESx_DealStatus { get; set; }
        public DbSet<dESx_DealStatusLog> dESx_DealStatusLogs { get; set; }
        public DbSet<dESx_DealTeam> dESx_DealTeams { get; set; }
        public DbSet<dESx_DealTeamRole> dESx_DealTeamRoles { get; set; }
        public DbSet<dESx_DealType> dESx_DealTypes { get; set; }
        public DbSet<dESx_DealValuation> dESx_DealValuations { get; set; }
        public DbSet<dESx_DealValuationAsset> dESx_DealValuationAssets { get; set; }
        public DbSet<dESx_DealValuationAssetUsageValue> dESx_DealValuationAssetUsageValues { get; set; }
        public DbSet<dESx_DealValuationCompany> dESx_DealValuationCompanies { get; set; }
        public DbSet<dESx_DealValuationDocument> dESx_DealValuationDocuments { get; set; }
        public DbSet<dESx_DealValuationRiskProfile> dESx_DealValuationRiskProfiles { get; set; }
        public DbSet<dESx_DealValuationType> dESx_DealValuationTypes { get; set; }
        public DbSet<dESx_DealValue> dESx_DealValues { get; set; }
        public DbSet<dESx_Document> dESx_Documents { get; set; }
        public DbSet<dESx_EastdilOffice> dESx_EastdilOffices { get; set; }
        public DbSet<dESx_EastdilOfficeMentionUsage> dESx_EastdilOfficeMentionUsages { get; set; }
        public DbSet<dESx_ElectronicAddress> dESx_ElectronicAddresses { get; set; }
        public DbSet<dESx_ElectronicAddressUsageType> dESx_ElectronicAddressUsageTypes { get; set; }
        public DbSet<dESx_Feature> dESx_Features { get; set; }
        public DbSet<dESx_FeatureRole> dESx_FeatureRoles { get; set; }
        public DbSet<dESx_LegalEntity> dESx_LegalEntities { get; set; }
        public DbSet<dESx_LoanBenchmarkType> dESx_LoanBenchmarkTypes { get; set; }
        public DbSet<dESx_LoanCashMgmtType> dESx_LoanCashMgmtTypes { get; set; }
        public DbSet<dESx_LoanLenderType> dESx_LoanLenderTypes { get; set; }
        public DbSet<dESx_LoanPaymentType> dESx_LoanPaymentTypes { get; set; }
        public DbSet<dESx_LoanRateType> dESx_LoanRateTypes { get; set; }
        public DbSet<dESx_LoanSourceType> dESx_LoanSourceTypes { get; set; }
        public DbSet<dESx_LoanTermDateType> dESx_LoanTermDateTypes { get; set; }
        public DbSet<dESx_LogSeverityType> dESx_LogSeverityTypes { get; set; }
        public DbSet<dESx_Market> dESx_Markets { get; set; }
        public DbSet<dESx_MarketHierarchy> dESx_MarketHierarchies { get; set; }
        public DbSet<dESx_MarketingList> dESx_MarketingLists { get; set; }
        public DbSet<dESx_MarketingListActionType> dESx_MarketingListActionTypes { get; set; }
        public DbSet<dESx_MarketingListCompany> dESx_MarketingListCompanies { get; set; }
        public DbSet<dESx_MarketingListCompanyDocument> dESx_MarketingListCompanyDocuments { get; set; }
        public DbSet<dESx_MarketingListContact> dESx_MarketingListContacts { get; set; }
        public DbSet<dESx_MarketingListContactStatusLog> dESx_MarketingListContactStatusLogs { get; set; }
        public DbSet<dESx_MarketingListStatusType> dESx_MarketingListStatusTypes { get; set; }
        public DbSet<dESx_MarketType> dESx_MarketTypes { get; set; }
        public DbSet<dESx_Mention> dESx_Mentions { get; set; }
        public DbSet<dESx_Notice> dESx_Notices { get; set; }
        public DbSet<dESx_NoticeMention> dESx_NoticeMentions { get; set; }
        public DbSet<dESx_NoticeRecipient> dESx_NoticeRecipients { get; set; }
        public DbSet<dESx_NoticeSeverityType> dESx_NoticeSeverityTypes { get; set; }
        public DbSet<dESx_Office> dESx_Offices { get; set; }
        public DbSet<dESx_OfficeElectronicAddress> dESx_OfficeElectronicAddresses { get; set; }
        public DbSet<dESx_OfficeMarket> dESx_OfficeMarkets { get; set; }
        public DbSet<dESx_Park> dESx_Parks { get; set; }
        public DbSet<dESx_PhysicalAddress> dESx_PhysicalAddresses { get; set; }
        public DbSet<dESx_PhysicalAddressType> dESx_PhysicalAddressTypes { get; set; }
        public DbSet<dESx_PreferenceType> dESx_PreferenceTypes { get; set; }
        public DbSet<dESx_Reminder> dESx_Reminders { get; set; }
        public DbSet<dESx_ReminderAssignee> dESx_ReminderAssignees { get; set; }
        public DbSet<dESx_Role> dESx_Roles { get; set; }
        public DbSet<dESx_StateProvidence> dESx_StateProvidences { get; set; }
        public DbSet<dESx_Tag> dESx_Tags { get; set; }
        public DbSet<dESx_TagUsage> dESx_TagUsages { get; set; }
        public DbSet<dESx_User> dESx_Users { get; set; }
        public DbSet<dESx_UserDelegate> dESx_UserDelegates { get; set; }
        public DbSet<dESx_UserMentionData> dESx_UserMentionDatas { get; set; }
        public DbSet<dESx_UserPreference> dESx_UserPreferences { get; set; }

        public FakeEsxDbContext()
        {
            dESx_ApplicationLogs = new FakeDbSet<dESx_ApplicationLog>("ApplicationLogKey");
            dESx_Assets = new FakeDbSet<dESx_Asset>("AssetKey");
            dESx_AssetContacts = new FakeDbSet<dESx_AssetContact>("AssetContactKey");
            dESx_AssetContactTypes = new FakeDbSet<dESx_AssetContactType>("AssetContactTypeCode");
            dESx_AssetLoanBorrowers = new FakeDbSet<dESx_AssetLoanBorrower>("LoanBorrowerKey");
            dESx_AssetLoanProperties = new FakeDbSet<dESx_AssetLoanProperty>("AssetLoanPropertyKey");
            dESx_AssetLoanTerms = new FakeDbSet<dESx_AssetLoanTerm>("AssetKey");
            dESx_AssetLoanTermDates = new FakeDbSet<dESx_AssetLoanTermDate>("AssetLoanTermDatesKey");
            dESx_AssetNames = new FakeDbSet<dESx_AssetName>("AssetNameKey");
            dESx_AssetTypes = new FakeDbSet<dESx_AssetType>("AssetTypeCode");
            dESx_AssetUsages = new FakeDbSet<dESx_AssetUsage>("AssetUsageKey");
            dESx_AssetUsageTypes = new FakeDbSet<dESx_AssetUsageType>("AssetUsageTypeKey");
            dESx_AssetValues = new FakeDbSet<dESx_AssetValue>("AssetValueKey");
            dESx_BidStatus = new FakeDbSet<dESx_BidStatu>("BidStatusCode");
            dESx_CacheAccessTokens = new FakeDbSet<dESx_CacheAccessToken>("CacheAccessTokenKey");
            dESx_CacheCompanyBriefs = new FakeDbSet<dESx_CacheCompanyBrief>("CacheSessionQueryKey", "MatchingAliasKey");
            dESx_CacheSessionQueries = new FakeDbSet<dESx_CacheSessionQuery>("CacheSessionQueryKey");
            dESx_Comments = new FakeDbSet<dESx_Comment>("CommentKey");
            dESx_CommentAttachments = new FakeDbSet<dESx_CommentAttachment>("CommentAttachmentKey");
            dESx_CommentMentions = new FakeDbSet<dESx_CommentMention>("CommentMentionKey");
            dESx_CommentTags = new FakeDbSet<dESx_CommentTag>("CommentTagKey");
            dESx_Companies = new FakeDbSet<dESx_Company>("CompanyKey");
            dESx_CompanyAlias = new FakeDbSet<dESx_CompanyAlias>("CompanyAliasKey");
            dESx_CompanyAssets = new FakeDbSet<dESx_CompanyAsset>("CompanyAssetKey");
            dESx_CompanyDealTypeInterests = new FakeDbSet<dESx_CompanyDealTypeInterest>("CompanyDealTypeInterestKey");
            dESx_CompanyElectronicAddresses = new FakeDbSet<dESx_CompanyElectronicAddress>("CompanyElectronicAddressKey");
            dESx_CompanyHierarchies = new FakeDbSet<dESx_CompanyHierarchy>("CompanyHierarchyKey");
            dESx_CompanyMarkets = new FakeDbSet<dESx_CompanyMarket>("CompanyMarketKey");
            dESx_Contacts = new FakeDbSet<dESx_Contact>("ContactKey");
            dESx_ContactElectronicAddresses = new FakeDbSet<dESx_ContactElectronicAddress>("ContactElectronicAddressKey");
            dESx_ContactMarkets = new FakeDbSet<dESx_ContactMarket>("ContactMarketKey");
            dESx_ContactOffices = new FakeDbSet<dESx_ContactOffice>("ContactOfficeKey");
            dESx_Countries = new FakeDbSet<dESx_Country>("CountryKey");
            dESx_Deals = new FakeDbSet<dESx_Deal>("DealKey");
            dESx_DealAssets = new FakeDbSet<dESx_DealAsset>("DealAssetKey");
            dESx_DealCompanies = new FakeDbSet<dESx_DealCompany>("DealCompanyKey");
            dESx_DealContacts = new FakeDbSet<dESx_DealContact>("DealContactKey");
            dESx_DealContactStatus = new FakeDbSet<dESx_DealContactStatus>("DealContactStatusKey");
            dESx_DealContactTypes = new FakeDbSet<dESx_DealContactType>("DealContactTypeCode");
            dESx_DealParticipationTypes = new FakeDbSet<dESx_DealParticipationType>("DealParticipationTypeCode");
            dESx_DealStatus = new FakeDbSet<dESx_DealStatus>("DealStatusCode");
            dESx_DealStatusLogs = new FakeDbSet<dESx_DealStatusLog>("DealStatusLogKey");
            dESx_DealTeams = new FakeDbSet<dESx_DealTeam>("DealTeamKey");
            dESx_DealTeamRoles = new FakeDbSet<dESx_DealTeamRole>("DealTeamRoleCode");
            dESx_DealTypes = new FakeDbSet<dESx_DealType>("DealTypeCode");
            dESx_DealValuations = new FakeDbSet<dESx_DealValuation>("DealValuationKey");
            dESx_DealValuationAssets = new FakeDbSet<dESx_DealValuationAsset>("DealValuationAssetKey");
            dESx_DealValuationAssetUsageValues = new FakeDbSet<dESx_DealValuationAssetUsageValue>("DealValuationAssetUsageValueKey");
            dESx_DealValuationCompanies = new FakeDbSet<dESx_DealValuationCompany>("DealValuationCompanyKey");
            dESx_DealValuationDocuments = new FakeDbSet<dESx_DealValuationDocument>("DealValuationDocumentKey");
            dESx_DealValuationRiskProfiles = new FakeDbSet<dESx_DealValuationRiskProfile>("DealValuationRiskProfileCode");
            dESx_DealValuationTypes = new FakeDbSet<dESx_DealValuationType>("DealValuationTypeCode");
            dESx_DealValues = new FakeDbSet<dESx_DealValue>("DealKey");
            dESx_Documents = new FakeDbSet<dESx_Document>("DocumentKey");
            dESx_EastdilOffices = new FakeDbSet<dESx_EastdilOffice>("EastdilOfficeKey");
            dESx_EastdilOfficeMentionUsages = new FakeDbSet<dESx_EastdilOfficeMentionUsage>("EastdilOfficeMentionUsageKey");
            dESx_ElectronicAddresses = new FakeDbSet<dESx_ElectronicAddress>("ElectronicAddressKey");
            dESx_ElectronicAddressUsageTypes = new FakeDbSet<dESx_ElectronicAddressUsageType>("ElectronicAddressTypeCode", "AddressUsageTypeCode");
            dESx_Features = new FakeDbSet<dESx_Feature>("FeatureCode");
            dESx_FeatureRoles = new FakeDbSet<dESx_FeatureRole>("FeatureRoleKey");
            dESx_LegalEntities = new FakeDbSet<dESx_LegalEntity>("LegalEntityCode");
            dESx_LoanBenchmarkTypes = new FakeDbSet<dESx_LoanBenchmarkType>("LoanBenchmarkTypeCode");
            dESx_LoanCashMgmtTypes = new FakeDbSet<dESx_LoanCashMgmtType>("LoanCashMgmtTypeCode");
            dESx_LoanLenderTypes = new FakeDbSet<dESx_LoanLenderType>("LoanLenderTypeCode");
            dESx_LoanPaymentTypes = new FakeDbSet<dESx_LoanPaymentType>("LoanPaymentTypeCode");
            dESx_LoanRateTypes = new FakeDbSet<dESx_LoanRateType>("LoanRateTypeCode");
            dESx_LoanSourceTypes = new FakeDbSet<dESx_LoanSourceType>("LoanSourceTypeCode");
            dESx_LoanTermDateTypes = new FakeDbSet<dESx_LoanTermDateType>("LoanTermDateTypeCode");
            dESx_LogSeverityTypes = new FakeDbSet<dESx_LogSeverityType>("LogSeverityTypeKey");
            dESx_Markets = new FakeDbSet<dESx_Market>("MarketKey");
            dESx_MarketHierarchies = new FakeDbSet<dESx_MarketHierarchy>("MarketHierarchyKey");
            dESx_MarketingLists = new FakeDbSet<dESx_MarketingList>("MarketingListKey");
            dESx_MarketingListActionTypes = new FakeDbSet<dESx_MarketingListActionType>("MarketingListActionCode");
            dESx_MarketingListCompanies = new FakeDbSet<dESx_MarketingListCompany>("MarketingListCompanyKey");
            dESx_MarketingListCompanyDocuments = new FakeDbSet<dESx_MarketingListCompanyDocument>("MarketingListCompanyDocumentKey");
            dESx_MarketingListContacts = new FakeDbSet<dESx_MarketingListContact>("MarketingListContactKey");
            dESx_MarketingListContactStatusLogs = new FakeDbSet<dESx_MarketingListContactStatusLog>("MarketingListContactStatusLogKey");
            dESx_MarketingListStatusTypes = new FakeDbSet<dESx_MarketingListStatusType>("MarketingListStatusCode");
            dESx_MarketTypes = new FakeDbSet<dESx_MarketType>("MarketTypeCode");
            dESx_Mentions = new FakeDbSet<dESx_Mention>("MentionKey");
            dESx_Notices = new FakeDbSet<dESx_Notice>("NoticeKey");
            dESx_NoticeMentions = new FakeDbSet<dESx_NoticeMention>("NoticeMentionKey");
            dESx_NoticeRecipients = new FakeDbSet<dESx_NoticeRecipient>("NoticeRecipientKey");
            dESx_NoticeSeverityTypes = new FakeDbSet<dESx_NoticeSeverityType>("NoticeSeverityTypeKey");
            dESx_Offices = new FakeDbSet<dESx_Office>("OfficeKey");
            dESx_OfficeElectronicAddresses = new FakeDbSet<dESx_OfficeElectronicAddress>("OfficeElectronicAddressKey");
            dESx_OfficeMarkets = new FakeDbSet<dESx_OfficeMarket>("OfficeMarketKey");
            dESx_Parks = new FakeDbSet<dESx_Park>("ParkKey");
            dESx_PhysicalAddresses = new FakeDbSet<dESx_PhysicalAddress>("PhysicalAddressKey");
            dESx_PhysicalAddressTypes = new FakeDbSet<dESx_PhysicalAddressType>("PhysicalAddressTypeCode");
            dESx_PreferenceTypes = new FakeDbSet<dESx_PreferenceType>("PreferenceTypeCode");
            dESx_Reminders = new FakeDbSet<dESx_Reminder>("ReminderKey");
            dESx_ReminderAssignees = new FakeDbSet<dESx_ReminderAssignee>("ReminderAssigneeKey");
            dESx_Roles = new FakeDbSet<dESx_Role>("RoleCode");
            dESx_StateProvidences = new FakeDbSet<dESx_StateProvidence>("StateProvidenceCode");
            dESx_Tags = new FakeDbSet<dESx_Tag>("TagKey");
            dESx_TagUsages = new FakeDbSet<dESx_TagUsage>("TagUsageKey");
            dESx_Users = new FakeDbSet<dESx_User>("UserKey");
            dESx_UserDelegates = new FakeDbSet<dESx_UserDelegate>("UserDelegateKey");
            dESx_UserMentionDatas = new FakeDbSet<dESx_UserMentionData>("UserMentionDataKey");
            dESx_UserPreferences = new FakeDbSet<dESx_UserPreference>("UserPreferenceKey");

            InitializePartial();
        }
        
        public int SaveChangesCount { get; private set; } 
        public int SaveChanges()
        {
            ++SaveChangesCount;
            return 1;
        }

        public System.Threading.Tasks.Task<int> SaveChangesAsync()
        {
            ++SaveChangesCount;
            return System.Threading.Tasks.Task<int>.Factory.StartNew(() => 1);
        }

        public System.Threading.Tasks.Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            ++SaveChangesCount;
            return System.Threading.Tasks.Task<int>.Factory.StartNew(() => 1, cancellationToken);
        }

        partial void InitializePartial();

        protected virtual void Dispose(bool disposing)
        {
        }
        
        public void Dispose()
        {
            Dispose(true);
        }
    }
}
// </auto-generated>

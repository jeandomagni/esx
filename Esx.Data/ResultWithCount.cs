﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Data
{
    public class ResultWithCount<T>
    {
        public IEnumerable<T> Result { get; set; } 
        public int TotalRecords { get; set; }
    }
}

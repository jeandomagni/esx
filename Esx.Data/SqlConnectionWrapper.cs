﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Intertech.Facade.DapperParameters;

namespace Esx.Data
{
    public interface ISqlConnectionWrapper
    {
        void Close();
        void Dispose();
        void Open();

        IEnumerable<T> Query<T>(string sql, DynamicParameters p, CommandType type);
        IEnumerable<T> QueryWithCount<T>(string sql, DynamicParameters p, CommandType type, out int totalRecords);
        int Execute(string sql, object p, CommandType type);

    }

    public class SqlConnectionWrapper : ISqlConnectionWrapper
    {
        private SqlConnection _conn;

        public SqlConnectionWrapper(string connectionString)
        {
            _conn = new SqlConnection(connectionString);
        }

        public void Close()
        {
            _conn.Close();
        }

        public void Dispose()
        {
            _conn.Dispose();
        }

        public void Open()
        {
            _conn.Open();
        }

        public IEnumerable<T> Query<T>(string sql, DynamicParameters p, CommandType type)
        {
            return _conn.Query<T>(sql, p, commandType: type);
        }

        public IEnumerable<T> QueryWithCount<T>(string sql, DynamicParameters p, CommandType type, out int totalRecords)
        {
            var multi = _conn.QueryMultiple(sql, p, commandType: type);
            totalRecords = multi.Read<int>().Single();
            return multi.Read<T>();
        }

        public int Execute(string sql, object p, CommandType type)
        {
            return _conn.Execute(sql, p, commandType: type);
        }

    }
}

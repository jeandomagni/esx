// <auto-generated>
// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier
// TargetFrameworkVersion = 4.52
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using Esx.Contracts.Models;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace Esx.Data
{
    // Comment_Tag
    public partial class dESx_CommentTagConfiguration : EntityTypeConfiguration<dESx_CommentTag>
    {
        public dESx_CommentTagConfiguration()
            : this("dESx")
        {
        }
 
        public dESx_CommentTagConfiguration(string schema)
        {
            ToTable(schema + ".Comment_Tag");
            HasKey(x => x.CommentTagKey);

            Property(x => x.CommentTagKey).HasColumnName("Comment_Tag_Key").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CommentKey).HasColumnName("Comment_Key").IsRequired().HasColumnType("int");
            Property(x => x.TagKey).HasColumnName("Tag_Key").IsRequired().HasColumnType("nvarchar").HasMaxLength(128);

            // Foreign keys
            HasRequired(a => a.dESx_Comment).WithMany(b => b.dESx_CommentTags).HasForeignKey(c => c.CommentKey); // FK_Comment_Tag_Comment
            HasRequired(a => a.dESx_Tag).WithMany(b => b.dESx_CommentTags).HasForeignKey(c => c.TagKey); // FK_Comment_Tag_Tag
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>

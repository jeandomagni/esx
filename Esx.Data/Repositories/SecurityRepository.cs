﻿using System.Data;
using Esx.Contracts.Models.User;
using Esx.Data.Dapper;
using Esx.Contracts.Models;
using System;

namespace Esx.Data.Repositories
{

   
    public interface ISecurityRepository
    {
        User Login(string username);
        bool AddNewUser(string userName, string fullName);
    }

    public class SecurityRepository : ISecurityRepository
    {
        private readonly IConnectionWrapper _conn;
        private readonly IDapperParameterProvider _dapperParameterProvider;

        public SecurityRepository(IConnectionWrapper conn, IDapperParameterProvider dynamicParameterProvider)
        {
            _dapperParameterProvider = dynamicParameterProvider;
            _conn = conn;
        }

        public User Login(string username)
        {
            var p = _dapperParameterProvider.GetDapperParameters();
            p.Add("@UserName", username);
            p.Add("@pAccessToken", dbType: DbType.Binary, direction: ParameterDirection.Output, size: 64);
            p.Add("@pMentionID", direction: ParameterDirection.Output, size: 128);
            p.Add("@pUserKey", dbType: DbType.Int32, direction: ParameterDirection.Output);

            _conn.Execute("xEsxApp.Login", p.DynamicParameters, CommandType.StoredProcedure);

            int? userKey = p.Get<int?>("@pUserKey");
            var accessToken = p.Get<byte[]>("@pAccessToken");
            var mentionId = p.Get<string>("@pMentionID");

            if (userKey == null || accessToken == null) return null;
           
            return new User
            {
                Username = username,
                AccessToken = accessToken,
                MentionId = mentionId,
                UserKey = (int)userKey
            };
        }
        public bool  AddNewUser(string userName, string fullName)
        {
            var p = _dapperParameterProvider.GetDapperParameters();
            p.Add("@UserName", userName);
            p.Add("@FullName", fullName);
            p.Add("@pMentionKey", dbType: DbType.Int32, direction: ParameterDirection.Output);

            _conn.Execute("xEsxApp.CreateNewUser", p.DynamicParameters, CommandType.StoredProcedure);

            int? mentionKey = p.Get<int?>("@pMentionKey");

            return mentionKey != null;
        }
        
    }
}

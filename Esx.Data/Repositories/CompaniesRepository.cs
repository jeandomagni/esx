﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Esx.Contracts.Interfaces;
using Esx.Contracts.Models.Company;
using Esx.Contracts.Security;
using Esx.Data.Dapper;

namespace Esx.Data.Repositories
{
    public interface ICompaniesRepository : IBaseRepository
    {
        ResultWithCount<CompanyListItem> GetCompanies(string searchText, int pageSize, int offset, string sort, bool sortDescending);

        CompanyStatistics GetCompanyStatistics(string mentionId);

        CompanySummaryStats GetCompanySummaryStats(string companyId, int pitches, int bidActivity,
            int holdPeriod, int loansMaturing);
        List<CompanyIntellisense> GetCompanyIntellisense(string searchText, int pageSize);

        IEnumerable<Alert> GetAlerts(string companyMentionId);

        void AcknowledgeAlert(int alertId);
        void RemoveAlert(int alertId);
        void SnoozeAlert(int alertId);
    }

    public class CompaniesRepository : RepositoryBase, ICompaniesRepository
    {
        private IEsxDbContext _esxDbContext;

        public CompaniesRepository(IConnectionWrapper connection, ISecurityContext securityContext, IDapperParameterProvider dapperParameterProvider, IEsxDbContext esxDbContext)
            : base(connection, securityContext, dapperParameterProvider)
        {
            _esxDbContext = esxDbContext;
        }

        public ResultWithCount<CompanyListItem> GetCompanies(string searchText, int pageSize, int offset, string sort, bool sortDescending)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("pAccessToken", SecurityContext.DbToken);
            p.Add("pFilterText", searchText);
            if (pageSize != 0) p.Add("pMaxRows", pageSize);
            if (offset != 0) p.Add("pFirstRow", offset);
            if (!string.IsNullOrWhiteSpace(sort)) p.Add("pSortColumn", sort);
            p.Add("pSortDescending", sortDescending);
            return Connection.QueryWithCount<CompanyListItem>("xESxApp.GetCompanyListBriefWCount", p.DynamicParameters, CommandType.StoredProcedure);
        }

        public CompanyStatistics GetCompanyStatistics(string mentionId)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("pAccessToken", SecurityContext.DbToken);
            p.Add("pMentionID", mentionId);
            return Connection.Query<CompanyStatistics>("xESxApp.GetCompanyEntityCount", p.DynamicParameters, CommandType.StoredProcedure).SingleOrDefault();
        }

        public CompanySummaryStats GetCompanySummaryStats(string companyId, int pitches, int bidActivity,
            int holdPeriod, int loansMaturing)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("pAccessToken", SecurityContext.DbToken);
            p.Add("pMentionID", companyId);
            p.Add("pPitchesSinceDays", pitches);
            p.Add("pBidsSinceDays", bidActivity);
            p.Add("pHoldPeriodMonths", holdPeriod);
            p.Add("pLoansMaturingBeforeDays", loansMaturing);
            return Connection.Query<CompanySummaryStats>("xESxApp.GetCompanySummaryEntityCount", p.DynamicParameters, CommandType.StoredProcedure).SingleOrDefault();
        }

        public List<CompanyIntellisense> GetCompanyIntellisense(string searchText, int pageSize)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("pAccessToken", SecurityContext.DbToken);
            p.Add("pNamePattern", searchText);
            p.Add("pMaxRows", pageSize);

            return Connection.Query<CompanyIntellisense>("xEsxApp.IntellisenseCompanies", p.DynamicParameters, CommandType.StoredProcedure).ToList();
        }

        public IEnumerable<Alert> GetAlerts(string companyMentionId)
        {
            return null;
        }

        public void AcknowledgeAlert(int alertId)
        {
            
        }

        public void RemoveAlert(int alertId)
        {

        }

        public void SnoozeAlert(int alertId)
        {

        }

    }
}

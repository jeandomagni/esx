﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Esx.Contracts.Interfaces;
using Esx.Contracts.Models.Mention;
using Esx.Contracts.Security;
using Esx.Data.Dapper;

namespace Esx.Data.Repositories
{
    public interface IMentionsRepository : IBaseRepository
    {
        List<Mention> GetMentions(string searchText, int count);

        Mention CreateNewMention(string objectType, int objectKey, bool isPrimary);

        void SetIsWatched(string mentionId, bool isWatched);
    }

    public class MentionsRepository : RepositoryBase, IMentionsRepository
    {
        public MentionsRepository(IConnectionWrapper conn, ISecurityContext securityContext, IDapperParameterProvider dapperParameterProvider)
            : base(conn, securityContext, dapperParameterProvider)
        {
        }

        public List<Mention> GetMentions(string searchText, int count)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("@pAccessToken", SecurityContext.DbToken);
            p.Add("@pMentionPrefix", searchText);
            p.Add("@pMaxRows", count);

            return Connection.Query<Mention>("xEsxApp.IntellisenseMentions", p.DynamicParameters, CommandType.StoredProcedure).ToList();
        }

        public Mention CreateNewMention(string objectType, int objectKey, bool isPrimary)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("@pAccessToken", SecurityContext.DbToken);
            p.Add("@pObjectType", objectType);
            p.Add("@pObjectKey", objectKey);
            p.Add("@pIsPrimary", isPrimary);
            p.AddOutputParameter("@pMentionID", DbType.String, 128);
            p.AddOutputParameter("@pMentionKey", DbType.Int32);

            Connection.Execute("xEsxApp.CreateNewMention", p.DynamicParameters, CommandType.StoredProcedure);

            return new Mention
            {
                MentionKey = p.Get<int>("@pMentionKey"),
                MentionID = p.Get<string>("@pMentionID")
            };
        }

        public void SetIsWatched(string mentionId, bool isWatched)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("pAccessToken", SecurityContext.DbToken);
            p.Add("pMentionID", mentionId);
            p.Add("pOnOff", isWatched);
            Connection.Execute("xESxApp.UpdateMentionWatch", p.DynamicParameters, CommandType.StoredProcedure);
        }
    }
}

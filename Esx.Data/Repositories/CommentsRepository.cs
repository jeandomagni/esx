﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Esx.Contracts.Interfaces;
using Esx.Contracts.Models.Comment;
using Esx.Contracts.Security;
using Esx.Data.Dapper;

namespace Esx.Data.Repositories
{
    public interface ICommentsRepository : IBaseRepository
    {
        /// <summary>
        /// Get comments that are made directly on an object
        /// </summary>
        /// <param name="mentionId">The mentionId of the object</param>
        /// <param name="pageSize">max number of comments to return</param>
        /// <param name="offSet">start position</param>
        /// <returns></returns>
        List<CommentListItem> GetComments(string mentionId, int pageSize, int offSet);

        /// <summary>
        /// Get comments that contain a mentionId or are on the object mentioned
        /// </summary>
        /// <param name="mentionId">The mentionId of the object</param>
        /// <param name="pageSize">max number of comments to return</param>
        /// <param name="offSet">start position</param>
        /// <returns></returns>
        List<CommentListItem> GetMentionComments(string mentionId, int pageSize, int offSet, string filterText);


        int SaveComment(Comment comment);
    }

    public class CommentsRepository : RepositoryBase, ICommentsRepository
    {
        public CommentsRepository(IConnectionWrapper connection, ISecurityContext securityContext, IDapperParameterProvider dapperParameterProvider) 
            : base(connection, securityContext, dapperParameterProvider)
        {
        }

        public List<CommentListItem> GetComments(string mentionId, int pageSize, int offSet)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("@pAccessToken", SecurityContext.DbToken);
            p.Add("@pMentionID", mentionId);
            p.Add("@pMaxRows", pageSize);
            p.Add("@pFirstRow", offSet);

            return Connection.Query<CommentListItem>("xEsxApp.GetMentionObjectComments", p.DynamicParameters, CommandType.StoredProcedure).ToList();

        }

        public List<CommentListItem> GetMentionComments(string mentionId, int pageSize, int offSet, string filterText = null)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("@pAccessToken", SecurityContext.DbToken);
            p.Add("@pMentionID", mentionId);
            p.Add("@pFilterText", filterText);
            p.Add("@pSortColumn", null);
            p.Add("@pSortDescending", true);
            p.Add("@pMaxRows", pageSize);
            p.Add("@pFirstRow", offSet);

            return Connection.Query<CommentListItem>("xEsxApp.GetMentionReferenceComments", p.DynamicParameters, CommandType.StoredProcedure).ToList();

        }

        public int SaveComment(Comment comment)
        {
            comment.AccessToken = SecurityContext.DbToken; 

            var p = DapperParameterProvider.GetCommentDynamicParameter(comment);
            return Connection.Execute("xEsxApp.PutComment", p, CommandType.StoredProcedure);            
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Esx.Contracts.Interfaces;
using Esx.Contracts.Security;
using Esx.Data.Dapper;

namespace Esx.Data.Repositories
{
    public abstract class RepositoryBase : IBaseRepository
    {
        protected readonly IConnectionWrapper Connection;
        protected readonly ISecurityContext SecurityContext;
        protected readonly IDapperParameterProvider DapperParameterProvider;

        protected RepositoryBase(IConnectionWrapper connection, ISecurityContext securityContext, IDapperParameterProvider dapperParameterProvider)
        {
            SecurityContext = securityContext;
            DapperParameterProvider = dapperParameterProvider;
            Connection = connection;
        }

    }
}

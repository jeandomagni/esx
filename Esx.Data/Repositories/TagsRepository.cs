﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Esx.Contracts.Interfaces;
using Esx.Contracts.Models.Tag;
using Esx.Contracts.Security;
using Esx.Data.Dapper;

namespace Esx.Data.Repositories
{
    public interface ITagsRepository : IBaseRepository
    {
        List<Tag> GetTags(string searchText, int pageSize);

        List<TagNote> GetTagNotes(string tagName, int pageSize, int offSet);
    }

    public class TagsRepository : RepositoryBase, ITagsRepository
    {
        public TagsRepository(IConnectionWrapper connection, ISecurityContext securityContext, IDapperParameterProvider dapperParameterProvider) 
            : base(connection, securityContext, dapperParameterProvider)
        {
        }

        public List<Tag> GetTags(string searchText, int pageSize)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("@pAccessToken", SecurityContext.DbToken);
            p.Add("@pTagPrefix", searchText);
            if (pageSize != 0) p.Add("@pMaxRows", pageSize);

            return Connection.Query<Tag>("xEsxApp.IntellisenseTags", p.DynamicParameters, CommandType.StoredProcedure).ToList();
        }

        public List<TagNote> GetTagNotes(string tagName, int pageSize, int offSet)
        {
            var p = DapperParameterProvider.GetDapperParameters();
            p.Add("@pAccessToken", SecurityContext.DbToken);
            p.Add("@pTagID", tagName);
            if (pageSize != 0) p.Add("@pMaxRows", pageSize);
            if (offSet != 0) p.Add("@pFirstRow", offSet);

            return Connection.Query<TagNote>("xEsxApp.GetTagComments", p.DynamicParameters, CommandType.StoredProcedure).ToList();
        }

    }
}

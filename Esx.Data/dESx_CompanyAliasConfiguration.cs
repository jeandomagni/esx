// <auto-generated>
// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier
// TargetFrameworkVersion = 4.52
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using Esx.Contracts.Models;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace Esx.Data
{
    // CompanyAlias
    public partial class dESx_CompanyAliasConfiguration : EntityTypeConfiguration<dESx_CompanyAlias>
    {
        public dESx_CompanyAliasConfiguration()
            : this("dESx")
        {
        }
 
        public dESx_CompanyAliasConfiguration(string schema)
        {
            ToTable(schema + ".CompanyAlias");
            HasKey(x => x.CompanyAliasKey);

            Property(x => x.CompanyAliasKey).HasColumnName("CompanyAlias_Key").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyKey).HasColumnName("Company_Key").IsRequired().HasColumnType("int");
            Property(x => x.AliasName).HasColumnName("AliasName").IsRequired().HasColumnType("nvarchar").HasMaxLength(128);
            Property(x => x.IsLegal).HasColumnName("IsLegal").IsRequired().HasColumnType("bit");
            Property(x => x.IsPrimary).HasColumnName("IsPrimary").IsRequired().HasColumnType("bit");
            Property(x => x.IsDefunct).HasColumnName("IsDefunct").IsRequired().HasColumnType("bit");
            Property(x => x.CreatedDate).HasColumnName("CreatedDate").IsRequired().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(20);
            Property(x => x.UpdatedDate).HasColumnName("UpdatedDate").IsRequired().HasColumnType("datetime");
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy").IsOptional().IsUnicode(false).HasColumnType("varchar").HasMaxLength(20);

            // Foreign keys
            HasRequired(a => a.dESx_Company).WithMany(b => b.dESx_CompanyAlias).HasForeignKey(c => c.CompanyKey); // FK_Alias_Company
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>

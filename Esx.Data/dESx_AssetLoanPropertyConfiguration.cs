// <auto-generated>
// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier
// TargetFrameworkVersion = 4.52
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using Esx.Contracts.Models;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace Esx.Data
{
    // AssetLoanProperty
    public partial class dESx_AssetLoanPropertyConfiguration : EntityTypeConfiguration<dESx_AssetLoanProperty>
    {
        public dESx_AssetLoanPropertyConfiguration()
            : this("dESx")
        {
        }
 
        public dESx_AssetLoanPropertyConfiguration(string schema)
        {
            ToTable(schema + ".AssetLoanProperty");
            HasKey(x => x.AssetLoanPropertyKey);

            Property(x => x.AssetLoanPropertyKey).HasColumnName("AssetLoanProperty_Key").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PropertyAssetKey).HasColumnName("PropertyAsset_Key").IsRequired().HasColumnType("int");
            Property(x => x.LoanAssetKey).HasColumnName("LoanAsset_Key").IsRequired().HasColumnType("int");

            // Foreign keys
            HasRequired(a => a.dESx_Asset_LoanAssetKey).WithMany(b => b.dESx_AssetLoanProperties_LoanAssetKey).HasForeignKey(c => c.LoanAssetKey); // FK_AssetLoanProperty_LoanAsset
            HasRequired(a => a.dESx_Asset_PropertyAssetKey).WithMany(b => b.dESx_AssetLoanProperties_PropertyAssetKey).HasForeignKey(c => c.PropertyAssetKey); // FK_AssetLoanProperty_Asset
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>

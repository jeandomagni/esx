﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using LinqKit;

namespace Esx.Data
{
    /// <summary>
    /// Defines a contract for interacting with various databases
    /// </summary>
    public interface IDatabaseRepository<T> : IDisposable where T : class
    {
        /// <summary>
        /// Returns single entity in the database
        /// </summary>
        T GetByID(long id);
        
        /// <summary>
        /// Returns all the entities in the database
        /// </summary>
        IQueryable<T> GetAll();

        IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties);

        /// <summary>
        /// Deletes a single entity
        /// </summary>
        void Delete(T entity);

        /// <summary>
        /// Deletes multiple entities in a single call
        /// </summary>
        void Delete(IEnumerable<T> entitiesToDelete);

        /// <summary>
        /// Inserts a single entity
        /// </summary>
        T Insert(T entity);

        /// <summary>
        /// Inserts multiple entities
        /// </summary>
        IEnumerable<T> Insert(IEnumerable<T> entitiesToInsert);

        /// <summary>
        /// Updates a single entity
        /// </summary>
        void Update(T entity);

        /// <summary>
        /// Updates multiple entities
        /// </summary>
        void Update(IEnumerable<T> entitiesToUpdate);

        /// <summary>
        /// Search for an entity
        /// </summary>
        IQueryable<T> SearchFor(Expression<Func<T, bool>> predicate);
    }

    /// <summary>
    /// A repository for tables in the various databases
    /// </summary>
    public class DatabaseRepository<T> : IDatabaseRepository<T> where T : class
    {

        protected readonly DbContext DbContext;

        private readonly DbSet<T> _entities;

        /// <summary>
        /// Creates a new <see cref="DatabaseRepository{T}"/> based on the specified connection name.
        /// </summary>
        public DatabaseRepository(DbContext dbContext)
        {
            DbContext = dbContext;
            _entities = dbContext.Set<T>();
        }

        public T GetByID(long id)
        {
            return _entities.Find(id);
        }

        /// <inheritdoc/>
        public IQueryable<T> GetAll()
        {
            return _entities;
        }

        public IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _entities;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        /// <inheritdoc/>
        public void Delete(T entity)
        {
            Delete(new[] { entity });
        }

        /// <inheritdoc/>
        public void Delete(IEnumerable<T> entitiesToDelete)
        {
            if (entitiesToDelete == null) throw new ArgumentNullException(nameof(entitiesToDelete));
            if (_entities == null) return;
            foreach (var entity in entitiesToDelete)
            {
                AttachIfNecessary(entity);
                _entities.Remove(entity);
            }
            DbContext.SaveChanges();
        }

        private void AttachIfNecessary(T entity)
        {
            var entry = DbContext.Entry(entity);
            if (entry == null || entry.State == EntityState.Detached)
            {
                _entities.Attach(entity);
            }
        }

        /// <inheritdoc/>
        public T Insert(T entity)
        {
            Insert(new[] { entity });
            return entity;
        }

        /// <inheritdoc/>
        public IEnumerable<T> Insert(IEnumerable<T> entitiesToInsert)
        {
            if (entitiesToInsert == null) return null;
            foreach (var entity in entitiesToInsert)
            {
                _entities.Add(entity);
            }
            DbContext.SaveChanges();

            return entitiesToInsert;
        }

        /// <inheritdoc/>
        public void Update(T entity)
        {
            Update(new[] { entity });
        }

        /// <inheritdoc/>
        public void Update(IEnumerable<T> entitiesToUpdate)
        {
            if (entitiesToUpdate == null) return;
            foreach (var entity in entitiesToUpdate)
            {
                AttachIfNecessary(entity);
                DbContext.Entry(entity).State = EntityState.Modified;
            }
            DbContext.SaveChanges();
        }

        public IQueryable<T> SearchFor(Expression<Func<T, bool>> predicate)
        {            
            return _entities.AsExpandable().Where(predicate);
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }
    }
}
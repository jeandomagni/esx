﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using Esx.Contracts.Interfaces;
using Esx.Contracts.Models.Mention;
using Esx.Contracts.Security;
using Esx.Data.Repositories;

namespace Esx.Data
{
    public partial class EsxDbContext
    {
        private readonly IMentionsRepository _mentionsRepository;
        private readonly ISecurityContext _securityContext;

        public EsxDbContext(IMentionsRepository mentionsRepository, ISecurityContext securityContext, string connectionString) : base(connectionString)
        {
            _mentionsRepository = mentionsRepository;
            _securityContext = securityContext;
        }

        public override int SaveChanges()
        {
            SetAuditFields();

            var stateEntries = 0;

            var mentionEntities = ChangeTracker.Entries<IMentionEntity>()
                .Where(p => p.State == EntityState.Added)
                .Select(p => p.Entity).ToList();

            stateEntries = base.SaveChanges();

            foreach (var m in mentionEntities)
            {
                if (string.IsNullOrEmpty(m.MentionSource)) continue;
                var mention = _mentionsRepository.CreateNewMention(m.ObjectType, m.ObjectKey, m.ObjectPrimary);
                m.MentionId = mention?.MentionID;
            }

            return stateEntries;
        }

        private void SetAuditFields()
        {
            var addedAuditedEntities = ChangeTracker.Entries<IAuditCreateEntity>()
                .Where(p => p.State == EntityState.Added)
                .Select(p => p.Entity);

            var modifiedAuditedEntities = ChangeTracker.Entries<IAuditCreateUpdateEntity>()
                .Where(p => p.State == EntityState.Modified)
                .Select(p => p.Entity);

            var now = DateTime.UtcNow;

            foreach (var added in addedAuditedEntities)
            {
                added.CreatedDate = now;
                added.CreatedBy = _securityContext.UserName;
                var modified = added as IAuditCreateUpdateEntity;
                if (modified == null) continue;
                modified.UpdatedDate = now;
                modified.UpdatedBy = _securityContext.UserName;
            }

            foreach (var modified in modifiedAuditedEntities)
            {
                modified.UpdatedDate = now;
                modified.UpdatedBy = _securityContext.UserName;
            }
        }
    }
}

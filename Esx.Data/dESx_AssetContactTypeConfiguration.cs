// <auto-generated>
// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier
// TargetFrameworkVersion = 4.52
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using Esx.Contracts.Models;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace Esx.Data
{
    // AssetContactType
    public partial class dESx_AssetContactTypeConfiguration : EntityTypeConfiguration<dESx_AssetContactType>
    {
        public dESx_AssetContactTypeConfiguration()
            : this("dESx")
        {
        }
 
        public dESx_AssetContactTypeConfiguration(string schema)
        {
            ToTable(schema + ".AssetContactType");
            HasKey(x => x.AssetContactTypeCode);

            Property(x => x.AssetContactTypeCode).HasColumnName("AssetContactType_Code").IsRequired().HasColumnType("nvarchar").HasMaxLength(20).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.Sort).HasColumnName("Sort").IsOptional().HasColumnType("int");
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>

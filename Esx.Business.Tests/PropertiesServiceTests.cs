﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Esx.Business.Properties;
using Esx.Business.Search;
using Esx.Contracts.Enums;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Models.Property;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;
using Esx.Data;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;
using Esx.Contracts.Security;
// ReSharper disable InconsistentNaming
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable PossibleInvalidOperationException
// ReSharper disable UnusedVariable

namespace Esx.Business.Tests
{
    public abstract class PropertiesServiceTests
    {
        private PropertiesService _service;
        private FakeEsxDbContext _fakeEsxDbContext;
        private Mock<ISecurityContext> _securityContextMock;
        private Mock<ICompanyCriteriaSetService> _companyCriteriaSetServiceMock;
        private Mock<IDealCriteriaSetService> _dealCriteriaSetServiceMock;
        private Mock<ILoanCriteriaSetService> _loanCriteriaSetServiceMock;
        private Mock<IPropertyCriteriaSetService> _propertyCriteriaSetServiceMock;

        [SetUp]
        public void TestInit()
        {
            _fakeEsxDbContext = new FakeEsxDbContext();
            _securityContextMock = new Mock<ISecurityContext>();
            _companyCriteriaSetServiceMock = new Mock<ICompanyCriteriaSetService>();
            _dealCriteriaSetServiceMock = new Mock<IDealCriteriaSetService>();
            _loanCriteriaSetServiceMock = new Mock<ILoanCriteriaSetService>();
            _propertyCriteriaSetServiceMock = new Mock<IPropertyCriteriaSetService>();

            _service = new PropertiesService(
                _fakeEsxDbContext,
                _securityContextMock.Object,
                _dealCriteriaSetServiceMock.Object,
                _loanCriteriaSetServiceMock.Object,
                _companyCriteriaSetServiceMock.Object,
                _propertyCriteriaSetServiceMock.Object);
        }

        [TearDown]
        public void TestDispose()
        {
        }

        public sealed class GetPropertyList : PropertiesServiceTests
        {
            private List<PropertyListItem> MockPropertyData()
            {
                return MockPropertyData(string.Empty);
            }

            #region GetPropertyList Setup
            private List<PropertyListItem> MockPropertyData(string searchText)
            {
                var expectedPropertyListItems = new List<PropertyListItem>();
                int userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);
                int count = 10;

                while (count-- > 0)
                {
                    var asset = new dESx_Asset
                    {
                        AssetKey = RandomData.GetInt(),
                        AssetTypeCode = AssetType.Property.Code,
                        PhysicalAddressKey = RandomData.GetInt()
                    };
                    var dealAsset = new dESx_DealAsset
                    {
                        AssetKey = asset.AssetKey,
                        DealKey = RandomData.GetInt(),
                        DealAssetKey = RandomData.GetInt()
                    };
                    var deal = new dESx_Deal
                    {
                        DealKey = dealAsset.DealKey
                    };
                    var dealStatus = _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog
                    {
                        CreatedDate = RandomData.GetDateTime(),
                        DealKey = dealAsset.DealKey,
                        IsCurrent = true,
                        DealStatusCode = (count == 1 || count == 7) ? "Closed" : RandomData.GetString(4)
                    });
                    var mention = new dESx_Mention
                    {
                        IsPrimary = true,
                        MentionKey = RandomData.GetInt(),
                        PropertyAssetNameKey = RandomData.GetInt()
                    };
                    var assetName = new dESx_AssetName
                    {
                        AssetNameKey = mention.PropertyAssetNameKey.Value,
                        AssetKey = asset.AssetKey,
                        AssetName = (count == 1 || count == 7) ? searchText : RandomData.GetString(20),
                        IsCurrent = true
                    };
                    var assetValue = new dESx_AssetValue
                    {
                        AssetKey = asset.AssetKey,
                        IsCurrent = true
                    };
                    var physicalAddress = new dESx_PhysicalAddress
                    {
                        PhysicalAddressKey = asset.PhysicalAddressKey.Value,
                        City = "City",
                        StateProvidenceCode = "MN"
                    };
                    var userMentionData = new dESx_UserMentionData
                    {
                        MentionKey = mention.MentionKey,
                        UserKey = userKey,
                        IsWatched = RandomData.GetInt() % 2 >= 1
                    };
                    var assetUsageType = new dESx_AssetUsageType
                    {
                        AssetUsageTypeKey = RandomData.GetInt(),
                        UsageType = "Test " + count
                    };
                    var assetUsage = new dESx_AssetUsage
                    {
                        AssetKey = asset.AssetKey,
                        AssetUsageKey = RandomData.GetInt(),
                        AssetUsageTypeKey = assetUsageType.AssetUsageTypeKey,
                        IsCurrent = true,
                        IsPrimary = count == 5
                    };
                    expectedPropertyListItems.Add(new PropertyListItem
                    {
                        PropertyName = assetName.AssetName,
                        Type = string.Empty,
                        City = physicalAddress.City,
                        State = physicalAddress.StateProvidenceCode,
                        MentionId = mention.MentionId,
                        ActiveDeal = dealStatus.DealStatusCode != DealStatusCode.Closed,
                        Valuation = assetValue.AssetValue,
                        IsWatched = userMentionData.IsWatched,
                        LastCloseDate = dealStatus.DealStatusCode == DealStatusCode.Closed ? dealStatus.CreatedDate : (DateTime?)null,
                        Size = assetValue.Spaces,
                        SizeUnits = assetValue.SpaceType
                    });

                    _fakeEsxDbContext.dESx_Assets.Attach(asset);
                    _fakeEsxDbContext.dESx_DealAssets.Attach(dealAsset);
                    _fakeEsxDbContext.dESx_Deals.Attach(deal);
                    _fakeEsxDbContext.dESx_Mentions.Attach(mention);
                    _fakeEsxDbContext.dESx_AssetNames.Attach(assetName);
                    _fakeEsxDbContext.dESx_AssetValues.Attach(assetValue);
                    _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(physicalAddress);
                    _fakeEsxDbContext.dESx_UserMentionDatas.Attach(userMentionData);
                    _fakeEsxDbContext.dESx_AssetUsages.Attach(assetUsage);
                    _fakeEsxDbContext.dESx_AssetUsageTypes.Attach(assetUsageType);
                }
                _fakeEsxDbContext.dESx_AssetTypes.Attach(new dESx_AssetType
                {
                    AssetTypeCode = AssetType.Property.Code,
                    Description = "Description"
                });
                return expectedPropertyListItems.OrderByDescending(p => p.IsWatched).ToList();
            }
            #endregion GetPropertyList Setup

            [Test]
            public void Should_Get_List_Of_Properties()
            {
                //arrange
                var expectedPropertyList = MockPropertyData();
                var expected = expectedPropertyList.OrderBy("IsWatched desc, propertyName").Select(p => p.PropertyName);

                //act
                var actualPropertyItemList = _service.Search(new PropertySearchCriteria()).Resources;

                //assert
                actualPropertyItemList.Select(a => a.PropertyName).Should().Have.SameSequenceAs(expected);
            }

            [Test]
            public void Should_Get_Paged_List_Of_Properties()
            {
                //arrange
                MockPropertyData();
                var criteria = new PropertySearchCriteria
                {
                    SearchText = "",
                    PageSize = 5
                };

                //act
                var actualPropertyItemList = _service.Search(criteria).Resources;

                //assert
                actualPropertyItemList.Count.Should().Be(5);
            }

            [Test]
            public void Should_Get_List_Of_Properties_Based_On_Search_By_PropertyName()
            {
                //arrange
                var searchText = RandomData.GetString(10);
                MockPropertyData(searchText);
                var criteria = new PropertySearchCriteria { SearchText = searchText };

                //act                
                var actualPropertyItemList = _service.Search(criteria).Resources;

                //assert
                actualPropertyItemList.Count.Should().Be(2);
            }

            [Test]
            public void Should_Get_List_Of_Properties_Based_On_Search_By_PropertyType()
            {
                //arrange
                var expectedPropertyList = MockPropertyData();
                var criteria = new PropertySearchCriteria
                {
                    SearchText = expectedPropertyList.First().Type
                };

                //act                
                var actualPropertyItemList = _service.Search(criteria).Resources;

                //assert
                actualPropertyItemList.Count.Should().Be(10);
            }

            [Test]
            public void Should_Return_No_Properties_Based_On_Search_By_PropertyName()
            {
                //arrange
                MockPropertyData();
                var criteria = new PropertySearchCriteria
                {
                    SearchText = "shouldGetNothingBack"
                };

                //act                
                var actualPropertyItemList = _service.Search(criteria).Resources;

                //assert
                actualPropertyItemList.Count.Should().Be(0);
            }
        }

        public sealed class GetPropertyDetails : PropertiesServiceTests
        {
            [Test]
            public void Should_Get_Property_Details()
            {
                // arrange
                var mentionId = "@TestMention";
                var userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);

                var asset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset { AssetKey = RandomData.GetInt(), AssetTypeCode = AssetType.Property.Code });
                var assetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName { AssetNameKey = RandomData.GetInt(), AssetKey = asset.AssetKey, AssetName = RandomData.GetString(20), IsCurrent = true });
                var assetMention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { PropertyAssetNameKey = assetName.AssetNameKey, IsPrimary = true, MentionId = mentionId });
                var deal1 = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal { DealKey = RandomData.GetInt() });
                var dealStatus1 = _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog { CreatedDate = DateTime.Parse("1/1/2016"), DealKey = deal1.DealKey, DealStatusCode = DealStatusCode.Pitching, IsCurrent = true });
                var deal2 = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal { DealKey = RandomData.GetInt() });
                var dealStatus2 = _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog { CreatedDate = DateTime.Parse("1/2/2016"), DealKey = deal2.DealKey, DealStatusCode = DealStatusCode.PitchedLost, IsCurrent = true });
                var dealAsset = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset { AssetKey = asset.AssetKey, DealKey = deal1.DealKey });
                var dealAsset2 = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset { AssetKey = asset.AssetKey, DealKey = deal2.DealKey });
                var address = _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(new dESx_PhysicalAddress { PhysicalAddressKey = RandomData.GetInt(), City = RandomData.GetString(20), StateProvidenceCode = RandomData.GetString(2) });
                var assetValue = _fakeEsxDbContext.dESx_AssetValues.Attach(new dESx_AssetValue { AssetKey = asset.AssetKey, AssetValue = RandomData.GetDecimal(0, 1000), IsCurrent = true });
                var companyAsset = _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset { AssetKey = asset.AssetKey, CompanyKey = RandomData.GetInt(), IsCurrent = true });
                var companyAlias = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { AliasName = RandomData.GetString(20), IsPrimary = true, CompanyKey = companyAsset.CompanyKey });

                // act
                var actual = _service.GetPropertyDetails(mentionId);

                // assert
                actual.Should().Not.Be.Null();

                actual.Satisfies(x => x.PropertyName == assetName.AssetName
                                      && x.IsWatched == false
                                      && x.Owner == companyAlias.AliasName
                                      && x.ActiveDeal);
            }

            [Test]
            public void Should_Get_Property_Details_With_LastClosedDate()
            {
                // arrange
                var mentionId = "@TestMention";
                var userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);

                var asset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset { AssetKey = RandomData.GetInt(), AssetTypeCode = AssetType.Property.Code });
                var assetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName { AssetNameKey = RandomData.GetInt(), AssetKey = asset.AssetKey, AssetName = RandomData.GetString(20), IsCurrent = true });
                var assetMention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { PropertyAssetNameKey = assetName.AssetNameKey, IsPrimary = true, MentionId = mentionId });
                var deal1 = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal { DealKey = RandomData.GetInt() });
                var dealStatus1 = _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog { CreatedDate = DateTime.Parse("1/1/2016"), DealKey = deal1.DealKey, DealStatusCode = DealStatusCode.Closed, IsCurrent = true });
                var deal2 = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal { DealKey = RandomData.GetInt() });
                var dealStatus2 = _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog { CreatedDate = DateTime.Parse("1/2/2016"), DealKey = deal2.DealKey, DealStatusCode = DealStatusCode.Closed, IsCurrent = true });
                var dealAsset = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset { AssetKey = asset.AssetKey, DealKey = deal1.DealKey });
                var dealAsset2 = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset { AssetKey = asset.AssetKey, DealKey = deal2.DealKey });
                var address = _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(new dESx_PhysicalAddress { PhysicalAddressKey = RandomData.GetInt(), City = RandomData.GetString(20), StateProvidenceCode = RandomData.GetString(2) });
                var assetValue = _fakeEsxDbContext.dESx_AssetValues.Attach(new dESx_AssetValue { AssetKey = asset.AssetKey, AssetValue = RandomData.GetDecimal(0, 1000), IsCurrent = true });
                var companyAsset = _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset { AssetKey = asset.AssetKey, CompanyKey = RandomData.GetInt(), IsCurrent = true });
                var companyAlias = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { AliasName = RandomData.GetString(20), IsPrimary = true, CompanyKey = companyAsset.CompanyKey });

                // act
                var actual = _service.GetPropertyDetails(mentionId);

                // assert
                actual.Should().Not.Be.Null();

                actual.Satisfies(x => x.PropertyName == assetName.AssetName
                                      && x.IsWatched == false
                                      && x.LastCloseDate == dealStatus2.CreatedDate
                                      && x.Owner == companyAlias.AliasName
                                      && x.ActiveDeal == false);
            }

            [Test]
            public void Should_Return_Null_If_No_Property_Exists()
            {
                var actual = _service.GetPropertyDetails("none");
                actual.Should().Be.Null();
            }
        }

        public sealed class GetPropertyValuations : PropertiesServiceTests
        {
            [Test]
            public void Should_Get_Property_Valuations()
            {
                // arrange          
                string mentionId = "@TestMention";
                SetupValuationsTests(mentionId);

                // act
                var actual = _service.GetPropertyValuations(new PagingData(), mentionId);

                // assert
                actual.Should().Not.Be.Null();
                actual.PagingData.TotalCount.Should().Be(1);
                var valuation = actual.Resources.First();
                valuation.DealName.Satisfies(d => d == "Test Deal");
                valuation.AskingPrice.Satisfies(a => a == 123);
                valuation.Buyer.Satisfies(b => b == "Test Alias");
                valuation.DealCode.Satisfies(d => d == "TEST");
                valuation.ValuationDataKey.Satisfies(v => v == 1);
                valuation.ValuationDate.Satisfies(v => v == DateTime.Parse("1/1/2020"));
            }

            [Test]
            public void Should_Return_Null_If_No_Valuations()
            {
                // arrange          
                string mentionId = "@TestMention";
                SetupValuationsTests(mentionId);

                // act
                var actual = _service.GetPropertyValuations(new PagingData(), string.Empty);

                // assert
                actual.Resources.Count.Should().Be(0);
            }

            #region GetPropertyValuations Setup
            private void SetupValuationsTests(string mentionId)
            {
                int userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);
                var asset = new dESx_Asset
                {
                    AssetKey = RandomData.GetInt(),
                    AssetTypeCode = AssetType.Property.Code,
                    PhysicalAddressKey = RandomData.GetInt()
                };
                var dealAsset = new dESx_DealAsset
                {
                    AssetKey = asset.AssetKey,
                    DealKey = RandomData.GetInt(),
                    DealAssetKey = RandomData.GetInt()
                };
                var deal = new dESx_Deal
                {
                    DealKey = dealAsset.DealKey,
                    DealName = "Test Deal",
                    DealCode = "TEST"
                };
                var dealStatusLog = new dESx_DealStatusLog
                {
                    IsCurrent = true,
                    DealStatusCode = RandomData.GetString(4),
                    CreatedDate = RandomData.GetDateTime(),
                    DealKey = dealAsset.DealKey
                };
                var mention = new dESx_Mention
                {
                    IsPrimary = true,
                    MentionKey = RandomData.GetInt(),
                    PropertyAssetNameKey = RandomData.GetInt(),
                    MentionId = mentionId
                };
                var assetName = new dESx_AssetName
                {
                    AssetNameKey = mention.PropertyAssetNameKey.Value,
                    AssetKey = asset.AssetKey,
                    AssetName = RandomData.GetString(20),
                    IsCurrent = true
                };
                var assetValue = new dESx_AssetValue
                {
                    AssetValueKey = 1,
                    AssetKey = asset.AssetKey,
                    IsCurrent = true,
                    AssetValue = 123,
                    CreatedDate = DateTime.Parse("1/1/2020")
                };
                var physicalAddress = new dESx_PhysicalAddress
                {
                    PhysicalAddressKey = asset.PhysicalAddressKey.Value,
                    City = "City",
                    StateProvidenceCode = "MN"
                };
                var userMentionData = new dESx_UserMentionData
                {
                    MentionKey = mention.MentionKey,
                    UserKey = userKey,
                    IsWatched = RandomData.GetInt() % 2 >= 1
                };
                var assetType = new dESx_AssetType
                {
                    AssetTypeCode = AssetType.Property.Code,
                    Description = "Description"
                };
                var companyAlias = new dESx_CompanyAlias
                {
                    IsPrimary = true,
                    CompanyKey = RandomData.GetInt(),
                    AliasName = "Test Alias"
                };
                var companyAsset = new dESx_CompanyAsset
                {
                    CompanyKey = companyAlias.CompanyKey,
                    AssetKey = asset.AssetKey
                };
                _fakeEsxDbContext.dESx_Assets.Attach(asset);
                _fakeEsxDbContext.dESx_DealAssets.Attach(dealAsset);
                _fakeEsxDbContext.dESx_Deals.Attach(deal);
                _fakeEsxDbContext.dESx_DealStatusLogs.Attach(dealStatusLog);
                _fakeEsxDbContext.dESx_Mentions.Attach(mention);
                _fakeEsxDbContext.dESx_AssetNames.Attach(assetName);
                _fakeEsxDbContext.dESx_AssetValues.Attach(assetValue);
                _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(physicalAddress);
                _fakeEsxDbContext.dESx_UserMentionDatas.Attach(userMentionData);
                _fakeEsxDbContext.dESx_AssetTypes.Attach(assetType);
                _fakeEsxDbContext.dESx_CompanyAlias.Attach(companyAlias);
                _fakeEsxDbContext.dESx_CompanyAssets.Attach(companyAsset);
            }
            #endregion GetPropertyList Setup
        }

        public sealed class GetPropertiesByCompanyMentionID : PropertiesServiceTests
        {
            int assetKey;
            string propertyName;
            int assetNameKey;
            string assetType;
            decimal assetValue;
            string companyMentionId;
            bool isCurrent;
            string propertyMentionId;
            int companyKey;
            int companyAliasKey;
            int spaces;
            bool isWatched;
            int dealKey;
            string dealCode;
            int propertyMentionKey;
            string propertyType;

            dESx_Asset property;
            dESx_AssetName propertyN;
            dESx_AssetValue propertyValue;
            dESx_Deal deal;

            private void ArrangeCompanyPropertyData()
            {
                assetKey = RandomData.GetInt();
                propertyName = RandomData.GetString(20);
                assetNameKey = RandomData.GetInt();
                assetType = AssetType.Property.Code;
                assetValue = RandomData.GetDecimal();
                companyMentionId = "SomeCompany";
                isCurrent = true;
                propertyMentionId = RandomData.GetString(20);
                companyKey = RandomData.GetInt();
                companyAliasKey = RandomData.GetInt();
                spaces = RandomData.GetInt();
                isWatched = RandomData.GetBoolean();
                dealKey = RandomData.GetInt();
                dealCode = RandomData.GetString(20);
                propertyMentionKey = RandomData.GetInt();

                property = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset { AssetKey = assetKey, AssetTypeCode = assetType });
                propertyN = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName { AssetNameKey = assetNameKey, AssetKey = assetKey, AssetName = propertyName, IsCurrent = isCurrent });
                _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { AliasKey = companyAliasKey, MentionId = companyMentionId });
                _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset { CompanyKey = companyKey, AssetKey = assetKey, IsCurrent = true });
                _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { CompanyKey = companyKey, CompanyAliasKey = companyAliasKey, IsPrimary = true });
                propertyValue = _fakeEsxDbContext.dESx_AssetValues.Attach(new dESx_AssetValue { AssetKey = assetKey, AssetValue = assetValue, Spaces = spaces, IsCurrent = isCurrent });
                _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { MentionId = propertyMentionId, PropertyAssetNameKey = assetNameKey });
                _fakeEsxDbContext.dESx_UserMentionDatas.Attach(new dESx_UserMentionData { IsWatched = isWatched, MentionKey = propertyMentionKey });
                _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset { AssetKey = assetKey, DealKey = dealKey });
                deal = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal { DealKey = dealKey, DealCode = dealCode });
                _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { DealKey = dealKey });
                _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog { DealKey = dealKey, IsCurrent = true });

                var usageKey1 = RandomData.GetInt();
                var usageKey2 = RandomData.GetInt();
                _fakeEsxDbContext.dESx_AssetUsages.Attach(new dESx_AssetUsage
                {
                    AssetKey = assetKey,
                    AssetUsageTypeKey = usageKey1,
                    IsCurrent = true,
                    IsPrimary = true
                });
                _fakeEsxDbContext.dESx_AssetUsages.Attach(new dESx_AssetUsage
                {
                    AssetKey = assetKey,
                    AssetUsageTypeKey = usageKey2,
                    IsCurrent = true,
                    IsPrimary = false
                });
                var auy1 = _fakeEsxDbContext.dESx_AssetUsageTypes.Attach(new dESx_AssetUsageType
                {
                    AssetUsageTypeKey = usageKey1,
                    UsageType = RandomData.GetString(20)
                });
                var auy2 = _fakeEsxDbContext.dESx_AssetUsageTypes.Attach(new dESx_AssetUsageType
                {
                    AssetUsageTypeKey = usageKey2,
                    UsageType = RandomData.GetString(20)
                });
                propertyType = $"{auy1.UsageType}, {auy2.UsageType}";

            }

            [Test]
            public void Should_Get_List_Of_Properties_For_Company()
            {
                // Arrange
                ArrangeCompanyPropertyData();
                var criteria = new PropertySearchCriteria
                {
                    CompanyMentionId = companyMentionId
                };

                // Act
                var actual = _service.Search(criteria).Resources.Single();

                // Assert
                actual.Satisfies(x => x.PropertyID == property.AssetKey
                                      && x.PropertyName == propertyN.AssetName
                                      && x.DealCode == deal.DealCode
                                      && x.Size == propertyValue.Spaces
                                      && x.Type == propertyType);
            }

            [Test]
            public void Should_Get_List_Of_Properties_For_Company_With_Search_Text()
            {
                // Arrange
                for (var i = 0; i < 10; i++)
                {
                    ArrangeCompanyPropertyData();
                }
                var criteria = new PropertySearchCriteria
                {
                    SearchText = propertyName.Substring(2, 4),
                    CompanyMentionId = companyMentionId
                };

                // Act
                var actual = _service.Search(criteria).Resources.Single();

                // Assert
                actual.Satisfies(x => x.PropertyID == property.AssetKey
                                      && x.PropertyName == propertyN.AssetName
                                      && x.DealCode == deal.DealCode
                                      && x.Size == propertyValue.Spaces
                                      && x.Type == propertyType);
            }

            [Test]
            public void Should_Get_Nothing_With_Bad_Search_Text()
            {
                // Arrange
                ArrangeCompanyPropertyData();
                var criteria = new PropertySearchCriteria
                {
                    SearchText = "CANT FIND ME",
                    CompanyMentionId = companyMentionId
                };

                // Act
                var actual = _service.Search(criteria).Resources;

                // Assert
                actual.Satisfies(x => !x.Any());
            }

            private static IEnumerable<string> FieldNames => new[] { "isWatched", "propertyID", "propertyName", "dealCode", "type", "size", "valuation", "jointVenture" };

            [TestCaseSource(nameof(FieldNames))]
            public void Should_Sort_Properties(string sortColumn)
            {

                //arrange 
                for (var i = 0; i < 10; i++)
                {
                    ArrangeCompanyPropertyData();
                }
                var criteria = new PropertySearchCriteria
                {
                    CompanyMentionId = companyMentionId,
                    Sort = sortColumn
                };

                // Act
                var actual = _service.Search(criteria).Resources;

                // Assert
                actual.Select(x => x.PropertyName).Should().Have.SameSequenceAs(actual.OrderBy(sortColumn).Select(x => x.PropertyName));
            }

            [TestCaseSource(nameof(FieldNames))]
            public void Should_Sort_Properties_Descending(string sortColumn)
            {

                //arrange 
                for (var i = 0; i < 10; i++)
                {
                    ArrangeCompanyPropertyData();
                }
                var criteria = new PropertySearchCriteria
                {
                    CompanyMentionId = companyMentionId,
                    Sort = sortColumn,
                    SortDescending = true
                };
                var testSortColumn = sortColumn + " DESC";

                // Act
                var actual = _service.Search(criteria).Resources;

                // Assert
                actual.Select(x => x.PropertyName).Should().Have.SameSequenceAs(actual.OrderBy(testSortColumn).Select(x => x.PropertyName));
            }

        }

        public sealed class GetPropertyIntellisense : PropertiesServiceTests
        {
            [Test]
            public void Should_Call_PropertyIntellisense_Method_and_return_list_of_results()
            {
                // Arrange
                var count = RandomData.GetInt();

                var asset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset { AssetKey = RandomData.GetInt() });
                var assetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName { AssetKey = asset.AssetKey, AssetName = RandomData.GetString(20), IsCurrent = true });
                var searchText = assetName.AssetName.Substring(4, 10);

                // Act
                var actual = _service.PropertyIntellisense(searchText);

                // Assert
                actual.Count().Should().Be(1);
            }
        }
    }
}
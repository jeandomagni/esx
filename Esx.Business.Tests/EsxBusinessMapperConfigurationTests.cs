﻿using AutoMapper;
using Esx.Business.Contacts;
using NUnit.Framework;

namespace Esx.Business.Tests
{

    public abstract class EsxBusinessMapperConfigurationTests
    {
        [SetUp]
        public void TestInit()
        {
            // Configure mappings
            Mapper.AddProfile(new EsxBusinessMapperConfiguration());
        }

        [TearDown]
        public void TestDispose()
        {
            // Clear mappings
            Mapper.Reset();
        }

        public sealed class Configure : EsxBusinessMapperConfigurationTests
        {
            [Test]
            public void Configuration_Should_Be_Valid()
            {
                //assert
                Mapper.AssertConfigurationIsValid();
            }

        }
    }
}

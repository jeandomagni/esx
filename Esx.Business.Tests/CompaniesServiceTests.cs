﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using AutoMapper;
using Esx.Business.Companies;
using Esx.Business.Search;
using Esx.Business.Users;
using Esx.Contracts.Enums;
using Esx.Contracts.ExceptionHandling;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Company;
using Esx.Contracts.Models.User;
using Esx.Contracts.Paging;
using Esx.Contracts.Security;
using Esx.Data;
using Esx.Data.Repositories;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

// ReSharper disable PossibleNullReferenceException

namespace Esx.Business.Tests
{
    public abstract class CompaniesServiceTests
    {
        private Mock<ICompaniesRepository> _companiesRepoMock;
        private FakeEsxDbContext _fakeEsxDbContext;
        private Mock<IUsersService> _usersServiceMock;
        private Mock<ISecurityContext> _securityContextMock;
        private Mock<ICompanyCriteriaSetService> _companyCriteriaSetServiceMock;
        private Mock<IDealCriteriaSetService> _dealCriteriaSetServiceMock;
        private Mock<ILoanCriteriaSetService> _loanCriteriaSetServiceMock;
        private Mock<IPropertyCriteriaSetService> _propertyCriteriaSetServiceMock;

        private CompaniesService _service;

        [SetUp]
        public void TestInit()
        {

            // Configure mappings
            Mapper.AddProfile(new EsxBusinessMapperConfiguration());

            _companiesRepoMock = new Mock<ICompaniesRepository>();
            _securityContextMock = new Mock<ISecurityContext>();
            _fakeEsxDbContext = new FakeEsxDbContext();
            _usersServiceMock = new Mock<IUsersService>();
            _companyCriteriaSetServiceMock = new Mock<ICompanyCriteriaSetService>();
            _dealCriteriaSetServiceMock = new Mock<IDealCriteriaSetService>();
            _loanCriteriaSetServiceMock = new Mock<ILoanCriteriaSetService>();
            _propertyCriteriaSetServiceMock = new Mock<IPropertyCriteriaSetService>();

            _service = new CompaniesService(
                _companiesRepoMock.Object,
                _fakeEsxDbContext,
                _usersServiceMock.Object,
                _securityContextMock.Object,
                _dealCriteriaSetServiceMock.Object,
                _loanCriteriaSetServiceMock.Object,
                _companyCriteriaSetServiceMock.Object,
                _propertyCriteriaSetServiceMock.Object
                );
        }

        [TearDown]
        public void TestDispose()
        {
            // Clear mappings
            Mapper.Reset();
        }

        public sealed class GetCompanyName : CompaniesServiceTests
        {
            [Test]
            public void Should_Get_Company_Name()
            {
                //arrange
                var aliasKey = RandomData.GetInt();
                var mentionId = RandomData.GetString(20);
                var expectedAliasKey = RandomData.GetString(20);

                _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                {
                    CompanyAliasKey = aliasKey,
                    AliasName = expectedAliasKey
                });
                _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    AliasKey = aliasKey,
                    MentionId = mentionId
                });

                //act
                var actualAliasName = _service.GetCompanyName(mentionId);

                //assert
                actualAliasName.Satisfies(s => s == expectedAliasKey);
            }
        }

        public sealed class GetCompanySumary : CompaniesServiceTests
        {
            [Test]
            public void Should_Get_Company_Summary_By_Mention_ID()
            {
                // Arrange
                var expected = new CompanyStatistics { ActiveDeals = 1, BidVolume = "1M", Investments = 2, Locations = 3 };
                _companiesRepoMock.Setup(c => c.GetCompanyStatistics(It.IsAny<string>())).Returns(expected);

                // Act
                var actual = _service.GetCompanyStatistics("some mention id");

                // Assert
                actual.Should().Not.Be.Null();
                actual.ActiveDeals.Should().Be.EqualTo(expected.ActiveDeals);
                actual.BidVolume.Should().Be.EqualTo(expected.BidVolume);
                actual.Investments.Should().Be.EqualTo(expected.Investments);
                actual.Locations.Should().Be.EqualTo(expected.Locations);
            }
        }

        public sealed class GetCompanySummaryStats : CompaniesServiceTests
        {
            [Test]
            public void Should_Call_GetSummaryStats_Repository_Method()
            {
                // Arrange
                var mentionID = "@test";
                var pitches = RandomData.GetInt();
                var bids = RandomData.GetInt();
                var holdPeriod = RandomData.GetInt();
                var loansMaturing = RandomData.GetInt();
                _companiesRepoMock.Setup(x => x.GetCompanySummaryStats(
                    It.Is<string>(m => m == mentionID),
                    It.Is<int>(i => i == pitches),
                    It.Is<int>(i => i == bids),
                    It.Is<int>(i => i == holdPeriod),
                    It.Is<int>(i => i == loansMaturing)
                    )).Verifiable();

                // Act
                _service.GetCompanySummaryStats(mentionID, pitches, bids, holdPeriod, loansMaturing);

                // Assert
                _companiesRepoMock.VerifyAll();
            }
        }

        public sealed class GetCompanyIntellisense : CompaniesServiceTests
        {
            [Test]
            public void Should_Call_CompanyIntellisense_Repository_Method_and_return_list_of_results()
            {
                // Arrange
                var query = RandomData.GetString(10);
                var count = RandomData.GetInt(1, 10);
                var expectedList = new List<CompanyIntellisense>
                {
                     new CompanyIntellisense
                    {
                        MentionID = RandomData.GetString(10),
                        AliasName = RandomData.GetString(10),
                        Relevance = RandomData.GetInt(0,100)
                    },
                    new CompanyIntellisense(),
                    new CompanyIntellisense(),
                };
                _companiesRepoMock.Setup(
                    x => x.GetCompanyIntellisense(It.Is<string>(m => m == query), It.Is<int>(i => i == count))).Returns(
                    expectedList
                    );

                // Act
                var actual = _service.CompanyIntellisense(query, count);

                // Assert
                actual.Should().Have.SameValuesAs(expectedList);
                _companiesRepoMock.VerifyAll();
            }
        }

        public sealed class AddCompany : CompaniesServiceTests
        {
            [Test]
            [ExpectedException(typeof(BadRequestException))]
            public void Should_Fail_When_CompanyName_Already_In_Use()
            {
                //Arrange
                CompanyAdd model = new CompanyAdd
                {
                    CompanyName = "Test Company"
                };

                _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                {
                    AliasName = model.CompanyName
                });

                //Act
                _service.AddCompany(model);
                //Assert
            }

            [Test]
            public void Should_Add_Company_Successfully()
            {
                //Arrange
                var model = new CompanyAdd
                {
                    CompanyName = RandomData.GetString(20),
                    ParentCompany = 1,
                    SetDefaultCountry = true,
                    LocationCountry = "102"
                };

                //Act
                _service.AddCompany(model);

                //Assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }

            [Test]
            public void Should_Add_Company_With_Seconday_ALias()
            {
                //Arrange
                var model = new CompanyAdd
                {
                    CompanyName = RandomData.GetString(20),
                    ParentCompany = 1,
                    AliasName = RandomData.GetString(20),
                    SetDefaultCountry = true,
                    LocationCountry = "102"
                };

                //Act
                _service.AddCompany(model);

                //Assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }

            [Test]
            public void Should_Add_Company_With_Existing_Contact_Successfully()
            {
                var contactKey = RandomData.GetInt();
                //Arrange
                var model = new CompanyAdd
                {
                    CompanyName = RandomData.GetString(20),
                    ParentCompany = 1,
                    ContactKey = contactKey,
                    LocationCountry = "102"
                };
                _fakeEsxDbContext.dESx_Contacts.Attach(new dESx_Contact
                {
                    ContactKey = contactKey
                });
                _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    MentionKey = RandomData.GetInt(),
                    ContactKey = contactKey
                });

                //Act
                _service.AddCompany(model);

                //Assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }

            [Test]
            public void Should_Call_UsersService_When_Country_Flag_Set()
            {
                //Arrange
                var model = new CompanyAdd
                {
                    CompanyName = RandomData.GetString(20),
                    SetDefaultCountry = true,
                    LocationCountry = "102"
                };
                _usersServiceMock.Setup(x => x.SaveUserPreference(It.IsAny<string>(), It.IsAny<string>()))
                    .Returns(new UserPreferenceListItem());

                //Act
                _service.AddCompany(model);

                //Assert
                _usersServiceMock.VerifyAll();
            }
        }

        public sealed class GetCompanyLocations : CompaniesServiceTests
        {
            private void MockData(string companyMentionId, string expectedName = null, string expectedAddress1 = null, string expectedAddress2 = null, string expectedCity = null, string expectedState = null, string expectedPhone = null, string expectedZip = null, int expectedCountryKey = 0)
            {
                expectedName = expectedName ?? RandomData.GetString(20);
                expectedAddress1 = expectedAddress1 ?? RandomData.GetString(20);
                expectedAddress2 = expectedAddress2 ?? RandomData.GetString(20);
                expectedCity = expectedCity ?? RandomData.GetString(20);
                expectedState = expectedState ?? RandomData.GetString(2);
                expectedPhone = expectedPhone ?? RandomData.GetString(20);
                expectedZip = expectedZip ?? RandomData.GetString(20);
                expectedCountryKey = expectedCountryKey > 0 ? expectedCountryKey : RandomData.GetInt();

                for (var i = 0; i < 10; i++)
                {
                    var electronicAddressKey = RandomData.GetInt();
                    var officeKey = RandomData.GetInt();
                    var aliasKey = RandomData.GetInt();
                    var addressKey = RandomData.GetInt();
                    var companyKey = RandomData.GetInt();
                    var contactKey = RandomData.GetInt();

                    var contactMentionKey = RandomData.GetInt();
                    var officeMentionKey = RandomData.GetInt();

                    _fakeEsxDbContext.dESx_ElectronicAddresses.Attach(new dESx_ElectronicAddress
                    {
                        ElectronicAddressKey = electronicAddressKey,
                        Address = i > 0 ? RandomData.GetString(10) : expectedPhone,
                        AddressUsageTypeCode = i > 0 ? RandomData.GetString(10) : AddressUsageType.Main,
                        ElectronicAddressTypeCode = ElectronicAddressType.PhoneNumber
                    });

                    _fakeEsxDbContext.dESx_OfficeElectronicAddresses.Attach(new dESx_OfficeElectronicAddress
                    {
                        ElectronicAddressKey = electronicAddressKey,
                        OfficeKey = officeKey
                    });

                    _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                    {
                        CompanyAliasKey = i > 0 ? RandomData.GetInt() : aliasKey,
                        CompanyKey = i > 0 ? RandomData.GetInt() : companyKey
                    });

                    _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                    {
                        AliasKey = aliasKey,
                        MentionId = i > 0 ? RandomData.GetString(10) : companyMentionId
                    });

                    _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                    {
                        ContactKey = contactKey,
                        MentionKey = contactMentionKey
                    });

                    _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                    {
                        OfficeKey = officeKey,
                        MentionKey = officeMentionKey
                    });

                    _fakeEsxDbContext.dESx_Offices.Attach(new dESx_Office
                    {
                        IsHeadquarters = true,
                        OfficeKey = officeKey,
                        CompanyKey = companyKey,
                        PhysicalAddressKey = addressKey,
                        OfficeName = i > 0 ? RandomData.GetString(10) : expectedName
                    });

                    _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(new dESx_PhysicalAddress
                    {
                        PhysicalAddressKey = addressKey,
                        Address1 = i > 0 ? RandomData.GetString(10) : expectedAddress1,
                        Address2 = i > 0 ? RandomData.GetString(10) : expectedAddress2,
                        City = i > 0 ? RandomData.GetString(10) : expectedCity,
                        StateProvidenceCode = i > 0 ? RandomData.GetString(2) : expectedState,
                        PostalCode = i > 0 ? RandomData.GetString(10) : expectedZip,
                        CountryKey = i > 0 ? RandomData.GetInt() : expectedCountryKey,
                    });

                    _fakeEsxDbContext.dESx_Contacts.Attach(new dESx_Contact
                    {
                        ContactKey = contactKey,
                        FirstName = RandomData.GetString(10)
                    });

                    _fakeEsxDbContext.dESx_ContactOffices.Attach(new dESx_ContactOffice
                    {
                        OfficeKey = officeKey,
                        IsCurrent = true,
                        ContactKey = contactKey
                    });
                }
            }

            [Test]
            public void Should_Get_Company_Locations_With_SearchText()
            {
                //arrange
                var expectedName = "My Office";
                var expectedAddress1 = RandomData.GetString(10);
                var expectedAddress2 = RandomData.GetString(10);
                var expectedAddress = $"{expectedAddress1} {expectedAddress2}";
                var expectedCity = RandomData.GetString(20);
                var expectedState = RandomData.GetString(2);
                var expectedPhone = RandomData.GetString(10);
                var expectedZip = RandomData.GetString(20);
                var companyMentionId = RandomData.GetString(20);
                var expectedCountry = RandomData.GetInt();
                MockData(companyMentionId, expectedName, expectedAddress1, expectedAddress2, expectedCity, expectedState, expectedPhone, expectedZip, expectedCountry);

                var searchText = "Office";
                var pagingData = new PagingData
                {
                    Offset = 0,
                    PageSize = 10,
                    Sort = "",
                    SortDescending = false
                };

                //act
                var actual = _service.GetCompanyLocations(companyMentionId, pagingData, searchText).Resources.Single();

                //assert
                actual.Satisfies(
                    s =>
                    s.IsHeadquarters
                    && s.Name == expectedName
                    && s.Address == expectedAddress
                    && s.Address1 == expectedAddress1
                    && s.Address2 == expectedAddress2
                    && s.City == expectedCity
                    && s.Zip == expectedZip
                    && s.CountryKey == expectedCountry
                    && s.Phone == expectedPhone
                    && s.StateProvince == expectedState
                    && s.Contacts == 1
                );
            }

            [Test]
            public void Should_Get_Company_Locations_With_Sort()
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                MockData(companyMentionId);

                var searchText = "";
                var pagingData = new PagingData
                {
                    Offset = 0,
                    PageSize = 10,
                    Sort = "Name",
                    SortDescending = false
                };

                //act
                var actual = _service.GetCompanyLocations(companyMentionId, pagingData, searchText).Resources;

                //assert
                actual.Select(x => x.Name).Should().Have.SameSequenceAs(actual.OrderBy(x => x.Name).Select(x => x.Name));
            }

            [Test]
            public void Should_Get_Company_Locations_With_Sort_Descending()
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                MockData(companyMentionId);

                var searchText = "";
                var pagingData = new PagingData
                {
                    Offset = 0,
                    PageSize = 10,
                    Sort = "Name",
                    SortDescending = true
                };

                //act
                var actual = _service.GetCompanyLocations(companyMentionId, pagingData, searchText).Resources;

                //assert
                actual.Select(x => x.Name).Should().Have.SameSequenceAs(actual.OrderByDescending(x => x.Name).Select(x => x.Name));
            }

            [Test]
            public void Should_Get_Company_Locations_PageSize()
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                MockData(companyMentionId);

                const string searchText = "";
                var pagingData = new PagingData
                {
                    Offset = 0,
                    PageSize = 1,
                    Sort = "",
                    SortDescending = false
                };

                //act
                var actual = _service.GetCompanyLocations(companyMentionId, pagingData, searchText).Resources;

                //assert
                actual.Satisfies(a => a.Count == 1);
            }

            [Test]
            public void Should_Not_Get_Company_Locations()
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                MockData(companyMentionId);

                const string searchText = "LKJFLKJFLKJFK";
                var pagingData = new PagingData
                {
                    Offset = 0,
                    PageSize = 10,
                    Sort = "",
                    SortDescending = false
                };

                //act
                var actual = _service.GetCompanyLocations(companyMentionId, pagingData, searchText).Resources;

                //assert
                actual.Satisfies(a => !a.Any());
            }
        }

        public sealed class AddCompanyContact : CompaniesServiceTests
        {
            [Test]
            public void Should_Add_Contact_Record_To_The_Database()
            {
                //arrange
                var companyMentionId = RandomData.GetString(10);
                var contactToAdd = new CompanyContactAdd { ContactName = RandomData.GetString(10), OfficeKey = RandomData.GetInt() };
                SetupCompanyMention(companyMentionId, RandomData.GetInt());

                //act
                var actual = _service.AddCompanyContact(companyMentionId, contactToAdd);

                //assert
                _fakeEsxDbContext.dESx_Contacts.First().Satisfies(x => x.FullName == contactToAdd.ContactName);
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
                actual.Should().Be.EqualTo(0);
            }

            [Test]
            [ExpectedException(typeof(InvalidOperationException))]
            public void Should_Throw_An_Exception_If_Missing_Location()
            {
                //arrange
                var companyMentionId = RandomData.GetString(10);
                var contactToAdd = new CompanyContactAdd { ContactName = RandomData.GetString(10) };
                SetupCompanyMention(companyMentionId, RandomData.GetInt());

                //act
                _service.AddCompanyContact(companyMentionId, contactToAdd);
            }

            [Test]
            public void Should_Add_A_Location_If_Existing_Location_Not_Specified()
            {
                //arrange
                var companyMentionId = RandomData.GetString(10);
                var contactToAdd = new CompanyContactAdd
                {
                    ContactName = RandomData.GetString(10),
                    Location = new CompanyLocationAdd
                    {
                        Name = RandomData.GetString(10),
                        Address1 = RandomData.GetString(10)
                    }
                };
                SetupCompanyMention(companyMentionId, RandomData.GetInt());

                //act
                var actual = _service.AddCompanyContact(companyMentionId, contactToAdd);

                //assert
                _fakeEsxDbContext.dESx_Companies.First().dESx_Offices.First().Satisfies(x => x.OfficeName == contactToAdd.Location.Name);
                _fakeEsxDbContext.dESx_Companies.First().dESx_Offices.First().dESx_PhysicalAddress.Satisfies(x => x.Address1 == contactToAdd.Location.Address1);
                _fakeEsxDbContext.SaveChangesCount.Should().Be(2);
                actual.Should().Be.EqualTo(0);
            }
        }

        public sealed class AddCompanyLocation : CompaniesServiceTests
        {
            [Test]
            public void Should_Add_Locaiton_To_A_Company_With_Given_MentionId_And_Return_OfficeKey()
            {
                //arrange
                var locationToAdd = new CompanyLocationAdd
                {
                    Name = RandomData.GetString(10),
                    Address1 = RandomData.GetString(10),
                    Phone = RandomData.GetString(10),
                    Website = RandomData.GetString(10),
                };

                var expectedLocationMentionId = RandomData.GetString(10);
                var company = SetupCompanyMention(expectedLocationMentionId, RandomData.GetInt());

                //act
                var actual = _service.AddCompanyLocation(expectedLocationMentionId, locationToAdd);

                //assert
                var expectedOffice = company.dESx_Offices.FirstOrDefault();

                expectedOffice.OfficeName.Should().Be.EqualTo(locationToAdd.Name);

                expectedOffice.dESx_PhysicalAddress.Address1.Should().Be.EqualTo(locationToAdd.Address1);

                expectedOffice.dESx_OfficeElectronicAddresses.FirstOrDefault(
                        a => a.dESx_ElectronicAddress.ElectronicAddressTypeCode == ElectronicAddressType.PhoneNumber)
                    .dESx_ElectronicAddress.Address.Should()
                    .Be.EqualTo(locationToAdd.Phone);

                expectedOffice.dESx_OfficeElectronicAddresses.FirstOrDefault(
                       a => a.dESx_ElectronicAddress.ElectronicAddressTypeCode == ElectronicAddressType.Website)
                   .dESx_ElectronicAddress.Address.Should()
                   .Be.EqualTo(locationToAdd.Website);

                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
                actual.Should().Be.EqualTo(0);
            }


            [Test]
            public void Should_Set_New_HQ_When_IsPrimary_Is_True()
            {
                //arrange
                var locationToAdd = new CompanyLocationAdd
                {
                    Address1 = RandomData.GetString(10),
                    IsPrimary = true
                };

                var expectedLocationMentionId = RandomData.GetString(10);
                var companyKey = RandomData.GetInt();
                SetupCompanyMention(expectedLocationMentionId, companyKey);

                var existingHq = new dESx_Office
                {
                    CompanyKey = companyKey,
                    OfficeName = RandomData.GetString(10),
                    IsHeadquarters = true
                };
                _fakeEsxDbContext.dESx_Offices.Attach(existingHq);

                //act
                _service.AddCompanyLocation(expectedLocationMentionId, locationToAdd);

                //assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
                existingHq.IsHeadquarters.Should().Be.False();
            }

            [Test]
            [ExpectedException(typeof(NotFoundException))]
            public void Should_Throw_Not_Found_Exception_If_Invalid_MentionId()
            {
                //arrange

                //act
                _service.AddCompanyLocation(RandomData.GetString(10), new CompanyLocationAdd());

                //assert
            }


            [Test]
            [ExpectedException(typeof(DuplicateException))]
            public void Should_Throw_Duplicate_Exception_If_Office_With_Same_Address_Exists()
            {
                //arrange
                var locationToAdd = new CompanyLocationAdd
                {
                    Address1 = RandomData.GetString(10),
                    Address2 = RandomData.GetString(10),
                    Address3 = RandomData.GetString(10),
                    Address4 = RandomData.GetString(10),
                    City = RandomData.GetString(10),
                    State = RandomData.GetString(10),
                    Zip = RandomData.GetString(10),
                    CountryKey = RandomData.GetInt(),
                };

                var officeKey = RandomData.GetInt();
                var addressKey = RandomData.GetInt();
                _fakeEsxDbContext.dESx_Offices.Attach(new dESx_Office
                {
                    OfficeKey = officeKey,
                    PhysicalAddressKey = addressKey
                });
                _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(new dESx_PhysicalAddress
                {
                    PhysicalAddressKey = addressKey,
                    Address1 = locationToAdd.Address1,
                    Address2 = locationToAdd.Address2,
                    Address3 = locationToAdd.Address3,
                    Address4 = locationToAdd.Address4,
                    City = locationToAdd.City,
                    StateProvidenceCode = locationToAdd.State,
                    PostalCode = locationToAdd.Zip,
                    CountryKey = locationToAdd.CountryKey,
                });

                var mentionId = RandomData.GetString(10);
                SetupCompanyMention(mentionId, RandomData.GetInt());

                //act
                _service.AddCompanyLocation(mentionId, locationToAdd);

            }
        }

        public sealed class AddAlias : CompaniesServiceTests
        {
            [Test]
            public void Should_Add_Company_Alias_Successfully()
            {
                //Arrange
                var companyKey = RandomData.GetInt();
                var model = new Alias
                {
                    CompanyKey = companyKey,
                    AliasName = RandomData.GetString(20)
                };

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = companyKey
                });

                //Act
                _service.AddAlias(model);

                //Assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }

            [Test]
            public void Should_Add_Company_Primary_Alias_Successfully()
            {
                //Arrange
                var companyKey = RandomData.GetInt();

                var model = new Alias
                {
                    CompanyKey = companyKey,
                    AliasName = RandomData.GetString(20),
                    IsPrimary = true
                };

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = companyKey
                });

                //Act
                _service.AddAlias(model);

                //Assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }

            [Test]
            [ExpectedException(typeof(NotFoundException))]
            public void Should_Should_Fail_If_Company_Not_Found()
            {
                //Arrange
                var companyKey = RandomData.GetInt();

                var model = new Alias
                {
                    CompanyKey = companyKey,
                    AliasName = RandomData.GetString(20)
                };

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = RandomData.GetInt()
                });

                //Act
                _service.AddAlias(model);
            }
        }

        public sealed class UpdateCompany : CompaniesServiceTests
        {
            [Test]
            public void Should_Update_Company_Profile_Successfully()
            {
                //Arrange
                var companyKey = RandomData.GetInt();
                var parentCompanyKey = RandomData.GetInt();
                var companyName = RandomData.GetString(20);
                var aliasKey = RandomData.GetInt();
                var companyHierarchyKey = RandomData.GetInt();

                CompanyDetails details = new CompanyDetails
                {
                    CompanyKey = companyKey,
                    CompanyName = companyName,
                    IsSolicitable = true,
                    FcpaName = RandomData.GetString(10),
                    IsOfacCheck = true,
                    RealEstateAllocation = RandomData.GetDecimal(),
                    HasWfsCorrelation = false,
                    CibosCode = RandomData.GetString(10),
                    IsForeignGovernment = false,
                    IsTrackedByAsiaTeam = false,
                    HasRestrictedVisibility = false,
                    HasProfileChanged = true,
                    Aliases = new List<Alias>
                    {
                        new Alias
                        {
                            CompanyKey = companyKey,
                            IsPrimary = true,
                            AliasKey = aliasKey,
                            AliasName = RandomData.GetString(10)
                        }
                    }
                };

                //TODO: rework with new db model
                //_fakeEsxDbContext.dESx_Aliases.Attach(new dESx_Alias
                //{
                //    CompanyKey = companyKey,
                //    IsPrimary = true,
                //    AliasKey = aliasKey
                //});

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = companyKey
                });

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = parentCompanyKey
                });

                _fakeEsxDbContext.dESx_CompanyHierarchies.Attach(new dESx_CompanyHierarchy
                {
                    CompanyHierarchyKey = companyHierarchyKey,
                    ChildCompanyKey = companyKey,
                    ParentCompanyKey = parentCompanyKey
                });

                //Act
                _service.UpdateCompany(details);

                //Assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }

            [Test]
            public void Should_Update_Parent_Company()
            {
                //Arrange
                var companyKey = RandomData.GetInt();
                var parentCompanyKey = RandomData.GetInt();
                var companyHierarchyKey = RandomData.GetInt();

                CompanyDetails details = new CompanyDetails
                {
                    CompanyKey = companyKey,
                    ParentCompany = new ParentCompany
                    {
                        CompanyHierarchyKey = companyHierarchyKey,
                        ChildCompanyKey = companyKey,
                        ParentCompanyKey = parentCompanyKey
                    },
                    HasProfileChanged = true
                };

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = companyKey
                });

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = parentCompanyKey
                });

                _fakeEsxDbContext.dESx_CompanyHierarchies.Attach(new dESx_CompanyHierarchy
                {
                    CompanyHierarchyKey = companyHierarchyKey,
                    ChildCompanyKey = companyKey,
                    ParentCompanyKey = parentCompanyKey
                });

                //Act
                _service.UpdateCompany(details);

                //Assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }

            [Test]
            public void Should_Add_Parent_Company()
            {
                //Arrange
                var companyKey = RandomData.GetInt();
                var parentCompanyKey = RandomData.GetInt();
                var companyHierarchyKey = RandomData.GetInt();

                CompanyDetails details = new CompanyDetails
                {
                    CompanyKey = companyKey,
                    ParentCompany = new ParentCompany
                    {
                        CompanyHierarchyKey = 0,
                        ChildCompanyKey = companyKey,
                        ParentCompanyKey = parentCompanyKey
                    },
                    HasProfileChanged = true
                };

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = companyKey
                });

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = parentCompanyKey
                });

                _fakeEsxDbContext.dESx_CompanyHierarchies.Attach(new dESx_CompanyHierarchy
                {
                    CompanyHierarchyKey = companyHierarchyKey,
                    ChildCompanyKey = companyKey,
                    ParentCompanyKey = parentCompanyKey
                });

                //Act
                _service.UpdateCompany(details);

                //Assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }

            [Test]
            public void Should_Delete_Parent_Company()
            {
                //Arrange
                var companyKey = RandomData.GetInt();
                var parentCompanyKey = RandomData.GetInt();
                var companyHierarchyKey = RandomData.GetInt();

                CompanyDetails details = new CompanyDetails
                {
                    CompanyKey = companyKey,
                    ParentCompany = new ParentCompany
                    {
                        CompanyHierarchyKey = companyHierarchyKey,
                        ChildCompanyKey = companyKey,
                        ParentCompanyKey = 0
                    },
                    HasProfileChanged = true
                };

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = companyKey
                });

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = parentCompanyKey
                });

                _fakeEsxDbContext.dESx_CompanyHierarchies.Attach(new dESx_CompanyHierarchy
                {
                    CompanyHierarchyKey = companyHierarchyKey,
                    ChildCompanyKey = companyKey,
                    ParentCompanyKey = parentCompanyKey
                });

                //Act
                _service.UpdateCompany(details);

                //Assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }

            [Test]
            public void Should_Update_Company_Compliance_Successfully()
            {
                //Arrange
                var companyKey = RandomData.GetInt();
                var companyName = RandomData.GetString(20);

                CompanyDetails details = new CompanyDetails
                {
                    CompanyKey = companyKey,
                    CompanyName = companyName,
                    IsSolicitable = true,
                    FcpaName = RandomData.GetString(10),
                    IsOfacCheck = true,
                    RealEstateAllocation = RandomData.GetDecimal(),
                    HasWfsCorrelation = false,
                    CibosCode = RandomData.GetString(10),
                    IsForeignGovernment = false,
                    IsTrackedByAsiaTeam = false,
                    HasRestrictedVisibility = false,
                    HasProfileChanged = false
                };

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = companyKey
                });

                //Act
                _service.UpdateCompany(details);

                //Assert
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }

            [Test]
            [ExpectedException(typeof(NotFoundException))]
            public void Should_Fail_If_Company_Not_Found()
            {
                //Arrange
                var companyKey = RandomData.GetInt();

                CompanyDetails details = new CompanyDetails
                {
                    CompanyKey = companyKey
                };

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = RandomData.GetInt()
                });

                //Act
                _service.UpdateCompany(details);
            }
        }

        public sealed class GetCompanyDetails : CompaniesServiceTests
        {
            private void MockData(string companyMentionId, string companyName)
            {
                var companyMentionKey = RandomData.GetInt();
                var aliasKey = RandomData.GetInt();
                var companyKey = RandomData.GetInt();
                var officeKey = RandomData.GetInt();
                var physicalAddressKey = RandomData.GetInt();
                var countryKey = RandomData.GetInt();
                var electronicAddressKey = RandomData.GetInt();
                var parentCompanyKey = RandomData.GetInt();
                var parentAliasKey = RandomData.GetInt();

                _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    MentionId = companyMentionId,
                    MentionKey = companyMentionKey,
                    AliasKey = aliasKey,

                    IsPrimary = true
                });

                _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    MentionId = RandomData.GetString(10),
                    MentionKey = RandomData.GetInt(),
                    AliasKey = parentAliasKey
                });


                _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias()
                {
                    CompanyAliasKey = aliasKey,
                    CompanyKey = companyKey,
                    AliasName = companyName,
                    IsPrimary = true
                });

                _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                {
                    CompanyAliasKey = parentAliasKey,
                    CompanyKey = parentCompanyKey,
                    AliasName = RandomData.GetString(20),
                    IsPrimary = true
                });

                _fakeEsxDbContext.dESx_CompanyHierarchies.Attach(new dESx_CompanyHierarchy
                {
                    CompanyHierarchyKey = RandomData.GetInt(),
                    ChildCompanyKey = companyKey,
                    ParentCompanyKey = parentCompanyKey
                });

                _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                {
                    CompanyAliasKey = RandomData.GetInt(),
                    CompanyKey = companyKey,
                    AliasName = RandomData.GetString(20),
                    IsPrimary = false
                });

                _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                {
                    CompanyAliasKey = RandomData.GetInt(),
                    CompanyKey = companyKey,
                    AliasName = RandomData.GetString(20),
                    IsPrimary = false
                });

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = companyKey
                });

                _fakeEsxDbContext.dESx_Offices.Attach(new dESx_Office
                {
                    OfficeKey = officeKey,
                    CompanyKey = companyKey,
                    OfficeName = RandomData.GetString(10),
                    IsHeadquarters = true,
                    PhysicalAddressKey = physicalAddressKey
                });

                _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(new dESx_PhysicalAddress
                {
                    PhysicalAddressKey = physicalAddressKey,
                    CountryKey = countryKey,
                    Address1 = RandomData.GetString(10)
                });

                _fakeEsxDbContext.dESx_Countries.Attach(new dESx_Country
                {
                    CountryKey = countryKey,
                    CountryName = RandomData.GetString(10)
                });

                _fakeEsxDbContext.dESx_CompanyElectronicAddresses.Attach(new dESx_CompanyElectronicAddress
                {
                    ElectronicAddressKey = electronicAddressKey,
                    CompanyKey = companyKey,
                    CompanyElectronicAddressKey = RandomData.GetInt()
                });

                _fakeEsxDbContext.dESx_ElectronicAddresses.Attach(new dESx_ElectronicAddress
                {
                    ElectronicAddressKey = electronicAddressKey,
                    ElectronicAddressTypeCode = RandomData.GetString(5),
                });

            }

            [Test]
            public void Should_Get_Company_Details_With_MentionId()
            {
                //arrange
                var companyMentionId = RandomData.GetString(10);
                var expectedCompanyName = "test company";

                MockData(companyMentionId, expectedCompanyName);

                //act
                var actual = _service.GetCompanyDetails(companyMentionId);

                //assert
                actual.Satisfies(s => s.CompanyName == expectedCompanyName);
            }
        }

        public sealed class AddCompanyLoan : CompaniesServiceTests
        {
            [Test]
            public void Should_Add_Company_Loan_Successfully()
            {
                //Arrange
                var companyMentionId = RandomData.GetString(10);
                CreateMockCompany(companyMentionId);

                var model = new CompanyLoanAdd
                {
                    LoanSourceType = RandomData.GetString(5),
                    LoanRateType = RandomData.GetString(5),
                    LoanBenchmarkType = RandomData.GetString(3),
                    LoanCashMgmtType = RandomData.GetString(2),
                    LoanPaymentType = RandomData.GetString(6),
                    LoanName = RandomData.GetString(10),
                    IsBorrower = false,
                    BorrowerLenderCompanyKey = RandomData.GetInt(),
                    LoanAmount = RandomData.GetDecimal(),
                    OriginationDate = RandomData.GetDateTime()
                };

                //Act
                var result = _service.AddCompanyLoan(companyMentionId, model);

                //Assert
                result.PropertyName.Should().Be.EqualTo(model.PropertyName);
            }

            [Test]
            [ExpectedException(typeof(NotFoundException))]
            public void Should_Should_Fail_If_Company_Not_Found()
            {
                //Arrange

                var model = new CompanyLoanAdd
                {
                    LoanSourceType = RandomData.GetString(5),
                    LoanRateType = RandomData.GetString(5),
                    LoanBenchmarkType = RandomData.GetString(3),
                    LoanCashMgmtType = RandomData.GetString(2),
                    LoanPaymentType = RandomData.GetString(6),
                    LoanName = RandomData.GetString(10),
                    BorrowerLenderCompanyKey = RandomData.GetInt(),
                    LoanAmount = RandomData.GetDecimal(),
                    OriginationDate = RandomData.GetDateTime()
                };

                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = RandomData.GetInt()
                });

                //Act
                _service.AddCompanyLoan(It.IsAny<string>(), model);
            }
            [Test]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Should_Throw_ArgumentNullException_When_Any_LoanTypeCode_Is_Null_Or_Empty()
            {
                //Arrange
                var companyMentionId = RandomData.GetString(10);
                CreateMockCompany(companyMentionId);

                CompanyLoanAdd model = new CompanyLoanAdd
                {
                    LoanCashMgmtType = RandomData.GetString(2),
                    LoanPaymentType = RandomData.GetString(6),
                    LoanName = RandomData.GetString(10),
                    IsBorrower = false,
                    BorrowerLenderCompanyKey = RandomData.GetInt(),
                    LoanAmount = RandomData.GetDecimal(),
                    OriginationDate = RandomData.GetDateTime()
                };

                //Act
                _service.AddCompanyLoan(companyMentionId, model);
            }
            private void CreateMockCompany(string companyMentionId)
            {
                var companyKey = RandomData.GetInt();


                _fakeEsxDbContext.dESx_Companies.Attach(new dESx_Company
                {
                    CompanyKey = companyKey
                });

                var expectedAliasKey = RandomData.GetInt();

                _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    MentionId = companyMentionId,
                    AliasKey = expectedAliasKey,

                });

                _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                {
                    CompanyAliasKey = expectedAliasKey,
                    CompanyKey = companyKey
                });
            }
        }

        protected dESx_Company SetupCompanyMention(string mentionId, int companyKey)
        {
            var expectedAliasKey = RandomData.GetInt();

            _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
            {
                MentionId = mentionId,
                AliasKey = expectedAliasKey,

            });
            _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
            {
                CompanyAliasKey = expectedAliasKey,
                CompanyKey = companyKey
            });
            var company = new dESx_Company
            {
                CompanyKey = companyKey
            };
            _fakeEsxDbContext.dESx_Companies.Attach(company);
            return company;
        }

    }
}

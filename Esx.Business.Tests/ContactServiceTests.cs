﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Esx.Business.Contacts;
using Esx.Contracts.Models;
using Esx.Data;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Contact;
using Esx.Data.Repositories;
using Esx.Business.Search;
using Esx.Contracts.Models.Search;

namespace Esx.Business.Tests
{
    public abstract class ContactServiceTests
    {
        private FakeEsxDbContext _fakeEsxDbContext;
        private Mock<ICompaniesRepository> _companiesRepoMock;
        private ContactsService _service;
        private Mock<ICompanyCriteriaSetService> _companyCriteriaSetServiceMock;
        private Mock<IDealCriteriaSetService> _dealCriteriaSetServiceMock;
        private Mock<IContactCriteriaSetService> _contactCriteriaSetServiceMock;

        [SetUp]
        public void TestInit()
        {
            // Configure mappings
            Mapper.AddProfile(new EsxBusinessMapperConfiguration());

            _fakeEsxDbContext = new FakeEsxDbContext();
            _companiesRepoMock = new Mock<ICompaniesRepository>();
            _companyCriteriaSetServiceMock = new Mock<ICompanyCriteriaSetService>();
            _dealCriteriaSetServiceMock = new Mock<IDealCriteriaSetService>();
            _contactCriteriaSetServiceMock = new Mock<IContactCriteriaSetService>();

            _service = new ContactsService(
                _companiesRepoMock.Object,
                _fakeEsxDbContext,
                _dealCriteriaSetServiceMock.Object,
                _contactCriteriaSetServiceMock.Object,
                _companyCriteriaSetServiceMock.Object
                );
        }

        [TearDown]
        public void TestDispose()
        {
            // Clear mappings
            Mapper.Reset();
        }

        private List<ContactDetails> MockContactData()
        {
            return MockContactData(string.Empty);
        }
        // ReSharper disable once UnusedMethodReturnValue.Local
        private List<ContactDetails> MockContactData(ContactDetails contactDetails)
        {
            return MockContactData(string.Empty, contactDetails);
        }

        private List<ContactDetails> MockContactData(string searchText, ContactDetails contactDetails = null)
        {
            var expectedContactList = new List<ContactDetails>();
            var count = 10;
            var lastName = !string.IsNullOrEmpty(searchText) ? searchText : RandomData.GetString(20);
            var contactKey = RandomData.GetInt();

            var address = RandomData.GetString(20);
            var city = RandomData.GetString(10);

            if (contactDetails != null)
            {
                contactKey = contactDetails.ContactId;
                lastName = contactDetails.LastName;
                address = contactDetails.Address;
                city = contactDetails.City;
            }

            while (count-- > 0)
            {
                var contact = new dESx_Contact
                {
                    ContactKey = RandomData.GetInt(),
                    LastName = (count == 2 || count == 3) ? lastName : RandomData.GetString(20),
                    Title = RandomData.GetString(15)
                };
                if (count == 1)
                {
                    contact.ContactKey = contactKey;
                    contact.LastName = lastName;
                }

                var company = new dESx_Company
                {
                    CompanyKey = RandomData.GetInt()
                };
                var alias = new dESx_CompanyAlias
                {
                    CompanyAliasKey = RandomData.GetInt(),
                    AliasName = RandomData.GetString(15),
                    CompanyKey = company.CompanyKey,
                    IsPrimary = true
                };

                var contactMention = new dESx_Mention
                {
                    ContactKey = contact.ContactKey,
                    MentionId = RandomData.GetString(10),
                    MentionKey = RandomData.GetInt()
                };

                var companyMention = new dESx_Mention
                {
                    MentionId = RandomData.GetString(10),
                    AliasKey = alias.CompanyAliasKey,
                    MentionKey = RandomData.GetInt()
                };

                var physicalAddress = new dESx_PhysicalAddress
                {
                    PhysicalAddressKey = RandomData.GetInt(),
                    PhysicalAddressTypeCode = RandomData.GetString(8),
                    Address1 = count == 1 ? address : RandomData.GetString(20),
                    City = count == 1 ? city : RandomData.GetString(10),
                    CountryKey = RandomData.GetInt()
                };
                var office = new dESx_Office
                {
                    OfficeKey = RandomData.GetInt(),
                    CompanyKey = company.CompanyKey,
                    PhysicalAddressKey = physicalAddress.PhysicalAddressKey
                };
                var contactOffice = new dESx_ContactOffice
                {
                    ContactOfficeKey = RandomData.GetInt(),
                    OfficeKey = office.OfficeKey,
                    ContactKey = contact.ContactKey,
                    ContactOfficeRoleCode = ContactOfficeRole.HomeOffice,
                    CreatedDate = DateTime.Now,
                    IsCurrent = true
                };

                var userMentionData = new dESx_UserMentionData
                {
                    MentionKey = contactMention.MentionKey
                };

                expectedContactList.Add(new ContactDetails
                {
                    LastName = contact.LastName,
                    Title = contact.Title,
                    ContactId = contact.ContactKey,
                    CompanyName = alias.AliasName
                });

                _fakeEsxDbContext.dESx_Contacts.Attach(contact);
                _fakeEsxDbContext.dESx_Companies.Attach(company);
                _fakeEsxDbContext.dESx_CompanyAlias.Attach(alias);
                _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(physicalAddress);
                _fakeEsxDbContext.dESx_Offices.Attach(office);
                _fakeEsxDbContext.dESx_ContactOffices.Attach(contactOffice);
                _fakeEsxDbContext.dESx_Mentions.Attach(companyMention);
                _fakeEsxDbContext.dESx_Mentions.Attach(contactMention);
                _fakeEsxDbContext.dESx_UserMentionDatas.Attach(userMentionData);

            }

            return expectedContactList.OrderBy(c => c.FullName).ToList();
        }

        public sealed class GetContactList : ContactServiceTests
        {

            [Test]
            public void Should_Get_List_Of_Contacts()
            {
                //arrange
                var expectedContactList = MockContactData();
                var expected = expectedContactList.OrderByDescending(a => a.IsWatched).ThenBy(a => a.LastName).Select(a => a.LastName);

                //act
                var actualContactItemList = _service.Search(new ContactSearchCriteria { SearchText = "" }).Resources;

                //assert
                actualContactItemList.Select(a => a.LastName).Should().Have.SameSequenceAs(expected);
            }
            [Test]
            public void Should_Paged_List_Of_Contacts()
            {
                //arrange
                MockContactData();

                //act
                var actualContactItemList = _service.Search(new ContactSearchCriteria { PageSize = 5, SearchText = "" }).Resources;

                //assert
                actualContactItemList.Count.Should().Be(5);
            }

            private static IEnumerable<string> FieldNames => new[] { "lastName", "isWatched", "title", "companyName", "mobilePhone", "emailAddress", "location", "contactMentionId" };

            [TestCaseSource(nameof(FieldNames))]
            public void Should_Sort_List_Of_Contacts_By_FieldName(string sortField)
            {
                //arrange
                MockContactData();

                //act
                var paging = new ContactSearchCriteria { Sort = sortField, SearchText = "" };
                var actualContactItemList = _service.Search(paging).Resources;

                //assert
                actualContactItemList.Select(a => a.LastName).Should().Have.SameSequenceAs(
                    actualContactItemList.OrderBy(a => sortField).Select(a => a.LastName)
                  );
            }

            [TestCaseSource(nameof(FieldNames))]
            public void Should_Sort_List_Of_Contacts_By_FieldName_Descending(string sortField)
            {
                //arrange
                MockContactData();

                //act
                var paging = new ContactSearchCriteria { Sort = sortField, SortDescending = true, SearchText = "" };
                var actualContactItemList = _service.Search(paging).Resources;

                //assert
                actualContactItemList.Select(a => a.LastName).Should().Have.SameSequenceAs(
                    actualContactItemList.OrderByDescending(a => sortField).Select(a => a.LastName)
                    );
            }


            [Test]
            public void Should_Get_List_Of_Contacts_With_SearchText()
            {
                //arrange
                var expectedLastName = "John Johnson";
                var expectedContactList = MockContactData(expectedLastName).Where(l => l.LastName.Contains(expectedLastName));

                //act
                var actualContactItemList = _service.Search(new ContactSearchCriteria { SearchText = expectedLastName }).Resources;

                //assert 
                actualContactItemList.Select(i => i.LastName).Should().Have.SameSequenceAs(
                    expectedContactList.Select(a => a.LastName));
            }
        }

        public sealed class GetContactSummary : ContactServiceTests
        {

            //TODO add tests to verify statistics are set. 

            [Test]
            public void Should_Get_Contact_Details()
            {
                // Arrange
                const int contactId = 82624;
                var pitch = RandomData.GetInt(0, 10);
                var bid = RandomData.GetInt(0, 10);
                var hold = RandomData.GetInt(0, 10);
                var loan = RandomData.GetInt(0, 10);

                // Act
                var expectedContactDetails = new ContactDetails()
                {
                    ContactId = contactId
                };

                MockContactData(expectedContactDetails);
                var actual = _service.GetContactSummary(contactId, pitch, bid, hold, loan);

                // Assert
                actual.Satisfies(
                 a =>
                 a.ContactId == expectedContactDetails.ContactId);

            }

        }

        public sealed class GetContactById : ContactServiceTests
        {
            [Test]
            public void Should_Get_Contact_By_ContactId()
            {
                //arrange
                var contactId = RandomData.GetInt();
                var expectedContactDetails = new ContactDetails
                {
                    ContactId = contactId,
                    LastName = RandomData.GetString(30),
                    Address = RandomData.GetString(20),
                    City = RandomData.GetString(10)
                };

                MockContactData(expectedContactDetails);

                //act
                var actual = _service.GetContactById(contactId);

                //assert
                actual.Satisfies(
                    a =>
                    a.ContactId == expectedContactDetails.ContactId &&
                    a.FullName == expectedContactDetails.FullName &&
                    a.Address == expectedContactDetails.Address &&
                    a.MobilePhone == expectedContactDetails.MobilePhone &&
                    a.City == expectedContactDetails.City
                    );
            }
        }

       
    }
}

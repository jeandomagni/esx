﻿using System.Linq;
using Esx.Business.Resources;
using Esx.Business.Users;
using Esx.Contracts.Models;
using Esx.Data;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Business.Tests
{
    public class ResourcesServiceTests
    {
        private FakeEsxDbContext _fakeEsxDbContext;
        private ResourcesService _service;

        [SetUp]
        public void TestInit()
        {
            _fakeEsxDbContext = new FakeEsxDbContext();
            _service = new ResourcesService(_fakeEsxDbContext);
        }

        [TearDown]
        public void TestDispose()
        {
            _fakeEsxDbContext.Dispose();
        }

        public sealed class GetStateProvinceList : ResourcesServiceTests
        {
            [Test]
            public void Should_Get_StateProvince_List_Successfully()
            {
                // arrange
                string state = "MN";                

                _fakeEsxDbContext.dESx_StateProvidences.Attach(new dESx_StateProvidence
                {
                    StateProvidenceCode = state,
                    StateProvidenceName = state
                });

                // act
                var actual = _service.GetStateProvinceList();

                // assert
                actual.First().StateProvinceCode.Should().Be(state);
                actual.First().StateProvinceName.Should().Be(state);
            }
        }

        public sealed class GetCountryList : ResourcesServiceTests
        {
            [Test]
            public void Should_Get_Country_List_Successfully()
            {
                // arrange                
                string country = "United States";

                _fakeEsxDbContext.dESx_Countries.Attach(new dESx_Country
                {
                    CountryName = country
                });
                // act
                var actual = _service.GetCountryList();

                // assert
                actual.First().CountryName.Should().Be(country);
            }
        }
    }
}

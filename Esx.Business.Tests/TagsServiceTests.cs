﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Esx.Business.Tags;
using Esx.Contracts.Models.Tag;
using Esx.Data;
using Esx.Data.Repositories;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Business.Tests
{
    public class TagsServiceTests
    {

        private Mock<ITagsRepository> _tagsRepositoryMock;
        private TagsService _service;

        [SetUp]
        public void TestInit()
        {
            Mapper.AddProfile(new EsxBusinessMapperConfiguration());

            _tagsRepositoryMock = new Mock<ITagsRepository>();

            _service = new TagsService(_tagsRepositoryMock.Object, Mapper.Engine);
        }

        [TearDown]
        public void TestDispose()
        {
            // clear mappings
            Mapper.Reset();
        }

        public sealed class SearchForTags : TagsServiceTests
        {
            [Test]
            public void Should_Search_for_Tags_With_SearchText_And_Return_Tags()
            {
                // arrange
                var tagName = RandomData.GetString(10);
                var pageSize = RandomData.GetInt(0, 100);

                var returnList = new List<Tag>
                {
                    new Tag {Relevance = 1, TagName = RandomData.GetString(10)},
                    new Tag {Relevance = 5, TagName = RandomData.GetString(10)},
                    new Tag {Relevance = 3, TagName = RandomData.GetString(10)}
                };

                _tagsRepositoryMock.Setup(t => t.GetTags(tagName, pageSize)).Returns(returnList);

                // act
                var actual = _service.GetTags(tagName, pageSize);

                // assert
                actual.Select(a => a.TagName).Should().Have.SameSequenceAs(returnList.Select(r => r.TagName));
                _tagsRepositoryMock.VerifyAll();
            }
        }

        public sealed class GetTagNotes : TagsServiceTests
        {
            [Test]
            public void Should_Get_Notes_That_Contain_Tag()
            {
                // arrange
                var tagName = RandomData.GetString(10);
                var pageSize = RandomData.GetInt(0, 100);
                var offSet = RandomData.GetInt(0, 100);

                var returnList = new List<TagNote>
                {
                    new TagNote
                    {
                        CommentID = RandomData.GetInt(0,10),
                        CreateTime = RandomData.GetDateTime(),
                        UpdateTime = RandomData.GetDateTime(),
                        CommentText = RandomData.GetString(1000)
                    },

                    new TagNote
                    {
                        CommentID = RandomData.GetInt(0,10),
                        CreateTime = RandomData.GetDateTime(),
                        UpdateTime = RandomData.GetDateTime(),
                        CommentText = RandomData.GetString(1000)
                    },

                    new TagNote
                    {
                        CommentID = RandomData.GetInt(0,10),
                        CreateTime = RandomData.GetDateTime(),
                        UpdateTime = RandomData.GetDateTime(),
                        CommentText = RandomData.GetString(1000)
                    }
                };

                _tagsRepositoryMock.Setup(t => t.GetTagNotes(tagName, pageSize, offSet)).Returns(returnList);

                // act
                var actual = _service.GetTagNotes(tagName, pageSize, offSet);

                // assert
                actual.Select(a => a.CommentID).Should().Have.SameSequenceAs(returnList.Select(r => r.CommentID));
                _tagsRepositoryMock.VerifyAll();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Esx.Business.Deals;
using Esx.Business.Search;
using Esx.Contracts.Enums;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Property;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;
using Esx.Contracts.Security;
using Esx.Data;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;
using System.Linq.Dynamic;
using Esx.Contracts.Models.Deal;

namespace Esx.Business.Tests
{
    public abstract class DealsServiceTests
    {
        private DealsService _service;
        private FakeEsxDbContext _fakeEsxDbContext;
        private Mock<ISecurityContext> _securityContextMock;
        private Mock<IDealCriteriaSetService> _dealCriteriaSetServiceMock;
        private Mock<IPropertyCriteriaSetService> _propertyCriteriaSetServiceMock;
        private Mock<ILoanCriteriaSetService> _loanCriteriaSetServiceMock;
        private Mock<ICompanyCriteriaSetService> _companyCriteriaSetServiceMock;

        [SetUp]
        public void TestInit()
        {
            _fakeEsxDbContext = new FakeEsxDbContext();
            _securityContextMock = new Mock<ISecurityContext>();
            _dealCriteriaSetServiceMock = new Mock<IDealCriteriaSetService>();
            _propertyCriteriaSetServiceMock = new Mock<IPropertyCriteriaSetService>();
            _loanCriteriaSetServiceMock = new Mock<ILoanCriteriaSetService>();
            _companyCriteriaSetServiceMock = new Mock<ICompanyCriteriaSetService>();

            _service = new DealsService(
                _fakeEsxDbContext,
                _securityContextMock.Object,
                _dealCriteriaSetServiceMock.Object,
                _propertyCriteriaSetServiceMock.Object,
                _loanCriteriaSetServiceMock.Object,
                _companyCriteriaSetServiceMock.Object);
        }

        [TearDown]
        public void TestDispose()
        {
        }

        public sealed class Search : DealsServiceTests
        {
            [Test]
            public void Should_Get_Deals()
            {
                const string dealName = "Test Loan";
                const string dealStatus = "Active";
                const decimal valuationAmount = 320000m;

                // arrange          
                SetupDealsTest(dealName, dealStatus, valuationAmount);

                // act
                var actual = _service.Search(new DealSearchCriteria { SearchText = "" });

                // assert
                actual.Should().Not.Be.Null();
                actual.PagingData.TotalCount.Should().Be(1);
                actual.Resources.First().DealName.Satisfies(d => d == dealName);
                actual.Resources.First().DealStatus.Satisfies(d => d == dealStatus);
                actual.Resources.First().Valuation.Satisfies(d => d == valuationAmount);
            }

            [Test]
            public void Should_Get_Deals_With_Sorting_And_Paging()
            {

                // arrange          
                const string mentionId = "@TestMention";
                SetupMultipleDealsTest(mentionId);

                // act
                var actual = _service.Search(new DealSearchCriteria { PageSize = 10, SortDescending = true, Sort = "DealName", SearchText = "Test Deal" });

                // assert
                actual.Should().Not.Be.Null();
                actual.PagingData.TotalCount.Should().Be(3);
                var lastDeal = MockDeals.Last();
                actual.Resources.First().DealName.Satisfies(d => d == lastDeal.Item1);
                actual.Resources.First().DealStatus.Satisfies(d => d == lastDeal.Item2);
                actual.Resources.First().Valuation.Satisfies(d => d == lastDeal.Item3);
            }

            [TestCase(false)]
            [TestCase(true)]
            public void Should_Get_Deals_SubSorting_By_DealName_When_Sorting_By_IsWatched(bool sortDescending)
            {
                // arrange          
                const string mentionId = "@TestMention";
                SetupMultipleDealsTest(mentionId);
                var propertyDealList = (from d in MockDeals
                                        select new DealListItem { DealName = d.Item1, DealCode = d.Item2, IsWatched = d.Item1 == MockDeals[1].Item1 });

                var expectedPropertyDealList = sortDescending ?
                    propertyDealList.OrderByDescending(a => a.IsWatched).ThenBy(a => a.DealName).Select(a => a.DealName) :
                    propertyDealList.OrderBy(a => a.IsWatched).ThenBy(a => a.DealName).Select(a => a.DealName);

                //act
                var actualPropertyDealItemList = _service.Search(new DealSearchCriteria { SortDescending = sortDescending, Sort = "IsWatched", SearchText = "" }).Resources;

                //assert
                actualPropertyDealItemList.Select(a => a.DealName).Should().Have.SameSequenceAs(expectedPropertyDealList);
            }
            [Test]
            public void Get_Property_Deals_With_Filter()
            {
                //arrange
                const string mentionId = "@TestMention";
                const string filterText = "Fake Deal";
                SetupMultipleDealsTest(mentionId);

                //actual
                var actual = _service.Search(new DealSearchCriteria { SearchText = filterText }).Resources.Single();

                //assert
                actual.Satisfies(
                    a =>
                    a.DealName == MockDeals[1].Item1 &&
                    a.DealStatus == MockDeals[1].Item2 &&
                    a.Valuation == MockDeals.Last().Item3
                    );

            }

            private void SetupDealsTest(string dealName, string dealStatus, decimal valuationAmount)
            {
                const int userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);
                var asset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset
                {
                    AssetKey = RandomData.GetInt(),
                    AssetTypeCode = AssetType.Property.Code,
                    PhysicalAddressKey = RandomData.GetInt()
                });
                var dealAsset = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset
                {
                    AssetKey = asset.AssetKey,
                    DealKey = RandomData.GetInt(),
                    DealAssetKey = RandomData.GetInt()
                });
                var deal = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal
                {
                    DealKey = dealAsset.DealKey,
                    DealName = dealName,
                    DealCode = "TEST"
                });
                var dealStatusLog = _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog
                {
                    IsCurrent = true,
                    DealStatusCode = dealStatus,
                    CreatedDate = RandomData.GetDateTime(),
                    DealKey = dealAsset.DealKey
                });
                var dealMention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    DealKey = deal.DealKey,
                    MentionKey = RandomData.GetInt()
                });

                var mention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    IsPrimary = true,
                    MentionKey = RandomData.GetInt(),
                    PropertyAssetNameKey = RandomData.GetInt(),
                    MentionId = RandomData.GetString(20)
                });
                var assetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName
                {
                    AssetNameKey = mention.PropertyAssetNameKey.GetValueOrDefault(),
                    AssetKey = asset.AssetKey,
                    AssetName = RandomData.GetString(20),
                    IsCurrent = true
                });
                _fakeEsxDbContext.dESx_DealValues.Attach(new dESx_DealValue
                {
                    DealKey = dealAsset.DealKey,
                    Value = valuationAmount
                });
                var physicalAddress = _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(new dESx_PhysicalAddress
                {
                    PhysicalAddressKey = asset.PhysicalAddressKey.GetValueOrDefault(),
                    City = "City",
                    StateProvidenceCode = "MN"
                });
                var userMentionData = _fakeEsxDbContext.dESx_UserMentionDatas.Attach(new dESx_UserMentionData
                {
                    MentionKey = dealMention.MentionKey,
                    UserKey = userKey,
                    IsWatched = RandomData.GetInt() % 2 >= 1
                });
                var assetType = _fakeEsxDbContext.dESx_AssetTypes.Attach(new dESx_AssetType
                {
                    AssetTypeCode = AssetType.Property.Code,
                    Description = "Description"
                });
                var companyAlias = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                {
                    IsPrimary = true,
                    CompanyKey = RandomData.GetInt(),
                    AliasName = "Test Alias"
                });
                var companyAsset = _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset
                {
                    CompanyKey = companyAlias.CompanyKey,
                    AssetKey = asset.AssetKey
                });
            }

        }
        public sealed class GetDealSummary : DealsServiceTests
        {
            [Test]
            public void Should_Get_Deal_Summary_Properties()
            {
                //arrange
                const string dealName = "dealName";
                const string dealMentionId = "@sampleDealMention";

                MockDealSummary(dealName, dealMentionId);

                //actual
                var actual = _service.GetDealSummary(dealMentionId);

                //assert
                actual.Satisfies(
                        a =>
                        a.DealName == dealName &&
                        a.Properties.Count() == 1 &&
                        a.Properties.FirstOrDefault().PhysicalAddress.Country == "USA" &&
                        a.Properties.FirstOrDefault().PhysicalAddress.Address1 == "321 main st"
                        );
            }

            private void MockDealSummary(string dealName, string dealMentionId)
            {
                const int userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);

                var deal = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal
                {
                    DealKey = RandomData.GetInt(),
                    DealCode = "TEST",
                    DealName = dealName
                });

                var dealStatus = _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog
                {
                    IsCurrent = true,
                    DealKey = deal.DealKey
                });

                var mention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    DealKey = deal.DealKey,
                    MentionId = dealMentionId,
                });

                //Property
                var asset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset
                {
                    AssetKey = RandomData.GetInt(),
                    AssetTypeCode = AssetType.Property.Code,
                    PhysicalAddressKey = RandomData.GetInt(),
                });

                var assetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName
                {
                    AssetKey = asset.AssetKey,
                    IsCurrent = true,
                    AssetName = RandomData.GetString(20)
                });

                var dealAsset = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset
                {
                    AssetKey = asset.AssetKey,
                    DealKey = deal.DealKey,
                    DealAssetKey = RandomData.GetInt()
                });

                var physicalAddress = _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(new dESx_PhysicalAddress
                {
                    PhysicalAddressKey = asset.PhysicalAddressKey.GetValueOrDefault(),
                    City = "City",
                    StateProvidenceCode = "MN",
                    Address1 = "321 main st",
                    CountryKey = RandomData.GetInt()
                });
                var country =
                    _fakeEsxDbContext.dESx_Countries.Attach(new dESx_Country
                    {
                        CountryKey = physicalAddress.CountryKey,
                        CountryName = "USA"
                    });

                _fakeEsxDbContext.dESx_DealValues.Attach(new dESx_DealValue
                {
                    DealKey = deal.DealKey,
                    Value = RandomData.GetDecimal()
                });
            }
        }

        public sealed class GetCompanyDeals : DealsServiceTests
        {
            private void MockData(string companyMentionId, string expectedName = null, string expectedCode = null, string expectedStatus = null, string expectedDealType = null)
            {
                expectedName = expectedName ?? RandomData.GetString(20);
                expectedCode = expectedCode ?? RandomData.GetString(20);
                expectedStatus = expectedStatus ?? RandomData.GetString(20);
                expectedDealType = expectedDealType ?? RandomData.GetString(2);

                for (var i = 0; i < 10; i++)
                {
                    var dealKey = RandomData.GetInt();
                    var companyMentionKey = RandomData.GetInt();
                    var dealMentionKey = RandomData.GetInt();
                    var aliasKey = RandomData.GetInt();
                    var companyKey = RandomData.GetInt();

                    string dealParticipationType = string.Empty;
                    switch (i % 4)
                    {
                        case 0:
                            dealParticipationType = DealParticipationType.Borrower.Code;
                            break;
                        case 1:
                            dealParticipationType = DealParticipationType.Buyer.Code;
                            break;
                        case 2:
                            dealParticipationType = DealParticipationType.Seller.Code;
                            break;
                        case 3:
                            dealParticipationType = DealParticipationType.Lender.Code;
                            break;
                    }

                    //company mention
                    _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                    {
                        MentionId = companyMentionId,
                        MentionKey = companyMentionKey,
                        AliasKey = aliasKey
                    });

                    //deal meantion
                    _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                    {
                        MentionId = RandomData.GetString(10),
                        MentionKey = dealMentionKey,
                        DealKey = dealKey,
                        IsPrimary = true
                    });

                    _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                    {
                        CompanyAliasKey = aliasKey,
                        CompanyKey = companyKey
                    });

                    _fakeEsxDbContext.dESx_DealCompanies.Attach(new dESx_DealCompany
                    {
                        DealKey = dealKey,
                        CompanyKey = companyKey,
                        DealParticipationTypeCode = dealParticipationType,
                    });

                    _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal
                    {
                        DealKey = dealKey,
                        DealName = i > 0 ? RandomData.GetString(10) : expectedName,
                        DealCode = i > 0 ? RandomData.GetString(5) : expectedCode,
                        DealTypeCode = i > 0 ? RandomData.GetString(5) : expectedDealType
                    });

                    _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog
                    {
                        CreatedDate = RandomData.GetDateTime(),
                        DealKey = dealKey,
                        IsCurrent = true,
                        DealStatusCode = i > 0 ? RandomData.GetString(10) : expectedStatus
                    });

                    var assetKey = RandomData.GetInt();
                    _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset
                    {
                        DealKey = dealKey,
                        AssetKey = assetKey,
                    });

                    var usageKey1 = RandomData.GetInt();
                    var usageKey2 = RandomData.GetInt();
                    _fakeEsxDbContext.dESx_AssetUsages.Attach(new dESx_AssetUsage
                    {
                        AssetKey = assetKey,
                        AssetUsageTypeKey = usageKey1,
                        IsCurrent = true,
                        IsPrimary = true
                    });
                    _fakeEsxDbContext.dESx_AssetUsages.Attach(new dESx_AssetUsage
                    {
                        AssetKey = assetKey,
                        AssetUsageTypeKey = usageKey2,
                        IsCurrent = true,
                        IsPrimary = false
                    });
                    _fakeEsxDbContext.dESx_AssetUsageTypes.Attach(new dESx_AssetUsageType
                    {
                        AssetUsageTypeKey = usageKey1,
                        UsageType = RandomData.GetString(20)
                    });
                    _fakeEsxDbContext.dESx_AssetUsageTypes.Attach(new dESx_AssetUsageType
                    {
                        AssetUsageTypeKey = usageKey2,
                        UsageType = RandomData.GetString(20)
                    });

                    _fakeEsxDbContext.dESx_DealValues.Attach(new dESx_DealValue
                    {
                        DealKey = dealKey,
                        Value = RandomData.GetDecimal()
                    });
                }
            }

            [Test]
            public void Should_Get_Company_Deals_With_SearchText()
            {
                //arrange
                var companyMentionId = RandomData.GetString(10);
                var expectedName = "test deal";
                var expectedCode = RandomData.GetString(20);
                var expectedStatus = RandomData.GetString(20);
                var expectedDealType = RandomData.GetString(2);
                var criteria = new DealSearchCriteria
                {
                    CompanyMentionId = companyMentionId,
                    SearchText = "test"
                };
                MockData(companyMentionId, expectedName, expectedCode, expectedStatus, expectedDealType);

                //act
                var actual = _service.Search(criteria).Resources.Single();

                //assert
                actual.Satisfies(
                    s =>
                    s.DealCode == expectedCode
                    && s.DealName == expectedName
                    && s.DealStatus == expectedStatus
                    && s.DealType == expectedDealType
                );
            }

            private static IEnumerable<string> FieldNames => new[] { "isWatched", "dealCode", "dealName", "dealMentionId", "dealType", "dealParticipationTypeCode", "dealStatus", "valuation", "propertyType", "statusDate" };

            [TestCaseSource(nameof(FieldNames))]
            public void Should_Get_Company_Deals_With_Sort(string sortColumn)
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                MockData(companyMentionId);

                var criteria = new DealSearchCriteria
                {
                    CompanyMentionId = companyMentionId,
                    Sort = sortColumn,
                    SortDescending = false
                };

                //act
                var actual = _service.Search(criteria).Resources;
                var expected = actual.OrderBy(sortColumn).Select(x => new { x.DealName, x.PropertyType });
                //assert
                actual.Select(x => new { x.DealName, x.PropertyType }).Should().Have.SameSequenceAs(expected);
            }

            [TestCaseSource(nameof(FieldNames))]
            public void Should_Get_Company_Deals_With_Sort_Descending(string sortColumn)
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                var testSortCoumn = sortColumn + " DESC";
                MockData(companyMentionId);

                var criteria = new DealSearchCriteria
                {
                    CompanyMentionId = companyMentionId,
                    Sort = sortColumn,
                    SortDescending = true
                };

                //act
                var actual = _service.Search(criteria).Resources;

                //assert
                actual.Select(x => x.DealName).Should().Have.SameSequenceAs(actual.OrderBy(testSortCoumn).Select(x => x.DealName));
            }

            [Test]
            public void Should_Get_Company_Deals_PageSize()
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                MockData(companyMentionId);

                var criteria = new DealSearchCriteria
                {
                    CompanyMentionId = companyMentionId,
                    PageSize = RandomData.GetInt(1, 9)
                };

                //act
                var actual = _service.Search(criteria).Resources;

                //assert
                actual.Satisfies(a => a.Count() == criteria.PageSize);
            }

            [Test]
            public void Should_Not_Get_Company_Deals_When_Search_Text_Not_Found()
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                MockData(companyMentionId);
                var criteria = new DealSearchCriteria
                {
                    SearchText = "WOJDLKLKSDFD",
                    CompanyMentionId = companyMentionId
                };

                //act
                var actual = _service.Search(criteria).Resources;

                //assert
                actual.Satisfies(a => !a.Any());
            }
        }

        public sealed class GetPropertyDeals : DealsServiceTests
        {
            [Test]
            public void Should_Get_Property_Deals()
            {
                // arrange   
                const string mentionId = "@TestMention";
                const string dealName = "Test Loan";
                const string dealStatus = "Active";
                const decimal valuationAmount = 320000m;
                var criteria = new DealSearchCriteria
                {
                    PropertyMentionId = mentionId
                };


                SetupDealsTest(mentionId, dealName, dealStatus, valuationAmount);

                // act
                var actual = _service.Search(criteria);

                // assert
                actual.Should().Not.Be.Null();
                actual.PagingData.TotalCount.Should().Be(1);
                actual.Resources.First().DealName.Satisfies(d => d == dealName);
                actual.Resources.First().DealStatus.Satisfies(d => d == dealStatus);
                actual.Resources.First().Valuation.Satisfies(d => d == valuationAmount);
            }

            [Test]
            public void Should_Get_Property_Deals_With_Sorting_And_Paging()
            {

                // arrange          
                const string mentionId = "@TestMention";
                var criteria = new DealSearchCriteria
                {
                    PropertyMentionId = mentionId,
                    SearchText = "Test Deal",
                    PageSize = 10,
                    SortDescending = true,
                    Sort = "DealName"
                };
                SetupMultiplePropertyDealsTest(mentionId);

                // act
                var actual = _service.Search(criteria);

                // assert
                actual.Should().Not.Be.Null();
                actual.PagingData.TotalCount.Should().Be(3);
                var lastDeal = MockPropertyDeals.Last();
                actual.Resources.First().DealName.Satisfies(d => d == lastDeal.Item1);
                actual.Resources.First().DealStatus.Satisfies(d => d == lastDeal.Item2);
                actual.Resources.First().Valuation.Satisfies(d => d == lastDeal.Item3);
            }
            //[TestCase(false)]
            //[TestCase(true)]
            //public void Should_Get_Property_Deals_SubSorting_By_DealName_When_Sorting_By_IsWatched(bool sortDescending)
            //{
            //    // arrange          
            //    const string mentionId = "@TestMention";
            //    var criteria = new DealSearchCriteria
            //    {
            //        PropertyMentionId = mentionId,
            //        SortDescending = sortDescending,
            //        Sort = "IsWatched"
            //    };
            //    SetupMultiplePropertyDealsTest(mentionId);
            //    var propertyDealList = (from d in MockPropertyDeals
            //                            select new DealListItem { DealName = d.Item1, DealCode = d.Item2, IsWatched = d.Item1 == MockPropertyDeals[1].Item1 });

            //    var expectedPropertyDealList = sortDescending ?
            //        propertyDealList.OrderByDescending(a => a.IsWatched).ThenBy(a => a.DealName).Select(a => a.DealName) :
            //        propertyDealList.OrderBy(a => a.IsWatched).ThenBy(a => a.DealName).Select(a => a.DealName);

            //    //act
            //    var actualPropertyDealItemList = _service.Search(criteria).Resources;

            //    //assert
            //    actualPropertyDealItemList.Select(a => a.DealName).Should().Have.SameSequenceAs(expectedPropertyDealList);
            //}
            [Test]
            public void Get_Property_Deals_With_Filter()
            {
                //arrange
                const string mentionId = "@TestMention";
                const string filterText = "Fake Deal";
                var criteria = new DealSearchCriteria
                {
                    PropertyMentionId = mentionId,
                    SearchText = filterText
                };
                SetupMultiplePropertyDealsTest(mentionId);

                //actual
                var actual = _service.Search(criteria).Resources.Single();

                //assert
                actual.Satisfies(
                    a =>
                    a.DealName == MockPropertyDeals[1].Item1 &&
                    a.DealStatus == MockPropertyDeals[1].Item2 &&
                    a.Valuation == MockPropertyDeals.Last().Item3
                    );

            }
            private List<Tuple<string, string, decimal>> MockPropertyDeals
            {
                get
                {
                    var deals = new List<Tuple<string, string, decimal>>
                    {
                        new Tuple<string, string, decimal>("Test Deal 1", "Active", 320000m),
                        new Tuple<string, string, decimal>("Fake Deal 2", "Inactive", 250000m),
                        new Tuple<string, string, decimal>("ATest Deal 3", "Active", 110000m),
                        new Tuple<string, string, decimal>("Test Deal 4", "Active", 153000m)
                    };
                    return deals;
                }
            }

            #region GetPropertyDeals Setup
            private void SetupMultiplePropertyDealsTest(string mentionId)
            {
                const int userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);

                var asset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset
                {
                    AssetKey = RandomData.GetInt(),
                    AssetTypeCode = AssetType.Property.Code,
                    PhysicalAddressKey = RandomData.GetInt()
                });


                var mention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    IsPrimary = true,
                    MentionKey = RandomData.GetInt(),
                    PropertyAssetNameKey = RandomData.GetInt(),
                    MentionId = mentionId
                });
                var assetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName
                {
                    AssetNameKey = mention.PropertyAssetNameKey.GetValueOrDefault(),
                    AssetKey = asset.AssetKey,
                    AssetName = RandomData.GetString(20),
                    IsCurrent = true
                });

                var physicalAddress = _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(new dESx_PhysicalAddress
                {
                    PhysicalAddressKey = asset.PhysicalAddressKey.GetValueOrDefault(),
                    City = "City",
                    StateProvidenceCode = "MN"
                });

                var assetType = _fakeEsxDbContext.dESx_AssetTypes.Attach(new dESx_AssetType
                {
                    AssetTypeCode = AssetType.Property.Code,
                    Description = "Description"
                });
                var companyAlias = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                {
                    IsPrimary = true,
                    CompanyKey = RandomData.GetInt(),
                    AliasName = "Test Alias"
                });
                var companyAsset = _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset
                {
                    CompanyKey = companyAlias.CompanyKey,
                    AssetKey = asset.AssetKey
                });

                for (var index = 0; index < MockPropertyDeals.Count; index++)
                {
                    var deal = MockPropertyDeals[index];

                    var dealAsset = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset
                    {
                        AssetKey = asset.AssetKey,
                        DealKey = RandomData.GetInt(),
                        DealAssetKey = RandomData.GetInt()
                    });

                    _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal
                    {
                        DealKey = dealAsset.DealKey,
                        DealName = deal.Item1,
                        DealCode = "TEST"
                    });

                    _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog
                    {
                        IsCurrent = true,
                        DealStatusCode = deal.Item2,
                        CreatedDate = RandomData.GetDateTime(),
                        DealKey = dealAsset.DealKey
                    });

                    var dealScenario = _fakeEsxDbContext.dESx_DealValuations.Attach(new dESx_DealValuation
                    {
                        DealKey = dealAsset.DealKey,
                        DealValuationTypeCode = DealValuationType.Basecase.Code,
                        Value = 153000m
                    });

                    _fakeEsxDbContext.dESx_DealValues.Attach(new dESx_DealValue
                    {
                        DealKey = dealAsset.DealKey,
                        Value = 153000m
                    });

                    var dealMentionKey = RandomData.GetInt(1, 1000);

                    _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                    {
                        DealKey = dealAsset.DealKey,
                        MentionKey = dealMentionKey,
                        MentionId = RandomData.GetString(10)
                    });

                    if (index == 1)
                    {
                        _fakeEsxDbContext.dESx_UserMentionDatas.Attach(new dESx_UserMentionData
                        {
                            UserMentionDataKey = RandomData.GetInt(1, 1000),
                            IsWatched = true,
                            UserKey = userKey,
                            MentionKey = dealMentionKey,
                            UseCount = 1,
                            LastUseDate = new DateTime(2015, 10, 14)
                        });
                    }
                }
            }

            private void SetupDealsTest(string mentionId, string dealName, string dealStatus, decimal valuationAmount)
            {
                const int userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);
                var asset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset
                {
                    AssetKey = RandomData.GetInt(),
                    AssetTypeCode = AssetType.Property.Code,
                    PhysicalAddressKey = RandomData.GetInt()
                });
                var dealAsset = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset
                {
                    AssetKey = asset.AssetKey,
                    DealKey = RandomData.GetInt(),
                    DealAssetKey = RandomData.GetInt()
                });
                var deal = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal
                {
                    DealKey = dealAsset.DealKey,
                    DealName = dealName,
                    DealCode = "TEST"
                });
                var dealStatusLog = _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog
                {
                    IsCurrent = true,
                    DealStatusCode = dealStatus,
                    CreatedDate = RandomData.GetDateTime(),
                    DealKey = dealAsset.DealKey
                });
                var dealMention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    DealKey = deal.DealKey,
                    MentionKey = RandomData.GetInt()
                });
                var mention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    IsPrimary = true,
                    MentionKey = RandomData.GetInt(),
                    PropertyAssetNameKey = RandomData.GetInt(),
                    MentionId = mentionId
                });
                var assetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName
                {
                    AssetNameKey = mention.PropertyAssetNameKey.GetValueOrDefault(),
                    AssetKey = asset.AssetKey,
                    AssetName = RandomData.GetString(20),
                    IsCurrent = true
                });
                var dealScenario = _fakeEsxDbContext.dESx_DealValuations.Attach(new dESx_DealValuation
                {
                    DealKey = deal.DealKey,
                    DealValuationTypeCode = DealValuationType.Basecase.Code,
                    Value = valuationAmount
                });
                _fakeEsxDbContext.dESx_DealValues.Attach(new dESx_DealValue
                {
                    DealKey = deal.DealKey,
                    Value = valuationAmount
                });
                //var assetValue = _fakeEsxDbContext.dESx_AssetValues.Attach(new dESx_AssetValue
                //{
                //    AssetValueKey = 1,
                //    AssetKey = asset.AssetKey,
                //    IsCurrent = true,
                //    AssetValue = valuationAmount,
                //    CreatedDate = DateTime.Parse("1/1/2020")
                //});
                var physicalAddress = _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(new dESx_PhysicalAddress
                {
                    PhysicalAddressKey = asset.PhysicalAddressKey.GetValueOrDefault(),
                    City = "City",
                    StateProvidenceCode = "MN"
                });
                var userMentionData = _fakeEsxDbContext.dESx_UserMentionDatas.Attach(new dESx_UserMentionData
                {
                    MentionKey = dealMention.MentionKey,
                    UserKey = userKey,
                    IsWatched = RandomData.GetInt() % 2 >= 1
                });
                var assetType = _fakeEsxDbContext.dESx_AssetTypes.Attach(new dESx_AssetType
                {
                    AssetTypeCode = AssetType.Property.Code,
                    Description = "Description"
                });
                var companyAlias = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                {
                    IsPrimary = true,
                    CompanyKey = RandomData.GetInt(),
                    AliasName = "Test Alias"
                });
                var companyAsset = _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset
                {
                    CompanyKey = companyAlias.CompanyKey,
                    AssetKey = asset.AssetKey
                });
            }
            #endregion GetPropertyLoans Setup
        }


        private static List<Tuple<string, string, decimal>> MockDeals
        {
            get
            {
                var deals = new List<Tuple<string, string, decimal>>
                    {
                        new Tuple<string, string, decimal>("Test Deal 1", "Active", 320000m),
                        new Tuple<string, string, decimal>("Fake Deal 2", "Inactive", 250000m),
                        new Tuple<string, string, decimal>("ATest Deal 3", "Active", 110000m),
                        new Tuple<string, string, decimal>("Test Deal 4", "Active", 153000m)
                    };
                return deals;
            }
        }
        private void SetupMultipleDealsTest(string mentionId)
        {
            SetupMultipleDealsTest(mentionId, String.Empty);
        }
        private void SetupMultipleDealsTest(string mentionId, string dealMentionId)
        {
            const int userKey = 1;
            _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);

            var asset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset
            {
                AssetKey = RandomData.GetInt(),
                AssetTypeCode = AssetType.Property.Code,
                PhysicalAddressKey = RandomData.GetInt()
            });


            var mention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
            {
                IsPrimary = true,
                MentionKey = RandomData.GetInt(),
                PropertyAssetNameKey = RandomData.GetInt(),
                MentionId = mentionId
            });
            var assetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName
            {
                AssetNameKey = mention.PropertyAssetNameKey.GetValueOrDefault(),
                AssetKey = asset.AssetKey,
                AssetName = RandomData.GetString(20),
                IsCurrent = true
            });

            var physicalAddress = _fakeEsxDbContext.dESx_PhysicalAddresses.Attach(new dESx_PhysicalAddress
            {
                PhysicalAddressKey = asset.PhysicalAddressKey.GetValueOrDefault(),
                City = "City",
                StateProvidenceCode = "MN"
            });

            var assetType = _fakeEsxDbContext.dESx_AssetTypes.Attach(new dESx_AssetType
            {
                AssetTypeCode = AssetType.Property.Code,
                Description = "Description"
            });
            var companyAlias = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
            {
                IsPrimary = true,
                CompanyKey = RandomData.GetInt(),
                AliasName = "Test Alias"
            });
            var companyAsset = _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset
            {
                CompanyKey = companyAlias.CompanyKey,
                AssetKey = asset.AssetKey
            });

            for (var index = 0; index < MockDeals.Count; index++)
            {
                var deal = MockDeals[index];

                var dealAsset = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset
                {
                    AssetKey = asset.AssetKey,
                    DealKey = RandomData.GetInt(),
                    DealAssetKey = RandomData.GetInt()
                });

                dealMentionId = index == 0 ? dealMentionId : RandomData.GetString(10);

                _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal
                {
                    DealKey = dealAsset.DealKey,
                    DealName = deal.Item1,
                    DealCode = "TEST"
                });

                _fakeEsxDbContext.dESx_DealStatusLogs.Attach(new dESx_DealStatusLog
                {
                    IsCurrent = true,
                    DealStatusCode = deal.Item2,
                    CreatedDate = RandomData.GetDateTime(),
                    DealKey = dealAsset.DealKey
                });

                var dealScenario = _fakeEsxDbContext.dESx_DealValuations.Attach(new dESx_DealValuation
                {
                    DealKey = dealAsset.DealKey,
                    DealValuationTypeCode = DealValuationType.Basecase.Code,
                    Value = 153000m
                });

                _fakeEsxDbContext.dESx_DealValues.Attach(new dESx_DealValue
                {
                    DealKey = dealAsset.DealKey,
                    Value = 153000m
                });

                var dealMentionKey = RandomData.GetInt(1, 1000);


                _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    DealKey = dealAsset.DealKey,
                    MentionKey = dealMentionKey,
                    MentionId = dealMentionId
                });

                if (index == 1)
                {
                    _fakeEsxDbContext.dESx_UserMentionDatas.Attach(new dESx_UserMentionData
                    {
                        UserMentionDataKey = RandomData.GetInt(1, 1000),
                        IsWatched = true,
                        UserKey = userKey,
                        MentionKey = dealMentionKey,
                        UseCount = 1,
                        LastUseDate = new DateTime(2015, 10, 14)
                    });
                }
            }
        }
    }
}

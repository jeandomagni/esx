﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Esx.Business.Mentions;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Mention;
using Esx.Data;
using Esx.Data.Repositories;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Business.Tests
{
    public abstract class MentionsServiceTests
    {

        private Mock<IMentionsRepository> _mentionsRepositoryMock;
        private MentionsService _service;
        private FakeEsxDbContext _fakeEsxDbContext;

        [SetUp]
        public void TestInit()
        {
            Mapper.AddProfile(new EsxBusinessMapperConfiguration());

            _mentionsRepositoryMock = new Mock<IMentionsRepository>();

            _fakeEsxDbContext = new FakeEsxDbContext();

            _service = new MentionsService(_mentionsRepositoryMock.Object, Mapper.Engine, _fakeEsxDbContext);
        }

        [TearDown]
        public void TestDispose()
        {
            // Clear mappings
            Mapper.Reset();
        }


        public sealed class SearchForMentions : MentionsServiceTests
        {

            [Test]
            public void Should_Search_For_Mentions_With_SearchText_And_Return_Items()
            {
                //arrange
                var query = RandomData.GetString(10);
                var count = RandomData.GetInt(0,100);
                var returnList = new List<Mention>
                {
                    new Mention { MentionID = RandomData.GetString(10), Relevance = 1},
                    new Mention { MentionID = RandomData.GetString(10), Relevance = 4},
                    new Mention { MentionID = RandomData.GetString(10), Relevance = 3},
                };

                _mentionsRepositoryMock.Setup(m => m.GetMentions(query, count)).Returns(returnList);
                       
                //act
                var actual = _service.GetMentions(query, count);

                //assert
                actual.Select(a=>a.MentionID).Should().Have.SameSequenceAs(returnList.Select(r => r.MentionID));
                _mentionsRepositoryMock.VerifyAll();
            }

        }

        public sealed class CreateNewMention : MentionsServiceTests
        {
            [Test]
            public void Should_Create_New_MentionId()
            {
                // arrange
                const string objectType = CoreType.Office;
                const int objectKey = 1;
                const bool isPrimary = true;

                var mentionKey = RandomData.GetInt();
                var mentionId = RandomData.GetString(10);

                var returnObject = new Mention
                {
                    MentionKey = mentionKey,
                    MentionID = mentionId
                };

                _mentionsRepositoryMock.Setup(m => m.CreateNewMention(objectType, objectKey, isPrimary)).Returns(returnObject);

                //act
#pragma warning disable 612, 618
                var actual = _service.CreateNewMention(objectType, objectKey, isPrimary);
#pragma warning restore 612, 618

                //assert
                actual.MentionKey.Should().Be(mentionKey);
                actual.MentionID.Should().Be(mentionId);

            }
        }

        public sealed class SetIsWatchedMethod : MentionsServiceTests
        {
            [Test]
            public void Should_Call_SetIsWatched_Repository_Method()
            {
                // Arrange
                var mentionID = "@test";
                var isWatched = true;
                _mentionsRepositoryMock.Setup(x => x.SetIsWatched(It.Is<string>(m => m == mentionID), It.Is<bool>(b => b == isWatched))).Verifiable();

                // Act
                _service.SetIsWatched(mentionID, isWatched);

                // Assert
                _mentionsRepositoryMock.VerifyAll();
            }
        }
    }
}

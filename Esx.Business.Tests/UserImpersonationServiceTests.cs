﻿using Esx.Business.UserImpersonation;
using Esx.Contracts.Models;
using Esx.Data;
using Esx.Tests.Common.Helpers;
using NUnit.Framework;
using SharpTestsEx;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Esx.Business.Tests
{
    public abstract class UserImpersonationServiceTests
    {
        private UserImpersonationService _service;
        private FakeEsxDbContext _fakeEsxDbContext;

        [SetUp]
        public void TestInit()
        {
            _fakeEsxDbContext = new FakeEsxDbContext();
            _service = new UserImpersonationService(_fakeEsxDbContext);
        }
        public sealed class GetUserList : UserImpersonationServiceTests
        {

            [Test]
            public void Should_Get_List_Of_Users_To_Be_Impersonated()
            {
                //arrange
                var expectedUsers = PrepareUserData().OrderBy( x => x);
              

                //act
                var actualUsers = _service.GetUserList();

                //assert
                expectedUsers.Should().Not.Be.Null();
                actualUsers.Should().Not.Be.Null();
                expectedUsers.Should().Have.SameSequenceAs(
                    actualUsers);
            }
            private IEnumerable<string> PrepareUserData()
            {
                List<string> ExpectedUsers = new List<string>();
                int userCount = 10;
                while (userCount-- > 0)
                {
                    var contact = new dESx_Contact
                    {
                        ContactKey = userCount % 2 == 0 ? -102 : RandomData.GetInt(),
                        LastName = RandomData.GetString(10)
                    };

                    var user = new dESx_User
                    {
                        dESx_Contact = contact,
                        UserKey = RandomData.GetInt(),
                        LoginName = RandomData.GetString(15),
                        HasAccess = true,
                        LastLoginDate = DateTime.UtcNow,
                        ContactKey = contact.ContactKey
                    };
                    _fakeEsxDbContext.dESx_Users.Add(user);
                    if(userCount % 2 == 0)
                    {
                        ExpectedUsers.Add(user.LoginName);
                    }
                }
                return ExpectedUsers;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Esx.Business.Comments;
using Esx.Contracts.Models.Comment;
using Esx.Contracts.Security;
using Esx.Data;
using Esx.Data.Repositories;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Business.Tests
{
    public class CommentsServiceTests
    {
        private Mock<ICommentsRepository> _commentsRespositoryMock;
        private Mock<ISecurityContext> _securityContextMock;
        private CommentsService _service;

        [SetUp]
        public void TestInit()
        {
            Mapper.AddProfile(new EsxBusinessMapperConfiguration());

            _commentsRespositoryMock = new Mock<ICommentsRepository>();

            _securityContextMock = new Mock<ISecurityContext>();

            _service = new CommentsService(_commentsRespositoryMock.Object, Mapper.Engine, new FakeEsxDbContext(), _securityContextMock.Object);
        }

        [TearDown]
        public void TestDispose()
        {
            Mapper.Reset();
        }

        public sealed class GetComments : CommentsServiceTests
        {
            [Test]
            public void Should_Search_For_Comments_With_Given_MentionId()
            {
                // arrange
                var mentionId = RandomData.GetString(10);
                var pageSize = RandomData.GetInt(0, 100);
                var offSet = RandomData.GetInt(0, 100);

                var returnList = new List<CommentListItem>
                {
                    new CommentListItem
                    {
                        Comment_Key = 123,
                        PrimaryMentionID = RandomData.GetString(10),
                        CommentText = RandomData.GetString(2000),
                        TableCode = RandomData.GetString(1),
                        CreateTime = RandomData.GetDateTime(),
                        UpdateTime = RandomData.GetDateTime()
                    },
                    new CommentListItem
                    {
                        Comment_Key = 456,
                        PrimaryMentionID = RandomData.GetString(10),
                        CommentText = RandomData.GetString(2000),
                        TableCode = RandomData.GetString(1),
                        CreateTime = RandomData.GetDateTime(),
                        UpdateTime = RandomData.GetDateTime()
                    },
                    new CommentListItem
                    {
                        Comment_Key = 789,
                        PrimaryMentionID = RandomData.GetString(10),
                        CommentText = RandomData.GetString(2000),
                        TableCode = RandomData.GetString(1),
                        CreateTime = RandomData.GetDateTime(),
                        UpdateTime = RandomData.GetDateTime()
                    }
                };

                _commentsRespositoryMock.Setup(t => t.GetComments(mentionId, pageSize, offSet)).Returns(returnList);

                // act
                var actual = _service.GetComments(mentionId, pageSize, offSet);

                // assert
                actual.Select(a => a.CommentText).Should().Have.SameSequenceAs(returnList.Select(r => r.CommentText));
                _commentsRespositoryMock.VerifyAll();
            }
        }

        public sealed class GetMentionComments : CommentsServiceTests
        {
            [Test]
            public void Should_Get_Comments_With_Given_MentionId()
            {
                // arrange
                var mentionId = RandomData.GetString(10);
                var pageSize = RandomData.GetInt();
                var offSet = RandomData.GetInt();
                var filterText = RandomData.GetString(10);

                var returnList = new List<CommentListItem>
                {
                    new CommentListItem
                    {
                        Comment_Key = 123,
                        PrimaryMentionID = RandomData.GetString(10),
                        CommentText = RandomData.GetString(2000),
                        TableCode = RandomData.GetString(1),
                        CreateTime = RandomData.GetDateTime(),
                        UpdateTime = RandomData.GetDateTime()
                    }
                };

                _commentsRespositoryMock.Setup(t => t.GetMentionComments(mentionId, pageSize, offSet, filterText)).Returns(returnList);

                // act
                var actual = _service.GetMentionComments(mentionId, pageSize, offSet, filterText);

                // assert
                actual.Select(a => a.CommentText).Should().Have.SameSequenceAs(returnList.Select(r => r.CommentText));
                _commentsRespositoryMock.VerifyAll();
            }
        }

        public sealed class SaveComments : CommentsServiceTests
        {
            [Test]
            [Ignore("Write tests once comments/reminders functionality is complete.")]
            public void Should_Save_Comment()
            {
                // arrange
                var tags = new List<string> { "#aTag01", "#aTag02" };
                var mentions = new List<string> { "@SomeEmployee" };
                
                var comment = new Comment()
                {
                    PrimaryMentionID = "@AnotherCompany",
                    CommentText = "This #aTag01 is a test #aTag02.",
                    Tags = tags,
                    Mentions = mentions
                };

                //act
                _service.SaveComment(comment);

                //assert
                _commentsRespositoryMock.Verify(c => c.SaveComment(comment));


            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using AutoMapper;
using Esx.Business.Alerts;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Comment;
using Esx.Contracts.Paging;
using Esx.Contracts.Security;
using Esx.Data;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Business.Tests
{
    public class AlertsServiceTests
    {
        private FakeEsxDbContext _fakeDbContext;
        private AlertsService _service;
        private Mock<ISecurityContext> _mockSecurityContext;
        private Mock<IEsxDbContext> _mockDbContext;

        [SetUp]
        public void TestInit()
        {
            Mapper.AddProfile(new EsxBusinessMapperConfiguration());
            _fakeDbContext = new FakeEsxDbContext();
            _mockSecurityContext = new Mock<ISecurityContext>();
            _mockDbContext = new Mock<IEsxDbContext>();
            _service = new AlertsService(_fakeDbContext, _mockSecurityContext.Object);
        }

        [TearDown]
        public void TestDispose()
        {
            Mapper.Reset();
        }

        public sealed class GetAlerts : AlertsServiceTests
        {
            [Test]
            public void Should_Get_Alerts_For_Given_MentionId()
            {
                // arrange
                const int userKey = 1;
                string mentionId = "@Test|E";
                _mockSecurityContext.SetupGet(x => x.UserKey).Returns(userKey);                                 
                _fakeDbContext.dESx_Notices.Attach(new dESx_Notice
                {
                    NoticeKey = 1,
                    SubjectMentionKey = 1
                });
                _fakeDbContext.dESx_Mentions.Attach(new dESx_Mention
                {
                    MentionKey = 1,
                    MentionId = mentionId
                });
                _fakeDbContext.dESx_NoticeRecipients.Attach(new dESx_NoticeRecipient
                {
                    NoticeKey = 1,
                    UserKey = userKey,
                    SnoozeUntilTime = DateTime.UtcNow.AddDays(-1)
                });
                var pagingData = new PagingData
                {
                    Offset = 0,
                    PageSize = 10
                };
                // act
                var actual = _service.GetAlerts(mentionId, pagingData);

                // assert      
                actual.Should().Not.Be.Null();
                actual.Resources.Count.Should().Be(1);
                actual.Resources.First().EntityMentionId.Should().Be(mentionId);
            }
        }


        public sealed class AcknowledgeAlert : AlertsServiceTests
        {
            [Test]
            public void Should_Acknowledge_Alert_For_AlertId()
            {
                // arrange
                const int userKey = 1;
                const int alertId = 1;
                _mockSecurityContext.SetupGet(x => x.UserKey).Returns(userKey);
                
                var noticeRecipient = new dESx_NoticeRecipient
                {
                    NoticeKey = alertId,
                    UserKey = userKey,
                    SnoozeUntilTime = DateTime.UtcNow.AddDays(-1)
                };
                
                _fakeDbContext.dESx_NoticeRecipients.Attach(noticeRecipient);
                // act
                _service.AcknowledgeAlert(alertId);

                // assert                      
                _fakeDbContext.SaveChangesCount.Should().Be(1);
            }           
        }

        public sealed class RemoveAlert : AlertsServiceTests
        {
            [Test]
            public void Should_Remove_Alert_For_AlertId()
            {
                // arrange
                const int userKey = 1;
                const int alertId = 1;
                _mockSecurityContext.SetupGet(x => x.UserKey).Returns(userKey);

                var noticeRecipient = new dESx_NoticeRecipient
                {
                    NoticeKey = alertId,
                    UserKey = userKey,
                    SnoozeUntilTime = DateTime.UtcNow.AddDays(-1)
                };

                _fakeDbContext.dESx_NoticeRecipients.Attach(noticeRecipient);
                // act
                _service.RemoveAlert(alertId);

                // assert                      
                _fakeDbContext.SaveChangesCount.Should().Be(1);
            }
        }

        public sealed class SnoozeAlert : AlertsServiceTests
        {
            [Test]
            public void Should_Snooze_Alert_For_AlertId()
            {
                // arrange
                const int userKey = 1;
                const int alertId = 1;
                _mockSecurityContext.SetupGet(x => x.UserKey).Returns(userKey);

                var noticeRecipient = new dESx_NoticeRecipient
                {
                    NoticeKey = alertId,
                    UserKey = userKey,
                    SnoozeUntilTime = DateTime.UtcNow.AddDays(-1)
                };

                _fakeDbContext.dESx_NoticeRecipients.Attach(noticeRecipient);
                // act
                _service.SnoozeAlert(alertId);

                // assert      
                noticeRecipient.Satisfies(x => x.SnoozeUntilTime > DateTime.UtcNow);
                _fakeDbContext.SaveChangesCount.Should().Be(1);
            }
        }
    }
}

﻿using System.Linq;
using Esx.Business.Users;
using Esx.Contracts.Models;
using Esx.Contracts.Security;
using Esx.Data;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Business.Tests
{
    public class UsersServiceTests
    {
        private FakeEsxDbContext _fakeEsxDbContext;
        private Mock<ISecurityContext> _securityContext;
        private UsersService _service;

        [SetUp]
        public void TestInit()
        {
            _fakeEsxDbContext = new FakeEsxDbContext();
            _securityContext = new Mock<ISecurityContext>();
            _service = new UsersService(_fakeEsxDbContext, _securityContext.Object);
        }

        [TearDown]
        public void TestDispose()
        {
            _fakeEsxDbContext.Dispose();
        }

        public sealed class GetUserPreferences : UsersServiceTests
        {
            [Test]
            public void Should_Get_User_Preferences_Successfully()
            {
                // arrange
                _securityContext.SetupGet(x => x.UserKey).Returns(1);
                var preferenceKey = "Country";
                var preferenceValue = "United States";

                _fakeEsxDbContext.dESx_UserPreferences.Attach(new dESx_UserPreference
                {
                    UserKey = 1,
                    PreferenceTypeCode = preferenceKey,
                    PreferenceValue = preferenceValue
                });

                // act
                var actual = _service.GetUserPreferences();

                // assert
                actual.First().PreferenceKey.Should().Be(preferenceKey);
                actual.First().PreferenceValue.Should().Be(preferenceValue);
            }
        }

        public sealed class SaveUserPreferences : UsersServiceTests
        {
            [Test]
            public void Should_Get_Save_Preferences_Successfully()
            {
                // arrange
                var preferenceKey = "Country";
                var preferenceValue = "United States";

                // act
                var actual = _service.SaveUserPreference(preferenceKey, preferenceValue);

                // assert
                actual.PreferenceKey.Should().Be(preferenceKey);
                actual.PreferenceValue.Should().Be(preferenceValue);
                _fakeEsxDbContext.SaveChangesCount.Should().Be(1);
            }
        }
    }
}

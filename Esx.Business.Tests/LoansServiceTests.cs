﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Moq;
using NUnit.Framework;
using SharpTestsEx;
using Esx.Business.Loans;
using Esx.Business.Search;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Loan;
using Esx.Contracts.Paging;
using Esx.Contracts.Security;
using Esx.Data;
using Esx.Tests.Common.Helpers;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Search;


namespace Esx.Business.Tests
{
    public abstract class LoansServiceTests
    {
        private LoansService _service;
        private FakeEsxDbContext _fakeEsxDbContext;
        private Mock<ISecurityContext> _securityContextMock;
        private Mock<IDealCriteriaSetService> _dealCriteriaSetServiceMock;
        private Mock<IPropertyCriteriaSetService> _propertyCriteriaSetServiceMock;
        private Mock<ILoanCriteriaSetService> _loanCriteriaSetServiceMock;
        private Mock<ICompanyCriteriaSetService> _companyCriteriaSetServiceMock;

        [SetUp]
        public void TestInit()
        {
            _fakeEsxDbContext = new FakeEsxDbContext();
            _securityContextMock = new Mock<ISecurityContext>();
            _dealCriteriaSetServiceMock = new Mock<IDealCriteriaSetService>();
            _propertyCriteriaSetServiceMock = new Mock<IPropertyCriteriaSetService>();
            _loanCriteriaSetServiceMock = new Mock<ILoanCriteriaSetService>();
            _companyCriteriaSetServiceMock = new Mock<ICompanyCriteriaSetService>();
            _service = new LoansService(
                _fakeEsxDbContext,
                _securityContextMock.Object,
                _dealCriteriaSetServiceMock.Object,
                _propertyCriteriaSetServiceMock.Object,
                _loanCriteriaSetServiceMock.Object,
                _companyCriteriaSetServiceMock.Object);
        }
        public sealed class GetLoanList : LoansServiceTests
        {

            [Test]
            public void Should_Get_List_Of_Loans()
            {
                //arrange
                var loanList = MockLoanData();
                var expected = loanList.OrderBy("IsWatched  DESC, LoanAmount DESC, LoanName").Select(a => new { a.LoanName, a.Lender, a.Borrower });

                //act
                var actualLoanList = _service.Search(new LoanSearchCriteria()).Resources;

                //assert
                actualLoanList.Select(a => new { a.LoanName, a.Lender, a.Borrower }).Should().Have.SameSequenceAs(expected);
                actualLoanList.Any(a => a.Lender.Contains("N/A") == false).Should().Be.True();
                actualLoanList.Any(a => a.Borrower.Contains("N/A") == false).Should().Be.True();
            }
            [Test]
            public void Should_Get_List_Of_Loans_Containing_Properties()
            {
                //arrange
                var loanList = MockLoanData(true);
                var expected = loanList.OrderBy("IsWatched DESC, LoanAmount DESC, LoanName").Select(a => new { a.PropertyType, a.LoanName });

                //act
                var actualLoanList = _service.Search(new LoanSearchCriteria()).Resources;

                //assert
                actualLoanList.Select(a => new { a.PropertyType, a.LoanName }).Should().Have.SameSequenceAs(expected);
                actualLoanList.Any(a => a.PropertyType.Contains("N/A") == false).Should().Be.True();
            }
            [Test]
            public void Should_Paged_List_Of_Loans()
            {
                //arrange
                MockLoanData();

                //act
                var actualLoanList = _service.Search(new LoanSearchCriteria { SearchText = "", PageSize = 5 }).Resources;

                //assert
                actualLoanList.Count.Should().Be(5);
            }


            static IEnumerable<string> ColumnNames =>
               new[] { "IsWatched", "LoanName", "Borrower", "Lender", "PropertyType", "LoanAmount", "AllInRate", "Active", "OriginationDate", "MaturityDate" };


            [TestCaseSource(nameof(ColumnNames))]
            public void Should_Sort_List_Of_Loans_By_ColumnName(string sortField)
            {
                //arrange
                MockLoanData();

                //act
                var criteria = new LoanSearchCriteria { Sort = sortField };
                var actualLoanList = _service.Search(criteria).Resources;

                //assert
                actualLoanList.Select(a => a.LoanName).Should().Have.SameSequenceAs(
                    actualLoanList.OrderBy(a => sortField).Select(a => a.LoanName)
                  );
            }

            [TestCaseSource(nameof(ColumnNames))]
            public void Should_Sort_List_Of_Loans_By_FieldName_Descending(string sortField)
            {
                //arrange
                MockLoanData();

                //act
                var criteria = new LoanSearchCriteria { Sort = sortField, SortDescending = true };
                var actualLoanList = _service.Search(criteria).Resources;

                //assert
                actualLoanList.Select(a => a.LoanName).Should().Have.SameSequenceAs(
                    actualLoanList.OrderByDescending(a => sortField).Select(a => a.LoanName)
                    );
            }

            [Test]
            public void Should_Get_List_Of_Loans_With_SearchText()
            {
                //arrange
                var searchText = "John Loan plan";
                var loanList = MockLoanData(searchText).Where(l => l.LoanName.Contains(searchText));
                var expectedLoanList = loanList.OrderBy("IsWatched, LoanAmount DESC, LoanName");
                var criteria = new LoanSearchCriteria { SearchText = searchText };

                //act
                var actualLoanList = _service.Search(criteria).Resources;

                //assert 
                actualLoanList.Select(i => i.LoanName).Should().Have.SameSequenceAs(
                    expectedLoanList.Select(a => a.LoanName));
            }
            private List<LoanListItem> MockLoanData()
            {
                return MockLoanData(string.Empty, false);
            }
            private List<LoanListItem> MockLoanData(bool addProperty)
            {
                return MockLoanData(string.Empty, addProperty);
            }
            private List<LoanListItem> MockLoanData(string searchText)
            {
                return MockLoanData(searchText, false);
            }
            private List<LoanListItem> MockLoanData(string searchText, bool addProperty)
            {

                var userKey = RandomData.GetInt();
                _securityContextMock.Setup(x => x.UserKey).Returns(userKey);

                var expectedLoans = new List<LoanListItem>();
                int loanCount = 10;

                while (loanCount-- > 0)
                {
                    var assetCodeType = new dESx_AssetType { AssetTypeCode = AssetType.Loan.Code };
                    var loanAsset = new dESx_Asset
                    {
                        AssetKey = RandomData.GetInt(),
                        AssetTypeCode = AssetType.Loan.Code
                    };

                    var loanAssetName = new dESx_AssetName
                    {
                        AssetNameKey = RandomData.GetInt(),
                        AssetKey = loanAsset.AssetKey,
                        IsCurrent = true,
                        AssetName = RandomData.GetString(10)
                    };
                    var loanTerms = new dESx_AssetLoanTerm
                    {
                        AssetKey = loanAsset.AssetKey,
                        LoanAmount = RandomData.GetDecimal(),
                        LoanName = RandomData.GetString(10),
                        LoanBenchmarkTypeCode = RandomData.GetString(4),
                        LoanCashMgmtTypeCode = RandomData.GetString(5),
                        LoanLenderTypeCode = RandomData.GetString(5),
                        LoanPaymentTypeCode = RandomData.GetString(6),
                        LoanRateTypeCode = RandomData.GetString(3),
                    };
                    var loanMention = new dESx_Mention
                    {
                        MentionKey = RandomData.GetInt(),
                        LoanAssetNameKey = loanAssetName.AssetNameKey,
                        MentionId = RandomData.GetString(20),
                        IsPrimary = true
                    };
                    var userMentionData = new dESx_UserMentionData
                    {
                        MentionKey = loanMention.MentionKey,
                        IsWatched = RandomData.GetBoolean(),
                        UserKey = userKey
                    };

                    if (String.IsNullOrEmpty(searchText) == false)
                    {
                        if (loanCount == 9 || loanCount == 8)
                        {
                            loanAssetName.AssetName += searchText;
                        }
                    }

                    var loanItem = new LoanListItem { LoanKey = loanAsset.AssetKey, LoanName = loanAssetName.AssetName, LoanAmount = loanTerms.LoanAmount, Lender = "N/A", Borrower = "N/A", PropertyType = "N/A" };

                    if (loanCount == 1)
                    {
                        var numberOfLenders = 3;
                        var lenders = new List<string>();
                        while (numberOfLenders-- > 0)
                        {
                            var lenderCompany = new dESx_Company { CompanyKey = RandomData.GetInt() };
                            var lenderCompanyAsset = new dESx_CompanyAsset { CompanyAssetKey = RandomData.GetInt(), AssetKey = loanAsset.AssetKey, CompanyKey = lenderCompany.CompanyKey, IsCurrent = true };
                            var lenderCompanyAlias = new dESx_CompanyAlias { CompanyAliasKey = RandomData.GetInt(), CompanyKey = lenderCompany.CompanyKey, AliasName = RandomData.GetString(10), IsPrimary = true };
                            _fakeEsxDbContext.dESx_Companies.Attach(lenderCompany);
                            _fakeEsxDbContext.dESx_CompanyAssets.Attach(lenderCompanyAsset);
                            _fakeEsxDbContext.dESx_CompanyAlias.Attach(lenderCompanyAlias);
                            lenders.Add(lenderCompanyAlias.AliasName);
                        }
                        lenders.Sort();
                        loanItem.Lender = String.Join(", ", lenders);


                    }

                    if (loanCount == 2)
                    {
                        var borrowers = new List<string>();
                        var numberOfBorrowers = 4;
                        while (numberOfBorrowers-- > 0)
                        {
                            var borrowerCompany = new dESx_Company { CompanyKey = RandomData.GetInt() };
                            var borrower = new dESx_AssetLoanBorrower { LoanBorrowerKey = RandomData.GetInt(), CompanyKey = borrowerCompany.CompanyKey, AssetKey = loanAsset.AssetKey };
                            var borrowerCompanyAlias = new dESx_CompanyAlias { CompanyAliasKey = RandomData.GetInt(), CompanyKey = borrowerCompany.CompanyKey, AliasName = RandomData.GetString(10), IsPrimary = true };
                            _fakeEsxDbContext.dESx_Companies.Attach(borrowerCompany);
                            _fakeEsxDbContext.dESx_AssetLoanBorrowers.Attach(borrower);
                            _fakeEsxDbContext.dESx_CompanyAlias.Attach(borrowerCompanyAlias);
                            borrowers.Add(borrowerCompanyAlias.AliasName);
                        }
                        borrowers.Sort();
                        loanItem.Borrower = String.Join(", ", borrowers);
                    }

                    if (addProperty)
                    {
                        if (loanCount == 9 || loanCount == 8)
                        {
                            var propertyAsset = new dESx_Asset { AssetKey = RandomData.GetInt() };
                            var propertyAssetName = new dESx_AssetName { AssetKey = propertyAsset.AssetKey, AssetNameKey = RandomData.GetInt(), AssetName = RandomData.GetString(10) };
                            var assetLoanProperty = new dESx_AssetLoanProperty { PropertyAssetKey = propertyAsset.AssetKey, LoanAssetKey = loanAsset.AssetKey };
                            var assetUsageType = new dESx_AssetUsageType { AssetUsageTypeKey = RandomData.GetInt(), UsageType = RandomData.GetString(5) };
                            var assetUsage = new dESx_AssetUsage { AssetKey = propertyAsset.AssetKey, IsCurrent = true, AssetUsageTypeKey = assetUsageType.AssetUsageTypeKey };
                            _fakeEsxDbContext.dESx_Assets.Attach(propertyAsset);
                            _fakeEsxDbContext.dESx_AssetNames.Attach(propertyAssetName);
                            _fakeEsxDbContext.dESx_AssetLoanProperties.Attach(assetLoanProperty);
                            _fakeEsxDbContext.dESx_AssetUsageTypes.Attach(assetUsageType);
                            _fakeEsxDbContext.dESx_AssetUsages.Attach(assetUsage);
                            loanItem.PropertyType = assetUsageType.UsageType;
                        }
                    }
                    _fakeEsxDbContext.dESx_AssetTypes.Attach(assetCodeType);
                    _fakeEsxDbContext.dESx_Assets.Attach(loanAsset);
                    _fakeEsxDbContext.dESx_AssetNames.Attach(loanAssetName);
                    _fakeEsxDbContext.dESx_Mentions.Attach(loanMention);
                    _fakeEsxDbContext.dESx_UserMentionDatas.Attach(userMentionData);
                    _fakeEsxDbContext.dESx_AssetLoanTerms.Attach(loanTerms);
                    expectedLoans.Add(loanItem);
                }

                return expectedLoans;
            }
        }

        public sealed class GetCompanyLoans : LoansServiceTests
        {
            private void MockData(string companyMentionId, string expectedName = null, string expectedBorrower = null, string expectedLender = null)
            {
                expectedName = expectedName ?? RandomData.GetString(20);
                expectedBorrower = expectedBorrower ?? RandomData.GetString(20);
                expectedLender = expectedLender ?? RandomData.GetString(20);
                var userKey = RandomData.GetInt();

                _securityContextMock.Setup(x => x.UserKey).Returns(userKey);

                for (var i = 0; i < 10; i++)
                {
                    var loanKey = RandomData.GetInt();
                    var loanMentionKey = RandomData.GetInt();
                    var assetNameKey = RandomData.GetInt();
                    var aliasKey = RandomData.GetInt();
                    var companyKey = RandomData.GetInt();
                    var borrowerCompanyKey = RandomData.GetInt();
                    var borrowerAliasKey = RandomData.GetInt();
                    var lenderCompanyKey = RandomData.GetInt();
                    var lenderAliasKey = RandomData.GetInt();

                    //Company mention
                    _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                    {
                        MentionId = companyMentionId,
                        MentionKey = RandomData.GetInt(),
                        AliasKey = aliasKey,
                        IsPrimary = true
                    });

                    //loan mention
                    _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                    {
                        MentionId = RandomData.GetString(10),
                        MentionKey = loanMentionKey,
                        LoanAssetNameKey = assetNameKey,
                        IsPrimary = true
                    });

                    // borrower mention
                    _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                    {
                        MentionId = RandomData.GetString(10),
                        MentionKey = RandomData.GetInt(),
                        AliasKey = borrowerAliasKey,
                        IsPrimary = true
                    });

                    //lender mention
                    _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention
                    {
                        MentionId = RandomData.GetString(10),
                        MentionKey = RandomData.GetInt(),
                        AliasKey = lenderAliasKey,
                        IsPrimary = true
                    });

                    _fakeEsxDbContext.dESx_UserMentionDatas.Attach(new dESx_UserMentionData
                    {
                        MentionKey = loanMentionKey,
                        IsWatched = RandomData.GetInt() % 2 > 0,
                        UserKey = userKey
                    });

                    _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                    {
                        CompanyAliasKey = aliasKey,
                        CompanyKey = companyKey,
                        IsPrimary = true,
                        AliasName = i > 0 ? RandomData.GetString(10) : expectedName
                    });

                    _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                    {
                        CompanyAliasKey = borrowerAliasKey,
                        CompanyKey = borrowerCompanyKey,
                        IsPrimary = true,
                        AliasName = i > 0 ? RandomData.GetString(10) : expectedBorrower
                    });

                    _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias
                    {
                        CompanyAliasKey = lenderAliasKey,
                        CompanyKey = lenderCompanyKey,
                        IsPrimary = true,
                        AliasName = i > 0 ? RandomData.GetString(10) : expectedLender
                    });

                    _fakeEsxDbContext.dESx_AssetLoanBorrowers.Attach(new dESx_AssetLoanBorrower
                    {
                        LoanBorrowerKey = RandomData.GetInt(),
                        CompanyKey = companyKey,
                        AssetKey = loanKey
                    });

                    _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset
                    {
                        CompanyKey = companyKey,
                        AssetKey = loanKey,
                        IsCurrent = true
                    });

                    _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset
                    {
                        AssetKey = loanKey,
                        AssetTypeCode = AssetType.Loan.Code
                    });

                    _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName
                    {
                        AssetNameKey = assetNameKey,
                        AssetKey = loanKey,
                        AssetName = i > 0 ? RandomData.GetString(10) : expectedName,
                        IsCurrent = true
                    });

                    _fakeEsxDbContext.dESx_AssetLoanTerms.Attach(new dESx_AssetLoanTerm
                    {
                        AssetKey = loanKey,
                        OriginationDate = RandomData.GetDateTime(),
                        MaturationDate = RandomData.GetDateTime(),
                        LoanAmount = RandomData.GetDecimal()
                    });

                }
            }

            [Test]
            public void Should_Get_Company_Loans_With_SearchText()
            {
                //arrange
                var companyMentionId = RandomData.GetString(10);
                var expectedName = "test loan";
                var expectedBorrower = RandomData.GetString(20);
                var expectedLender = RandomData.GetString(20);
                var criteria = new LoanSearchCriteria
                {
                    CompanyMentionId = companyMentionId,
                    SearchText = "test"
                };
                MockData(companyMentionId, expectedName, expectedBorrower, expectedLender);


                //act
                var actual = _service.Search(criteria).Resources.Single();

                //assert
                actual.Satisfies(
                    s =>
                    s.LoanName == expectedName
                );
            }

            private static IEnumerable<string> FieldNames => new[] { "loanName", "loanMentionId", "isWatched", "borrower", "borrowerMentionId", "lender", "lenderMentionId", "loanAmount", "originationDate", "maturityDate" };

            [TestCaseSource(nameof(FieldNames))]
            public void Should_Get_Company_Loans_With_Sort(string sortColumn)
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                MockData(companyMentionId);

                var criteria = new LoanSearchCriteria
                {
                    CompanyMentionId = companyMentionId,
                    Sort = sortColumn
                };

                //act
                var actual = _service.Search(criteria).Resources;

                //assert
                actual.Select(x => x.LoanName).Should().Have.SameSequenceAs(actual.OrderBy(sortColumn).Select(x => x.LoanName));
            }

            [TestCaseSource(nameof(FieldNames))]
            public void Should_Get_Company_Loans_With_Sort_Descending(string sortColumn)
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                var testSortCoumn = sortColumn + " DESC";
                MockData(companyMentionId);

                var criteria = new LoanSearchCriteria
                {
                    CompanyMentionId = companyMentionId,
                    Sort = sortColumn,
                    SortDescending = true
                };

                //act
                var actual = _service.Search(criteria).Resources;

                //assert
                actual.Select(x => x.LoanName).Should().Have.SameSequenceAs(actual.OrderBy(testSortCoumn).Select(x => x.LoanName));
            }

            [Test]
            public void Should_Get_Company_Loans_With_Default_sort()
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                MockData(companyMentionId);
                var criteria = new LoanSearchCriteria
                {
                    CompanyMentionId = companyMentionId
                };

                //act
                var actual = _service.Search(criteria).Resources;

                var expected = actual.OrderBy("IsWatched DESC, LoanAmount DESC, LoanName ASC").Select(x => new { x.LoanName, x.IsWatched });
                //assert
                actual.Select(x => new { x.LoanName, x.IsWatched }).Should().Have.SameSequenceAs(expected);
            }

            [Test]
            public void Should_Get_Company_Loans_PageSize()
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                var expectedPageSize = RandomData.GetInt(1, 9);
                MockData(companyMentionId);

                var criteria = new LoanSearchCriteria
                {
                    CompanyMentionId = companyMentionId,
                    Offset = 0,
                    PageSize = expectedPageSize,
                    Sort = "",
                    SortDescending = false
                };

                //act
                var actual = _service.Search(criteria).Resources;

                //assert
                actual.Satisfies(a => a.Count() == expectedPageSize);
            }

            [Test]
            public void Should_Not_Get_Company_Loans()
            {
                //arrange
                var companyMentionId = RandomData.GetString(20);
                MockData(companyMentionId);

                var criteria = new LoanSearchCriteria
                {
                    SearchText = "WOJDLKLKSDFD",
                    Offset = 0,
                    PageSize = 10,
                    Sort = "",
                    SortDescending = false
                };

                //act
                var actual = _service.Search(criteria).Resources;

                //assert
                actual.Satisfies(a => !a.Any());
            }
        }

        public sealed class GetPropertyLoans : LoansServiceTests
        {
            [Test]
            public void Should_Get_Property_Loans()
            {
                // arrange          
                string mentionId = "@TestMention";
                var criteria = new LoanSearchCriteria
                {
                    PropertyMentionId = mentionId
                };

                var userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);

                var propertyAsset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset { AssetKey = RandomData.GetInt() });
                var propertyAssetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName { IsCurrent = true, AssetKey = propertyAsset.AssetKey, AssetNameKey = RandomData.GetInt() });
                var propertyMention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { PropertyAssetNameKey = propertyAssetName.AssetNameKey, IsPrimary = true, MentionId = mentionId });
                var loanAsset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset { AssetKey = RandomData.GetInt(), AssetTypeCode = AssetType.Loan.Code });
                var loanTerms = _fakeEsxDbContext.dESx_AssetLoanTerms.Attach(new dESx_AssetLoanTerm { AssetKey = loanAsset.AssetKey, LoanAmount = RandomData.GetDecimal(), MaturationDate = RandomData.GetDateTime() });
                var loanProperty = _fakeEsxDbContext.dESx_AssetLoanProperties.Attach(new dESx_AssetLoanProperty { PropertyAssetKey = propertyAsset.AssetKey, LoanAssetKey = loanTerms.AssetKey });
                var loanDeal = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal { DealKey = RandomData.GetInt(), DealCode = RandomData.GetString(20) });
                var loanDealAsset = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset { AssetKey = loanAsset.AssetKey, DealKey = loanDeal.DealKey });
                var loanAssetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName { AssetKey = loanAsset.AssetKey, IsCurrent = true, AssetName = RandomData.GetString(20), AssetNameKey = RandomData.GetInt() });
                var loanMention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { MentionKey = RandomData.GetInt(), LoanAssetNameKey = loanAssetName.AssetNameKey, IsPrimary = true, MentionId = RandomData.GetString(20) });
                var umd = _fakeEsxDbContext.dESx_UserMentionDatas.Attach(new dESx_UserMentionData { IsWatched = true, UserKey = userKey, MentionKey = loanMention.MentionKey });
                var companyAsset = _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset { IsCurrent = true, CompanyKey = RandomData.GetInt(), AssetKey = loanAsset.AssetKey });
                var lender = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { IsPrimary = true, AliasName = RandomData.GetString(20), CompanyKey = companyAsset.CompanyKey });
                var assetLoanBorrower = _fakeEsxDbContext.dESx_AssetLoanBorrowers.Attach(new dESx_AssetLoanBorrower { AssetKey = loanAsset.AssetKey, CompanyKey = RandomData.GetInt() });
                var borrower = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { CompanyKey = assetLoanBorrower.CompanyKey, AliasName = RandomData.GetString(20), IsPrimary = true });

                // act
                var actual = _service.Search(criteria);

                // assert
                actual.Should().Not.Be.Null();
                actual.PagingData.TotalCount.Should().Be(1);

                var loan = actual.Resources.First();
                loan.Satisfies(x => x.LoanName == loanAssetName.AssetName
                                    && x.LoanAmount == loanTerms.LoanAmount
                                    && x.IsWatched == true
                                    && x.Borrower == borrower.AliasName
                                    && x.Lender == lender.AliasName
                                    && x.LoanKey == loanAsset.AssetKey
                                    && x.DealCode == loanDeal.DealCode
                                    && x.LoanMentionId == loanMention.MentionId
                                    && x.MaturityDate == loanTerms.MaturationDate);
            }

            [Test]
            public void Should_Get_Property_Loans_With_Sorting_And_Paging()
            {
                // arrange          
                var criteria = new LoanSearchCriteria
                {
                    SearchText = "Test Loan",
                    PropertyMentionId = "@TestMention",
                    PageSize = 10,
                    SortDescending = true,
                    Sort = "LoanName"
                };

                var userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);

                string lastLoanName = null;
                string lastMentionName = null;
                decimal lastLoanAmount = 0;

                for (int i = 0; i < 10; i++)
                {
                    var loanName = i >= 7 ? "Test Loan" + i : RandomData.GetString(20);
                    var propertyAsset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset { AssetKey = RandomData.GetInt() });
                    var propertyAssetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName { IsCurrent = true, AssetKey = propertyAsset.AssetKey, AssetNameKey = RandomData.GetInt() });
                    var propertyMention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { PropertyAssetNameKey = propertyAssetName.AssetNameKey, IsPrimary = true, MentionId = criteria.PropertyMentionId });
                    var loanAsset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset { AssetKey = RandomData.GetInt(), AssetTypeCode = AssetType.Loan.Code });
                    var loanTerms = _fakeEsxDbContext.dESx_AssetLoanTerms.Attach(new dESx_AssetLoanTerm { AssetKey = loanAsset.AssetKey, LoanAmount = RandomData.GetDecimal(), MaturationDate = RandomData.GetDateTime() });
                    var loanProperty = _fakeEsxDbContext.dESx_AssetLoanProperties.Attach(new dESx_AssetLoanProperty { PropertyAssetKey = propertyAsset.AssetKey, LoanAssetKey = loanTerms.AssetKey });
                    var loanDeal = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal { DealKey = RandomData.GetInt(), DealCode = RandomData.GetString(20) });
                    var loanDealAsset = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset { AssetKey = loanAsset.AssetKey, DealKey = loanDeal.DealKey });
                    var loanAssetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName { AssetKey = loanAsset.AssetKey, IsCurrent = true, AssetName = loanName, AssetNameKey = RandomData.GetInt() });
                    var loanMention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { MentionKey = RandomData.GetInt(), LoanAssetNameKey = loanAssetName.AssetNameKey, IsPrimary = true, MentionId = RandomData.GetString(20) });
                    var umd = _fakeEsxDbContext.dESx_UserMentionDatas.Attach(new dESx_UserMentionData { IsWatched = true, UserKey = userKey, MentionKey = loanMention.MentionKey });
                    var companyAsset = _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset { IsCurrent = true, CompanyKey = RandomData.GetInt(), AssetKey = loanAsset.AssetKey });
                    var lender = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { IsPrimary = true, AliasName = "LenderAlias1", CompanyKey = companyAsset.CompanyKey });
                    var lender2 = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { IsPrimary = true, AliasName = "LenderAlias2", CompanyKey = companyAsset.CompanyKey });
                    var assetLoanBorrower = _fakeEsxDbContext.dESx_AssetLoanBorrowers.Attach(new dESx_AssetLoanBorrower { AssetKey = loanAsset.AssetKey, CompanyKey = RandomData.GetInt() });
                    var borrower = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { CompanyKey = assetLoanBorrower.CompanyKey, AliasName = "BorrowerAlias1", IsPrimary = true });
                    var borrower2 = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { CompanyKey = assetLoanBorrower.CompanyKey, AliasName = "BorrowerAlias2", IsPrimary = true });

                    lastLoanName = loanAssetName.AssetName;
                    lastMentionName = loanMention.MentionId;
                    lastLoanAmount = loanTerms.LoanAmount;
                }


                // act
                var actual = _service.Search(criteria);

                // assert
                actual.Should().Not.Be.Null();
                actual.PagingData.TotalCount.Should().Be(3);
                actual.Resources.First().Satisfies(ln =>
                   ln.LoanName == lastLoanName
                   && ln.LoanMentionId == lastMentionName
                   && ln.LoanAmount == lastLoanAmount
                   && ln.Lender == "LenderAlias1, LenderAlias2"
                   && ln.Borrower == "BorrowerAlias1, BorrowerAlias2");
            }
            [Test]
            public void Should_Get_Property_Loans_Sorted_By_WatchListLoans_By_Default()
            {
                // arrange   
                var criteria = new LoanSearchCriteria
                {
                    PropertyMentionId = "@TestMention"
                };

                const int userKey = 1;
                _securityContextMock.SetupGet(x => x.UserKey).Returns(userKey);

                string expectedLoanName = null;
                string expectedMentionName = null;
                decimal expectedLoanAmount = 0;

                for (var i = 0; i < 10; i++)
                {
                    var isWatched = i == 7;
                    var propertyAsset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset { AssetKey = RandomData.GetInt() });
                    var propertyAssetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName { IsCurrent = true, AssetKey = propertyAsset.AssetKey, AssetNameKey = RandomData.GetInt() });
                    var propertyMention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { PropertyAssetNameKey = propertyAssetName.AssetNameKey, IsPrimary = true, MentionId = criteria.PropertyMentionId });
                    var loanAsset = _fakeEsxDbContext.dESx_Assets.Attach(new dESx_Asset { AssetKey = RandomData.GetInt(), AssetTypeCode = AssetType.Loan.Code });
                    var loanTerms = _fakeEsxDbContext.dESx_AssetLoanTerms.Attach(new dESx_AssetLoanTerm { AssetKey = loanAsset.AssetKey, LoanAmount = RandomData.GetDecimal(), MaturationDate = RandomData.GetDateTime() });
                    var loanProperty = _fakeEsxDbContext.dESx_AssetLoanProperties.Attach(new dESx_AssetLoanProperty { PropertyAssetKey = propertyAsset.AssetKey, LoanAssetKey = loanTerms.AssetKey });
                    var loanDeal = _fakeEsxDbContext.dESx_Deals.Attach(new dESx_Deal { DealKey = RandomData.GetInt(), DealCode = RandomData.GetString(20) });
                    var loanDealAsset = _fakeEsxDbContext.dESx_DealAssets.Attach(new dESx_DealAsset { AssetKey = loanAsset.AssetKey, DealKey = loanDeal.DealKey });
                    var loanAssetName = _fakeEsxDbContext.dESx_AssetNames.Attach(new dESx_AssetName { AssetKey = loanAsset.AssetKey, IsCurrent = true, AssetName = RandomData.GetString(20), AssetNameKey = RandomData.GetInt() });
                    var loanMention = _fakeEsxDbContext.dESx_Mentions.Attach(new dESx_Mention { MentionKey = RandomData.GetInt(), LoanAssetNameKey = loanAssetName.AssetNameKey, IsPrimary = true, MentionId = RandomData.GetString(20) });
                    var umd = _fakeEsxDbContext.dESx_UserMentionDatas.Attach(new dESx_UserMentionData { IsWatched = isWatched, UserKey = userKey, MentionKey = loanMention.MentionKey });
                    var companyAsset = _fakeEsxDbContext.dESx_CompanyAssets.Attach(new dESx_CompanyAsset { IsCurrent = true, CompanyKey = RandomData.GetInt(), AssetKey = loanAsset.AssetKey });
                    var lender = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { IsPrimary = true, AliasName = "LenderAlias1", CompanyKey = companyAsset.CompanyKey });
                    var lender2 = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { IsPrimary = true, AliasName = "LenderAlias2", CompanyKey = companyAsset.CompanyKey });
                    var assetLoanBorrower = _fakeEsxDbContext.dESx_AssetLoanBorrowers.Attach(new dESx_AssetLoanBorrower { AssetKey = loanAsset.AssetKey, CompanyKey = RandomData.GetInt() });
                    var borrower = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { CompanyKey = assetLoanBorrower.CompanyKey, AliasName = "BorrowerAlias1", IsPrimary = true });
                    var borrower2 = _fakeEsxDbContext.dESx_CompanyAlias.Attach(new dESx_CompanyAlias { CompanyKey = assetLoanBorrower.CompanyKey, AliasName = "BorrowerAlias2", IsPrimary = true });

                    if (i == 7)
                    {
                        expectedLoanName = loanAssetName.AssetName;
                        expectedMentionName = loanMention.MentionId;
                        expectedLoanAmount = loanTerms.LoanAmount;
                    }
                }

                // act
                var actual = _service.Search(criteria);

                //assert
                actual.PagingData.TotalCount.Should().Be(10);
                actual.Resources.First().Satisfies(x => x.LoanName == expectedLoanName
                    && x.LoanAmount == expectedLoanAmount
                    && x.LoanMentionId == expectedMentionName
                    && x.IsWatched == true);
            }
        }
    }
}

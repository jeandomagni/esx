﻿using System;
using Esx.Business.Security;
using Esx.Contracts.Models.User;
using Esx.Data.Repositories;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Business.Tests.Security
{
    public abstract class SecurityContextTests
    {

        private SecurityContext _preparedSecurityContext;
        private string _userName;
        private Mock<ISecurityRepository> _securityRepository;
        private User _user;

        [SetUp]
        public void Setup()
        {
            _user = new User
            {
                Username = RandomData.GetString(10),
                AccessToken = RandomData.GetByteArray(64),
                MentionId = "@" + RandomData.GetString(10)
            };
                _securityRepository = new Mock< ISecurityRepository>();
            _securityRepository.Setup(r => r.Login(It.IsAny<string>())).Returns(_user);

            _preparedSecurityContext = new SecurityContext(_securityRepository.Object);
            _userName = RandomData.GetString(10);
            _preparedSecurityContext.PrepareContext(_userName, false);
        }

        public sealed class PrepareContextMethod : SecurityContextTests
        {
            [Test]
            public void Should_throw_if_already_prepared()
            {
                Executing.This(() => _preparedSecurityContext.PrepareContext(RandomData.GetString(10), false))
                         .Should().Throw<InvalidOperationException>()
                         .And.Exception.Message.Should().Be.EqualTo("Context already prepared.");
            }

            [Test]
            public void Should_set_token_from_repository_login()
            {

                //arrange
                var context = new SecurityContext(_securityRepository.Object);
                var username = RandomData.GetString(10);
                var expectedUser = new User
                {
                    AccessToken = RandomData.GetByteArray(64)
                };

                //TODO use actual Identity instead of hardcoded user. 
                //_securityRepository.Setup(r => r.Login(It.Is<string>(s => s == username))).Returns(expectedUser);
                _securityRepository.Setup(r => r.Login(It.Is<string>(s => s == "User1"))).Returns(expectedUser);

                //act
                context.PrepareContext("User1", false);

                //assert
                (context.DbToken.Equals(expectedUser.AccessToken)).Should().Be.True();
                _securityRepository.VerifyAll();
            }
        }

        public sealed class DbTokenProperty : SecurityContextTests
        {
            [Test]
            public void Should_throw_if_context_not_prepared()
            {
                var context = new SecurityContext(_securityRepository.Object);
                Executing.This(() => { var zz = context.DbToken; })
                         .Should().Throw<InvalidOperationException>()
                         .And.Exception.Message.Should().Be.EqualTo("Context not prepared.");
            }

            [Test]
            public void Should_set_dbToken()
            {
                //act
                var token = _preparedSecurityContext.DbToken;

                //assert
                token.Should().Not.Be.Empty();

            }
        }

    }
}

﻿using Esx.Business.Companies;
using Esx.Business.Contacts;
using Esx.Business.Deals;
using Esx.Business.Loans;
using Esx.Business.Properties;
using Esx.Business.Search;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Company;
using Esx.Contracts.Models.Contact;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Models.Loan;
using Esx.Contracts.Models.Property;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;
using Moq;
using NUnit.Framework;

namespace Esx.Business.Tests
{
    public abstract class SearchServiceTests
    {
        private Mock<ISearchCriteriaFactory> _searchCriteriaFactory;
        private Mock<ICompaniesService> _companiesServiceMock;
        private Mock<IContactsService> _contactsServiceMock;
        private Mock<IPropertiesService> _propertiesServiceMock;
        private Mock<IDealsService> _dealsServiceMock;
        private Mock<ILoansService> _loansServiceMock;

        private SearchService _service;

        [SetUp]
        public void TestInit()
        {
            _searchCriteriaFactory = new Mock<ISearchCriteriaFactory>();
            _companiesServiceMock = new Mock<ICompaniesService>();
            _contactsServiceMock = new Mock<IContactsService>();
            _propertiesServiceMock = new Mock<IPropertiesService>();
            _dealsServiceMock = new Mock<IDealsService>();
            _loansServiceMock = new Mock<ILoansService>();

            _service = new SearchService(
                _searchCriteriaFactory.Object,
                _companiesServiceMock.Object,
                _contactsServiceMock.Object,
                _propertiesServiceMock.Object,
                _loansServiceMock.Object,
                _dealsServiceMock.Object
                );
        }

        [TearDown]
        public void TestDispose()
        {

        }

        public sealed class Search : SearchServiceTests
        {

            [Test]
            public void Should_Call_The_Company_Search_If_Core_Object_Type_Matches()
            {
                //arrange
                var criteria = new SearchCriteria { CoreObjectType = CoreType.Company };
                var companyCriteria = new CompanySearchCriteria();
                _companiesServiceMock.Setup(x => x.Search(It.IsAny<CompanySearchCriteria>())).Returns(new PagedResources<CompanyListItem>());
                _searchCriteriaFactory.Setup(x => x.GetCompanySearchCriteria(It.IsAny<SearchCriteria>())).Returns(companyCriteria);

                //act
                _service.Search(criteria);

                //assert
                _companiesServiceMock.Verify(x => x.Search(companyCriteria), Times.Once);
            }

            [Test]
            public void Should_Call_The_Contact_Search_If_Core_Object_Type_Matches()
            {
                //arrange
                var criteria = new SearchCriteria { CoreObjectType = CoreType.Contact };
                var contactCriteria = new ContactSearchCriteria();
                _contactsServiceMock.Setup(x => x.Search(It.IsAny<ContactSearchCriteria>())).Returns(new PagedResources<ContactListItem>());
                _searchCriteriaFactory.Setup(x => x.GetContactSearchCriteria(It.IsAny<SearchCriteria>())).Returns(contactCriteria);

                //act
                _service.Search(criteria);

                //assert
                _contactsServiceMock.Verify(x => x.Search(contactCriteria), Times.Once);
            }

            [Test]
            public void Should_Call_The_Property_Search_If_Core_Object_Type_Matches()
            {
                //arrange
                var criteria = new SearchCriteria { CoreObjectType = CoreType.Property };
                var propertyCriteria = new PropertySearchCriteria();
                _propertiesServiceMock.Setup(x => x.Search(It.IsAny<PropertySearchCriteria>())).Returns(new PagedResources<PropertyListItem>());
                _searchCriteriaFactory.Setup(x => x.GetPropertySearchCriteria(It.IsAny<SearchCriteria>())).Returns(propertyCriteria);

                //act
                _service.Search(criteria);

                //assert
                _propertiesServiceMock.Verify(x => x.Search(propertyCriteria), Times.Once);
            }

            [Test]
            public void Should_Call_The_Deal_Search_If_Core_Object_Type_Matches()
            {
                //arrange
                var criteria = new SearchCriteria { CoreObjectType = CoreType.Deal };
                var dealCriteria = new DealSearchCriteria();
                _dealsServiceMock.Setup(x => x.Search(It.IsAny<DealSearchCriteria>(), null)).Returns(new PagedResources<DealListItem>());
                _searchCriteriaFactory.Setup(x => x.GetDealSearchCriteria(It.IsAny<SearchCriteria>())).Returns(dealCriteria);

                //act
                _service.Search(criteria);

                //assert
                _dealsServiceMock.Verify(x => x.Search(dealCriteria, null), Times.Once);
            }

            [Test]
            public void Should_Call_The_Loan_Search_If_Core_Object_Type_Matches()
            {
                //arrange
                var criteria = new SearchCriteria { CoreObjectType = CoreType.Loan };
                var loanCriteria = new LoanSearchCriteria();
                _loansServiceMock.Setup(x => x.Search(It.IsAny<LoanSearchCriteria>())).Returns(new PagedResources<LoanListItem>());
                _searchCriteriaFactory.Setup(x => x.GetLoanSearchCriteria(It.IsAny<SearchCriteria>())).Returns(loanCriteria);

                //act
                _service.Search(criteria);

                //assert
                _loansServiceMock.Verify(x => x.Search(loanCriteria), Times.Once);
            }
        }
    }
}

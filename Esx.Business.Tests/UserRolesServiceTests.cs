﻿using Esx.Business.Security;
using Esx.Contracts.Security;
using Esx.Data;
using Moq;
using NUnit.Framework;

namespace Esx.Business.Tests
{
    //TODO, complete this tests
    public class UserRolesServiceTests
    {
        private Mock<ISecurityContext> _securityContextMock;
        private UserRoleService _userRolesService;
        private Mock<IActiveDirectoryService> _activeDirectoryService;
        private Mock<FakeEsxDbContext> _dbContext;
        private Mock<IRoleFeatureService> _roleFeatureService;

        [SetUp]
        public void TestInit()
        {
            _securityContextMock = new Mock<ISecurityContext>();
            _activeDirectoryService = new Mock<IActiveDirectoryService>();
            _dbContext = new Mock<FakeEsxDbContext>();
            _roleFeatureService = new Mock<IRoleFeatureService>();
            _userRolesService = new UserRoleService(_activeDirectoryService.Object, _securityContextMock.Object, 
                _dbContext.Object, _roleFeatureService.Object);
        }

        [TearDown]
        public void TestDispose()
        {
           // _securityContextMock.Reset();
        }
        public sealed class HasAccess : UserRolesServiceTests
        {

            [Test]
            public void Should_Grant_Permission_To_Role()
            {
                //arrange

            }
        }
    }
}

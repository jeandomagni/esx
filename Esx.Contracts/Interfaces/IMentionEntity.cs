﻿namespace Esx.Contracts.Interfaces
{
    public interface IMentionEntity
    {
        int ObjectKey { get; }
        string ObjectType { get; }
        bool ObjectPrimary { get; }
        string MentionSource { get; }
        string MentionId { get; set; }
    }
}

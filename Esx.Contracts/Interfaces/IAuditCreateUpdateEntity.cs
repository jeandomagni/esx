﻿using System;

namespace Esx.Contracts.Interfaces
{
    public interface IAuditCreateUpdateEntity : IAuditCreateEntity
    {
        DateTime UpdatedDate { get; set; }
        string UpdatedBy { get; set; }
    }
}

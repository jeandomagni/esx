﻿using System.Collections.Generic;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;

namespace Esx.Contracts.Interfaces
{
    public interface ISearchableService
    {
        PagedResources<object> Search(SearchCriteria searchCriteria);
    }
}

﻿using System;

namespace Esx.Contracts.Interfaces
{
    public interface IAuditCreateEntity
    {
        DateTime CreatedDate { get; set; }
        string CreatedBy { get; set; }
    }
}

﻿namespace Esx.Contracts.Paging
{
    public class PagingData
    {
        public PagingData()
        {
            PageSize = 30;
        }

        public PagingData(PagingData pagingDataToCopy)
        {
            TotalCount = pagingDataToCopy.TotalCount;
            Offset = pagingDataToCopy.Offset;
            PageSize = pagingDataToCopy.PageSize;
            Sort = pagingDataToCopy.Sort;
            SortDescending = pagingDataToCopy.SortDescending;
        }

        public int? TotalCount { get; set; }
        public int Offset { get; set; }
        public int PageSize { get; set; }
        public string Sort { get; set; }
        public bool SortDescending { get; set; }
    }
}
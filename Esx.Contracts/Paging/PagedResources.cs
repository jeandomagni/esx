﻿using System.Collections.Generic;
using System.Linq;

namespace Esx.Contracts.Paging
{
    public class PagedResources<TModel>
    {
        public ICollection<TModel> Resources { get; set; }
        public PagingData PagingData { get; set; }
    }
}
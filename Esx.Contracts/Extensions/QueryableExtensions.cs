﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Esx.Contracts.Models.Search;

namespace Esx.Contracts.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> WhereIn<T>(this IQueryable<T> source, Expression<Func<T, DateTime?>> dateSelector, IEnumerable<Range<DateTime>> ranges)
        {
            var enumerable = ranges as Range<DateTime>[] ?? ranges.ToArray();
            if (!enumerable.Any()) return source;

            var filter = enumerable
                .Select(range =>
                {
                    var startFilter = range.Start != null
                        ? GreaterThanOrEqual(dateSelector.Body, Expression.Constant((DateTime?)range.Start.Value.Date))
                        : null;
                    var endFilter = range.End != null
                        ? LessThan(dateSelector.Body, Expression.Constant((DateTime?)range.End.Value.Date.AddDays(1)))
                        : null;
                    return startFilter == null
                        ? endFilter
                        : endFilter == null ? startFilter : Expression.AndAlso(startFilter, endFilter);
                })
                .Where(item => item != null)
                .Aggregate(Expression.OrElse);
            return filter != null
                ? source.Where(Expression.Lambda<Func<T, bool>>(filter, dateSelector.Parameters[0]))
                : source;
        }

        public static IQueryable<T> WhereIn<T>(this IQueryable<T> source, Expression<Func<T, decimal?>> numSelector, IEnumerable<Range<decimal>> ranges)
        {
            var enumerable = ranges as Range<decimal>[] ?? ranges.ToArray();
            if (!enumerable.Any()) return source;

            var filter = enumerable
                .Select(range =>
                {
                    var startFilter = range.Start != null
                        ? GreaterThanOrEqual(numSelector.Body, Expression.Constant(range.Start))
                        : null;
                    var endFilter = range.End != null
                        ? LessThanOrEqual(numSelector.Body, Expression.Constant(range.End))
                        : null;
                    return startFilter == null
                        ? endFilter
                        : endFilter == null ? startFilter : Expression.AndAlso(startFilter, endFilter);
                })
                .Where(item => item != null)
                .Aggregate(Expression.OrElse);
            return filter != null
                ? source.Where(Expression.Lambda<Func<T, bool>>(filter, numSelector.Parameters[0]))
                : source;
        }

        private static Expression GreaterThan(Expression e1, Expression e2)
        {
            if (IsNullableType(e1.Type) && !IsNullableType(e2.Type))
                e2 = Expression.Convert(e2, e1.Type);
            else if (!IsNullableType(e1.Type) && IsNullableType(e2.Type))
                e1 = Expression.Convert(e1, e2.Type);
            return Expression.GreaterThan(e1, e2);
        }
        private static Expression GreaterThanOrEqual(Expression e1, Expression e2)
        {
            if (IsNullableType(e1.Type) && !IsNullableType(e2.Type))
                e2 = Expression.Convert(e2, e1.Type);
            else if (!IsNullableType(e1.Type) && IsNullableType(e2.Type))
                e1 = Expression.Convert(e1, e2.Type);
            return Expression.GreaterThanOrEqual(e1, e2);
        }

        private static Expression LessThan(Expression e1, Expression e2)
        {
            if (IsNullableType(e1.Type) && !IsNullableType(e2.Type))
                e2 = Expression.Convert(e2, e1.Type);
            else if (!IsNullableType(e1.Type) && IsNullableType(e2.Type))
                e1 = Expression.Convert(e1, e2.Type);
            return Expression.LessThan(e1, e2);
        }

        private static Expression LessThanOrEqual(Expression e1, Expression e2)
        {
            if (IsNullableType(e1.Type) && !IsNullableType(e2.Type))
                e2 = Expression.Convert(e2, e1.Type);
            else if (!IsNullableType(e1.Type) && IsNullableType(e2.Type))
                e1 = Expression.Convert(e1, e2.Type);
            return Expression.LessThanOrEqual(e1, e2);
        }

        private static bool IsNullableType(Type t)
        {
            return t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>);
        }
    }
}

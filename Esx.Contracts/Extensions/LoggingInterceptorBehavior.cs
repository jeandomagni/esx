﻿using System;
using System.Collections.Generic;
using log4net;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace Esx.Contracts.Extensions
{
    public class LoggingInterceptorBehavior : IInterceptionBehavior
    {
        private readonly ILog _logger;
        public LoggingInterceptorBehavior(ILog logger)
        {
            _logger = logger;
        }

        public IMethodReturn Invoke(IMethodInvocation input,
          GetNextInterceptionBehaviorDelegate getNext)
        {
            // Before invoking the method on the original target.
            _logger.Debug(String.Format(
              "Invoking method {0} at {1}",
              input.MethodBase, DateTime.Now.ToLongTimeString()));

            // Invoke the next behavior in the chain.
            var result = getNext()(input, getNext);

            // After invoking the method on the original target.
            if (result.Exception != null)
            {
                _logger.Error(String.Format(
                  "Method {0} threw exception {1} at {2}",
                  input.MethodBase, result.Exception.Message,
                  DateTime.Now.ToLongTimeString()));
            }
            else
            {
                _logger.Debug(String.Format(
                  "Method {0} returned {1} at {2}",
                  input.MethodBase, result.ReturnValue,
                  DateTime.Now.ToLongTimeString()));
            }

            return result;
        }

        public IEnumerable<Type> GetRequiredInterfaces()
        {
            return Type.EmptyTypes;
        }

        public bool WillExecute
        {
            get { return true; }
        }
    }
}

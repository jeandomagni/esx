﻿using System;
namespace Esx.Contracts.Extensions
{
    public static class StringExtensions
    {
        public static bool IgnoreCaseContains(this string text, string value,
            StringComparison stringComparison = StringComparison.CurrentCultureIgnoreCase)
        {
            return text.IndexOf(value, stringComparison) >= 0;
        }
    }
}

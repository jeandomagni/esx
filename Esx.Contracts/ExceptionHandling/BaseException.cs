﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Esx.Contracts.ExceptionHandling
{
    public class BaseException : Exception
    {    
        public BaseException(string message, HttpStatusCode statusCode) : base(message)
        {
            StatusCode = statusCode;
        }
        
        public HttpStatusCode StatusCode { get; set; }
    }
}
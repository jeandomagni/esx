﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.ExceptionHandling
{
    public  class DuplicateException: BaseException
    {
        public DuplicateException(string message) : base(message, HttpStatusCode.Conflict)
        {
        }
    }
}

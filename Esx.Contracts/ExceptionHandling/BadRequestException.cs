﻿using System.Net;

namespace Esx.Contracts.ExceptionHandling
{
    public class BadRequestException : BaseException
    {
        public BadRequestException(string message) 
            : base(message, HttpStatusCode.BadRequest)
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Esx.Contracts.ExceptionHandling
{
    public class InternalServerErrorException : BaseException
    {
        public InternalServerErrorException(string message) 
            : base(message, HttpStatusCode.InternalServerError)
        {
        }
    }
}
﻿using Esx.Contracts.Models.User;

namespace Esx.Contracts.Security
{
    public interface ISecurityContext
    {
        User PrepareContext(string identity, bool impersonatingUser);

        void Init(string userName, string userData, string principalUser);

        byte[] DbToken { get; }

        int UserKey { get; }

        string UserName { get; }

        string PrincipalUserName { get; }
        bool AddNewUser(string userName, string fullName);
    }

}

﻿namespace Esx.Contracts.Enums
{
    public static class AddressUsageType
    {
        public const string Alternate = "Alternate";
        public const string Assistant = "Assistant";
        public const string Main = "Main";
        public const string Blackberry = "Blackberry";
        public const string Ext = "Ext";
        public const string Fax = "Fax";
        public const string Home = "Home";
        public const string Mobile = "Mobile";
    }
}

﻿namespace Esx.Contracts.Enums
{
    public static class DealStatusCode
    {
        public const string Pitching = "Pitching";
        public const string PitchedWon = "Pitched & Won";
        public const string PitchedLost = "Pitched & Lost";
        public const string Closed = "Closed";
    }
}

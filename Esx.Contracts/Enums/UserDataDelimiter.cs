﻿namespace Esx.Contracts.Enums
{
    public static class UserDataDelimiter
    {
        public const char Delimiter = '@';
        public const char RoleDelimiter = '|';
    }
}

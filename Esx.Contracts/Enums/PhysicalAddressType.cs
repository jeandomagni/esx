﻿
namespace Esx.Contracts.Enums
{
    public static class PhysicalAddressType
    {
        public const string Company = "Company";
        public const string Primary = "Primary";
    }
}

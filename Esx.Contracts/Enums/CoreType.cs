﻿namespace Esx.Contracts.Enums
{
    public static class CoreType
    {
        public const string Company = "A";
        public const string Contact = "C";
        public const string Deal = "D";
        public const string Loan = "L";
        public const string Office = "O";
        public const string Property = "P";
        public const string Market = "M";
    }
}

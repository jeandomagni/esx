﻿using System;

namespace Esx.Contracts.Enums
{
    public abstract class LookupBase : IComparable
    {
        protected LookupBase()
        {
        }

        protected LookupBase(string code, string label)
        {
            Code = code;
            Label = label;
        }

        public string Code { get; }

        public string Label { get; }

        public override string ToString()
        {
            return Code;
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as LookupBase;

            if (otherValue == null) return false;

            var typeMatches = GetType() == obj.GetType();
            var valueMatches = Code.Equals(otherValue.Code);

            return typeMatches && valueMatches;
        }

        public static bool operator ==(LookupBase a, LookupBase b)
        {
            if (a == null && b == null) return true;
            if (a == null || b == null) return false;
            return a.Code == b.Code;
        }

        public static bool operator !=(LookupBase a, LookupBase b)
        {
            if (a == null && b == null) return false;
            if (a == null || b == null) return true;
            return a.Code != b.Code;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

        public int CompareTo(object other)
        {
            return string.Compare(Code, ((LookupBase)other).Code, StringComparison.Ordinal);
        }
    }
}

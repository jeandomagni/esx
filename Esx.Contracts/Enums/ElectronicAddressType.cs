﻿namespace Esx.Contracts.Enums
{
    public static class ElectronicAddressType
    {
        public const string EmailAddress = "EmailAddress";
        public const string PhoneNumber = "PhoneNumber";
        public const string Website = "Website";
        public const string LinkedInProfile = "LinkedInProfile";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Enums
{
    public static class LoanRole
    {
        public const string Borrower = "Borrower";
        public const string Lender = "Lender";
    }
}

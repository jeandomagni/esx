﻿namespace Esx.Contracts.Enums
{
    public static class FilterName
    {
        // Deal Filter Options
        public static class Deal
        {
            public const string DealType = "DealType";
            public const string StructureType = "StructureType";
            public const string LastCloseDate = "LastCloseDate";
            public const string BidStatus = "BidStatus";
            public const string BidDate = "BidDate";
            public const string Fees = "Fees";
            public const string DealStatus = "DealStatus";
            public const string TransactionAmount = "TransactionAmount";
            public const string ValuationAmount = "ValuationAmount";
            public const string DealsIncluded = "DealsIncluded";
            public const string DealName = "DealName";
            public const string BidderCount = "BidderCount";
            public const string DealPropertyName = "DealPropertyName";
            public const string DealPropertyLocation = "DealPropertyLocation";
            public const string DealPropertyType = "DealPropertyType";
            public const string DealPropertyRiskProfile = "DealPropertyRiskProfile";
            public const string DealContactName = "DealContactName";
        }

        // Company Filter Options
        public static class Company
        {
            public const string CompanyName = "CompanyName";
            public const string CompanyDealRole = "CompanyDealRole";
            public const string CompanyPropertyLocation = "CompanyPropertyLocation";
            public const string CompanyPropertyType = "CompanyPropertyType";
            public const string CompanyPropertyRiskProfile = "CompanyPropertyRiskProfile";
        }

        // Property Filter Options
        public static class Property
        {
            public const string PropertyKey = "PropertyKey";
            public const string PropertyName = "PropertyName";
            public const string PropertyLocation = "PropertyLocation";
            public const string PropertyType = "PropertyType";
            public const string PropertySize = "PropertySize";
        }

        // Property Location Filter Options
        public static class PropertyLocation
        {
            public const string PropertyContinent = "PropertyContinent";
            public const string PropertyCountry = "PropertyCountry";
            public const string PropertyCity = "PropertyCity";
            public const string PropertyRegion = "PropertyRegion";
            public const string PropertyMapView = "PropertyMapView";
        }

        // Loan Filter Options
        public static class Loan
        {
            public const string OriginationDate = "OriginationDate";
            public const string LockoutDate = "LockoutDate";
            public const string YieldMaintenanceEndDate = "YieldMaintenanceEndDate";
            public const string NextMaturityDate = "NextMaturityDate";
            public const string FinalMaturityDate = "FinalMaturityDate";
            public const string LoanAmount = "LoanAmount";
            public const string InterestRate = "InterestRate";
            public const string RateType = "RateType";
            public const string LTV = "LTV";
            public const string DebtYield = "DebtYield";
            public const string LoanStatusDate = "LoanStatusDate";
            public const string LoanPropertyLocation = "LoanPropertyLocation";
            public const string LoanPropertyType = "LoanPropertyType";
        }

        // Contact Filter Options
        public static class Contact
        {
            public const string ContactName = "ContactName";
            public const string ContactTitle = "ContactTitle";
            public const string ContactDealRole = "ContactDealRole";
            public const string EastdilOffice = "EastdilOffice";
        }
    }
}

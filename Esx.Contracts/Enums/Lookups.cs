﻿
using System.Collections.Generic;

namespace Esx.Contracts.Enums
{
	public class AssetContactType : LookupBase
	{
		private AssetContactType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<AssetContactType> GetAll()
        {
			yield return Unknown;
		}

		/// <summary>Unknown</summary>
		public static readonly AssetContactType Unknown = new AssetContactType("Unknown", "Unknown");
	}
	
	public class AssetType : LookupBase
	{
		private AssetType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<AssetType> GetAll()
        {
			yield return Loan;
			yield return Property;
		}

		/// <summary>Loan</summary>
		public static readonly AssetType Loan = new AssetType("Loan", "Loan");
		/// <summary>Property</summary>
		public static readonly AssetType Property = new AssetType("Property", "Property");
	}
	
	public class BidStatus : LookupBase
	{
		private BidStatus(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<BidStatus> GetAll()
        {
			yield return B;
			yield return Bf;
			yield return D;
			yield return W;
		}

		/// <summary>Bridesmaid</summary>
		public static readonly BidStatus B = new BidStatus("B", "Bridesmaid");
		/// <summary>Best and Final</summary>
		public static readonly BidStatus Bf = new BidStatus("BF", "Best and Final");
		/// <summary>Declined</summary>
		public static readonly BidStatus D = new BidStatus("D", "Declined");
		/// <summary>Bid Awarded</summary>
		public static readonly BidStatus W = new BidStatus("W", "Bid Awarded");
	}
	
	public class DealContactType : LookupBase
	{
		private DealContactType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<DealContactType> GetAll()
        {
			yield return Acquisitions;
			yield return AssetManagement;
			yield return Development;
			yield return Dispositions;
			yield return ExecutiveCSuite;
			yield return FinancingDept;
			yield return Leasing;
			yield return PortfolioManagement;
		}

		/// <summary>Acquisitions</summary>
		public static readonly DealContactType Acquisitions = new DealContactType("Acquisitions", "Acquisitions");
		/// <summary>Asset Management</summary>
		public static readonly DealContactType AssetManagement = new DealContactType("Asset Management", "Asset Management");
		/// <summary>Development</summary>
		public static readonly DealContactType Development = new DealContactType("Development", "Development");
		/// <summary>Dispositions</summary>
		public static readonly DealContactType Dispositions = new DealContactType("Dispositions", "Dispositions");
		/// <summary>Executive - (C Suite)</summary>
		public static readonly DealContactType ExecutiveCSuite = new DealContactType("Executive - (C Suite)", "Executive - (C Suite)");
		/// <summary>Financing/Dept</summary>
		public static readonly DealContactType FinancingDept = new DealContactType("Financing/Dept", "Financing/Dept");
		/// <summary>Leasing</summary>
		public static readonly DealContactType Leasing = new DealContactType("Leasing", "Leasing");
		/// <summary>Portfolio Management</summary>
		public static readonly DealContactType PortfolioManagement = new DealContactType("Portfolio Management", "Portfolio Management");
	}
	
	public class DealParticipationType : LookupBase
	{
		private DealParticipationType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<DealParticipationType> GetAll()
        {
			yield return Borrower;
			yield return Broker;
			yield return Buyer;
			yield return Lender;
			yield return Seller;
		}

		/// <summary>Borrower</summary>
		public static readonly DealParticipationType Borrower = new DealParticipationType("Borrower", "Borrower");
		/// <summary>Broker</summary>
		public static readonly DealParticipationType Broker = new DealParticipationType("Broker", "Broker");
		/// <summary>Buyer</summary>
		public static readonly DealParticipationType Buyer = new DealParticipationType("Buyer", "Buyer");
		/// <summary>Lender</summary>
		public static readonly DealParticipationType Lender = new DealParticipationType("Lender", "Lender");
		/// <summary>Seller</summary>
		public static readonly DealParticipationType Seller = new DealParticipationType("Seller", "Seller");
	}
	
	public class DealStatus : LookupBase
	{
		private DealStatus(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<DealStatus> GetAll()
        {
			yield return Targeting;
			yield return Pitching;
			yield return PitchedAndLost;
			yield return OnHold;
			yield return Packaging;
			yield return Marketing;
			yield return BidReceipt;
			yield return UnderAgreement;
			yield return NonRefundable;
			yield return WithdrawnByClient;
			yield return WithdrawnByCounterparty;
			yield return Closed;
		}

		/// <summary>Targeting</summary>
		public static readonly DealStatus Targeting = new DealStatus("Targeting", "Targeting");
		/// <summary>Pitching</summary>
		public static readonly DealStatus Pitching = new DealStatus("Pitching", "Pitching");
		/// <summary>Pitched and Lost</summary>
		public static readonly DealStatus PitchedAndLost = new DealStatus("Pitched and Lost", "Pitched and Lost");
		/// <summary>On Hold</summary>
		public static readonly DealStatus OnHold = new DealStatus("On Hold", "On Hold");
		/// <summary>Packaging</summary>
		public static readonly DealStatus Packaging = new DealStatus("Packaging", "Packaging");
		/// <summary>Marketing</summary>
		public static readonly DealStatus Marketing = new DealStatus("Marketing", "Marketing");
		/// <summary>Bid Receipt</summary>
		public static readonly DealStatus BidReceipt = new DealStatus("Bid Receipt", "Bid Receipt");
		/// <summary>Under Agreement</summary>
		public static readonly DealStatus UnderAgreement = new DealStatus("Under Agreement", "Under Agreement");
		/// <summary>Non-Refundable</summary>
		public static readonly DealStatus NonRefundable = new DealStatus("Non-Refundable", "Non-Refundable");
		/// <summary>Withdrawn by Client</summary>
		public static readonly DealStatus WithdrawnByClient = new DealStatus("Withdrawn by Client", "Withdrawn by Client");
		/// <summary>Withdrawn by Counterparty</summary>
		public static readonly DealStatus WithdrawnByCounterparty = new DealStatus("Withdrawn by Counterparty", "Withdrawn by Counterparty");
		/// <summary>Closed</summary>
		public static readonly DealStatus Closed = new DealStatus("Closed", "Closed");
	}
	
	public class DealTeamRole : LookupBase
	{
		private DealTeamRole(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<DealTeamRole> GetAll()
        {
			yield return Primary;
		}

		/// <summary>PRIMARY</summary>
		public static readonly DealTeamRole Primary = new DealTeamRole("PRIMARY", "PRIMARY");
	}
	
	public class DealType : LookupBase
	{
		private DealType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<DealType> GetAll()
        {
			yield return DebtPlacement;
			yield return EquitySaleLessThan100;
			yield return EquitySale100;
			yield return InvestmentBanking;
			yield return LoanSale;
			yield return NonTransactionalAdvisory;
		}

		/// <summary>Debt Placement</summary>
		public static readonly DealType DebtPlacement = new DealType("Debt Placement", "Debt Placement");
		/// <summary>Equity Sale <100%</summary>
		public static readonly DealType EquitySaleLessThan100 = new DealType("Equity Sale <100%", "Equity Sale <100%");
		/// <summary>Equity Sale 100%</summary>
		public static readonly DealType EquitySale100 = new DealType("Equity Sale 100%", "Equity Sale 100%");
		/// <summary>Investment Banking</summary>
		public static readonly DealType InvestmentBanking = new DealType("Investment Banking", "Investment Banking");
		/// <summary>Loan Sale</summary>
		public static readonly DealType LoanSale = new DealType("Loan Sale", "Loan Sale");
		/// <summary>Non-transactional Advisory</summary>
		public static readonly DealType NonTransactionalAdvisory = new DealType("Non-transactional Advisory", "Non-transactional Advisory");
	}
	
	public class DealValuationRiskProfile : LookupBase
	{
		private DealValuationRiskProfile(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<DealValuationRiskProfile> GetAll()
        {
			yield return Core;
			yield return CorePlus;
			yield return Trophy;
			yield return ValueAdd;
		}

		/// <summary>Core</summary>
		public static readonly DealValuationRiskProfile Core = new DealValuationRiskProfile("Core", "Core");
		/// <summary>Core-Plus</summary>
		public static readonly DealValuationRiskProfile CorePlus = new DealValuationRiskProfile("Core-Plus", "Core-Plus");
		/// <summary>Trophy</summary>
		public static readonly DealValuationRiskProfile Trophy = new DealValuationRiskProfile("Trophy", "Trophy");
		/// <summary>Value-Add</summary>
		public static readonly DealValuationRiskProfile ValueAdd = new DealValuationRiskProfile("Value-Add", "Value-Add");
	}
	
	public class DealValuationType : LookupBase
	{
		private DealValuationType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<DealValuationType> GetAll()
        {
			yield return Basecase;
			yield return Bid;
			yield return Customname;
			yield return Pushcase;
		}

		/// <summary>Base Case</summary>
		public static readonly DealValuationType Basecase = new DealValuationType("BaseCase", "Base Case");
		/// <summary>Bid</summary>
		public static readonly DealValuationType Bid = new DealValuationType("Bid", "Bid");
		/// <summary>Custom Name</summary>
		public static readonly DealValuationType Customname = new DealValuationType("CustomName", "Custom Name");
		/// <summary>Push Case</summary>
		public static readonly DealValuationType Pushcase = new DealValuationType("PushCase", "Push Case");
	}
	
	public class Feature : LookupBase
	{
		private Feature(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<Feature> GetAll()
        {
			yield return Application;
			yield return Bids;
			yield return Fees;
		}

		/// <summary>application</summary>
		public static readonly Feature Application = new Feature("application", "application");
		/// <summary>Bids</summary>
		public static readonly Feature Bids = new Feature("Bids", "Bids");
		/// <summary>Fees</summary>
		public static readonly Feature Fees = new Feature("Fees", "Fees");
	}
	
	public class LegalEntity : LookupBase
	{
		private LegalEntity(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<LegalEntity> GetAll()
        {
			yield return Lgl;
		}

		/// <summary>LGL</summary>
		public static readonly LegalEntity Lgl = new LegalEntity("LGL", "LGL");
	}
	
	public class LoanBenchmarkType : LookupBase
	{
		private LoanBenchmarkType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<LoanBenchmarkType> GetAll()
        {
			yield return E;
			yield return L;
			yield return Na;
			yield return S;
			yield return T;
		}

		/// <summary>Euribor</summary>
		public static readonly LoanBenchmarkType E = new LoanBenchmarkType("E", "Euribor");
		/// <summary>Libor</summary>
		public static readonly LoanBenchmarkType L = new LoanBenchmarkType("L", "Libor");
		/// <summary>Not Available</summary>
		public static readonly LoanBenchmarkType Na = new LoanBenchmarkType("NA", "Not Available");
		/// <summary>Swap</summary>
		public static readonly LoanBenchmarkType S = new LoanBenchmarkType("S", "Swap");
		/// <summary>Displayed as Treasury</summary>
		public static readonly LoanBenchmarkType T = new LoanBenchmarkType("T", "Displayed as Treasury");
	}
	
	public class LoanCashMgmtType : LookupBase
	{
		private LoanCashMgmtType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<LoanCashMgmtType> GetAll()
        {
			yield return Dscr;
			yield return Dy;
			yield return Ltv;
		}

		/// <summary>Debt service coverage ratio</summary>
		public static readonly LoanCashMgmtType Dscr = new LoanCashMgmtType("DSCR", "Debt service coverage ratio");
		/// <summary>Debt Yield</summary>
		public static readonly LoanCashMgmtType Dy = new LoanCashMgmtType("DY", "Debt Yield");
		/// <summary>Loan to Value</summary>
		public static readonly LoanCashMgmtType Ltv = new LoanCashMgmtType("LTV", "Loan to Value");
	}
	
	public class LoanLenderType : LookupBase
	{
		private LoanLenderType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<LoanLenderType> GetAll()
        {
			yield return Db;
			yield return Df;
			yield return Fb;
			yield return Lc;
		}

		/// <summary>Domestic Bank</summary>
		public static readonly LoanLenderType Db = new LoanLenderType("DB", "Domestic Bank");
		/// <summary>Debt Fund</summary>
		public static readonly LoanLenderType Df = new LoanLenderType("DF", "Debt Fund");
		/// <summary>Foreign Bank</summary>
		public static readonly LoanLenderType Fb = new LoanLenderType("FB", "Foreign Bank");
		/// <summary>Life Co</summary>
		public static readonly LoanLenderType Lc = new LoanLenderType("LC", "Life Co");
	}
	
	public class LoanPaymentType : LookupBase
	{
		private LoanPaymentType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<LoanPaymentType> GetAll()
        {
			yield return Df;
			yield return L;
			yield return O;
			yield return Pb;
			yield return Sp;
			yield return Ym;
		}

		/// <summary>Defeasance</summary>
		public static readonly LoanPaymentType Df = new LoanPaymentType("DF", "Defeasance");
		/// <summary>Lockout</summary>
		public static readonly LoanPaymentType L = new LoanPaymentType("L", "Lockout");
		/// <summary>Open</summary>
		public static readonly LoanPaymentType O = new LoanPaymentType("O", "Open");
		/// <summary>% of Balance</summary>
		public static readonly LoanPaymentType Pb = new LoanPaymentType("PB", "% of Balance");
		/// <summary>Spread Maintenance</summary>
		public static readonly LoanPaymentType Sp = new LoanPaymentType("SP", "Spread Maintenance");
		/// <summary>Yield Maintenance</summary>
		public static readonly LoanPaymentType Ym = new LoanPaymentType("YM", "Yield Maintenance");
	}
	
	public class LoanRateType : LookupBase
	{
		private LoanRateType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<LoanRateType> GetAll()
        {
			yield return E;
			yield return L;
			yield return S;
			yield return T;
		}

		/// <summary>Euribor</summary>
		public static readonly LoanRateType E = new LoanRateType("E", "Euribor");
		/// <summary>Libor</summary>
		public static readonly LoanRateType L = new LoanRateType("L", "Libor");
		/// <summary>Swap</summary>
		public static readonly LoanRateType S = new LoanRateType("S", "Swap");
		/// <summary>Treasury</summary>
		public static readonly LoanRateType T = new LoanRateType("T", "Treasury");
	}
	
	public class LoanSourceType : LookupBase
	{
		private LoanSourceType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<LoanSourceType> GetAll()
        {
			yield return A;
			yield return R;
		}

		/// <summary>Aquisition</summary>
		public static readonly LoanSourceType A = new LoanSourceType("A", "Aquisition");
		/// <summary>Refinance</summary>
		public static readonly LoanSourceType R = new LoanSourceType("R", "Refinance");
	}
	
	public class LoanTermDateType : LookupBase
	{
		private LoanTermDateType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<LoanTermDateType> GetAll()
        {
			yield return Finalmaturity;
			yield return Lockoutend;
			yield return Nextmaturity;
			yield return Yieldmaintenanceend;
		}

		/// <summary>Final Maturity</summary>
		public static readonly LoanTermDateType Finalmaturity = new LoanTermDateType("FinalMaturity", "Final Maturity");
		/// <summary>Lockout End</summary>
		public static readonly LoanTermDateType Lockoutend = new LoanTermDateType("LockoutEnd", "Lockout End");
		/// <summary>Next Maturity</summary>
		public static readonly LoanTermDateType Nextmaturity = new LoanTermDateType("NextMaturity", "Next Maturity");
		/// <summary>Yield Maintenance End</summary>
		public static readonly LoanTermDateType Yieldmaintenanceend = new LoanTermDateType("YieldMaintenanceEnd", "Yield Maintenance End");
	}
	
	public class MarketingListActionType : LookupBase
	{
		private MarketingListActionType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<MarketingListActionType> GetAll()
        {
			yield return CalledLeftMsg;
			yield return Contacted;
			yield return TourScheduled;
		}

		/// <summary>Called Left Msg</summary>
		public static readonly MarketingListActionType CalledLeftMsg = new MarketingListActionType("Called Left Msg", "Called Left Msg");
		/// <summary>Contacted</summary>
		public static readonly MarketingListActionType Contacted = new MarketingListActionType("Contacted", "Contacted");
		/// <summary>Tour Scheduled</summary>
		public static readonly MarketingListActionType TourScheduled = new MarketingListActionType("Tour Scheduled", "Tour Scheduled");
	}
	
	public class MarketingListStatusType : LookupBase
	{
		private MarketingListStatusType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<MarketingListStatusType> GetAll()
        {
			yield return AdditionalMaterials;
			yield return CaApproved;
			yield return CaFollowUpSignature;
			yield return CaFollowUpTerms;
			yield return CallForOffers;
			yield return PrintedOm;
			yield return TeaserSent;
			yield return WarRoomAccess;
		}

		/// <summary>Additional Materials</summary>
		public static readonly MarketingListStatusType AdditionalMaterials = new MarketingListStatusType("Additional Materials", "Additional Materials");
		/// <summary>CA Approved</summary>
		public static readonly MarketingListStatusType CaApproved = new MarketingListStatusType("CA Approved", "CA Approved");
		/// <summary>CA Follow Up Signature</summary>
		public static readonly MarketingListStatusType CaFollowUpSignature = new MarketingListStatusType("CA Follow Up Signature", "CA Follow Up Signature");
		/// <summary>CA Follow Up Terms</summary>
		public static readonly MarketingListStatusType CaFollowUpTerms = new MarketingListStatusType("CA Follow Up Terms", "CA Follow Up Terms");
		/// <summary>Call for Offers</summary>
		public static readonly MarketingListStatusType CallForOffers = new MarketingListStatusType("Call for Offers", "Call for Offers");
		/// <summary>Printed OM</summary>
		public static readonly MarketingListStatusType PrintedOm = new MarketingListStatusType("Printed OM", "Printed OM");
		/// <summary>Teaser Sent</summary>
		public static readonly MarketingListStatusType TeaserSent = new MarketingListStatusType("Teaser Sent", "Teaser Sent");
		/// <summary>War Room Access</summary>
		public static readonly MarketingListStatusType WarRoomAccess = new MarketingListStatusType("War Room Access", "War Room Access");
	}
	
	public class MarketType : LookupBase
	{
		private MarketType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<MarketType> GetAll()
        {
			yield return Msa;
			yield return Region;
			yield return State;
		}

		/// <summary>Metro Statistical Area</summary>
		public static readonly MarketType Msa = new MarketType("MSA", "Metro Statistical Area");
		/// <summary>U.S. Region</summary>
		public static readonly MarketType Region = new MarketType("Region", "U.S. Region");
		/// <summary>US State Name</summary>
		public static readonly MarketType State = new MarketType("State", "US State Name");
	}
	
	public class PhysicalAddressType : LookupBase
	{
		private PhysicalAddressType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<PhysicalAddressType> GetAll()
        {
			yield return Blank;
			yield return Alt;
			yield return Alternate;
			yield return Business;
			yield return Business2;
			yield return Caleast;
			yield return Chicago;
			yield return China;
			yield return Colorado;
			yield return Company;
			yield return Eastdil;
			yield return Fedex;
			yield return Havorvord;
			yield return Home;
			yield return Japan;
			yield return London;
			yield return Mailing;
			yield return Main;
			yield return Maybe;
			yield return New;
			yield return Ny;
			yield return Office;
			yield return Overnight;
			yield return Physical;
			yield return Po;
			yield return Primary;
			yield return Purchase;
			yield return Secondary;
			yield return Sf;
			yield return Shipping;
			yield return South;
			yield return Street;
			yield return Temp;
			yield return Toronto;
			yield return Usps;
			yield return Work;
		}

		/// <summary>Blank</summary>
		public static readonly PhysicalAddressType Blank = new PhysicalAddressType("Blank", "Blank");
		/// <summary>Alt</summary>
		public static readonly PhysicalAddressType Alt = new PhysicalAddressType("Alt", "Alt");
		/// <summary>Alternate</summary>
		public static readonly PhysicalAddressType Alternate = new PhysicalAddressType("Alternate", "Alternate");
		/// <summary>Business</summary>
		public static readonly PhysicalAddressType Business = new PhysicalAddressType("Business", "Business");
		/// <summary>Business2</summary>
		public static readonly PhysicalAddressType Business2 = new PhysicalAddressType("Business2", "Business2");
		/// <summary>CalEast</summary>
		public static readonly PhysicalAddressType Caleast = new PhysicalAddressType("CalEast", "CalEast");
		/// <summary>Chicago</summary>
		public static readonly PhysicalAddressType Chicago = new PhysicalAddressType("Chicago", "Chicago");
		/// <summary>China</summary>
		public static readonly PhysicalAddressType China = new PhysicalAddressType("China", "China");
		/// <summary>Colorado</summary>
		public static readonly PhysicalAddressType Colorado = new PhysicalAddressType("Colorado", "Colorado");
		/// <summary>Company</summary>
		public static readonly PhysicalAddressType Company = new PhysicalAddressType("Company", "Company");
		/// <summary>Eastdil</summary>
		public static readonly PhysicalAddressType Eastdil = new PhysicalAddressType("Eastdil", "Eastdil");
		/// <summary>Fedex</summary>
		public static readonly PhysicalAddressType Fedex = new PhysicalAddressType("Fedex", "Fedex");
		/// <summary>Havorvord</summary>
		public static readonly PhysicalAddressType Havorvord = new PhysicalAddressType("Havorvord", "Havorvord");
		/// <summary>Home</summary>
		public static readonly PhysicalAddressType Home = new PhysicalAddressType("Home", "Home");
		/// <summary>Japan</summary>
		public static readonly PhysicalAddressType Japan = new PhysicalAddressType("Japan", "Japan");
		/// <summary>London</summary>
		public static readonly PhysicalAddressType London = new PhysicalAddressType("London", "London");
		/// <summary>Mailing</summary>
		public static readonly PhysicalAddressType Mailing = new PhysicalAddressType("Mailing", "Mailing");
		/// <summary>Main</summary>
		public static readonly PhysicalAddressType Main = new PhysicalAddressType("Main", "Main");
		/// <summary>maybe</summary>
		public static readonly PhysicalAddressType Maybe = new PhysicalAddressType("maybe", "maybe");
		/// <summary>New</summary>
		public static readonly PhysicalAddressType New = new PhysicalAddressType("New", "New");
		/// <summary>NY</summary>
		public static readonly PhysicalAddressType Ny = new PhysicalAddressType("NY", "NY");
		/// <summary>Office</summary>
		public static readonly PhysicalAddressType Office = new PhysicalAddressType("Office", "Office");
		/// <summary>Overnight</summary>
		public static readonly PhysicalAddressType Overnight = new PhysicalAddressType("Overnight", "Overnight");
		/// <summary>Physical</summary>
		public static readonly PhysicalAddressType Physical = new PhysicalAddressType("Physical", "Physical");
		/// <summary>PO</summary>
		public static readonly PhysicalAddressType Po = new PhysicalAddressType("PO", "PO");
		/// <summary>Primary</summary>
		public static readonly PhysicalAddressType Primary = new PhysicalAddressType("Primary", "Primary");
		/// <summary>Purchase</summary>
		public static readonly PhysicalAddressType Purchase = new PhysicalAddressType("Purchase", "Purchase");
		/// <summary>Secondary</summary>
		public static readonly PhysicalAddressType Secondary = new PhysicalAddressType("Secondary", "Secondary");
		/// <summary>SF</summary>
		public static readonly PhysicalAddressType Sf = new PhysicalAddressType("SF", "SF");
		/// <summary>Shipping</summary>
		public static readonly PhysicalAddressType Shipping = new PhysicalAddressType("Shipping", "Shipping");
		/// <summary>South</summary>
		public static readonly PhysicalAddressType South = new PhysicalAddressType("South", "South");
		/// <summary>Street</summary>
		public static readonly PhysicalAddressType Street = new PhysicalAddressType("Street", "Street");
		/// <summary>Temp</summary>
		public static readonly PhysicalAddressType Temp = new PhysicalAddressType("Temp", "Temp");
		/// <summary>Toronto</summary>
		public static readonly PhysicalAddressType Toronto = new PhysicalAddressType("Toronto", "Toronto");
		/// <summary>USPS</summary>
		public static readonly PhysicalAddressType Usps = new PhysicalAddressType("USPS", "USPS");
		/// <summary>Work</summary>
		public static readonly PhysicalAddressType Work = new PhysicalAddressType("Work", "Work");
	}
	
	public class PreferenceType : LookupBase
	{
		private PreferenceType(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<PreferenceType> GetAll()
        {
			yield return Startpage;
		}

		/// <summary>Dashboard</summary>
		public static readonly PreferenceType Startpage = new PreferenceType("STARTPAGE", "Dashboard");
	}
	
	public class Role : LookupBase
	{
		private Role(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<Role> GetAll()
        {
			yield return DtcaWsdEsxUsers;
			yield return DtcgWsdEspreportsFees;
			yield return DtcgWsdusaEspreportsBids;
		}

		/// <summary>DTCA_WSD_ESX_USERS</summary>
		public static readonly Role DtcaWsdEsxUsers = new Role("DTCA_WSD_ESX_USERS", "DTCA_WSD_ESX_USERS");
		/// <summary>DTCG_WSD_ESPREPORTS_FEES</summary>
		public static readonly Role DtcgWsdEspreportsFees = new Role("DTCG_WSD_ESPREPORTS_FEES", "DTCG_WSD_ESPREPORTS_FEES");
		/// <summary>DTCG_WSDUSA_ESPREPORTS_BIDS</summary>
		public static readonly Role DtcgWsdusaEspreportsBids = new Role("DTCG_WSDUSA_ESPREPORTS_BIDS", "DTCG_WSDUSA_ESPREPORTS_BIDS");
	}
	
	public class StateProvidence : LookupBase
	{
		private StateProvidence(string code, string label) : base(code, label)
		{
		}

		public static IEnumerable<StateProvidence> GetAll()
        {
			yield return Ab;
			yield return Ak;
			yield return Al;
			yield return Ar;
			yield return Az;
			yield return Bc;
			yield return Ca;
			yield return Co;
			yield return Ct;
			yield return Dc;
			yield return De;
			yield return Fl;
			yield return Ga;
			yield return Hi;
			yield return Ia;
			yield return Id;
			yield return Il;
			yield return In;
			yield return Ks;
			yield return Ky;
			yield return La;
			yield return Ma;
			yield return Mb;
			yield return Md;
			yield return Me;
			yield return Mi;
			yield return Mn;
			yield return Mo;
			yield return Ms;
			yield return Mt;
			yield return Nb;
			yield return Nc;
			yield return Nd;
			yield return Ne;
			yield return Nh;
			yield return Nj;
			yield return Nl;
			yield return Nm;
			yield return Ns;
			yield return Nv;
			yield return Ny;
			yield return Oh;
			yield return Ok;
			yield return On;
			yield return Or;
			yield return Pa;
			yield return Pe;
			yield return Pr;
			yield return Qc;
			yield return Ri;
			yield return Sc;
			yield return Sd;
			yield return Sk;
			yield return Tn;
			yield return Tx;
			yield return Ut;
			yield return Va;
			yield return Vt;
			yield return Wa;
			yield return Wi;
			yield return Wv;
			yield return Wy;
		}

		/// <summary>AB</summary>
		public static readonly StateProvidence Ab = new StateProvidence("AB", "AB");
		/// <summary>AK</summary>
		public static readonly StateProvidence Ak = new StateProvidence("AK", "AK");
		/// <summary>AL</summary>
		public static readonly StateProvidence Al = new StateProvidence("AL", "AL");
		/// <summary>AR</summary>
		public static readonly StateProvidence Ar = new StateProvidence("AR", "AR");
		/// <summary>AZ</summary>
		public static readonly StateProvidence Az = new StateProvidence("AZ", "AZ");
		/// <summary>BC</summary>
		public static readonly StateProvidence Bc = new StateProvidence("BC", "BC");
		/// <summary>CA</summary>
		public static readonly StateProvidence Ca = new StateProvidence("CA", "CA");
		/// <summary>CO</summary>
		public static readonly StateProvidence Co = new StateProvidence("CO", "CO");
		/// <summary>CT</summary>
		public static readonly StateProvidence Ct = new StateProvidence("CT", "CT");
		/// <summary>DC</summary>
		public static readonly StateProvidence Dc = new StateProvidence("DC", "DC");
		/// <summary>DE</summary>
		public static readonly StateProvidence De = new StateProvidence("DE", "DE");
		/// <summary>FL</summary>
		public static readonly StateProvidence Fl = new StateProvidence("FL", "FL");
		/// <summary>GA</summary>
		public static readonly StateProvidence Ga = new StateProvidence("GA", "GA");
		/// <summary>HI</summary>
		public static readonly StateProvidence Hi = new StateProvidence("HI", "HI");
		/// <summary>IA</summary>
		public static readonly StateProvidence Ia = new StateProvidence("IA", "IA");
		/// <summary>ID</summary>
		public static readonly StateProvidence Id = new StateProvidence("ID", "ID");
		/// <summary>IL</summary>
		public static readonly StateProvidence Il = new StateProvidence("IL", "IL");
		/// <summary>IN</summary>
		public static readonly StateProvidence In = new StateProvidence("IN", "IN");
		/// <summary>KS</summary>
		public static readonly StateProvidence Ks = new StateProvidence("KS", "KS");
		/// <summary>KY</summary>
		public static readonly StateProvidence Ky = new StateProvidence("KY", "KY");
		/// <summary>LA</summary>
		public static readonly StateProvidence La = new StateProvidence("LA", "LA");
		/// <summary>MA</summary>
		public static readonly StateProvidence Ma = new StateProvidence("MA", "MA");
		/// <summary>MB</summary>
		public static readonly StateProvidence Mb = new StateProvidence("MB", "MB");
		/// <summary>MD</summary>
		public static readonly StateProvidence Md = new StateProvidence("MD", "MD");
		/// <summary>ME</summary>
		public static readonly StateProvidence Me = new StateProvidence("ME", "ME");
		/// <summary>MI</summary>
		public static readonly StateProvidence Mi = new StateProvidence("MI", "MI");
		/// <summary>MN</summary>
		public static readonly StateProvidence Mn = new StateProvidence("MN", "MN");
		/// <summary>MO</summary>
		public static readonly StateProvidence Mo = new StateProvidence("MO", "MO");
		/// <summary>MS</summary>
		public static readonly StateProvidence Ms = new StateProvidence("MS", "MS");
		/// <summary>MT</summary>
		public static readonly StateProvidence Mt = new StateProvidence("MT", "MT");
		/// <summary>NB</summary>
		public static readonly StateProvidence Nb = new StateProvidence("NB", "NB");
		/// <summary>NC</summary>
		public static readonly StateProvidence Nc = new StateProvidence("NC", "NC");
		/// <summary>ND</summary>
		public static readonly StateProvidence Nd = new StateProvidence("ND", "ND");
		/// <summary>NE</summary>
		public static readonly StateProvidence Ne = new StateProvidence("NE", "NE");
		/// <summary>NH</summary>
		public static readonly StateProvidence Nh = new StateProvidence("NH", "NH");
		/// <summary>NJ</summary>
		public static readonly StateProvidence Nj = new StateProvidence("NJ", "NJ");
		/// <summary>NL</summary>
		public static readonly StateProvidence Nl = new StateProvidence("NL", "NL");
		/// <summary>NM</summary>
		public static readonly StateProvidence Nm = new StateProvidence("NM", "NM");
		/// <summary>NS</summary>
		public static readonly StateProvidence Ns = new StateProvidence("NS", "NS");
		/// <summary>NV</summary>
		public static readonly StateProvidence Nv = new StateProvidence("NV", "NV");
		/// <summary>NY</summary>
		public static readonly StateProvidence Ny = new StateProvidence("NY", "NY");
		/// <summary>OH</summary>
		public static readonly StateProvidence Oh = new StateProvidence("OH", "OH");
		/// <summary>OK</summary>
		public static readonly StateProvidence Ok = new StateProvidence("OK", "OK");
		/// <summary>ON</summary>
		public static readonly StateProvidence On = new StateProvidence("ON", "ON");
		/// <summary>OR</summary>
		public static readonly StateProvidence Or = new StateProvidence("OR", "OR");
		/// <summary>PA</summary>
		public static readonly StateProvidence Pa = new StateProvidence("PA", "PA");
		/// <summary>PE</summary>
		public static readonly StateProvidence Pe = new StateProvidence("PE", "PE");
		/// <summary>PR</summary>
		public static readonly StateProvidence Pr = new StateProvidence("PR", "PR");
		/// <summary>QC</summary>
		public static readonly StateProvidence Qc = new StateProvidence("QC", "QC");
		/// <summary>RI</summary>
		public static readonly StateProvidence Ri = new StateProvidence("RI", "RI");
		/// <summary>SC</summary>
		public static readonly StateProvidence Sc = new StateProvidence("SC", "SC");
		/// <summary>SD</summary>
		public static readonly StateProvidence Sd = new StateProvidence("SD", "SD");
		/// <summary>SK</summary>
		public static readonly StateProvidence Sk = new StateProvidence("SK", "SK");
		/// <summary>TN</summary>
		public static readonly StateProvidence Tn = new StateProvidence("TN", "TN");
		/// <summary>TX</summary>
		public static readonly StateProvidence Tx = new StateProvidence("TX", "TX");
		/// <summary>UT</summary>
		public static readonly StateProvidence Ut = new StateProvidence("UT", "UT");
		/// <summary>VA</summary>
		public static readonly StateProvidence Va = new StateProvidence("VA", "VA");
		/// <summary>VT</summary>
		public static readonly StateProvidence Vt = new StateProvidence("VT", "VT");
		/// <summary>WA</summary>
		public static readonly StateProvidence Wa = new StateProvidence("WA", "WA");
		/// <summary>WI</summary>
		public static readonly StateProvidence Wi = new StateProvidence("WI", "WI");
		/// <summary>WV</summary>
		public static readonly StateProvidence Wv = new StateProvidence("WV", "WV");
		/// <summary>WY</summary>
		public static readonly StateProvidence Wy = new StateProvidence("WY", "WY");
	}
	
 
}

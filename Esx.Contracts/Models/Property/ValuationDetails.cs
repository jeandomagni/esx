﻿using System;

namespace Esx.Contracts.Models.Property
{
    public class ValuationDetails
    {
        public int? ValuationDataKey { get; set; }
        public DateTime? ValuationDate { get; set; }        
        public decimal? AskingPrice { get; set; } 
        public decimal? UnitPrice { get; set; }
        public string DealCode { get; set; }
        public string DealName { get; set; }
        public string Buyer { get; set; }
    }
}

﻿using System;

namespace Esx.Contracts.Models.Property
{
    public class PropertyListItem
    {

        public int PropertyID { get; set; }

        public string MentionId { get; set; }

        public bool IsWatched { get; set; }

        public string PropertyName { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Type { get; set; }

        public decimal? Size { get; set; }

        public string SizeUnits { get; set; }

        public DateTime? LastCloseDate { get; set; }

        public decimal? Valuation { get; set; }

        public bool ActiveDeal { get; set; }

        public string DealCode { get; set; }

        public bool JointVenture { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }
    }
}

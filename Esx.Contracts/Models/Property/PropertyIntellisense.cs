﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Property
{
    public class PropertyIntellisense
    {
        public int PropertyKey { get; set; }

        public int PortfolioKey { get; set; }

        public string MentionId { get; set; }

        public string PropertyName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Address4 { get; set; }
    }
}

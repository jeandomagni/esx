﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Property
{
    public class NewProperty
    {
        public string MentionId { get; set; }
        public string PropertyName { get; set;}
        public string Campus { get; set; }
        public string FormattedAddress { get; set; }
        public string Address { get; set; }
        public string City{ get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public double Latiude { get; set; }
        public double Longitude { get; set; }

    }
}

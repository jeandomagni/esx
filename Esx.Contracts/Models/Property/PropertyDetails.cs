﻿using System;
using System.Collections.Generic;
using Esx.Contracts.Models.Location;

namespace Esx.Contracts.Models.Property
{
    public class PropertyDetails
    {
        public int PropertyKey { get; set; }

        public string MentionId { get; set; }

        public bool IsWatched { get; set; }

        public string PropertyName { get; set; }

        public PhysicalAddress PhysicalAddress { get; set; }

        public string Type { get; set; }

        public decimal? Size { get; set; }

        public string SizeUnits { get; set; }

        public DateTime? LastCloseDate { get; set; }

        public IEnumerable<ValuationDetails> ValuationHistory { get; set; }

        public bool ActiveDeal { get; set; }

        public string Owner { get; set; }

        public string AlternateName { get; set; }

        public DateTime? OpenedDate { get; set; }

        public int? Floors { get; set; }

        public string Region { get; set; }

        public string SubMarket { get; set; }

        public string PortfolioName { get; set; }

        public string Campus { get; set; }

        public string RiskProfile { get; set; }

        public string Country { get; set; }
        public decimal? Valuation { get; set; }
    }
}

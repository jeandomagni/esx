﻿namespace Esx.Contracts.Models.Tag
{
    public class TagNote
    {
        public int CommentID { get; set; }
        public System.DateTime CreateTime { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public string CommentText { get; set; }
        public string PrimaryMentionID { get; set; }
        public string TableCode { get; set; }
    }
}

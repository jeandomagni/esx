﻿
namespace Esx.Contracts.Models.Tag
{
    public class Tag
    {
        public string TagName { get; set; }
        public int Relevance { get; set; }
    }
}

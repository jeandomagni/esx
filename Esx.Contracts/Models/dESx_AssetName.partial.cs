﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Esx.Contracts.Enums;
using Esx.Contracts.Interfaces;

namespace Esx.Contracts.Models
{
    public partial class dESx_AssetName : IMentionEntity
    {
        [NotMapped]
        public string MentionSource => AssetName;

        [NotMapped]
        public int ObjectKey => AssetNameKey;

        [NotMapped]
        public string ObjectType
        {
            get
            {
                if (dESx_Asset.AssetTypeCode == AssetType.Property.Code)
                    return CoreType.Property;
                if (dESx_Asset.AssetTypeCode == AssetType.Loan.Code)
                    return CoreType.Loan;
                throw new NotSupportedException();
            }
        }

        [NotMapped]
        public bool ObjectPrimary => true;

        [NotMapped]
        public string MentionId
        {
            get
            {
                return !string.IsNullOrEmpty(_mentionId) ? _mentionId
                    : dESx_Asset?.AssetTypeCode == CoreType.Property ? dESx_Mentions_PropertyAssetNameKey.FirstOrDefault(x => x.IsPrimary)?.MentionId
                    : dESx_Asset?.AssetTypeCode == CoreType.Loan ? dESx_Mentions_LoanAssetNameKey.FirstOrDefault(x => x.IsPrimary)?.MentionId
                    : null;
            }
            set { _mentionId = value; }
        }

        private string _mentionId;
    }
}

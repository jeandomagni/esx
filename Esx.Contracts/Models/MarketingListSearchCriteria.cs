﻿
using Esx.Contracts.Paging;

namespace Esx.Contracts.Models
{
    public class MarketingListSearchCriteria :PagingData
    {

        public MarketingListSearchCriteria()
        {
        }

        public MarketingListSearchCriteria(PagingData pagingDataToCopy) : base(pagingDataToCopy)
        {
        }

        public int MarketingListId { get; set; }

        public string ContactMentionId { get; set; }
        public string CompanyMentionId { get; set; }

        public string SearchText { get; set; }
        public bool? IsPrimary { get; set; }
        public bool? IsSolicitable { get; set; }
        public bool? IsRequestingTeaser { get; set; }
        public bool? IsRequestingWarRoomAccess { get; set; }
        public bool? IsRequestingAdditionalMaterials { get; set; }
        public bool? IsRequestingPrintedOm { get; set; }
        public bool? IsRequestingCallForOffers { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Esx.Contracts.Models.Company;

namespace Esx.Contracts.Models.Contact
{
    public class ContactDetails
    {
        public bool? IsWatched { get; set; }
        public int ContactId { get; set; }
        public string ContactMentionId { get; set; }
        public string FullName { get; set; }
        public string Title { get; set; }
        public int CompanyKey { get; set; }
        public string CompanyName { get; set; }
        public string CompanyMentionId { get; set; }
        public string MobilePhone { get; set; }
        public string EmailAddress { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Location { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OfficePhone { get; set; }
        public string Website { get; set; }
        public string LinkedInProfileLink { get; set; }
        public bool IsDefunct { get; set; }
        public string DefunctReason { get; set; }
        public CompanySummaryStats SummaryRollup { get; set; }
    }
}

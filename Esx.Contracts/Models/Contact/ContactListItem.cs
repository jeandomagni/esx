﻿namespace Esx.Contracts.Models.Contact
{
    public class ContactListItem
    {
        public int ContactId { get; set; }

        public string FullName { get; set; }

        public string Title { get; set; }

        public string CompanyName { get; set; }

        public string MobilePhone { get; set; }

        public string OfficePhone { get; set; }

        public string EmailAddress { get; set; }

        public string Location { get; set; }        

        public string FirstName { get; set; }

        public string LastName { get; set; }         

        public bool IsWatched { get; set; }

        public string ContactMentionId { get; set; }
    }
}
﻿namespace Esx.Contracts.Models.Search
{
    public class Range<T>
        where T: struct 
    {
        public T? Start { get; set; }

        public T? End { get; set; }
    }
}

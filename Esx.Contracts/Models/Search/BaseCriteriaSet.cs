﻿using System.Collections;
using System.Linq;
using System.Linq.Dynamic;

namespace Esx.Contracts.Models.Search
{
    public abstract class BaseCriteriaSet
    {
        public abstract bool HasValue { get; }

        protected bool CheckValues(params IEnumerable[] enumerables)
            => enumerables != null && enumerables.Any(x => x != null && x.Any());
    }
}

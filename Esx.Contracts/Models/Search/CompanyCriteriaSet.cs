﻿using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace Esx.Contracts.Models.Search
{
    public class CompanyCriteriaSet : BaseCriteriaSet
    {
        public IEnumerable<string> CompanyNames { get; set; }

        public IEnumerable<string> CompanyDealRoles { get; set; }

        public IEnumerable<DbGeography> PropertyLocations { get; set; }

        public IEnumerable<string> PropertyTypes { get; set; }

        public IEnumerable<string> PropertyRiskProfiles { get; set; }

        public override bool HasValue => CheckValues(CompanyNames, CompanyDealRoles, PropertyLocations, PropertyTypes, PropertyRiskProfiles);
    }
}

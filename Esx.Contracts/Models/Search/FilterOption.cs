﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Search
{
    public class FilterOption
    {
        public string Category { get; set; }
        public List<FilterItem> Filters { get; set; }
    }
}

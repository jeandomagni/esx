﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Search
{
    public class FilterItem
    {
        public string Name { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public List<FilterValue> TypeValues { get; set; }
        public List<FilterValue> Values { get; set; }
    }
}

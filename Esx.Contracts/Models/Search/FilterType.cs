﻿namespace Esx.Contracts.Models.Search
{
    public static class FilterType
    {
        public const string MultiSelect = "multiSelect";
        public const string Input = "input";
        public const string DateRange = "dateRange";
        public const string NumberRange = "numberRange";
        public const string Map = "map";
    }
}

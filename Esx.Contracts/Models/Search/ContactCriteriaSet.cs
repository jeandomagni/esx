﻿using System.Collections.Generic;

namespace Esx.Contracts.Models.Search
{
    public class ContactCriteriaSet : BaseCriteriaSet
    {
        public IEnumerable<string> ContactNames { get; set; }

        public IEnumerable<string> ContactTitles { get; set; }

        public IEnumerable<string> ContactDealRoles { get; set; }

        public IEnumerable<int> EastdilOffices { get; set; } 

        public override bool HasValue => CheckValues(ContactNames, ContactTitles, ContactDealRoles, EastdilOffices);
    }
}

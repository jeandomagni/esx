﻿using System.Collections.Generic;

namespace Esx.Contracts.Models.Search
{
    public class PropertyLocationCriteriaSet : BaseCriteriaSet
    {
        public IEnumerable<string> PropertyContinents { get; set; }

        public IEnumerable<string> PropertyCountries { get; set; }

        public IEnumerable<string> PropertyCities { get; set; }

        public IEnumerable<int> PropertyRegions { get; set; }

        public IEnumerable<string> PropertyMapView { get; set; }

        public override bool HasValue => CheckValues(PropertyContinents, PropertyCountries, PropertyCities, PropertyRegions, PropertyMapView);
    }
}

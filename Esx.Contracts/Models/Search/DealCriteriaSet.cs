﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace Esx.Contracts.Models.Search
{
    public class DealCriteriaSet : BaseCriteriaSet
    {
        public IEnumerable<string> DealTypes { get; set; }

        public IEnumerable<string> StructureTypes { get; set; }

        public IEnumerable<Range<DateTime>> LastCloseDates { get; set; }

        public IEnumerable<string> BidStatuses { get; set; }

        public IEnumerable<Range<DateTime>> BidDates { get; set; }

        public IEnumerable<Range<decimal>> Fees { get; set; }

        public IEnumerable<string> DealStatuses { get; set; }

        public IEnumerable<Range<decimal>> TransactionAmounts { get; set; }

        public IEnumerable<Range<decimal>> Valuations { get; set; }

        public IEnumerable<string> DealNames { get; set; }

        public IEnumerable<Range<decimal>> BidderCounts { get; set; }

        public IEnumerable<string> DealsIncluded { get; set; }

        public IEnumerable<string> PropertyNames { get; set; }

        public IEnumerable<DbGeography> PropertyLocations { get; set; }

        public IEnumerable<int> PropertyTypes { get; set; }

        public IEnumerable<string> PropertyRiskProfiles { get; set; }

        public IEnumerable<string> ContactNames { get; set; } 

        public override bool HasValue => CheckValues(DealTypes, StructureTypes, LastCloseDates, BidStatuses, BidDates, Fees, DealStatuses, TransactionAmounts, Valuations,
            DealNames, BidderCounts, DealsIncluded, PropertyNames, PropertyLocations, PropertyTypes, PropertyRiskProfiles, ContactNames);
    }
}

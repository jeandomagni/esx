﻿using Esx.Contracts.Paging;

namespace Esx.Contracts.Models.Search
{
    public class PropertySearchCriteria : PagingData
    {
        public PropertySearchCriteria()
        {
        }

        public PropertySearchCriteria(PagingData pagingDataToCopy) : base(pagingDataToCopy)
        {
        }

        /// <summary>
        /// Used if searching for company properties.
        /// </summary>
        public string CompanyMentionId { get; set; }

        public string DealMentionId { get; set; }

        /// <summary>
        /// Search for this text in ANY fields.
        /// </summary>
        public string SearchText { get; set; }

        public CompanyCriteriaSet CompanyCriteriaSet { get; set; }

        public DealCriteriaSet DealCriteriaSet { get; set; }

        public LoanCriteriaSet LoanCriteriaSet { get; set; }

        public PropertyCriteriaSet PropertyCriteriaSet { get; set; }

        public PropertyLocationCriteriaSet PropertyLocationCriteriaSet { get; set; }

    }
}

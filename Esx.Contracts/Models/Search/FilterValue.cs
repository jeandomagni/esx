﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Search
{
    public class FilterValue
    {
        public string Label { get; set; }
        public string Value { get; set; }
    }
}

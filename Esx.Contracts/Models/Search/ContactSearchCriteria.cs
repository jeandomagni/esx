﻿using System.Collections.Generic;
using Esx.Contracts.Paging;

namespace Esx.Contracts.Models.Search
{
    public class ContactSearchCriteria : PagingData
    {
        public ContactSearchCriteria()
        {
        }

        public ContactSearchCriteria(PagingData pagingDataToCopy) : base(pagingDataToCopy)
        {
        }

        /// <summary>
        /// Used if searching for company contacts.
        /// </summary>
        public string CompanyMentionId { get; set; }

        /// <summary>
        /// Search for this text in ANY fields.
        /// </summary>
        public string SearchText { get; set; }

        public CompanyCriteriaSet CompanyCriteriaSet { get; set; }

        public ContactCriteriaSet ContactCriteriaSet { get; set; }

        public DealCriteriaSet DealCriteriaSet { get; set; }

    }
}

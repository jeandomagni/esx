﻿using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace Esx.Contracts.Models.Search
{
    public class PropertyCriteriaSet : BaseCriteriaSet
    {
        public IEnumerable<int> PropertyKeys { get; set; }

        public IEnumerable<DbGeography> PropertyLocations { get; set; }
        
        public IEnumerable<string> PropertyNames { get; set; } 

        public IEnumerable<int> PropertyTypes { get; set; }

        public IEnumerable<Range<decimal>> PropertySizes { get; set; }

        public override bool HasValue => CheckValues(PropertyKeys, PropertyLocations, PropertyNames, PropertyTypes, PropertySizes);
    }
}

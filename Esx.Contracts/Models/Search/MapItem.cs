﻿namespace Esx.Contracts.Models.Search
{
    public class MapItem
    {
        public string MentionId { get; set; }
        public string Name { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}

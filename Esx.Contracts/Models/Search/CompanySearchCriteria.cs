﻿using System.Collections.Generic;
using Esx.Contracts.Paging;

namespace Esx.Contracts.Models.Search
{
    public class CompanySearchCriteria : PagingData
    {
        public CompanySearchCriteria()
        {
        }

        public CompanySearchCriteria(PagingData pagingDataToCopy) : base(pagingDataToCopy)
        {
        }

        /// <summary>
        /// Search for this text in ANY fields.
        /// </summary>
        public string SearchText { get; set; }

        /// <summary>
        /// Search only for Primary Aliases?
        /// </summary>
        public bool? SearchPrimaryAlias { get; set; }

        public CompanyCriteriaSet CompanyCriteriaSet { get; set; }

        public DealCriteriaSet DealCriteriaSet { get; set; }

        public LoanCriteriaSet LoanCriteriaSet { get; set; }

        public PropertyCriteriaSet PropertyCriteriaSet { get; set; }

    }
}

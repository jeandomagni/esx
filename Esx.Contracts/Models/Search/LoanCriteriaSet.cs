﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace Esx.Contracts.Models.Search
{
    public class LoanCriteriaSet : BaseCriteriaSet
    {
        public IEnumerable<Range<DateTime>> OriginationDates { get; set; }

        public IEnumerable<Range<DateTime>> LockoutDates { get; set; }

        public IEnumerable<Range<DateTime>> YieldMaintenanceEndDates { get; set; }

        public IEnumerable<Range<DateTime>> NextMaturityDates { get; set; }

        public IEnumerable<Range<DateTime>> FinalMaturityDates { get; set; }

        public IEnumerable<Range<decimal>> LoanAmounts { get; set; }

        public IEnumerable<Range<decimal>> InterestRates { get; set; }

        public IEnumerable<string> RateTypes { get; set; }

        public IEnumerable<Range<decimal>> Ltvs { get; set; }

        public IEnumerable<Range<decimal>> DebtYields { get; set; }

        public IEnumerable<Range<DateTime>> LoanStatusDates { get; set; }

        public IEnumerable<DbGeography> PropertyLocations { get; set; } 

        public IEnumerable<int> PropertyTypes { get; set; } 

        public override bool HasValue =>
                CheckValues(OriginationDates, LockoutDates, YieldMaintenanceEndDates, NextMaturityDates, FinalMaturityDates,
                    LoanAmounts, InterestRates, RateTypes, Ltvs, DebtYields, LoanStatusDates, PropertyLocations, PropertyTypes);
    }
}

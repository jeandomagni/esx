﻿using Esx.Contracts.Paging;

namespace Esx.Contracts.Models.Search
{
    public class LoanSearchCriteria : PagingData
    {
        public LoanSearchCriteria()
        {
        }

        public LoanSearchCriteria(PagingData pagingDataToCopy) : base(pagingDataToCopy)
        {
        }

        /// <summary>
        /// Used if searching for company loans
        /// </summary>
        public string CompanyMentionId { get; set; }

        /// <summary>
        /// Used if searching for property loans.
        /// </summary>
        public string PropertyMentionId { get; set; }

        /// <summary>
        /// Used if searching for deal loans.
        /// </summary>
        public string DealMentionId { get; set; }

        /// <summary>
        /// Search for this text in ANY fields.
        /// </summary>
        public string SearchText { get; set; }

        public CompanyCriteriaSet CompanyCriteriaSet { get; set; }

        public DealCriteriaSet DealCriteriaSet { get; set; }

        public LoanCriteriaSet LoanCriteriaSet { get; set; }

        public PropertyCriteriaSet PropertyCriteriaSet { get; set; }

    }
}

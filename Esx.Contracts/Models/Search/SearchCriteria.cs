﻿using System.Collections.Generic;
using Esx.Contracts.Paging;

namespace Esx.Contracts.Models.Search
{
    public class SearchCriteria : PagingData
    {
        public string CoreObjectType { get; set; }

        public List<FilterItem> Filters { get; set; } 
    }
}

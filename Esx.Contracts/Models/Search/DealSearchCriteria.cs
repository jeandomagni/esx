﻿using Esx.Contracts.Paging;

namespace Esx.Contracts.Models.Search
{
    public class DealSearchCriteria : PagingData
    {
        public DealSearchCriteria()
        {
        }

        public DealSearchCriteria(PagingData pagingDataToCopy) : base(pagingDataToCopy)
        {
        }

        /// <summary>
        /// Used if searching for company deals.
        /// </summary>
        public string CompanyMentionId { get; set; }

        /// <summary>
        /// Used if searching for contact deals.
        /// </summary>
        public string ContactMentionId { get; set; }

        /// <summary>
        /// Used if searching for property deals.
        /// </summary>
        public string PropertyMentionId { get; set; }

        /// <summary>
        /// Search for this text in ANY fields.
        /// </summary>
        public string SearchText { get; set; }

        public bool ShowUserTeamDealsFirst { get; set; }

        public CompanyCriteriaSet CompanyCriteriaSet { get; set; }

        public DealCriteriaSet DealCriteriaSet { get; set; }

        public LoanCriteriaSet LoanCriteriaSet { get; set; }

        public PropertyCriteriaSet PropertyCriteriaSet { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Comment
{
    public class Note
    {
        public int NoteKey { get; set; }
        public int ImplicitMentionKey { get; set; }
        public string NoteText { get; set; }
        public int CreateUserKey { get; set; }
        public System.DateTime CreateTime { get; set; }
        public int UpdateUserKey { get; set; }
        public System.DateTime UpdateTime { get; set; }

        //public virtual Mention Mention { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Note_Mention> Note_Mention { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Note_Tag> Note_Tag { get; set; }
        //public virtual User User { get; set; }
        //public virtual User User1 { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Comment
{
    public class Attachment
    {
        public string Name { get; set; }
        public Guid ExternalKey { get; set; }
    }
}

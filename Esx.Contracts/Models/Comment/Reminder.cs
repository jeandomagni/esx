﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Comment
{
    public class Reminder
    {
        public int? ReminderId { get; set; }
        public int? CommentKey { get; set; }
        public int? MentionKey { get; set; }
        public string ReminderText { get; set; }
        public DateTime? SnoozeDate { get; set; }
        public DateTime? ReminderDate { get; set; }
        public DateTime? DoneDate { get; set; }
        public bool IsSnoozed { get; set; }
        public bool IsDone { get; set; }
        public int? Assignee { get; set; }
        public string AssigneeName { get; set; }
        public string AssignerName { get; set; }
    }
}

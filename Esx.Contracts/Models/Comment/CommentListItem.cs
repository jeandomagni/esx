﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Comment
{
    public class CommentListItem
    {
        public int Comment_Key { get; set; }
        public System.DateTime CreateTime { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public string CommentText { get; set; }
        public string PrimaryMentionID { get; set; }
        public string TableCode { get; set; }
        public string CreatorName { get; set; }
        public string CreatorMentionID { get; set; }
        public Reminder Reminder { get; set; }
        public List<string> Mentions { get; set; } 
        public List<Attachment> Attachments { get; set; } 
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Esx.Contracts.Models.Comment
{
    public class Comment
    {
        public byte[] AccessToken { get; set; }
        public string PrimaryMentionID { get; set; }
        public string CommentText { get; set; }
        public int Comment_Key { get; set; }
        public List<string> Tags { get; set; }
        public List<string> Mentions { get; set; }
        public List<int> DocumentIds { get; set; } 
        public bool CreateReminder { get; set; }
        public Reminder Reminder { get; set; }
    }
}
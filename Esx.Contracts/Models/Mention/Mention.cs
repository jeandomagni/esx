﻿
namespace Esx.Contracts.Models.Mention
{
    public class Mention
    {
        public int MentionKey { get; set; }

        public string MentionID { get; set; }

        public int Relevance { get; set; }

        public string ReferencedTableCode { get; set; }

        public string MentionLabel { get; set; }
       
    }
}

﻿namespace Esx.Contracts.Models.User
{
    public class UserDetails
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public bool IsPrincipal { get; set; }
        public string PrincipalUserFullName { get; set; }
    }
}

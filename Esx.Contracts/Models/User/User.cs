﻿namespace Esx.Contracts.Models.User
{
    public class User
    {
        public int UserKey { get; set; }
        public string Username { get; set; }
        public string MentionId { get; set; }        
        public byte [] AccessToken { get; set; }
        public int ContactKey { get; set; }
        public string FullName { get; set; }
    }
}

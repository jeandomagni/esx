﻿namespace Esx.Contracts.Models.User
{
    public class UserListItem
    {
        public int UserKey { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public bool IsDelegatedUser { get; set; }
        public bool IsSelected { get; set; }
        public bool IsPrincipal { get; set; }
    }
}

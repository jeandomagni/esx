﻿namespace Esx.Contracts.Models.User
{
    public class UserPreferenceListItem
    {
        public int UserId { get; set; }
        public string PreferenceKey { get; set; }
        public string PreferenceValue { get; set; }
    }
}

﻿namespace Esx.Contracts.Models
{
    public class StateProvinceListItem
    {
        public string StateProvinceCode { get; set; }

        public string StateProvinceName { get; set; }
    }
}

﻿namespace Esx.Contracts.Models
{
    public class CountryListItem
    {
        public int CountryKey { get; set; }

        public string CountryName { get; set; }

        public string NativeCountryName { get; set; }

        public string CallingCode { get; set; }
    }
}

﻿using System;

namespace Esx.Contracts.Models.Loan
{
    public class LoanListItem
    {
        public int LoanKey { get; set; }

        public bool? IsWatched { get; set; }

        public string LoanName { get; set; }

        public string Borrower { get; set; }

        public string BorrowerMentionId { get; set; }

        public string Lender { get; set; }

        public string LenderMentionId { get; set; }

        public string PropertyType { get; set; }

        public decimal? LoanAmount { get; set; }

        public string LoanMentionId { get; set; }

        public bool Active { get; set; }

        public DateTime? OriginationDate { get; set; }

        public DateTime? MaturityDate { get; set; }

        public string AllInRate { get; set; }

        public string DealCode { get; set; }
    }
}

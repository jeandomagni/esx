// <auto-generated>
// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier
// TargetFrameworkVersion = 4.52
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using Newtonsoft.Json;
using Esx.Contracts.Interfaces;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Threading;

namespace Esx.Contracts.Models
{
    // OfficeMarket
    public partial class dESx_OfficeMarket: IAuditCreateUpdateEntity
    {
        public int OfficeMarketKey { get; set; } // OfficeMarket_Key (Primary key)
        public int OfficeKey { get; set; } // Office_Key
        public int MarketKey { get; set; } // Market_Key
        public DateTime? EndDate { get; set; } // EndDate
        public bool IsCurrent { get; set; } // IsCurrent
        public DateTime CreatedDate { get; set; } // CreatedDate
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime UpdatedDate { get; set; } // UpdatedDate
        public string UpdatedBy { get; set; } // UpdatedBy

        // Foreign keys
        [JsonIgnore]
        public virtual dESx_Market dESx_Market { get; set; } // FK_OfficeMarket_Market
        [JsonIgnore]
        public virtual dESx_Office dESx_Office { get; set; } // FK_OfficeMarket_Office
        
        public dESx_OfficeMarket()
        {
            CreatedDate = System.DateTime.UtcNow;
            UpdatedDate = System.DateTime.UtcNow;
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>

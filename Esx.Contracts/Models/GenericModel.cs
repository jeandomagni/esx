﻿namespace Esx.Contracts.Models
{
    public class GenericModel<T>
    {
        public T Data { get; set; }
    }
}

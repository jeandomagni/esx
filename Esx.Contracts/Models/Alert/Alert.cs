﻿using System;
using System.Collections.Generic;

namespace Esx.Contracts.Models.Alert
{
    public class Alert
    {
        public int AlertId { get; set; }
        public string EntityCode { get; set; }
        public int EntityKey { get; set; }
        public string EntityMentionId { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public IDictionary<string, string> EntityAttributes { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Company
{
    public class CompanyLoanAdd
    {
     
        public int PropertyKey { get; set; }

        public string PropertyName { get; set; }

        public string LoanName { get; set; }

        public string MentionId { get; set; }

        public decimal LoanAmount { get; set; }

        public bool IsBorrower { get; set; }

        public int BorrowerLenderCompanyKey { get; set; }

        public DateTime? OriginationDate { get; set; }

        public DateTime? MaturationDate { get; set; }

        public string LoanSourceType { get; set; }

        public string LoanRateType { get; set; }

        public decimal LoanRate { get; set; }

        public string LoanPaymentType { get; set; }

        public string LoanCashMgmtType { get; set; }

        public string LoanBenchmarkType { get; set; }
    }
}

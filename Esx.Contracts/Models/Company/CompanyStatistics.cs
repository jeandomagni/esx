﻿namespace Esx.Contracts.Models.Company
{
    public class CompanyStatistics
    {
        public int ActiveDeals { get; set; }
        
        public int Investments { get; set; }

        public string BidVolume { get; set; }

        public int Locations { get; set; }

        public int Contacts { get; set; }
        public int Properties { get; set; }
        public int Loans { get; set; }
    }
}

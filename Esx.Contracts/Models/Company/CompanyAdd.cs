﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Company
{
    public class CompanyAdd
    {        
        public int CompanyID { get; set; }

        public string CompanyName { get; set; }

        public string AliasName { get; set; }

        public string Website { get; set; }

        public string Phone { get; set; }

        public string CompanyEmail { get; set; }

        // Parent Company
        public int ParentCompany { get; set; }

        // Location Properties
        public int LocationKey { get; set; }

        public string LocationName { get; set; }

        public string LocationAddress1 { get; set; }

        public string LocationAddress2 { get; set; }

        public string LocationCity { get; set; }

        public string LocationState { get; set; }

        public string LocationZip { get; set; }

        public string LocationCountry { get; set; }

        public int LocationCountryKey { get; set; }

        public string LocationPhone { get; set; }


        // Contact Properties
        public int ContactKey { get; set; }

        public string ContactName { get; set; }

        public string ContactTitle { get; set; }

        public string ContactEmailAddress { get; set; }

        public string ContactMobilePhone { get; set; }

        public string ContactOfficePhone { get; set; }

        public string ContactHomePhone { get; set; }

        public string ContactAddress1 { get; set; }

        public string ContactAddress2 { get; set; }

        public string ContactCity { get; set; }

        public string ContactState { get; set; }

        public string ContactZip { get; set; }

        public string ContactCountry { get; set; }

        public int ContactCountryKey { get; set; }

        public bool SetDefaultCountry { get; set; }
    }
}

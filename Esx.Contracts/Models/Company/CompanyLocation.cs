﻿namespace Esx.Contracts.Models.Company
{
    public class CompanyLocation
    {
        public int OfficeKey { get; set; }
        public bool IsHeadquarters { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string Zip { get; set; }
        public int CountryKey  { get; set; }
        public string Phone { get; set; }
        public int Contacts { get; set; }
    }
}

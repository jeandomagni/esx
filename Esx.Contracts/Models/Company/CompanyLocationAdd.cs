﻿namespace Esx.Contracts.Models.Company
{
    public class CompanyLocationAdd
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public int CountryKey { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Website { get; set; }
        public bool IsPrimary { get; set; }
    }
}

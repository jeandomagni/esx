﻿namespace Esx.Contracts.Models.Company
{
    public class Broker
    {
        public string CompanyAlias { get; set; }
        public bool IsEastdil { get; set; }
    }
}

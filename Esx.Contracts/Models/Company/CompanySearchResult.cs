﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Company
{
    public class CompanySearchResult : CompanyListItem
    {
        public CompanyStatistics Statistics { get; set; }
    }
}

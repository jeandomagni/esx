﻿namespace Esx.Contracts.Models.Company
{
    public class CompanySummaryStats
    {
        public int Pitches { get; set; }
        public int ActiveDeals { get; set; }
        public int BidActivity { get; set; }
        public int DealsInPursuit { get; set; }
        public int Properties { get; set; }
        public int HoldPeriod { get; set; }
        public int LoansMaturing { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Company
{
    public class CompanyDetails
    {
        // Profile
        public string MentionId { get; set; }

        public int CompanyKey { get; set; }

        public string CompanyName { get; set; }

        public string TickerSymbol { get; set; }

        public bool IsDefunct { get; set; }

        // Aliases
        public List<Alias> Aliases { get; set; }

        // Parent Company
        public ParentCompany ParentCompany { get; set; }

        public List<ParentCompany> ParentCompanies { get; set; }

        // Headquarter (office)
        public Office Office { get; set; }

        // Compliance
        public string FcpaName { get; set; }

        public bool IsOfacCheck { get; set; }

        public DateTime? OfacCheckDate { get; set; }

        public decimal? RealEstateAllocation { get; set; }

        public bool HasWfsCorrelation { get; set; }

        public string CibosCode { get; set; }

        public bool IsForeignGovernment { get; set; }

        public bool IsTrackedByAsiaTeam { get; set; }

        public bool HasRestrictedVisibility { get; set; }

        public bool IsSolicitable { get; set; }

        public DateTime? IsSolicitableDate { get; set; }

        public bool HasProfileChanged { get; set; }
    }
}

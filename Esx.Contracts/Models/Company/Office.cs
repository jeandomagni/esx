﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Esx.Contracts.Models.Location;

namespace Esx.Contracts.Models.Company
{
    public class Office
    {
        public int OfficeKey { get; set; }
        public string OfficeName { get; set; }

        public bool IsHeadQuarters { get; set; }

        public PhysicalAddress PhysicalAddress { get; set; }

        public List<ElectronicAddress> ElectronicAddress { get; set; }
    }
}

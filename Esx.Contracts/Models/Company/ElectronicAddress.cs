﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Company
{
    public class ElectronicAddress
    {
        public int ElectronicAddressKey { get; set; }
        public string ElectronicAddressTypeCode { get; set; }
        public string AddressUsageTypeCode { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
    }
}

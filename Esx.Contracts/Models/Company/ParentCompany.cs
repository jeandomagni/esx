﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Company
{
    public class ParentCompany
    {
        public int CompanyHierarchyKey { get; set; }
        public int ParentCompanyKey { get; set; }
        public int ChildCompanyKey { get; set; }
        public string ParentMentionId { get; set; }
        public string ParentCompanyName { get; set; }
    }
}

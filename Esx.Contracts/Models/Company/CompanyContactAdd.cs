﻿namespace Esx.Contracts.Models.Company
{
    public class CompanyContactAdd
    {
        public string ContactName { get; set; }
        public string Title { get; set; }
        public string MobilePhone { get; set; }
        public string OfficePhone { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public int? OfficeKey { get; set; }
        public CompanyLocationAdd Location { get; set; }
    }
}

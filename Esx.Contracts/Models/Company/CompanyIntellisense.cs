﻿namespace Esx.Contracts.Models.Company
{
    public class CompanyIntellisense
    {
        public string MentionID { get; set; }
        public string AliasName { get; set; }
        public int Company_Key { get; set; }
        public int Relevance { get; set; }
    }
}

﻿namespace Esx.Contracts.Models.Company
{
    public class CompanyListItem
    {
        public bool IsWatched { get; set; }

        public string MentionID { get; set; }

        public int CompanyKey { get; set; }

        public string CanonicalCompany { get; set; }
    }
}

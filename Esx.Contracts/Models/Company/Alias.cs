﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Company
{
    public class Alias
    {
        public int AliasKey { get; set; }

        public string AliasName { get; set; }

        public int CompanyKey { get; set; }

        public bool IsLegal { get; set; }

        public bool IsPrimary { get; set; }

        public bool IsDefunct { get; set; }

        public string MentionId { get; set; }
    }
}


namespace Esx.Contracts.Models.Location
{
    public class PhysicalAddress
    {
        public int PhysicalAddressKey { get; set; }
        public string AddressTypeCode { get; set; }
        public string Description { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string City { get; set; }
        public string StateProvidenceCode { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int CountryKey { get; set; }


        public string Address
        {
            get
            {
                var address = "";
                address = !string.IsNullOrWhiteSpace(Address1) ? $"{address} {Address1}" : address;
                address = !string.IsNullOrWhiteSpace(Address2) ? $"{address} {Address2}" : address;
                address = !string.IsNullOrWhiteSpace(Address3) ? $"{address} {Address3}" : address;
                address = !string.IsNullOrWhiteSpace(Address4) ? $"{address} {Address4}" : address;
                return address;
            }
        }

    }
}

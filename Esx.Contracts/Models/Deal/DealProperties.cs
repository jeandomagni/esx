﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Esx.Contracts.Models.Property;

namespace Esx.Contracts.Models.Deal
{
    public class DealProperties
    {
        public List<PropertyDetails> Properties { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Deal
{
    public class ActivePipelineDeal
    {
        public int DealId { get; set; }
        public string Name{ get; set; }
        public string EntityType { get; set; }
        public string Office { get; set; }
        public string Client { get; set; }
        public string DealType { get; set; }
        public string PropertyType { get; set; }
        public decimal EstPrice { get; set; }
        public decimal EstFees { get; set; }
        public string Status { get; set; }
        public DateTime PitchLaunchDate { get; set; }
        public bool MyOffice { get; set; }
        public bool MyDeal { get; set; }
    }
}

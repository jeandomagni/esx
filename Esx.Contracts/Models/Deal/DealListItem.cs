﻿using System;

namespace Esx.Contracts.Models.Deal
{
    public class DealListItem
    {
        public bool? IsWatched { get; set; }

        public string DealMentionId { get; set; }

        public string DealCode { get; set; }

        public string DealName { get; set; }

        public string DealOffice { get; set; }

        public DateTime? StatusDate { get; set; }

        public string PropertyType { get; set; }

        public string Location { get; set; }

        public string DealType { get; set; }

        public decimal? Valuation { get; set; }

        public int DealID { get; set; }

        public string DealStatus { get; set; }

        public DateTime BidDate { get; set; }
        
        public DateTime? UpdatedDate { get; set; }
        
        public DateTime? LaunchDate { get; set; }
        
        public string SellerCompanies { get; set; }

        public string DealParticipationTypeCode { get; set; }
    }
}

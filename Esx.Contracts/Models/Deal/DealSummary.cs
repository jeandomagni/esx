﻿using Esx.Contracts.Models.Company;
using Esx.Contracts.Models.Property;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Esx.Contracts.Models.Deal
{
    public class DealSummary
    {
        public string DealMentionId { get; set; }

        public string DealCode { get; set; }

        public string DealName { get; set; }

        public string DealOffice { get; set; }

        public DateTime? StatusDate { get; set; }

        public string Location { get; set; }

        public string DealType { get; set; }

        public decimal? Valuation { get; set; }

        public int DealID { get; set; }

        public string DealStatus { get; set; }

        public DateTime BidDate { get; set; }

        public IEnumerable<PropertyDetails> Properties { get; set; }
        public List<Alias> SellerCompanies { get; set; }

        public IEnumerable<Broker> Brokers { get; set; } 

    }
}

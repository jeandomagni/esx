﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Esx.Contracts.Enums;
using Esx.Contracts.Interfaces;

namespace Esx.Contracts.Models
{
    public partial class dESx_CompanyAlias : IMentionEntity
    {
        [NotMapped]
        public string MentionSource => AliasName;

        [NotMapped]
        public int ObjectKey => CompanyAliasKey;

        [NotMapped]
        public string ObjectType => CoreType.Company;

        [NotMapped]
        public bool ObjectPrimary => IsPrimary;

        [NotMapped]
        public string MentionId
        {
            get
            {
                return string.IsNullOrEmpty(_mentionId)
                    ? dESx_Mentions.FirstOrDefault(x => x.IsPrimary)?.MentionId
                    : _mentionId;
            }
            set { _mentionId = value; }
        }

        private string _mentionId;
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Esx.Contracts.Enums;
using Esx.Contracts.Interfaces;

namespace Esx.Contracts.Models
{
    public partial class dESx_Market : IMentionEntity
    {
        [NotMapped]
        public string MentionSource => MarketName;

        [NotMapped]
        public int ObjectKey => MarketKey;

        [NotMapped]
        public string ObjectType => CoreType.Market;

        [NotMapped]
        public bool ObjectPrimary => true;

        [NotMapped]
        public string MentionId
        {
            get
            {
                return string.IsNullOrEmpty(_mentionId)
                    ? dESx_Mentions.FirstOrDefault(x => x.IsPrimary)?.MentionId
                    : _mentionId;
            }
            set { _mentionId = value; }
        }

        private string _mentionId;
    }
}

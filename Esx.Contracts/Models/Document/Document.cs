﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Contracts.Models.Document
{
    public class Document
    {
        public int DocumentKey { get; set; }
        public Guid ExternalKey { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
    }
}

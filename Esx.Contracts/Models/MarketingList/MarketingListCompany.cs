﻿using System;
using System.Collections.Generic;
using System.Linq;
using Esx.Contracts.Models.Company;

namespace Esx.Contracts.Models.MarketingList
{
    public class MarketingListCompany : CompanyListItem
    {

        public MarketingListCompany()
        {
            Contacts = new List<MarketingListContact>();
        }
        public ICollection<MarketingListContact> Contacts { get; set; }

        public bool IsSolicitable { get; set; }
        public bool IsRequestingTeaser { get; set; }
        public bool IsRequestingWarRoomAccess { get; set; }
        public bool IsRequestingAdditionalMaterials { get; set; }
        public bool IsRequestingPrintedOm { get; set; }
        public bool IsRequestingCallForOffers { get; set; }

        public Activity Ca { get; set; }
        public Activity TeaserSent { get; set; }
        public Activity PrintedOm { get; set; }
        public Activity AdditionalMaterials { get; set; }
        public Activity WarRoomAccess { get; set; }
        public Activity CallForOffers { get; set; }

    }
}

﻿using System;
using Newtonsoft.Json;

namespace Esx.Contracts.Models.MarketingList
{
    public class LogItem
    {
        public string Value { get; set; }
        public DateTime Date { get; set; }
    }
}

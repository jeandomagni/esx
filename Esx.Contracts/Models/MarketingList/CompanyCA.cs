﻿
using System;
using System.Collections.Generic;

namespace Esx.Contracts.Models.MarketingList
{
    public class CompanyCA
    {
        public List<string> ContactMentionIds{ get; set; }
        public List<int> DocumentIds { get; set; }
        public string Status { get; set; }
        public DateTime StatusDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Esx.Contracts.Models.Company;

namespace Esx.Contracts.Models.MarketingList
{
    public class CompanyEngagementItem : MarketingListCompany
    {
        public int DaysSinceLastReached { get; set; }
        public int NumberOfCasInPipeline { get; set; }
    }
}

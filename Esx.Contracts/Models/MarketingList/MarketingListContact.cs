﻿using System;
using System.Collections.Generic;
using System.Linq;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Contact;

namespace Esx.Contracts.Models.MarketingList
{
    public class MarketingListContact : ContactListItem
    {

        public MarketingListContact()
        {
            Statuses = new List<LogItem>();
        }

        public ICollection<LogItem> Statuses { get; set; }
        public int MarketingListId { get; set; }
        public bool? IsPrimary { get; set; }
        public bool? IsSolicitable { get; set; }
        public bool? IsRequestingTeaser { get; set; }
        public bool? IsRequestingWarRoomAccess { get; set; }
        public bool? IsRequestingAdditionalMaterials { get; set; }
        public bool? IsRequestingPrintedOm { get; set; }
        public bool? IsRequestingCallForOffers { get; set; }

        public Activity Ca
        {
            get
            {
                var activity = new Activity
                {
                    Level = ActivityLevel.Blocked,
                    Dates = new List<DateTime>()
                };

                var approved =
                    Statuses.Where(s => s.Value == MarketingListStatusType.CaApproved.Code)
                        .OrderByDescending(s => s.Date)
                        .Select(s => s.Date).ToList();
                if (approved.Any())
                {
                    activity.Level = ActivityLevel.Complete;
                    activity.Dates.AddRange(approved);
                    return activity;
                }

                var terms =
                    Statuses.Where(s => s.Value == MarketingListStatusType.CaFollowUpTerms.Code)
                        .OrderByDescending(s => s.Date)
                        .Select(s => s.Date).ToList();
                if (terms.Any())
                {
                    activity.Level = ActivityLevel.FollowUp;
                    activity.Dates.AddRange(terms);
                }

                var sig =
                Statuses.Where(s => s.Value == MarketingListStatusType.CaFollowUpSignature.Code)
                    .OrderByDescending(s => s.Date)
                    .Select(s => s.Date).ToList();
                if (sig.Any())
                {
                    activity.Level = ActivityLevel.FollowUp;
                    activity.Dates.AddRange(sig);
                }

                activity.Dates = activity.Dates.OrderByDescending(x=>x.Date).ToList();
                return activity;
            }
        }

        public Activity TeaserSent => GetActivities(MarketingListStatusType.TeaserSent.Code);
        public Activity AdditionalMaterials => GetActivities(MarketingListStatusType.AdditionalMaterials.Code);
        public Activity PrintedOm => GetActivities(MarketingListStatusType.PrintedOm.Code);
        public Activity WarRoomAccess => GetActivities(MarketingListStatusType.WarRoomAccess.Code);
        public Activity CallForOffers => GetActivities(MarketingListStatusType.CallForOffers.Code);

        private Activity GetActivities(string t)
        {
            var activity = new Activity
            {
                Dates = Statuses.Where(s => s.Value == t).OrderByDescending(s => s.Date).Select(s => s.Date).ToList()
            };
            activity.Level = activity.Dates.Any()? ActivityLevel.Complete : ActivityLevel.Blocked;
            return activity;
        }

    }
}

﻿
namespace Esx.Contracts.Models.MarketingList
{
    public class  MarketingListLookup
    {
        public string DealName { get; set; }
        public string DealCode { get; set; }
        public string Name { get; set; }
        public int MarketingListId { get; set; }
    }
}

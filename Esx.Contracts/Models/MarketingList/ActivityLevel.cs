﻿namespace Esx.Contracts.Models.MarketingList
{
    public static class ActivityLevel
    {
        public const string Complete = "complete";
        public const string Blocked = "blocked";
        public const string FollowUp = "followUp";
        public const string Warning = "warning";
        public const string Hold = "hold";
    }
}

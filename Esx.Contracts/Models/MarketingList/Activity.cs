﻿using System;
using System.Collections.Generic;

namespace Esx.Contracts.Models.MarketingList
{
    public class Activity
    {
        public List<DateTime> Dates { get; set; } 
        public string Level { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharpTestsEx;
using SharpTestsEx.Assertions;
using SharpTestsEx.ExtensionsImpl;


namespace Esx.Tests.Common.Helpers
{
    public static class SharpTestsExIEnumerableContainsExtension
    {
        /// <summary>
        /// Should().Contains with comparer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="constraint"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IAndConstraints<IEnumerableConstraints<T>> Contains<T>(this IEnumerableConstraints<T> constraint,
                                                                            Func<T, bool> predicate)
        {
            constraint.AssertionInfo.AssertUsing(new UnaryAssertion<IEnumerable<T>>("Contain",
                                                                                    a => (a != null) && a.Any(predicate)));
            return ConstraintsHelper.AndChain(constraint);
        }
    }
}

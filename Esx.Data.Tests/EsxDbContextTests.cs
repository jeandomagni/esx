﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;
using Dapper;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Comment;
using Esx.Contracts.Security;
using Esx.Data.Dapper;
using Esx.Data.Repositories;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Data.Tests
{
    public abstract class EsxDbContextTests
    {
        public sealed class SaveChanges : EsxDbContextTests
        {
            private Mock<IMentionsRepository> _mentionsRepositoryMock;
            private Mock<ISecurityContext> _securityContextMock;

            [SetUp]
            public void TestInit()
            {
                _mentionsRepositoryMock = new Mock<IMentionsRepository>();
                _securityContextMock = new Mock<ISecurityContext>();
            }

            [TearDown]
            public void TestTearDown()
            {
            }

            [Test]
            public void Should_Create_New_Mention_For_IMentionEntity()
            {
                //TODO: Figure out how to mock the DbContext's connection and transactions
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Esx.Contracts.Models.Comment;
using Esx.Contracts.Security;
using Esx.Data.Dapper;
using Esx.Data.Repositories;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Data.Tests
{

    public abstract class CommentsRepositoryTests : RepositoryTestBase
    {
        private Mock<ISecurityContext> _securityContextMock;
        private CommentsRepository _repository;

        [SetUp]
        public void TestIn()
        {
            _securityContextMock = new Mock<ISecurityContext>();
            _repository = new CommentsRepository(SqlConnMock.Object,
                                                 _securityContextMock.Object,
                                                 DapperParamProviderMock.Object);
        }

        public sealed class GetComments : CommentsRepositoryTests
        {
            [Test]
            public void Should_Get_Comments_With_Given_MentionId()
            {
                // arrange
                var mentionId = "@BigCompany";
                var pageSize = RandomData.GetInt(1, 10);
                var offSet = RandomData.GetInt(1, 10);
                var expectedToken = RandomData.GetByteArray(10);

                _securityContextMock.Setup(x => x.DbToken).Returns(expectedToken);

                ExpectParam("@pAccessToken", expectedToken);
                ExpectParam("@pMentionID", mentionId);
                ExpectParam("@pMaxRows", pageSize);
                ExpectParam("@pFirstRow", offSet);

                var expected = ExpectQuery("xEsxApp.GetMentionObjectComments", new List<CommentListItem>{
                    new CommentListItem
                    {
                        PrimaryMentionID = mentionId
                    }
                });

                // Act
                var actual = _repository.GetComments(mentionId, pageSize, offSet);

                // Assert
                actual.Should().Have.SameValuesAs(expected);

                _securityContextMock.VerifyAll();
                ParamMock.VerifyAll();
                SqlConnMock.VerifyAll();
            }
           
        }

        public sealed class SaveComments : CommentsRepositoryTests
        {
            [Test]
            public void Should_Save_Comments()
            {
                // arrange
                var tags = new List<string> { "#aTag01", "#aTag02" };
                var mentions = new List<string> { "@AaronRents|A" };

                var comment = new Comment()
                {
                    PrimaryMentionID = "@Brixmor|A",
                    CommentText = "This #aTag01 is a test #aTag02.",
                    Comment_Key = 0,
                    Tags = tags,
                    Mentions = mentions
                };

                DapperParamProviderMock.Setup(d => d.GetCommentDynamicParameter(It.Is<Comment>(c => c == comment)))
                    .Returns(new CommentDynamicParameter(comment)).Verifiable();

                SqlConnMock.Setup(
                    c =>
                        c.Execute(
                            "xEsxApp.PutComment",
                            It.IsAny<SqlMapper.IDynamicParameters>(),
                            It.Is<CommandType>(t => t == CommandType.StoredProcedure)));

                // Act
                _repository.SaveComment(comment);

                //
                SqlConnMock.VerifyAll();
                DapperParamProviderMock.Verify();
            }
        }

        public sealed class GetMentionComments : CommentsRepositoryTests
        {
            [Test]
            public void Should_Get_Comments_With_Given_MentionId()
            {
                // arrange
                var mentionId = "@BigCompany";
                var pageSize = RandomData.GetInt(1, 10);
                var offSet = RandomData.GetInt(1, 10);
                var expectedToken = RandomData.GetByteArray(10);
                
                _securityContextMock.Setup(x => x.DbToken).Returns(expectedToken);

                ExpectParam("@pAccessToken", expectedToken);
                ExpectParam("@pMentionID", mentionId);
                ExpectParam("@pMaxRows", pageSize);
                ExpectParam("@pFirstRow", offSet);

                var expected = ExpectQuery("xEsxApp.GetMentionReferenceComments", new List<CommentListItem>{
                    new CommentListItem
                    {
                        PrimaryMentionID = mentionId
                    }
                });

                // Act
                var actual = _repository.GetMentionComments(mentionId, pageSize, offSet);

                // Assert
                actual.Should().Have.SameValuesAs(expected);

                _securityContextMock.VerifyAll();
                ParamMock.VerifyAll();
                SqlConnMock.VerifyAll();
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Dapper;
using Esx.Data.Dapper;
using Intertech.Facade.DapperParameters;
using Moq;
using NUnit.Framework;

namespace Esx.Data.Tests
{
    public class RepositoryTestBase
    {
        protected Mock<IConnectionWrapper> SqlConnMock;
        protected Mock<IDapperParameterProvider> DapperParamProviderMock;
        protected Mock<IDapperParameters> ParamMock;

        [SetUp]
        public void Init()
        {
            SqlConnMock = new Mock<IConnectionWrapper>();
            DapperParamProviderMock = new Mock<IDapperParameterProvider>();
            ParamMock = new Mock<IDapperParameters>();
            ParamMock.Setup(p => p.DynamicParameters).Returns(new DynamicParameters());
            DapperParamProviderMock.Setup(p => p.GetDapperParameters()).Returns(ParamMock.Object);

        }

        /// <summary>
        /// Helper function to create an expectation of a parameter.
        /// </summary>
        /// <typeparam name="TValue">The type of value to expect.</typeparam>
        /// <param name="name">The name of the parameter to expect.</param>
        /// <param name="value">The value of the parameter to expect.</param>
        /// <param name="dbType">The DbType of the paremter to expect. If null, it will look for any DbType.</param>
        /// <param name="direction">The ParameterDirection of the parameter to expect. Defaults to Input.</param>
        /// <param name="size">The size of the parameter to expect. If null, it will look for any int.</param>
        /// <returns></returns>
        protected TValue ExpectParam<TValue>(string name, TValue value, DbType? dbType, int? size, ParameterDirection? direction = ParameterDirection.Input)
        {
            if (value == null) throw new Exception("Cannot use this ExpectParam if expected value is null.");

            ParamMock.Setup(m => m.Add(
                It.Is<string>(x => x == name),
                It.Is<TValue>(x => EqualityComparer<TValue>.Default.Equals(x, value)),
                It.Is<DbType>(x => x == dbType),
                It.Is<ParameterDirection?>(x => x == direction),
                It.Is<int?>(x => x == size)
            )).Verifiable();
            return value;
        }

        protected TValue ExpectParam<TValue>(string name, TValue value, int? size, ParameterDirection? direction = ParameterDirection.Input)
        {
            if (value == null) throw new Exception("Cannot use this ExpectParam if expected value is null.");

            ParamMock.Setup(m => m.Add(
                It.Is<string>(x => x == name),
                It.Is<TValue>(x => EqualityComparer<TValue>.Default.Equals(x, value)),
                It.IsAny<DbType?>(),
                It.Is<ParameterDirection?>(x => x == direction),
                It.Is<int?>(x => x == size)
            )).Verifiable();

            return value;
        }

        protected TValue ExpectParam<TValue>(string name, TValue value, DbType? dbType, ParameterDirection? direction = ParameterDirection.Input)
        {
            if (value == null) throw new Exception("Cannot use this ExpectParam if expected value is null.");

            ParamMock.Setup(m => m.Add(
                    It.Is<string>(x => x == name),
                    It.Is<TValue>(x => EqualityComparer<TValue>.Default.Equals(x, value)),
                    It.Is<DbType?>(x => x == dbType),
                    It.Is<ParameterDirection?>(x => x == direction),
                    It.IsAny<int?>()
            )).Verifiable();
            return value;
        }

        protected TValue ExpectParam<TValue>(string name, TValue value)
        {
            if (value == null) throw new Exception("Cannot use this ExpectParam if expected value is null.");

            ParamMock.Setup(m => m.Add(
                It.Is<string>(x => x == name),
                It.Is<TValue>(x => EqualityComparer<TValue>.Default.Equals(x, value)),
                It.IsAny<DbType?>(),
                It.IsAny<ParameterDirection?>(),
                It.IsAny<int?>()
            )).Verifiable();

            return value;
        }

        /// <summary>
        /// Helper function to create an expection of a query call.
        /// </summary>
        /// <typeparam name="TValue">The type of an indivdual result.</typeparam>
        /// <param name="name">The name of the query. Ex: xESxApp.GetSomeResultSet</param>
        /// <param name="expectedResults">The results to return.</param>
        /// <param name="commandType">The type of query command. Defaults to StoredProcedure.</param>
        /// <returns>The expected results</returns>
        protected IList<TValue> ExpectQuery<TValue>(string name, IList<TValue> expectedResults, CommandType commandType = CommandType.StoredProcedure)
        {
            SqlConnMock.Setup(
                c =>
                    c.Query<TValue>(
                        It.Is<string>(s => s == name),
                        It.IsAny<DynamicParameters>(),
                        It.Is<CommandType>(t => t == commandType)
                        )).Returns(expectedResults);
            return expectedResults;
        }

        /// <summary>
        /// Helper function to create an expection of a query call.
        /// </summary>
        /// <typeparam name="TValue">The type of an indivdual result.</typeparam>
        /// <param name="name">The name of the query. Ex: xESxApp.GetSomeResultSet</param>
        /// <param name="expectedResults">The results to return.</param>
        /// <param name="commandType">The type of query command. Defaults to StoredProcedure.</param>
        /// <returns>The expected results</returns>
        protected ResultWithCount<TValue> ExpectQueryWithCount<TValue>(string name, ResultWithCount<TValue> expectedResults, CommandType commandType = CommandType.StoredProcedure)
        {
            SqlConnMock.Setup(
                c =>
                    c.QueryWithCount<TValue>(
                        It.Is<string>(s => s == name),
                        It.IsAny<DynamicParameters>(),
                        It.Is<CommandType>(t => t == commandType)
                        )).Returns(expectedResults);
            return expectedResults;
        }

        /// <summary>
        /// Helper function to create an expectation of an execute call.
        /// </summary>
        /// <param name="name">The name of the query. Ex: xESxApp.SetSomeValue</param>
        /// <param name="commandType">The type of query command. Defaults to StoredProcedure.</param>
        protected void ExpectExecute(string name, CommandType commandType = CommandType.StoredProcedure)
        {
            SqlConnMock.Setup(
                c =>
                c.Execute(
                    name,
                    It.IsAny<DynamicParameters>(),
                    It.Is<CommandType>(t => t == commandType))).Verifiable();
        }

        public Mock<DbSet<T>> GetMockDataSet<T>(List<T> data) where T : class
        {
            var objData = data.AsQueryable();
            var mock = new Mock<DbSet<T>>();
            mock.As<IQueryable<T>>().Setup(m => m.Provider).Returns(objData.Provider);
            mock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(objData.Expression);
            mock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(objData.ElementType);
            mock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(objData.GetEnumerator());

            return mock;
        }
    }
}

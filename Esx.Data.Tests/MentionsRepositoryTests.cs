﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using DapperWrapper;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Company;
using Esx.Contracts.Models.Mention;
using Esx.Contracts.Security;
using Esx.Data.Repositories;
using Esx.Tests.Common.Helpers;
using Intertech.Facade.DapperParameters;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Data.Tests
{
    public class MentionsRepositoryTests : RepositoryTestBase
    {
        private Mock<ISecurityContext> _securityContextMock;
        private MentionsRepository _repository;
        private Mock<IEsxDbContext> _esxDbContext;

        [SetUp]
        public void TestIn()
        {
            _securityContextMock = new Mock<ISecurityContext>();
            _esxDbContext = new Mock<IEsxDbContext>();

            _repository = new MentionsRepository(SqlConnMock.Object,
                                                 _securityContextMock.Object,
                                                 DapperParamProviderMock.Object);
        }

        public sealed class CreateNewMention : MentionsRepositoryTests
        {
            [Test]
            public void Should_Create_New_Mention()
            {
                // Arrange
                var expectedToken = RandomData.GetByteArray(10);
                _securityContextMock.Setup(x => x.DbToken).Returns(expectedToken);

                var mentionKey = RandomData.GetInt();
                var mentionId = RandomData.GetString(10);

                ExpectParam("pAccessToken", expectedToken);
                var objectType = ExpectParam("pObjectType", "A");
                var objectKey = ExpectParam("pObjectKey", 1);
                var isPrimary = ExpectParam("pIsPrimary", true);

                var pMentionKey = ExpectParam("pMentionKey", mentionKey, DbType.Int32, ParameterDirection.Output);
                var pMentionId = ExpectParam("pMentionID", mentionId, DbType.String, ParameterDirection.Output);

                var expected = ExpectQuery("xESxApp.CreateNewMention", new[]
                {
                    new Mention
                    {
                        MentionKey = mentionKey,
                        MentionID = mentionId
                    }
                });
               
                // Act
                var actual = _repository.CreateNewMention(objectType, objectKey, isPrimary);

                // Assert
                //actual.Satisfies(a => a.MentionKey == expected.First().MentionKey);

                //SqlConnMock.VerifyAll();
                //ParamMock.VerifyAll();



                ////ExpectExecute("xEsxApp.CreateNewMention");

                ////var expected = ExpectExecute("xESxApp.GetCompanyListBriefWCount",
                ////    new ResultWithCount<CompanyListItem>
                ////    {
                ////        Result = new[] { new CompanyListItem() },
                ////        TotalRecords = 1
                ////    });

                //// Act
                //var actual = _repository.CreateNewMention(objectType, objectKey, isPrimary);

                // Assert
                //actual.Satisfies(a => a.TotalRecords == expected.TotalRecords);

                //SqlConnMock.VerifyAll();
                //ParamMock.VerifyAll();
            }
        }

        public sealed class SetIsWatched : MentionsRepositoryTests
        {
            [Test]
            public void Should_Set_Company_With_Mention_ID()
            {
                // Arrange
                var expectedToken = RandomData.GetByteArray(10);
                _securityContextMock.Setup(x => x.DbToken).Returns(expectedToken);

                ExpectParam("pAccessToken", expectedToken);
                var mentionId = ExpectParam("pMentionID", "@123");
                var isWatched = ExpectParam("pOnOff", true);
                ExpectExecute("xESxApp.UpdateMentionWatch");

                // Act
                _repository.SetIsWatched(mentionId, isWatched);

                // Assert
                _securityContextMock.VerifyAll();
                SqlConnMock.VerifyAll();
                ParamMock.VerifyAll();
            }
        }
    }
}

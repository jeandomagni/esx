﻿using System.Linq;
using Esx.Contracts.Models.Company;
using Esx.Contracts.Security;
using Esx.Data.Repositories;
using Esx.Tests.Common.Helpers;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Data.Tests
{
    public class CompaniesRepositoryTests : RepositoryTestBase
    {
        private Mock<ISecurityContext> _securityContextMock;
        private CompaniesRepository _repository;
        private Mock<IEsxDbContext> _esxDbContext;

        [SetUp]
        public void TestIn()
        {
            _securityContextMock = new Mock<ISecurityContext>();
            _esxDbContext = new Mock<IEsxDbContext>();

            _repository = new CompaniesRepository(SqlConnMock.Object,
                                                 _securityContextMock.Object,
                                                 DapperParamProviderMock.Object,
                                                 _esxDbContext.Object);
        }

        public sealed class GetCompanies : CompaniesRepositoryTests
        {
            [Test]
            public void Should_Get_Companies_With_Given_Search_Text()
            {
                // Arrange
                var expectedToken = RandomData.GetByteArray(10);
                _securityContextMock.Setup(x => x.DbToken).Returns(expectedToken);

                var offset = 0;
                ExpectParam("pAccessToken", expectedToken);
                var searchText = ExpectParam("pFilterText", "Big Company");
                var pageSize = ExpectParam<int>("pMaxRows", 10);
                var sort = ExpectParam("pSortColumn", "CompanyCanonical");
                var sortDescending = ExpectParam("pSortDescending", false);
                var expected = ExpectQueryWithCount("xESxApp.GetCompanyListBriefWCount",
                    new ResultWithCount<CompanyListItem>
                    {
                        Result = new[] { new CompanyListItem() },
                        TotalRecords = 1
                    });

                // Act
                var actual = _repository.GetCompanies(searchText, pageSize, offset, sort, sortDescending);

                // Assert
                actual.Satisfies(a => a.TotalRecords == expected.TotalRecords);

                SqlConnMock.VerifyAll();
                ParamMock.VerifyAll();
            }
        }

        public sealed class GetCompanyStatistics : CompaniesRepositoryTests
        {
            [Test]
            public void Should_Get_Company_Summary_With_Mention_ID()
            {
                // Arrange
                var expectedToken = RandomData.GetByteArray(10);
                _securityContextMock.Setup(x => x.DbToken).Returns(expectedToken);

                ExpectParam("pAccessToken", expectedToken);
                var mentionId = ExpectParam("pMentionID", "@123");
                var expected = ExpectQuery("xESxApp.GetCompanyEntityCount", new[]
                {
                    new CompanyStatistics
                    {
                        Locations = RandomData.GetInt(),
                        Investments = RandomData.GetInt(),
                        ActiveDeals = RandomData.GetInt(),
                        BidVolume = RandomData.GetString(3)
                    }
                });


                // Act
                var actual = _repository.GetCompanyStatistics(mentionId);

                // Assert
                actual.Satisfies(a =>
                    a.ActiveDeals == expected.First().ActiveDeals
                    && a.BidVolume == expected.First().BidVolume
                    && a.Investments == expected.First().Investments
                    && a.Locations == expected.First().Locations);

                _securityContextMock.VerifyAll();
                SqlConnMock.VerifyAll();
                ParamMock.VerifyAll();
            }
        }
    }
}

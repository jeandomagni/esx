﻿using System.Data;
using Dapper;
using Esx.Data.Repositories;
using Esx.Tests.Common.Helpers;
using Intertech.Facade.DapperParameters;
using Moq;
using NUnit.Framework;
using SharpTestsEx;

namespace Esx.Data.Tests
{
    public abstract class SecurityRepositoryTests : RepositoryTestBase
    {
        private SecurityRepository _repository;
      
        [SetUp]
        public void TestIn()
        {
            _repository = new SecurityRepository(SqlConnMock.Object, DapperParamProviderMock.Object);
        }

        public sealed class LoginMethod : SecurityRepositoryTests
        {

            [Test]
            public void Should_call_the_login_stored_procedure_with_correct_params()
            {
                //arrange
                var username = RandomData.GetString(10);

                ParamMock.Setup(m => m.Add(It.Is<string>(name => name == "@UserName"),
                    It.Is<string>(val => val == username),
                    It.IsAny<DbType?>(),
                    It.IsAny<ParameterDirection?>(),
                    It.IsAny<int?>())).Verifiable();

                ParamMock.Setup(m => m.Add(It.Is<string>(name => name == "@pAccessToken"),
                    It.Is<byte[]>(val => val == null),
                    It.Is<DbType?>(dt => dt == DbType.Binary),
                    It.Is<ParameterDirection?>(pd => pd == ParameterDirection.Output),
                    It.Is<int?>(size => size == 64))).Verifiable();

                ParamMock.Setup(m => m.Add(It.Is<string>(name => name == "@pMentionID"),
                    It.Is<string>(val => val == null),
                    It.IsAny<DbType?>(),
                    It.Is<ParameterDirection?>(pd => pd == ParameterDirection.Output),
                    It.IsAny<int?>())).Verifiable();

                ParamMock.Setup(p => p.DynamicParameters).Returns(new DynamicParameters());

                SqlConnMock.Setup(
                    c =>
                        c.Execute(It.Is<string>(s => s == "xEsxApp.Login"),
                            It.IsAny<DynamicParameters>(),
                            It.Is<CommandType>(t => t == CommandType.StoredProcedure))
                    ).Returns(1);

                //act
                _repository.Login(username);

                //assert    
                SqlConnMock.VerifyAll();
                ParamMock.VerifyAll();
            }

            [Test]
            public void Should_retreive_output_parameters_and_set_them_in_user()
            {
                //arrange
                var username = RandomData.GetString(10);
                var token = RandomData.GetByteArray(64);
                var mentionId = RandomData.GetString(10);
                var userId = RandomData.GetInt();

                ParamMock.Setup(d => d.Get<byte[]>(It.Is<string>(s => s == "@pAccessToken"))).Returns(token);
                ParamMock.Setup(d => d.Get<string>(It.Is<string>(s => s == "@pMentionID"))).Returns(mentionId);
                ParamMock.Setup(d => d.Get<int?>(It.Is<string>(s => s == "@pUserKey"))).Returns(userId);

                SqlConnMock.Setup(
                    c =>
                        c.Execute(It.IsAny<string>(),
                            It.IsAny<IDapperParameters>(),
                            It.IsAny<CommandType>())
                    ).Returns(1);

                //act
                var actual = _repository.Login(username);

                //assert    
                actual.Satisfies(a => a.AccessToken.Equals(token) &&
                                    a.MentionId == mentionId &&
                                    a.Username == username);

                ParamMock.VerifyAll();

            }
        }
    }
}

﻿using Esx.Contracts.Models.User;
using Esx.Contracts.Security;
using Esx.Data;
using System;
using System.Linq;

namespace Esx.Business.sidebar
{
    public interface ISidebarService
    {
        UserDetails GetCurrentUserDetails();
    }
    public class SidebarService : ISidebarService
    {
        private readonly IEsxDbContext _dbContext;
        private readonly ISecurityContext _securityContext;
        public SidebarService(IEsxDbContext dbContext, ISecurityContext securityContext)
        {
            _dbContext = dbContext;
            _securityContext = securityContext;

        }
        public UserDetails GetCurrentUserDetails()
        {
            var principalAndCurrentUsers = (from user in _dbContext.dESx_Users
                       join contact in _dbContext.dESx_Contacts on user.ContactKey equals contact.ContactKey
                       where user.UserKey == _securityContext.UserKey || user.LoginName == _securityContext.PrincipalUserName
                       select new { contact.FullName, user.LoginName, IsPrincipal = _securityContext.PrincipalUserName.Equals(user.LoginName, StringComparison.InvariantCultureIgnoreCase) }).ToList();

            var currentUser = principalAndCurrentUsers.FirstOrDefault(u => !u.IsPrincipal);
            var principalUser = principalAndCurrentUsers.FirstOrDefault(u => u.IsPrincipal);

            return new UserDetails
            {
                FullName = currentUser?.FullName ?? principalUser?.FullName,
                PrincipalUserFullName = principalUser?.FullName,
                IsPrincipal =
                    _securityContext.PrincipalUserName.Equals(_securityContext.UserName,
                        StringComparison.InvariantCultureIgnoreCase)
            };
        }
    }
}

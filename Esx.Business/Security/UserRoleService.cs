﻿using Esx.Contracts.Enums;
using Esx.Contracts.Models;
using Esx.Contracts.Security;
using Esx.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;

namespace Esx.Business.Security
{
    public interface IUserRoleService
    {
        bool HasAccess(string featureCode);
        bool HasAccess(string featureCode, string userName);
        ADUser GetUser(string userName);
        void Init(string roleAccessData);
    }
    public class UserRoleService : IUserRoleService
    {
        private readonly IActiveDirectoryService _activeDirectoryService;
        private readonly ISecurityContext _securityContext;
        private ADUser _user;
        private readonly IEsxDbContext _dbContext;
        private readonly IRoleFeatureService _roleFeatureService;

        public UserRoleService(IActiveDirectoryService activeDirectoryService, ISecurityContext securityContext, IEsxDbContext dbContext, IRoleFeatureService roleFeatureService)
        {
            _activeDirectoryService = activeDirectoryService;
            _securityContext = securityContext;
            _dbContext = dbContext;
            _roleFeatureService = roleFeatureService;
        }
        public bool HasAccess(string featureCode)
        {
            return HasAccess(featureCode, _securityContext.UserName);
        }
        public bool HasAccess(string featureCode, string userName)
        {
            if (featureCode == null || userName == null)
            {
                throw new ArgumentNullException("Feature or username Code cannot be null");
            }
            if (_user == null)
            {
                _user = GetUser(userName);
            }
            var actualRoles = _user.Roles;
            var roleFeatures = _roleFeatureService.GetRoleFeatures().Where(x => x.FeatureCode == featureCode);
            var userRestricted = roleFeatures.Any(x => !x.HasAccess && actualRoles.Contains(x.Role));

            return !roleFeatures.Any()
                || (roleFeatures.Any(x => x.HasAccess && actualRoles.Contains(x.Role)) && !userRestricted)
                || (!roleFeatures.Any(x => x.HasAccess) && !userRestricted);
        }
        public ADUser GetUser(string userName)
        {
            if (_user == null) SetUserFromAD(userName);

            return _user;
        }
        private void SetUserFromAD(string userName)
        {
            var  adUser = _activeDirectoryService.SearchRolesForUser(userName);
            if(adUser != null)
            {
                _user = new ADUser { FullName = adUser.FullName, Roles = adUser.Roles.Intersect(Role.GetAll().Select(x=> x.Code)) };
            }
        }
        public void Init(string userRoles)
        {
            string[] roleparts = userRoles.Split(UserDataDelimiter.Delimiter);
            if(roleparts.Length == 2)
            {
                _user = new ADUser { Roles = roleparts[1].Split(UserDataDelimiter.RoleDelimiter) };
            }
        }
    }
}

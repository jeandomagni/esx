﻿using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;

namespace Esx.Business.Security
{
    public interface IActiveDirectoryService
    {
        ADUser SearchRolesForUser(string username);
    }
    public class ActiveDirectoryService : IActiveDirectoryService
    {
        private const string domainName = "ent.wfb.bank.corp";
        public ADUser SearchRolesForUser(string username)
        {
            ADUser adUser = null;
            var roles = new List<string>();
            var user = GetUser(username);

            if (user != null){
                //I saw this method call occasionally failing but will work when you try again, we need to implement a retry logic: exponential backoff
                PrincipalSearchResult<Principal> groups = user.GetAuthorizationGroups();

                if(groups != null) {
                    roles.AddRange(groups.OfType<GroupPrincipal>().Select(g => g.Name));
                }

                adUser = new ADUser { FullName = user.DisplayName, Roles = roles };
            }

            return adUser;
        }
        private UserPrincipal GetUser(string userName) {

            PrincipalContext pDomainContext = new PrincipalContext(ContextType.Domain);
            var user = UserPrincipal.FindByIdentity(pDomainContext, userName);
           
            return user;
        }
    }
  
}

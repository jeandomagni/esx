﻿using Esx.Business.Security;
using Esx.Contracts.Enums;
using Esx.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Business.Security
{
    public interface IRoleFeatureService
    {
        IEnumerable<RoleFeature> GetRoleFeatures();
    }
    public class RoleFeatureService : IRoleFeatureService
    {
        private readonly IEsxDbContext _dbContext;
        private readonly ICacheProvider<IEnumerable<RoleFeature>> _cacheProvider;
       
        public RoleFeatureService(IEsxDbContext dbContext, ICacheProvider<IEnumerable<RoleFeature>> cacheProvider)
        {
            _dbContext = dbContext;
            _cacheProvider = cacheProvider;
        }

        public IEnumerable<RoleFeature> GetRoleFeatures()
        {
            var roleFeatures = _cacheProvider.GetItem(CacheProviderKey.featureRoleCacheKey);

            if(roleFeatures == null)
            {
                roleFeatures = GetAllRoleFeatures();
                _cacheProvider.AddItem(CacheProviderKey.featureRoleCacheKey, roleFeatures);
            }
            return roleFeatures;
        }
        private IEnumerable<RoleFeature> GetAllRoleFeatures()
        {
            var roleFeature = _dbContext.dESx_FeatureRoles.Select(f => new RoleFeature
            {
                FeatureCode = f.FeatureCode,
                Role = f.RoleCode,
                HasAccess = f.HasAccess
            });
            return roleFeature.ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Business.Security
{
    public class RoleFeature
    {
        public string Role { get; set; }
        public string FeatureCode { get; set; }
        public bool HasAccess { get; set; }
    }
}

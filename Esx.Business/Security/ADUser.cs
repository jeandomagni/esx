﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esx.Business.Security
{
    public class ADUser
    {
        public IEnumerable<string> Roles { get; set; }
        public string FullName { get; set; }
    }
}

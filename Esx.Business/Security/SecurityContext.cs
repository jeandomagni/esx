﻿using System;
using Esx.Contracts.Security;
using Esx.Data.Repositories;
using Esx.Contracts.Extensions;
using Esx.Contracts.Models.User;
using Esx.Contracts.Enums;

namespace Esx.Business.Security
{

    public class SecurityContext : ISecurityContext
    {

        private bool _isPrepared;
        private int _userKey;
        private string _userName;
        private byte[] _dbToken;
        private readonly ISecurityRepository _securityRepository;

        public SecurityContext(ISecurityRepository securityRepository)
        {
            _securityRepository = securityRepository;
        }

        public byte[] DbToken
        {
            get
            {
                ThrowIfNotPrepared();
                return _dbToken;
            }
            private set { _dbToken = value; }
        }

        public int UserKey
        {
            get { return _userKey; }
            private set { _userKey = value; }
        }
        public string PrincipalUserName { get; private set; }

        public string UserName
        {
            get { return _userName; }
            private set { _userName = value; }
        }

        public User PrepareContext(string identity, bool impersonatingUser)
        {
            PrincipalUserName = identity;
            if (_isPrepared && impersonatingUser == false)
            {
                throw new InvalidOperationException("Context already prepared.");
            }
            var user = _securityRepository.Login(identity);
            _userName = identity;
            if(user != null) {
                _isPrepared = true;
                _dbToken = user.AccessToken;
                _userKey = user.UserKey;
            }
            return user;

        }
        public void Init(string userName, string userData, string principalUser)
        {
            PrincipalUserName = principalUser;
            //todo: may need to validate this to avoid exceptions. maybe before getting here
            string[] usernameparts = userName.Split(UserDataDelimiter.Delimiter);
            UserName = usernameparts[0];
            UserKey = Convert.ToInt32(usernameparts[1]);
            string[] dataParts = userData.Split(UserDataDelimiter.Delimiter);
            DbToken = dataParts[0].ToBytes();
            _isPrepared = true;
        }
        private void ThrowIfNotPrepared()
        {
            if (!_isPrepared)
            {
                throw new InvalidOperationException("Context not prepared.");
            }
        }
        public bool AddNewUser(string userName, string fullName)
        {
            return _securityRepository.AddNewUser(userName, fullName);
        }
    }
}

﻿using System;
using System.Runtime.Caching;
using System.Collections.Specialized;
using System.Collections.Generic;

namespace Esx.Business.Security
{
    public interface ICacheProvider<T> where T : class
    {
        void AddItem(string key, T item, CacheItemPolicy cachePolicy = null);
        T GetItem(string key);

    }
    public class CacheProvider<T> : ICacheProvider<T> where T : class
    {
        private CacheItemPolicy defaultCachePolicy => new CacheItemPolicy()
        {
            SlidingExpiration = new TimeSpan(24, 0, 0)
        };
        private MemoryCache _cacheProvider;
        public CacheProvider()
        {
            _cacheProvider = MemoryCache.Default;
        }
      
        public void AddItem(string key, T item, CacheItemPolicy cachePolicy = null)
        {
            _cacheProvider.Set(key, item, cachePolicy ?? defaultCachePolicy);
        }
        public T GetItem(string key)
        {
            return (T)_cacheProvider[key];
        }
    }
}

﻿using System.Collections.Generic;
using Esx.Data;
using System.Linq;
using Esx.Contracts.Models;

namespace Esx.Business.Resources
{
    public interface IResourcesService
    {
        List<StateProvinceListItem> GetStateProvinceList();

        List<CountryListItem> GetCountryList();
    }

    public class ResourcesService : IResourcesService
    {
        private readonly IEsxDbContext _esxDbContext;
      
        public ResourcesService(IEsxDbContext esxDbContext)
        {
            _esxDbContext = esxDbContext;
        }

        public List<StateProvinceListItem> GetStateProvinceList()
        {
            return _esxDbContext.dESx_StateProvidences.Select(x => new StateProvinceListItem
            {
                StateProvinceCode = x.StateProvidenceCode,
                StateProvinceName = x.StateProvidenceName
            }).ToList();
        }

        public List<CountryListItem> GetCountryList()
        {
            return _esxDbContext.dESx_Countries.Select(x => new CountryListItem
            {
                CountryKey = x.CountryKey,
                CountryName = x.CountryName
            }).ToList();
        }
    }    
}

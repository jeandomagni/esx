﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Document;
using Esx.Data;

namespace Esx.Business.Documents
{
    public interface IDocumentsService
    {
        Document AddDocument(Document documents);
    }

    public class DocumentsService : IDocumentsService
    {
        private readonly IEsxDbContext _dbContext;

        public DocumentsService(IEsxDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Document AddDocument(Document document)
        {
            var desxDocument = new dESx_Document
            {
                Name = document.Name,
                Size = document.Size
            };

            _dbContext.dESx_Documents.Add(desxDocument);            
            _dbContext.SaveChanges();

            document.DocumentKey = desxDocument.DocumentKey;
            return document;
        }
    }
}


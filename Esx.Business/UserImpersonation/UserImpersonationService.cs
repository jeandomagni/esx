﻿using Esx.Data;
using System.Collections.Generic;
using System.Linq;

namespace Esx.Business.UserImpersonation
{
    public interface IUserImpersonationService
    {
        IEnumerable<string> GetUserList();
    }
    public class UserImpersonationService : IUserImpersonationService
    {
        private readonly IEsxDbContext _esxDbContext;
        public UserImpersonationService(IEsxDbContext esxDbContext)
        {
            _esxDbContext = esxDbContext;
        }
        public IEnumerable<string> GetUserList()
        {
            //-102 is the contact key of users to be impersonated for testing
            return _esxDbContext.dESx_Users.Where(u => u.ContactKey == -102).OrderBy(u => u.LoginName)
                .Select(u => u.LoginName).ToList();
        }
    }
}

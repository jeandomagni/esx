﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using Esx.Business.Search;
using Esx.Business.Users;
using Esx.Contracts.Enums;
using Esx.Contracts.ExceptionHandling;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Company;
using Esx.Contracts.Models.Location;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;
using Esx.Contracts.Security;
using Esx.Data;
using Esx.Data.Repositories;

namespace Esx.Business.Companies
{
    public interface ICompaniesService
    {
        CompanyStatistics GetCompanyStatistics(string mentionId);

        CompanySummaryStats GetCompanySummaryStats(string companyId, int pitches, int bidActivity, int holdPeriod,
            int loansMaturing);

        CompanyDetails GetCompanyDetails(string mentionId);

        IEnumerable<CompanyIntellisense> CompanyIntellisense(string searchText, int count);

        CompanyAdd AddCompany(CompanyAdd companyAdd);

        string GetCompanyName(string mentionId);

        PagedResources<CompanyLocation> GetCompanyLocations(string companyMentionId, PagingData pagingData, string searchText);

        int AddCompanyLocation(string companyMentionId, CompanyLocationAdd locationToAdd);

        int AddCompanyContact(string companyMentionId, CompanyContactAdd contact);

        void UpdateCompany(CompanyDetails companydetails);

        Alias AddAlias(Alias alias);

        CompanyLoanAdd AddCompanyLoan(string companyMentionId, CompanyLoanAdd companyLoanToAdd);

        PagedResources<CompanyListItem> Search(CompanySearchCriteria searchCriteria);
    }

    public class CompaniesService : ICompaniesService
    {
        private readonly ICompaniesRepository _companiesRepository;
        private readonly IEsxDbContext _dbContext;
        private readonly IUsersService _usersService;
        private readonly ISecurityContext _securityContext;
        private readonly IDealCriteriaSetService _dealCriteriaSetService;
        private readonly ILoanCriteriaSetService _loanCriteriaSetService;
        private readonly ICompanyCriteriaSetService _companyCriteriaSetService;
        private readonly IPropertyCriteriaSetService _propertyCriteriaSetService;

        public CompaniesService(
            ICompaniesRepository companiesRepository,
            IEsxDbContext dbContext,
            IUsersService usersService,
            ISecurityContext securityContext,
            IDealCriteriaSetService dealCriteriaSetService,
            ILoanCriteriaSetService loanCriteriaSetService,
            ICompanyCriteriaSetService companyCriteriaSetService,
            IPropertyCriteriaSetService propertyCriteriaSetService)
        {
            _companiesRepository = companiesRepository;
            _dbContext = dbContext;
            _usersService = usersService;
            _securityContext = securityContext;
            _dealCriteriaSetService = dealCriteriaSetService;
            _loanCriteriaSetService = loanCriteriaSetService;
            _companyCriteriaSetService = companyCriteriaSetService;
            _propertyCriteriaSetService = propertyCriteriaSetService;
        }

        public PagedResources<CompanyListItem> Search(CompanySearchCriteria searchCriteria)
        {
            if (searchCriteria == null) throw new NullReferenceException(nameof(searchCriteria));

            var companyList = from company in _dbContext.dESx_Companies
                              join companyAlias in _dbContext.dESx_CompanyAlias on company.CompanyKey equals companyAlias.CompanyKey
                              join companyMention in _dbContext.dESx_Mentions on companyAlias.CompanyAliasKey equals
                                  companyMention.AliasKey
                              where companyMention.IsPrimary
                              from userMentionData in
                                  _dbContext.dESx_UserMentionDatas.Where(
                                      x => x.UserKey == _securityContext.UserKey && x.MentionKey == companyMention.MentionKey)
                                      .DefaultIfEmpty()
                              select new
                              {
                                  // Only needed for further filtering below, so it's not in the result object.
                                  AliasIsPrimary = companyAlias.IsPrimary,
                                  CompanyRecord = company,

                                  // The object to display to the user.
                                  Row = new CompanyListItem
                                  {
                                      CompanyKey = company.CompanyKey,
                                      CanonicalCompany = companyAlias.AliasName,
                                      IsWatched = userMentionData != null && userMentionData.IsWatched,
                                      MentionID = companyMention.MentionId
                                  }
                              };

            if (searchCriteria.SearchPrimaryAlias.HasValue)
            {
                companyList = companyList.Where(x => x.AliasIsPrimary == searchCriteria.SearchPrimaryAlias);
            }

            // Add search criteria if specified.
            if (!string.IsNullOrWhiteSpace(searchCriteria.SearchText) && searchCriteria.SearchText.Length > 2)
            {
                // Since we're only searching the company alias column, it's easier to just add it as a search "chip"
                // rather than repeat all the related alias logic here.
                searchCriteria.CompanyCriteriaSet = searchCriteria.CompanyCriteriaSet ?? new CompanyCriteriaSet { CompanyNames = Enumerable.Empty<string>() };
                searchCriteria.CompanyCriteriaSet.CompanyNames = Enumerable.Repeat(searchCriteria.SearchText, 1).Concat(searchCriteria.CompanyCriteriaSet.CompanyNames);
            }

            // If searching companies using filters.
            if (searchCriteria.CompanyCriteriaSet != null)
            {
                var companyKeys = _companyCriteriaSetService.MatchingCompanyKeys(_dbContext, searchCriteria.CompanyCriteriaSet);
                companyList = companyList.Where(x => companyKeys.Any(key => x.Row.CompanyKey == key));
            }

            // If searching companies by deal.
            if (searchCriteria.DealCriteriaSet != null)
            {
                var dealKeys = _dealCriteriaSetService.MatchingDealKeys(_dbContext, searchCriteria.DealCriteriaSet);
                var companyDeals = _dbContext.dESx_DealCompanies.Where(x => dealKeys.Any(key => key == x.DealKey));
                companyList = companyList.Where(x => companyDeals.Any(cd => cd.CompanyKey == x.Row.CompanyKey));
            }

            // If searching companies by loan.
            if (searchCriteria.LoanCriteriaSet != null)
            {
                var loanAssetKeys = _loanCriteriaSetService.MatchingLoanAssetKeys(_dbContext, searchCriteria.LoanCriteriaSet);
                var companyLoans = _dbContext.dESx_CompanyAssets.Where(x => loanAssetKeys.Any(key => key == x.AssetKey));
                companyList = companyList.Where(x => companyLoans.Any(cl => cl.CompanyKey == x.Row.CompanyKey));
            }

            // If searching companies by property.
            if (searchCriteria.PropertyCriteriaSet != null)
            {
                var propertyAssetKeys = _propertyCriteriaSetService.MatchingPropertyAssetKeys(_dbContext, searchCriteria.PropertyCriteriaSet);
                var companyProperties = _dbContext.dESx_CompanyAssets.Where(x => x.IsCurrent && propertyAssetKeys.Any(key => key == x.AssetKey));
                companyList = companyList.Where(x => companyProperties.Any(cp => cp.CompanyKey == x.Row.CompanyKey));
            }

            if (string.IsNullOrEmpty(searchCriteria.Sort))
            {
                searchCriteria.Sort = "IsWatched";
                searchCriteria.SortDescending = true;
            }
            var sortDirection = searchCriteria.SortDescending ? "descending" : "ascending";
            var sort = $"Row.{searchCriteria.Sort} {sortDirection}, Row.CanonicalCompany ascending";

            searchCriteria.TotalCount = companyList.Count();
            companyList = companyList.OrderBy(sort).Skip(searchCriteria.Offset).Take(searchCriteria.PageSize);

            return new PagedResources<CompanyListItem>
            {
                PagingData = searchCriteria,
                Resources = companyList.Select(x => x.Row).AsNoTracking().ToList()
            };
        }

        public CompanyStatistics GetCompanyStatistics(string mentionId)
        {
            return _companiesRepository.GetCompanyStatistics(mentionId);
        }

        public CompanySummaryStats GetCompanySummaryStats(string companyId, int pitches, int bidActivity,
            int holdPeriod, int loansMaturing)
        {
            return _companiesRepository.GetCompanySummaryStats(companyId, pitches, bidActivity, holdPeriod,
                loansMaturing);
        }

        public CompanyDetails GetCompanyDetails(string mentionId)
        {
            //get company details
            var details = (from m in _dbContext.dESx_Mentions
                           join a in _dbContext.dESx_CompanyAlias on m.AliasKey equals a.CompanyAliasKey
                           join c in _dbContext.dESx_Companies on a.CompanyKey equals c.CompanyKey
                           from o in _dbContext.dESx_Offices.Where(x => x.CompanyKey == c.CompanyKey && x.IsHeadquarters).DefaultIfEmpty()
                           from p in _dbContext.dESx_PhysicalAddresses.Where(x => x.PhysicalAddressKey == o.PhysicalAddressKey).DefaultIfEmpty()
                           from cu in _dbContext.dESx_Countries.Where(x => x.CountryKey == p.CountryKey).DefaultIfEmpty()
                           where m.MentionId == mentionId && a.IsPrimary

                           select new CompanyDetails()
                           {
                               MentionId = mentionId,
                               CompanyKey = c.CompanyKey,
                               CompanyName = a.AliasName,
                               Office = (o != null) ? new Office
                               {
                                   OfficeKey = o.OfficeKey,
                                   OfficeName = o.OfficeName,
                                   IsHeadQuarters = o.IsHeadquarters,
                                   PhysicalAddress = new PhysicalAddress
                                   {
                                       PhysicalAddressKey = p.PhysicalAddressKey,
                                       AddressTypeCode = p.PhysicalAddressTypeCode,
                                       Address1 = p.Address1,
                                       Address2 = p.Address2,
                                       Address3 = p.Address3,
                                       Address4 = p.Address4,
                                       City = p.City,
                                       StateProvidenceCode = p.StateProvidenceCode,
                                       PostalCode = p.PostalCode,
                                       County = p.County,
                                       Country = cu.CountryName,
                                       Latitude = p.Latitude,
                                       Longitude = p.Longitude
                                   }
                               } : null,
                               TickerSymbol = c.TickerSymbol,
                               IsSolicitable = c.IsSolicitable,
                               IsSolicitableDate = c.IsSolicitableDate,
                               IsDefunct = c.IsDefunct,
                               FcpaName = c.FcpaName,
                               IsOfacCheck = (c.OfacCheckDate.HasValue),
                               OfacCheckDate = c.OfacCheckDate,
                               RealEstateAllocation = c.RealEstateAllocation,
                               HasWfsCorrelation = c.HasWfsCorrelation,
                               CibosCode = c.CibosCode,
                               IsForeignGovernment = c.IsForeignGovernment,
                               IsTrackedByAsiaTeam = c.IsTrackedByAsiaTeam,
                               HasRestrictedVisibility = c.HasRestrictedVisibility
                           }).FirstOrDefault();

            // get Aliases
            if (details != null)
            {
                var aliases = (from a in _dbContext.dESx_CompanyAlias
                               from m in _dbContext.dESx_Mentions.Where(x => x.AliasKey == a.CompanyAliasKey).DefaultIfEmpty()
                               where a.CompanyKey == details.CompanyKey
                               select new Alias
                               {
                                   AliasName = a.AliasName,
                                   AliasKey = a.CompanyAliasKey,
                                   IsLegal = a.IsLegal,
                                   IsPrimary = a.IsPrimary,
                                   IsDefunct = a.IsDefunct,
                                   MentionId = (m != null) ? m.MentionId : ""
                               }).ToList();

                details.Aliases = aliases;
            }
            // get Electronic Addresses
            if (details?.Office != null)
            {
                var electronicAddress = (from ce in _dbContext.dESx_CompanyElectronicAddresses
                                         from e in
                                             _dbContext.dESx_ElectronicAddresses.Where(x => x.ElectronicAddressKey == ce.ElectronicAddressKey)
                                                 .DefaultIfEmpty()
                                         where ce.CompanyKey == details.CompanyKey
                                         select new ElectronicAddress
                                         {
                                             ElectronicAddressKey = e.ElectronicAddressKey,
                                             ElectronicAddressTypeCode = e.ElectronicAddressTypeCode,
                                             AddressUsageTypeCode = e.AddressUsageTypeCode,
                                             Description = e.Description,
                                             Address = e.Address
                                         }
                    ).ToList();
                details.Office.ElectronicAddress = electronicAddress;
            }

            // get Parent Companies
            if (details != null)
            {
                var parentCompany = (from c in _dbContext.dESx_Companies
                                     join h in _dbContext.dESx_CompanyHierarchies on c.CompanyKey equals h.ChildCompanyKey
                                     join a in _dbContext.dESx_CompanyAlias on h.ParentCompanyKey equals a.CompanyKey
                                     join m in _dbContext.dESx_Mentions on a.CompanyAliasKey equals m.AliasKey
                                     where c.CompanyKey == details.CompanyKey
                                     select new ParentCompany
                                     {
                                         CompanyHierarchyKey = h.CompanyHierarchyKey,
                                         ParentCompanyKey = h.ParentCompanyKey,
                                         ChildCompanyKey = h.ChildCompanyKey,
                                         ParentCompanyName = a.AliasName,
                                         ParentMentionId = m.MentionId
                                     }).FirstOrDefault();


                details.ParentCompany = parentCompany;
            }

            return details;

        }

        public string GetCompanyName(string mentionId)
        {
            return (from alias in _dbContext.dESx_CompanyAlias
                    join mention in _dbContext.dESx_Mentions on alias.CompanyAliasKey equals mention.AliasKey
                    where mention.MentionId == mentionId
                    select alias.AliasName).FirstOrDefault();
        }

        public IEnumerable<CompanyIntellisense> CompanyIntellisense(string searchText, int count)
        {
            return _companiesRepository.GetCompanyIntellisense(searchText, count);
        }

        public PagedResources<CompanyLocation> GetCompanyLocations(string companyMentionId, PagingData pagingData, string searchText)
        {
            // Define the electronic address subquery.
            var eAddresses = from address in _dbContext.dESx_ElectronicAddresses
                             join locAddrs in _dbContext.dESx_OfficeElectronicAddresses on address.ElectronicAddressKey
                                 equals locAddrs.ElectronicAddressKey
                             join office in _dbContext.dESx_Offices on locAddrs.OfficeKey equals office.OfficeKey
                             where address.AddressUsageTypeCode == AddressUsageType.Main && address.ElectronicAddressTypeCode == ElectronicAddressType.PhoneNumber
                             select new { office.OfficeKey, Value = address.Address };

            // Define the main locations query.
            var locations = from mention in _dbContext.dESx_Mentions
                            join alias in _dbContext.dESx_CompanyAlias on mention.AliasKey equals alias.CompanyAliasKey
                            join office in _dbContext.dESx_Offices on alias.CompanyKey equals office.CompanyKey
                            join address in _dbContext.dESx_PhysicalAddresses on office.PhysicalAddressKey equals address.PhysicalAddressKey
                            where mention.MentionId == companyMentionId
                            let phone = eAddresses.FirstOrDefault(p => p.OfficeKey == office.OfficeKey)
                            select new CompanyLocation
                            {
                                OfficeKey = office.OfficeKey,
                                IsHeadquarters = office.IsHeadquarters,
                                Name = office.OfficeName,
                                Address1 = address.Address1,
                                Address2 = address.Address2,
                                Address = address.Address1 + " " + address.Address2,
                                City = address.City,
                                StateProvince = address.StateProvidenceCode,
                                Zip = address.PostalCode,
                                CountryKey = address.CountryKey,
                                Phone = phone.Value,
                                Contacts = _dbContext.dESx_ContactOffices.Count(co => co.OfficeKey == office.OfficeKey && co.IsCurrent)
                            };

            // Add search criteria if specified.
            if (!string.IsNullOrWhiteSpace(searchText) && searchText.Length > 2)
            {
                locations = from l in locations
                            where (l.Name != null && l.Name.Contains(searchText))
                                  || (l.Address != null && l.Address.Contains(searchText))
                                  || (l.City != null && l.City.Contains(searchText))
                                  || (l.StateProvince != null && l.StateProvince.Contains(searchText))
                                  || (l.Phone != null && l.Phone.Contains(searchText))
                            select l;
            }

            // Add sorting.
            if (!string.IsNullOrWhiteSpace(pagingData.Sort))
            {
                var dir = pagingData.SortDescending ? "DESC" : "ASC";
                var sort = $"{pagingData.Sort} {dir}";
                locations = locations.OrderBy(sort);
            }
            else
            {
                locations = locations.OrderBy("Name ASC");
            }

            // Get total record count.
            pagingData.TotalCount = locations.Count();

            // Add offset and page size if specified.
            if (pagingData.Offset > 0) locations = locations.Skip(pagingData.Offset);
            if (pagingData.PageSize > 0) locations = locations.Take(pagingData.PageSize);

            return new PagedResources<CompanyLocation>
            {
                PagingData = pagingData,
                Resources = locations.ToList()
            };

        }

        public CompanyAdd AddCompany(CompanyAdd companyAddModel)
        {
            var existingAlias = _dbContext.dESx_CompanyAlias.FirstOrDefault(a => a.AliasName == companyAddModel.CompanyName);

            if (existingAlias != null)
                throw new BadRequestException("Company name already exists.");

            var company = new dESx_Company { IsCanonical = true };


            if (companyAddModel.ParentCompany != 0)
            {
                var topCompanyKey =
                    _dbContext.dESx_CompanyHierarchies.Where(
                        rel =>
                            rel.ParentCompanyKey == companyAddModel.ParentCompany ||
                            rel.ChildCompanyKey == companyAddModel.ParentCompany).Select(x => x.TopParentCompanyKey).FirstOrDefault();

                company.dESx_CompanyHierarchies_ChildCompanyKey.Add(new dESx_CompanyHierarchy
                {
                    ParentCompanyKey = companyAddModel.ParentCompany,
                    TopParentCompanyKey = topCompanyKey != 0 ? topCompanyKey : companyAddModel.ParentCompany
                });
            }

            var primaryAlias = new dESx_CompanyAlias
            {
                AliasName = companyAddModel.CompanyName
            };
            company.dESx_CompanyAlias.Add(primaryAlias);

            if (!string.IsNullOrEmpty(companyAddModel.AliasName))
            {
                var secondaryAlias = new dESx_CompanyAlias
                {
                    AliasName = companyAddModel.AliasName,
                    IsLegal = false,
                    IsPrimary = false
                };
                company.dESx_CompanyAlias.Add(secondaryAlias);
            }

            var office = new dESx_Office
            {
                OfficeName = companyAddModel.LocationName ?? string.Empty,
                IsHeadquarters = true,
                dESx_PhysicalAddress = new dESx_PhysicalAddress
                {
                    Address1 = companyAddModel.LocationAddress1,
                    Address2 = companyAddModel.LocationAddress2 ?? string.Empty,
                    City = companyAddModel.LocationCity,
                    StateProvidenceCode = companyAddModel.LocationState,
                    PostalCode = companyAddModel.LocationZip,
                    CountryKey = int.Parse(companyAddModel.LocationCountry),
                    PhysicalAddressTypeCode = "Company"
                }
            };

            company.dESx_CompanyElectronicAddresses.Add(new dESx_CompanyElectronicAddress
            {
                dESx_ElectronicAddress = new dESx_ElectronicAddress
                {
                    Address = companyAddModel.Website ?? string.Empty,
                    Description = ElectronicAddressType.Website,
                    ElectronicAddressTypeCode = ElectronicAddressType.Website,
                    AddressUsageTypeCode = AddressUsageType.Main
                }
            });

            dESx_Contact contact;

            if (companyAddModel.ContactKey != 0)
            {
                contact = _dbContext.dESx_Contacts.Single(c => c.ContactKey == companyAddModel.ContactKey);
            }
            else
            {
                contact = new dESx_Contact
                {
                    FullName = companyAddModel.ContactName,
                    Title = companyAddModel.ContactTitle,
                    LastName = companyAddModel.ContactName
                };
                contact.dESx_ContactElectronicAddresses.Add(new dESx_ContactElectronicAddress
                {
                    dESx_ElectronicAddress = new dESx_ElectronicAddress
                    {
                        Address = companyAddModel.ContactEmailAddress,
                        Description = "Email",
                        ElectronicAddressTypeCode = ElectronicAddressType.EmailAddress,
                        AddressUsageTypeCode = AddressUsageType.Main
                    }
                });
                if (!string.IsNullOrEmpty(companyAddModel.ContactMobilePhone))
                {
                    contact.dESx_ContactElectronicAddresses.Add(new dESx_ContactElectronicAddress
                    {
                        dESx_ElectronicAddress = new dESx_ElectronicAddress
                        {
                            Address = companyAddModel.ContactMobilePhone,
                            Description = "Phone",
                            ElectronicAddressTypeCode = ElectronicAddressType.PhoneNumber,
                            AddressUsageTypeCode = AddressUsageType.Main
                        }
                    });
                }
                if (!string.IsNullOrEmpty(companyAddModel.ContactOfficePhone))
                {
                    contact.dESx_ContactElectronicAddresses.Add(new dESx_ContactElectronicAddress
                    {
                        dESx_ElectronicAddress = new dESx_ElectronicAddress
                        {
                            Address = companyAddModel.ContactOfficePhone,
                            Description = "Phone",
                            ElectronicAddressTypeCode = ElectronicAddressType.PhoneNumber,
                            AddressUsageTypeCode = AddressUsageType.Main
                        }
                    });
                }
                if (!string.IsNullOrEmpty(companyAddModel.ContactHomePhone))
                {
                    contact.dESx_ContactElectronicAddresses.Add(new dESx_ContactElectronicAddress
                    {
                        dESx_ElectronicAddress = new dESx_ElectronicAddress
                        {
                            Address = companyAddModel.ContactHomePhone,
                            Description = "Phone",
                            ElectronicAddressTypeCode = ElectronicAddressType.PhoneNumber,
                            AddressUsageTypeCode = AddressUsageType.Main
                        }
                    });
                }

                office.dESx_ContactOffices.Add(new dESx_ContactOffice
                {
                    ContactOfficeRoleCode = "HomeOffice"
                });
            }
            company.dESx_Offices.Add(office);
            var savedCompany = _dbContext.dESx_Companies.Add(company);

            if (contact != null && contact.ContactKey <= 0)
            {
                _dbContext.dESx_Contacts.Add(contact);
            }

            try
            {
                _dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new BadRequestException(e.Message);
            }

            if (companyAddModel.SetDefaultCountry)
            {
                _usersService.SaveUserPreference("Country", companyAddModel.LocationCountry);
            }

            companyAddModel.CompanyID = savedCompany.CompanyKey;
            return companyAddModel;
        }

        public int AddCompanyContact(string companyMentionId, CompanyContactAdd contactToAdd)
        {
            if (contactToAdd == null) throw new ArgumentNullException(nameof(contactToAdd));
            if (contactToAdd.OfficeKey == null && contactToAdd.Location == null) throw new InvalidOperationException("A new or existing office must be specified");

            // Add the new location if an existing one wasn't selected.
            if (!contactToAdd.OfficeKey.HasValue)
            {
                contactToAdd.OfficeKey = AddCompanyLocation(companyMentionId, contactToAdd.Location);
            }

            // Create the contact.
            var contact = _dbContext.dESx_Contacts.Add(new dESx_Contact
            {
                FullName = contactToAdd.ContactName,
                LastName = contactToAdd.ContactName.Split(' ').LastOrDefault() ?? "",
                Title = contactToAdd.Title
            });

            // Relate the contact to the office.
            contact.dESx_ContactOffices.Add(new dESx_ContactOffice
            {
                OfficeKey = contactToAdd.OfficeKey.Value,
                ContactOfficeRoleCode = ContactOfficeRole.HomeOffice //TODO: This can't be right, but there's only one option in the contact office role table to choose from.
            });

            // Define a helper function for adding electronic addresses to contacts.
            Action<string, string, string> addElectronicAddress = (address, addressTypeCode, usageTypeCode) =>
            {
                if (!string.IsNullOrWhiteSpace(address))
                {
                    contact.dESx_ContactElectronicAddresses.Add(new dESx_ContactElectronicAddress
                    {
                        dESx_ElectronicAddress = new dESx_ElectronicAddress
                        {
                            Address = address,
                            ElectronicAddressTypeCode = addressTypeCode,
                            AddressUsageTypeCode = usageTypeCode
                        }
                    });
                }
            };

            // Add the contact electronic addresses.
            addElectronicAddress(contactToAdd.MobilePhone, ElectronicAddressType.PhoneNumber, AddressUsageType.Mobile);
            addElectronicAddress(contactToAdd.OfficePhone, ElectronicAddressType.PhoneNumber, AddressUsageType.Main);
            addElectronicAddress(contactToAdd.EmailAddress, ElectronicAddressType.EmailAddress, AddressUsageType.Main);
            addElectronicAddress(contactToAdd.Website, ElectronicAddressType.Website, AddressUsageType.Main);

            _dbContext.SaveChanges();

            return contact.ContactKey;
        }

        public int AddCompanyLocation(string companyMentionId, CompanyLocationAdd locationToAdd)
        {

            var company = GetCompanyByMentionId(companyMentionId);
            if (company == null)
            {
                throw new NotFoundException($"Company not found for: {companyMentionId}");
            }

            var dup = (from o in _dbContext.dESx_Offices
                       join a in _dbContext.dESx_PhysicalAddresses on o.PhysicalAddressKey equals a.PhysicalAddressKey
                       where a.Address1.Equals(locationToAdd.Address1, StringComparison.InvariantCultureIgnoreCase) &&
                             a.Address2.Equals(locationToAdd.Address2, StringComparison.InvariantCultureIgnoreCase) &&
                             a.Address3.Equals(locationToAdd.Address3, StringComparison.InvariantCultureIgnoreCase) &&
                             a.Address4.Equals(locationToAdd.Address4, StringComparison.InvariantCultureIgnoreCase) &&
                             a.City.Equals(locationToAdd.City, StringComparison.InvariantCultureIgnoreCase) &&
                             a.StateProvidenceCode.Equals(locationToAdd.State, StringComparison.InvariantCultureIgnoreCase) &&
                             a.PostalCode.Equals(locationToAdd.Zip, StringComparison.InvariantCultureIgnoreCase) &&
                             a.CountryKey.Equals(locationToAdd.CountryKey)
                       select a);
            if (dup.Any())
            {
                throw new DuplicateException($"A location with this address already exists.");
            }

            if (locationToAdd.IsPrimary)
            {
                var existingHq =
                    _dbContext.dESx_Offices
                        .FirstOrDefault(o => o.CompanyKey == company.CompanyKey && o.IsHeadquarters);
                if (existingHq != null)
                    existingHq.IsHeadquarters = false;
            }

            var office = new dESx_Office
            {
                OfficeName = locationToAdd.Name ?? string.Empty,
                IsHeadquarters = locationToAdd.IsPrimary,
                dESx_PhysicalAddress = new dESx_PhysicalAddress
                {
                    Address1 = locationToAdd.Address1,
                    Address2 = locationToAdd.Address2,
                    Address3 = locationToAdd.Address3,
                    Address4 = locationToAdd.Address4,
                    City = locationToAdd.City,
                    StateProvidenceCode = locationToAdd.State,
                    PostalCode = locationToAdd.Zip,
                    CountryKey = locationToAdd.CountryKey,
                    PhysicalAddressTypeCode = PhysicalAddressType.Company.Code
                }
            };

            if (!string.IsNullOrEmpty(locationToAdd.Phone))
            {
                var websiteMap = new dESx_OfficeElectronicAddress
                {
                    dESx_ElectronicAddress = new dESx_ElectronicAddress
                    {
                        Address = locationToAdd.Website ?? string.Empty,
                        Description = ElectronicAddressType.Website,
                        ElectronicAddressTypeCode = ElectronicAddressType.Website,
                        AddressUsageTypeCode = AddressUsageType.Main
                    }
                };
                office.dESx_OfficeElectronicAddresses.Add(websiteMap);
            }

            if (!string.IsNullOrEmpty(locationToAdd.Phone))
            {
                var phoneMap = new dESx_OfficeElectronicAddress
                {
                    dESx_ElectronicAddress = new dESx_ElectronicAddress
                    {
                        Address = locationToAdd.Phone,
                        Description = ElectronicAddressType.PhoneNumber,
                        ElectronicAddressTypeCode = ElectronicAddressType.PhoneNumber,
                        AddressUsageTypeCode = AddressUsageType.Main
                    }
                };
                office.dESx_OfficeElectronicAddresses.Add(phoneMap);
            }

            company.dESx_Offices.Add(office);

            _dbContext.SaveChanges();

            return office.OfficeKey;

        }

        private dESx_Company GetCompanyByMentionId(string mentionId)
        {
            return (from m in _dbContext.dESx_Mentions
                    join a in _dbContext.dESx_CompanyAlias on m.AliasKey equals a.CompanyAliasKey
                    join c in _dbContext.dESx_Companies on a.CompanyKey equals c.CompanyKey
                    where m.MentionId == mentionId
                    select c).FirstOrDefault();
        }

        public void UpdateCompany(CompanyDetails details)
        {
            //_companiesRepository.UpdateCompany(details);
            var company = _dbContext.dESx_Companies.FirstOrDefault(c => c.CompanyKey == details.CompanyKey);

            //dESx_Alias newPrimaryAlias = null;

            if (company == null)
            {
                throw new NotFoundException($"Company not found for: {details.MentionId}");
            }

            if (details.HasProfileChanged)
            {
                // PARENT COMPANY
                if (details.ParentCompany?.CompanyHierarchyKey > 0)
                {
                    var parentCompany = _dbContext.dESx_CompanyHierarchies.FirstOrDefault(h => h.CompanyHierarchyKey == details.ParentCompany.CompanyHierarchyKey);
                    if (parentCompany != null)
                    {
                        if (details.ParentCompany.ParentCompanyKey > 0)
                        {
                            // UPDATE Parent Company
                            parentCompany.ParentCompanyKey = details.ParentCompany.ParentCompanyKey;
                        }
                        else
                        {
                            // DELETE Parent Company
                            _dbContext.dESx_CompanyHierarchies.Remove(parentCompany);
                        }
                    }
                }
                else
                {
                    if (details.ParentCompany?.ParentCompanyKey > 0)
                    {
                        // ADD Parent Company
                        company.dESx_CompanyHierarchies_ChildCompanyKey.Add(new dESx_CompanyHierarchy
                        {
                            ParentCompanyKey = details.ParentCompany.ParentCompanyKey
                        });
                    }
                }

                company.IsDefunct = details.IsDefunct;
                company.TickerSymbol = details.TickerSymbol;
            }

            else
            {
                // COMPLIANCE Updates
                company.IsSolicitable = details.IsSolicitable;
                company.IsSolicitableDate = (details.IsSolicitable) ? details.IsSolicitableDate : null;
                company.FcpaName = details.FcpaName;
                company.OfacCheckDate = (details.IsOfacCheck && !details.OfacCheckDate.HasValue) ? DateTime.UtcNow.Date : (DateTime?)null;
                company.RealEstateAllocation = details.RealEstateAllocation;
                company.HasWfsCorrelation = details.HasWfsCorrelation;
                company.CibosCode = details.CibosCode;
                company.IsForeignGovernment = details.IsForeignGovernment;
                company.IsTrackedByAsiaTeam = details.IsTrackedByAsiaTeam;
                company.HasRestrictedVisibility = details.HasRestrictedVisibility;
            }

            try
            {
                _dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new BadRequestException(e.Message);
            }
        }

        public Alias AddAlias(Alias alias)
        {
            var company = _dbContext.dESx_Companies.FirstOrDefault(c => c.CompanyKey == alias.CompanyKey);

            var isAliasPrimary = alias.IsPrimary;

            if (company == null)
            {
                throw new NotFoundException($"Company not found for: {alias.CompanyKey}");
            }

            if (alias.IsPrimary)
            {
                var primaryAlias = _dbContext.dESx_CompanyAlias.SingleOrDefault(x => x.CompanyKey == alias.CompanyKey && x.IsPrimary);

                if (primaryAlias != null)
                {
                    primaryAlias.IsPrimary = false;
                }
            }

            var newAlias = new dESx_CompanyAlias
            {
                AliasName = alias.AliasName,
                IsPrimary = isAliasPrimary,
                IsLegal = false,
                IsDefunct = false
            };

            company.dESx_CompanyAlias.Add(newAlias);

            try
            {
                _dbContext.SaveChanges();

                alias.AliasKey = newAlias.CompanyAliasKey;
                alias.MentionId = newAlias.MentionId;

                return alias;
            }
            catch (Exception e)
            {
                throw new BadRequestException(e.Message);
            }

        }

        public CompanyLoanAdd AddCompanyLoan(string companyMentionId, CompanyLoanAdd companyLoanToAdd)
        {

            var company = GetCompanyByMentionId(companyMentionId);

            if (company == null)
            {
                throw new NotFoundException($"Company not found for: {companyMentionId}");
            }
            if (string.IsNullOrEmpty(companyLoanToAdd.LoanBenchmarkType) || string.IsNullOrEmpty(companyLoanToAdd.LoanCashMgmtType) || string.IsNullOrEmpty(companyLoanToAdd.LoanPaymentType)
                || string.IsNullOrEmpty(companyLoanToAdd.LoanRateType) || string.IsNullOrEmpty(companyLoanToAdd.LoanSourceType))
            {
                // ReSharper disable once NotResolvedInText
                throw new ArgumentNullException("Loan type code cannot be null or empty");
            }

            var propertyAsset = _dbContext.dESx_Assets.FirstOrDefault(a => a.AssetKey == companyLoanToAdd.PropertyKey);
            if (propertyAsset == null)
            {
                propertyAsset = _dbContext.dESx_Assets.Add(new dESx_Asset
                {
                    AssetTypeCode = AssetType.Property.Code,
                    AssetKey = companyLoanToAdd.PropertyKey
                });
                propertyAsset.dESx_AssetNames.Add(new dESx_AssetName
                {
                    AssetName = companyLoanToAdd.PropertyName
                });
            }

            var loanAsset = _dbContext.dESx_Assets.Add(new dESx_Asset
            {
                AssetTypeCode = AssetType.Loan.Code
            });


            var loanAssetName = new dESx_AssetName
            {
                AssetName = companyLoanToAdd.LoanName
            };
            loanAsset.dESx_AssetNames.Add(loanAssetName);


            var assetLoanTerm = new dESx_AssetLoanTerm
            {
                LoanName = companyLoanToAdd.LoanName,
                LoanAmount = companyLoanToAdd.LoanAmount,
                OriginationDate = companyLoanToAdd.OriginationDate,
                MaturationDate = companyLoanToAdd.MaturationDate,
                LoanSourceTypeCode = companyLoanToAdd.LoanSourceType,
                LoanPaymentTypeCode = companyLoanToAdd.LoanPaymentType,
                LoanRateTypeCode = companyLoanToAdd.LoanRateType,
                LoanCashMgmtTypeCode = companyLoanToAdd.LoanCashMgmtType,
                LoanBenchmarkTypeCode = companyLoanToAdd.LoanBenchmarkType,
                LoanRate = companyLoanToAdd.LoanRate
            };
            loanAsset.dESx_AssetLoanTerm = assetLoanTerm;


            var assetLoanProperty = new dESx_AssetLoanProperty
            {
                dESx_Asset_LoanAssetKey = loanAsset,
                dESx_Asset_PropertyAssetKey = propertyAsset
            };
            _dbContext.dESx_AssetLoanProperties.Add(assetLoanProperty);

            var assetLoanBorrower = new dESx_AssetLoanBorrower
            {
                CompanyKey = companyLoanToAdd.IsBorrower ? company.CompanyKey : companyLoanToAdd.BorrowerLenderCompanyKey,
            };
            loanAsset.dESx_AssetLoanBorrowers.Add(assetLoanBorrower);


            var companyAsset = new dESx_CompanyAsset
            {
                CompanyKey = companyLoanToAdd.IsBorrower ? companyLoanToAdd.BorrowerLenderCompanyKey : company.CompanyKey
            };
            loanAsset.dESx_CompanyAssets.Add(companyAsset);

            _dbContext.SaveChanges();

            companyLoanToAdd.MentionId = loanAssetName.MentionId;

            return companyLoanToAdd;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using Esx.Contracts.Models;
using Esx.Contracts.Models.User;
using Esx.Contracts.Paging;
using Esx.Contracts.Security;
using Esx.Data;
using System.Linq.Dynamic;
using System.Security;
using System;

namespace Esx.Business.Users
{
    public interface IUsersService
    {
        IEnumerable<UserPreferenceListItem> GetUserPreferences();
        UserPreferenceListItem SaveUserPreference(string preferenceKey, string preferenceValue);
        PagedResources<UserListItem> GetUsersList(PagingData pagingData, string searchText = null);
        void SetDelegatedUser(int userKey, bool isDelegated);
        IEnumerable<UserListItem> GetDelegatedUserList();
        User GetCurrentUser();
    }

    public class UsersService : IUsersService
    {
        private readonly IEsxDbContext _esxDbContext;
        private readonly ISecurityContext _securityContext;
        public UsersService(IEsxDbContext esxDbContext, ISecurityContext securityContext)
        {
            _esxDbContext = esxDbContext;
            _securityContext = securityContext;
        }

        public IEnumerable<UserPreferenceListItem> GetUserPreferences()
        {            
            return _esxDbContext.dESx_UserPreferences.Where(u => u.UserKey == _securityContext.UserKey).Select(x => new UserPreferenceListItem
            {
                PreferenceKey = x.PreferenceTypeCode,
                PreferenceValue = x.PreferenceValue
            });
        }

        public UserPreferenceListItem SaveUserPreference(string preferenceKey, string preferenceValue)
        {
            var addedItem = _esxDbContext.dESx_UserPreferences.Add(new dESx_UserPreference
            {
                UserKey = _securityContext.UserKey,
                PreferenceTypeCode = preferenceKey,
                PreferenceValue = preferenceValue
            });
            _esxDbContext.SaveChanges();
            return new UserPreferenceListItem
            {
                UserId = addedItem.UserKey,
                PreferenceKey = addedItem.PreferenceTypeCode,
                PreferenceValue = addedItem.PreferenceValue
            };
        }

        public PagedResources<UserListItem> GetUsersList(PagingData pagingData, string searchText = null)
        {
            if (_securityContext.UserName.Equals(_securityContext.PrincipalUserName, StringComparison.CurrentCultureIgnoreCase) == false)
            {
                throw new SecurityException("Access denied.");
            }
            var users = from user in _esxDbContext.dESx_Users
                        from delegatedUser in _esxDbContext.dESx_UserDelegates.Where(u => u.UserKey == user.UserKey
                        && u.DelegateUserKey == _securityContext.UserKey).DefaultIfEmpty()
                        join contact in _esxDbContext.dESx_Contacts on user.ContactKey equals contact.ContactKey
                        where user.UserKey != _securityContext.UserKey
                        select new UserListItem
                        {
                            UserKey = user.UserKey,
                            Username = user.LoginName,
                            FullName = contact.FullName,
                            IsDelegatedUser = delegatedUser != null

                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                users = from u in users
                        where (u.FullName != null && u.FullName.ToUpper().Contains(searchText.ToUpper()))
                        select u;
            }

            pagingData.TotalCount = users.Count();
            users = users.OrderBy("IsDelegatedUser DESC").Skip(pagingData.Offset).Take(pagingData.PageSize);

            return new PagedResources<UserListItem>
            {
                PagingData = pagingData,
                Resources = users.ToList()
            };
        }
        public void SetDelegatedUser(int userKey, bool isDelegated)
        {
            var delegatedUser = _esxDbContext.dESx_UserDelegates.FirstOrDefault(u => u.DelegateUserKey == _securityContext.UserKey
          && u.UserKey == userKey);
            if(isDelegated == false && delegatedUser != null)
            {
                _esxDbContext.dESx_UserDelegates.Remove(delegatedUser);
                _esxDbContext.SaveChanges();
            }
            if (isDelegated == true && delegatedUser == null)
            {
                _esxDbContext.dESx_UserDelegates.Add(
                    new dESx_UserDelegate { UserKey = userKey, DelegateUserKey = _securityContext.UserKey }
                    );
                _esxDbContext.SaveChanges();
            }
            
        }
        public IEnumerable<UserListItem> GetDelegatedUserList()
        {
            var principaUser = (from user in _esxDbContext.dESx_Users
                               join contact in _esxDbContext.dESx_Contacts on user.ContactKey equals contact.ContactKey
                               where user.LoginName == _securityContext.PrincipalUserName
                               select new UserListItem { FullName = contact.FullName, IsPrincipal = true, UserKey = user.UserKey,
                                    IsSelected = user.UserKey == _securityContext.UserKey,
                                   Username = _securityContext.PrincipalUserName }).FirstOrDefault();

            if (principaUser == null) return Enumerable.Empty<UserListItem>();

            var users = (from userDelegate in _esxDbContext.dESx_UserDelegates
                join user in _esxDbContext.dESx_Users on userDelegate.DelegateUserKey equals user.UserKey
                join contact in _esxDbContext.dESx_Contacts on user.ContactKey equals contact.ContactKey
                where userDelegate.UserKey == principaUser.UserKey
                select new UserListItem
                {
                    UserKey = user.UserKey,
                    IsSelected = user.UserKey == _securityContext.UserKey,
                    Username = user.LoginName,
                    FullName = contact.FullName
                }).ToList();

            users.Add(principaUser);
            users = users.OrderBy("IsPrincipal DESC, FullName").ToList();
            return users;
        }

        public User GetCurrentUser()
        {
            var me = from user in _esxDbContext.dESx_Users
                     join contact in _esxDbContext.dESx_Contacts on user.ContactKey equals contact.ContactKey
                     where user.UserKey == _securityContext.UserKey
                        select new User
                        {
                            UserKey = user.UserKey,
                            ContactKey = contact.ContactKey,
                            FullName = contact.FullName
                        };

            return me.FirstOrDefault();
        }
    }
}

﻿using System;
using System.Linq;
using System.Data.Entity.Migrations;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Alert;
using Esx.Contracts.Paging;
using Esx.Contracts.Security;
using Esx.Data;

namespace Esx.Business.Alerts
{
    public interface IAlertsService
    {
        PagedResources<Alert> GetAlerts(string companyMentionId, PagingData pagingData);

        void AcknowledgeAlert(int alertId);

        void RemoveAlert(int alertId);

        void SnoozeAlert(int alertId);
    }

    public class AlertsService : IAlertsService
    {
        private readonly IEsxDbContext _dbContext;
        private readonly ISecurityContext _securityContext;

        public AlertsService(
            IEsxDbContext dbContext, 
            ISecurityContext securityContext)
        {
            _dbContext = dbContext;
            _securityContext = securityContext;
        }

        public PagedResources<Alert> GetAlerts(string mentionId, PagingData pagingData)
        {            
            var userKey = _securityContext.UserKey;

            var alerts = from n in _dbContext.dESx_Notices
                         join m in _dbContext.dESx_Mentions on n.SubjectMentionKey equals m.MentionKey
                         from nr in
                             _dbContext.dESx_NoticeRecipients.Where(
                                 nr =>
                                     nr.NoticeKey == n.NoticeKey && nr.UserKey == userKey &&
                                     (nr.IsAcknowledged || nr.SnoozeUntilTime > DateTime.UtcNow))                                 
                                 .DefaultIfEmpty()
                         where nr == null
                         select new Alert
                         {
                             AlertId = n.NoticeKey,
                             Title = n.NoticeText,
                             Date = n.CreatedDate,
                             EntityKey = n.SubjectMentionKey,
                             EntityMentionId = m.MentionId,
                             EntityCode = m.ReferencedTableCode
                         };

            pagingData.TotalCount = alerts.Count();

            if (pagingData.Offset > 0) alerts = alerts.Skip(pagingData.Offset);
            if (pagingData.PageSize > 0) alerts = alerts.Take(pagingData.PageSize);

            return new PagedResources<Alert>
            {
                PagingData = pagingData,
                Resources = alerts.ToList()
            };
        }

        public void AcknowledgeAlert(int alertId)
        {            
            var userKey = _securityContext.UserKey;

            var noticeRecipient =
                _dbContext.dESx_NoticeRecipients.FirstOrDefault(x => x.NoticeKey == alertId && x.UserKey == userKey) ??
                new dESx_NoticeRecipient { NoticeKey = alertId, UserKey = userKey };

            noticeRecipient.IsAcknowledged = true;

            _dbContext.dESx_NoticeRecipients.AddOrUpdate(noticeRecipient);
            _dbContext.SaveChanges();
        }

        public void RemoveAlert(int alertId)
        {
            var userKey = _securityContext.UserKey;

            var noticeRecipient =
                _dbContext.dESx_NoticeRecipients.FirstOrDefault(x => x.NoticeKey == alertId && x.UserKey == userKey) ??
                new dESx_NoticeRecipient { NoticeKey = alertId, UserKey = userKey };

            //noticeRecipient.IsDismissed = true;

            _dbContext.dESx_NoticeRecipients.AddOrUpdate(noticeRecipient);
            _dbContext.SaveChanges();
        }

        public void SnoozeAlert(int alertId)
        {            
            var userKey = _securityContext.UserKey;

            var noticeRecipient =
                _dbContext.dESx_NoticeRecipients.FirstOrDefault(x => x.NoticeKey == alertId && x.UserKey == userKey) ??
                new dESx_NoticeRecipient { NoticeKey = alertId, UserKey = userKey };

            noticeRecipient.SnoozeUntilTime = DateTime.UtcNow.AddDays(14);

            _dbContext.dESx_NoticeRecipients.AddOrUpdate(noticeRecipient);
            _dbContext.SaveChanges();
        }
    }
}

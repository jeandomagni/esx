using System;
using System.Collections.Generic;
using System.Linq;
using Esx.Contracts.Paging;
using Esx.Data;
using System.Linq.Dynamic;
using Esx.Business.Search;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Contact;
using Esx.Contracts.Models.Search;
using Esx.Data.Repositories;
using System.Data.Entity;

namespace Esx.Business.Contacts
{
    public interface IContactsService
    {
        ContactDetails GetContactById(int customerId);

        ContactDetails GetContactSummary(int contactId, int pitches, int bidActivity, int holdPeriod, int loansMaturing);

        List<ContactListItem> SearchContacts(string searchQuery);

        PagedResources<ContactListItem> Search(ContactSearchCriteria searchCriteria);
    }

    public class ContactsService : IContactsService
    {
        private readonly ICompaniesRepository _companiesRepository;
        private readonly IEsxDbContext _dbContext;
        private readonly IDealCriteriaSetService _dealCriteriaSetService;
        private readonly IContactCriteriaSetService _contactCriteriaSetService;
        private readonly ICompanyCriteriaSetService _companyCriteriaSetService;

        public ContactsService(
                               ICompaniesRepository companiesRepository,
                               IEsxDbContext dbContext,
                               IDealCriteriaSetService dealCriteriaSetService,
                               IContactCriteriaSetService contactCriteriaSetService,
                               ICompanyCriteriaSetService companyCriteriaSetService)
        {
            _companiesRepository = companiesRepository;
            _dbContext = dbContext;
            _dealCriteriaSetService = dealCriteriaSetService;
            _contactCriteriaSetService = contactCriteriaSetService;
            _companyCriteriaSetService = companyCriteriaSetService;
        }

        public List<ContactListItem> SearchContacts(string searchQuery)
        {
            return Search(new ContactSearchCriteria
            {
                SearchText = searchQuery
            }).Resources.ToList();
        }

        public PagedResources<ContactListItem> Search(ContactSearchCriteria searchCriteria)
        {
            if (searchCriteria == null) throw new NullReferenceException(nameof(searchCriteria));

            var contactAddresses = from electronicAddress in _dbContext.dESx_ElectronicAddresses
                                   from contactElectronicAddress in electronicAddress.dESx_ContactElectronicAddresses
                                   select new { contactElectronicAddress.ContactKey, electronicAddress.ElectronicAddressTypeCode, electronicAddress.AddressUsageTypeCode, electronicAddress.Address };

            var contactOffices = from contactOffice in _dbContext.dESx_ContactOffices
                                 join office in _dbContext.dESx_Offices on contactOffice.OfficeKey equals office.OfficeKey
                                 join companyAlias in _dbContext.dESx_CompanyAlias on office.CompanyKey equals companyAlias.CompanyKey
                                 from physicalAddress in _dbContext.dESx_PhysicalAddresses.Where(pa => office.PhysicalAddressKey == pa.PhysicalAddressKey).DefaultIfEmpty()
                                 where contactOffice.IsCurrent
                                 && companyAlias.IsPrimary
                                 select new
                                 {
                                     contactOffice.ContactKey,
                                     companyAlias,
                                     physicalAddress
                                 };

            var contactsList = from contact in _dbContext.dESx_Contacts
                               from contactOffice in contactOffices.Where(co => co.ContactKey == contact.ContactKey).DefaultIfEmpty()
                               join contactMention in _dbContext.dESx_Mentions on contact.ContactKey equals contactMention.ContactKey
                               from watch in _dbContext.dESx_UserMentionDatas.Where(watch => watch.MentionKey == contactMention.MentionKey).DefaultIfEmpty()
                               let email = contactAddresses.FirstOrDefault(email => email.ContactKey == contact.ContactKey && email.ElectronicAddressTypeCode == ElectronicAddressType.EmailAddress)
                               let mobilePhone = contactAddresses.FirstOrDefault(mobilePhone => mobilePhone.ContactKey == contact.ContactKey && mobilePhone.ElectronicAddressTypeCode == ElectronicAddressType.PhoneNumber && mobilePhone.AddressUsageTypeCode == AddressUsageType.Mobile)
                               select new
                               {
                                   // These are only needed for further filtering, that's why they're not in the result object.
                                   CompanyAliasKey = contactOffice != null ? (int?)contactOffice.companyAlias.CompanyAliasKey : null,

                                   // The object to display.
                                   Row = new ContactListItem
                                   {
                                       ContactId = contact.ContactKey,
                                       FullName = contact.FullName,
                                       LastName = contact.LastName,
                                       Title = contact.Title,
                                       CompanyName = contactOffice != null && contactOffice.companyAlias != null ? contactOffice.companyAlias.AliasName : null,
                                       MobilePhone = mobilePhone != null ? mobilePhone.Address : null,
                                       EmailAddress = email != null ? email.Address : null,
                                       Location = contactOffice != null && contactOffice.physicalAddress != null ? contactOffice.physicalAddress.City + ", " + contactOffice.physicalAddress.StateProvidenceCode : null,
                                       ContactMentionId = contactMention.MentionId,
                                       IsWatched = watch != null && watch.IsWatched
                                   }
                               };

            // If searching for company contacts.
            if (!string.IsNullOrEmpty(searchCriteria.CompanyMentionId))
            {
                contactsList = from contact in contactsList
                               join companyMention in _dbContext.dESx_Mentions on contact.CompanyAliasKey equals companyMention.AliasKey
                               where companyMention.MentionId == searchCriteria.CompanyMentionId
                               select contact;
            }

            // If searching contacts using filters
            if (searchCriteria.ContactCriteriaSet != null)
            {
                var contactKeys = _contactCriteriaSetService.MatchingContactKeys(_dbContext, searchCriteria.ContactCriteriaSet);
                contactsList = contactsList.Where(c => contactKeys.Any(key => c.Row.ContactId == key));
            }

            // If searching contacts by company
            if (searchCriteria.CompanyCriteriaSet != null)
            {
                var companyKeys = _companyCriteriaSetService.MatchingCompanyKeys(_dbContext, searchCriteria.CompanyCriteriaSet);
                var companyContacts = _dbContext.dESx_ContactOffices.Where(co => companyKeys.Any(key => key == co.dESx_Office.CompanyKey));
                contactsList = contactsList.Where(c => companyContacts.Any(cc => cc.ContactKey == c.Row.ContactId));
            }

            // If searching contacts by deal.
            if (searchCriteria.DealCriteriaSet != null)
            {
                var dealKeys = _dealCriteriaSetService.MatchingDealKeys(_dbContext, searchCriteria.DealCriteriaSet);
                var contactDeals = _dbContext.dESx_DealContacts.Where(x => dealKeys.Any(key => key == x.DealKey));
                contactsList = contactsList.Where(c => contactDeals.Any(cd => cd.ContactKey == c.Row.ContactId));
            }

            // Add search criteria if specified.
            if (!string.IsNullOrWhiteSpace(searchCriteria.SearchText) && searchCriteria.SearchText.Length > 2)
            {
                contactsList = from contact in contactsList
                               where (contact.Row.FullName != null && contact.Row.FullName.Contains(searchCriteria.SearchText))
                                 || (contact.Row.LastName != null && contact.Row.LastName.Contains(searchCriteria.SearchText))
                                 || (contact.Row.Title != null && contact.Row.Title.Contains(searchCriteria.SearchText))
                                 || (contact.Row.CompanyName != null && contact.Row.CompanyName.Contains(searchCriteria.SearchText))
                                 || (contact.Row.MobilePhone != null && contact.Row.MobilePhone.Contains(searchCriteria.SearchText))
                                 || (contact.Row.EmailAddress != null && contact.Row.EmailAddress.Contains(searchCriteria.SearchText))
                                 || (contact.Row.Location != null && contact.Row.Location.Contains(searchCriteria.SearchText))
                               select contact;
            }

            if (string.IsNullOrEmpty(searchCriteria.Sort))
            {
                searchCriteria.Sort = "IsWatched";
                searchCriteria.SortDescending = true;
            }
            var sortDirection = searchCriteria.SortDescending ? "descending" : "ascending";
            var sort = $"Row.{searchCriteria.Sort} {sortDirection}, Row.LastName ascending";

            searchCriteria.TotalCount = contactsList.Count();
            contactsList = contactsList.OrderBy(sort).Skip(searchCriteria.Offset).Take(searchCriteria.PageSize);

            return new PagedResources<ContactListItem>
            {
                PagingData = searchCriteria,
                Resources = contactsList.Select(x => x.Row).AsNoTracking().ToList()
            };
        }

        public ContactDetails GetContactById(int contactId)
        {
            var contactAddresses = from electronicAddress in _dbContext.dESx_ElectronicAddresses
                                   from contactElectronicAddress in electronicAddress.dESx_ContactElectronicAddresses
                                   select new { contactElectronicAddress.ContactKey, electronicAddress.ElectronicAddressTypeCode, electronicAddress.AddressUsageTypeCode, electronicAddress.Address };

            var contactDetails = (from c in _dbContext.dESx_Contacts
                                  join co in _dbContext.dESx_ContactOffices on c.ContactKey equals co.ContactKey
                                  join o in _dbContext.dESx_Offices on co.OfficeKey equals o.OfficeKey
                                  join a in _dbContext.dESx_CompanyAlias on o.CompanyKey equals a.CompanyKey
                                  join cnm in _dbContext.dESx_Mentions on c.ContactKey equals cnm.ContactKey
                                  join cm in _dbContext.dESx_Mentions on a.CompanyAliasKey equals cm.AliasKey
                                  from pa in _dbContext.dESx_PhysicalAddresses.Where(pa => pa.PhysicalAddressKey == o.PhysicalAddressKey).DefaultIfEmpty()
                                  let email = contactAddresses.FirstOrDefault(email => email.ContactKey == c.ContactKey && email.ElectronicAddressTypeCode == ElectronicAddressType.EmailAddress)
                                  let mobilePhone = contactAddresses.FirstOrDefault(mobilePhone => mobilePhone.ContactKey == c.ContactKey &&
                                  mobilePhone.ElectronicAddressTypeCode == ElectronicAddressType.PhoneNumber &&
                                  mobilePhone.AddressUsageTypeCode == AddressUsageType.Mobile)
                                  let officePhone = contactAddresses.FirstOrDefault(officePhone => officePhone.ContactKey == c.ContactKey &&
                                  officePhone.ElectronicAddressTypeCode == ElectronicAddressType.PhoneNumber &&
                                  officePhone.AddressUsageTypeCode == AddressUsageType.Main)
                                  let website = contactAddresses.FirstOrDefault(website => website.ContactKey == c.ContactKey &&
                                  website.ElectronicAddressTypeCode == ElectronicAddressType.Website &&
                                  website.AddressUsageTypeCode == AddressUsageType.Main)
                                  let linkedInProfile = contactAddresses.FirstOrDefault(linkedIn => linkedIn.ContactKey == c.ContactKey &&
                                  linkedIn.ElectronicAddressTypeCode == ElectronicAddressType.LinkedInProfile &&
                                  linkedIn.AddressUsageTypeCode == AddressUsageType.Main)

                                  where co.IsCurrent && a.IsPrimary && c.ContactKey == contactId

                                  select new ContactDetails
                                  {
                                      FullName = c.FullName,
                                      ContactId = c.ContactKey,
                                      ContactMentionId = cnm.MentionId,
                                      Title = c.Title,
                                      CompanyKey = a.CompanyKey,
                                      CompanyName = a.AliasName,
                                      CompanyMentionId = cm.MentionId,
                                      IsDefunct = c.IsDefunct,
                                      DefunctReason = c.DefunctReason,
                                      MobilePhone = mobilePhone != null ? mobilePhone.Address : null,
                                      OfficePhone = officePhone != null ? officePhone.Address : null,
                                      EmailAddress = email != null ? email.Address : null,
                                      Address = pa != null ? pa.Address1 : null,
                                      Address2 = pa != null ? pa.Address2 : null,
                                      City = pa != null ? pa.City : null,
                                      State = pa != null ? pa.StateProvidenceCode : null,
                                      Website = website != null ? website.Address : null,
                                      LinkedInProfileLink = linkedInProfile != null ? linkedInProfile.Address : null
                                  }).Single();
            return contactDetails;
        }

        public ContactDetails GetContactSummary(int contactId, int pitches, int bidActivity,
            int holdPeriod, int loansMaturing)
        {
            var contactDetails = GetContactById(contactId);
            var contactSummaryRollup = _companiesRepository.GetCompanySummaryStats(contactDetails.CompanyMentionId, pitches, bidActivity, holdPeriod, loansMaturing);

            return new ContactDetails
            {
                ContactId = contactId,
                CompanyKey = contactDetails.CompanyKey,
                CompanyMentionId = contactDetails.CompanyMentionId,
                FullName = contactDetails.FullName,
                CompanyName = contactDetails.CompanyName,
                OfficePhone = contactDetails.OfficePhone,
                EmailAddress = contactDetails.EmailAddress,
                ContactMentionId = contactDetails.ContactMentionId,
                SummaryRollup = contactSummaryRollup
            };
        }

    }

}


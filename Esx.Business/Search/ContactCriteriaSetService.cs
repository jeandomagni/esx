using System;
using System.Linq;
using Esx.Contracts.Models.Search;
using Esx.Data;

namespace Esx.Business.Search
{
    public interface IContactCriteriaSetService
    {
        IQueryable<int> MatchingContactKeys(IEsxDbContext dbContext, ContactCriteriaSet contactCriteriaSet);
    }

    public class ContactCriteriaSetService : IContactCriteriaSetService
    {
        public IQueryable<int> MatchingContactKeys(IEsxDbContext dbContext, ContactCriteriaSet contactCriteriaSet)
        {
            if (contactCriteriaSet == null) throw new ArgumentNullException(nameof(contactCriteriaSet));

            var contacts = dbContext.dESx_Contacts.AsQueryable();

            if ((contactCriteriaSet.ContactNames?.Any()).GetValueOrDefault())
                contacts = contacts.Where(c => contactCriteriaSet.ContactNames.Any(name => c.FullName.Contains(name)));

            if ((contactCriteriaSet.ContactTitles?.Any()).GetValueOrDefault())
                contacts = contacts.Where(c => contactCriteriaSet.ContactTitles.Any(name => c.Title.Contains(name)));

            if ((contactCriteriaSet.EastdilOffices?.Any()).GetValueOrDefault())
                contacts = contacts.Where(c => contactCriteriaSet.EastdilOffices.Any(officeKey => officeKey == c.EastdilOfficeKey));

            return contacts.Select(c => c.ContactKey).Distinct();
        }
    }
}
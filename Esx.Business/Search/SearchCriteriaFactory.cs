﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Search;

namespace Esx.Business.Search
{
    public interface ISearchCriteriaFactory
    {
        CompanySearchCriteria GetCompanySearchCriteria(SearchCriteria searchCriteria);

        ContactSearchCriteria GetContactSearchCriteria(SearchCriteria searchCriteria);

        DealSearchCriteria GetDealSearchCriteria(SearchCriteria searchCriteria);

        LoanSearchCriteria GetLoanSearchCriteria(SearchCriteria searchCriteria);

        PropertySearchCriteria GetPropertySearchCriteria(SearchCriteria searchCriteria);
    }

    public class SearchCriteriaFactory : ISearchCriteriaFactory
    {
        #region Get SearchCriteria objects

        public CompanySearchCriteria GetCompanySearchCriteria(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null) throw new NullReferenceException(nameof(searchCriteria));

            return new CompanySearchCriteria
            {
                CompanyCriteriaSet = GetCompanyCriteriaSet(searchCriteria),
                DealCriteriaSet = GetDealCriteriaSet(searchCriteria),
                LoanCriteriaSet = GetLoanCriteriaSet(searchCriteria),
                PropertyCriteriaSet = GetPropertyCriteriaSet(searchCriteria)
            };
        }

        public ContactSearchCriteria GetContactSearchCriteria(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null) throw new NullReferenceException(nameof(searchCriteria));

            return new ContactSearchCriteria
            {
                CompanyCriteriaSet = GetCompanyCriteriaSet(searchCriteria),
                ContactCriteriaSet = GetContactCriteriaSet(searchCriteria),
                DealCriteriaSet = GetDealCriteriaSet(searchCriteria)
            };
        }

        public DealSearchCriteria GetDealSearchCriteria(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null) throw new NullReferenceException(nameof(searchCriteria));

            return new DealSearchCriteria
            {
                CompanyCriteriaSet = GetCompanyCriteriaSet(searchCriteria),
                DealCriteriaSet = GetDealCriteriaSet(searchCriteria),
                LoanCriteriaSet = GetLoanCriteriaSet(searchCriteria),
                PropertyCriteriaSet = GetPropertyCriteriaSet(searchCriteria)
            };
        }

        public LoanSearchCriteria GetLoanSearchCriteria(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null) throw new NullReferenceException(nameof(searchCriteria));

            return new LoanSearchCriteria
            {
                CompanyCriteriaSet = GetCompanyCriteriaSet(searchCriteria),
                DealCriteriaSet = GetDealCriteriaSet(searchCriteria),
                LoanCriteriaSet = GetLoanCriteriaSet(searchCriteria),
                PropertyCriteriaSet = GetPropertyCriteriaSet(searchCriteria)
            };
        }

        public PropertySearchCriteria GetPropertySearchCriteria(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null) throw new NullReferenceException(nameof(searchCriteria));

            return new PropertySearchCriteria
            {
                CompanyCriteriaSet = GetCompanyCriteriaSet(searchCriteria),
                DealCriteriaSet = GetDealCriteriaSet(searchCriteria),
                LoanCriteriaSet = GetLoanCriteriaSet(searchCriteria),
                PropertyCriteriaSet = GetPropertyCriteriaSet(searchCriteria),
                PropertyLocationCriteriaSet = GetPropertyLocationCriteriaSet(searchCriteria)
            };

        }

        #endregion

        #region Get CriteriaSet objects

        private static CompanyCriteriaSet GetCompanyCriteriaSet(SearchCriteria searchCriteria)
        {
            var set = new CompanyCriteriaSet
            {
                CompanyNames = GetValues(searchCriteria, FilterName.Company.CompanyName),
                CompanyDealRoles = GetValues(searchCriteria, FilterName.Company.CompanyDealRole),
                PropertyLocations = GetGeographies(searchCriteria, FilterName.Company.CompanyPropertyLocation),
                PropertyTypes = GetValues(searchCriteria, FilterName.Company.CompanyPropertyType),
                PropertyRiskProfiles = GetValues(searchCriteria, FilterName.Company.CompanyPropertyRiskProfile)
            };
            return set.HasValue ? set : null;
        }

        private static ContactCriteriaSet GetContactCriteriaSet(SearchCriteria searchCriteria)
        {
            var set = new ContactCriteriaSet
            {
                ContactNames = GetValues(searchCriteria, FilterName.Contact.ContactName),
                ContactTitles = GetValues(searchCriteria, FilterName.Contact.ContactTitle),
                ContactDealRoles = GetValues(searchCriteria, FilterName.Contact.ContactDealRole),
                EastdilOffices = GetInts(searchCriteria, FilterName.Contact.EastdilOffice)
            };
            return set.HasValue ? set : null;
        }

        private static DealCriteriaSet GetDealCriteriaSet(SearchCriteria searchCriteria)
        {
            var set = new DealCriteriaSet
            {
                DealTypes = GetValues(searchCriteria, FilterName.Deal.DealType),
                StructureTypes = GetValues(searchCriteria, FilterName.Deal.StructureType),
                LastCloseDates = GetDateRanges(searchCriteria, FilterName.Deal.LastCloseDate),
                BidStatuses = GetValues(searchCriteria, FilterName.Deal.BidStatus),
                BidDates = GetDateRanges(searchCriteria, FilterName.Deal.BidDate),
                Fees = GetNumericRanges(searchCriteria, FilterName.Deal.Fees),
                DealStatuses = GetValues(searchCriteria, FilterName.Deal.DealStatus),
                TransactionAmounts = GetNumericRanges(searchCriteria, FilterName.Deal.TransactionAmount),
                Valuations = GetNumericRanges(searchCriteria, FilterName.Deal.ValuationAmount),
                BidderCounts = GetNumericRanges(searchCriteria, FilterName.Deal.BidderCount),
                DealsIncluded = GetValues(searchCriteria, FilterName.Deal.DealsIncluded),
                DealNames = GetValues(searchCriteria, FilterName.Deal.DealName),
                PropertyNames = GetValues(searchCriteria, FilterName.Deal.DealPropertyName),
                PropertyLocations = GetGeographies(searchCriteria, FilterName.Deal.DealPropertyLocation),
                PropertyTypes = GetInts(searchCriteria, FilterName.Deal.DealPropertyType),
                PropertyRiskProfiles = GetValues(searchCriteria, FilterName.Deal.DealPropertyRiskProfile),
                ContactNames = GetValues(searchCriteria, FilterName.Deal.DealContactName)
            };
            return set.HasValue ? set : null;
        }

        private static LoanCriteriaSet GetLoanCriteriaSet(SearchCriteria searchCriteria)
        {
            var set = new LoanCriteriaSet
            {
                DebtYields = GetNumericRanges(searchCriteria, FilterName.Loan.DebtYield),
                FinalMaturityDates = GetDateRanges(searchCriteria, FilterName.Loan.FinalMaturityDate),
                InterestRates = GetNumericRanges(searchCriteria, FilterName.Loan.InterestRate),
                LoanAmounts = GetNumericRanges(searchCriteria, FilterName.Loan.LoanAmount),
                LoanStatusDates = GetDateRanges(searchCriteria, FilterName.Loan.LoanStatusDate),
                LockoutDates = GetDateRanges(searchCriteria, FilterName.Loan.LockoutDate),
                Ltvs = GetNumericRanges(searchCriteria, FilterName.Loan.LTV),
                NextMaturityDates = GetDateRanges(searchCriteria, FilterName.Loan.NextMaturityDate),
                OriginationDates = GetDateRanges(searchCriteria, FilterName.Loan.OriginationDate),
                RateTypes = GetValues(searchCriteria, FilterName.Loan.RateType),
                YieldMaintenanceEndDates = GetDateRanges(searchCriteria, FilterName.Loan.YieldMaintenanceEndDate),
                PropertyLocations = GetGeographies(searchCriteria, FilterName.Loan.LoanPropertyLocation),
                PropertyTypes = GetInts(searchCriteria, FilterName.Loan.LoanPropertyType)
            };
            return set.HasValue ? set : null;
        }

        private static PropertyCriteriaSet GetPropertyCriteriaSet(SearchCriteria searchCriteria)
        {
            var set = new PropertyCriteriaSet
            {
                PropertyKeys = GetInts(searchCriteria, FilterName.Property.PropertyKey),
                PropertyNames = GetValues(searchCriteria, FilterName.Property.PropertyName),
                PropertySizes = GetNumericRanges(searchCriteria, FilterName.Property.PropertySize),
                PropertyTypes = GetInts(searchCriteria, FilterName.Property.PropertyType),
                PropertyLocations = GetGeographies(searchCriteria, FilterName.Property.PropertyLocation)
            };
            return set.HasValue ? set : null;
        }

        private static PropertyLocationCriteriaSet GetPropertyLocationCriteriaSet(SearchCriteria searchCriteria)
        {
            var set = new PropertyLocationCriteriaSet
            {
                PropertyRegions = GetInts(searchCriteria, FilterName.PropertyLocation.PropertyRegion),
                PropertyMapView = GetValues(searchCriteria, FilterName.PropertyLocation.PropertyMapView),
                PropertyCities = GetValues(searchCriteria, FilterName.PropertyLocation.PropertyCity),
                PropertyCountries = GetValues(searchCriteria, FilterName.PropertyLocation.PropertyCountry),
                PropertyContinents = GetValues(searchCriteria, FilterName.PropertyLocation.PropertyContinent)
            };
            return set.HasValue ? set : null;
        }

        #endregion

        #region Get Filter Value helpers

        private static IEnumerable<string> GetValues(SearchCriteria searchCriteria, string filterName)
        {
            return (from filter in searchCriteria.Filters
                    where filter.Name == filterName
                    from value in filter.Values
                    select value.Value).Distinct().ToList();
        }

        private static IEnumerable<int> GetInts(SearchCriteria searchCriteria, string filterName)
        {
            return GetValues(searchCriteria, filterName).Select(x => Convert.ToInt32(x));
        }

        private static IEnumerable<bool> GetBooleans(SearchCriteria searchCriteria, string filterName)
        {
            return GetValues(searchCriteria, filterName).Select(Convert.ToBoolean);
        }

        private static IEnumerable<DbGeography> GetGeographies(SearchCriteria searchCriteria, string filterName)
        {
            return GetValues(searchCriteria, filterName).Select(x => DbGeography.FromText(x));
        }

        private static IEnumerable<Range<DateTime>> GetDateRanges(SearchCriteria searchCriteria, string filterName)
        {
            foreach (var filterItem in searchCriteria.Filters.Where(x => x.Name == filterName))
            {
                DateTime d1;
                DateTime d2;
                var startDate = filterItem.Values.Count > 0 && DateTime.TryParse(filterItem.Values[0]?.Value, out d1) ? (DateTime?)d1 : null;
                var endDate = filterItem.Values.Count > 1 && DateTime.TryParse(filterItem.Values[1]?.Value, out d2) ? (DateTime?)d2 : null;

                // If the user put the dates in backwards, flip them.
                if (startDate.HasValue && endDate.HasValue && startDate > endDate)
                {
                    var temp = startDate;
                    startDate = endDate;
                    endDate = temp;
                }

                if (startDate.HasValue || endDate.HasValue)
                {
                    yield return new Range<DateTime> { Start = startDate, End = endDate };
                }
            }
        }

        private static IEnumerable<Range<decimal>> GetNumericRanges(SearchCriteria searchCriteria, string filterName)
        {
            foreach (var filterItem in searchCriteria.Filters.Where(x => x.Name == filterName))
            {
                decimal n1;
                decimal n2;
                var startNum = filterItem.Values.Count > 0 && decimal.TryParse(filterItem.Values[0]?.Value, out n1) ? (decimal?)n1 : null;
                var endNum = filterItem.Values.Count > 1 && decimal.TryParse(filterItem.Values[1]?.Value, out n2) ? (decimal?)n2 : null;

                // If the user put the numbers in backwards, flip them.
                if (startNum.HasValue && endNum.HasValue && startNum > endNum)
                {
                    var temp = startNum;
                    startNum = endNum;
                    endNum = temp;
                }

                if (startNum.HasValue || endNum.HasValue)
                {
                    yield return new Range<decimal> { Start = startNum, End = endNum };
                }
            }
        }
        #endregion
    }
}

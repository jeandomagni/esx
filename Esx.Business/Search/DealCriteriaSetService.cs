﻿using System;
using System.Linq;
using Esx.Contracts.Enums;
using Esx.Contracts.Extensions;
using Esx.Contracts.Models.Search;
using Esx.Data;

namespace Esx.Business.Search
{
    public interface IDealCriteriaSetService
    {
        IQueryable<int> MatchingDealKeys(IEsxDbContext dbContext, DealCriteriaSet dealCriteriaSet);
    }

    public class DealCriteriaSetService : IDealCriteriaSetService
    {
        public IQueryable<int> MatchingDealKeys(IEsxDbContext dbContext, DealCriteriaSet dealCriteriaSet)
        {
            if (dealCriteriaSet == null) throw new ArgumentNullException(nameof(dealCriteriaSet));

            var c = dealCriteriaSet;

            // Start with all deals and then reduce with each filter.
            var deals = from deal in dbContext.dESx_Deals
                        join dealStatus in dbContext.dESx_DealStatusLogs on deal.DealKey equals dealStatus.DealKey
                        where dealStatus.IsCurrent
                        select new
                        {
                            Deal = deal,
                            DealStatus = dealStatus,
                            LastClosedDate =
                                dbContext.dESx_DealStatusLogs.Where(
                                    x => x.DealKey == deal.DealKey && x.DealStatusCode == DealStatusCode.Closed)
                                    .OrderByDescending(x => x.CreatedDate)
                                    .Select(x => x.CreatedDate)
                                    .FirstOrDefault()
                        };

            if ((c.DealNames?.Any()).GetValueOrDefault())
                deals = deals.Where(x => c.DealNames.Any(name => x.Deal.DealName.Contains(name)));

            if ((c.DealTypes?.Any()).GetValueOrDefault())
                deals = deals.Where(x => c.DealTypes.Any(dt => x.Deal.DealTypeCode == dt));

            if ((c.DealStatuses?.Any()).GetValueOrDefault())
                deals = deals.Where(x => c.DealStatuses.Any(ds => x.DealStatus.DealStatusCode == ds));

            if ((c.Fees?.Any()).GetValueOrDefault())
                deals = deals.WhereIn(deal => deal.Deal.DealFee, c.Fees);

            if ((c.LastCloseDates?.Any()).GetValueOrDefault())
                deals = deals.WhereIn(deal => deal.LastClosedDate, c.LastCloseDates);

            if ((c.TransactionAmounts?.Any()).GetValueOrDefault())
                deals = deals.WhereIn(deal => deal.Deal.FinalTransactionAmount, c.TransactionAmounts);

            if (c.DealsIncluded.Count() == 1 && c.DealsIncluded.First() == "recent") // Because all deals are included by default, it only makes sense to apply this filter if "recent" is the only option selected.
                deals =
                    deals.Where(
                        deal =>
                            deal.DealStatus.DealStatusCode != DealStatus.Closed.Code ||
                            deal.LastClosedDate == deal.DealStatus.CreatedDate);


            // If any bid filters are specified, join to the bids table.
            if ((c.BidDates?.Any()).GetValueOrDefault() || (c.BidderCounts?.Any()).GetValueOrDefault() || (c.BidStatuses?.Any()).GetValueOrDefault())
            {
                var dealBids = from deal in deals
                               join bid in dbContext.dESx_DealValuations on deal.Deal.DealKey equals bid.DealKey
                               where bid.DealValuationTypeCode == DealValuationType.Bid.Code
                               let bidderCount = dbContext.dESx_DealValuationCompanies
                                                    .Where(x => x.dESx_DealValuation.DealKey == deal.Deal.DealKey)
                                                    .Select(x => x.CompanyKey)
                                                    .Distinct()
                                                    .Count()
                               select new
                               {
                                   deal,
                                   bid,
                                   bidderCount
                               };

                if ((c.BidDates?.Any()).GetValueOrDefault())
                    dealBids = dealBids.WhereIn(dealBid => dealBid.bid.BidReceivedDate, c.BidDates);

                if ((c.BidderCounts?.Any()).GetValueOrDefault())
                    dealBids = dealBids.WhereIn(dealBid => dealBid.bidderCount, c.BidderCounts);

                if ((c.BidStatuses?.Any()).GetValueOrDefault())
                    dealBids = dealBids.Where(dealBid => dealBid.bid != null && c.BidStatuses.Any(bs => dealBid.bid.BidStatusCode == bs));

                deals = deals.Where(deal => dealBids.Any(b => b.deal.Deal.DealKey == deal.Deal.DealKey));
            }

            if ((c.Valuations?.Any()).GetValueOrDefault())
            {
                var dealValuations = from deal in deals
                                     join dealValue in dbContext.dESx_DealValues on deal.Deal.DealKey equals dealValue.DealKey
                                     select new { deal, dealValue.Value };
                deals = dealValuations.WhereIn(dv => dv.Value, c.Valuations).Select(dv => dv.deal);
            }

            // If any property filters are specified, join to the asset table.
            if ((c.PropertyLocations?.Any()).GetValueOrDefault() || (c.PropertyNames?.Any()).GetValueOrDefault() || (c.PropertyTypes?.Any()).GetValueOrDefault() || (c.PropertyRiskProfiles?.Any()).GetValueOrDefault())
            {
                var dealProperties = from deal in deals
                                     join dealValue in dbContext.dESx_DealValues on deal.Deal.DealKey equals dealValue.DealKey
                                     from dealValuation in dbContext.dESx_DealValuations.Where(x=> x.DealKey == dealValue.DealKey && x.DealValuationKey == dealValue.DealValuationKey).DefaultIfEmpty()
                                     join dealAsset in dbContext.dESx_DealAssets on deal.Deal.DealKey equals dealAsset.DealKey
                                     join asset in dbContext.dESx_Assets on dealAsset.AssetKey equals asset.AssetKey
                                     join assetName in dbContext.dESx_AssetNames on asset.AssetKey equals assetName.AssetKey
                                     from assetUsage in dbContext.dESx_AssetUsages.Where(x => x.AssetKey == asset.AssetKey && x.IsCurrent).DefaultIfEmpty()
                                     where asset.AssetTypeCode == AssetType.Property.Code
                                     && assetName.IsCurrent
                                     select new
                                     {
                                         deal,
                                         asset,
                                         assetName,
                                         assetUsage,
                                         dealValuation
                                     };

                if ((c.PropertyLocations?.Any()).GetValueOrDefault())
                    throw new NotSupportedException("Support for property locations has not been added yet.");

                if ((c.PropertyNames?.Any()).GetValueOrDefault())
                    dealProperties = dealProperties.Where(dp => c.PropertyNames.Any(name => dp.assetName.AssetName.Contains(name)));

                if ((c.PropertyTypes?.Any()).GetValueOrDefault())
                    dealProperties = dealProperties.Where(dp => c.PropertyTypes.Any(type => type == dp.assetUsage.AssetUsageTypeKey));

                if ((c.PropertyRiskProfiles?.Any()).GetValueOrDefault())
                    dealProperties = dealProperties.Where(dp => dp.dealValuation != null && c.PropertyRiskProfiles.Any(risk => risk == dp.dealValuation.DealValuationRiskProfileCode));

                deals = dealProperties.Select(dp => dp.deal).Distinct();
            }

            if ((c.ContactNames?.Any()).GetValueOrDefault())
            {
                deals = (from deal in deals
                         join dealContact in dbContext.dESx_DealContacts on deal.Deal.DealKey equals dealContact.DealKey
                         join contact in dbContext.dESx_Contacts on dealContact.ContactKey equals contact.ContactKey
                         where c.ContactNames.Any(cn => contact.FullName.Contains(cn)
                                                    || contact.LastName.Contains(cn)
                                                    || contact.FirstName.Contains(cn))
                         select deal).Distinct();
            }

            if (c.StructureTypes?.Count() == 1) // Because deals are either JV or they're not, it only makes sense to apply this filter if 1 of the 2 filters are selected.
            {
                if (c.StructureTypes.Any(x => x == "JV"))
                {
                    deals = from companyDeal in deals
                            where
                                companyDeal.Deal.dESx_DealCompanies.Count(company => company.DealParticipationTypeCode == DealParticipationType.Buyer.Code) > 1
                                || companyDeal.Deal.dESx_DealCompanies.Count(company => company.DealParticipationTypeCode == DealParticipationType.Seller.Code) > 1
                                || companyDeal.Deal.DealTypeCode == DealType.EquitySaleLessThan100.Code
                            select companyDeal;
                }
                else
                {
                    deals = from companyDeal in deals
                            where
                                companyDeal.Deal.dESx_DealCompanies.Count(company => company.DealParticipationTypeCode == DealParticipationType.Buyer.Code) <= 1
                                && companyDeal.Deal.dESx_DealCompanies.Count(company => company.DealParticipationTypeCode == DealParticipationType.Seller.Code) <= 1
                                && companyDeal.Deal.DealTypeCode != DealType.EquitySaleLessThan100.Code
                            select companyDeal;
                }
            }

            return deals.Select(d => d.Deal.DealKey).Distinct();
        }
    }
}

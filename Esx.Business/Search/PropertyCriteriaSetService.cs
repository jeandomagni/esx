﻿using System;
using System.Linq;
using Esx.Contracts.Extensions;
using Esx.Contracts.Models.Search;
using Esx.Data;

namespace Esx.Business.Search
{
    public interface IPropertyCriteriaSetService
    {
        IQueryable<int> MatchingPropertyAssetKeys(IEsxDbContext dbContext, PropertyCriteriaSet propertyCriteriaSet);
    }

    public class PropertyCriteriaSetService : IPropertyCriteriaSetService
    {
        public IQueryable<int> MatchingPropertyAssetKeys(IEsxDbContext dbContext, PropertyCriteriaSet propertyCriteriaSet)
        {
            var c = propertyCriteriaSet;

            // Start with all properties and then reduce with each filter.
            var query = from asset in dbContext.dESx_Assets
                        join assetName in dbContext.dESx_AssetNames on asset.AssetKey equals assetName.AssetKey
                        from assetUsage in dbContext.dESx_AssetUsages.Where(x => asset.AssetKey == x.AssetKey && x.IsCurrent).DefaultIfEmpty()
                        from assetValue in dbContext.dESx_AssetValues.Where(x => asset.AssetKey == x.AssetKey && x.IsCurrent).DefaultIfEmpty()
                        where assetName.IsCurrent
                        select new
                        {
                            asset,
                            assetUsage,
                            assetValue,
                            assetName
                        };

            if ((c.PropertyKeys?.Any()).GetValueOrDefault())
                query = query.Where(x => c.PropertyKeys.Any(key => x.asset.AssetKey == key));

            if ((c.PropertyNames?.Any()).GetValueOrDefault())
                query = query.Where(x => x.assetName.AssetName != null && c.PropertyNames.Any(name => x.assetName.AssetName.Contains(name)));

            if ((c.PropertySizes?.Any()).GetValueOrDefault())
            {
                query = query.Where(p => p.assetValue != null).WhereIn(p => p.assetValue.Spaces, c.PropertySizes);

                // If any property types are specified, then one of them must be the primary usage
                // because property size is only stored for the primary usage.
                if ((c.PropertyTypes?.Any()).GetValueOrDefault())
                {
                    query =
                        query.Where(
                            p =>
                                p.assetUsage != null &&
                                c.PropertyTypes.Any(pt => pt == p.assetUsage.AssetUsageTypeKey)
                                && p.assetUsage.IsPrimary.Value);
                }
            }

            if ((c.PropertyTypes?.Any()).GetValueOrDefault())
                query = query.Where(x => c.PropertyTypes.Any(pt => pt == x.assetUsage.AssetUsageTypeKey));

            return query.Select(d => d.asset.AssetKey).Distinct();
        }
    }
}

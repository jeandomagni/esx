﻿using System;
using System.Linq;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Search;
using Esx.Data;
using Esx.Contracts.Extensions;

namespace Esx.Business.Search
{
    public interface ILoanCriteriaSetService
    {
        IQueryable<int> MatchingLoanAssetKeys(IEsxDbContext dbContext, LoanCriteriaSet loanCriteriaSet);
    }

    public class LoanCriteriaSetService : ILoanCriteriaSetService
    {
        public IQueryable<int> MatchingLoanAssetKeys(IEsxDbContext dbContext, LoanCriteriaSet loanCriteriaSet)
        {
            if (loanCriteriaSet == null) throw new ArgumentNullException(nameof(loanCriteriaSet));

            var c = loanCriteriaSet;

            // Start with all company loans then reduce with each filter.
            var loans = from asset in dbContext.dESx_Assets
                        join loanTerms in dbContext.dESx_AssetLoanTerms on asset.AssetKey equals loanTerms.AssetKey
                        from loanTermDate in dbContext.dESx_AssetLoanTermDates.Where(x => x.AssetKey == loanTerms.AssetKey).DefaultIfEmpty()
                        where asset.AssetTypeCode == AssetType.Loan.Code
                        select new
                        {
                            asset,
                            loanTerms,
                            loanTermDate
                        };

            if((c.LockoutDates?.Any()).GetValueOrDefault())
            {
                loans = loans.Where(
                    x =>
                        x.loanTermDate != null &&
                        x.loanTermDate.LoanTermDateTypeCode == LoanTermDateType.Lockoutend.Code)
                    .WhereIn(x => x.loanTermDate.LoanTermDate, c.LockoutDates);
            }

            if((c.YieldMaintenanceEndDates?.Any()).GetValueOrDefault())
            {
                loans = loans.Where(
                    x =>
                        x.loanTermDate != null &&
                        x.loanTermDate.LoanTermDateTypeCode == LoanTermDateType.Yieldmaintenanceend.Code)
                    .WhereIn(x => x.loanTermDate.LoanTermDate, c.YieldMaintenanceEndDates);
            }

            if((c.NextMaturityDates?.Any()).GetValueOrDefault())
            {
                loans = loans.Where(
                    x =>
                        x.loanTermDate != null &&
                        x.loanTermDate.LoanTermDateTypeCode == LoanTermDateType.Nextmaturity.Code)
                    .WhereIn(x => x.loanTermDate.LoanTermDate, c.NextMaturityDates);
            }

            if((c.FinalMaturityDates?.Any()).GetValueOrDefault())
            {
                loans = loans.Where(
                        x =>
                            x.loanTermDate != null &&
                            x.loanTermDate.LoanTermDateTypeCode == LoanTermDateType.Finalmaturity.Code)
                        .WhereIn(x => x.loanTermDate.LoanTermDate, c.FinalMaturityDates);
            }

            if((c.OriginationDates?.Any()).GetValueOrDefault())
                loans = loans.WhereIn(loan => loan.loanTerms.OriginationDate, c.OriginationDates);

            if((c.LoanAmounts?.Any()).GetValueOrDefault())
                loans = loans.WhereIn(x => x.loanTerms.LoanAmount, c.LoanAmounts);

            if((c.InterestRates?.Any()).GetValueOrDefault())
                loans = loans.WhereIn(x => x.loanTerms.LoanRate, c.InterestRates);

            if((c.RateTypes?.Any()).GetValueOrDefault())
                loans = loans.Where(x => c.RateTypes.Any(r => x.loanTerms.LoanRateTypeCode == r));

            if((c.Ltvs?.Any()).GetValueOrDefault())
                loans = loans.WhereIn(x => x.loanTerms.LoanToValue, c.Ltvs);

            if((c.DebtYields?.Any()).GetValueOrDefault())
                loans = loans.WhereIn(x => x.loanTerms.DebtYield, c.DebtYields);

            if((c.LoanStatusDates?.Any()).GetValueOrDefault())
            {
                loans = (from loan in loans
                         join dealAsset in dbContext.dESx_DealAssets on loan.asset.AssetKey equals dealAsset.AssetKey
                         join deal in dbContext.dESx_Deals on dealAsset.DealKey equals deal.DealKey
                         join dealStatus in dbContext.dESx_DealStatusLogs on deal.DealKey equals dealStatus.DealKey
                         where dealStatus.IsCurrent
                               && dealStatus.DealStatusCode == DealStatus.Closed.Code
                               && deal.DealTypeCode == DealType.DebtPlacement.Code
                         select new
                         {
                             loan,
                             loanStatusDate = dealStatus.CreatedDate
                         })
                    .WhereIn(x => x.loanStatusDate, c.LoanStatusDates)
                    .Select(x => x.loan);
            }

            // If any property filters, join to the asset table.
            if ((c.PropertyLocations?.Any()).GetValueOrDefault() || (c.PropertyTypes?.Any()).GetValueOrDefault())
            {
                var loanProperties = from loan in loans
                                     join assetLoanProperty in dbContext.dESx_AssetLoanProperties on loan.asset.AssetKey equals
                                         assetLoanProperty.LoanAssetKey
                                     join propertyAsset in dbContext.dESx_Assets on assetLoanProperty.PropertyAssetKey equals
                                         propertyAsset.AssetKey
                                     join assetUsage in dbContext.dESx_AssetUsages on propertyAsset.AssetKey equals assetUsage.AssetKey
                                     select new
                                     {
                                         loan,
                                         assetUsage
                                     };

                if((c.PropertyLocations?.Any()).GetValueOrDefault())
                    throw new NotSupportedException("Support for property locations has not been added yet.");

                if((c.PropertyTypes?.Any()).GetValueOrDefault())
                    loanProperties = loanProperties.Where(lp => c.PropertyTypes.Any(type => type == lp.assetUsage.AssetUsageTypeKey));

                loans = loanProperties.Select(x => x.loan).Distinct();
            }

            return loans.Select(d => d.asset.AssetKey).Distinct();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Esx.Business.Companies;
using Esx.Business.Contacts;
using Esx.Business.Deals;
using Esx.Business.Loans;
using Esx.Business.Properties;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;

namespace Esx.Business.Search
{
    public interface ISearchService
    {
        PagedResources<object> Search(SearchCriteria searchCriteria);
        IEnumerable<MapItem> MapItems(SearchCriteria searchCriteria);
    }

    public class SearchService : ISearchService
    {
        private readonly ISearchCriteriaFactory _searchCriteriaFactory;
        private readonly ICompaniesService _companiesService;
        private readonly IContactsService _contactsService;
        private readonly IPropertiesService _propertiesService;
        private readonly ILoansService _loansService;
        private readonly IDealsService _dealsService;

        public SearchService(
            ISearchCriteriaFactory searchCriteriaFactory,
            ICompaniesService companiesService,
            IContactsService contactsService,
            IPropertiesService propertiesService,
            ILoansService loansService,
            IDealsService dealsService
            )
        {
            _searchCriteriaFactory = searchCriteriaFactory;
            _companiesService = companiesService;
            _contactsService = contactsService;
            _propertiesService = propertiesService;
            _loansService = loansService;
            _dealsService = dealsService;
        }

        public PagedResources<object> Search(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null) throw new ArgumentNullException(nameof(searchCriteria));

            var result = new PagedResources<object>
            {
                PagingData = searchCriteria
            };

            switch (searchCriteria.CoreObjectType)
            {
                case CoreType.Company:
                    result.Resources =
                        _companiesService.Search(_searchCriteriaFactory.GetCompanySearchCriteria(searchCriteria))
                            ?.Resources
                            ?.Cast<object>()
                            .ToList();
                    break;
                case CoreType.Contact:
                    result.Resources =
                        _contactsService.Search(_searchCriteriaFactory.GetContactSearchCriteria(searchCriteria))
                            ?.Resources
                            ?.Cast<object>()
                            .ToList();
                    break;
                case CoreType.Property:
                    result.Resources =
                        _propertiesService.Search(_searchCriteriaFactory.GetPropertySearchCriteria(searchCriteria))
                            ?.Resources
                            ?.Cast<object>()
                            .ToList();
                    break;
                case CoreType.Deal:
                    result.Resources =
                        _dealsService.Search(_searchCriteriaFactory.GetDealSearchCriteria(searchCriteria))
                            ?.Resources
                            ?.Cast<object>()
                            .ToList();
                    break;
                case CoreType.Loan:
                    result.Resources =
                        _loansService.Search(_searchCriteriaFactory.GetLoanSearchCriteria(searchCriteria))
                            ?.Resources
                            ?.Cast<object>()
                            .ToList();
                    break;
                default:
                    throw new NotSupportedException("The core object type is not recognized.");
            }

            return result;
        }
        public IEnumerable<MapItem> MapItems(SearchCriteria searchCriteria)
        {
            return _propertiesService.MapItems(_searchCriteriaFactory.GetPropertySearchCriteria(searchCriteria));
        }
    }
}

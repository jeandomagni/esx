﻿using System;
using System.Linq;
using Esx.Contracts.Models.Search;
using Esx.Data;

namespace Esx.Business.Search
{
    public interface ICompanyCriteriaSetService
    {
        IQueryable<int> MatchingCompanyKeys(IEsxDbContext dbContext, CompanyCriteriaSet companyCriteriaSet);
    }

    public class CompanyCriteriaSetService : ICompanyCriteriaSetService
    {
        public IQueryable<int> MatchingCompanyKeys(IEsxDbContext dbContext, CompanyCriteriaSet companyCriteriaSet)
        {
            if (companyCriteriaSet == null) throw new ArgumentNullException(nameof(companyCriteriaSet));

            // Start with all companies and reduce with each filter.
            var companies = dbContext.dESx_Companies.AsQueryable();

            if ((companyCriteriaSet.CompanyNames?.Any()).GetValueOrDefault())
            {
                // Get all the aliases of all the companies in the same "family".
                var relatedCompanies = (from relatedAlias in dbContext.dESx_CompanyAlias
                                        join rel in dbContext.dESx_CompanyHierarchies on relatedAlias.CompanyKey equals rel.ParentCompanyKey
                                        select new
                                        {
                                            rel.TopParentCompanyKey,
                                            relatedAlias.AliasName
                                        }).Union(
                                            from relatedAlias in dbContext.dESx_CompanyAlias
                                            join rel in dbContext.dESx_CompanyHierarchies on relatedAlias.CompanyKey equals rel.ChildCompanyKey
                                            select new
                                            {
                                                rel.TopParentCompanyKey,
                                                relatedAlias.AliasName
                                            }).Distinct();

                companies = from company in companies
                            join companyAlias in dbContext.dESx_CompanyAlias on company.CompanyKey equals companyAlias.CompanyKey
                            // Get the "family" of this company if one exists
                            let topCompanyKey =
                                  dbContext.dESx_CompanyHierarchies.Where(
                                      rel =>
                                          rel.ParentCompanyKey == company.CompanyKey ||
                                          rel.ChildCompanyKey == company.CompanyKey)
                                      .Select(x => x.TopParentCompanyKey)
                                      .FirstOrDefault()
                            where
                                // Search this company's aliases separately in case it has no parent or child companies.
                                companyCriteriaSet.CompanyNames.Any(name => companyAlias.AliasName.Contains(name))
                                // Search all related company aliases.
                                || relatedCompanies.Any(
                                    related =>
                                        related.TopParentCompanyKey == topCompanyKey &&
                                        // ReSharper disable once ConvertClosureToMethodGroup
                                        companyCriteriaSet.CompanyNames.Any(name => related.AliasName.Contains(name)))
                            select company;
            }

            if ((companyCriteriaSet.CompanyDealRoles?.Any()).GetValueOrDefault())
            {
                companies = from company in companies
                            join dealCompany in dbContext.dESx_DealCompanies on company.CompanyKey equals
                                dealCompany.CompanyKey
                            where companyCriteriaSet.CompanyDealRoles.Any(role => dealCompany.DealParticipationTypeCode == role)
                            select company;
            }

            return companies.Select(c => c.CompanyKey).Distinct();
        }
    }
}

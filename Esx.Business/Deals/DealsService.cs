using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using Esx.Business.Search;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Paging;
using Esx.Data;
using Esx.Contracts.Security;
using Esx.Contracts.Enums;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Models.Property;
using Esx.Contracts.Models.Location;
using Esx.Contracts.Models.Company;

namespace Esx.Business.Deals
{
    public interface IDealsService
    {
        PagedResources<DealListItem> Search(DealSearchCriteria searchCriteria, bool? isPitch = null);

        DealSummary GetDealSummary(string mentionId);

        IEnumerable<dESx_AssetUsageType> GetDealPropertyPrimaryUsages(int dealKey, params int[] propertyAssetKeys);

        dESx_DealValuation GetLatestValuation(int dealKey, params int[] propertyAssetKeys);
    }

    public class DealsService : IDealsService
    {
        private readonly IEsxDbContext _dbContext;
        private readonly ISecurityContext _securityContext;
        private readonly IDealCriteriaSetService _dealCriteriaSetService;
        private readonly IPropertyCriteriaSetService _propertyCriteriaSetService;
        private readonly ILoanCriteriaSetService _loanCriteriaSetService;
        private readonly ICompanyCriteriaSetService _companyCriteriaSetService;

        public DealsService(IEsxDbContext dbContext,
            ISecurityContext securityContext,
            IDealCriteriaSetService dealCriteriaSetService,
            IPropertyCriteriaSetService propertyCriteriaSetService,
            ILoanCriteriaSetService loanCriteriaSetService,
            ICompanyCriteriaSetService companyCriteriaSetService
            )
        {
            _dbContext = dbContext;
            _securityContext = securityContext;
            _dealCriteriaSetService = dealCriteriaSetService;
            _propertyCriteriaSetService = propertyCriteriaSetService;
            _loanCriteriaSetService = loanCriteriaSetService;
            _companyCriteriaSetService = companyCriteriaSetService;
        }

        public PagedResources<DealListItem> Search(DealSearchCriteria searchCriteria, bool? isPitch = null)
        {
            if (searchCriteria == null) throw new NullReferenceException(nameof(searchCriteria));

            var userKey = _securityContext.UserKey;

            var pitchStatuses = new[] { DealStatus.Pitching.Code, DealStatus.PitchedAndLost.Code };

            var dealAssetUsages = from assetUsage in _dbContext.dESx_AssetUsages
                                  join dealAsset in _dbContext.dESx_DealAssets on assetUsage.AssetKey equals dealAsset.AssetKey
                                  join assetUsageType in _dbContext.dESx_AssetUsageTypes on assetUsage.AssetUsageTypeKey equals
                                      assetUsageType.AssetUsageTypeKey
                                  where assetUsage.IsCurrent
                                  select new
                                  {
                                      dealAsset.DealKey,
                                      assetUsage.IsPrimary,
                                      assetUsageType.UsageType
                                  };

            var sellerCompanies = from dealCompany in _dbContext.dESx_DealCompanies
                                  join companyAlias in _dbContext.dESx_CompanyAlias on dealCompany.CompanyKey equals companyAlias.CompanyKey
                                  where dealCompany.DealParticipationTypeCode == DealParticipationType.Seller.Code
                                  select new
                                  {
                                      dealCompany.DealKey,
                                      companyAlias.AliasName
                                  };

            var dealsList = from deal in _dbContext.dESx_Deals
                            join dealStatus in _dbContext.dESx_DealStatusLogs on deal.DealKey equals dealStatus.DealKey
                            join dealValue in _dbContext.dESx_DealValues on deal.DealKey equals dealValue.DealKey
                            from office in _dbContext.dESx_Offices.Where(x => x.OfficeKey == deal.OfficeKey).DefaultIfEmpty()
                            from location in _dbContext.dESx_PhysicalAddresses.Where(x => office != null && x.PhysicalAddressKey == office.PhysicalAddressKey).DefaultIfEmpty()
                            from mention in _dbContext.dESx_Mentions.Where(mention => mention.DealKey == deal.DealKey).DefaultIfEmpty()
                            from userMention in _dbContext.dESx_UserMentionDatas.Where(um => mention != null && um.MentionKey == mention.MentionKey && um.UserKey == userKey).DefaultIfEmpty()
                            let userOnDealTeam = _dbContext.dESx_DealTeams.Any(x => x.DealKey == deal.DealKey && x.dESx_Contact.dESx_Users.Any(u => u.UserKey == userKey))
                            where dealStatus.IsCurrent
                                  && (isPitch == null || isPitch == pitchStatuses.Contains(dealStatus.DealStatusCode))
                            select new
                            {
                                userOnDealTeam,
                                Row = new DealListItem
                                {
                                    DealID = deal.DealKey,
                                    DealName = deal.DealName,
                                    DealCode = deal.DealCode,
                                    DealStatus = dealStatus.DealStatusCode,
                                    DealType = deal.DealTypeCode,
                                    DealMentionId = mention != null ? mention.MentionId : null,
                                    IsWatched = userMention != null && userMention.IsWatched,
                                    StatusDate = dealStatus.CreatedDate,
                                    PropertyType = (from dealAssetUsage in dealAssetUsages
                                                    where dealAssetUsage.DealKey == deal.DealKey
                                                    orderby dealAssetUsage.IsPrimary descending, dealAssetUsage.UsageType
                                                    select dealAssetUsage.UsageType).FirstOrDefault() ?? @"N/A",
                                    Valuation = dealValue.Value,
                                    DealOffice = office != null ? office.OfficeName : "N/A",
                                    Location = location != null ? location.Address1 : "N/A",
                                    UpdatedDate = deal.UpdatedDate,
                                    LaunchDate = deal.LaunchDate,
                                    SellerCompanies = sellerCompanies
                                                    .Where(x => x.DealKey == deal.DealKey)
                                                    .OrderBy(x => x.AliasName)
                                                    .Select(x => x.AliasName)
                                                    .FirstOrDefault(),
                                    DealParticipationTypeCode = "N/A"
                                }
                            };

            // If searching for company deals.
            if (!string.IsNullOrEmpty(searchCriteria.CompanyMentionId))
            {
                // Filter on the company mention and also fill in the DealParticipationTypeCode
                dealsList = from deal in dealsList
                            join dealCompany in _dbContext.dESx_DealCompanies on deal.Row.DealID equals dealCompany.DealKey
                            join companyAlias in _dbContext.dESx_CompanyAlias on dealCompany.CompanyKey equals companyAlias.CompanyKey
                            join companyMention in _dbContext.dESx_Mentions on companyAlias.CompanyAliasKey equals companyMention.AliasKey
                            where companyMention.MentionId == searchCriteria.CompanyMentionId
                            select new
                            {
                                deal.userOnDealTeam,
                                Row = new DealListItem
                                {
                                    DealID = deal.Row.DealID,
                                    DealName = deal.Row.DealName,
                                    DealCode = deal.Row.DealCode,
                                    DealStatus = deal.Row.DealStatus,
                                    DealType = deal.Row.DealType,
                                    DealMentionId = deal.Row.DealMentionId,
                                    IsWatched = deal.Row.IsWatched,
                                    StatusDate = deal.Row.StatusDate,
                                    PropertyType = deal.Row.PropertyType,
                                    Valuation = deal.Row.Valuation,
                                    DealOffice = deal.Row.DealOffice,
                                    Location = deal.Row.Location,
                                    UpdatedDate = deal.Row.UpdatedDate,
                                    LaunchDate = deal.Row.LaunchDate,
                                    SellerCompanies = deal.Row.SellerCompanies,
                                    DealParticipationTypeCode = dealCompany.DealParticipationTypeCode
                                }
                            };
            }

            // If searching for contact deals.
            if (!string.IsNullOrEmpty(searchCriteria.ContactMentionId))
            {
                throw new NotImplementedException();
            }

            // If searching for property deals.
            if (!string.IsNullOrEmpty(searchCriteria.PropertyMentionId))
            {
                var propertyDeals = from dealAsset in _dbContext.dESx_DealAssets
                                    join assetName in _dbContext.dESx_AssetNames on dealAsset.AssetKey equals assetName.AssetKey
                                    join propertyMention in _dbContext.dESx_Mentions on assetName.AssetNameKey equals
                                        propertyMention.PropertyAssetNameKey
                                    where propertyMention.MentionId == searchCriteria.PropertyMentionId
                                    select dealAsset.DealKey;

                dealsList = dealsList.Where(x => propertyDeals.Any(dealKey => dealKey == x.Row.DealID));
            }

            // Add search criteria if specified.
            if (!string.IsNullOrWhiteSpace(searchCriteria.SearchText) && searchCriteria.SearchText.Length > 2)
            {
                DateTime dateSearchBegin;
                var dateSearchEnd = DateTime.MinValue;
                var dateConversionSucceeded = DateTime.TryParse(searchCriteria.SearchText, out dateSearchBegin);
                if (dateConversionSucceeded) dateSearchEnd = dateSearchBegin.AddDays(1);

                dealsList = from deal in dealsList
                            where (deal.Row.DealName != null && deal.Row.DealName.Contains(searchCriteria.SearchText)
                                || deal.Row.DealCode != null && deal.Row.DealCode.Contains(searchCriteria.SearchText)
                                || deal.Row.DealOffice != null && deal.Row.DealOffice.Contains(searchCriteria.SearchText)
                                || deal.Row.DealStatus != null && deal.Row.DealStatus.Contains(searchCriteria.SearchText)
                                || deal.Row.PropertyType != null && deal.Row.PropertyType.Contains(searchCriteria.SearchText)
                                || deal.Row.Location != null && deal.Row.Location.Contains(searchCriteria.SearchText)
                                || deal.Row.DealType != null && deal.Row.DealType.Contains(searchCriteria.SearchText)
                                || deal.Row.Valuation != null && deal.Row.Valuation.ToString().Contains(searchCriteria.SearchText)
                                || deal.Row.UpdatedDate != null && dateConversionSucceeded
                                    && deal.Row.UpdatedDate.Value >= dateSearchBegin && deal.Row.UpdatedDate.Value < dateSearchEnd
                                || deal.Row.LaunchDate != null && dateConversionSucceeded
                                    && deal.Row.LaunchDate.Value >= dateSearchBegin && deal.Row.LaunchDate.Value < dateSearchEnd
                                || sellerCompanies.Any(x => x.DealKey == deal.Row.DealID && x.AliasName != null && x.AliasName.Contains(searchCriteria.SearchText))
                                || deal.Row.StatusDate != null && dateConversionSucceeded
                                    && deal.Row.StatusDate.Value >= dateSearchBegin && deal.Row.StatusDate.Value < dateSearchEnd
                                )
                            select deal;
            }

            // If searching deals using filters.
            if (searchCriteria.DealCriteriaSet != null)
            {
                var dealKeys = _dealCriteriaSetService.MatchingDealKeys(_dbContext, searchCriteria.DealCriteriaSet);
                dealsList = dealsList.Where(x => dealKeys.Any(key => x.Row.DealID == key));
            }

            // If searching deals by property.
            if (searchCriteria.PropertyCriteriaSet != null)
            {
                var propertyAssetKeys = _propertyCriteriaSetService.MatchingPropertyAssetKeys(_dbContext, searchCriteria.PropertyCriteriaSet);
                var dealProperties = _dbContext.dESx_DealAssets.Where(x => propertyAssetKeys.Any(key => key == x.AssetKey));
                dealsList = dealsList.Where(x => dealProperties.Any(cp => cp.DealKey == x.Row.DealID));
            }

            // If searching deals by loan.
            if (searchCriteria.LoanCriteriaSet != null)
            {
                var loanAssetKeys = _loanCriteriaSetService.MatchingLoanAssetKeys(_dbContext, searchCriteria.LoanCriteriaSet);
                var dealLoans = _dbContext.dESx_DealAssets.Where(x => loanAssetKeys.Any(key => key == x.AssetKey));
                dealsList = dealsList.Where(x => dealLoans.Any(l => l.DealKey == x.Row.DealID));
            }

            // If searching deals by company.
            if (searchCriteria.CompanyCriteriaSet != null)
            {
                var companyKeys = _companyCriteriaSetService.MatchingCompanyKeys(_dbContext, searchCriteria.CompanyCriteriaSet);
                var dealCompanies = _dbContext.dESx_DealCompanies.Where(x => companyKeys.Any(key => x.CompanyKey == key));
                dealsList = dealsList.Where(x => dealCompanies.Any(c => c.DealKey == x.Row.DealID));
            }

            searchCriteria.TotalCount = dealsList.Count();

            // Add sorting.
            if (string.IsNullOrWhiteSpace(searchCriteria.Sort))
            {
                searchCriteria.Sort = "IsWatched";
                searchCriteria.SortDescending = true;
            }
            var sortByUser = searchCriteria.ShowUserTeamDealsFirst ? "userOnDealTeam," : "";
            var dir = searchCriteria.SortDescending ? "descending" : "ascending";
            var sort = $"{sortByUser}Row.{searchCriteria.Sort} {dir}, Row.DealName ascending";

            dealsList = dealsList.OrderBy(sort).Skip(searchCriteria.Offset).Take(searchCriteria.PageSize);

            var pageResults = dealsList.ToList();
            var pageKeys = pageResults.Select(x => x.Row.DealID).Distinct().ToList();
            var pageUsages = dealAssetUsages.Where(dau => pageKeys.Contains(dau.DealKey)).Distinct().ToList();
            var pageSellers = sellerCompanies.Where(sc => pageKeys.Contains(sc.DealKey)).Distinct().ToList();
            pageResults.ForEach(deal =>
            {
                var propertyTypes = string.Join(", ",
                        pageUsages.Where(x => x.DealKey == deal.Row.DealID)
                            .OrderBy(x => x.IsPrimary)
                            .ThenBy(x => x.UsageType)
                            .Select(x => x.UsageType));
                deal.Row.PropertyType = string.IsNullOrEmpty(propertyTypes) ? "N/A" : propertyTypes;

                var sellers = string.Join(", ",
                    pageSellers.Where(x => x.DealKey == deal.Row.DealID)
                        .OrderBy(x => x.AliasName)
                        .Select(x => x.AliasName));

                deal.Row.SellerCompanies = string.IsNullOrEmpty(sellers) ? "N/A" : sellers;
            });

            return new PagedResources<DealListItem>
            {
                PagingData = searchCriteria,
                Resources = dealsList.Select(x => x.Row).ToList()
            };
        }

        public DealSummary GetDealSummary(string mentionId)
        {
            var dealSummary = (from m in _dbContext.dESx_Mentions
                               join deal in _dbContext.dESx_Deals on m.DealKey equals deal.DealKey
                               join dealStatus in _dbContext.dESx_DealStatusLogs on deal.DealKey equals dealStatus.DealKey
                               from office in _dbContext.dESx_Offices.Where(x => x.OfficeKey == deal.OfficeKey).DefaultIfEmpty()
                               from location in _dbContext.dESx_PhysicalAddresses.Where(x => office != null && x.PhysicalAddressKey == office.PhysicalAddressKey).DefaultIfEmpty()
                               join dealValue in _dbContext.dESx_DealValues on deal.DealKey equals dealValue.DealKey
                               where m.MentionId == mentionId
                                     && dealStatus.IsCurrent
                               select new DealSummary
                               {
                                   DealID = deal.DealKey,
                                   DealName = deal.DealName,
                                   DealCode = deal.DealCode,
                                   DealStatus = dealStatus.DealStatusCode,
                                   DealType = deal.DealTypeCode,
                                   DealMentionId = mentionId,
                                   StatusDate = dealStatus.CreatedDate,
                                   Valuation = dealValue.Value,
                                   DealOffice = office != null ? office.OfficeName : "N/A",
                                   Location = location != null ? location.Address1 : "N/A"
                               }).FirstOrDefault();

            if (dealSummary == null) return null;

            var dealProperties = (from dealAsset in _dbContext.dESx_DealAssets
                                  join asset in _dbContext.dESx_Assets on dealAsset.AssetKey equals asset.AssetKey
                                  join assetName in _dbContext.dESx_AssetNames on asset.AssetKey equals assetName.AssetKey
                                  from assetValue in _dbContext.dESx_AssetValues.Where(av => av.AssetKey == asset.AssetKey && av.IsCurrent).DefaultIfEmpty()
                                  join physicalAddress in _dbContext.dESx_PhysicalAddresses on asset.PhysicalAddressKey equals physicalAddress.PhysicalAddressKey
                                  join country in _dbContext.dESx_Countries on physicalAddress.CountryKey equals country.CountryKey
                                  where dealAsset.DealKey == dealSummary.DealID
                                  && asset.AssetTypeCode == AssetType.Property.Code
                                  select new PropertyDetails
                                  {

                                      PropertyKey = asset.AssetKey,
                                      Size = assetValue != null ? assetValue.Spaces : null,
                                      SizeUnits = assetValue != null ? assetValue.SpaceType : null,
                                      PropertyName = assetName.AssetName,
                                      Valuation = assetValue != null ? assetValue.AssetValue : (decimal?)null,
                                      PhysicalAddress = new PhysicalAddress
                                      {
                                          Address1 = physicalAddress.Address1,
                                          Address2 = physicalAddress.Address2,
                                          Address3 = physicalAddress.Address3,
                                          Address4 = physicalAddress.Address4,
                                          City = physicalAddress.City,
                                          County = physicalAddress.County,
                                          PostalCode = physicalAddress.PostalCode,
                                          StateProvidenceCode = physicalAddress.StateProvidenceCode,
                                          Country = country.CountryName,
                                          CountryKey = country.CountryKey,
                                          Latitude = physicalAddress.Latitude,
                                          Longitude = physicalAddress.Longitude
                                      }
                                  }).ToList();

            var sellerCompanies = (from dealCompany in _dbContext.dESx_DealCompanies
                                   join companyAlias in _dbContext.dESx_CompanyAlias on dealCompany.CompanyKey equals companyAlias.CompanyKey
                                   join m in _dbContext.dESx_Mentions on companyAlias.CompanyAliasKey equals m.AliasKey
                                   where dealCompany.DealParticipationTypeCode == DealParticipationType.Seller.Code
                                  && dealCompany.DealKey == dealSummary.DealID
                                   select new Alias
                                   {
                                       AliasName = companyAlias.AliasName,
                                       MentionId = m.MentionId
                                   }).OrderBy(a => a.AliasName).ToList();

            var brokers = from dealCompany in _dbContext.dESx_DealCompanies
                          join company in _dbContext.dESx_Companies on dealCompany.CompanyKey equals company.CompanyKey
                          join companyAlias in _dbContext.dESx_CompanyAlias on dealCompany.CompanyKey equals
                              companyAlias.CompanyKey
                          where dealCompany.DealParticipationTypeCode == DealParticipationType.Broker.Code
                                && dealCompany.DealKey == dealSummary.DealID
                                && companyAlias.IsPrimary
                          select new Broker
                          {
                              CompanyAlias = companyAlias.AliasName,
                              IsEastdil = company.IsEastdil
                          };


            dealSummary.Properties = dealProperties.ToList();
            dealSummary.SellerCompanies = sellerCompanies;
            var dealAssetUsages = from assetUsage in _dbContext.dESx_AssetUsages
                                  join dealAsset in _dbContext.dESx_DealAssets on assetUsage.AssetKey equals dealAsset.AssetKey
                                  join assetUsageType in _dbContext.dESx_AssetUsageTypes on assetUsage.AssetUsageTypeKey equals
                                      assetUsageType.AssetUsageTypeKey
                                  where assetUsage.IsCurrent && dealAsset.DealKey == dealSummary.DealID
                                  select new
                                  {
                                      dealAsset.DealKey,
                                      dealAsset.AssetKey,
                                      assetUsage.IsPrimary,
                                      assetUsageType.UsageType
                                  };

            var propertyKeys = dealProperties.Select(p => p.PropertyKey).ToList();
            var propertyUsages = dealAssetUsages.Where(a => propertyKeys.Contains(a.AssetKey));

            dealProperties.ForEach(p =>
            {
                var usages = string.Join(",", propertyUsages.Where(u => u.AssetKey == p.PropertyKey)
                    .OrderBy(a => a.IsPrimary).ThenBy(a => a.UsageType).Select(u => u.UsageType));

                p.Type = string.IsNullOrEmpty(usages) ? "N/A" : usages;

            });

            dealSummary.Properties = dealProperties.ToList();
            dealSummary.SellerCompanies = sellerCompanies;
            dealSummary.Brokers = brokers;

            return dealSummary;
        }

        public IEnumerable<dESx_AssetUsageType> GetDealPropertyPrimaryUsages(int dealKey, params int[] propertyAssetKeys)
        {
            if (propertyAssetKeys == null || !propertyAssetKeys.Any()) throw new ArgumentNullException(nameof(propertyAssetKeys), "propertyAssetKeys cannot be null or empty.");

            return (from dealAsset in _dbContext.dESx_DealAssets
                    join assetUsage in _dbContext.dESx_AssetUsages on dealAsset.AssetKey equals assetUsage.AssetKey
                    join assetUsageType in _dbContext.dESx_AssetUsageTypes on assetUsage.AssetUsageTypeKey equals assetUsageType.AssetUsageTypeKey
                    where dealAsset.DealKey == dealKey
                          && assetUsage.IsCurrent
                          && assetUsage.IsPrimary == true
                          && propertyAssetKeys.Contains(dealAsset.AssetKey)
                    select assetUsageType).Distinct().OrderBy(x => x.UsageType);
        }

        public dESx_DealValuation GetLatestValuation(int dealKey, params int[] propertyAssetKeys)
        {
            // Limit to only property assets.
            var dealValuationAssets = from dealValuationAsset in _dbContext.dESx_DealValuationAssets
                                      join dealAsset in _dbContext.dESx_DealAssets on dealValuationAsset.DealAssetKey equals
                                          dealAsset.DealAssetKey
                                      join asset in _dbContext.dESx_Assets on dealAsset.AssetKey equals asset.AssetKey
                                      where asset.AssetTypeCode == AssetType.Property.Code
                                            && dealAsset.DealKey == dealKey
                                      select dealValuationAsset;

            // If no properties are specified, it is assumed that we match on all properties.
            if (!propertyAssetKeys.Any())
            {
                propertyAssetKeys = (from dealAsset in _dbContext.dESx_DealAssets
                                     join asset in _dbContext.dESx_Assets on dealAsset.AssetKey equals asset.AssetKey
                                     where asset.AssetTypeCode == AssetType.Property.Code
                                           && dealAsset.DealKey == dealKey
                                     select dealAsset.AssetKey).ToArray();
            }

            // Get the latest valuation, but only if all its properties exactly match the currently selected properties.
            var valuation = (from dealValue in _dbContext.dESx_DealValues
                             join dealValuation in _dbContext.dESx_DealValuations on dealValue.DealValuationKey equals
                                 dealValuation.DealValuationKey
                             where dealValue.DealKey == dealKey
                                   && dealValuationAssets.All(x => propertyAssetKeys.Contains(x.DealAssetKey))
                                   && dealValuationAssets.Count() == propertyAssetKeys.Count()
                             select dealValuation).Include(x => x.dESx_DealValuationAssetUsageValues).FirstOrDefault();
            return valuation;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Search;
using Esx.Data;

namespace Esx.Business.Filter
{
    public interface IFilterService
    {
        List<FilterOption> GetFilterOptions(string coreObjectType);
    }

    public class FilterService : IFilterService
    {
        private readonly IEsxDbContext _dbContext;

        public FilterService(IEsxDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<FilterOption> GetFilterOptions(string coreObjectType)
        {
            switch (coreObjectType)
            {
                case CoreType.Company:
                    return new List<FilterOption> { GetCompaniesFilterOption(true), GetDealsFilterOption(true), GetLoansFilterOption(true) };
                case CoreType.Contact:
                    return new List<FilterOption> { GetContactsFilterOption(), GetCompaniesFilterOption(true) };
                case CoreType.Property:
                    return new List<FilterOption> { GetPropertiesFilterOption(), GetDealsFilterOption(false), GetLoansFilterOption(false), GetCompaniesFilterOption(false) };
                case CoreType.Deal:
                    return new List<FilterOption> { GetDealsFilterOption(true), GetLoansFilterOption(true), GetCompaniesFilterOption(true) };
                case CoreType.Loan:
                    return new List<FilterOption> { GetLoansFilterOption(true), GetDealsFilterOption(true), GetCompaniesFilterOption(true) };
                default:
                    throw new NotSupportedException("The core object type is not recognized.");
            }
        }

        private static FilterOption GetDealsFilterOption(bool includePropertyFilters)
        {
            var filterOption = new FilterOption
            {
                Category = "Deals",
                Filters = new List<FilterItem>
                {
                    new FilterItem
                    {
                        Name = FilterName.Deal.DealsIncluded,
                        Label = "Deals Included",
                        Type = FilterType.MultiSelect,
                        TypeValues = new List<FilterValue>
                        {
                            new FilterValue {Label = "All", Value = "all"},
                            new FilterValue {Label = "Most Recent", Value = "recent"}
                        }
                    },
                    new FilterItem
                    {
                        Name = FilterName.Deal.DealName,
                        Label = "Deal Name",
                        Type = FilterType.Input
                    },
                    new FilterItem
                    {
                        Name = FilterName.Deal.DealType,
                        Label = "Deal Type",
                        Type = FilterType.MultiSelect,
                        TypeValues = DealType.GetAll().Select(x => new FilterValue {Label = x.Label, Value = x.Code}).ToList()
                    },
                    new FilterItem
                    {
                        Name = FilterName.Deal.DealStatus,
                        Label = "Deal Status",
                        Type = FilterType.MultiSelect,
                        TypeValues =
                            DealStatus.GetAll().Select(x => new FilterValue {Label = x.Label, Value = x.Code}).ToList()
                    },
                    new FilterItem
                    {
                        Name = FilterName.Deal.LastCloseDate,
                        Label = "Last Close Date",
                        Type = FilterType.DateRange
                    },
                    new FilterItem
                    {
                        Name = FilterName.Deal.ValuationAmount,
                        Label = "Transaction Amount",
                        Type = FilterType.NumberRange
                    },
                    new FilterItem
                    {
                        Name = FilterName.Deal.BidderCount,
                        Label = "Bidder Count",
                        Type = FilterType.NumberRange
                    },
                    new FilterItem
                    {
                        Name = FilterName.Deal.BidDate,
                        Label = "Bid Date",
                        Type = FilterType.DateRange
                    },
                    new FilterItem
                    {
                        Name = FilterName.Deal.DealContactName,
                        Label = "Contact Name",
                        Type = FilterType.Input
                    }
                }
            };

            if (includePropertyFilters)
            {
                filterOption.Filters.AddRange(
                    new List<FilterItem>
                    {
                        new FilterItem
                        {
                            Name = FilterName.Deal.DealPropertyLocation,
                            Label = "Property Location",
                            Type = FilterType.Map
                        },
                        new FilterItem
                        {
                            Name = FilterName.Deal.DealPropertyType,
                            Label = "Property Type",
                            Type = FilterType.MultiSelect,
                            TypeValues = new List<FilterValue>
                            {
                                new FilterValue {Label = "Office", Value = "2"},
                                new FilterValue {Label = "Retail", Value = "3"},
                                new FilterValue {Label = "Hotel", Value = "4"},
                                new FilterValue {Label = "Residential", Value = "5"},
                                new FilterValue {Label = "Industrial", Value = "6"},
                            }
                        },
                        new FilterItem
                        {
                            Name = FilterName.Deal.DealPropertyRiskProfile,
                            Label = "Property Risk Profile",
                            Type = FilterType.MultiSelect,
                            TypeValues =
                                DealValuationRiskProfile.GetAll().Select(x => new FilterValue {Label = x.Label, Value = x.Code})
                                    .ToList()
                        }
                    });
            }
            return filterOption;
        }

        private static FilterOption GetCompaniesFilterOption(bool includePropertyFilters)
        {
            var filterOption = new FilterOption
            {
                Category = "Companies",
                Filters = new List<FilterItem>
                {
                    new FilterItem
                    {
                        Name = FilterName.Company.CompanyName,
                        Label = "Company Name",
                        Type = FilterType.Input
                    },
                    new FilterItem
                    {
                        Name = FilterName.Company.CompanyDealRole,
                        Label = "Deal Role",
                        Type = FilterType.MultiSelect,
                        TypeValues =
                            DealParticipationType.GetAll().Select(x => new FilterValue {Label = x.Label, Value = x.Code})
                                .ToList()
                    }
                }
            };
            if (includePropertyFilters)
            {
                filterOption.Filters.AddRange(new List<FilterItem>
                {
                     new FilterItem
                    {
                        Name = FilterName.Company.CompanyPropertyLocation,
                        Label = "Property Location",
                        Type = FilterType.Map
                    },
                    new FilterItem
                    {
                        Name = FilterName.Company.CompanyPropertyType,
                        Label = "Property Type",
                        Type = FilterType.MultiSelect,
                        TypeValues = new List<FilterValue>
                        {
                            new FilterValue {Label = "Office", Value = "2"},
                            new FilterValue {Label = "Retail", Value = "3"},
                            new FilterValue {Label = "Hotel", Value = "4"},
                            new FilterValue {Label = "Residential", Value = "5"},
                            new FilterValue {Label = "Industrial", Value = "6"},
                        }
                    },
                    new FilterItem
                    {
                        Name = FilterName.Company.CompanyPropertyRiskProfile,
                        Label = "Property Risk Profile",
                        Type = FilterType.MultiSelect,
                        TypeValues =
                            DealValuationRiskProfile.GetAll().Select(x => new FilterValue {Label = x.Label, Value = x.Code}).ToList()
                    }
                });
            }
            return filterOption;
        }

        private static FilterOption GetPropertiesFilterOption()
        {
            return new FilterOption
            {
                Category = "Properties",
                Filters = new List<FilterItem>
                {
                    new FilterItem
                    {
                        Name = FilterName.Property.PropertyName,
                        Label = "Property Name",
                        Type = FilterType.Input
                    },
                    new FilterItem
                    {
                        Name = FilterName.Property.PropertyLocation,
                        Label = "Location",
                        Type = FilterType.Map
                    },
                    new FilterItem
                    {
                        Name = FilterName.Property.PropertyType,
                        Label = "Property Type",
                        Type = FilterType.MultiSelect,
                        TypeValues = new List<FilterValue>
                        {
                            new FilterValue {Label = "Office", Value = "2"},
                            new FilterValue {Label = "Retail", Value = "3"},
                            new FilterValue {Label = "Hotel", Value = "4"},
                            new FilterValue {Label = "Residential", Value = "5"},
                            new FilterValue {Label = "Industrial", Value = "6"},
                        }
                    },
                    new FilterItem
                    {
                        Name = FilterName.Property.PropertySize,
                        Label = "Property Size",
                        Type = FilterType.NumberRange
                    }
                }
            };
        }

        private static FilterOption GetLoansFilterOption(bool includePropertyFilters)
        {
            var filterOption = new FilterOption
            {
                Category = "Loans",
                Filters = new List<FilterItem>
                {
                    new FilterItem
                    {
                        Name = FilterName.Loan.FinalMaturityDate,
                        Label = "Final Maturity Date",
                        Type = FilterType.DateRange
                    },
                    new FilterItem
                    {
                        Name = FilterName.Loan.LoanAmount,
                        Label = "Loan Amount",
                        Type = FilterType.NumberRange
                    },
                    new FilterItem
                    {
                        Name = FilterName.Loan.InterestRate,
                        Label = "Interest Rate",
                        Type = FilterType.NumberRange
                    }
                }
            };
            if (includePropertyFilters)
            {
                filterOption.Filters.AddRange(new List<FilterItem>
                {
                    new FilterItem
                    {
                        Name = FilterName.Loan.LoanPropertyLocation,
                        Label = "Property Location",
                        Type = FilterType.Map
                    },
                    new FilterItem
                    {
                        Name = FilterName.Loan.LoanPropertyType,
                        Label = "Property Type",
                        Type = FilterType.MultiSelect,
                        TypeValues = new List<FilterValue>
                        {
                            new FilterValue {Label = "Office", Value = "2"},
                            new FilterValue {Label = "Retail", Value = "3"},
                            new FilterValue {Label = "Hotel", Value = "4"},
                            new FilterValue {Label = "Residential", Value = "5"},
                            new FilterValue {Label = "Industrial", Value = "6"},
                        }
                    }
                });
            }
            return filterOption;
        }

        private FilterOption GetContactsFilterOption()
        {
            return new FilterOption
            {
                Category = "Contacts",
                Filters = new List<FilterItem>
                {
                    new FilterItem
                    {
                        Name = FilterName.Contact.ContactName,
                        Label = "Contact Name",
                        Type = FilterType.Input
                    },
                    new FilterItem
                    {
                        Name = FilterName.Contact.EastdilOffice,
                        Label = "Office",
                        Type = FilterType.MultiSelect,
                        TypeValues = _dbContext.dESx_EastdilOffices
                            .OrderBy(x => x.EastdilOfficeName)
                            .Select(x => new FilterValue
                            {
                                Label = x.EastdilOfficeName,
                                Value = x.EastdilOfficeKey.ToString()
                            })
                            .AsNoTracking()
                            .ToList()
                    }
                }
            };
        }
    }
}

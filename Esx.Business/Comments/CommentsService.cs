﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Esx.Contracts.Models.Comment;
using Esx.Contracts.Paging;
using Esx.Data;
using Esx.Data.Repositories;
using System.Linq.Dynamic;
using Esx.Business.Users;
using Esx.Contracts.Models;
using Esx.Contracts.Security;

namespace Esx.Business.Comments
{
    public interface ICommentsService
    {
        List<CommentListItem> GetComments(string mentionId, int rowCount = 10, int offSet = 1);

        List<CommentListItem> GetMentionComments(string mentionId, int rowCount = 10, int offSet = 1, string filterText = null);

        PagedResources<CommentListItem> GetMentionComments2(PagingData pagingData, string searchText = null,
            bool isWatched = false, bool reminders = false, bool isSnoozed = false, bool isDone = false,
            string mentionId = null);

        int SaveComment(Comment comment);

        void DeleteComment(int commentId);

        Reminder AddReminder(Reminder reminder);

        Reminder MarkReminderAsSnoozed(Reminder reminder);

        Reminder MarkReminderAsDone(int reminderId);

        void DeleteReminder(int reminderId);

        int GetActiveRemindersCount();
    }

    public class CommentsService:ICommentsService
    {
        private readonly ICommentsRepository _commentsRepository;
        private readonly IMappingEngine _mapper;
        private readonly IEsxDbContext _dbContext;
        private readonly ISecurityContext _securityContext;

        public CommentsService(
            ICommentsRepository commentsRepository, 
            IMappingEngine mapper, 
            IEsxDbContext dbContext, 
            ISecurityContext securityContext)
        {
            _commentsRepository = commentsRepository;
            _mapper = mapper;
            _dbContext = dbContext;
            _securityContext = securityContext;
        }

        public List<CommentListItem> GetComments(string mentionId, int pageSize = 10, int offSet = 1)
        {
            return _commentsRepository.GetComments(mentionId, pageSize, offSet);
        }

        public List<CommentListItem> GetMentionComments(string mentionId, int pageSize = 10, int offSet = 1, string filterText = null)
        {
            return _commentsRepository.GetMentionComments(mentionId, pageSize, offSet, filterText);
        }

        public PagedResources<CommentListItem> GetMentionComments2(PagingData pagingData, string searchText = null, bool isWatched = false, bool reminders = false, bool isSnoozed = false, bool isDone = false, string mentionId = null)
        {
            var mentionsList = from mention in _dbContext.dESx_Mentions
                join commentMention in _dbContext.dESx_CommentMentions on mention.MentionKey equals commentMention.MentionKey
                from contact in _dbContext.dESx_Contacts.Where(c => c.ContactKey == mention.ContactKey).DefaultIfEmpty()
                from company in _dbContext.dESx_CompanyAlias.Where(x => x.CompanyAliasKey == mention.AliasKey && x.IsPrimary).DefaultIfEmpty()
                from property in _dbContext.dESx_AssetNames.Where(x => x.AssetNameKey == mention.PropertyAssetNameKey && x.IsCurrent).DefaultIfEmpty()
                from deal in _dbContext.dESx_Deals.Where(x => x.DealKey == mention.DealKey).DefaultIfEmpty()
                from loan in _dbContext.dESx_AssetNames.Where(x => x.AssetNameKey == mention.LoanAssetNameKey && x.IsCurrent).DefaultIfEmpty()
                select new
                {
                    commentMention.CommentKey,
                    mention.MentionId,
                    mention.MentionKey,                    
                    MentionLabel = contact != null ? contact.FullName :
                                   company != null ? company.AliasName :
                                   property != null ? property.AssetName :
                                   deal != null ? deal.DealName :
                                   loan != null ? loan.AssetName : mention.MentionId
                };

            // 'Following' tab on pulse page
            if (isWatched)
            {
                mentionsList = from m in mentionsList
                    from userMention in
                        _dbContext.dESx_UserMentionDatas.Where(
                            x => x.MentionKey == m.MentionKey && x.UserKey == _securityContext.UserKey).DefaultIfEmpty()
                    from comment in _dbContext.dESx_Comments.Where(x => x.CommentKey == m.CommentKey).DefaultIfEmpty()
                    where (userMention != null && userMention.IsWatched) || (comment != null && comment.CreateUserKey == _securityContext.UserKey)
                    select m;
            }

            if (!string.IsNullOrEmpty(mentionId))
            {
                mentionsList = mentionsList.Where(m => m.MentionId == mentionId);
            }

            var commentAttachments = from attachment in _dbContext.dESx_CommentAttachments
                join document in _dbContext.dESx_Documents on attachment.DocumentKey equals document.DocumentKey
                select new
                {
                    attachment.CommentKey,
                    document.Name,
                    document.ExternalKey
                };            

            var commentsList = from comment in _dbContext.dESx_Comments       
                               from commentReminder in _dbContext.dESx_Reminders.Where(c => c.CommentKey == comment.CommentKey && !c.IsDeleted).DefaultIfEmpty()   
                               from reminderAssignment in _dbContext.dESx_ReminderAssignees.Where(x => x.ReminderKey == commentReminder.ReminderKey).DefaultIfEmpty()
                               from userAssignee in _dbContext.dESx_Users.Where(x => x.UserKey == reminderAssignment.AssigneeUserKey).DefaultIfEmpty()
                               from contactAssignee in _dbContext.dESx_Contacts.Where(x => x.ContactKey == userAssignee.ContactKey).DefaultIfEmpty()
                               from userAssigner in _dbContext.dESx_Users.Where(x => x.UserKey == reminderAssignment.AssignerUserKey).DefaultIfEmpty()
                               from contactAssigner in _dbContext.dESx_Contacts.Where(x => x.ContactKey == userAssigner.ContactKey).DefaultIfEmpty()
                               where (commentReminder == null || commentReminder.CreateUserKey == _securityContext.UserKey) || (reminderAssignment.AssigneeUserKey == _securityContext.UserKey)
                               select new CommentListItem
                                {
                                    Comment_Key = comment.CommentKey,
                                    CommentText = comment.CommentText,
                                    CreateTime = comment.CreateTime,                                        
                                    Mentions = mentionsList.Where(m => m.CommentKey == comment.CommentKey).Select(m => m.MentionLabel).ToList(),
                                    Reminder = commentReminder != null ? new Reminder {
                                        ReminderId = commentReminder.ReminderKey,
                                        ReminderDate = commentReminder.ReminderDate,
                                        ReminderText = commentReminder.ReminderText,
                                        DoneDate = commentReminder.IsDone ? commentReminder.UpdateTime : (DateTime?)null,
                                        IsSnoozed = commentReminder.IsSnoozed,
                                        IsDone = commentReminder.IsDone,
                                        AssigneeName = userAssignee != null && userAssignee.UserKey == _securityContext.UserKey ? "You" : contactAssignee != null ? contactAssignee.FullName : null,
                                        AssignerName = userAssigner != null && userAssigner.UserKey == _securityContext.UserKey ? "You" : contactAssigner != null ? contactAssigner.FullName : null
                                    } : null,                    
                                    Attachments = commentAttachments.Where(c => c.CommentKey == comment.CommentKey).Select(c => new Attachment
                                    {
                                        ExternalKey = c.ExternalKey,
                                        Name = c.Name
                                    }).ToList()
                                };

            if (isWatched || !string.IsNullOrEmpty(mentionId))
            {
                commentsList = commentsList.Where(x => x.Mentions.Any());
            }

            if (reminders)
            {
                commentsList = commentsList.Where(c => c.Reminder != null && c.Reminder.IsSnoozed == isSnoozed && c.Reminder.IsDone == isDone);
            }

            commentsList = commentsList.OrderBy("Comment_Key DESC");

            pagingData.TotalCount = commentsList.Count();
            if (pagingData.Offset > 0) commentsList = commentsList.Skip(pagingData.Offset);
            if (pagingData.PageSize > 0) commentsList = commentsList.Take(pagingData.PageSize);

            return new PagedResources<CommentListItem>
            {
                PagingData = pagingData,
                Resources = commentsList.ToList()
            };
        }

        public int SaveComment(Comment comment)
        {
            if (!comment.Mentions.Any())
            {
                var selfMention = (from user in _dbContext.dESx_Users
                    join mention in _dbContext.dESx_Mentions on user.ContactKey equals mention.ContactKey
                    where user.UserKey == _securityContext.UserKey
                    select new
                    {
                        mention.MentionId
                    }).FirstOrDefault();

                if (selfMention == null)
                    throw new NullReferenceException("User must have a mentionId");

                comment.Mentions.Add(selfMention.MentionId);
            }

            var newComment = new dESx_Comment
            {
                ImplicitMentionKey = 1, //TODO: Do we need this?
                CommentText = comment.CommentText,
                CreateUserKey = _securityContext.UserKey,
                UpdateUserKey = _securityContext.UserKey
            };

            var mentions = from mention in _dbContext.dESx_Mentions
                join commentMention in comment.Mentions on mention.MentionId equals commentMention
                select new
                {
                    mention.MentionKey
                };

            foreach (var mention in mentions)
            {
                newComment.dESx_CommentMentions.Add(new dESx_CommentMention
                {
                    MentionKey = mention.MentionKey
                });
            }

            foreach (var documentId in comment.DocumentIds)
            {
                newComment.dESx_CommentAttachments.Add(new dESx_CommentAttachment
                {
                    DocumentKey = documentId,
                    CreateUserKey = _securityContext.UserKey,
                    UpdateUserKey = _securityContext.UserKey
                });
            }

            _dbContext.dESx_Comments.Add(newComment);
            _dbContext.SaveChanges();

            if (comment.Reminder != null)
            {
                // From to-do page
                //if (comment.CreateReminder)
                //{
                //    var reminder = new dESx_Reminder
                //    {
                //        CommentKey = newComment.CommentKey,
                //        CreateUserKey = _securityContext.UserKey,
                //        UpdateUserKey = _securityContext.UserKey
                //    };
                //    newComment.dESx_Reminders.Add(reminder);
                //    _dbContext.SaveChanges();
                //}
                // From pin on list pages
             //   else
              //  {
                    comment.Reminder.CommentKey = newComment.CommentKey;
                    comment.Reminder.MentionKey = comment.CreateReminder ? mentions.First().MentionKey : (int?)null;
                    AddReminder(comment.Reminder);
                //}                               
            }

            return newComment.CommentKey;
        }

        public void DeleteComment(int commentId)
        {
            var commentMentions = _dbContext.dESx_CommentMentions.Where(cm => cm.CommentKey == commentId);            
            _dbContext.dESx_CommentMentions.RemoveRange(commentMentions);

            var commentReminders =
                _dbContext.dESx_Reminders.Where(
                    c => c.CommentKey == commentId);
            _dbContext.dESx_Reminders.RemoveRange(commentReminders);

            var commentAttachments = _dbContext.dESx_CommentAttachments.Where(c => c.CommentKey == commentId);
            _dbContext.dESx_CommentAttachments.RemoveRange(commentAttachments);

            var comment = _dbContext.dESx_Comments.Find(commentId);
            _dbContext.dESx_Comments.Remove(comment);
            _dbContext.SaveChanges();
        }

        public Reminder AddReminder(Reminder reminder)
        {
            //Update
            if (reminder.ReminderId.HasValue)
            {
                var reminderToUpdate = _dbContext.dESx_Reminders.Find(reminder.ReminderId.Value);
                if (reminderToUpdate == null || reminderToUpdate.CreateUserKey != _securityContext.UserKey)
                    return null;

                reminderToUpdate.ReminderDate = reminder.ReminderDate;
                reminderToUpdate.IsSnoozed = reminder.ReminderDate.HasValue;
                reminderToUpdate.IsDone = false;
                reminderToUpdate.ReminderText = reminder.ReminderText;

                _dbContext.SaveChanges();
                reminder.IsDone = false;
                reminder.DoneDate = null;
                return reminder;
            }

            //Add new
            var newReminder = new dESx_Reminder
            {
                CommentKey = reminder.CommentKey,
                MentionKey = reminder.MentionKey,
                ReminderDate = reminder.ReminderDate,
                IsSnoozed = reminder.ReminderDate.HasValue,
                ReminderText = reminder.ReminderText,                
                CreateUserKey = _securityContext.UserKey,
                UpdateUserKey = _securityContext.UserKey
            };

            if (reminder.Assignee.HasValue)
            {
               newReminder.dESx_ReminderAssignees.Add(new dESx_ReminderAssignee
               {
                   AssigneeUserKey = reminder.Assignee.Value,
                   AssignerUserKey = _securityContext.UserKey
               });
            }

            _dbContext.dESx_Reminders.Add(newReminder);
            _dbContext.SaveChanges();

            reminder.ReminderId = newReminder.ReminderKey;
            reminder.IsSnoozed = newReminder.IsSnoozed;
            return reminder;
        }

        public Reminder MarkReminderAsSnoozed(Reminder reminder)
        {
            var reminderToSnooze =
                _dbContext.dESx_Reminders.FirstOrDefault(
                    c => c.ReminderKey == reminder.ReminderId);
            if (reminderToSnooze == null)
                return null;

            reminderToSnooze.ReminderText = reminder.ReminderText;
            reminderToSnooze.ReminderDate = reminder.ReminderDate;
            reminderToSnooze.IsSnoozed = true;
            reminderToSnooze.IsDone = false;
            reminderToSnooze.UpdateTime = DateTime.Now;
            _dbContext.SaveChanges();

            return new Reminder
            {
                ReminderId = reminderToSnooze.ReminderKey,
                ReminderDate = reminderToSnooze.ReminderDate,
                ReminderText = reminderToSnooze.ReminderText,
                DoneDate = null,
                IsDone = false,
                IsSnoozed = reminderToSnooze.IsSnoozed
            };
        }

        public Reminder MarkReminderAsDone(int reminderId)
        {
            var reminder =
                _dbContext.dESx_Reminders.FirstOrDefault(
                    c => c.ReminderKey == reminderId);
            if (reminder == null)
                return null;

            reminder.IsDone = true;
            reminder.IsSnoozed = false;
            reminder.ReminderText = null;
            reminder.ReminderDate = null;
            reminder.UpdateTime = DateTime.Now;        
            _dbContext.SaveChanges();

            return new Reminder
            {
                ReminderId = reminder.ReminderKey,
                ReminderDate = reminder.ReminderDate,
                DoneDate = reminder.UpdateTime
            };
        }

        public void DeleteReminder(int reminderId)
        {
            var reminder =
                _dbContext.dESx_Reminders.FirstOrDefault(
                    c => c.ReminderKey == reminderId);
            if (reminder == null)
                return;

            reminder.IsDeleted = true;
            reminder.UpdateTime = DateTime.Now;
            _dbContext.SaveChanges();
        }

        public int GetActiveRemindersCount()
        {
            var commentsList = from comment in _dbContext.dESx_Comments
                join commentReminder in _dbContext.dESx_Reminders on comment.CommentKey equals
                    commentReminder.CommentKey
                where commentReminder.CreateUserKey == _securityContext.UserKey 
                && !commentReminder.IsSnoozed 
                && !commentReminder.IsDone
                && !commentReminder.IsDeleted
                select comment;
            return commentsList.Count();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Mention;
using Esx.Contracts.Paging;
using Esx.Data;
using Esx.Data.Repositories;

namespace Esx.Business.Mentions
{

    public interface IMentionsService
    {
        List<Mention> GetMentions(string searchText, int count);

        PagedResources<Mention> GetMentions2(PagingData pagingData, string searchText = null, string coreObjectType = null);

        Mention CreateNewMention(string objectType, int objectKey, bool isPrimary);

        void SetIsWatched(string mentionId, bool isWatched);
    }

    public class MentionsService : IMentionsService
    {
        private readonly IMentionsRepository _mentionRepository;
        private readonly IMappingEngine _mapper;
        private readonly IEsxDbContext _esxDbContext;

        public MentionsService(IMentionsRepository mentionRepository, IMappingEngine mapper, IEsxDbContext esxDbContext)
        {
            _mentionRepository = mentionRepository;
            _mapper = mapper;
            _esxDbContext = esxDbContext;
        }

        public List<Mention> GetMentions(string searchText, int count)
        {
            return _mentionRepository.GetMentions(searchText, count);
        }

        public PagedResources<Mention> GetMentions2(PagingData pagingData, string searchText = null, string coreObjectType = null)
        {
            var mentions = from mention in _esxDbContext.dESx_Mentions                           
                           from contact in _esxDbContext.dESx_Contacts.Where(c => c.ContactKey == mention.ContactKey).DefaultIfEmpty()
                           from company in _esxDbContext.dESx_CompanyAlias.Where(x => x.CompanyAliasKey == mention.AliasKey && x.IsPrimary).DefaultIfEmpty()
                           from property in _esxDbContext.dESx_AssetNames.Where(x => x.AssetNameKey == mention.PropertyAssetNameKey && x.IsCurrent).DefaultIfEmpty()
                           from deal in _esxDbContext.dESx_Deals.Where(x => x.DealKey == mention.DealKey).DefaultIfEmpty()
                           from loan in _esxDbContext.dESx_AssetNames.Where(x => x.AssetNameKey == mention.LoanAssetNameKey && x.IsCurrent).DefaultIfEmpty()
                           where mention.OfficeKey == null
                        select new Mention
                        {
                            MentionKey = mention.MentionKey,
                            MentionID = mention.MentionId,
                            ReferencedTableCode = mention.ReferencedTableCode,
                            MentionLabel = contact != null ? contact.FullName :
                                           company != null ? company.AliasName : 
                                           property != null ? property.AssetName : 
                                           deal != null ? deal.DealName : 
                                           loan != null ? loan.AssetName : mention.MentionId
                        };

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                mentions = from m in mentions
                           where (m.MentionID != null && m.MentionID.Contains(searchText))
                    select m;
            }

            if (!string.IsNullOrWhiteSpace(coreObjectType))
            {
                mentions = from m in mentions
                           where (m.MentionID != null && m.ReferencedTableCode == coreObjectType)
                    select m;
            }
            
            pagingData.TotalCount = mentions.Count();
            if (pagingData.Offset > 0) mentions = mentions.Skip(pagingData.Offset);
            if (pagingData.PageSize > 0) mentions = mentions.Take(pagingData.PageSize);

            return new PagedResources<Mention>
            {
                PagingData = pagingData,
                Resources = mentions.ToList()
            };
        }

        [Obsolete("Mentions are created automatically when you call EsxDbContext.SaveChanges()")]
        public Mention CreateNewMention(string objectType, int objectKey, bool isPrimary)
        {
            return _mentionRepository.CreateNewMention(objectType, objectKey, isPrimary);
        }

        public void SetIsWatched(string mentionId, bool isWatched)
        {
            _mentionRepository.SetIsWatched(mentionId, isWatched);
        }
    }
}

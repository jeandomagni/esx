﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Esx.Contracts.Models.Tag;
using Esx.Data;
using Esx.Data.Repositories;

namespace Esx.Business.Tags
{
    public interface ITagsService
    {
        List<Tag> GetTags(string searchText, int count);
        List<TagNote> GetTagNotes(string tagName, int pageSize, int offSet);
    }

    public class TagsService : ITagsService
    {
        private readonly ITagsRepository _tagsRepository;
        private readonly IMappingEngine _mapper;

        public TagsService(ITagsRepository tagsRepository, IMappingEngine mapper)
        {
            _tagsRepository = tagsRepository;
            _mapper = mapper;
        }

        public List<Tag> GetTags(string searchText, int count)
        {
            return _tagsRepository.GetTags(searchText, count);
            //var list = _tagsRepository.GetTags(searchText, count);
            //return _mapper.Map<List<IntellisenseTags_Result>, List<TagListItem>>(list);
        }

        public List<TagNote> GetTagNotes(string tagName, int pageSize, int offSet)
        {
            return _tagsRepository.GetTagNotes(tagName, pageSize, offSet);
            //var list = _tagsRepository.GetTagNotes(tagName, pageSize, offSet);
            //return _mapper.Map<List<GetTagNotes_Result>, List<TagNoteListItem>>(list);
        }
    }
}

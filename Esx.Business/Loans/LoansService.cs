﻿using System;
using System.Data.Entity;
using System.Linq;
using Esx.Contracts.Models.Loan;
using Esx.Contracts.Paging;
using System.Linq.Dynamic;
using Esx.Data;
using Esx.Contracts.Security;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Search;
using Esx.Business.Search;

namespace Esx.Business.Loans
{
    public interface ILoansService
    {
        PagedResources<LoanListItem> Search(LoanSearchCriteria searchCriteria);
    }

    public class LoansService : ILoansService
    {
        private readonly IEsxDbContext _dbContext;
        private readonly ISecurityContext _securityContext;
        private readonly IDealCriteriaSetService _dealCriteriaSetService;
        private readonly IPropertyCriteriaSetService _propertyCriteriaSetService;
        private readonly ILoanCriteriaSetService _loanCriteriaSetService;
        private readonly ICompanyCriteriaSetService _companyCriteriaSetService;

        public LoansService(
            IEsxDbContext dbContext,
            ISecurityContext securityContext,
            IDealCriteriaSetService dealCriteriaSetService,
            IPropertyCriteriaSetService propertyCriteriaSetService,
            ILoanCriteriaSetService loanCriteriaSetService,
            ICompanyCriteriaSetService companyCriteriaSetService
)
        {
            _dbContext = dbContext;
            _securityContext = securityContext;
            _dealCriteriaSetService = dealCriteriaSetService;
            _propertyCriteriaSetService = propertyCriteriaSetService;
            _loanCriteriaSetService = loanCriteriaSetService;
            _companyCriteriaSetService = companyCriteriaSetService;
        }

        public PagedResources<LoanListItem> Search(LoanSearchCriteria searchCriteria)
        {
            var assetDeals = from dealAsset in _dbContext.dESx_DealAssets
                             join deal in _dbContext.dESx_Deals on dealAsset.DealKey equals deal.DealKey
                             select new
                             {
                                 dealAsset.AssetKey,
                                 deal.DealCode
                             };

            var lenders = from loanAsset in _dbContext.dESx_Assets
                          join companyAsset in _dbContext.dESx_CompanyAssets on loanAsset.AssetKey equals companyAsset.AssetKey
                          join companyAlias in _dbContext.dESx_CompanyAlias on companyAsset.CompanyKey equals companyAlias.CompanyKey
                          where companyAsset.IsCurrent
                                && companyAlias.IsPrimary
                                && loanAsset.AssetTypeCode == AssetType.Loan.Code
                          select new
                          {
                              LoanAssetKey = loanAsset.AssetKey,
                              companyAlias.AliasName
                          };

            var borrowers = from loanAsset in _dbContext.dESx_Assets
                            join assetLoanBorrower in _dbContext.dESx_AssetLoanBorrowers on loanAsset.AssetKey equals assetLoanBorrower.AssetKey
                            join companyAlias in _dbContext.dESx_CompanyAlias on assetLoanBorrower.CompanyKey equals companyAlias.CompanyKey
                            where companyAlias.IsPrimary
                                  && loanAsset.AssetTypeCode == AssetType.Loan.Code
                            select new
                            {
                                LoanAssetKey = loanAsset.AssetKey,
                                companyAlias.AliasName
                            };

            var properties = from asset in _dbContext.dESx_Assets
                             join assetName in _dbContext.dESx_AssetNames on asset.AssetKey equals assetName.AssetKey
                             join assetLoanProperty in _dbContext.dESx_AssetLoanProperties on asset.AssetKey equals assetLoanProperty.PropertyAssetKey
                             join assetUsage in _dbContext.dESx_AssetUsages on assetLoanProperty.PropertyAssetKey equals assetUsage.AssetKey
                             join assetUsageType in _dbContext.dESx_AssetUsageTypes on assetUsage.AssetUsageTypeKey equals assetUsageType.AssetUsageTypeKey
                             where assetUsage.IsCurrent
                             select new
                             {
                                 loanKey = assetLoanProperty.LoanAssetKey,
                                 assetLoanProperty.PropertyAssetKey,
                                 assetUsageType.UsageType,
                                 assetName.AssetName,
                                 assetUsage.IsPrimary
                             };

            var now = DateTime.UtcNow;

            var loansList = from loanAsset in _dbContext.dESx_Assets
                            join loanAssetName in _dbContext.dESx_AssetNames on loanAsset.AssetKey equals loanAssetName.AssetKey
                            join loanTerms in _dbContext.dESx_AssetLoanTerms on loanAsset.AssetKey equals loanTerms.AssetKey
                            from loanMention in _dbContext.dESx_Mentions.Where(mention => mention.LoanAssetNameKey == loanAssetName.AssetNameKey && mention.IsPrimary).DefaultIfEmpty()
                            from umd in _dbContext.dESx_UserMentionDatas.Where(x => x.UserKey == _securityContext.UserKey && x.MentionKey == loanMention.MentionKey).DefaultIfEmpty()
                            let deal = assetDeals.FirstOrDefault(deal => deal.AssetKey == loanAsset.AssetKey)
                            where loanAssetName.IsCurrent && loanAsset.AssetTypeCode == AssetType.Loan.Code
                            select new LoanListItem
                            {
                                IsWatched = umd != null && umd.IsWatched,
                                LoanMentionId = loanMention.MentionId,
                                LoanKey = loanAsset.AssetKey,
                                LoanName = loanAssetName.AssetName,
                                LoanAmount = loanTerms.LoanAmount,
                                AllInRate = loanTerms.LoanRateTypeCode == LoanRateType.S.Code || loanTerms.LoanRateTypeCode == LoanRateType.T.Code
                                   ? loanTerms.LoanRate.ToString() : loanTerms.LoanDuration + " mo " + loanTerms.LoanRateTypeCode + " + " + loanTerms.CreditSpread,
                                OriginationDate = loanTerms.OriginationDate,
                                // A loan is active if either origination or maturity date is specified and the current time isn't outside their range.
                                Active = now >= loanTerms.OriginationDate || now <= loanTerms.MaturationDate,
                                MaturityDate = loanTerms.MaturationDate,
                                Lender = lenders.Where(x => x.LoanAssetKey == loanAsset.AssetKey).OrderBy(x => x.AliasName).Select(x => x.AliasName).FirstOrDefault() ?? @"N/A",
                                Borrower = borrowers.Where(x => x.LoanAssetKey == loanAsset.AssetKey).OrderBy(x => x.AliasName).Select(x => x.AliasName).FirstOrDefault() ?? @"N/A",
                                PropertyType = properties.Where(p => p.loanKey == loanAsset.AssetKey).OrderByDescending(p => p.IsPrimary).ThenBy(p => p.UsageType).Select(p => p.UsageType).FirstOrDefault() ?? @"N/A",
                                DealCode = deal != null ? deal.DealCode : null
                            };

            // If searching for company loans.
            if (!string.IsNullOrEmpty(searchCriteria.CompanyMentionId))
            {
                var companyLoans = from companyAsset in _dbContext.dESx_CompanyAssets
                                   join companyAlias in _dbContext.dESx_CompanyAlias on companyAsset.CompanyKey equals companyAlias.CompanyKey
                                   join companyMention in _dbContext.dESx_Mentions on companyAlias.CompanyAliasKey equals companyMention.AliasKey
                                   where companyMention.MentionId == searchCriteria.CompanyMentionId
                                   select companyAsset.AssetKey;

                loansList = loansList.Where(x => companyLoans.Any(loanKey => x.LoanKey == loanKey));
            }

            // If searching for property loans.
            if (!string.IsNullOrEmpty(searchCriteria.PropertyMentionId))
            {
                var propertyLoans = from loanProperty in _dbContext.dESx_AssetLoanProperties
                                    join propertyAssetName in _dbContext.dESx_AssetNames on loanProperty.PropertyAssetKey equals propertyAssetName.AssetKey
                                    join propertyMention in _dbContext.dESx_Mentions on propertyAssetName.AssetNameKey equals propertyMention.PropertyAssetNameKey
                                    where propertyMention.MentionId == searchCriteria.PropertyMentionId
                                    select loanProperty.LoanAssetKey;

                loansList = loansList.Where(x => propertyLoans.Any(loanKey => x.LoanKey == loanKey));
            }

            // If searching for deal loans.
            if (!string.IsNullOrEmpty(searchCriteria.DealMentionId))
            {
                var dealAssets = from dealAsset in _dbContext.dESx_DealAssets
                                 join dealMention in _dbContext.dESx_Mentions on dealAsset.DealKey equals dealMention.DealKey
                                 where dealMention.MentionId == searchCriteria.DealMentionId
                                 select dealAsset.AssetKey;

                loansList = loansList.Where(x => dealAssets.Any(assetKey => x.LoanKey == assetKey));
            }

            // If searching loans using filters.
            if (searchCriteria.LoanCriteriaSet != null)
            {
                var loanAssetKeys = _loanCriteriaSetService.MatchingLoanAssetKeys(_dbContext, searchCriteria.LoanCriteriaSet);
                loansList = loansList.Where(x => loanAssetKeys.Any(key => key == x.LoanKey));
            }

            // If searching loans by deal.
            if (searchCriteria.DealCriteriaSet != null)
            {
                var dealKeys = _dealCriteriaSetService.MatchingDealKeys(_dbContext, searchCriteria.DealCriteriaSet);
                var loanDeals = _dbContext.dESx_DealAssets.Where(x => dealKeys.Any(key => key == x.DealKey));
                loansList = loansList.Where(x => loanDeals.Any(ld => x.LoanKey == ld.AssetKey));
            }

            // If searching loans by property.
            if (searchCriteria.PropertyCriteriaSet != null)
            {
                var propertyAssetKeys = _propertyCriteriaSetService.MatchingPropertyAssetKeys(_dbContext, searchCriteria.PropertyCriteriaSet);
                var loanProperties = _dbContext.dESx_AssetLoanProperties.Where(x => propertyAssetKeys.Any(key => key == x.PropertyAssetKey));
                loansList = loansList.Where(x => loanProperties.Any(lp => lp.LoanAssetKey == x.LoanKey));
            }

            // If searching loans by company.
            if (searchCriteria.CompanyCriteriaSet != null)
            {
                var companyKeys = _companyCriteriaSetService.MatchingCompanyKeys(_dbContext, searchCriteria.CompanyCriteriaSet);
                var loanCompanies = _dbContext.dESx_CompanyAssets.Where(x => companyKeys.Any(key => x.CompanyKey == key));
                loansList = loansList.Where(x => loanCompanies.Any(c => c.AssetKey == x.LoanKey));
            }

            if (!string.IsNullOrWhiteSpace(searchCriteria.SearchText) && searchCriteria.SearchText.Length > 2)
            {
                loansList = from loan in loansList
                            where (loan.LoanName != null && loan.LoanName.Contains(searchCriteria.SearchText))
                                 || (loan.PropertyType != null && loan.PropertyType.Contains(searchCriteria.SearchText))
                                 || (loan.AllInRate != null && loan.AllInRate.Contains(searchCriteria.SearchText))
                                 || (lenders.Any(lender => lender.LoanAssetKey == loan.LoanKey && lender.AliasName.Contains(searchCriteria.SearchText)))
                                 || (borrowers.Any(borrower => borrower.LoanAssetKey == loan.LoanKey && borrower.AliasName.Contains(searchCriteria.SearchText)))
                                 || (loan.LoanAmount != null && loan.LoanAmount.ToString().Contains(searchCriteria.SearchText))
                                 || (properties.Any(p => p.PropertyAssetKey == loan.LoanKey && (p.UsageType.Contains(searchCriteria.SearchText) || p.AssetName.Contains(searchCriteria.SearchText))))
                                 || (loan.MaturityDate != null && loan.MaturityDate.ToString().Contains(searchCriteria.SearchText))
                                 || (loan.OriginationDate != null && loan.OriginationDate.ToString().Contains(searchCriteria.SearchText))
                            select loan;
            }

            if (string.IsNullOrWhiteSpace(searchCriteria.Sort))
            {
                searchCriteria.Sort = "IsWatched";
                searchCriteria.SortDescending = true;
            }
            var dir = searchCriteria.SortDescending ? "descending" : "ascending";
            var sort = $"{searchCriteria.Sort} {dir}, LoanAmount descending, LoanName ";

            searchCriteria.TotalCount = loansList.Count();
            loansList = loansList.OrderBy(sort).Skip(searchCriteria.Offset).Take(searchCriteria.PageSize);

            var loanListResult = loansList.AsNoTracking().ToList();

            var loanKeys = loanListResult.Select(k => k.LoanKey);

            var lenderList = lenders.Where(a => loanKeys.Contains(a.LoanAssetKey)).ToList();
            var borrowerList = borrowers.Where(a => loanKeys.Contains(a.LoanAssetKey)).ToList();
            var propertyList = properties.Where(a => loanKeys.Contains(a.loanKey)).ToList();

            loanListResult.ForEach(loan =>
            {
                loan.Lender = string.Join(", ", lenderList.Where(x => x.LoanAssetKey == loan.LoanKey).OrderBy(x => x.AliasName).Select(x => x.AliasName));
                if (string.IsNullOrEmpty(loan.Lender)) loan.Lender = "N/A";

                loan.Borrower = string.Join(", ", borrowerList.Where(x => x.LoanAssetKey == loan.LoanKey).OrderBy(x => x.AliasName).Select(x => x.AliasName));
                if (string.IsNullOrEmpty(loan.Borrower)) loan.Borrower = "N/A";

                var assetUsages = propertyList.Where(p => p.loanKey == loan.LoanKey)
                    .OrderByDescending(p => p.IsPrimary).ThenBy(p => p.UsageType).GroupBy(p => p.UsageType)
                    .Select(type => type.First().UsageType);

                loan.PropertyType = string.Join(", ", assetUsages);
                if (string.IsNullOrEmpty(loan.PropertyType)) loan.PropertyType = "N/A";
            });

            return new PagedResources<LoanListItem>
            {
                PagingData = searchCriteria,
                Resources = loanListResult
            };
        }
    }

}

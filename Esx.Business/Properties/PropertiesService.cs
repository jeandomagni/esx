﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Esx.Business.Search;
using Esx.Contracts.Enums;
using Esx.Contracts.Models.Property;
using Esx.Contracts.Paging;
using Esx.Contracts.Security;
using Esx.Data;
using Esx.Contracts.Models.Location;
using Esx.Contracts.Models.Search;
using Esx.Contracts.Models;
using System.Data.Entity;

namespace Esx.Business.Properties
{
    public interface IPropertiesService
    {
        string AddProperty(NewProperty property);
        PropertyDetails GetPropertyDetails(string mentionId);

        PagedResources<ValuationDetails> GetPropertyValuations(PagingData pagingData, string mentionId);

        IEnumerable<PropertyIntellisense> PropertyIntellisense(string searchText);
        IEnumerable<MapItem> MapItems(PropertySearchCriteria searchCriteria);

        PagedResources<PropertyListItem> Search(PropertySearchCriteria searchCriteria);
    }

    public class PropertiesService : IPropertiesService
    {
        private readonly IEsxDbContext _dbContext;
        private readonly ISecurityContext _securityContext;
        private readonly IDealCriteriaSetService _dealCriteriaSetService;
        private readonly ILoanCriteriaSetService _loanCriteriaSetService;
        private readonly ICompanyCriteriaSetService _companyCriteriaSetService;
        private readonly IPropertyCriteriaSetService _propertyCriteriaSetService;

        public PropertiesService(
            IEsxDbContext dbContext,
            ISecurityContext securityContext,
            IDealCriteriaSetService dealCriteriaSetService,
            ILoanCriteriaSetService loanCriteriaSetService,
            ICompanyCriteriaSetService companyCriteriaSetService,
            IPropertyCriteriaSetService propertyCriteriaSetService)
        {
            _dbContext = dbContext;
            _securityContext = securityContext;
            _dealCriteriaSetService = dealCriteriaSetService;
            _loanCriteriaSetService = loanCriteriaSetService;
            _companyCriteriaSetService = companyCriteriaSetService;
            _propertyCriteriaSetService = propertyCriteriaSetService;
        }

        public string AddProperty(NewProperty property)
        {
            var asset = _dbContext.dESx_Assets.Add(new dESx_Asset { AssetTypeCode = AssetType.Property.Label });
            var assetName = new dESx_AssetName { AssetName = property.PropertyName, AssetKey = asset.AssetKey };
            asset.dESx_AssetNames.Add(assetName);
            var physicalAddress = _dbContext.dESx_PhysicalAddresses.Add(new dESx_PhysicalAddress
            {
                PhysicalAddressTypeCode = PhysicalAddressType.Primary.Label,
                Description = property.FormattedAddress,
                Address1 = property.Address,
                City = property.City,
                PostalCode = property.Zipcode,
                StateProvidenceCode = property.State,
                CountryKey = 102,
                Longitude = property.Longitude,
                Latitude = property.Latiude
            });

            asset.PhysicalAddressKey = physicalAddress.PhysicalAddressKey;

            _dbContext.SaveChanges();
            return assetName.MentionId;
        }

        public PagedResources<PropertyListItem> Search(PropertySearchCriteria searchCriteria)
        {
            if (searchCriteria == null) throw new NullReferenceException(nameof(searchCriteria));

            var assetCompanies = from asset in _dbContext.dESx_Assets
                                 join companyAsset in _dbContext.dESx_CompanyAssets on asset.AssetKey equals companyAsset.AssetKey
                                 join companyAlias in _dbContext.dESx_CompanyAlias on companyAsset.CompanyKey equals companyAlias.CompanyKey
                                 join companyMention in _dbContext.dESx_Mentions on companyAlias.CompanyAliasKey equals companyMention.AliasKey
                                 where companyAsset.IsCurrent
                                 select new
                                 {
                                     PropertyAssetKey = asset.AssetKey,
                                     companyMention.MentionId,
                                     CompanyHasMultipleParents = _dbContext.dESx_CompanyHierarchies.Count(ch => ch.ChildCompanyKey == companyAsset.CompanyKey) > 1
                                 };

            var assetDeals = from asset in _dbContext.dESx_Assets
                             join dealAsset in _dbContext.dESx_DealAssets on asset.AssetKey equals dealAsset.AssetKey
                             join deal in _dbContext.dESx_Deals on dealAsset.DealKey equals deal.DealKey
                             join dealMention in _dbContext.dESx_Mentions on deal.DealKey equals dealMention.DealKey
                             join dealStatus in _dbContext.dESx_DealStatusLogs on deal.DealKey equals dealStatus.DealKey
                             where asset.AssetTypeCode == AssetType.Property.Code
                                && dealStatus.IsCurrent
                             select new
                             {
                                 asset.AssetKey,
                                 deal.DealKey,
                                 dealStatus.DealStatusCode,
                                 dealMention.MentionId,
                                 StatusDate = dealStatus.CreatedDate,
                                 deal.DealCode
                             };

            var propertyAssetUsages = from assetUsage in _dbContext.dESx_AssetUsages
                                      join assetUsageType in _dbContext.dESx_AssetUsageTypes on assetUsage.AssetUsageTypeKey equals assetUsageType.AssetUsageTypeKey
                                      where assetUsage.IsCurrent
                                      orderby assetUsage.IsPrimary, assetUsageType.UsageType
                                      select new
                                      {
                                          assetUsage.AssetKey,
                                          assetUsageType.UsageType,
                                          assetUsage.IsPrimary
                                      };

            var properties = from asset in _dbContext.dESx_Assets
                             join assetName in _dbContext.dESx_AssetNames on asset.AssetKey equals assetName.AssetKey
                             join propertyMention in _dbContext.dESx_Mentions on assetName.AssetNameKey equals propertyMention.PropertyAssetNameKey
                             from userMentionData in _dbContext.dESx_UserMentionDatas.Where(m => m.MentionKey == propertyMention.MentionKey).DefaultIfEmpty()
                             from pa in _dbContext.dESx_PhysicalAddresses.Where(x => x.PhysicalAddressKey == asset.PhysicalAddressKey).DefaultIfEmpty()
                             from assetValue in _dbContext.dESx_AssetValues.Where(x => x.AssetKey == asset.AssetKey && x.IsCurrent).DefaultIfEmpty()
                             from activeDeal in assetDeals.Where(activeDeal => activeDeal.AssetKey == asset.AssetKey && activeDeal.DealStatusCode != DealStatusCode.Closed).DefaultIfEmpty()
                             where assetName.IsCurrent
                             let lastClosedDeal = assetDeals.Where(x => x.DealStatusCode == DealStatusCode.Closed && asset.AssetKey == x.AssetKey).OrderByDescending(x => x.DealKey).Select(x => (DateTime?)x.StatusDate).FirstOrDefault()
                             let propertyHasMultipleOwners = assetCompanies.Count(x => x.PropertyAssetKey == asset.AssetKey) > 1
                             let companyHasMultipleParents = assetCompanies.Any(x => x.PropertyAssetKey == asset.AssetKey && x.CompanyHasMultipleParents)
                             select new PropertyListItem
                             {
                                 PropertyID = asset.AssetKey,
                                 PropertyName = assetName.AssetName,
                                 City = pa != null ? pa.City : null,
                                 State = pa != null ? pa.StateProvidenceCode : null,
                                 MentionId = propertyMention.MentionId,
                                 ActiveDeal = activeDeal != null,
                                 DealCode = activeDeal != null ? activeDeal.DealCode : null,
                                 Valuation = assetValue != null ? assetValue.AssetValue : 0,
                                 IsWatched = userMentionData != null && userMentionData.IsWatched,
                                 LastCloseDate = lastClosedDeal,
                                 Size = assetValue.Spaces,
                                 SizeUnits = assetValue.SpaceType,
                                 Type = propertyAssetUsages
                                            .Where(p => p.AssetKey == asset.AssetKey)
                                            .OrderByDescending(p => p.IsPrimary)
                                            .ThenBy(p => p.UsageType)
                                            .Select(p => p.UsageType)
                                            .FirstOrDefault() ?? @"N/A",
                                 JointVenture = propertyHasMultipleOwners || companyHasMultipleParents
                             };

            // If searching for company properties.
            if (!string.IsNullOrEmpty(searchCriteria.CompanyMentionId))
            {
                properties =
                    properties.Where(
                        x =>
                            assetCompanies.Any(
                                ac =>
                                    ac.PropertyAssetKey == x.PropertyID &&
                                    ac.MentionId == searchCriteria.CompanyMentionId));
            }

            // If searching for deal properties
            if (!string.IsNullOrEmpty(searchCriteria.DealMentionId))
            {
                properties =
                    properties.Where(
                        x =>
                            assetDeals.Any(
                                ad =>
                                    ad.AssetKey == x.PropertyID &&
                                    ad.MentionId == searchCriteria.DealMentionId));
            }

            // If searching properties using property filters.
            if (searchCriteria.PropertyCriteriaSet != null)
            {
                var propertyAssetKeys = _propertyCriteriaSetService.MatchingPropertyAssetKeys(_dbContext, searchCriteria.PropertyCriteriaSet);
                properties = properties.Where(x => propertyAssetKeys.Any(key => key == x.PropertyID));
            }

            // If searching properties by deal.
            if (searchCriteria.DealCriteriaSet != null)
            {
                var dealKeys = _dealCriteriaSetService.MatchingDealKeys(_dbContext, searchCriteria.DealCriteriaSet);
                var propertyDeals = _dbContext.dESx_DealAssets.Where(x => dealKeys.Any(key => key == x.DealKey));
                properties = properties.Where(x => propertyDeals.Any(pd => pd.AssetKey == x.PropertyID));
            }

            // If searching properties by loan.
            if (searchCriteria.LoanCriteriaSet != null)
            {
                var loanAssetKeys = _loanCriteriaSetService.MatchingLoanAssetKeys(_dbContext, searchCriteria.LoanCriteriaSet);
                var loanProperties = _dbContext.dESx_AssetLoanProperties.Where(x => loanAssetKeys.Any(key => key == x.LoanAssetKey));
                properties = properties.Where(x => loanProperties.Any(lp => lp.PropertyAssetKey == x.PropertyID));
            }

            // If searching properties by company.
            if (searchCriteria.CompanyCriteriaSet != null)
            {
                var companyKeys = _companyCriteriaSetService.MatchingCompanyKeys(_dbContext, searchCriteria.CompanyCriteriaSet);
                var companyProperties = _dbContext.dESx_CompanyAssets.Where(x => x.IsCurrent && companyKeys.Any(key => key == x.CompanyKey));
                properties = properties.Where(x => companyProperties.Any(cp => cp.AssetKey == x.PropertyID));
            }

            //Add search criteria if specified.
            if (!string.IsNullOrWhiteSpace(searchCriteria.SearchText) && searchCriteria.SearchText.Length > 2)
            {
                properties = from p in properties
                             where (p.PropertyName != null && p.PropertyName.Contains(searchCriteria.SearchText))
                                   || (p.Type != null && p.Type.Contains(searchCriteria.SearchText))
                             select p;
            }

            // Add sorting.
            if (string.IsNullOrWhiteSpace(searchCriteria.Sort))
            {
                searchCriteria.Sort = "IsWatched";
                searchCriteria.SortDescending = true;
            }
            var dir = searchCriteria.SortDescending ? "DESC" : "ASC";
            var sort = $"{searchCriteria.Sort} {dir}, PropertyName";

            searchCriteria.TotalCount = properties.Count();
            properties = properties.OrderBy(sort).Skip(searchCriteria.Offset).Take(searchCriteria.PageSize);

            // Fill in the lenders and borrowers as comma-separated lists.
            var resources = properties.AsNoTracking().ToList();
            var pagePropertyKeys = resources.Select(r => r.PropertyID).Distinct();
            var propertyTypesList = propertyAssetUsages.Where(x => pagePropertyKeys.Contains(x.AssetKey)).ToList();

            resources.ForEach(property =>
            {
                var type = string.Join(", ", propertyTypesList.Where(x => x.AssetKey == property.PropertyID).OrderByDescending(x => x.IsPrimary).ThenBy(x => x.UsageType).Select(x => x.UsageType));
                property.Type = string.IsNullOrEmpty(type) ? "N/A" : type;
            });

            return new PagedResources<PropertyListItem>
            {
                PagingData = searchCriteria,
                Resources = resources
            };
        }
        public IEnumerable<MapItem> MapItems(PropertySearchCriteria searchCriteria)
        {
            var properties = FilterProperties(searchCriteria, false);
            return (from property in properties
                    where property.Latitude != null
                          && property.Longitude != null
                    select new MapItem
                    {
                        MentionId = property.MentionId,
                        Name = property.PropertyName,
                        Latitude = property.Latitude,
                        Longitude = property.Longitude
                    }).ToList();
        }

        private IQueryable<PropertyListItem> FilterProperties(PropertySearchCriteria searchCriteria, bool pagedResource)
        {
            if (searchCriteria == null) throw new NullReferenceException(nameof(searchCriteria));

            var assetCompanies = from asset in _dbContext.dESx_Assets
                                 join companyAsset in _dbContext.dESx_CompanyAssets on asset.AssetKey equals companyAsset.AssetKey
                                 join companyAlias in _dbContext.dESx_CompanyAlias on companyAsset.CompanyKey equals companyAlias.CompanyKey
                                 join companyMention in _dbContext.dESx_Mentions on companyAlias.CompanyAliasKey equals companyMention.AliasKey
                                 where companyAsset.IsCurrent
                                 select new
                                 {
                                     PropertyAssetKey = asset.AssetKey,
                                     companyMention.MentionId,
                                     CompanyHasMultipleParents = _dbContext.dESx_CompanyHierarchies.Count(ch => ch.ChildCompanyKey == companyAsset.CompanyKey) > 1
                                 };

            var assetDeals = from asset in _dbContext.dESx_Assets
                             join dealAsset in _dbContext.dESx_DealAssets on asset.AssetKey equals dealAsset.AssetKey
                             join deal in _dbContext.dESx_Deals on dealAsset.DealKey equals deal.DealKey
                             join dealStatus in _dbContext.dESx_DealStatusLogs on deal.DealKey equals dealStatus.DealKey
                             where asset.AssetTypeCode == AssetType.Property.Code
                             && dealStatus.IsCurrent
                             select new
                             {
                                 asset.AssetKey,
                                 deal.DealKey,
                                 dealStatus.DealStatusCode,
                                 StatusDate = dealStatus.CreatedDate,
                                 deal.DealCode
                             };

            var propertyAssetUsages = from assetUsage in _dbContext.dESx_AssetUsages
                                      join assetUsageType in _dbContext.dESx_AssetUsageTypes on assetUsage.AssetUsageTypeKey equals assetUsageType.AssetUsageTypeKey
                                      where assetUsage.IsCurrent
                                      orderby assetUsage.IsPrimary, assetUsageType.UsageType
                                      select new
                                      {
                                          assetUsage.AssetKey,
                                          assetUsageType.UsageType,
                                          assetUsage.IsPrimary
                                      };

            var properties = from asset in _dbContext.dESx_Assets
                             join assetName in _dbContext.dESx_AssetNames on asset.AssetKey equals assetName.AssetKey
                             join propertyMention in _dbContext.dESx_Mentions on assetName.AssetNameKey equals propertyMention.PropertyAssetNameKey
                             from userMentionData in _dbContext.dESx_UserMentionDatas.Where(m => m.MentionKey == propertyMention.MentionKey).DefaultIfEmpty()
                             from pa in _dbContext.dESx_PhysicalAddresses.Where(x => x.PhysicalAddressKey == asset.PhysicalAddressKey).DefaultIfEmpty()
                             from assetValue in _dbContext.dESx_AssetValues.Where(x => x.AssetKey == asset.AssetKey && x.IsCurrent).DefaultIfEmpty()
                             from activeDeal in assetDeals.Where(activeDeal => activeDeal.AssetKey == asset.AssetKey && activeDeal.DealStatusCode != DealStatusCode.Closed).DefaultIfEmpty()
                             where assetName.IsCurrent
                             let lastClosedDeal = assetDeals.Where(x => x.DealStatusCode == DealStatusCode.Closed && asset.AssetKey == x.AssetKey).OrderByDescending(x => x.DealKey).Select(x => (DateTime?)x.StatusDate).FirstOrDefault()
                             let propertyHasMultipleOwners = assetCompanies.Count(x => x.PropertyAssetKey == asset.AssetKey) > 1
                             let companyHasMultipleParents = assetCompanies.Any(x => x.PropertyAssetKey == asset.AssetKey && x.CompanyHasMultipleParents)
                             select new PropertyListItem
                             {
                                 PropertyID = asset.AssetKey,
                                 PropertyName = assetName.AssetName,
                                 City = pa != null ? pa.City : null,
                                 State = pa != null ? pa.StateProvidenceCode : null,
                                 MentionId = propertyMention.MentionId,
                                 ActiveDeal = activeDeal != null,
                                 DealCode = activeDeal != null ? activeDeal.DealCode : null,
                                 Valuation = assetValue != null ? assetValue.AssetValue : 0,
                                 IsWatched = userMentionData != null && userMentionData.IsWatched,
                                 LastCloseDate = lastClosedDeal,
                                 Size = assetValue.Spaces,
                                 SizeUnits = assetValue.SpaceType,
                                 Type = propertyAssetUsages
                                            .Where(p => p.AssetKey == asset.AssetKey)
                                            .OrderByDescending(p => p.IsPrimary)
                                            .ThenBy(p => p.UsageType)
                                            .Select(p => p.UsageType)
                                            .FirstOrDefault() ?? @"N/A",
                                 JointVenture = propertyHasMultipleOwners || companyHasMultipleParents,
                                 Latitude = pa != null ? pa.Latitude : null,
                                 Longitude = pa != null ? pa.Longitude : null
                             };

            // If searching for company properties.
            if (!string.IsNullOrEmpty(searchCriteria.CompanyMentionId))
            {
                properties =
                    properties.Where(
                        x =>
                            assetCompanies.Any(
                                ac =>
                                    ac.PropertyAssetKey == x.PropertyID &&
                                    ac.MentionId == searchCriteria.CompanyMentionId));
            }

            // If searching properties using property filters.
            if (searchCriteria.PropertyCriteriaSet != null)
            {
                var propertyAssetKeys = _propertyCriteriaSetService.MatchingPropertyAssetKeys(_dbContext, searchCriteria.PropertyCriteriaSet);
                properties = properties.Where(x => propertyAssetKeys.Any(key => key == x.PropertyID));
            }

            // If searching properties by deal.
            if (searchCriteria.DealCriteriaSet != null)
            {
                var dealKeys = _dealCriteriaSetService.MatchingDealKeys(_dbContext, searchCriteria.DealCriteriaSet);

                var propertyDeals = from dealAsset in _dbContext.dESx_DealAssets
                                    join dealKey in dealKeys on dealAsset.DealKey equals dealKey
                                    select dealAsset;
                properties = properties.Where(x => propertyDeals.Any(pd => pd.AssetKey == x.PropertyID));
            }

            // If searching properties by loan.
            if (searchCriteria.LoanCriteriaSet != null)
            {
                var loanAssetKeys = _loanCriteriaSetService.MatchingLoanAssetKeys(_dbContext, searchCriteria.LoanCriteriaSet);

                var loanProperties = from loanProperty in _dbContext.dESx_AssetLoanProperties
                                     join loanAssetKey in loanAssetKeys on loanProperty.LoanAssetKey equals loanAssetKey
                                     select loanProperty;
                properties = properties.Where(x => loanProperties.Any(lp => lp.PropertyAssetKey == x.PropertyID));
            }

            // If searching properties by company.
            if (searchCriteria.CompanyCriteriaSet != null)
            {
                var companyKeys = _companyCriteriaSetService.MatchingCompanyKeys(_dbContext, searchCriteria.CompanyCriteriaSet);
                var companyProperties = from companyAsset in _dbContext.dESx_CompanyAssets
                                        join companyKey in companyKeys on companyAsset.CompanyKey equals companyKey
                                        where companyAsset.IsCurrent
                                        select companyAsset;
                properties = properties.Where(x => companyProperties.Any(cp => cp.AssetKey == x.PropertyID));
            }

            //Add search criteria if specified.
            if (!string.IsNullOrWhiteSpace(searchCriteria.SearchText) && searchCriteria.SearchText.Length > 2)
            {
                properties = from p in properties
                             where (p.PropertyName != null && p.PropertyName.Contains(searchCriteria.SearchText))
                                   || (p.Type != null && p.Type.Contains(searchCriteria.SearchText))
                             select p;
            }

            if (pagedResource)
            {
                // Add sorting.
                if (string.IsNullOrWhiteSpace(searchCriteria.Sort))
                {
                    searchCriteria.Sort = "IsWatched";
                    searchCriteria.SortDescending = true;
                }
                var dir = searchCriteria.SortDescending ? "DESC" : "ASC";
                var sort = $"{searchCriteria.Sort} {dir}, PropertyName";

                if (properties.Any())
                {
                    searchCriteria.TotalCount = properties.Count();
                }

                properties = properties.OrderBy(sort).Skip(searchCriteria.Offset).Take(searchCriteria.PageSize);

                // Fill in the lenders and borrowers as comma-separated lists.
                var resources = properties.ToList();
                var pagePropertyKeys = resources.Select(r => r.PropertyID).Distinct();
                var propertyTypesList = propertyAssetUsages.Where(x => pagePropertyKeys.Contains(x.AssetKey)).ToList();

                resources.ForEach(property =>
                {
                    var type = string.Join(", ", propertyTypesList.Where(x => x.AssetKey == property.PropertyID).OrderByDescending(x => x.IsPrimary).ThenBy(x => x.UsageType).Select(x => x.UsageType));
                    property.Type = string.IsNullOrEmpty(type) ? "N/A" : type;
                });

                properties = resources.AsQueryable();
            }

            return properties;
        }

        public PropertyDetails GetPropertyDetails(string mentionId)
        {
            var companyAssets = from companyAlias in _dbContext.dESx_CompanyAlias
                                join companyAsset in _dbContext.dESx_CompanyAssets on companyAlias.CompanyKey equals
                                    companyAsset.CompanyKey
                                where companyAlias.IsPrimary
                                select new { companyAsset.AssetKey, companyAlias.AliasName };

            var assetDeals = from asset in _dbContext.dESx_Assets
                             join dealAsset in _dbContext.dESx_DealAssets on asset.AssetKey equals dealAsset.AssetKey
                             join deal in _dbContext.dESx_Deals on dealAsset.DealKey equals deal.DealKey
                             join dealStatus in _dbContext.dESx_DealStatusLogs on deal.DealKey equals dealStatus.DealKey
                             where asset.AssetTypeCode == AssetType.Property.Code
                             && dealStatus.IsCurrent
                             select new
                             {
                                 asset.AssetKey,
                                 deal.DealKey,
                                 deal.DealName,
                                 dealStatus.DealStatusCode,
                                 StatusDate = dealStatus.CreatedDate
                             };

            var details = (from m in _dbContext.dESx_Mentions
                           join assetName in _dbContext.dESx_AssetNames on m.PropertyAssetNameKey equals assetName.AssetNameKey
                           join asset in _dbContext.dESx_Assets on assetName.AssetKey equals asset.AssetKey
                           from companyAsset in companyAssets.Where(x => x.AssetKey == asset.AssetKey).DefaultIfEmpty()
                           from pa in _dbContext.dESx_PhysicalAddresses.Where(x => x.PhysicalAddressKey == asset.PhysicalAddressKey).DefaultIfEmpty()
                           from activeDeal in assetDeals.Where(activeDeal => activeDeal.AssetKey == asset.AssetKey && activeDeal.DealStatusCode != DealStatusCode.Closed).DefaultIfEmpty()
                           from lastClosedDeal in assetDeals.Where(lastClosedDeal => lastClosedDeal.AssetKey == asset.AssetKey && lastClosedDeal.DealStatusCode == DealStatusCode.Closed).OrderByDescending(x => x.StatusDate).DefaultIfEmpty()
                           where m.MentionId == mentionId
                           && m.IsPrimary
                           && assetName.IsCurrent
                           select new PropertyDetails
                           {
                               PropertyName = assetName.AssetName,
                               LastCloseDate = lastClosedDeal != null ? (DateTime?)lastClosedDeal.StatusDate : null,
                               Owner = companyAsset != null ? companyAsset.AliasName : @"N/A",
                               ActiveDeal = activeDeal != null,
                               ValuationHistory = from v in _dbContext.dESx_AssetValues
                                                  where v.AssetKey == asset.AssetKey
                                                  orderby v.IsCurrent descending, v.CreatedDate descending
                                                  select new ValuationDetails
                                                  {
                                                      ValuationDataKey = v.AssetValueKey,
                                                      AskingPrice = v.AssetValue
                                                  },
                               PhysicalAddress = pa != null ? new PhysicalAddress()
                               {
                                   Address1 = pa.Address1,
                                   Address2 = pa.Address2,
                                   Address3 = pa.Address3,
                                   Address4 = pa.Address4,
                                   City = pa.City,
                                   StateProvidenceCode = pa.StateProvidenceCode,
                                   PostalCode = pa.PostalCode,
                                   Latitude = pa.Latitude,
                                   Longitude = pa.Longitude
                               } : null
                           }).FirstOrDefault();

            return details;
        }

        public PagedResources<ValuationDetails> GetPropertyValuations(PagingData pagingData, string mentionId)
        {
            var companyAssets = from companyAlias in _dbContext.dESx_CompanyAlias
                                join companyAsset in _dbContext.dESx_CompanyAssets on companyAlias.CompanyKey equals
                                    companyAsset.CompanyKey
                                where companyAlias.IsPrimary
                                select new { companyAsset.AssetKey, companyAlias.AliasName };

            var assetDeals = from asset in _dbContext.dESx_Assets
                             join dealAsset in _dbContext.dESx_DealAssets on asset.AssetKey equals dealAsset.AssetKey
                             join deal in _dbContext.dESx_Deals on dealAsset.DealKey equals deal.DealKey
                             join dealStatus in _dbContext.dESx_DealStatusLogs on deal.DealKey equals dealStatus.DealKey
                             where asset.AssetTypeCode == AssetType.Property.Code
                             && dealStatus.IsCurrent
                             select new
                             {
                                 asset.AssetKey,
                                 deal.DealName,
                                 deal.DealCode,
                                 dealStatus.DealStatusCode,
                                 StatusDate = dealStatus.CreatedDate
                             };

            var valuations = from m in _dbContext.dESx_Mentions
                             join assetName in _dbContext.dESx_AssetNames on m.PropertyAssetNameKey equals assetName.AssetNameKey
                             join asset in _dbContext.dESx_Assets on assetName.AssetKey equals asset.AssetKey
                             from companyAsset in companyAssets.Where(x => x.AssetKey == asset.AssetKey).DefaultIfEmpty()
                             from assetValue in _dbContext.dESx_AssetValues.Where(x => x.AssetKey == asset.AssetKey).DefaultIfEmpty()
                             from deal in assetDeals.Where(deal => deal.AssetKey == asset.AssetKey && deal.DealStatusCode != DealStatusCode.Closed).DefaultIfEmpty()
                             where m.MentionId == mentionId
                             && m.IsPrimary
                             && assetName.IsCurrent
                             select new ValuationDetails
                             {
                                 ValuationDataKey = assetValue.AssetValueKey,
                                 DealName = deal.DealName,
                                 DealCode = deal.DealCode,
                                 AskingPrice = assetValue.AssetValue,
                                 ValuationDate = assetValue.CreatedDate,
                                 Buyer = companyAsset.AliasName
                             };

            pagingData.TotalCount = valuations.Count();
            valuations = valuations.OrderBy("ValuationDataKey DESC");
            return new PagedResources<ValuationDetails>
            {
                PagingData = pagingData,
                Resources = valuations.ToList()
            };
        }

        public IEnumerable<PropertyIntellisense> PropertyIntellisense(string searchText)
        {
            var properties = (from asset in _dbContext.dESx_Assets
                              join assetName in _dbContext.dESx_AssetNames on asset.AssetKey equals assetName.AssetKey
                              from pa in _dbContext.dESx_PhysicalAddresses.Where(x => x.PhysicalAddressKey == asset.PhysicalAddressKey).DefaultIfEmpty()
                              where assetName.IsCurrent && (assetName.AssetName.Contains(searchText) || (pa != null && (pa.Address1.Contains(searchText) ||
                              pa.Address2.Contains(searchText) || pa.Address3.Contains(searchText) || pa.Address4.Contains(searchText))))
                              select new PropertyIntellisense
                              {
                                  PropertyKey = asset.AssetKey,
                                  PropertyName = assetName.AssetName,
                                  Address1 = (pa != null) ? pa.Address1 : null,
                                  Address2 = (pa != null) ? pa.Address2 : null,
                                  Address3 = (pa != null) ? pa.Address3 : null,
                                  Address4 = (pa != null) ? pa.Address4 : null
                              });

            return properties;
        }
    }

}

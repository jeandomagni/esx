﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using Esx.Business.Comments;
using Esx.Contracts.Enums;
using Esx.Contracts.ExceptionHandling;
using Esx.Contracts.Extensions;
using Esx.Contracts.Models;
using Esx.Contracts.Models.Deal;
using Esx.Contracts.Models.MarketingList;
using Esx.Contracts.Paging;
using Esx.Contracts.Security;
using Esx.Data;
using OfficeOpenXml;

// ReSharper disable PossibleMultipleEnumeration

namespace Esx.Business.MarketingList
{

    public interface IMarketingListService
    {
        PagedResources<MarketingListLookup> FindExistingMarketingLists(MarketingListSearchCriteria criteria);
        MarketingListLookup Add(string dealMention, MarketingListLookup[] esistingLists);
        MarketingListLookup GetDealMarketingList(string dealMentionId);
        PagedResources<MarketingListCompany> Search(MarketingListSearchCriteria criteria);
        PagedResources<MarketingListContact> GetContacts(MarketingListSearchCriteria criteria);
        MarketingListLookup AddContacts(int marketingListId, string[] contactMentionIds);
        MarketingListLookup RemoveContacts(int marketingListId, string[] contactMentionIds);
        MarketingListCompany SetStatus(int marketingListId, string[] contactMentionIds, string newStatus, DateTime date);
        MarketingListContact UpdateContact(MarketingListContact contact);
        void AddCA(int marketingListId, CompanyCA ca);
        byte[] Export(int marketingListId, string status, bool initial);


        // Engagement stuff
        PagedResources<CompanyEngagementItem> GetCompaniesForEngagement(MarketingListSearchCriteria criteria);
        PagedResources<MarketingListContact> GetPrimaryContactsForCompany(string companyMentionId, string searchText);

        PagedResources<DealListItem> GetDealsForCompany(string companyMentionId, string contactMentionId, bool hasCa = false);
    }

    public class MarketingListService : IMarketingListService
    {
        private readonly IEsxDbContext _dbContext;
        private readonly ISecurityContext _securityContext;
        private readonly ICommentsService _commentsService;

        public MarketingListService(IEsxDbContext dbContext, ISecurityContext securityContext, ICommentsService commentsService)
        {
            _dbContext = dbContext;
            _securityContext = securityContext;
            _commentsService = commentsService;
        }

        public PagedResources<MarketingListLookup> FindExistingMarketingLists(MarketingListSearchCriteria criteria)
        {

            var existingLists = from marketingList in _dbContext.dESx_MarketingLists
                                join deal in _dbContext.dESx_Deals on marketingList.DealKey equals deal.DealKey
                                select new MarketingListLookup
                                {
                                    DealCode = deal.DealCode,
                                    DealName = deal.DealName,
                                    Name = marketingList.Name,
                                    MarketingListId = marketingList.MarketingListKey
                                };

            existingLists = from lookup in existingLists
                            where (lookup.DealName.Contains(criteria.SearchText)
                                   || lookup.DealCode.Contains(criteria.SearchText)
                                   || lookup.Name.Contains(criteria.SearchText)
                                )
                            select lookup;

            return new PagedResources<MarketingListLookup>
            {
                Resources = existingLists.OrderBy("DealName").Skip(criteria.Offset).Take(criteria.PageSize).ToList(),
                PagingData = criteria
            };
        }

        public MarketingListLookup Add(string dealMention, MarketingListLookup[] existingLists)
        {
            //Add all the contacts from existing lists to a new marketing list
            var listIds = existingLists.Select(x => x.MarketingListId).Distinct();

            var deal = (from m in _dbContext.dESx_Mentions
                        join d in _dbContext.dESx_Deals on m.DealKey equals d.DealKey
                        where m.MentionId == dealMention
                        select new { d.DealKey, d.DealCode, d.DealName }).FirstOrDefault();

            if (deal == null)
                throw new NotFoundException(dealMention);

            var existingContacts = (from list in _dbContext.dESx_MarketingLists
                                    join listContacts in _dbContext.dESx_MarketingListContacts on list.MarketingListKey equals listContacts.MarketingListKey
                                    join contact in _dbContext.dESx_Contacts on listContacts.ContactKey equals contact.ContactKey
                                    join co in _dbContext.dESx_ContactOffices on contact.ContactKey equals co.ContactKey
                                    where listIds.Contains(list.MarketingListKey)
                                    select new { contact, co.IsPrimaryCompanyContact });

            var marketingList = new dESx_MarketingList
            {
                DealKey = deal.DealKey
            };

            foreach (var c in existingContacts.Distinct())
            {
                var mlc = new dESx_MarketingListContact
                {
                    ContactKey = c.contact.ContactKey,
                    IsPrimaryContact = c.IsPrimaryCompanyContact.GetValueOrDefault(),
                    IsSolicitable = c.contact.IsSolicitable,
                    IsRequestingTeaser = true,
                    IsRequestingCallForOffers = true,
                    IsRequestingAdditionalMaterials = c.contact.IsRequestingAdditionalMaterials,
                    IsRequestingWarRoomAccess = c.contact.IsRequestingWarRoomAccess,
                    IsRequestingPrintedOm = c.contact.IsRequestingPrintedOm,
                };
                marketingList.dESx_MarketingListContacts.Add(mlc);
            }
            _dbContext.dESx_MarketingLists.Add(marketingList);

            _dbContext.SaveChanges();

            var ret = new MarketingListLookup
            {
                MarketingListId = marketingList.MarketingListKey,
                Name = marketingList.Name,
                DealName = deal.DealName,
                DealCode = deal.DealCode,
            };

            return ret;
        }

        public MarketingListLookup GetDealMarketingList(string dealMentionId)
        {
            var list = (from m in _dbContext.dESx_Mentions
                        join d in _dbContext.dESx_Deals on m.DealKey equals d.DealKey
                        join l in _dbContext.dESx_MarketingLists on d.DealKey equals l.DealKey
                        where m.MentionId == dealMentionId
                        orderby l.MarketingListKey
                        select new MarketingListLookup
                        {
                            MarketingListId = l.MarketingListKey,
                            Name = l.Name,
                            DealName = d.DealName,
                            DealCode = d.DealCode,
                        }).FirstOrDefault();
            return list;
        }

        public PagedResources<MarketingListCompany> Search(MarketingListSearchCriteria criteria)
        {

            var electronicAddresses = from contact in _dbContext.dESx_Contacts
                                      join cea in _dbContext.dESx_ContactElectronicAddresses on contact.ContactKey equals cea.ContactKey
                                      join ea in _dbContext.dESx_ElectronicAddresses on cea.ElectronicAddressKey equals ea.ElectronicAddressKey
                                      where ea.ElectronicAddressTypeCode == ElectronicAddressType.EmailAddress
                                      select new { contact.ContactKey, Email = ea.Address };

            var companyQuery = (from marketingList in _dbContext.dESx_MarketingLists
                                join mlcontact in _dbContext.dESx_MarketingListContacts on marketingList.MarketingListKey equals mlcontact.MarketingListKey
                                join contacts in _dbContext.dESx_Contacts on mlcontact.ContactKey equals contacts.ContactKey
                                join contactOffice in _dbContext.dESx_ContactOffices on contacts.ContactKey equals contactOffice.ContactKey
                                join office in _dbContext.dESx_Offices on contactOffice.OfficeKey equals office.OfficeKey
                                join company in _dbContext.dESx_Companies on office.CompanyKey equals company.CompanyKey
                                join companyAlias in _dbContext.dESx_CompanyAlias on company.CompanyKey equals companyAlias.CompanyKey
                                where marketingList.MarketingListKey == criteria.MarketingListId && companyAlias.IsPrimary
                                from companyMention in _dbContext.dESx_Mentions.Where(m => m.AliasKey == companyAlias.CompanyAliasKey).DefaultIfEmpty()
                                from contactMention in _dbContext.dESx_Mentions.Where(m => m.ContactKey == mlcontact.ContactKey).DefaultIfEmpty()
                                from emails in electronicAddresses.Where(x => x.ContactKey == mlcontact.ContactKey).DefaultIfEmpty()
                                select new
                                {
                                    companyAlias.AliasName,
                                    company.CompanyKey,
                                    marketingList.MarketingListKey,
                                    contact = contacts,
                                    mlcontact,
                                    companyMentionId = companyMention != null ? companyMention.MentionId : null,
                                    contactMentionId = contactMention != null ? contactMention.MentionId : null,
                                    emails
                                });

            //Use later to set contacts
            var contactQuery = from c in companyQuery
                               select c;

            //Use later to set statuses
            var mlStatusLogQuery = (from sl in _dbContext.dESx_MarketingListContactStatusLogs
                                    join cc in companyQuery on sl.MarketingListContactKey equals cc.mlcontact.MarketingListContactKey
                                    select new { cc.CompanyKey, cc.mlcontact.ContactKey, sl.StatusDate, sl.MarketingListStatusCode }).Distinct();


            // Filter down by contact id
            if (criteria.ContactMentionId != null)
            {
                companyQuery = companyQuery.Where(x => x.contactMentionId == criteria.ContactMentionId);
            }
            // Filter down by company id
            if (criteria.CompanyMentionId != null)
            {
                companyQuery = companyQuery.Where(x => x.companyMentionId == criteria.CompanyMentionId);
            }
            // Filter down by search text
            if (!string.IsNullOrEmpty(criteria.SearchText))
            {
                companyQuery = companyQuery.Where(x => x.AliasName.Contains(criteria.SearchText)
                    || x.contact.FullName.Contains(criteria.SearchText)
                    || x.contact.Title.Contains(criteria.SearchText)
                    || x.emails.Email.Contains(criteria.SearchText)
                    );
            }


            //Filter by flags
            if (criteria.IsPrimary.HasValue)
                companyQuery = companyQuery.Where(x => x.mlcontact.IsPrimaryContact);
            if (criteria.IsSolicitable.HasValue)
                companyQuery = companyQuery.Where(x => x.mlcontact.IsSolicitable);
            if (criteria.IsRequestingTeaser.HasValue)
                companyQuery = companyQuery.Where(x => x.mlcontact.IsRequestingTeaser);
            if (criteria.IsRequestingWarRoomAccess.HasValue)
                companyQuery = companyQuery.Where(x => x.mlcontact.IsRequestingWarRoomAccess);
            if (criteria.IsRequestingAdditionalMaterials.HasValue)
                companyQuery = companyQuery.Where(x => x.mlcontact.IsRequestingAdditionalMaterials);
            if (criteria.IsRequestingPrintedOm.HasValue)
                companyQuery = companyQuery.Where(x => x.mlcontact.IsRequestingPrintedOm);
            if (criteria.IsRequestingCallForOffers.HasValue)
                companyQuery = companyQuery.Where(x => x.mlcontact.IsRequestingCallForOffers);

            // Get the paged companies. 
            var companies = (from c in companyQuery
                             select new MarketingListCompany
                             {
                                 CanonicalCompany = c.AliasName,
                                 CompanyKey = c.CompanyKey,
                                 MentionID = c.companyMentionId
                             })
                .Distinct()
                .OrderBy("CanonicalCompany")
                .Skip(criteria.Offset)
                .Take(criteria.PageSize)
                .ToList();

            //Filter down the logs to only the companies we need
            var companyKeys = companies.Select(x => x.CompanyKey);
            var mlStatusLogs = mlStatusLogQuery.Where(x => companyKeys.Contains(x.CompanyKey)).ToList();

            //get the contacts for only the companies listed
            var mlContacts = (from c in contactQuery
                              where companyKeys.Contains(c.CompanyKey)
                              select new
                              {
                                  companyKey = c.CompanyKey,
                                  companyAlias = c.AliasName,
                                  contact = new MarketingListContact
                                  {
                                      ContactId = c.contact.ContactKey,
                                      FirstName = c.contact.FirstName ?? string.Empty,
                                      LastName = c.contact.LastName ?? string.Empty,
                                      FullName = c.contact.FullName ?? string.Empty,
                                      EmailAddress = c.emails.Email ?? string.Empty,
                                      Title = c.contact.Title ?? string.Empty,
                                      IsPrimary = c.mlcontact.IsPrimaryContact,
                                      IsSolicitable = c.mlcontact.IsSolicitable,
                                      IsRequestingTeaser = c.mlcontact.IsRequestingTeaser,
                                      IsRequestingWarRoomAccess = c.mlcontact.IsRequestingWarRoomAccess,
                                      IsRequestingPrintedOm = c.mlcontact.IsRequestingPrintedOm,
                                      IsRequestingAdditionalMaterials = c.mlcontact.IsRequestingAdditionalMaterials,
                                      IsRequestingCallForOffers = c.mlcontact.IsRequestingCallForOffers,
                                      ContactMentionId = c.contactMentionId,
                                  }
                              })
                .Distinct()
                .ToList();

            foreach (var c in mlContacts)
            {
                c.contact.Statuses =
                    mlStatusLogs.Where(x => x.ContactKey == c.contact.ContactId)
                        .Select(x => new LogItem
                        {
                            Value = x.MarketingListStatusCode,
                            Date = x.StatusDate
                        })
                        .OrderByDescending(x => x.Date)
                        .ToList();
            }

            //set each companies info including a filtered down list of contacts. 
            foreach (var company in companies)
            {
                var contacts =
                    mlContacts.Where(x => x.companyKey == company.CompanyKey)
                        .Select(x => x.contact)
                        .OrderBy("LastName, FirstName");

                //Set all the company info
                company.IsSolicitable = contacts.Any(c => c.IsSolicitable ?? false);
                company.IsRequestingTeaser = contacts.Any(c => c.IsRequestingTeaser ?? false);
                company.IsRequestingWarRoomAccess = contacts.Any(c => c.IsRequestingWarRoomAccess ?? false);
                company.IsRequestingAdditionalMaterials = contacts.Any(c => c.IsRequestingAdditionalMaterials ?? false);
                company.IsRequestingPrintedOm = contacts.Any(c => c.IsRequestingPrintedOm ?? false);
                company.IsRequestingCallForOffers = contacts.Any(c => c.IsRequestingCallForOffers ?? false);
                company.Ca = GetCompanyCa(contacts);
                company.TeaserSent = GetCompanyTeaserSent(contacts);
                company.PrintedOm = GetCompanyPrintedOm(contacts);
                company.AdditionalMaterials = GetCompanyAdditionalMaterials(contacts);
                company.WarRoomAccess = GetCompanyWarRoomAccess(contacts);
                company.CallForOffers = GetCompanyCallForOffers(contacts);

                //Re filter to set the contacts
                //TODO figure out how to not double filter
                if (criteria.ContactMentionId != null)
                {
                    contacts = contacts.Where(x => x.ContactMentionId == criteria.ContactMentionId);
                }

                // Only filter down contacts via search text when the company does not contain the search text
                if (!string.IsNullOrEmpty(criteria.SearchText) && !company.CanonicalCompany.Contains(criteria.SearchText))
                {
                    contacts = contacts.Where(x => x.FullName.IgnoreCaseContains(criteria.SearchText)
                        || x.Title.IgnoreCaseContains(criteria.SearchText)
                        || x.EmailAddress.IgnoreCaseContains(criteria.SearchText));
                }

                //Filter by flags
                if (criteria.IsPrimary.HasValue)
                    contacts = contacts.Where(x => x.IsWatched);
                if (criteria.IsSolicitable.HasValue)
                    contacts = contacts.Where(x => x.IsSolicitable.HasValue && x.IsSolicitable.Value);
                if (criteria.IsRequestingTeaser.HasValue)
                    contacts = contacts.Where(x => x.IsRequestingTeaser.HasValue && x.IsRequestingTeaser.Value);
                if (criteria.IsRequestingWarRoomAccess.HasValue)
                    contacts = contacts.Where(x => x.IsRequestingWarRoomAccess.HasValue && x.IsRequestingWarRoomAccess.Value);
                if (criteria.IsRequestingAdditionalMaterials.HasValue)
                    contacts = contacts.Where(x => x.IsRequestingAdditionalMaterials.HasValue && x.IsRequestingAdditionalMaterials.Value);
                if (criteria.IsRequestingPrintedOm.HasValue)
                    contacts = contacts.Where(x => x.IsRequestingPrintedOm.HasValue && x.IsRequestingPrintedOm.Value);
                if (criteria.IsRequestingCallForOffers.HasValue)
                    contacts = contacts.Where(x => x.IsRequestingCallForOffers.HasValue && x.IsRequestingCallForOffers.Value);

                company.Contacts = contacts.ToList();
            }

            return new PagedResources<MarketingListCompany>
            {
                PagingData = criteria,
                Resources = companies.ToList()
            };
        }

        public PagedResources<MarketingListContact> GetContacts(MarketingListSearchCriteria criteria)
        {
            var mlContacts = (from mlc in _dbContext.dESx_MarketingListContacts
                              join c in _dbContext.dESx_Contacts on mlc.ContactKey equals c.ContactKey
                              where mlc.MarketingListKey == criteria.MarketingListId &&
                              (
                                c.FullName.Contains(criteria.SearchText) ||
                                c.MiddleName.Contains(criteria.SearchText) ||
                                c.Nickname.Contains(criteria.SearchText) ||
                                c.Title.Contains(criteria.SearchText)
                              )
                              select
                              new MarketingListContact
                              {
                                  ContactId = c.ContactKey,
                                  FirstName = c.FirstName,
                                  LastName = c.LastName,
                                  FullName = c.FullName,
                                  IsPrimary = mlc.IsPrimaryContact,
                                  IsSolicitable = mlc.IsSolicitable,
                                  IsRequestingTeaser = mlc.IsRequestingTeaser,
                                  IsRequestingWarRoomAccess = mlc.IsRequestingWarRoomAccess,
                                  IsRequestingPrintedOm = mlc.IsRequestingPrintedOm,
                                  IsRequestingAdditionalMaterials = mlc.IsRequestingAdditionalMaterials,
                                  IsRequestingCallForOffers = mlc.IsRequestingCallForOffers
                              })
                .Distinct()
                .Skip(criteria.Offset)
                .Take(criteria.PageSize)
                .ToList();

            return new PagedResources<MarketingListContact>
            {
                PagingData = criteria,
                Resources = mlContacts
            };

        }

        public MarketingListLookup AddContacts(int marketingListId, string[] contactMentionIds)
        {
            //get the marketingList
            var marketingList = GetMarketingList(marketingListId);

            //get the contacts

            var contactsToAdd = (from contact in _dbContext.dESx_Contacts
                join mentions in _dbContext.dESx_Mentions on contact.ContactKey equals mentions.ContactKey
                                 where contactMentionIds.Contains(mentions.MentionId)
                select new  { mentionId = mentions.MentionId, contact}
                ).ToList();

            foreach (var c in contactsToAdd)
            {

                // TODO Check to see if contact exists
                marketingList.dESx_MarketingListContacts.Add(new dESx_MarketingListContact
                {
                    ContactKey = c.contact.ContactKey,
                    IsSolicitable = c.contact.IsSolicitable,
                    IsRequestingTeaser = true,
                    IsRequestingCallForOffers = true,
                    IsRequestingAdditionalMaterials = c.contact.IsRequestingAdditionalMaterials,
                    IsRequestingWarRoomAccess = c.contact.IsRequestingWarRoomAccess,
                    IsRequestingPrintedOm = c.contact.IsRequestingPrintedOm,
                });
            }

            _dbContext.SaveChanges();

            return new MarketingListLookup { MarketingListId = marketingList.MarketingListKey };
        }

        public MarketingListLookup RemoveContacts(int marketingListId, string[] contactMentionIds)
        {
            //get the marketingList
            var marketingList = GetMarketingList(marketingListId);

            //get the contacts and their children
            var contactsToRemove = GetMarketingListContacts(marketingListId, contactMentionIds)
                .Include(x => x.dESx_MarketingListContactStatusLogs)
                .ToList();

            // blow them away
            foreach (var contact in contactsToRemove)
            {
                _dbContext.dESx_MarketingListContactStatusLogs.RemoveRange(contact.dESx_MarketingListContactStatusLogs);
                _dbContext.dESx_MarketingListContacts.RemoveRange(contactsToRemove);
            }

            _dbContext.SaveChanges();

            return new MarketingListLookup { MarketingListId = marketingList.MarketingListKey };
        }

        public MarketingListCompany SetStatus(int marketingListId, string[] contactMentionIds, string newStatus, DateTime date)
        {
            //check if a valid status
            if (!MarketingListStatusType.GetAll().Select(x => x.Code).Contains(newStatus))
                throw new NotFoundException("Status not found: " + newStatus);

            //get the marketingList
            GetMarketingList(marketingListId);

            //get the contacts and their children
            var contactsToUpdate = GetMarketingListContacts(marketingListId, contactMentionIds).ToList();

            // Add new status
            foreach (var contact in contactsToUpdate)
            {
                contact.dESx_MarketingListContactStatusLogs.Add(new dESx_MarketingListContactStatusLog
                {
                    MarketingListStatusCode = newStatus,
                    StatusDate = date
                });
            }

            _dbContext.SaveChanges();

            return new MarketingListCompany();
        }

        public MarketingListContact UpdateContact(MarketingListContact contact)
        {
            //get the contacts and their children
            var contactToUpdate = (from contacts in _dbContext.dESx_Contacts
                                   join mentions in _dbContext.dESx_Mentions on contacts.ContactKey equals mentions.ContactKey
                                   join mlc in _dbContext.dESx_MarketingListContacts on contacts.ContactKey equals mlc.ContactKey
                                   where mlc.MarketingListKey == contact.MarketingListId && mentions.MentionId == contact.ContactMentionId
                                   select mlc).FirstOrDefault();

            if (contactToUpdate == null)
                throw new NotFoundException("ContactMentionId: " + contact.ContactMentionId);

            if (contact.IsPrimary.HasValue) contactToUpdate.IsPrimaryContact = contact.IsPrimary.Value;
            if (contact.IsRequestingAdditionalMaterials.HasValue) contactToUpdate.IsRequestingAdditionalMaterials = contact.IsRequestingAdditionalMaterials.Value;
            if (contact.IsRequestingCallForOffers.HasValue) contactToUpdate.IsRequestingCallForOffers = contact.IsRequestingCallForOffers.Value;
            if (contact.IsRequestingPrintedOm.HasValue) contactToUpdate.IsRequestingPrintedOm = contact.IsRequestingPrintedOm.Value;
            if (contact.IsRequestingTeaser.HasValue) contactToUpdate.IsRequestingTeaser = contact.IsRequestingTeaser.Value;
            if (contact.IsRequestingWarRoomAccess.HasValue) contactToUpdate.IsRequestingWarRoomAccess = contact.IsRequestingWarRoomAccess.Value;
            if (contact.IsSolicitable.HasValue) contactToUpdate.IsSolicitable = contact.IsSolicitable.Value;

            _dbContext.SaveChanges();

            return contact;
        }

        public void AddCA(int marketingListId, CompanyCA companyCa)
        {
            //check if a valid status
            if (!MarketingListStatusType.GetAll().Select(x => x.Code).Contains(companyCa.Status))
                throw new NotFoundException("Status not found: " + companyCa.Status);

            var ml = GetMarketingList(marketingListId);

            //Get the company and its its contacts
            var companiesContacts = (from mlcontacts in _dbContext.dESx_MarketingListContacts
                                     join contacts in _dbContext.dESx_Contacts on mlcontacts.ContactKey equals contacts.ContactKey
                                     join contactOffice in _dbContext.dESx_ContactOffices on contacts.ContactKey equals contactOffice.ContactKey
                                     join office in _dbContext.dESx_Offices on contactOffice.OfficeKey equals office.OfficeKey
                                     join company in _dbContext.dESx_Companies on office.CompanyKey equals company.CompanyKey
                                     join mention in _dbContext.dESx_Mentions on contacts.ContactKey equals mention.ContactKey
                                     where mlcontacts.MarketingListKey == marketingListId
                                             && companyCa.ContactMentionIds.Contains(mention.MentionId)
                                     select new { company, mlcontacts }).Distinct().ToList();

            var companyList = companiesContacts.Select(x => x.company).Distinct();
            //Add marketingListCompany and its documents
            foreach (var company in companyList)
            {
                //Get the MarketingListCompany if it exists if not then make a new one.
                dESx_MarketingListCompany mlCompany;
                var existingCompany =
                    _dbContext.dESx_MarketingListCompanies
                        .FirstOrDefault(x => x.CompanyKey == company.CompanyKey);
                if (existingCompany != null)
                {
                    mlCompany = existingCompany;
                }
                else
                {
                    mlCompany = new dESx_MarketingListCompany
                    {
                        CompanyKey = company.CompanyKey
                    };
                }

                //Add the documents
                foreach (var documentId in companyCa.DocumentIds)
                {
                    mlCompany.dESx_MarketingListCompanyDocuments.Add(new dESx_MarketingListCompanyDocument
                    {
                        DocumentKey = documentId
                    });
                }
                ml.dESx_MarketingListCompanies.Add(mlCompany);
            }

            // Add ca status to all contacts
            var mlContacts = companiesContacts.Select(x => x.mlcontacts).Distinct();
            foreach (var contact in mlContacts)
            {
                contact.dESx_MarketingListContactStatusLogs.Add(new dESx_MarketingListContactStatusLog
                {
                    StatusDate = companyCa.StatusDate,
                    MarketingListStatusCode = companyCa.Status
                });
            }

            _dbContext.SaveChanges();
        }

        public byte[] Export(int marketingListId, string status, bool initial)
        {
            //check if a valid status
            if (!MarketingListStatusType.GetAll().Select(x => x.Code).Contains(status))
                throw new NotFoundException("Status not found: " + status);

            //Fetch all the contacts with given status and correct IsRequested flags
            var electronicAddresses = from contact in _dbContext.dESx_Contacts
                                      join cea in _dbContext.dESx_ContactElectronicAddresses on contact.ContactKey equals cea.ContactKey
                                      join ea in _dbContext.dESx_ElectronicAddresses on cea.ElectronicAddressKey equals ea.ElectronicAddressKey
                                      where ea.ElectronicAddressTypeCode == ElectronicAddressType.EmailAddress && 
                                            (ea.AddressUsageTypeCode == AddressUsageType.Main || ea.AddressUsageTypeCode == AddressUsageType.Alternate)
                                      select new { contact.ContactKey, Email = ea.Address, AddressUsageType = ea.AddressUsageTypeCode };

            var exportQuery = (from mlcontact in _dbContext.dESx_MarketingListContacts
                               join contacts in _dbContext.dESx_Contacts on mlcontact.ContactKey equals contacts.ContactKey
                               join contactOffice in _dbContext.dESx_ContactOffices on contacts.ContactKey equals contactOffice.ContactKey
                               join office in _dbContext.dESx_Offices on contactOffice.OfficeKey equals office.OfficeKey
                               join company in _dbContext.dESx_Companies on office.CompanyKey equals company.CompanyKey
                               join companyAlias in _dbContext.dESx_CompanyAlias on company.CompanyKey equals companyAlias.CompanyKey
                               where mlcontact.MarketingListKey == marketingListId && companyAlias.IsPrimary
                               from companyMention in _dbContext.dESx_Mentions.Where(m => m.AliasKey == companyAlias.CompanyAliasKey).DefaultIfEmpty()
                               from contactMention in _dbContext.dESx_Mentions.Where(m => m.ContactKey == mlcontact.ContactKey).DefaultIfEmpty()
                               from mainEmail in electronicAddresses.Where(x => x.ContactKey == mlcontact.ContactKey && x.AddressUsageType ==AddressUsageType.Main).DefaultIfEmpty()
                               from altEmail in electronicAddresses.Where(x => x.ContactKey == mlcontact.ContactKey && x.AddressUsageType ==AddressUsageType.Alternate).DefaultIfEmpty()
                               select new
                               {
                                   companyAlias.AliasName,
                                   company.CompanyKey,
                                   Contact = contacts,
                                   Mlcontact = mlcontact,
                                   CompanyMentionId = companyMention != null ? companyMention.MentionId : null,
                                   ContactMentionId = contactMention != null ? contactMention.MentionId : null,
                                   MainEmail = mainEmail.Email,
                                   AltEmail = altEmail.Email,
                               });

            if (status == MarketingListStatusType.TeaserSent.Code)
            {
                exportQuery = exportQuery.Where(x => x.Mlcontact.IsSolicitable && x.Mlcontact.IsRequestingTeaser);
            }
            if (status == MarketingListStatusType.PrintedOm.Code)
            {
                exportQuery = exportQuery.Where(x => x.Mlcontact.IsSolicitable && x.Mlcontact.IsRequestingPrintedOm);
            }
            if (status == MarketingListStatusType.AdditionalMaterials.Code)
            {
                var warRoomStatusQuery = (from sl in _dbContext.dESx_MarketingListContactStatusLogs
                                          join cc in exportQuery on sl.MarketingListContactKey equals cc.Mlcontact.MarketingListContactKey
                                          where sl.MarketingListStatusCode == MarketingListStatusType.WarRoomAccess.Code
                                          select cc.Mlcontact.ContactKey).Distinct().ToList();
                exportQuery =
                    exportQuery.Where(x => x.Mlcontact.IsSolicitable
                        && x.Mlcontact.IsRequestingAdditionalMaterials
                        && warRoomStatusQuery.Contains(x.Mlcontact.ContactKey));
                initial = true; // always only grab additional material people that have not been sent them. 
            }
            if (status == MarketingListStatusType.WarRoomAccess.Code)
            {
                exportQuery = exportQuery.Where(x => x.Mlcontact.IsSolicitable && x.Mlcontact.IsRequestingWarRoomAccess);
                initial = true; // always only grab war room people that have not been granted access. 
            }
            if (status == MarketingListStatusType.CallForOffers.Code)
            {
                exportQuery = exportQuery.Where(x => x.Mlcontact.IsSolicitable && x.Mlcontact.IsRequestingCallForOffers);
            }

            // Get only the ones that do not have the status
            if (initial)
            {
                var mlStatusLogQuery = (from sl in _dbContext.dESx_MarketingListContactStatusLogs
                                        join cc in exportQuery on sl.MarketingListContactKey equals cc.Mlcontact.MarketingListContactKey
                                        where sl.MarketingListStatusCode == status
                                        select cc.Mlcontact.ContactKey).Distinct().ToList();

                exportQuery = exportQuery.Where(x => !mlStatusLogQuery.Contains(x.Mlcontact.ContactKey) );
            }

            var contactList = exportQuery.ToList();

            DataTable exportTable = new DataTable();
            exportTable.Clear();
            exportTable.Columns.Add("Company");
            exportTable.Columns.Add("First Name");
            exportTable.Columns.Add("Last Name");
            exportTable.Columns.Add("Email");
            exportTable.Columns.Add("Secondary Email");
            exportTable.Columns.Add("Last Update");

            var now = DateTime.Now;
            //Build some list to export
            foreach (var contact in contactList)
            {
                //Add a date to all the activity logs. 
                contact.Mlcontact.dESx_MarketingListContactStatusLogs.Add(new dESx_MarketingListContactStatusLog
                {
                    MarketingListStatusCode = status,
                    StatusDate = now
                });
                //Add a row to the exportTable
                DataRow dtr = exportTable.NewRow();
                dtr["Company"] = contact.AliasName;
                dtr["First Name"] = contact.Contact.FirstName;
                dtr["Last Name"] = contact.Contact.LastName;
                dtr["Email"] = contact.MainEmail;
                dtr["Secondary Email"] = contact.AltEmail;
                dtr["Last Update"] = contact.Contact.UpdatedDate;
                exportTable.Rows.Add(dtr);
            }

            //Save changes
            _dbContext.SaveChanges();

            byte[] doc;
            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Content");
                ws.Cells["A1"].LoadFromDataTable(exportTable, true);
                //Write it to bytes
                doc = pck.GetAsByteArray();
            }
            return doc;

        }


        //TODO: Move these to an engage service?
        //TODO: Refactor to use shared search method.

        public PagedResources<CompanyEngagementItem> GetCompaniesForEngagement(MarketingListSearchCriteria criteria)
        {
            var electronicAddresses = from contact in _dbContext.dESx_Contacts
                                      join cea in _dbContext.dESx_ContactElectronicAddresses on contact.ContactKey equals cea.ContactKey
                                      join ea in _dbContext.dESx_ElectronicAddresses on cea.ElectronicAddressKey equals ea.ElectronicAddressKey
                                      select new { contact.ContactKey, ea.Address, Type = ea.ElectronicAddressTypeCode };

            var companyQuery = from company in _dbContext.dESx_Companies
                               join companyAlias in _dbContext.dESx_CompanyAlias on company.CompanyKey equals companyAlias.CompanyKey
                               join office in _dbContext.dESx_Offices on company.CompanyKey equals office.CompanyKey
                               join contactOffice in _dbContext.dESx_ContactOffices on office.OfficeKey equals contactOffice.OfficeKey
                               join contact in _dbContext.dESx_Contacts on contactOffice.ContactKey equals contact.ContactKey
                               join marketingContact in _dbContext.dESx_MarketingListContacts on contact.ContactKey equals marketingContact.ContactKey
                               join marketingList in _dbContext.dESx_MarketingLists on marketingContact.MarketingListKey equals marketingList.MarketingListKey
                               join deal in _dbContext.dESx_Deals on marketingList.DealKey equals deal.DealKey
                               join dealTeam in _dbContext.dESx_DealTeams on deal.DealKey equals dealTeam.DealKey
                               //join user in _dbContext.dESx_Users on dealTeam.ContactKey equals user.ContactKey
                               where companyAlias.IsPrimary //&& user.UserKey == _securityContext.UserKey
                               from companyMention in _dbContext.dESx_Mentions.Where(m => m.AliasKey == companyAlias.CompanyAliasKey).DefaultIfEmpty()
                               from contactMention in _dbContext.dESx_Mentions.Where(m => m.ContactKey == marketingContact.ContactKey).DefaultIfEmpty()
                               from emailAddress in electronicAddresses.Where(x => x.ContactKey == marketingContact.ContactKey && x.Type == ElectronicAddressType.EmailAddress).DefaultIfEmpty()
                               select new
                               {
                                   companyAlias.AliasName,
                                   company.CompanyKey,
                                   marketingList.MarketingListKey,
                                   contact,
                                   marketingContact,
                                   companyMentionId = companyMention != null ? companyMention.MentionId : null,
                                   contactMentionId = contactMention != null ? contactMention.MentionId : null,
                                   emailAddress
                               };

            //Use later to set contacts
            var contactQuery = from c in companyQuery
                               select c;

            //Use later to set statuses
            var mlStatusLogQuery = (from sl in _dbContext.dESx_MarketingListContactStatusLogs
                                    join cc in companyQuery on sl.MarketingListContactKey equals cc.marketingContact.MarketingListContactKey
                                    select new { cc.CompanyKey, cc.marketingContact.ContactKey, sl.StatusDate, sl.MarketingListStatusCode }).Distinct();


            // Filter down by contact id
            if (criteria.ContactMentionId != null)
            {
                companyQuery = companyQuery.Where(x => x.contactMentionId == criteria.ContactMentionId);
            }
            // Filter down by company id
            if (criteria.CompanyMentionId != null)
            {
                companyQuery = companyQuery.Where(x => x.companyMentionId == criteria.CompanyMentionId);
            }
            // Filter down by search text
            if (!string.IsNullOrEmpty(criteria.SearchText))
            {
                companyQuery = companyQuery.Where(x => x.AliasName.Contains(criteria.SearchText)
                    || x.contact.FullName.Contains(criteria.SearchText)
                    || x.contact.Title.Contains(criteria.SearchText)
                    );
            }


            var companies = (from c in companyQuery
                             select new CompanyEngagementItem
                             {
                                 CanonicalCompany = c.AliasName,
                                 CompanyKey = c.CompanyKey,
                                 MentionID = c.companyMentionId
                             })
                .Distinct()
                .OrderBy("CanonicalCompany")
                .Skip(criteria.Offset)
                .Take(criteria.PageSize)
                .ToList();

            //Filter down the logs to only the companies we need
            var companyKeys = companies.Select(x => x.CompanyKey);
            var mlStatusLogs = mlStatusLogQuery.Where(x => companyKeys.Contains(x.CompanyKey)).ToList();

            //get the contacts for only the companies listed
            var mlContacts = (from c in contactQuery
                              where companyKeys.Contains(c.CompanyKey)
                              select new
                              {
                                  companyKey = c.CompanyKey,
                                  contact = new MarketingListContact
                                  {
                                      ContactId = c.contact.ContactKey,
                                      FirstName = c.contact.FirstName ?? string.Empty,
                                      LastName = c.contact.LastName ?? string.Empty,
                                      FullName = c.contact.FullName ?? string.Empty,
                                      EmailAddress = c.emailAddress.Address,
                                      Title = c.contact.Title ?? string.Empty,
                                      IsPrimary = c.marketingContact.IsPrimaryContact,
                                      IsSolicitable = c.marketingContact.IsSolicitable,
                                      IsRequestingTeaser = c.marketingContact.IsRequestingTeaser,
                                      IsRequestingWarRoomAccess = c.marketingContact.IsRequestingWarRoomAccess,
                                      IsRequestingPrintedOm = c.marketingContact.IsRequestingPrintedOm,
                                      IsRequestingAdditionalMaterials = c.marketingContact.IsRequestingAdditionalMaterials,
                                      IsRequestingCallForOffers = c.marketingContact.IsRequestingCallForOffers,
                                      ContactMentionId = c.contactMentionId,
                                  }
                              })
                .Distinct()
                .ToList();

            foreach (var c in mlContacts)
            {
                c.contact.Statuses =
                    mlStatusLogs.Where(x => x.ContactKey == c.contact.ContactId)
                        .Select(x => new LogItem
                        {
                            Value = x.MarketingListStatusCode,
                            Date = x.StatusDate
                        })
                        .OrderByDescending(x => x.Date)
                        .ToList();
            }

            //set each companies info including a filter down list of contacts. 
            foreach (var company in companies)
            {
                var contacts =
                    mlContacts.Where(x => x.companyKey == company.CompanyKey)
                        .Select(x => x.contact)
                        .OrderBy("LastName, FirstName");

                //Set all the company info
                company.IsRequestingTeaser = contacts.Any(c => c.IsRequestingTeaser ?? false);
                company.IsRequestingWarRoomAccess = contacts.Any(c => c.IsRequestingWarRoomAccess ?? false);
                company.IsRequestingAdditionalMaterials = contacts.Any(c => c.IsRequestingAdditionalMaterials ?? false);
                company.IsRequestingPrintedOm = contacts.Any(c => c.IsRequestingPrintedOm ?? false);
                company.IsRequestingCallForOffers = contacts.Any(c => c.IsRequestingCallForOffers ?? false);
                company.Ca = GetCompanyCa(contacts);
                company.TeaserSent = GetCompanyTeaserSent(contacts);
                company.PrintedOm = GetCompanyPrintedOm(contacts);
                company.AdditionalMaterials = GetCompanyAdditionalMaterials(contacts);
                company.WarRoomAccess = GetCompanyWarRoomAccess(contacts);
                company.CallForOffers = GetCompanyCallForOffers(contacts);

                //Re filter to set the contacts
                //TODO figure out how to not double filter
                if (criteria.ContactMentionId != null)
                {
                    contacts = contacts.Where(x => x.ContactMentionId == criteria.ContactMentionId);
                }

                if (!string.IsNullOrEmpty(criteria.SearchText))
                {
                    //contacts = contacts.Where(x => x.FullName.IgnoreCaseContains(criteria.SearchText)
                    //    || x.Title.IgnoreCaseContains(criteria.SearchText)
                    //    || x.EmailAddress.IgnoreCaseContains(criteria.SearchText)
                    //    );
                }

                //Filter by flags
                if (criteria.IsPrimary.HasValue)
                    contacts = contacts.Where(x => x.IsWatched);
                if (criteria.IsSolicitable.HasValue)
                    contacts = contacts.Where(x => x.IsSolicitable.HasValue && x.IsSolicitable.Value);
                if (criteria.IsRequestingTeaser.HasValue)
                    contacts = contacts.Where(x => x.IsRequestingTeaser.HasValue && x.IsRequestingTeaser.Value);
                if (criteria.IsRequestingWarRoomAccess.HasValue)
                    contacts = contacts.Where(x => x.IsRequestingWarRoomAccess.HasValue && x.IsRequestingWarRoomAccess.Value);
                if (criteria.IsRequestingAdditionalMaterials.HasValue)
                    contacts = contacts.Where(x => x.IsRequestingAdditionalMaterials.HasValue && x.IsRequestingAdditionalMaterials.Value);
                if (criteria.IsRequestingPrintedOm.HasValue)
                    contacts = contacts.Where(x => x.IsRequestingPrintedOm.HasValue && x.IsRequestingPrintedOm.Value);
                if (criteria.IsRequestingCallForOffers.HasValue)
                    contacts = contacts.Where(x => x.IsRequestingCallForOffers.HasValue && x.IsRequestingCallForOffers.Value);

                company.Contacts = contacts.ToList();

                var lastReachedDate = DateTime.MinValue;
                foreach (var contact in contacts)
                {
                    var status = contact.Statuses.FirstOrDefault(x => x.Value != "Not Reached");
                    if (status != null && status.Date > lastReachedDate)
                    {
                        lastReachedDate = status.Date;
                    }
                }
                if (lastReachedDate != DateTime.MinValue)
                {
                    company.DaysSinceLastReached = DateTime.UtcNow.Subtract(lastReachedDate).Days;
                }

                company.NumberOfCasInPipeline =
                    contacts.Count(
                        c => c.Ca.Level == ActivityLevel.Complete || c.WarRoomAccess.Level == ActivityLevel.Complete);

            }

            return new PagedResources<CompanyEngagementItem>
            {
                PagingData = criteria,
                Resources = companies.ToList()
            };

        }

        public PagedResources<MarketingListContact> GetPrimaryContactsForCompany(string companyMentionId, string searchText)
        {
            var company = GetCompaniesForEngagement(new MarketingListSearchCriteria
            {
                CompanyMentionId = companyMentionId,
                SearchText = searchText
            }).Resources.FirstOrDefault();

            if (company == null)
            {
                return null;
            }

            var contacts = company.Contacts.OrderByDescending(x => x.IsPrimary);

            return new PagedResources<MarketingListContact>
            {
                PagingData = null,
                Resources = contacts.ToList()
            };
        }


        public PagedResources<DealListItem> GetDealsForCompany(string companyMentionId, string contactMentionId, bool hasCa = false)
        {
            var company = GetCompaniesForEngagement(new MarketingListSearchCriteria
            {
                CompanyMentionId = companyMentionId,
                ContactMentionId = contactMentionId
            }).Resources.FirstOrDefault();

            if (company == null)
            {
                return null;
            }

            var contacts = company.Contacts.AsEnumerable();

            if (hasCa)
            {
                contacts = contacts.Where(c => c.Ca.Level == ActivityLevel.Complete);
            }

            var contactKeys = contacts.Select(c => c.ContactId);

            var dealStatuses = from dealStatus in _dbContext.dESx_DealStatusLogs
                               join deal in _dbContext.dESx_Deals on dealStatus.DealKey equals deal.DealKey
                               orderby dealStatus.CreatedDate descending
                               select new
                               {
                                   deal.DealKey,
                                   dealStatus.DealStatusCode,
                                   dealStatus.CreatedDate
                               };

            var deals = from deal in _dbContext.dESx_Deals
                        join dealTeam in _dbContext.dESx_DealTeams on deal.DealKey equals dealTeam.DealKey
                        join dealMention in _dbContext.dESx_Mentions on deal.DealKey equals dealMention.DealKey
                        where contactKeys.Contains(dealTeam.ContactKey)
                        select new
                        {
                            deal.DealKey,
                            deal.DealName,
                            dealMention.MentionId
                        };

            var dealList = from deal in deals
                           select new DealListItem
                           {
                               DealID = deal.DealKey,
                               DealName = deal.DealName,
                               DealMentionId = deal.MentionId,
                               DealStatus = dealStatuses.Where(d => d.DealKey == deal.DealKey).Select(d => d.DealStatusCode).FirstOrDefault()
                           };

            return new PagedResources<DealListItem>
            {
                PagingData = null,
                Resources = dealList.ToList()
            };
        }

        #region helper functions

        private IQueryable<dESx_MarketingListContact> GetMarketingListContacts(int marketingListId, string[] contactMentionIds)
        {
            var contactsToUpdate = (from contacts in _dbContext.dESx_Contacts
                                    join mentions in _dbContext.dESx_Mentions on contacts.ContactKey equals mentions.ContactKey
                                    join mlc in _dbContext.dESx_MarketingListContacts on contacts.ContactKey equals mlc.ContactKey
                                    where mlc.MarketingListKey == marketingListId && contactMentionIds.Contains(mentions.MentionId)
                                    select mlc);
            return contactsToUpdate;
        }

        private dESx_MarketingList GetMarketingList(int marketingListId)
        {
            var marketingList = (from ml in _dbContext.dESx_MarketingLists
                                 where ml.MarketingListKey == marketingListId
                                 select ml).FirstOrDefault();

            if (marketingList == null)
                throw new NotFoundException("MarketingListID: " + marketingListId);
            return marketingList;
        }

        #endregion

        #region Calculate Company values

        private Activity GetCompanyCa(IEnumerable<MarketingListContact> contacts)
        {
            var activity = new Activity
            {
                Dates = new List<DateTime>()
            };
            var aproved =
                contacts.Where(c => c.Ca.Level == ActivityLevel.Complete)
                    .SelectMany(c => c.Ca.Dates.Select(d => d))
                    .OrderByDescending(d => d).ToList();

            if (aproved.Any())
            {
                activity.Dates.AddRange(aproved);
                activity.Level = ActivityLevel.Complete;
            }

            var follow =
                contacts.Where(c => c.Ca.Level == ActivityLevel.FollowUp)
                    .SelectMany(c => c.Ca.Dates.Select(d => d))
                    .OrderByDescending(d => d).ToList();

            if (follow.Any())
            {
                activity.Dates.AddRange(follow);
                activity.Level = ActivityLevel.FollowUp;
            }
            activity.Dates = activity.Dates.OrderByDescending(x => x.Date).ToList();

            return activity;
        }
        private Activity GetCompanyTeaserSent(IEnumerable<MarketingListContact> contacts)
        {
            var activity = new Activity
            {
                Dates = new List<DateTime>()
            };
            var aproved =
                    contacts.Where(c => c.TeaserSent.Level == ActivityLevel.Complete)
                        .SelectMany(c => c.TeaserSent.Dates.Select(d => d))
                        .OrderByDescending(d => d).Distinct().ToList();

            if (aproved.Any())
            {
                activity.Dates = aproved;
                activity.Level = ActivityLevel.Complete;
                return activity;
            }
            return activity;
        }
        private Activity GetCompanyPrintedOm(IEnumerable<MarketingListContact> contacts)
        {
            var activity = new Activity
            {
                Dates = new List<DateTime>()
            };
            var aproved =
                   contacts.Where(c => c.PrintedOm.Level == ActivityLevel.Complete)
                       .SelectMany(c => c.PrintedOm.Dates.Select(d => d))
                       .OrderByDescending(d => d).Distinct();

            if (aproved.Any())
            {
                activity.Dates = aproved.ToList();
                activity.Level = ActivityLevel.Complete;
                return activity;
            }
            return activity;
        }
        private Activity GetCompanyAdditionalMaterials(IEnumerable<MarketingListContact> contacts)
        {
            var activity = new Activity
            {
                Dates = new List<DateTime>()
            };
            var aproved =
                   contacts.Where(c => c.AdditionalMaterials.Level == ActivityLevel.Complete)
                       .SelectMany(c => c.AdditionalMaterials.Dates.Select(d => d))
                       .OrderByDescending(d => d).Distinct();

            if (aproved.Any())
            {
                activity.Dates = aproved.ToList();
                activity.Level = ActivityLevel.Complete;
                return activity;
            }
            return activity;
        }
        private Activity GetCompanyWarRoomAccess(IEnumerable<MarketingListContact> contacts)
        {
            var activity = new Activity
            {
                Dates = new List<DateTime>()
            };
            var aproved =
                  contacts.Where(c => c.WarRoomAccess.Level == ActivityLevel.Complete)
                      .SelectMany(c => c.WarRoomAccess.Dates.Select(d => d))
                      .OrderByDescending(d => d).Distinct();

            if (aproved.Any())
            {
                activity.Dates = aproved.ToList();
                activity.Level = ActivityLevel.Complete;
                return activity;
            }
            return activity;
        }
        private Activity GetCompanyCallForOffers(IEnumerable<MarketingListContact> contacts)
        {
            var activity = new Activity
            {
                Dates = new List<DateTime>()
            };
            var aproved =
                   contacts.Where(c => c.CallForOffers.Level == ActivityLevel.Complete)
                       .SelectMany(c => c.CallForOffers.Dates.Select(d => d))
                       .OrderByDescending(d => d).Distinct();

            if (aproved.Any())
            {
                activity.Dates = aproved.ToList();
                activity.Level = ActivityLevel.Complete;
                return activity;
            }
            return activity;
        }

        #endregion
    }
}
